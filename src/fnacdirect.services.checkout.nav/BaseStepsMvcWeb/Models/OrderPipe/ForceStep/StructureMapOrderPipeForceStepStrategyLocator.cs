﻿using FnacDirect.OrderPipe.Config;
using StructureMap;

namespace FnacDirect.OrderPipe.CoreRouting
{
    public class StructureMapOrderPipeForceStepStrategyLocator : IOrderPipeForceStepStrategyLocator
    {
        private readonly IContainer _container;

        public StructureMapOrderPipeForceStepStrategyLocator(IContainer container)
        {
            _container = container;
        }

        public IOrderPipeForceStepStrategy GetOrderPipeForceStepStrategyFor<TDescriptor>(TDescriptor descriptor) where TDescriptor : class, IUiPipeDescriptor
        {
            var instanceType = descriptor.GetType();
            var serviceType = typeof(IOrderPipeForceStepStrategy<>);
            var concreteServiceType = serviceType.MakeGenericType(instanceType);

            return _container.GetInstance(concreteServiceType) as IOrderPipeForceStepStrategy;
        }
    }
}
