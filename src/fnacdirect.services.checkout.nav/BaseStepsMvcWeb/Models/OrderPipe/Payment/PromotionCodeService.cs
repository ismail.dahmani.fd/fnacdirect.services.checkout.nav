using System.Linq;
using System.Text.RegularExpressions;
using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.Technical.Framework.Web.Mvc;

namespace FnacDirect.OrderPipe.Base.Business.Payment
{
    public class PromotionCodeService : IPromotionCodeService
    {
        private readonly IApplicationContext _applicationContext;
        private readonly IVoucherBusiness _voucherBusiness;
        private readonly Regex _cuuPatern;

        public PromotionCodeService(IApplicationContext applicationContext, IVoucherBusiness voucherBusiness)
        {
            _applicationContext = applicationContext;
            _voucherBusiness = voucherBusiness;
            _cuuPatern = new Regex(@"^\w{4}\-\w{4}\-\w{4}$");
        }

        public void AddCode(string code, OPContext opContext)
        {
            if (opContext.OF is PopOrderForm popOrderForm && !string.IsNullOrEmpty(code))
            {
                if (CodeType(code) == Payment.CodeType.Cuu && CanAddCuu(popOrderForm))
                {
                    if (CheckVoucher(code, popOrderForm) != VoucherBusiness.CheckCode.DoesNotExist)
                    {
                        AddCuu(code, opContext, popOrderForm);
                    }
                }
                else if (CanAddAdvantageCode(popOrderForm) && !HasCuu(popOrderForm))
                {
                    AddAdvatageCode(code, popOrderForm);
                }
            }
        }

        public void RemoveCode(string code, PopOrderForm popOrderForm)
        {
            if (CodeType(code) == Payment.CodeType.AdvantageCode)
            {
                popOrderForm.OrderGlobalInformations.AdvantageCode = string.Empty;
            }
            else
            {
                _voucherBusiness.RemoveVoucher(popOrderForm, code);
            }
        }

        private bool HasCcvMethodInOrderform(PopOrderForm popOrderForm)
        {
            return popOrderForm.OrderBillingMethods.Where(
                bm =>
                    bm is FidelityPointsBillingMethod || bm is VirtualGiftCheckBillingMethod ||
                    bm is GiftCardBillingMethod || bm is FidelityPointsOccazBillingMethod ||
                    bm is AvoirOccazBillingMethod).Any(bm => bm != null);
        }

        private void AddAdvatageCode(string code, PopOrderForm popOrderForm)
        {
            popOrderForm.OrderGlobalInformations.AdvantageCode = code;
        }

        private void AddCuu(string code, OPContext opContext, PopOrderForm popOrderForm)
        {
            
            switch (_voucherBusiness.AddVoucher(opContext, popOrderForm,popOrderForm,popOrderForm,popOrderForm,code))
            {
                case AddVoucherStatus.KOWithErase:
                case AddVoucherStatus.KOWithoutErase:
                case AddVoucherStatus.OK: 
                    SaveLastCuu(popOrderForm, string.Empty, false);
                    break;
                case AddVoucherStatus.KOKeep:
                    SaveLastCuu(popOrderForm, code, false);
                    break;
            }
        }
        
        private void SaveLastCuu(PopOrderForm popOrderForm, string value, bool addMode)
        {
            //LastCUUMemoryData cuu = popOrderForm.GetPrivateData<LastCUUMemoryData>("lastCUUMemory");
            var cuu = popOrderForm.GetPrivateData<LastCUUMemoryData>();
            cuu.Value = value;
            cuu.AddMode = addMode;
        }

        private bool HasCuu(PopOrderForm popOrderForm)
        {
            return popOrderForm.OrderBillingMethods.OfType<VoucherBillingMethod>().Any();
        }

        private bool CanAddAdvantageCode(PopOrderForm popOrderForm)
        {
            return !HasCcvMethodInOrderform(popOrderForm);
        }

        private bool CanAddCuu(PopOrderForm popOrderForm)
        {

            return popOrderForm.HasFnacComArticle() || popOrderForm.HasNumericalArticle() || popOrderForm.HasClickAndCollectArticle() && !HasCcvMethodInOrderform(popOrderForm);
        }

        public VoucherBusiness.CheckCode CheckVoucher(string cuu, PopOrderForm popOrderForm)
        {
            return _voucherBusiness.CheckVoucher(cuu, 1, popOrderForm, popOrderForm, popOrderForm, out var vi, _applicationContext.GetSiteContext());
        }

        public CodeType CodeType(string code)
        {
            return _cuuPatern.IsMatch(code.Trim()) ? Payment.CodeType.Cuu : Payment.CodeType.AdvantageCode;
        }
    }

    public enum CodeType
    {
        AdvantageCode,
        Cuu
    }
}
