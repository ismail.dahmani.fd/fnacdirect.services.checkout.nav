using FnacDirect.Solex.Shipping.Client.Contract;

namespace FnacDirect.OrderPipe.Base.BaseModel.Model.Quote
{
    public class Parcel
    {
        public int Id { get; set; }
        public int ShippingMethodId { get; set; }
        public int DelayInHours { get; set; }
        public Source Source { get; set; }

        public Parcel()
        {

        }

        public Parcel(int id, Solex.Shipping.Client.Contract.Parcel parcel)
        {
            Id = id;
            ShippingMethodId = parcel.ShippingMethodId;
            DelayInHours = parcel.DelayInHours;
            Source = parcel.Source;
        }
    }
}
