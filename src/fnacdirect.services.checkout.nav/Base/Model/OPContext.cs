using System;
using FnacDirect.Contracts.Online.Model;
using System.Globalization;
using System.Collections.Generic;

namespace FnacDirect.OrderPipe
{
    /// <summary>
    /// Contexte d'execution de l'OrderPipe
    /// Ce contexte est utilisé pour l'ensemble de chaque passe de l'orderpipe
    /// Il est initialisé par le contrôleur OPControler
    /// L'OPContextManager permet de le stocker dans l'environnement d'exécution
    /// </summary>
    public class OPContext
    {
        public OPContext()
        {

        }

        public OPContext(IOF orderForm)
        {
            OF = orderForm;
        }

        /// <summary>
        /// Identifiant de l'OrderForm
        /// </summary>
        public string SID { get; set; }

        /// <summary>
        /// Identifiant de l'OrderForm
        /// </summary>
        public string UID { get; set; }

        /// <summary>
        /// OrderForm
        /// L'OrderForm concret dérive de OF.
        /// </summary>
        public IOF OF { get; set; }

        /// <summary>
        /// Etape interactive courante
        /// </summary>
        public IInteractiveStep CurrentStep { get; set; }

        /// <summary>
        /// Instance du Pipe en cours d'exécution
        /// </summary>
        public IOP Pipe { get; set; }

        public bool IsDefaultPipe
        {
            get
            {
                return Pipe != null && Pipe.PipeName == "default";
            }
        }

        public bool IsInTransaction { get; set; }

        public bool ContinueMode { get; set; }

        public string ContinuingStep { get; set; }

        public string ForcedStep { get; set; }

        public SiteContext SiteContext { get; set; }

        public string ErrorStep { get; set; }

        public ErrorInfo ErrorInfo { get; set; }

        public bool ForceMode { get; set; }

        public string ForcedRetrievalChannel { get; set; }

        public List<string> ExecutedSteps { get; set; }

        public void Log(string s, params object[] prms)
        {
            if (Pipe != null && Pipe.DebugMode)
            {
                var escapedMessage = string.Format(
                    CultureInfo.InvariantCulture,
                    string.IsNullOrEmpty(s) ? string.Empty : s.Replace("{","{{").Replace("}","}}"),
                    prms);

                Logger.Log(
                    string.Format(
                        CultureInfo.InvariantCulture,
                        "{0}:{1}:{2} {3}",
                        Pipe.ConfigKey,
                        SID,
                        OF.CurrentStep,
                        escapedMessage
                    )
                );
            }
        }

        public event EventHandler<LogEventArgs> OnAddLogContextualData;

        public void AddLogContextualData(string label, object data)
        {
            if (OnAddLogContextualData != null)
            {
                var logEventArgs = new LogEventArgs
                {
                    Label = label,
                    Data = data
                };

                OnAddLogContextualData(this, logEventArgs);
            }
        }

        public virtual void AddWarning(string category, string messageId)
        {
            OF.UserMessages.Add(category, UserMessage.MessageLevel.Warning, messageId);
        }

        public void Reset()
        {
            Pipe.Reset(this);
        }
    }

    public class ErrorInfo
    {
        public Exception Exception { get; set; }
        public StepReturnError Error { get; set; }
        public IStep FailedStep { get; set; }
        public bool FailedInTransaction { get; set; }
    }

    public class LogEventArgs : EventArgs
    {
        public string Label { get; set; }
        public object Data { get; set; }
    }
}
