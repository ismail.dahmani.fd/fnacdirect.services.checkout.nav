using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.Technical.Framework.Web.Mvc;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FnacDirect.OrderPipe.Mvc
{
    public enum AlertMessageLevel
    {
        Disclaimer,
        Info,
        Warning,
        Danger
    }

    public class AlertMessage
    {
        private readonly string _title;
        private readonly IEnumerable<string> _content;
        private readonly AlertMessageLevel _level;

        public AlertMessage(AlertMessageLevel alertMessageLevel, string title, params string[] content)
        {
            _title = title;
            _content = content;
            _level = alertMessageLevel;
        }

        public string Title
        {
            get
            {
                return _title;
            }
        }

        public IEnumerable<string> Content
        {
            get
            {
                return _content;
            }
        }

        public AlertMessageLevel Level
        {
            get
            {
                return _level;
            }
        }
    }

    public class AlertMessageBuilder
    {
        private IApplicationContext _applicationContext;

        public AlertMessageBuilder()
        {
            _applicationContext = DependencyResolver.Current.GetService<IApplicationContext>();
        }

        public AlertMessage Build(AlertMessageLevel alertMessageLevel, string title, params string[] content)
        {
            return new AlertMessage(alertMessageLevel, title, content);
        }

        public AlertMessage Build(AlertMessageLevel alertMessageLevel, IHtmlString title, IHtmlString content)
        {
            return Build(alertMessageLevel, title.ToString(), content.ToString());
        }

        public AlertMessage Disclaimer(string title, params string[] content)
        {
            return Build(AlertMessageLevel.Disclaimer, title, content);
        }

        public AlertMessage Disclaimer(IHtmlString title, IHtmlString content)
        {
            return Build(AlertMessageLevel.Disclaimer, title, content);
        }

        public AlertMessage Info(string title, params string[] content)
        {
            return Build(AlertMessageLevel.Info, title, content);
        }

        public AlertMessage Info(IHtmlString title, IHtmlString content)
        {
            return Build(AlertMessageLevel.Info, title, content);
        }

        public AlertMessage Warning(string title, params string[] content)
        {
            return Build(AlertMessageLevel.Warning, title, content);
        }

        public AlertMessage Warning(IHtmlString title, IHtmlString content)
        {
            return Build(AlertMessageLevel.Warning, title, content);
        }

        public AlertMessage Danger(string title, string content)
        {
            return Build(AlertMessageLevel.Danger, title, content);
        }

        public AlertMessage Danger(IHtmlString title, IHtmlString content)
        {
            return Build(AlertMessageLevel.Danger, title, content);
        }

        public AlertMessage PipeMessage(PipeMessage pipeMessage)
        {
            var level = ToAlertMesssageLevel(pipeMessage.Level);
            var title = string.Empty;
            var msg = pipeMessage.Parameters.FirstOrDefault();
            var content = _applicationContext.TryGetMessage(pipeMessage.RessourceId, msg);

            return Build(level, title, content);
        }

        private AlertMessageLevel ToAlertMesssageLevel(PipeMessageLevel pipeMessageLevel)
        {
            if(pipeMessageLevel == PipeMessageLevel.Error)
            {
                return AlertMessageLevel.Danger;
            }

            if(pipeMessageLevel == PipeMessageLevel.Warning)
            {
                return AlertMessageLevel.Warning;
            }

            if(pipeMessageLevel == PipeMessageLevel.Info)
            {
                return AlertMessageLevel.Info;
            }

            return AlertMessageLevel.Disclaimer;
        }
    }
}
