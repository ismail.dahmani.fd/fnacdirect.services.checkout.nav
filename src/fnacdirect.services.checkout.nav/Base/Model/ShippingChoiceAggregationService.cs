using FnacDirect.OrderPipe.Base.Business.Configuration;
using FnacDirect.OrderPipe.Base.Model.Solex;
using System.Collections.Generic;
using System.Linq;

namespace FnacDirect.OrderPipe.Base.Business.Shipping
{
    public class ShippingChoiceAggregationService : IShippingChoiceAggregationService
    {
        private readonly IAggregatedMethodConfigurationProvider _aggregatedMethodConfigurationProvider;
        private readonly ISwitchProvider _switchProvider;

        public ShippingChoiceAggregationService(
            IAggregatedMethodConfigurationProvider aggregatedMethodConfigurationAccessor,
            ISwitchProvider switchProvider)
        {
            _aggregatedMethodConfigurationProvider = aggregatedMethodConfigurationAccessor;
            _switchProvider = switchProvider;
        }

        public List<ShippingChoice> AggregateShippingChoices(IEnumerable<ShippingChoice> choices)
        {
            var result = choices.ToList();

            foreach (var aggregateConfiguration in _aggregatedMethodConfigurationProvider.AggregatedMethodsConfiguration)
            {
                var switchId = aggregateConfiguration.Value.SwitchId;
                if (!string.IsNullOrEmpty(switchId) && !_switchProvider.IsEnabled(switchId))
                {
                    continue;
                }

                var choicesOnlyInGenericMethod = choices.Where(choice => aggregateConfiguration.Value.ExclusiveShippingMethods.Contains(choice.MainShippingMethodId));

                var choicesAlsoInGenericMethod = choices.Where(choice => aggregateConfiguration.Value.InclusiveShippingMethods.Contains(choice.MainShippingMethodId));

                AggregatedShippingChoice aggregatedShippingChoice = null;

                if (choicesOnlyInGenericMethod.Any() || choicesAlsoInGenericMethod.Any())
                {
                    aggregatedShippingChoice = new AggregatedShippingChoice(aggregateConfiguration.Key);

                    //if Exists OnlyInGenericShippingMethod then Move Choice to AggregatedChoice.ShippingChoices
                    if (choicesOnlyInGenericMethod.Any())
                    {
                        aggregatedShippingChoice.ShippingChoices.AddRange(choicesOnlyInGenericMethod);
                        var filteredChoices = result.Where(choice => choicesOnlyInGenericMethod.Any(c => c.MainShippingMethodId == choice.MainShippingMethodId)).ToList();

                        foreach (var choice in filteredChoices)
                        {
                            result.Remove(choice);
                        }
                    }
                    //if Exists AlsoInGenericShippingMethod then Copy Choice to AggregatedChoice.ShippingChoices
                    if (choicesAlsoInGenericMethod.Any())
                    {
                        var copiedChoices = choicesAlsoInGenericMethod.Select(sc => new ShippingChoice(sc));
                        aggregatedShippingChoice.ShippingChoices.AddRange(copiedChoices);
                    }

                    result.Add(aggregatedShippingChoice);
                }
            }

            return result;
        }
    }
}
