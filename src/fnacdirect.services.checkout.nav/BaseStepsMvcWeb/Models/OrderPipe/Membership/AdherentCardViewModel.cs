﻿namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Membership
{
    public enum CartType
    {
        Recruit,
        Renew
    }
    public class AdherentCardBaseViewModel
    {
            public CartType CardType { get; set; }

            public int CardId { get; set; }

            public decimal? SouscriptionCardPrice { get; set; }

            public decimal? RenewCardPrice { get; set; }
        
    }
}
