using FnacDirect.OrderPipe.Base.Model;
using System.Collections.Generic;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Common
{
    public interface IConfigurationViewModelBuilder
    {
        SortedDictionary<string, bool> Build(PopOrderForm popOrderForm);
    }
}
