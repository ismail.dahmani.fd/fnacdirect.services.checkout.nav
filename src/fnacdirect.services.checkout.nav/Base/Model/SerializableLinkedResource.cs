﻿using System.IO;
using System.Net.Mail;

namespace FnacDirect.OrderPipe.Base.Steps.Mailing
{
    public class SerializableLinkedResource
    {
        public byte[] Stream { get; set; }

        public string ContentId { get; set; }

        public string MediaType { get; set; }

        public SerializableLinkedResource() { }

        public SerializableLinkedResource(byte[] stream, string contentId, string mediaType)
        {
            Stream = stream;
            ContentId = contentId;
            MediaType = mediaType;
        }

        public LinkedResource ToLinkedResource()
        {
            var linkedResource = new LinkedResource(new MemoryStream(Stream), MediaType)
            {
                ContentId = ContentId
            };
            return linkedResource;
        }
    }
}
