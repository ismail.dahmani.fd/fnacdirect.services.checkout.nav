using System;
using System.Linq;
using FnacDirect.Customer.Model;
using FnacDirect.Ogone.Model;
using FnacDirect.OrderPipe.Base.BaseModel;
using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.OrderPipe.Base.Model.LineGroups;
using FnacDirect.OrderPipe.Base.Proxy.Ogone;
using FnacDirect.Technical.Framework.Web;
using FnacDirect.Technical.Framework.Web.Mvc;
using FnacDirect.Technical.Framework.Web.UrlMgr;
using System.Collections.Generic;
using FnacDirect.Context;
using FnacDirect.OrderPipe.Base.LineGroups;
using StructureMap;
using FnacDirect.OrderPipe.Base.Proxy.Ogone.PspId;
using FnacDirect.Ogone.BLL;
using FnacDirect.OrderPipe.Base.Business.Payment;
using FnacDirect.OrderPipe.Base.Model.Facets;

namespace FnacDirect.OrderPipe.Base.Business.Ogone
{
    /// <summary>
    /// Class contenant le specific business pour l'orderpipe
    /// </summary>
    public class OgoneBusiness : IOgoneBusiness
    {
        private readonly IExecutionContextProvider _executionContextProvider;
        private readonly IOgoneConfigurationService _ogoneConfigurationService;
        private readonly IOrderTransactionManager _orderTransactionManager;
        private readonly IApplicationContext _applicationContext;
        //remark: Pourquoi c'est au business de porter l'état de cette variable ? (Peut etre mieux que ca soit l'orderform)
        private OrderTransactionOgone ogoneAliasGatewayData = null;
        private readonly IUserContextProvider _userContextProvider;
        private readonly IPspIdServiceFactory _pspIdServiceFactory;
        private readonly ICreditCardTransactionBusiness _creditCardBusiness;
        private readonly ISwitchProvider _switchProvider;


        public OgoneBusiness(IExecutionContextProvider executionContextProvider,
                             IOgoneConfigurationService ogoneConfigurationService,
                             IOrderTransactionManager orderTransactionManager,
                             IApplicationContext applicationContext,
                             IUserContextProvider userContextProvider,
                             IContainer container,
                             IPspIdServiceFactory pspIdServiceFactory,
                             ICreditCardTransactionBusiness creditCardBusiness,
                             ISwitchProvider switchProvider)
        {
            _executionContextProvider = executionContextProvider;
            _ogoneConfigurationService = ogoneConfigurationService;
            _orderTransactionManager = orderTransactionManager;
            _applicationContext = applicationContext;
            _userContextProvider = userContextProvider;
            _pspIdServiceFactory = pspIdServiceFactory;
            _creditCardBusiness = creditCardBusiness;
            _switchProvider = switchProvider;
        }

        #region AliasGateway

        /// <summary>
        /// construits le sparamètres spécifiques au pipe + appel la construction des paramètres Ogone Alias Gateway de l'OgoneBusiness
        /// </summary>
        /// <param name="culture"></param>
        /// <param name="pipeName"></param>
        /// <param name="creditCardAlias"></param>
        /// <returns></returns>
        public SortedList<string, string> BuildOgoneAliasGatewayParameters(string pspId, string pipeName, string culture, string creditCardAlias = null)
        {
            // Init des paramètres invariant d'Alias Gateway au premier Build des paramètres.
            if (ogoneAliasGatewayData == null)
            {
                ogoneAliasGatewayData = new OrderTransactionOgone
                {
                    OrderID = Guid.NewGuid().ToString()
                };

                var executionContext = _executionContextProvider.GetCurrentExecutionContext();
                var queryStrings = executionContext.GetQueryString().ToList();

                Action<UrlWriter> addParameter = w =>
                {
                    foreach (var parameter in queryStrings)
                    {
                        if (parameter.Key.ToLowerInvariant() == "app")
                        {
                            w.WithParam(new UrlWriterParameter(parameter.Key, parameter.Value, UrlWriterParameterType.Standard));
                        }
                    }
                };

                var BaseOgoneReturnUrl = FnacContext.Current.UrlManagerNew.Sites.WWWDotNetSecure.GetPage("PopOgoneReturnAliasGateway").DefaultUrl;

                var acceptUrl = BaseOgoneReturnUrl.WithParam(new UrlWriterParameter("OgoneResultAliasGateway", OgoneResultEnum.ALIASGATEWAY_RESULT_ACCEPT, UrlWriterParameterType.Standard))
                    .WithParam(new UrlWriterParameter("pipe", pipeName, UrlWriterParameterType.Standard));
                addParameter(acceptUrl);
                ogoneAliasGatewayData.AcceptUrl = acceptUrl.ToString();

                var exceptionUrl = BaseOgoneReturnUrl.WithParam(new UrlWriterParameter("OgoneResultAliasGateway", OgoneResultEnum.ALIASGATEWAY_RESULT_EXCEPT, UrlWriterParameterType.Standard))
                    .WithParam(new UrlWriterParameter("pipe", pipeName, UrlWriterParameterType.Standard));
                addParameter(exceptionUrl);
                ogoneAliasGatewayData.ExceptionUrl = exceptionUrl.ToString();

                ogoneAliasGatewayData.PspId = pspId;
            }

            ogoneAliasGatewayData.Alias = creditCardAlias;

            return _orderTransactionManager.GetOgoneAliasGatewayParameters(ogoneAliasGatewayData, culture);
        }

        public string GetPspId(OrderTypeEnum orderTypeEnum, IEnumerable<Article> articles, OgoneConfigurationData ogoneConfigurationData, int? legalEntityId = null)
        {
            var pspIdService = _pspIdServiceFactory.GetInstance(_userContextProvider.GetCurrentUserContext().GetUserId().ToString(), legalEntityId);

            return pspIdService.ComputePspId(orderTypeEnum, articles, ogoneConfigurationData);
        }

        public string GetSHASignParameterValue(SortedList<string, string> ogoneUrlParameters)
        {
            return ogoneUrlParameters[OgoneToolManager.SHASIGN_PARAM_NAME.ToUpperInvariant()];
        }

        #endregion AliasGateway

        public void CompleteCurrentOri(OgoneCreditCardBillingMethod billingMethod, ILogisticLineGroup logisticLineGroup, OrderTransactionInfo currentOti, OrderTransactionRuleType? currentOrderTransactionRule, string culture)
        {
            if (currentOrderTransactionRule != null)
            {
                var brand = (billingMethod.TypeId != 11 ? billingMethod.Brand : billingMethod.TypeLabel.ToLowerInvariant());
                var autorisationRule = _ogoneConfigurationService.GetTransactionRule(culture, billingMethod.DebitType, brand, currentOrderTransactionRule.ToString());
                if (autorisationRule != null)
                {
                    // Si le montant de la billing est à 0€ => on fait une demande d'author à 1€.
                    currentOti.OrderTransaction.AutorisationAmount = logisticLineGroup.BillingMethodConsolidation[billingMethod.BillingMethodId] <= 0 ? AutorisationAmount.OneEuro : autorisationRule.AutorisationAmount;

                    currentOti.OrderTransaction.TransactionAmount = GetTransactionAmount(currentOti.OrderTransaction.AutorisationAmount, logisticLineGroup, billingMethod.BillingMethodId);
                }
            }
        }

        public decimal? GetTransactionAmount(AutorisationAmount? autorizationAmount, ILogisticLineGroup logisticLineGroup, int billingMethodId)
        {
            switch (autorizationAmount)
            {
                case AutorisationAmount.OneEuro:
                    return 1;
                case AutorisationAmount.ZeroEuro:
                    return 0;
                default:
                    return logisticLineGroup.BillingMethodConsolidation[billingMethodId];
            }
        }

        public void FillInCurrentOti(OgoneCreditCardBillingMethod billingMethod, DebitType debitType, OrderTransactionInfo currentOti, bool isSelectedForPsp)
        {
            currentOti.OrderTransaction.OrderTransactionOperation.OrderTransactionOperationStatus = TransactionOperationStatus.INITIALIZED;

            if (debitType == DebitType.DirectDebit)
            {
                currentOti.OrderTransaction.OrderTransactionStatus = TransactionStatus.Validating;
                currentOti.OrderTransaction.OrderTransactionOperation.OrderTransactionOperationType = TransactionOperationType.SAL;
            }
            else 
            {
                if (MustSetOrderTransactionStatusOnInitialized(billingMethod, isSelectedForPsp, currentOti.OrderTransaction.AutorisationAmount))
                {
                    currentOti.OrderTransaction.OrderTransactionStatus = TransactionStatus.Initialized;
                    currentOti.OrderTransaction.OrderTransactionOperation.OrderTransactionOperationType = TransactionOperationType.INIT;
                }
                else
                {
                    currentOti.OrderTransaction.OrderTransactionStatus = TransactionStatus.Reserving;
                    currentOti.OrderTransaction.OrderTransactionOperation.OrderTransactionOperationType = TransactionOperationType.RES;
                }
            }
        }

        private bool MustSetOrderTransactionStatusOnInitialized(OgoneCreditCardBillingMethod billingMethod, bool isSelectedForPsp, AutorisationAmount? authorAmount)
        {
            if (_switchProvider.IsEnabled("op.ogone.v2.enable", false) && authorAmount == AutorisationAmount.ZeroEuro)
                return false;

            if (_ogoneConfigurationService.GetPspMode() != PspMode.MultiPsp)
                return false;

            if (isSelectedForPsp)
                return false;

            if (!_creditCardBusiness.CanSetOrderTransactionStatusOnInitialized(billingMethod))
                return false;

            return true;
        }

        private bool IsDeferredDebit(DebitType debitType)
        {
            return debitType == DebitType.DeferredDebit;
        }

        public void SetBillingMethodAmount(AutorisationAmount? autorisationAmount, OgoneCreditCardBillingMethod billingMethod, ILogisticLineGroup hightAmountLineGroup)
        {
            switch (autorisationAmount)
            {
                case AutorisationAmount.OneEuro:
                    billingMethod.Amount = 1;
                    break;
                case AutorisationAmount.ZeroEuro:
                    billingMethod.Amount = 0;
                    break;
                case AutorisationAmount.TotalAmount:
                    billingMethod.Amount = hightAmountLineGroup.BillingMethodConsolidation[billingMethod.BillingMethodId];
                    break;
            }
        }

        public OrderTransactionOgone GetOgoneData(OrderTransactionInfoList orderTransactionInfos, IGotBillingInformations gotBillingInformations)
        {
            var orderGlobalInformations = gotBillingInformations?.OrderGlobalInformations;
            if (orderGlobalInformations == null)
            {
                return null;
            }

            FnacDirect.Ogone.Model.OrderTransaction selectedOrdertransaction;

            if (orderGlobalInformations.MainOrderTransactionInfos == null
                   || !orderGlobalInformations.MainOrderTransactionInfos.Any())
            {
                var selectedOrderTransactionInfo = orderTransactionInfos?.GetByBillingId(OgoneCreditCardBillingMethod.IdBillingMethod);
                selectedOrdertransaction = selectedOrderTransactionInfo?.OrderTransaction;
            }
            else
            {
                var mainOrderTransactionInfo = orderGlobalInformations.MainOrderTransactionInfos.FirstOrDefault();
                selectedOrdertransaction = mainOrderTransactionInfo?.OrderTransaction;
            }

            return selectedOrdertransaction?.OrderTransactionOgone;
        }

        public OrderTransactionOgone GetOgoneData(PopOrderForm popOrderForm)
        {
            var pspData = popOrderForm.GetPrivateData<PspData>(create: false);
            var orderTransactionInfos = pspData?.OrderTransactionInfos;
            return GetOgoneData(orderTransactionInfos, popOrderForm);
        }
    }
}
