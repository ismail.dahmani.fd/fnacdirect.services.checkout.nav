using System;
using System.Collections.ObjectModel;

namespace FnacDirect.OrderPipe.Config
{
	public class PipeList : KeyedCollection<string, Pipe>
	{
		protected override string GetKeyForItem(Pipe item)
		{
			if (item == null)
				throw new ArgumentNullException("item");

			return item.Name;
		}
	}
}