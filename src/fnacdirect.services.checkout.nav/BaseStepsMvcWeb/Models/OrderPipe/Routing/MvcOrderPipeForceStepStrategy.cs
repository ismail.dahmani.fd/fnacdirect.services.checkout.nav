﻿using System.Linq;
using System.Web.Routing;
using FnacDirect.OrderPipe.CoreRouting;
using FnacDirect.OrderPipe.Config;

namespace FnacDirect.OrderPipe.Core.Web.Routing
{
    public class MvcOrderPipeForceStepStrategy : OrderPipeForceStepStrategyBase<MvcPipeDescriptor>
    {
        public override string GetForceStepFor(IOP pipe, RequestContext requestContext)
        {
            if (!string.IsNullOrEmpty(requestContext.HttpContext.Request.QueryString["step"]))
            {
                return requestContext.HttpContext.Request.QueryString["step"];
            }

            if (requestContext.HttpContext.Request.HttpMethod != "GET")
            {
                return null;
            }

            var interactiveSteps = pipe.Steps.OfType<IInteractiveStep>()
                                             .Where(s => s.UIDescriptor is MvcUiDescriptor)
                                             .Select(s => new
                                             {
                                                 Step = s.Name,
                                                 RouteName = (s.UIDescriptor as MvcUiDescriptor).Route
                                             })
                                             .ToList();

            var matchingInteractiveStep = interactiveSteps.FirstOrDefault(s => s.RouteName == requestContext.RouteData.GetRouteName());

            if (matchingInteractiveStep != null)
            {
                return matchingInteractiveStep.Step;
            }

            return null;
        }
    }
}
