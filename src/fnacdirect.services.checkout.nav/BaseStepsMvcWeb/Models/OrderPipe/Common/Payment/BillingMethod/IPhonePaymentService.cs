using FnacDirect.OrderPipe.Base.Model;

namespace FnacDirect.OrderPipe.Base.Business.Payment
{
    public interface IPhonePaymentService
    {
        bool IsAllowed(OPContext opContext, PopOrderForm popOrderForm, bool disableECCVCheck = false);
    }
}
