namespace FnacDirect.OrderPipe.Base.Model
{
    public class CaixaData : GroupData
    {
        public string DN { get; set; }
        public string MemberNumber { get; set; }
    }
}
