using FnacDirect.OrderPipe.Base.Model;

namespace FnacDirect.OrderPipe.Base.Business
{
    public interface IArticleDetailsShopService
    {
        void GetArticleCodeSegment(StandardArticle prmoArticle);
        void GetArticleDetailsShopStocks(StandardArticle prmoArticle, string refUG);
    }
}
