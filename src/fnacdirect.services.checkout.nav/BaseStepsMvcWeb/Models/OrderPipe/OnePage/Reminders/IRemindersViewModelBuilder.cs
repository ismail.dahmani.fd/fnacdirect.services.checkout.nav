using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Common;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Pop.Models.Common
{
    public interface IRemindersViewModelBuilder
    {
        RemindersViewModel Build(PopOrderForm popOrderForm);
    }
}
