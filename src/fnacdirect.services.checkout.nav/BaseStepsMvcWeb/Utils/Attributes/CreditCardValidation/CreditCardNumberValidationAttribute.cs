﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using StructureMap;
using FnacDirect.Context;
using FnacDirect.Contracts.Online.Model;
using FnacDirect.Customer.Model;
using FnacDirect.OrderPipe.Base.Business;
using FnacDirect.OrderPipe.Base.Business.Payment;
using FnacDirect.OrderPipe.BaseMvc.Web.Services.Pop;
using FnacDirect.Technical.Framework.Web.Mvc;
using FnacDirect.OrderPipe.Config;
using FnacDirect.OrderPipe.Base.Model.Facets;
using FnacDirect.Technical.Framework.ServiceLocation;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Validations
{
    public class CreditCardNumberValidationAttribute : ValidationAttribute, IClientValidatable
    {
        private readonly string _propertyCreditCardType;

        private readonly IApplicationContext _applicationContext;
        private readonly IPaymentSelectionBusiness _paymentSelectionBusiness;
        private readonly SiteContext _siteContext;
        private readonly string _culture;
        private readonly string _configShippingMethodSelection;

        public CreditCardNumberValidationAttribute(string propertyCreditCardType)
        {
            _propertyCreditCardType = propertyCreditCardType;

            _applicationContext = ServiceLocator.Current.GetInstance<IApplicationContext>();
            _siteContext = _applicationContext.GetSiteContext();
            _culture = _siteContext != null ? _siteContext.Culture : "";
            _configShippingMethodSelection = _applicationContext.GetAppSetting("config.shippingMethodSelection");
            _paymentSelectionBusiness = ServiceLocator.Current.GetInstance<IPaymentSelectionBusiness>();
        }

        public IEnumerable<ModelClientValidationRule> GetClientValidationRules(ModelMetadata metadata, ControllerContext context)
        {
            var modelClientValidationRule = new ModelClientValidationRule
            {
                ErrorMessage = _paymentSelectionBusiness.GetErrorMessage("OP.CARD.ErrMsg.number.length"),
                ValidationType = "creditcardnumbervalidation"
            };

            modelClientValidationRule.ValidationParameters["propertycreditcardtype"] = "SelectedCreditCardId";


            yield return modelClientValidationRule;
        }

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            if (!(value is string))
                return new ValidationResult(_paymentSelectionBusiness.GetErrorMessage("OP.CARD.ErrMsg.number.digit"));

            var creditCardNumber = value.ToString().Replace("-", "");

            if (string.IsNullOrEmpty(creditCardNumber))
                return new ValidationResult(_paymentSelectionBusiness.GetErrorMessage("OP.CARD.ErrMsg.number.length"));

            var creditCardDescriptorBusiness = ServiceLocator.Current.GetInstance<ICreditCardDescriptorBusiness>();

            if(string.IsNullOrEmpty(_propertyCreditCardType))
            {
                return CheckFnacCardNumber(creditCardNumber);
            }

            var propertyInfo = validationContext.ObjectType.GetProperty(_propertyCreditCardType);

            if (propertyInfo == null)
                return ValidationResult.Success;

            var propertyValue = propertyInfo.GetGetMethod().Invoke(validationContext.ObjectInstance, null);

            if (!(propertyValue is int))
                return ValidationResult.Success;

            var creditCardTypeId = (int)propertyValue;

            var creditCardDescriptor = creditCardDescriptorBusiness.GetCreditCardDescriptor(creditCardTypeId, _configShippingMethodSelection, _culture);

            return CheckCreditCardNumber(creditCardTypeId, creditCardNumber, creditCardDescriptor);
        }

        private ValidationResult CheckCreditCardNumber(int creditCardTypeId, string creditCardNumber, CustCreditCardDescriptor creditCardDescriptor)
        {            
            var validationResult = _paymentSelectionBusiness.CheckSipsCreditCardNumber(creditCardNumber, creditCardDescriptor.NumberLength, creditCardTypeId, creditCardDescriptor.Prefix);

            return validationResult;
        }

        private ValidationResult CheckFnacCardNumber(string creditCardNumber)
        {
            var maintenanceConfigurationService = ServiceLocator.Current.GetInstance<IMaintenanceConfigurationService>();

            var validationResult = _paymentSelectionBusiness.CheckFnacCardNumber(creditCardNumber, maintenanceConfigurationService.CleLuhn);

            return validationResult;
        }
    }
}
