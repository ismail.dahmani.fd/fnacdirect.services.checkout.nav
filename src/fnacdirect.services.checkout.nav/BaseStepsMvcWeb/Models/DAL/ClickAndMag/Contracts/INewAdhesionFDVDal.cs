using FnacDirect.OrderPipe.CM.Model;
using System;

namespace FnacDirect.OrderPipe.Base.DAL
{
    public interface INewAdhesionFDVDal
    {
        void AddOrderInformation(int adhesionId, int purchaseOrder, string purchaseOrderUserReference);
        int CreateAdhesion(int accountId, int articleID, int statut, int purchaseOrder, string purchaseOrderUserReference);
        void AddStatutInformation(int adhesionId, int status);
        void GetCustomerInfosFromPlasticCardNumber(string cardNumber,  CreateAccountData cad);
        void GetCustomerInfosFromAdherentNumber(string adhNumber, DateTime? birthDate, CreateAccountData cad);
        void AddMembershipAccountLight(
            string adherentID,
            string adherentNumber,
            int intAdherentGender,
            string strAdherentFirstname,
            string strAdherentLastname,
            DateTime dteAdherentBirthDate,
            string strAdherentAddress,
            string strAdherentZipCode,
            string strAdherentCity,
            string strAdherentCountryIso3,
            string strAdherentEmail,
            int intAdherentStopPubEmail,
            int intAdhesionType,
            string strAdhesionProgramme,
            int intContractType,
            int intAdhesionDuration,
            DateTime dteAdherentStartDate,
            DateTime dteAdherentEndDate,
            string strAdherentStatus,
            string strOrigine,
            string strRegGU
            );
    }
}
