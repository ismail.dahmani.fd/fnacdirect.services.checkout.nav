using FnacDirect.KeyGenerator;
using FnacDirect.OrderPipe.Base.LineGroups;
using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.OrderPipe.Base.Model.OrderInformations;
using FnacDirect.OrderPipe.Base.Model.UserInformations;
using System.Collections.Generic;

namespace FnacDirect.OrderPipe.Base.Business.MainOrderTransactionInfo
{
    public interface IKeyGeneratorBusiness
    {
        MultiKey GenerateKeyGen(IOrderGlobalInformations orderGlobalInformations, IUserContextInformations userContextInformations, IEnumerable<ILogisticLineGroup> logisticLineGroups, IEnumerable<BillingMethod> orderBillingMethods, BillingMethod mainBillingMethod, int retryCount, int timeout);
    }
}
