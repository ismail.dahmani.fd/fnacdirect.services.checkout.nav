using FnacDirect.OrderPipe.Base.LineGroups;
using FnacDirect.OrderPipe.Base.Model.Facets;
using System.Collections.Generic;

namespace FnacDirect.OrderPipe.Base.Business.Billing
{
    public interface ISplitBillingMethodBusiness
    {
        void SplitBillingMethodByLineGroup(IGotBillingInformations iGotBillingInformations, IEnumerable<ILogisticLineGroup> logisticLineGroups);
    }
}
