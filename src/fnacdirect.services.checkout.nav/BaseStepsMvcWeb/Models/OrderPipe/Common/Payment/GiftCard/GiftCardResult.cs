using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Common.Payment
{
    public class GiftCardResult
    {
        public bool IsValid { get; set; }

        public List<string> ErrorMessages { get; set; }

        public bool AmountGreaterThanMaxCardAmount { get; set; }

        public bool AmountGreaterThanOrderAmount { get; set; }

        public bool ErrorAmount { get; set; }
        public bool GreaterThanMaximumAllowedAmount { get; set; }
    }
}
