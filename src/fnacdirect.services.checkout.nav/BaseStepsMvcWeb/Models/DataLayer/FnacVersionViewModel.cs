using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.DataLayer
{
    public class FnacVersionViewModel
    {
        public string Version { get; set; }
        public string BuildDefinitionName { get; set; }
        public string LastChangeset { get; set; }
        public string PackageName { get; set; }
        public string BuildDate { get; set; }
        public string RepositoryType { get; set; }
        public string RepositoryUrl { get; set; }
        public List<WorkspaceMaping> WorkspaceMapings { get; set; }
    }
    public class WorkspaceMaping
    {
        public string ServerPath { get; set; }
        public string LocalPath { get; set; }
    }
}
