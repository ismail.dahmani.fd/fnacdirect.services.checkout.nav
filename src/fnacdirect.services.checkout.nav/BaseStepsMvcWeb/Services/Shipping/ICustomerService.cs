﻿using System;
using System.Collections.Generic;
using FnacDirect.Contracts.Online.Model;
using FnacDirect.Customer.AdherentBLL;
using FnacDirect.Customer.IModel;
using FnacDirect.Customer.Model;
using FnacDirect.Front.WebBusiness.NewCustomer;

namespace FnacDirect.OrderPipe.Base.Proxy.Customer
{
    public interface ICustomerService
    {
        CustomerEntity Load();
        CustomerEntity Load(int userId, Guid userReference);
        CustomerEntity GetCurrentCustomer();
        void SaveCustomerFavoriteStore(string favoriteStoreId);
        void SaveCustomerPreferences(NewCustomerEntity nCustomerEntity, List<MailAttributeEntity> preference);
        void SetCustomerCellPhone(int identity, string cellPhone);
        bool GetCustomerByAdherentCardNum(NewCustomerEntity customer , out Adherent.CheckLoginResult status , bool withAddress);
        OperationStatus? Update(CustomerEntity customerEntity);        
        OperationStatus? Add(CustomerEntity customerEntity, string culture);
        List<Gender> GetGenders(string culture);
        bool CheckIfValidAdherent(CustomerEntity customer, out Adherent.CheckLoginResult statuts);
        Dictionary<string, string> GetAllCompanyTypes();
        Dictionary<string, string> GetAllCompanySizes();
        Dictionary<string, string> GetAllBusinessSectors();
        Dictionary<string, string> GetAllJobTitles();
    }
}
