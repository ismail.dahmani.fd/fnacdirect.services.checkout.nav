using FnacDirect.Front.Meteor.Business.Models;
using FnacDirect.OrderPipe.Base.BaseModel;
using FnacDirect.OrderPipe.Base.LineGroups;
using FnacDirect.OrderPipe.Base.Model.FDV.LineGroups;
using FnacDirect.OrderPipe.Shipping.Model;
using FnacDirect.Solex.Shipping.Client.Contract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Serialization;

namespace FnacDirect.OrderPipe.Base.Model
{
    public class LineGroup : IShippingLineGroup
    {
        private readonly ArticleList _unfoldedArticles = new ArticleList();

        public LineGroupInfo OrderInfo { get; set; }

        [XmlIgnore]
        public WrapList AvailableWraps { get; set; }

        [XmlIgnore]
        private readonly IDictionary<int, decimal> _billingMethodConsolidation = new Dictionary<int, decimal>();

        public decimal RemainingTotal { get; set; }

        [XmlIgnore]
        public XmlOrderForm XmlOrder { get; set; }

        [XmlIgnore]
        public ShippingMethodResult ShippingMethodChoice { get; set; }

        public int? GlobalShippingMethodChoice { get; set; }

        public bool HasShippingMethod(ShippingMethodEvaluation shippingMethodEvaluation, IEnumerable<int> shippingMethodIds)
        {
            var shippingMethodEvaluationGroup = shippingMethodEvaluation.Seller(Seller.SellerId);
            if (shippingMethodEvaluationGroup.HasValue)
            {
                var shippingMethodEvaluationItem = shippingMethodEvaluationGroup.Value.ShippingMethodEvaluationItems
                    .FirstOrDefault(s => s.ShippingMethodEvaluationType == shippingMethodEvaluationGroup.Value.SelectedShippingMethodEvaluationType);

                if (shippingMethodEvaluationItem != null && shippingMethodEvaluationItem.HasChoices)
                {
                    var selectedChoice = shippingMethodEvaluationItem.Choices
                        .FirstOrDefault(c => c.MainShippingMethodId == shippingMethodEvaluationItem.SelectedShippingMethodId);

                    return selectedChoice != null && shippingMethodIds.Contains(selectedChoice.MainShippingMethodId);
                }
            }

            return false;
        }

        public BaseModel.Model.Quote.Choice Choice { get; set; }

        public string InvoiceReference { get; set; }


        public ShippingAddress ShippingAddress { get; set; }


        [XmlIgnore]
        public BillingAddress BillingAddress { get; set; }

        [XmlIgnore]
        public decimal Volume { get; set; }

        public LineGroup()
        {
            OrderInfo = new LineGroupInfo();
            Articles = new ArticleList();
            Seller = new SellerInformation();
            ShippingAddress = null;
            BillingAddress = null;
            IsVoluminous = false;
            LogisticTypes = new LogisticTypeList();
            GlobalPrices = new GlobalPrices();
            UniqueId = Guid.NewGuid().ToString();
            AvailableWraps = new WrapList();
            Choice = new BaseModel.Model.Quote.Choice();
        }

        [XmlIgnore]
        public bool IsVoluminous { get; set; }

        public List<ShippingModeValue> ShippingModeValueCollection { get; set; }

        /// <summary>
        /// Pour les LineGroup de type Fnac.com ou FnacShop, détermine si au moins un des articles présents a un service attaché.
        /// Renvoie false pour les LineGroup autres
        /// </summary>
        public bool ContainsServices
        {
            get
            {
                if (HasFnacComArticle || HasShopArticle)
                {
                    foreach (StandardArticle art in Articles)
                    {
                        if (art.Services != null && art.Services.Count > 0)
                        {
                            return true;
                        }
                    }
                }
                return false;
            }
        }

        /// <summary>
        /// Pour savoir si l'utilisateur a sélectionné la méthode de livraison manuellement
        /// Utiliser seulement dans le pipe legacy
        /// </summary>
        public bool GlobalShippingMethodChoiceIsSetManually { get; set; }

        /// <summary>
        /// Identifiant unique permettant d'identifier le linegroup
        /// </summary>
        public string UniqueId { get; set; }

        public virtual bool IsTemporary
        {
            get
            {
                return false;
            }
        }

        [XmlIgnore]
        public VatCountry VatCountry { get; set; }

        [XmlIgnore]
        public GlobalPrices GlobalPrices { get; set; }

        public SellerInformation Seller { get; set; }

        public ArticleList Articles { get; set; }

        public OrderTypeEnum OrderType { get; set; }

        [XmlIgnore]
        public int OrderOrigin { get; set; }

        public LogisticTypeList LogisticTypes { get; set; }

        [XmlIgnore]
        public ArticleList UnfoldedArticles
        {
            get { return _unfoldedArticles; }
        }

        [XmlIgnore]
        public bool HasMarketPlaceArticle
        {
            get
            {
                return (OrderType & OrderTypeEnum.MarketPlace) == OrderTypeEnum.MarketPlace;
            }
        }

        [XmlIgnore]
        public bool HasFnacComArticle
        {
            get
            {
                return (OrderType & OrderTypeEnum.FnacCom) == OrderTypeEnum.FnacCom
                    || (OrderType & OrderTypeEnum.Pro) == OrderTypeEnum.Pro;
            }
        }

        [XmlIgnore]
        public IEnumerable<Article> CollectInStoreArticles
        {
            get
            {
                return Articles.OfType<StandardArticle>().Where(a => a.CollectInShop);
            }
        }

        [XmlIgnore]
        public bool HasCollectInStoreArticles
        {
            get
            {
                return CollectInStoreArticles.Any();
            }
        }

        [XmlIgnore]
        public bool HasShopArticle
        {
            get
            {
                return (OrderType & OrderTypeEnum.IntraMag) == OrderTypeEnum.IntraMag || (OrderType & OrderTypeEnum.IntraMagPE) == OrderTypeEnum.IntraMagPE;
            }
        }

        [XmlIgnore]
        public bool HasIntraMagPE
        {
            get
            {
                return (OrderType & OrderTypeEnum.IntraMagPE) == OrderTypeEnum.IntraMagPE;
            }
        }

        [XmlIgnore]
        public bool HasNumericalArticles
        {
            get
            {
                return (OrderType & OrderTypeEnum.Numerical) == OrderTypeEnum.Numerical;
            }
        }

        [XmlIgnore]
        public bool HasDematSoftArticles
        {
            get
            {
                return (OrderType & OrderTypeEnum.DematSoft) == OrderTypeEnum.DematSoft;
            }
        }

        [XmlIgnore]
        public bool IsPro
        {
            get
            {
                return (OrderType & OrderTypeEnum.Pro) == OrderTypeEnum.Pro;
            }
        }

        [XmlIgnore]
        public bool IsMixed
        {
            get
            {
                return (HasMarketPlaceArticle && HasFnacComArticle);
            }
        }

        [XmlIgnore]
        ILineGroupInformations ILineGroupHeader.Informations
        {
            get
            {
                return OrderInfo;
            }
        }

        [XmlIgnore]
        public IDictionary<int, decimal> BillingMethodConsolidation
        {
            get
            {
                return _billingMethodConsolidation;
            }
        }

        public void RemoveArticlesById(IEnumerable<int?> articlesIdToRemove)
        {
            Articles.RemoveAll(a => articlesIdToRemove.Contains(a.ProductID));
        }
    }
}
