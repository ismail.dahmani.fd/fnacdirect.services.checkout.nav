using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.OrderPipe.Base.Model.Solex;
using FnacDirect.OrderPipe.Shipping.Model;
using System;
using System.Collections.Generic;

namespace FnacDirect.OrderPipe.Base.LineGroups
{
    public interface ILogisticLineGroup : ILineGroupHeader
    {
        List<Article> Articles { get; }

        int? OrderPk { get; set; }
        string OrderUserReference { get; set; }
        Guid OrderReference { get; set; }
        string GUPTReference { get; set; }
        int GUPTTransactionId { get; set; }
        DateTime OrderDate { get; set; }

        bool IsFnacCom { get; }
        bool IsMarketPlace { get; }
        bool IsClickAndCollect { get; }
        bool IsIntraMag { get; }
        bool IsIntramagPE { get; }
        bool IsNumerical { get; }
        bool IsSteedFromStore { get; }

        ShippingAddress ShippingAddress { get; }

        ShippingMethodResult ShippingMethodChoice { get; }

        ShippingChoice SelectedShippingChoice { get; }
    }
}
