﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Routing;

namespace FnacDirect.OrderPipe.CoreRouting.Rules
{
    public interface IRoutingRule
    {
        /// <summary>
        /// Apply routing rule to current request
        /// </summary>
        /// <param name="requestContext">Current request context</param>
        /// Returns an handler is rule match
        IHttpHandler Apply(RequestContext requestContext);
    }
}
