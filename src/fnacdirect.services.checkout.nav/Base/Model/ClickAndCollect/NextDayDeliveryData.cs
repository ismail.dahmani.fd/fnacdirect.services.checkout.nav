﻿namespace FnacDirect.OrderPipe.Base.Model.ClickAndCollect
{
    public class NextDayDeliveryData : Data
    {
        public bool IsAvailable { get; set; }
    }
}
