﻿using System.Collections.Generic;

namespace FnacDirect.OrderPipe
{
    /// <summary>
    /// Interface de stockage du contexte OPContext
    /// </summary>
    public interface IOPContextManager
    {
        Dictionary<string, OPContext> OpContextList { get; set; }
    }
}
