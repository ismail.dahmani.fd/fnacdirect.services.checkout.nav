using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.OrderPipe.Base.Model.Facets;
using FnacDirect.OrderPipe.Core.Services;
using FnacDirect.Technical.Framework.Utils;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;

namespace FnacDirect.OrderPipe.Base.Business.Payment
{
    public class OgonePaymentService : IOgonePaymentService
    {
        private readonly ISwitchProvider _switchProvider;
        private readonly IPaymentEligibilityService _paymentEligibilityService;

        private readonly RangeSet<int> _creditCardBillingMethodAllowed;
        private readonly Regex _customerAllowedForMultiLogistic;
        private readonly Regex _customerAllowedForMonoLogistic;
        private readonly Regex _customerAllowedForMarketPlace;
        private readonly Regex _customerAllowedForMonoProduct;
        private readonly Regex _customerAllowedForMultiProduct;
        private readonly Regex _customerAllowedForDemat;
        private readonly Regex _customerAllowedForClicAndCollect;
        private readonly Regex _customerAllowedForDematSoft;
        private readonly Regex _customerAllowedForEbook;


        public OgonePaymentService(ISwitchProvider switchProvider,
            IPipeParametersProvider pipeParametersProvider,
            IPaymentEligibilityService paymentEligibilityService)
        {
            _switchProvider = switchProvider;
            _paymentEligibilityService = paymentEligibilityService;

            _creditCardBillingMethodAllowed = RangeSet.Parse<int>(pipeParametersProvider.Get("creditcardbillingmethod.allowed", "0"));
            _customerAllowedForClicAndCollect = new Regex(pipeParametersProvider.Get("ogone.customer.allowed.clicandcollect.regularexpression", @"[\D]"));
            _customerAllowedForMultiLogistic = new Regex(pipeParametersProvider.Get("ogone.customer.allowed.multilogistic.regularexpression", @"[\D]"));
            _customerAllowedForMonoLogistic = new Regex(pipeParametersProvider.Get("ogone.customer.allowed.monologistic.regularexpression", @"[\D]"));
            _customerAllowedForMarketPlace = new Regex(pipeParametersProvider.Get("ogone.customer.allowed.marketplace.regularexpression", @"[\D]"));
            _customerAllowedForMonoProduct = new Regex(pipeParametersProvider.Get("ogone.customer.allowed.monoproduct.regularexpression", @"[\D]"));
            _customerAllowedForMultiProduct = new Regex(pipeParametersProvider.Get("ogone.customer.allowed.multiproduct.regularexpression", @"[\D]"));
            _customerAllowedForDemat = new Regex(pipeParametersProvider.Get("ogone.customer.allowed.demat.regularexpression", @"[\D]"));
            _customerAllowedForDematSoft = new Regex(pipeParametersProvider.Get("ogone.customer.allowed.dematsoft.regularexpression", @"[\D]"));
            _customerAllowedForEbook = new Regex(pipeParametersProvider.Get("ogone.customer.allowed.ebook.regularexpression", @"[\D]"));
        }

        public bool IsAllowed(IGotLineGroups iContainsLineGroups, IGotBillingInformations iContainsBillingInformations, IGotUserContextInformations iContainsUserContextInformations)
        {
            var rules = _paymentEligibilityService.GetRules("orderpipe.pop.payment.ogone.eligibilityrules");

            if (!_paymentEligibilityService.IsAllowed(iContainsLineGroups, rules))
            {
                return false;
            }

            var accountId = iContainsUserContextInformations.UserContextInformations.AccountId.ToString(CultureInfo.InvariantCulture);
            var enableOgoneCreditCard = _creditCardBillingMethodAllowed.Contains(OgoneCreditCardBillingMethod.IdBillingMethod) && iContainsLineGroups.LineGroups != null;

            if (!enableOgoneCreditCard || !_switchProvider.IsEnabled("op.ogone.checkeligibility"))
            {
                return enableOgoneCreditCard;
            }

            if (iContainsUserContextInformations.UserContextInformations.IdentityImpersonator != null && _switchProvider.IsEnabled("op.ogone.disablefromcallcenter"))
            {
                return false;
            }

            if (iContainsLineGroups.HasDematSoftArticle())
            {
                enableOgoneCreditCard &= _customerAllowedForDematSoft.IsMatch(accountId);
            }

            if (iContainsLineGroups.HasClickAndCollectArticle())
            {
                enableOgoneCreditCard &= _customerAllowedForClicAndCollect.IsMatch(accountId);
            }

            if (iContainsLineGroups.LineGroups.Count() != 1)
            {
                enableOgoneCreditCard &= _customerAllowedForMultiLogistic.IsMatch(accountId);
            }
            else
            {
                enableOgoneCreditCard &= _customerAllowedForMonoLogistic.IsMatch(accountId);

                var firstLineGroup = iContainsLineGroups.LineGroups.First();
                if (enableOgoneCreditCard && firstLineGroup.Articles.Count == 1)
                {
                    enableOgoneCreditCard &= _customerAllowedForMonoProduct.IsMatch(accountId);
                }
                else
                {
                    enableOgoneCreditCard &= _customerAllowedForMultiProduct.IsMatch(accountId);
                }

                if (enableOgoneCreditCard && iContainsLineGroups.HasMarketPlaceArticle())
                {
                    enableOgoneCreditCard &= _customerAllowedForMarketPlace.IsMatch(accountId);
                }
            }

            if (enableOgoneCreditCard && iContainsLineGroups.HasNumericalArticle())
            {
                if (iContainsLineGroups.LineGroups.GetArticles().OfType<StandardArticle>().Any(a => a.IsEbook))
                {
                    enableOgoneCreditCard &= _customerAllowedForEbook.IsMatch(accountId);
                }
                else
                {
                    enableOgoneCreditCard &= _customerAllowedForDemat.IsMatch(accountId);
                }

            }

            return enableOgoneCreditCard;
        }
    }
}
