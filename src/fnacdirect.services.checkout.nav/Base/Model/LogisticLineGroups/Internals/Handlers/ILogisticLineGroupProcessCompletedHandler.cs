﻿using FnacDirect.OrderPipe.Base.LineGroups;
using FnacDirect.OrderPipe.Base.Model.Facets;
using System.Collections.Generic;

namespace FnacDirect.OrderPipe.Base.Business.LogisticLineGroups.Internals.Handlers
{
    public interface ILogisticLineGroupProcessCompletedHandler
    {
        void OnGenerated(ILogisticLineGroupGenerationProcessContext logisticLineGroupGenerationProcessContext, IEnumerable<ILogisticLineGroup> logisticLineGroups);
    }
}
