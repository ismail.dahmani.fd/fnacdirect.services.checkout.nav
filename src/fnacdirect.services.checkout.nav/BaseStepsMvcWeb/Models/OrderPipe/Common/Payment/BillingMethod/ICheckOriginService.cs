using FnacDirect.OrderPipe.Base.Model.Facets;

namespace FnacDirect.OrderPipe.Base.Business.Payment
{
    public interface ICheckOriginService
    {
        bool IsFromFnacPro();
        bool IsFromFnacProAndCallCenter(IGotUserContextInformations gotUserContextInformations);
        bool IsFromCallCenter(IGotUserContextInformations gotUserContextInformations);
        bool IsIphoneOrIpad(IGotMobileInformations mobileInformations);
    }
}
