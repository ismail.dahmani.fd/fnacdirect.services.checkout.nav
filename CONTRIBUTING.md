# Responsabilité

L'équipe DF/fnacproduit est responsable de ce projet.

# Contributions

Les contributions sont bienvenues. Elles doivent néanmoins se conformer aux règles décrites dans ce document.
N'hésitez pas à vous rapprocher de l'équipe responsable pour vous assurer de la pertinence de la modification que vous proposez.

# Workflow

* Le développement est réalisé dans une feature branch issue de ``master`` nommée ``feature/xxxx``.
* Il est soumis à l'aide d'une Pull Request de ``feature/xxxx`` vers ``master``.
* Pour être validée, la pull request est revue par 2 mainteneurs de l'équipe Architecture. Un build réussi est nécessaire.
* La pull request est mergée sur ``master`` en un squash commit unique, la branche source est supprimée.
* Un tag est posé pour fixer la version sur ``master``, les nugets *release* sont construits et publiés sur proget.

# Versioning

La numérotation de version suit le système [semver 2.0](https://semver.org/lang/fr/).

# Readme

Le fichier README.md est le premier point d'entrée documentaire du projet. 

Il permet de répondre aux questions essentielles suivantes :
* A quoi sert la librairie ?
* Comment s'en sert-on ?
* Comment fonctionne-t-elle ?
* Comment l'intègre-t-on ?

Le fichier README peut bien sûr être complété par d'autres documentations, dans le repo du projet ou sur une page dédiée sur Confluence.
Dans ce dernier cas, il faut ajouter des liens croisés entre le repo du projet et la documentation.

# Changelog

Une changelog synthétique est maintenue dans le fichier CHANGELOG.md, au format [Keep a Changelog](https://keepachangelog.com/en/1.0.0/).
La section _Unreleased_ doit être complétée au fur et à mesure des développements de la future version.

# Remonté d'anomalie, suivi des tickets

Les tickets liés à ce projet sont suivis sur le [projet Jira PCD](https://jira.darty.fr/browse/PCD).