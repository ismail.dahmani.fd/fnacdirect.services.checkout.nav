using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Common.Payment
{
    public class FidelityEurosViewModel : BillingMethodViewModel
    {
        public FidelityEurosViewModel()
        {
            Values = Enumerable.Empty<SelectListItem>();
        }

        public bool HasExpiration { get; set; }

        public bool Active { get; set; }

        public string Used { get; set; }
        
        public string Remaining { get; set; }        

        public string Amount { get; set; }
        public string FormattedAmount { get; set; }
        public bool HasUniqueExpiration { get; set; }

        public bool ExpireInLessThanOneMonth { get; set; }

        public decimal? MinAmount { get; set; }

        public decimal? MaxAmount { get; set; }

        public string AmountToExpire { get; set; }

        public string Expiration { get; set; }
        public string MaxUsableAmount { get; set; }

        public IEnumerable<SelectListItem> Values { get; set; }
        public decimal? Step { get; set; }

        public bool HasMaximumAllowedAmountForClickAndCollect { get; set; } = false;
        public decimal? MaximumAllowedAmountForClickAndCollect { get; set; }
    }
}
