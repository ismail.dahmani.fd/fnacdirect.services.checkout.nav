using System.Net;
using static FnacDirect.OrderPipe.Constants.Error;

namespace FnacDirect.OrderPipe.Base.Model
{
    public class ErrorData : GroupData
    {
        public ErrorType ErrorType { get; set; }

        public int ErrorCode { get; set; }

        public HttpStatusCode HttpStatusCode { get; set; }

        public string ErrorMessage { get; set; }
    }
}
