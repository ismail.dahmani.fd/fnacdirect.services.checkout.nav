﻿namespace FnacDirect.OrderPipe.Base.Model
{
    public enum DiscountReason
    {
        OpcPricer = 1,
        Quote = 2,
        CallCenter = 3
    }
}
