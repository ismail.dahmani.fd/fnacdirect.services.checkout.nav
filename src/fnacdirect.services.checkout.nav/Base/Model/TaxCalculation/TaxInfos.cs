namespace FnacDirect.OrderPipe.Base.Model.TaxCalculation
{
    public class TaxInfos
    {
        public int TaxType { get; set; }
        public decimal TaxRate { get; set; }
        public int CountryId { get; set; }
        public decimal CountryTaxRate { get; set; }
        public string CodeGu { get; set; }
        public bool UseTax { get; set; }    
    }
}
