using System;
using FnacDirect.Technical.Framework.Web.Mvc;
using System.Collections.Generic;
using System.Linq;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Basket
{
    public class PriceViewModel
    {
        /// <summary>
        /// Article gratuit ? prix en rouge
        /// </summary>
        public bool Free { get; set; }

        /// <summary>
        /// Article numérique (dematsoft et ebook) pour prix en bleu
        /// </summary>
        public bool Numerical { get; set; }

        /// <summary>
        /// Prix barré
        /// </summary>
        public string OldPrice { get; set; }

        public bool ShouldDisplayOldPrice
        {
            get { return (OldPrice != null && Price != OldPrice); }
        }

        public PriceDetailsViewModel OldPriceModel { get; set; }

        /// <summary>
        /// Prix HT barré
        /// </summary>
        public string OldPriceNoVat { get; set; }

        public PriceDetailsViewModel OldPriceNoVatModel { get; set; }

        /// <summary>
        /// Prix à afficher en fonction de l'environnement (FnacCom ou FnacPro)
        /// </summary>
        public PriceDetailsViewModel OldPriceModelToDisplay { get; set; }

        /// <summary>
        /// Prix - potentiellement avec les remises appliquées
        /// </summary>
        public string Price { get; set; }

        public PriceDetailsViewModel PriceModel { get; set; }

        /// <summary>
        /// Prix HT - potentiellement avec les remises appliquées
        /// </summary>
        public string PriceNoVat { get; set; }

        public PriceDetailsViewModel PriceNoVatModel { get; set; }

        /// <summary>
        /// Prix à afficher en fonction de l'environnement (FnacCom ou FnacPro)
        /// </summary>
        public PriceDetailsViewModel PriceModelToDisplay { get; set; }

        /// <summary>
        /// Prix mensuel
        /// </summary>
        public string MonthlyPrice { get; set; }

        /// <summary>
        /// Prix mensuel HT
        /// </summary>
        public string MonthlyPriceNoVAT { get; set; }

        /// <summary>
        /// Prix mensuel à afficher en fonction de l'environnement (FnacCom ou FnacPro)
        /// </summary>
        public PriceDetailsViewModel MonthlyPriceModelToDisplay { get; set; }

        /// <summary>
        /// Texte pré construit contenant eco tax et rcp
        /// </summary>
        public string EcoTaxAndCopyTaxLabel { get; set; }

        /// <summary>
        /// Texte pré construit contenant eco tax en HT et rcp 
        /// </summary>
        public string EcoTaxNoVatAndCopyTaxLabel { get; set; }

        /// <summary>
        /// RCP
        /// </summary>
        public bool HasRCP { get; set; }

        /// <summary>
        /// Eco taxe
        /// </summary>
        public bool HasEcoTax { get; set; }

        /// <summary>
        /// Eco-participation DEEE (dechets d'equipements electriques et electroniques) 
        /// </summary>
        public PriceDetailsViewModel DeeePrice { get; set; }

        /// <summary>
        /// Eco-participation DEA (dechets d'equipements d'ameublement) 
        /// </summary>
        public PriceDetailsViewModel DeaPrice { get; set; }

        public PriceDetailsViewModel RCPPrice { get; set; }

        private readonly List<PromotionTemplateViewModel> _templates
            = new List<PromotionTemplateViewModel>();

        public IReadOnlyCollection<PromotionTemplateViewModel> Templates
        {
            get
            {
                return _templates;
            }
        }

        public void WithTemplate(PromotionTemplateViewModel template)
        {
            if(_templates.All(t => string.Compare(t.Label, template.Label, StringComparison.OrdinalIgnoreCase) != 0))
                _templates.Add(template);
        }

        private readonly List<PromotionViewModel> _usages
            = new List<PromotionViewModel>();

        public IReadOnlyCollection<PromotionViewModel> Usages
        {
            get
            {
                return _usages;
            }
        }

        public void WithUsage(PromotionViewModel usage)
        {
            _usages.Add(usage);
        }

        private readonly List<string> _promotions
            = new List<string>();

        public IReadOnlyCollection<string> Promotions
        {
            get
            {
                return _promotions;
            }
        }

        public void WithPromotion(string promotion)
        {
            _promotions.Add(promotion);
        }
    }
}
