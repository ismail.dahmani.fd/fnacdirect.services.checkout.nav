using System;
using FnacDirect.Customer.BLL;
using FnacDirect.Customer.Model;

namespace FnacDirect.OrderPipe.Base.Business
{
    public class BlacklistBusiness
    {
        public void BlacklistCustomer(int accountId)
        {
            try
            {
                var attributeBusiness = new AttributBusiness();
                var attributeEntity = new AttributEntity
                {
                    AttributeId = 12,
                    CustomerId = accountId,
                    AttributeValue = "1"
                };

                attributeBusiness.Add(attributeEntity);
            }
            catch (Exception exception)
            {
                throw BaseBusinessException.BuildException("Blacklist customer failed", exception, BusinessRange.Purchase, 4, "ACCOUNTID", accountId);
            }
        }
    }
}
