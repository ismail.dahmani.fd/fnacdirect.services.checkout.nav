using System;
using FnacDirect.OrderPipe.Base.Proxy;

namespace FnacDirect.OrderPipe.Base.Business
{
    public class CustomerContextProvider : ICustomerContextProvider
    {
        private readonly Func<ICustomerContext> _factory;

        public CustomerContextProvider(Func<ICustomerContext> factory)
        {
            _factory = factory;
        }

        public ICustomerContext GetCurrentCustomerContext()
        {
            return _factory();
        }
    }
}
