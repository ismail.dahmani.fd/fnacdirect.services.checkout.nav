using FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Common.Payment;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Pop.Models.SelfCheckout
{
    public class SelfCheckoutCryptoViewModel
    {
        public CustomerCreditCardViewModel CustomerCreditCardViewModel { get; set; }
        public OgoneAliasGatewayViewModel OgoneAliasGatewayViewModel { get; set; }
    }
}
