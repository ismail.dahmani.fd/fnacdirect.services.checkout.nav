using System.Xml.Serialization;

namespace FnacDirect.OrderPipe.Base.Model
{
    public enum ArticleType
    {
        /// <summary>
        /// 1
        /// </summary>
        [XmlEnum("1")]
        Saleable = 1,

        /// <summary>
        /// 2
        /// </summary>
        [XmlEnum("2")]
        Free = 2
    }
}
