using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.OrderPipe.Base.Proxy;
using FnacDirect.ProxyEAI.Model;
using FnacDirect.OrderPipe.Base.LineGroups;
using FnacDirect.OrderPipe.Base.Model.UserInformations;
using System.Collections.Generic;
using System.Linq;

namespace FnacDirect.OrderPipe.Base.Business
{
    public abstract class GuptBusiness
    {
        private readonly IClickAndMagService _clickAndMagService;

        protected GuptBusiness(IClickAndMagService clickMagService)
        {
            _clickAndMagService = clickMagService;
        }

        protected virtual ShopCustomer BuildCustomer(IUserContextInformations userContextInformations,
            string contactEmail,
            BillingAddress billingAddress,
            bool containsNewAdh)
        {
            var cust = new ShopCustomer();

            if (userContextInformations.IsAdherent || containsNewAdh)
            {
                BuildCustomerAdhDatas(cust, userContextInformations);
            }

            if (billingAddress != null)
            {
                cust.FirstName = billingAddress.Firstname;
                cust.LastName = billingAddress.Lastname;
                cust.PhoneNumber = billingAddress.Phone;
                cust.EMail = contactEmail;
                cust.SocialReason = billingAddress.Company;
                cust.Address = billingAddress.AddressLine1;
                cust.ZipCode = billingAddress.ZipCode;
                cust.City = billingAddress.City;
                cust.CountryCode = billingAddress.CountryCode2;
                cust.CellPhone = billingAddress.CellPhone;
                cust.Civilite = GetGenderType(billingAddress.GenderId);
            }
            return cust;
        }

        protected void BuildCustomerAdhDatas(ShopCustomer customer, IUserContextInformations userContextInformations)
        {
            customer.AdherentNumber = userContextInformations.ClientCard;
            if (string.IsNullOrWhiteSpace(userContextInformations.PlasticCardNumber))
            {
                if (userContextInformations.CustomerEntity != null)
                    customer.AdherentPlasticCard = userContextInformations.CustomerEntity.AdherentCardNumber;
            }
            else
            {
                customer.AdherentPlasticCard = userContextInformations.PlasticCardNumber;
            }
        }

        protected int GetGenderType(int pGenderId)
        {
            switch (pGenderId)
            {
                case 2:
                    return 1;
                case 3:
                    return 2;
                case 4:
                    return 3;
                default:
                    return 4;
            }
        }

        protected abstract ShopTvRoyalty BuildTvRoyalty(ILogisticLineGroup lg);
        protected abstract ShopOrder BuildOrder(ILogisticLineGroup lg,
            FnacStore.Model.FnacStore store,
            IEnumerable<BillingMethod> billingMethods,
            IUserContextInformations userInformations,
            bool containsAdhThreeYears);

        protected int GetOrderCredit(IEnumerable<BillingMethod> billingMethods)
        {
            return billingMethods.OfType<CreditCardBillingMethod>().Any(ccbm => ccbm.BaremeCredit != null) ? 1 : 0;
        }

        protected string GetClientID(string pServiceID)
        {
            return _clickAndMagService.GetClientId(pServiceID);
        }
    }
}
