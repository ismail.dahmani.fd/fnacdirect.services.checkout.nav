using System.Collections.Generic;
using System.Linq;

namespace FnacDirect.OrderPipe.Base.BaseModel.Model
{
    public class RelayDummyData : GroupData
    {
        public List<RelayPrices> PriceByLogisticType { get; set; }

        public int DistanceToNearestStore { get; set; }

        public decimal MinPriceTotal
        {
            get
            {
                if (PriceByLogisticType == null)
                    return 0.0m;

                return PriceByLogisticType.Sum(p => p.MinPrice);
            }
        }

        public decimal MaxPriceTotal
        {
            get
            {
                if (PriceByLogisticType == null)
                    return 0.0m;

                return PriceByLogisticType.Sum(p => p.MaxPrice);
            }
        }
    }

    public class RelayPrices
    {
        public int LogisticType { get; set; }

        public decimal MinPrice { get; set; }

        public decimal MaxPrice { get; set; }
    }
}
