
using System.Xml.Serialization;

namespace FnacDirect.OrderPipe.Base.Model
{
    public class GuestData : GroupData
    {
        public ShippingAddress ShippingAddress { get; set; }
        public BillingAddress BillingAddress { get; set; }
        public string Email { get; set; }
        public ShippingMethodEnum ShippingMethod { get; set; }
        public string Reference { get; set; }
        public string GuestOrderTrackingUrl { get; set; }
    }
}

