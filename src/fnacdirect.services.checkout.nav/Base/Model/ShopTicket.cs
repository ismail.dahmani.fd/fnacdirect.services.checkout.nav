using System;
using System.Collections.Generic;

namespace FnacDirect.OrderPipe.Base.Model.SelfCheckout
{
    public class ShopTicket
    {
        public string ShopName { get; set; }
        public string ShopTel { get; set; }
        public string NumCaisse { get; set; }
        public string NumTicket { get; set; }
        public string NumCaissier { get; set; }
        public string RefUg { get; set; }
        public DateTime DateTicket { get; set; }

        public List<ShopTicketOrder> Orders { get; set; }
        public List<ShopTicketVATLine> VATLines { get; set; }
        public ShopTicketTotal Total { get; set; }
        public ShopTicketCustomerData Customer { get; set; }
    }

    public class ShopTicketOrder
    {
        public string OrderReference { get; set; }
        public string Seller { get; set; }
        public string GuptReference { get; set; }
        public decimal SubTotalAmount { get; set; }
        public decimal ShippingCost { get; set; }
        public decimal WrappingCost { get; set; }
        public int NbArticles { get; set; }

        public bool IsIntramagPE { get; set; }
        public bool IsFnacCom { get; set; }
        public bool IsMP { get; set; }

        public List<ShopTicketArticle> Articles { get; set; }

        public decimal RemiseAmount { get; set; }
    }

    public class ShopTicketVATLine
    {
        public decimal? MtHT { get; set; }
        public decimal? MtTTC { get; set; }
        public decimal? MtTVA { get; set; }
        public decimal? TvaTsa { get; set; }
    }

    public class ShopTicketArticle
    {
        public string EAN { get; set; }
        public string Title { get; set; }
        public decimal TotalAmount { get; set; }
        public string Type { get; set; }
        public int Quantity { get; set; }
        public decimal UnitAmount { get; set; }
        public decimal RemiseAmount { get; set; }
        public string RemiseCodeMotif { get; set; }
        public string RemiseCodeType { get; set; }
        public List<ShopTicketArticle> Components { get; set; }
    }

    public class ShopTicketTotal
    {
        public int NbArticles { get; set; }
        public List<ShopTicketPaymentData> PaymentDatas { get; set; }
        public decimal TotalAmount { get; set; }
    }

    public class ShopTicketPaymentData
    {
        public decimal PaymentAmount { get; set; }
        public string PaymentMethodName { get; set; }
    }

    public class ShopTicketCustomerData
    {
        public string AdherentCardNumber { get; set; }
        public string AdherentNumber { get; set; }
        public string AdherentProgramType { get; set; }
        public string Company { get; set; }
        public string FirstName { get; set; }
        public bool IsAdherent { get; set; }
        public string LastName { get; set; }
        public string ZipCode { get; set; }
    }
}
