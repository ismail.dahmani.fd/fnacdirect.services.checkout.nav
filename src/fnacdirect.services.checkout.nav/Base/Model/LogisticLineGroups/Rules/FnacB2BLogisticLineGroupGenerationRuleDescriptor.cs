﻿using FnacDirect.OrderPipe.Base.Business.LogisticLineGroups.Initializers;

namespace FnacDirect.OrderPipe.Base.Business.LogisticLineGroups.Rules
{
    public class FnacB2BLogisticLineGroupGenerationRuleDescriptor
         : BaseFnacLogisticLineGroupGenerationRuleDescriptor<FnacB2BLogisticLineGroupInitializer>
    {
        public override string SwitchId
        {
            get
            {
                return "orderpipe.logisticlinegroupgenerationrule.fnacb2b";
            }
        }
    }
}
