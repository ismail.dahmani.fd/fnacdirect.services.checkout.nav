﻿using FnacDirect.OrderPipe.Config;

namespace FnacDirect.OrderPipe.CoreRouting
{
    public interface IOrderPipeInteractiveStepHandlerStrategyLocator
    {
        IOrderPipeInteractiveStepHandlerStrategy GetOrderPipeInteractiveStepHandlerStrategyFor<TDescriptor>(TDescriptor descriptor)
            where TDescriptor : class, IUiDescriptor;
    }
}
