﻿using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.OrderPipe.Base.Model.Pop;
using System;
using System.Web.Mvc;

namespace FnacDirect.OrderPipe.BaseMvc.Web.HtmlHelpers.Pop.AbTest
{
    public static class ABTestHelper
    {
        public static bool IsExperience(this HtmlHelper htmlHelper, string experienceName)
        {
            var opContext = DependencyResolver.Current.GetService<OPContext>();
            if (opContext != null)
            {
                if (opContext.OF is PopOrderForm popOf)
                {
                    var privateData = popOf.GetPrivateData<PopData>(false);
                    if (privateData != null)
                    {
                        return string.Compare(privateData.PaymentABTest, experienceName, StringComparison.InvariantCultureIgnoreCase) == 0;
                    }
                }
            }

            return false;
        }
    }
}
