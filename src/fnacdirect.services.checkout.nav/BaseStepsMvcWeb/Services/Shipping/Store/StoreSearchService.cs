using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;

using System.Text;
using System.Threading.Tasks;
using FnacDirect.OrderPipe.Base.Proxy.Store;
using FnacDirect.Technical.Framework;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Services.ShipToStore
{
    public class StoreSearchService : IStoreSearchService
    {
        private const int DefaultSearchRadius = 100;

        private readonly IFnacStoreService _fnacStoreService;

        public StoreSearchService(IFnacStoreService fnacStoreService)
        {
            _fnacStoreService = fnacStoreService;
        }

        public FnacStore.Model.FnacStore GetStore(int storeId)
        {
            return _fnacStoreService.GetStore(storeId);
        }

        public IEnumerable<StoreSearchGeoResult> GetStoreNearStore(int storeId)
        {
            return GetStoreNearStore(GetStore(storeId));
        }

        public IEnumerable<StoreSearchGeoResult> GetStoreNearStore(FnacStore.Model.FnacStore fnacStore)
        {
            return GetNearestStores(fnacStore);
        }

        public IEnumerable<StoreSearchGeoResult> GetStoreByGeoLocation(double latitude, double longitude)
        {
            return GetNearestStores(latitude, longitude);
        }

        public IEnumerable<StoreSearchGeoResult> GetStoreByGeoLocationandName(double latitude, double longitude,string city)
        {
            return GetNearestStoresName(latitude, longitude,city);
        }

        private IEnumerable<StoreSearchGeoResult> GetNearestStores(FnacStore.Model.FnacStore fnacStore)
        {
            if (fnacStore != null)
                return GetNearestStores(fnacStore.GeoLocation);
            else
                return new List<StoreSearchGeoResult>();
        }

        private IEnumerable<StoreSearchGeoResult> GetNearestStores(double latitude, double longitude)
        {
            return GetNearestStores(new Position(latitude, longitude));
        }

        private IEnumerable<StoreSearchGeoResult> GetNearestStoresName(double latitude, double longitude,string city)
        {
            return GetNearestStoresName(new Position(latitude, longitude),city);
        }

        private IEnumerable<StoreSearchGeoResult> GetNearestStores(Position searchGeoPoint)
        {
            var fnacStores = _fnacStoreService.GetAllStoresWithDeliveryOrClickAndCollect().ToList();

            return (from fnacStore in fnacStores
                    where !string.IsNullOrEmpty(fnacStore.GeoLocalizationAddress)
                    let distanceFromGivenPoint = searchGeoPoint.GetDistanceFrom(fnacStore.GeoLocation)
                    where distanceFromGivenPoint <= DefaultSearchRadius
                    select new StoreSearchGeoResult(fnacStore, distanceFromGivenPoint))
                   .OrderBy(r => r.DistanceFromSearch)
                   .ToList();
        }

        private IEnumerable<StoreSearchGeoResult> GetNearestStoresName(Position searchGeoPoint, string city)
        {

           
            var fnacStores = _fnacStoreService.GetAllStoresWithDeliveryOrClickAndCollect().ToList();

            return (from fnacStore in fnacStores
                    where !string.IsNullOrEmpty(fnacStore.GeoLocalizationAddress)
                    let distanceFromGivenPoint = searchGeoPoint.GetDistanceFrom(fnacStore.GeoLocation)
                    let Name = fnacStore.FullName ?? string.Empty
                    where (distanceFromGivenPoint <= DefaultSearchRadius
                            || Name.IndexOf(city,StringComparison.InvariantCultureIgnoreCase)>=0)
                    select new StoreSearchGeoResult(fnacStore, distanceFromGivenPoint))
                   .OrderBy(r => r.DistanceFromSearch)
                   .ToList();
        }
    }
}
