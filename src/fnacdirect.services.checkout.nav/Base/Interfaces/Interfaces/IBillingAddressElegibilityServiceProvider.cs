﻿namespace FnacDirect.OrderPipe.Base.Business.Billing
{
    public interface IBillingAddressElegibilityServiceProvider
    {
        IBillingAddressElegibilityService GetBillingAddressElegibilityServiceFor(OPContext opContext);
    }
}