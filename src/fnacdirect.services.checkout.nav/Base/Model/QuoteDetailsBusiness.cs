using Common.Logging;
using FnacDirect.Contracts.Online.Model;
using FnacDirect.Customer.BLL;
using FnacDirect.OrderPipe.Base.BaseModel.Model.Quote;
using FnacDirect.OrderPipe.Base.Business.Helpers;
using FnacDirect.OrderPipe.Base.DAL;
using FnacDirect.OrderPipe.Base.Model;
using SolexModel = FnacDirect.OrderPipe.Base.Model.Solex;
using FnacDirect.OrderPipe.Base.Model.LineGroups;
using FnacDirect.Technical.Framework.Web.Mvc;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Xml.Linq;
using System.Xml.Serialization;

namespace FnacDirect.OrderPipe.Base.Business.Quote
{
    // Enum des raisons récupérée depuis l'application Quote
    public enum DiscountRaison
    {
        OPC = 0,
        QuotePrice = 1,
        QuotePort = 2,
        QuoteGlobalPort = 3,
        QuoteGlobalPrice = 4,
        OPCGlobalPrice = 5,
        OPCGlobalPort = 6,
        OPCGlobalSplit = 7,
        OPCGlobalWrap = 8
    }

    public class QuoteDetailsBusiness : IQuoteDetailsBusiness
    {
        private readonly ILog _log;
        private readonly IApplicationContext _applicationContext;
        private readonly IAddressBookService _addressBookService;
        private readonly IPostalAddressService _postalAddressService;
        private readonly IArticleDetailService _articleDetailService;
        private readonly IQuoteDetailsDal _quoteDetailsDal;

        public QuoteDetailsBusiness(IApplicationContext applicationContext, ILog log, IQuoteDetailsDal quoteDetailsDal,
            IAddressBookService addressBookService, IPostalAddressService postalAddressService,
            IArticleDetailService articleDetailService)
        {
            _applicationContext = applicationContext;
            _log = log;
            _quoteDetailsDal = quoteDetailsDal;
            _addressBookService = addressBookService;
            _postalAddressService = postalAddressService;
            _articleDetailService = articleDetailService;
        }

        public PopOrderForm GetQuoteDetailBySID(string quoteSid, PopOrderForm popOrderForm)
        {
            try
            {
                var quoteInfos = _quoteDetailsDal.GetQuoteDetailBySid(quoteSid, true);
                if (quoteInfos == null)
                {
                    return null;
                }

                var accountId = quoteInfos.AccountId;
                popOrderForm.QuoteInfos = quoteInfos;
                var document = XDocument.Parse(quoteInfos.QuoteXmlContent);

                popOrderForm.LineGroups.Clear();

                var linegroupsXML = document.Root.Elements("LineGroups");
                if (!linegroupsXML.Any())
                {
                    return null;
                }

                popOrderForm.OrderType = OrderTypeEnum.Pro;

                foreach (var linegroupXML in linegroupsXML)
                {
                    var lg = linegroupXML.Element("LineGroup");
                    if (lg == null)
                    {
                        return null;
                    }

                    var selectedShippingMethodId = int.Parse(GetElementToParse(lg.Element("GlobalShippingMethodChoice")));
                    var shippingModeValues = lg.Elements("ShippingModeValueCollection")?.Elements("ShippingModeValue");

                    var choice = SetChoiceGroups(selectedShippingMethodId, shippingModeValues);

                    var logisticTypes = new LogisticTypeList();
                    foreach (var logisticType in lg.Elements("LogisticTypes")?.Elements("LogisticType"))
                    {
                        logisticTypes.Add(new Model.LogisticType()
                        {
                            Index = int.Parse(GetElementToParse(logisticType.Element("Index"))),
                            SplitLevel = int.Parse(GetElementToParse(logisticType.Element("SplitLevel"))),
                            InBasket = 1
                        });
                    }

                    const int StatusCommandeAPayer = 41;

                    var lineGroup = new PopLineGroup()
                    {
                        Articles = new ArticleList(),
                        OrderType = OrderTypeEnum.Pro,
                        LogisticTypes = logisticTypes,
                        UniqueId = lg.Element("UniqueId")?.Value,
                        Seller = new SellerInformation
                        {
                            SellerId = int.Parse(GetElementToParse(lg.Element("Seller")?.Element("SellerId")))
                        },
                        Informations = new LineGroups.PopLineGroupInformations()
                        {
                            OrderStatusId = StatusCommandeAPayer,
                            OrderType = (int)OrderInfoOrderTypeEnum.StandardPro,
                            ChosenWrapMethod = int.Parse(GetElementToParse(lg.Element("OrderInfo")?.Element("ChosenWrapMethod"))),
                            OrderMessage = lg.Element("OrderInfo").Element("OrderMessage")?.Value
                        }
                    };

                    var totalWrapNoVat = decimal.Parse(GetElementToParse(document.Root.Element("WrapCost")), CultureInfo.InvariantCulture);

                    var articles = lg.Elements("Articles")?.Elements("Article");

                    var parcelId = 1;

                    //why not loop on quoteArticle ?
                    foreach (var articleItem in articles)
                    {
                        var quoteArticles = document.Root.Element("QuoteLineDetailCollection")
                                                      .Elements("QuoteLineDetail");

                        var articleItemCatalog = articleItem.Element("Referentiel")?.Value;
                        var articleItemProductId = articleItem.Element("ProductID")?.Value;

                        if (articleItemCatalog == null || articleItemProductId == null)
                        {
                            return null;
                        }

                        var quoteArticle = quoteArticles.FirstOrDefault(qa => qa.Element("Referentiel")?.Value == articleItemCatalog
                                                                           && qa.Element("ProductID")?.Value == articleItemProductId);

                        var standardArticle = GetArticleDetails(quoteArticle);

                        var quoteShippingModeValue = quoteArticle.Element("QuoteShippingModeValue");

                        parcelId = AddingStandardArticleToGroup(choice, quoteShippingModeValue, standardArticle, parcelId);

                        foreach (var service in standardArticle.Services)
                        {
                            parcelId = AddingServiceToGroup(choice, service.LogType, service, parcelId);
                        }

                        if (lineGroup.Informations.ChosenWrapMethod != 0 && !lineGroup.AvailableWraps.Contains(lineGroup.Informations.ChosenWrapMethod))
                        {
                            lineGroup.AvailableWraps.Add(new Model.Wrap()
                            {
                                WrapId = lineGroup.Informations.ChosenWrapMethod,
                                TotalNoVATPrice = totalWrapNoVat,
                            });
                        }

                        lineGroup.Articles.Add(standardArticle);

                        //Récupération des remises QuotePort et QuotePrice
                        var quoteLineRemises = articleItem.Element("Remises")?.Elements("QuoteLineRemise");
                        if (quoteLineRemises != null)
                        {
                            foreach (var quoteLineRemise in quoteLineRemises)
                            {
                                var priceDiscount = decimal.Parse(GetElementToParse(quoteLineRemise.Element("PriceDiscount")), CultureInfo.InvariantCulture);
                                var discountReason = int.Parse(GetElementToParse(quoteLineRemise.Element("DiscountReason")));

                                if (priceDiscount > 0)
                                {
                                    var remise = new Remise();
                                    // Remise Quote sur le frais de livraison
                                    if (discountReason == (int)DiscountRaison.QuotePort)
                                    {
                                        // Le DiscountReason est "Quote" si on est dans le contexte Quote
                                        remise.DiscountReason = (int)DiscountReason.Quote;
                                        remise.DiscountType = (int)Model.DiscountType.ShippingPrice;
                                        remise.PriceDiscount = priceDiscount;
                                        popOrderForm.QuoteInfos.Remises.Add(standardArticle.ProductID.GetValueOrDefault(), remise);
                                    }

                                    // Remise Quote sur le prix de l'article
                                    if (discountReason == (int)DiscountRaison.QuotePrice)
                                    {
                                        // Le DiscountReason est "Quote" si on est dans le contexte Quote
                                        discountReason = (int)DiscountReason.Quote;
                                        remise.DiscountType = (int)Model.DiscountType.Article;
                                        remise.PriceDiscount = priceDiscount;
                                        popOrderForm.QuoteInfos.Remises.Add(standardArticle.ProductID.GetValueOrDefault(), remise);
                                    }
                                }
                            }
                        }
                    }

                    popOrderForm.LineGroups.Add(lineGroup);

                    //Récupération de l'adresse et de la méthode de livraison.
                    GetShippingInformation(popOrderForm, document, lineGroup, choice);
                }

                //Récupération des remises Globales
                var globalRemises = document.Element("GlobalRemises")?.Elements("QuoteLineRemise");
                if (globalRemises != null)
                {
                    foreach (var globalRemise in globalRemises)
                    {
                        var priceDiscount = decimal.Parse(GetElementToParse(globalRemise.Element("PriceDiscount")));
                        var quoteAddressId = int.Parse(GetElementToParse(globalRemise.Element("QuoteAddressId")));

                        // on accepte les remises à la hausse
                        if (priceDiscount != 0)
                        {
                            var remise = new Remise();

                            // frais de livraison de type Quote
                            if (remise.DiscountReason == (int)DiscountRaison.QuoteGlobalPort)
                            {
                                remise.PriceDiscount = priceDiscount;
                                remise.DiscountReason = (int)DiscountReason.Quote;
                                remise.DiscountType = (int)Model.DiscountType.ShippingPrice;
                                popOrderForm.QuoteInfos.GlobalRemises.Add(quoteAddressId, remise);
                            }

                            if (remise.DiscountReason == (int)DiscountRaison.OPCGlobalPrice)
                            {
                                // contexte OPC
                                remise.DiscountReason = (int)DiscountReason.OpcPricer;
                                remise.DiscountType = (int)Model.DiscountType.Article;
                                popOrderForm.QuoteInfos.GlobalRemises.Add(quoteAddressId, remise);
                            }

                            if (remise.DiscountReason == (int)DiscountRaison.OPCGlobalPort)
                            {
                                // contexte OPC
                                remise.DiscountReason = (int)DiscountReason.OpcPricer;
                                remise.DiscountType = (int)Model.DiscountType.ShippingPrice;
                                popOrderForm.QuoteInfos.GlobalRemises.Add(quoteAddressId, remise);
                            }

                            if (remise.DiscountReason == (int)DiscountRaison.OPCGlobalSplit)
                            {
                                // contexte OPC
                                remise.DiscountReason = (int)DiscountReason.OpcPricer;
                                remise.DiscountType = (int)Model.DiscountType.SplitPrice;
                                popOrderForm.QuoteInfos.GlobalRemises.Add(quoteAddressId, remise);
                            }

                            if (remise.DiscountReason == (int)DiscountRaison.OPCGlobalWrap)
                            {
                                // contexte OPC
                                remise.DiscountReason = (int)DiscountReason.OpcPricer;
                                remise.DiscountType = (int)Model.DiscountType.WrapPrice;
                                popOrderForm.QuoteInfos.GlobalRemises.Add(quoteAddressId, remise);
                            }
                        }
                    }
                }

                //Récupération des informations sur les taux de TVA.
                GetVATRates(popOrderForm, document);

                //Récupération de la partie BillingAddress.
                GetBillingAddress(popOrderForm, accountId, document);

                popOrderForm.BillingMethodConsolidation = new Dictionary<int, decimal>();
            }
            catch (Exception exception)
            {
                _log.Error("Error while loading quote details", exception);
            }

            return popOrderForm;
        }

        /// <summary>
        /// Il s'agit d'un copier-collé de la méthode SetGroupsAndParcelForArticle existant dans OrderWithPendingPaymentBusiness. Ce code étant jetable la refacto n'est pas nécessaire.
        /// </summary>
        protected internal int AddingStandardArticleToGroup(SolexModel.ShippingChoice choice, XElement quoteShippingModeValue, StandardArticle standardArticle, int parcelId)
        {
            var logisticType = int.Parse(GetElementToParse(quoteShippingModeValue.Element("LogisticType")));

            var group = choice.Groups.FirstOrDefault(g => g.Id == logisticType);

            var item = new SolexModel.ItemInGroup
            {
                Identifier = new SolexModel.Identifier
                {
                    Prid = standardArticle.ProductID
                },
                Quantity = standardArticle.Quantity.GetValueOrDefault(),
                UnitPrice = new GenericPrice { Cost = standardArticle.ShipPriceUserEur.GetValueOrDefault() },
                ParcelId = parcelId
            };

            var existingParcel = choice.Parcels.FirstOrDefault(p => p.Value.ShippingMethodId == standardArticle.ShipMethod
                                                                    && p.Value.Source.ShippingDate == standardArticle.TheoreticalExpeditionDate
                                                                    && p.Value.Source.DeliveryDateMin == standardArticle.TheoreticalDeliveryDate);

            if (existingParcel.Value != null)
            {
                parcelId = existingParcel.Key;
            }
            else
            {
                var parcel = new SolexModel.Parcel
                {
                    ShippingMethodId = standardArticle.ShipMethod.GetValueOrDefault(),
                    DelayInHours = standardArticle.ShipDelay.GetValueOrDefault(),
                    Source = new SolexModel.Source
                    {
                        ShippingDate = standardArticle.TheoreticalExpeditionDate.GetValueOrDefault(),
                        DeliveryDateMax = standardArticle.TheoreticalDeliveryDateMax.GetValueOrDefault(),
                        DeliveryDateMin = standardArticle.TheoreticalDeliveryDate.GetValueOrDefault()
                    }
                };

                choice.Parcels.Add(parcelId, parcel);
                // Que se passera-t-il si parcelId + 1 existe déjà ?
                parcelId++;
            }

            var items = group.Items.ToList();
            items.Add(item);
            group.Items = items;

            return parcelId;
        }

        protected internal int AddingServiceToGroup(SolexModel.ShippingChoice choice, int? logisticType, Service service, int parcelId)
        {
            if (!logisticType.HasValue)
            {
                return parcelId;
            }

            var group = choice.Groups.FirstOrDefault(g => g.Id == logisticType);

            if (group == null)
            {
                return parcelId;
            }

            var item = new SolexModel.ItemInGroup
            {
                Identifier = new SolexModel.Identifier
                {
                    Prid = service.ProductID
                },
                Quantity = service.Quantity.GetValueOrDefault(),
                UnitPrice = new GenericPrice { Cost = service.ShipPriceUserEur.GetValueOrDefault() },
                ParcelId = parcelId
            };

            var existingParcel = choice.Parcels.FirstOrDefault(p => p.Value.ShippingMethodId == service.ShipMethod
                                                                    && p.Value.Source.ShippingDate == service.TheoreticalExpeditionDate
                                                                    && p.Value.Source.DeliveryDateMin == service.TheoreticalDeliveryDate);

            if (existingParcel.Value != null)
            {
                parcelId = existingParcel.Key;
            }
            else
            {
                var parcel = new SolexModel.Parcel
                {
                    ShippingMethodId = service.ShipMethod.GetValueOrDefault(),
                    DelayInHours = service.ShipDelay.GetValueOrDefault(),
                    Source = new SolexModel.Source
                    {
                        ShippingDate = service.TheoreticalExpeditionDate.GetValueOrDefault(),
                        DeliveryDateMax = service.TheoreticalDeliveryDateMax.GetValueOrDefault(),
                        DeliveryDateMin = service.TheoreticalDeliveryDate.GetValueOrDefault()
                    }
                };

                choice.Parcels.Add(parcelId, parcel);
                // Que se passera-t-il si parcelId + 1 existe déjà ?
                parcelId++;
            }

            var items = group.Items.ToList();
            items.Add(item);
            group.Items = items;

            return parcelId;
        }

        protected internal SolexModel.ShippingChoice SetChoiceGroups(int selectedShippingMethodId, IEnumerable<XElement> shippingModeValues)
        {
            var groups = new List<SolexModel.Group>();

            foreach (var shippingModeValue in shippingModeValues)
            {
                var id = int.Parse(GetElementToParse(shippingModeValue.Element("LogisticType")));
                var globalPrice = decimal.Parse(GetElementToParse(shippingModeValue.Element("MethodGlobalUserCost")), CultureInfo.InvariantCulture);

                if (!groups.Any(g => g.Id == id && g.GlobalPrice.Cost == globalPrice))
                {
                    var group = new SolexModel.Group
                    {
                        Id = id,
                        GlobalPrice = new GenericPrice { Cost = globalPrice },
                        Items = new List<SolexModel.ItemInGroup>()
                    };
                    groups.Add(group);
                }
            }

            var choice = new SolexModel.ShippingChoice
            {
                MainShippingMethodId = selectedShippingMethodId,
                Groups = groups,
                Parcels = new Dictionary<int, SolexModel.Parcel>(),
                TotalPrice = new GenericPrice()
            };

            return choice;
        }

        protected internal StandardArticle GetArticleDetails(XElement quoteArticle)
        {
            var productId = int.Parse(GetElementToParse(quoteArticle.Element("ProductID")));
            var servicesXML = quoteArticle.Element("Services").Elements("Service");
            var services = new ServiceList();

            foreach (var serviceItem in servicesXML)
            {
                var service = new Service
                {
                    ProductID = int.Parse(GetElementToParse(serviceItem.Element("ProductID"))),
                    Quantity = int.Parse(GetElementToParse(serviceItem.Element("Quantity"))),
                    // TODO à vérifier
                    VATRate = decimal.Parse(GetElementToParse(quoteArticle.Element("VATRate")), CultureInfo.InvariantCulture)
                };

                services.Add(service);
            }

            var standardArticle = new StandardArticle
            {
                ProductID = productId,
                Quantity = int.Parse(GetElementToParse(quoteArticle.Element("QuoteDetailQuantity"))),
                ReferenceGU = quoteArticle.Element("QuoteDetailArticleReference")?.Value,
                Services = services
            };

            standardArticle.VATId = int.Parse(GetElementToParse(quoteArticle.Element("VATId")), CultureInfo.InvariantCulture);
            // Prix de l'article repris du XML
            SetArticlePrices(standardArticle, quoteArticle);

            _articleDetailService.GetArticleDetail(standardArticle, _applicationContext.GetSiteContext());

            standardArticle.AvailabilityId = int.Parse(GetElementToParse(quoteArticle.Element("QuoteDetailArticleAvailabilityID")));

            standardArticle.Address = new PostalAddress()
            {
                Identity = int.Parse(GetElementToParse(quoteArticle.Element("AddressID")))
            };

            standardArticle.VATRate = decimal.Parse(GetElementToParse(quoteArticle.Element("VATRate")), CultureInfo.InvariantCulture);
            standardArticle.SplitLevel = int.Parse(GetElementToParse(quoteArticle.Element("SplitLevel")), CultureInfo.InvariantCulture);
            standardArticle.Type = (Model.ArticleType)int.Parse(GetElementToParse(quoteArticle.Element("Type")), CultureInfo.InvariantCulture);
            standardArticle.ShipMethod = int.Parse(GetElementToParse(quoteArticle.Element("ShipMethod")));
            standardArticle.ArticleDelay = int.Parse(GetElementToParse(quoteArticle.Element("ArticleDelay")));
            standardArticle.ShipDelay = int.Parse(GetElementToParse(quoteArticle.Element("DelayInHours")));
            standardArticle.TheoreticalExpeditionDate = DateTime.TryParse(GetElementToParse(quoteArticle.Element("QuoteDetailShippingTheoricDate")), out var date) ? date : (DateTime?)null;

            standardArticle.Promotions = new PromotionList();

            return standardArticle;
        }

        protected internal void GetShippingInformation(PopOrderForm popOrderForm, XDocument document, PopLineGroup lineGroup, SolexModel.ShippingChoice choice)
        {
            foreach (var groupedArticles in lineGroup.Articles.OfType<StandardArticle>().GroupBy(x => x.Address.Identity))
            {
                var quoteAddressEntity = document.Root.Element("AddressRepository")
                                                      .Descendants("QuoteAddressEntity")
                                                      .FirstOrDefault(x => int.Parse(x.Element("AddressBookId")?.Value ?? "0") == groupedArticles.Key);

                if (quoteAddressEntity != null)
                {
                    var firstName = quoteAddressEntity.Element("FirstName")?.Value;
                    var lastName = quoteAddressEntity.Element("LastName")?.Value;
                    var zipCode = quoteAddressEntity.Element("Zipcode")?.Value;
                    var addressLine1 = quoteAddressEntity.Element("AddressLine1")?.Value;
                    var addressLine2 = quoteAddressEntity.Element("AddressLine2")?.Value;
                    var addressLine3 = quoteAddressEntity.Element("AddressLine3")?.Value;
                    var company = quoteAddressEntity.Element("Company")?.Value;
                    var city = quoteAddressEntity.Element("City")?.Value;
                    var countryId = int.Parse(GetElementToParse(quoteAddressEntity.Element("CountryId")));

                    var countryCode = quoteAddressEntity.Element("CountryCode2")?.Value;


                    var postalAddress = new PostalAddress()
                    {
                        Firstname = firstName,
                        LastName = lastName,
                        ZipCode = zipCode,
                        AddressLine1 = addressLine1,
                        AddressLine2 = addressLine2,
                        AddressLine3 = addressLine3,
                        Company = company,
                        City = city,
                        CountryCode2 = countryCode,
                        CountryId = countryId,
                        AddressBookReference = quoteAddressEntity.Element("AddressBookReference")?.Value,
                        AddressEntity = new Customer.Model.AddressEntity()
                        {
                            Reference = quoteAddressEntity.Element("AddressBookReference")?.Value,
                            Id = groupedArticles.Key.GetValueOrDefault()
                        },
                        Identity = groupedArticles.Key.GetValueOrDefault()
                    };

                    popOrderForm.ShippingMethodEvaluation.WithSeller(lineGroup.Seller.SellerId);
                    if (popOrderForm.ShippingMethodEvaluation.FnacCom.HasValue)
                    {
                        popOrderForm.ShippingMethodEvaluation.FnacCom.Value.SelectShippingMethodEvaluationType(BaseModel.ShippingMethodEvaluationType.Postal);
                        popOrderForm.ShippingMethodEvaluation.FnacCom.Value.ReplaceAddress(postalAddress);

                        var selectedShippingMethodId = choice.MainShippingMethodId;

                        popOrderForm.ShippingMethodEvaluation.FnacCom.Value.PostalShippingMethodEvaluationItem.Value.SelectedShippingMethodId = selectedShippingMethodId;
                        popOrderForm.ShippingMethodEvaluation.FnacCom.Value.PostalShippingMethodEvaluationItem.Value.DefaultShippingMethodId = popOrderForm.ShippingMethodEvaluation.FnacCom.Value.PostalShippingMethodEvaluationItem.Value.SelectedShippingMethodId;

                        popOrderForm.ShippingMethodEvaluation.FnacCom.Value.PostalShippingMethodEvaluationItem.Value.Choices = new List<SolexModel.ShippingChoice> { choice };

                    }
                }
            }
        }

        protected internal void GetVATRates(PopOrderForm popOrderForm, XDocument document)
        {
            var vats = new List<VATApplied>();

            foreach (var vatApplied in document.Root.Element("VAT").Elements("VATApplied"))
            {
                var rateVatItem = decimal.Parse(GetElementToParse(vatApplied.Element("Rate")), CultureInfo.InvariantCulture);
                var totalVatItem = decimal.Parse(GetElementToParse(vatApplied.Element("Total")), CultureInfo.InvariantCulture);

                vats.Add(new VATApplied
                {
                    Rate = rateVatItem,
                    Total = totalVatItem
                });
            }

            popOrderForm.GlobalPrices.VATList.AddRange(vats);
        }

        protected internal void GetBillingAddress(PopOrderForm popOrderForm, int accountId, XDocument document)
        {
            var billingAddressReference = document.Root.Element("BillingAddressReference")?.Value;
            var billingAddress = !string.IsNullOrWhiteSpace(billingAddressReference) ? _addressBookService.GetAddressBook(accountId, null).FirstOrDefault(a => a.Reference == new Guid(billingAddressReference)) : null;
            if (billingAddress != null)
            {
                var addressEntity = _postalAddressService.GetPostalAddress(accountId, billingAddress.AddressId);
                popOrderForm.BillingAddress = new BillingAddress
                {
                    Firstname = $"{addressEntity.FirstName}",
                    Lastname = $"{addressEntity.LastName}"
                };

                if (!string.IsNullOrEmpty(addressEntity.AliasName))
                {
                    popOrderForm.BillingAddress.Alias = $"{addressEntity.AliasName}";
                }

                if (!string.IsNullOrEmpty(addressEntity.VatNumber))
                {
                    popOrderForm.BillingAddress.VATNumber = $"{addressEntity.VatNumber}";
                }

                if (!string.IsNullOrEmpty(addressEntity.Company))
                {
                    popOrderForm.BillingAddress.Company = $"{addressEntity.Company}";
                }

                if (!string.IsNullOrEmpty(addressEntity.AddressLine1))
                {
                    popOrderForm.BillingAddress.AddressLine1 = $"{addressEntity.AddressLine1}";
                }

                if (!string.IsNullOrEmpty(addressEntity.AddressLine2))
                {
                    popOrderForm.BillingAddress.AddressLine2 = $"{addressEntity.AddressLine2}";
                }

                if (!string.IsNullOrEmpty(addressEntity.AddressLine3))
                {
                    popOrderForm.BillingAddress.AddressLine3 = $"{addressEntity.AddressLine3}";
                }

                if (!string.IsNullOrEmpty(addressEntity.AddressLine4))
                {
                    popOrderForm.BillingAddress.AddressLine4 = $"{addressEntity.AddressLine4}";
                }

                popOrderForm.BillingAddress.ZipCode = $"{addressEntity.Zipcode}";
                popOrderForm.BillingAddress.City = $"{addressEntity.City}";

                if (!string.IsNullOrEmpty(addressEntity.State))
                {
                    popOrderForm.BillingAddress.State = $"{addressEntity.State}";
                }

                popOrderForm.BillingAddress.CountryId = addressEntity.CountryId;

                if (!string.IsNullOrEmpty(addressEntity.Tel))
                {
                    popOrderForm.BillingAddress.Phone = $"{addressEntity.Tel}";
                }

                if (!string.IsNullOrEmpty(addressEntity.CellPhone))
                {
                    popOrderForm.BillingAddress.CellPhone = $"{addressEntity.CellPhone}";
                }

                if (!string.IsNullOrEmpty(addressEntity.Fax))
                {
                    popOrderForm.BillingAddress.Fax = $"{addressEntity.Fax}";
                }

                if (!string.IsNullOrEmpty(addressEntity.TaxIdNumber))
                {
                    popOrderForm.BillingAddress.TaxIdNumber = $"{addressEntity.TaxIdNumber}";
                }

                popOrderForm.BillingAddress.NationalityCountry = addressEntity.NationalityCountryId;

                popOrderForm.BillingAddress.LegalStatus = addressEntity.LegalStatusId;

                popOrderForm.BillingAddress.GenderId = addressEntity.GenderId;
            }
        }

        private string GetElementToParse(XElement xelement)
        {
            return string.IsNullOrEmpty(xelement?.Value.Trim()) ?
                "0" : xelement.Value.Trim();
        }

        protected internal void SetArticlePrices(StandardArticle standardArticle, XElement quoteArticle)
        {
            standardArticle.PriceDBEur = decimal.Parse(GetElementToParse(quoteArticle.Element("PriceDBEur")), CultureInfo.InvariantCulture);
            standardArticle.PriceRealEur = decimal.Parse(GetElementToParse(quoteArticle.Element("PriceRealEur")), CultureInfo.InvariantCulture);

            var quoteDetailDiscountAmount = decimal.Parse(GetElementToParse(quoteArticle.Element("QuoteDetailDiscountAmount")), CultureInfo.InvariantCulture);

            var articlePriceNoVATEur = decimal.Parse(GetElementToParse(quoteArticle.Element("PriceNoVATEur")), CultureInfo.InvariantCulture);
            var EcoTaxEur = decimal.Parse(GetElementToParse(quoteArticle.Element("EcoTaxPrice")), CultureInfo.InvariantCulture);
            var EcoTaxEurNoVAT = decimal.Parse(GetElementToParse(quoteArticle.Element("EcoTaxPriceNoVAT")), CultureInfo.InvariantCulture);
            var vat = decimal.Parse(GetElementToParse(quoteArticle.Element("VATRate")), CultureInfo.InvariantCulture);

            standardArticle.PriceNoVATEur = articlePriceNoVATEur - quoteDetailDiscountAmount;
            standardArticle.PriceUserEur = Math.Round(standardArticle.PriceNoVATEur.GetValueOrDefault() * (1 + (vat / 100)), 2);

            //TODO : Refacto
            //set de l'hors taxe de l'article qui vaut les hors taxe des trois type de prix.
            standardArticle.PriceUserEurNoVAT = standardArticle.PriceNoVATEur.GetValueOrDefault();
            standardArticle.PriceDBEurNoVAT = standardArticle.PriceNoVATEur.GetValueOrDefault();
            standardArticle.PriceRealEurNoVAT = standardArticle.PriceNoVATEur.GetValueOrDefault();
            //set de l'hors taxe de l'article qui vaut les hors taxe des trois type de prix.


            standardArticle.EcoTaxEur = EcoTaxEur;
            standardArticle.EcoTaxEurNoVAT = EcoTaxEurNoVAT;

            standardArticle.ShipPriceDBEur = decimal.Parse(GetElementToParse(quoteArticle.Element("ShipPriceDBEur")), CultureInfo.InvariantCulture);
            standardArticle.ShipPriceUserEur = decimal.Parse(GetElementToParse(quoteArticle.Element("ShipPriceUserEur")), CultureInfo.InvariantCulture);
            standardArticle.ShipPriceNoVATEur = decimal.Parse(GetElementToParse(quoteArticle.Element("ShipPriceNoVATEur")), CultureInfo.InvariantCulture);

            standardArticle.WrapPriceDBEur = decimal.Parse(GetElementToParse(quoteArticle.Element("QuoteWrapPrice")), CultureInfo.InvariantCulture);
            standardArticle.WrapPriceUserEur = decimal.Parse(GetElementToParse(quoteArticle.Element("QuoteDetailWrapPrice")), CultureInfo.InvariantCulture);
            standardArticle.WrapPriceNoVATEur = decimal.Parse(GetElementToParse(quoteArticle.Element("QuoteWrapPriceNoVAT")), CultureInfo.InvariantCulture);

            SetPricesSalesInfo(standardArticle);
        }

        private static void SetPricesSalesInfo(StandardArticle standardArticle)
        {
            standardArticle.SalesInfo = new SalesInfo
            {
                Promotions = new List<FnacDirect.Contracts.Online.Model.Promotion>()
            };

            standardArticle.SalesInfo.VATId = (int)standardArticle.VATId;

            var reducedPrice = new Price
            {
                Type = PriceType.Reduced,
                HT = (decimal)standardArticle.PriceNoVATEur,
                TTC = (decimal)standardArticle.PriceUserEur
            };

            var standardPrice = new Price
            {
                Type = PriceType.Standard,
                TTC = (decimal)standardArticle.PriceDBEur
            };

            var publicPrice = new Price
            {
                Type = PriceType.Public,
                TTC = (decimal)standardArticle.PriceRealEur
            };

            standardArticle.SalesInfo.Prices = new PriceCollection
            {
                reducedPrice,
                standardPrice,
                publicPrice
            };
        }
    }
}
