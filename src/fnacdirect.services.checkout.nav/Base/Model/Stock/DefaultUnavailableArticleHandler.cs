﻿using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.OrderPipe.Base.Model.Facets;
using FnacDirect.OrderPipe.Base.Model.LineGroups;
using System.Linq;

namespace FnacDirect.OrderPipe.Base.Business.Stock
{
    public class DefaultUnavailableArticleHandler : IUnavailableArticleHandler
    {
        private readonly IGotLineGroups _iGotLineGroups;

        public DefaultUnavailableArticleHandler(IGotLineGroups iGotLineGroups)
        {
            _iGotLineGroups = iGotLineGroups;
        }

        public bool Handle(ILineGroup parentLineGroup, StandardArticle standardArticle)
        {
            var deletedArticles = _iGotLineGroups.DeletedArticles;

            if ((standardArticle.Type == ArticleType.Saleable && !standardArticle.IsForSale)
                || standardArticle.Availability == StockAvailability.None
                || standardArticle.IsAvailable == false
                || standardArticle.Quantity <= 0)
            {
                if (!deletedArticles.Exists(delArt => delArt.ID == standardArticle.ID))
                {
                    deletedArticles.Add(standardArticle);
                }

                parentLineGroup.Articles.Remove(standardArticle);

                return true;
            }

            foreach (var service in standardArticle.Services.ToList())
            {
                if (!service.IsForSale
                    || service.Availability == StockAvailability.None)
                {
                    deletedArticles.Add(service);
                    standardArticle.Services.Remove(service);
                }
            }

            return false;
        }
    }
}
