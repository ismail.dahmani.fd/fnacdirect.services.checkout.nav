namespace FnacDirect.OrderPipe
{
    public class MultiFacet<T1, T2> : IPrivateDataContainer
        where T1 : IOF
        where T2 : IOF
    {
        public MultiFacet(T1 facet1, T2 facet2)
        {
            Facet1 = facet1;
            Facet2 = facet2;
        }

        public T1 Facet1 { get; }
        public T2 Facet2 { get; }

        public static MultiFacet<T1, T2> From<TGlobal>(TGlobal global)
            where TGlobal : T1, T2
        {
            return new MultiFacet<T1, T2>(global, global);
        }

        public T GetPrivateData<T>(string dataName, bool create)
            where T : Data, new()
        {
            return Facet1.GetPrivateData<T>(dataName, create);
        }

        public T GetPrivateData<T>(string dataName)
            where T : Data, new()
        {
            return Facet1.GetPrivateData<T>(dataName);
        }

        public T GetPrivateData<T>(bool create)
            where T : Data, new()
        {
            return Facet1.GetPrivateData<T>(create);
        }

        public T GetPrivateData<T>()
            where T : Data, new()
        {
            return Facet1.GetPrivateData<T>();
        }

        public void RemovePrivateData<T>()
            where T : Data, new()
        {
            Facet1.RemovePrivateData<T>();
        }

        public void RemovePrivateData<T>(string name)
            where T : Data, new()
        {
            Facet1.RemovePrivateData<T>(name);
        }

        public void RemovePrivateData<T>(T data)
            where T : Data
        {
            Facet1.RemovePrivateData<T>(data);
        }

        public override string ToString()
        {
            return Facet1.ToString();
        }
    }

    public class MultiFacet<T1, T2, T3> : MultiFacet<T1, T2>
        where T1 : IOF
        where T2 : IOF
        where T3 : IOF
    {
        public MultiFacet(T1 facet1, T2 facet2, T3 facet3)
            : base(facet1, facet2)
        {
            Facet3 = facet3;
        }

        public T3 Facet3 { get; }

        public static new MultiFacet<T1, T2, T3> From<TGlobal>(TGlobal global)
            where TGlobal : T1, T2, T3
        {
            return new MultiFacet<T1, T2, T3>(global, global, global);
        }
    }

    public class MultiFacet<T1, T2, T3, T4> : MultiFacet<T1, T2, T3>
        where T1 : IOF
        where T2 : IOF
        where T3 : IOF
        where T4 : IOF
    {
        public MultiFacet(T1 facet1, T2 facet2, T3 facet3, T4 facet4)
            : base(facet1, facet2, facet3)
        {
            Facet4 = facet4;
        }

        public T4 Facet4 { get; }

        public static new MultiFacet<T1, T2, T3, T4> From<TGlobal>(TGlobal global)
            where TGlobal : T1, T2, T3, T4
        {
            return new MultiFacet<T1, T2, T3, T4>(global, global, global, global);
        }
    }

    public class MultiFacet<T1, T2, T3, T4, T5> : MultiFacet<T1, T2, T3, T4>
        where T1 : IOF
        where T2 : IOF
        where T3 : IOF
        where T4 : IOF
        where T5 : IOF
    {
        public MultiFacet(T1 facet1, T2 facet2, T3 facet3, T4 facet4, T5 facet5)
            : base(facet1, facet2, facet3, facet4)
        {
            Facet5 = facet5;
        }

        public T5 Facet5 { get; }

        public static new MultiFacet<T1, T2, T3, T4, T5> From<TGlobal>(TGlobal global)
            where TGlobal : T1, T2, T3, T4, T5
        {
            return new MultiFacet<T1, T2, T3, T4, T5>(global, global, global, global, global);
        }
    }

    public class MultiFacet<T1, T2, T3, T4, T5, T6> : MultiFacet<T1, T2, T3, T4, T5>
        where T1 : IOF
        where T2 : IOF
        where T3 : IOF
        where T4 : IOF
        where T5 : IOF
        where T6 : IOF
    {
        public MultiFacet(T1 facet1, T2 facet2, T3 facet3, T4 facet4, T5 facet5, T6 facet6)
            : base(facet1, facet2, facet3, facet4, facet5)
        {
            Facet6 = facet6;
        }

        public T6 Facet6 { get; }

        public static new MultiFacet<T1, T2, T3, T4, T5, T6> From<TGlobal>(TGlobal global)
            where TGlobal : T1, T2, T3, T4, T5, T6
        {
            return new MultiFacet<T1, T2, T3, T4, T5, T6>(global, global, global, global, global, global);
        }
    }

    public class MultiFacet<T1, T2, T3, T4, T5, T6, T7> : MultiFacet<T1, T2, T3, T4, T5, T6>
        where T1 : IOF
        where T2 : IOF
        where T3 : IOF
        where T4 : IOF
        where T5 : IOF
        where T6 : IOF
        where T7 : IOF
    {
        public MultiFacet(T1 facet1, T2 facet2, T3 facet3, T4 facet4, T5 facet5, T6 facet6, T7 facet7)
            : base(facet1, facet2, facet3, facet4, facet5, facet6)
        {
            Facet7 = facet7;
        }

        public T7 Facet7 { get; }

        public static new MultiFacet<T1, T2, T3, T4, T5, T6, T7> From<TGlobal>(TGlobal global)
            where TGlobal : T1, T2, T3, T4, T5, T6, T7
        {
            return new MultiFacet<T1, T2, T3, T4, T5, T6, T7>(global, global, global, global, global, global, global);
        }
    }
}
