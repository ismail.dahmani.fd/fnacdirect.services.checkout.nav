namespace FnacDirect.OrderPipe.Base.Business.Configuration
{
    public interface IOrangeConfigurationProvider
    {
        OrangeConfiguration Get();
    }
}
