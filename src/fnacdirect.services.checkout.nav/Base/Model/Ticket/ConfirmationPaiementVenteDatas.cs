﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FnacDirect.OrderPipe.FDV.Model
{
    public class ConfirmationPaiementVenteDatas
    {
        public bool IsSucceeded { get; set; }
        public bool IsNoResponse { get; set; }
        public string ErrorCode { get; set; }
        public string ErrorDescription { get; set; }
    }
}
