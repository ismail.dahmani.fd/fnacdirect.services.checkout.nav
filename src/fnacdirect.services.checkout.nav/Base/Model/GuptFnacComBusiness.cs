using FnacDirect.OrderPipe.Base.LineGroups;
using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.OrderPipe.Base.Model.Pop;
using FnacDirect.OrderPipe.Base.Model.UserInformations;
using FnacDirect.ProxyEAI.IService;
using FnacDirect.ProxyEAI.Model;
using FnacDirect.Technical.Framework.CoreServices;
using FnacDirect.Technical.Framework.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using FnacModel = FnacDirect.OrderPipe.Base.BaseModel.Model;

namespace FnacDirect.OrderPipe.Base.Business
{

    public class GuptFnacComBusiness : IGuptFnacComBusiness
    {
        private readonly RangeSet<int> _tvArticleTypesId;
        private readonly List<string> _nonEligibleToD3EShopIds;

        public GuptFnacComBusiness(RangeSet<int> tvArticleTypesId)
        {
            _tvArticleTypesId = tvArticleTypesId;
        }

        public GuptFnacComBusiness(RangeSet<int> tvArticleTypesId, List<string> nonEligibleToD3eShopIds)
            : this(tvArticleTypesId)
        {
            _nonEligibleToD3EShopIds = nonEligibleToD3eShopIds;
        }

        private ShopTvRoyalty BuildTvRoyalty(FnacModel.TelevisualRoyaltyData trd, ILogisticLineGroup lineGroup)
        {
            ShopTvRoyalty tr = null;

            var art = lineGroup.Articles.OfType<StandardArticle>().FirstOrDefault(a => _tvArticleTypesId.Contains(a.TypeId.GetValueOrDefault()));

            if (trd != null && trd.Value != null && art != null)
            {
                var infoTv = trd.Value;
                int.TryParse(infoTv.BirthState, out var dep);

                tr = new ShopTvRoyalty()
                {
                    Marque = art.Editor,
                    Appelation = int.Parse(infoTv.Gender),

                    Prenom = infoTv.FirstName,
                    Nom = infoTv.LastName,
                    RaisonSociale = infoTv.Company,
                    NoVoie = infoTv.StreetNo,
                    Rang = infoTv.StreetRange,

                    TypeVoie = infoTv.StreetType,
                    Adresse = infoTv.AddressLine1,
                    Adresse2 = infoTv.AddressLine2,
                    CodePostal = infoTv.ZipCode,
                    Commune = infoTv.City,

                    UtilisationPays = int.Parse(infoTv.TVUse),
                    UtilisationDomaine = int.Parse(infoTv.TVUse2),
                    Quantite = art.Quantity.GetValueOrDefault(1),

                    DateNaissance = string.Format("{0}/{1}/{2}", infoTv.DOBYear, infoTv.DOBMonth, infoTv.DOBDay),
                    PaysNaissance = infoTv.BirthLocation,

                    DepartementNaissance = dep
                };
            }
            return tr;
        }

        private ShopOrder BuildOrder(ILogisticLineGroup logisticLineGroup, IEnumerable<BillingMethod> billingMethods, FnacModel.ShopData data, ShopAddress shop, bool isMobile = false)
        {

            var order = new ShopOrder
            {
                CountryCode = shop.CountryCode2,
                ShopCode = string.Format("{0:0000}", shop.RefUG),
                RetrievalDate = data.DateRetrait,
                PaymentCredit = GetOrderCredit(billingMethods),
                MessageType = "FNAC_COM_VENTE_DIF",
                Articles = new List<ShopArticle>(logisticLineGroup.Articles.Count)
            };

            var isEligibleToEcoTax = _nonEligibleToD3EShopIds != null && !_nonEligibleToD3EShopIds.Contains(order.ShopCode);
            order.OrderId = logisticLineGroup.OrderPk.Value;
            order.RefOrder = logisticLineGroup.OrderUserReference;

            var total = 0m;
            foreach (var tmpArt in logisticLineGroup.Articles)
            {
                var strTypePromo = string.Empty;
                var strMotivationPromo = string.Empty;

                if (tmpArt is StandardArticle stdArt)
                {
                    if (stdArt.Promotions.Count > 0)
                    {
                        foreach (var tmpPromo in stdArt.Promotions)
                        {
                            if (stdArt.PriceDBEur.Value - stdArt.PriceUserEur.Value > 0)
                            {
                                if (tmpPromo.Code.ToUpperInvariant().StartsWith("JRA"))
                                {
                                    strTypePromo = "ADH";
                                    strMotivationPromo = "05";
                                }
                                else
                                {
                                    if (tmpPromo.Code.ToUpperInvariant().StartsWith("ADH") ||
                                        tmpPromo.Code.ToUpperInvariant().StartsWith("NONADH"))
                                    {
                                        strTypePromo = "ADH";
                                        strMotivationPromo = "01";
                                    }
                                    else
                                    {
                                        strTypePromo = "PRM";
                                        strMotivationPromo = "13";
                                    }
                                }
                            }

                        }
                    }

                    var shopArticle = new ShopArticle
                    {
                        RefId = stdArt.ReferenceGU,
                        Quantity = stdArt.Quantity.GetValueOrDefault(1),
                        PricePerUnit = stdArt.PriceDBEur.Value,
                        PriceDiscount = stdArt.PriceUserEur.Value,
                        Discount = stdArt.PriceDBEur.Value - stdArt.PriceUserEur.Value,
                        DiscountCode = strMotivationPromo,
                        DiscountType = strTypePromo,
                        EcoTax = isEligibleToEcoTax ? stdArt.EcoTaxEur.GetValueOrDefault(0) : 0,
                        CommercialOperationsCodes = stdArt.Promotions.Select(promotion => promotion.Code).ToList(),
                        PriorityCode = stdArt.SteedFromStore ? "2HC" : (isMobile ? "MOB" : "FCO"),
                        OrderDetailId = stdArt.OrderDetailPk.Value
                    };

                    order.Articles.Add(shopArticle);

                    total += (stdArt.PriceUserEur.Value + stdArt.EcoTaxEur.GetValueOrDefault(0)) * stdArt.Quantity.Value;
                }
            }

            order.Total = total;

            return order;
        }

        private static int GetOrderCredit(IEnumerable<BillingMethod> billingMethods)
        {
            return billingMethods.OfType<CreditCardBillingMethod>().Any() ? 1 : 0;
        }

        public string GetResultReserveClickAndCollect(ILogisticLineGroup logisticLineGroup, IUserContextInformations userContext,
            IEnumerable<BillingMethod> billingMethods, BillingAddress billingAddress, FnacModel.ShopData data,
            FnacModel.TelevisualRoyaltyData televisualRoyaltyData, ShopAddress shopAddress, bool isMobile, PopAdherentData popAdherentData)
        {
            var result = string.Empty;
            try
            {
                if (!ConfigurationManager.Current.Maintenance.ProxyEAI)
                {
                    var svc = Service<IProxyEAIService>.Instance;
                    var order = BuildOrder(logisticLineGroup, billingMethods, data, shopAddress, isMobile);
                    var customer = BuildCustomer(userContext, billingAddress);
                    var tvRoyalty = BuildTvRoyalty(televisualRoyaltyData, logisticLineGroup);

                    result = svc.CreerCommande(order, customer, tvRoyalty);
                }
            }
            catch (Exception ex)
            {
                Logging.Current.WriteError("GuptHelperBiztalk.GetGUPTRefInvoiceShop", ex);
            }

            return result;
        }

        private static ShopCustomer BuildCustomer(IUserContextInformations userContext, BillingAddress billingAddress)
        {
            var cust = new ShopCustomer
            {
                AdherentNumber = userContext.ClientCard,
                AdherentPlasticCard = string.IsNullOrEmpty(userContext.PlasticCardNumber) ? userContext.CustomerEntity.AdherentCardNumber : userContext.PlasticCardNumber
            };

            if (billingAddress != null)
            {
                cust.FirstName = billingAddress.Firstname;
                cust.LastName = billingAddress.Lastname;
                cust.PhoneNumber = billingAddress.Phone;
                cust.EMail = userContext.ContactEmail;
                cust.SocialReason = billingAddress.Company;
                cust.Address = billingAddress.AddressLine1;
                cust.ZipCode = billingAddress.ZipCode;
                cust.City = billingAddress.City;
                cust.CountryCode = billingAddress.CountryCode2;
                cust.CellPhone = billingAddress.CellPhone;
                cust.Civilite = GetGenderType(billingAddress.GenderId);
            }
            return cust;
        }

        private static int GetGenderType(int pGenderId)
        {
            switch (pGenderId)
            {
                case 2:
                    return 1;
                case 3:
                    return 2;
                case 4:
                    return 3;
                case 1:
                default:
                    return 4;
            }
        }
    }
}
