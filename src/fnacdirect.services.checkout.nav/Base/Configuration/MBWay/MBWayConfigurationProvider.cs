using FnacDirect.Technical.Framework.Configuration;

namespace FnacDirect.OrderPipe.Base.Business.Configuration
{
    public class MBWayConfigurationProvider : IMBWayConfigurationProvider
    {
        private readonly MBWayConfiguration _mbwayConfiguration;

        public MBWayConfigurationProvider(IConfigurationProvider configurationProvider)
        {
            _mbwayConfiguration = configurationProvider.GetAs<MBWayConfiguration>("mbway");
        }

        public MBWayConfiguration Get()
        {
            return _mbwayConfiguration;
        }
    }
}
