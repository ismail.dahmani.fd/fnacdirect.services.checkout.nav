﻿
using System.Xml.Serialization;
namespace FnacDirect.OrderPipe.Base.Business.OrderDetail
{
    public class RelayAddress : BaseAddress
    {
        public RelayAddress()
        {
            Type = "RelayAddress";
        }

        public string Name { get; set; }

        public string State { get; set; }

        public string Type { get; set; }
    }
}
