using FnacDirect.Contracts.Online.IModel;
using FnacDirect.Contracts.Online.Model;
using FnacDirect.Membership.Model.Constants;
using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.OrderPipe.BaseMvc.Web.Services.Pop;
using FnacDirect.Technical.Framework.Web.Mvc;
using System.Collections.Generic;
using System.Linq;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Common.Basket
{
    public class BasketItemViewModelBuilder : IBasketItemViewModelBuilder
    {
        private readonly IApplicationContext _applicationContext;
        private readonly IArticleImagePathService _articleImagePathService;
        private readonly IArticleBusiness3 _articleBusiness3;

        public BasketItemViewModelBuilder(IApplicationContext applicationContext, IArticleImagePathService articleImagePathService, IArticleBusiness3 articleBusiness3)
        {
            _applicationContext = applicationContext;
            _articleImagePathService = articleImagePathService;
            _articleBusiness3 = articleBusiness3;
        }

        private BasketItemViewModel BuildItemStandardArticleWithoutServices(StandardArticle standardArticle, IEnumerable<Base.Model.Promotion> promotions)
        {
            var basketItemViewModel = new BasketItemViewModel();

            var isFnacPro = _applicationContext.GetSiteId() == (int)FnacSites.FNACPRO;

            var ecoTax = standardArticle.EcoTaxEur.GetValueOrDefault(0);
            var priceOldUnit = standardArticle.PriceDBEur.GetValueOrDefault(0) + ecoTax;
            basketItemViewModel.PriceOld = _applicationContext.GetPriceHelper().Format(priceOldUnit * (decimal)standardArticle.Quantity, PriceFormat.CurrencyAsDecimalSeparator);
            var priceUnit = standardArticle.PriceUserEur.GetValueOrDefault(0) + ecoTax;
            basketItemViewModel.Price = _applicationContext.GetPriceHelper().Format(priceUnit * (decimal)standardArticle.Quantity, PriceFormat.CurrencyAsDecimalSeparator);

            var ecoTaxNoVat = standardArticle.EcoTaxEurNoVAT.GetValueOrDefault(0);// * standardArticle.Quantity.GetValueOrDefault();                        
            var priceNoVatUnit = standardArticle.PriceNoVATEur.GetValueOrDefault(0) + ecoTaxNoVat;
            basketItemViewModel.PriceNoVat = _applicationContext.GetPriceHelper().Format(priceNoVatUnit * (decimal)standardArticle.Quantity, PriceFormat.CurrencyAsDecimalSeparator);
            basketItemViewModel.PriceToDisplay = isFnacPro ? basketItemViewModel.PriceNoVat : basketItemViewModel.Price;

            if (standardArticle.SalesInfo != null && standardArticle.SalesInfo.StandardPrice != null)
            {
                var priceNoVatOldUnit = standardArticle.SalesInfo.StandardPrice.HT + ecoTaxNoVat;

                if (priceNoVatUnit != priceNoVatOldUnit)
                {
                    basketItemViewModel.PriceOldNoVat = _applicationContext.GetPriceHelper().Format(priceNoVatOldUnit * (decimal)standardArticle.Quantity, PriceFormat.CurrencyAsDecimalSeparator);
                    basketItemViewModel.PriceOldToDisplay = isFnacPro ? basketItemViewModel.PriceOldNoVat : basketItemViewModel.PriceOld;
                }
            }

            basketItemViewModel.IsNumerical = standardArticle.IsNumerical;
            basketItemViewModel.IsEbook = standardArticle.IsEbook;
            basketItemViewModel.IsBook = standardArticle.IsBook;
            basketItemViewModel.HasD3E = standardArticle.HasD3E;
            basketItemViewModel.HasRCP = standardArticle.HasRCP;

            var isFnacPlusTryCard = standardArticle.TypeId.GetValueOrDefault() == (int)CategoryCardType.SouscriptionFnacPlusTrial;

            basketItemViewModel.IsFree = standardArticle.Type == Base.Model.ArticleType.Free || isFnacPlusTryCard;

            var promotion = promotions.FirstOrDefault(p => p.FreeArtId == standardArticle.ProductID);

            basketItemViewModel.Title = BuildTitle(standardArticle, basketItemViewModel.IsFree, promotion);

            basketItemViewModel.SubTitle = BuildSubTitle(standardArticle);

            basketItemViewModel.AvailabilityId = standardArticle.AvailabilityId ?? (int)AvailabilityEnum.Indisponible;
            basketItemViewModel.TotalOfferCount = standardArticle.TotalOfferCount ?? 0;

            return basketItemViewModel;
        }

        public BasketItemViewModel BuildItemStandardArticle(StandardArticle standardArticle, IEnumerable<Base.Model.Promotion> promotions)
        {
            var basketItemViewModel = BuildItemStandardArticleWithoutServices(standardArticle, promotions);

            if (standardArticle.Services != null)
            {
                foreach (var service in standardArticle.Services)
                {
                    basketItemViewModel.AddServices(BuildItemStandardArticle(service, promotions));
                }
            }

            return basketItemViewModel;
        }

        public BasketItemWithPictureViewModel BuildItemStandardArticleWithPicture(StandardArticle standardArticle, IEnumerable<Base.Model.Promotion> promotions)
        {
            var basketItemViewModel = BuildItemStandardArticleWithoutServices(standardArticle, promotions);

            var basketItemWithPictureViewModel = new BasketItemWithPictureViewModel(basketItemViewModel);

            var promotion = promotions.FirstOrDefault(p => p.FreeArtId == standardArticle.ProductID);

            basketItemWithPictureViewModel.NormalImage = _articleImagePathService.BuildPath(standardArticle, promotion);

            if (standardArticle.Services != null)
            {
                foreach (var service in standardArticle.Services)
                {
                    basketItemWithPictureViewModel.AddServices(BuildItemStandardArticleWithPicture(service, promotions));
                }
            }

            return basketItemWithPictureViewModel;
        }

        public BasketItemViewModel BuildItemMpArticle(MarketPlaceArticle mpArticle)
        {
            var prefix = string.Empty;

            if (mpArticle.Quantity.HasValue && mpArticle.Quantity.Value > 1)
            {
                prefix = mpArticle.Quantity.Value + " x ";
            }
            //Attention IPriceHelper n'est pas injectable pour le moment, d'ailleur je ne vois pas pourquoi on devrait passer par l'application context,
            //peut être à cause du besoin de contexte pays pour le format.
            var priceHelper = _applicationContext.GetPriceHelper();
            var priceOld = priceHelper.Format(mpArticle.TotalDbEur, PriceFormat.CurrencyAsDecimalSeparator);
            var price = priceHelper.Format(mpArticle.TotalUserEur, PriceFormat.CurrencyAsDecimalSeparator);
            var basketItemViewModel = new BasketItemViewModel
            {
                Title = prefix + mpArticle.Title,
                PriceOld = priceOld,
                PriceOldToDisplay = priceOld,
                Price = price,
                PriceToDisplay = price,
                HasRCP = mpArticle.HasRCP,
            };

            return basketItemViewModel;
        }

        public BasketItemWithPictureViewModel BuildItemMpArticleWithPicture(MarketPlaceArticle marketPlaceArticle)
        {
            var basketItemViewModel = BuildItemMpArticle(marketPlaceArticle);

            var basketItemWithPictureViewModel = new BasketItemWithPictureViewModel(basketItemViewModel)
            {
                NormalImage = _articleImagePathService.BuildPath(marketPlaceArticle)
            };

            return basketItemWithPictureViewModel;
        }

        protected internal string BuildTitle(StandardArticle standardArticle, bool isFree, Base.Model.Promotion promotion)
        {
            var prefix = string.Empty;

            if (standardArticle.Quantity.HasValue && standardArticle.Quantity.Value > 1)
            {
                prefix = standardArticle.Quantity.Value + " x ";
            }

            if (isFree && promotion != null && !string.IsNullOrWhiteSpace(promotion.DisplayText))
            {
                return prefix + promotion.DisplayText;
            }

            return prefix + standardArticle.Title;
        }

        protected internal string BuildSubTitle(StandardArticle standardArticle)
        {
            var isBook = (ArticleGroup)standardArticle.Family.GetValueOrDefault(0) == ArticleGroup.Book;

            if (standardArticle.IsNumerical)
            {
                return standardArticle.FormatLabel;
            }

            var dtoArticle = _articleBusiness3.GetArticle(_applicationContext.GetSiteContext(), standardArticle.ToArticleReference(), ArticleLoadingOption.Default, ArticleLoadingLevel.Default);

            var tome = 0;
            if (dtoArticle != null)
            {
                if (dtoArticle.ExtendedProperties.Volume.HasValue)
                {
                    tome = dtoArticle.ExtendedProperties.Volume.Value;
                }
            }

            return isBook && tome > 0 ? string.Format(_applicationContext.GetMessage("orderpipe.pop.basket.subtitle.tomaison"), tome) : null;
        }
    }
}
