using FnacDirect.OrderPipe.Base.BaseModel;
using FnacDirect.OrderPipe.Base.Model.LineGroups;
using FnacDirect.OrderPipe.Base.Model.Solex;
using System.Collections.Generic;
using System.Linq;
using AppointmentDelivery = FnacDirect.Solex.AppointmentDelivery.Client.Contract;

namespace FnacDirect.OrderPipe.Base.Business.Shipping
{
    public class AppointmentDeliveryCalendarService : IAppointmentDeliveryCalendarService
    {
        private readonly ISolexBusiness _solexBusiness;
        private readonly IArticleService _articleService;

        public AppointmentDeliveryCalendarService(ISolexBusiness solexBusiness, IArticleService articleService)
        {
            _solexBusiness = solexBusiness;
            _articleService = articleService;
        }

        public ItemsForAppointmentDeliveryRequest GetItemsAndPromotionForAppointmentDeliveryRequest(IEnumerable<ILineGroup> lineGroups, ShippingMethodEvaluationItem shippingMethodEvaluationItem, int shippingMethodId)
        {
            var articles = lineGroups.SelectMany(lg => lg.Articles);
            return GetItemsAndPromotionForAppointmentDeliveryRequest(articles, shippingMethodEvaluationItem, shippingMethodId);
        }

        public ItemsForAppointmentDeliveryRequest GetItemsAndPromotionForAppointmentDeliveryRequest(IEnumerable<Model.Article> articles, ShippingMethodEvaluationItem shippingMethodEvaluationItem, int shippingMethodId)
        {
            var promotions = new List<AppointmentDelivery.Promotion>();
            var items = new List<AppointmentDelivery.Item>();
            var identifiers = new List<Identifier>();

            if (shippingMethodEvaluationItem != null)
            {
                identifiers = _solexBusiness.GetIdentifiers(shippingMethodEvaluationItem, shippingMethodId).ToList();
            }

            var baseArticles = (from article in articles
                                where identifiers.Any(i => i.Prid == article.ProductID)
                                select article).OfType<Model.BaseArticle>().ToList();

            baseArticles.AddRange(articles.OfType<Model.StandardArticle>().Where(s => _articleService.IsFlyService(s)));

            foreach (var baseArticle in baseArticles)
            {
                items.Add(
                    new AppointmentDelivery.Item
                    {
                        Quantity = (int)baseArticle.Quantity,
                        Identifier = new AppointmentDelivery.Identifier { Prid = baseArticle.ProductID },
                        Expedition = new AppointmentDelivery.Expedition()
                        {
                            AvailabilityId = baseArticle.AvailabilityId.GetValueOrDefault()
                        }
                    }
                );
            }

            promotions.AddRange(GetPromotions(baseArticles));

            return new ItemsForAppointmentDeliveryRequest(items, promotions);
        }

        public IEnumerable<AppointmentDelivery.Promotion> GetPromotions(IEnumerable<Model.BaseArticle> baseArticles)
        {
            var promotions = new List<AppointmentDelivery.Promotion>();

            var baseArticlesWithPromotionsRules = baseArticles.Where(art => art.ShippingPromotionRules != null);
            if (baseArticlesWithPromotionsRules != null && baseArticlesWithPromotionsRules.Any())
            {
                foreach (var baseArticle in baseArticlesWithPromotionsRules)
                {
                    foreach (var shippingPromotionRule in baseArticle.ShippingPromotionRules.Where(x => x != null))
                    {
                        var shippingPromotionTrigger = new AppointmentDelivery.Promotion
                        {
                            TriggerKey = shippingPromotionRule.TriggerKey,
                            PromotionCode = shippingPromotionRule.Promotion != null ? shippingPromotionRule.Promotion.Code : string.Empty,
                            AdditionalRuleParam1 = shippingPromotionRule.Parameter1,
                            AdditionalRuleParam2 = shippingPromotionRule.Parameter2,
                            ApplyToLogisticTypes = shippingPromotionRule.LogisticFamily ?? new List<int>(),
                            Quantity = baseArticle.Quantity.GetValueOrDefault()
                        };

                        var existingTrigger = promotions.FirstOrDefault(x => x.TriggerKey == shippingPromotionTrigger.TriggerKey);

                        if (existingTrigger == null)
                        {
                            promotions.Add(shippingPromotionTrigger);
                        }
                        else
                        {
                            existingTrigger.Quantity += baseArticle.Quantity.GetValueOrDefault();
                        }
                    }
                }
            }

            return promotions;
        }
    }
}
