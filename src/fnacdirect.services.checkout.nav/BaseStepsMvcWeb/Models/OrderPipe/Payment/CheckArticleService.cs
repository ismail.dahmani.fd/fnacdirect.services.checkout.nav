using FnacDirect.Contracts.Online.Model;
using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.OrderPipe.Base.Model.Facets;
using FnacDirect.OrderPipe.Core.Services;
using FnacDirect.Technical.Framework.Web.Mvc;
using System.Collections.Generic;
using System.Linq;
using Article = FnacDirect.OrderPipe.Base.Model.Article;

namespace FnacDirect.OrderPipe.Base.Business.Payment
{
    public class CheckArticleService : ICheckArticleService
    {
        private readonly string _productSupportIds;
        private readonly IEnumerable<int> _eccvTypeIds;
        private readonly int _donationTypeId;

        public CheckArticleService(IApplicationContext applicationContext,
            IPipeParametersProvider pipeParametersProvider)
        {
            _productSupportIds = pipeParametersProvider.Get("ogone.3dsecure.sensiblesupportids", "");
            _eccvTypeIds = pipeParametersProvider.GetAsEnumerable<int>("eccv.typeid");

            _donationTypeId = int.Parse(applicationContext.GetAppSetting("DonationTypeId"));
        }

        public bool HasSensibleSupportId(IEnumerable<Article> articles)
        {
            if(articles == null)
            {
                return false;
            }

            var orderProductSupportIds = articles.Select(l =>
                (l is StandardArticle) ? (l as StandardArticle).SupportId :
                (l is MarketPlaceArticle) && (l as MarketPlaceArticle).Format.HasValue ?
                (l as MarketPlaceArticle).Format.Value : 0).ToList();

            return orderProductSupportIds.Any(p => _productSupportIds.Split(',').ToList().Contains(p.ToString()));
        }

        public bool HasECCV(IEnumerable<Article> articles)
        {
            return GetECCVs(articles).Any();
        }

        public IEnumerable<Article> GetECCVs(IEnumerable<Article> articles)
        {
            if (articles == null)
            {
                return new List<Article>();
            }
            return articles.Where(a => IsECCV(a));
        }

        public bool IsECCV(Article article)
        {
            if (article == null)
            {
                return false;
            }
            return article.TypeId.HasValue && _eccvTypeIds.Contains(article.TypeId.Value);
        }

        public bool HasDonation(IEnumerable<Article> articles)
        {
            return articles!= null && articles.Any(a => a.TypeId.HasValue && a.TypeId.Value == _donationTypeId);
        }
    }
}
