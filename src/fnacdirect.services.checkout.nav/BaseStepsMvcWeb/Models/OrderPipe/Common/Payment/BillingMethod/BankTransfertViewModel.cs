﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Common.Payment
{
    public class BankTransfertMethodViewModel : BillingMethodViewModel
    { 
        public string TransfertAccount { get; set; }
    }

}
