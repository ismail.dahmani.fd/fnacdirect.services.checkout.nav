using FnacDirect.OrderPipe.Base.BaseModel.Model;
using FnacDirect.OrderPipe.Base.Business;
using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.OrderPipe.Base.Model.Facets;
using FnacDirect.OrderPipe.Base.Model.Pop;
using FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop;
using System.Web.Mvc;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Controllers.Pop
{
    public class PopTvRoyaltyController : PopBaseController
    {
        private readonly OPContext _opContext;
        private readonly ITvRoyaltyViewModelBuilder _tvRoyaltyViewModelBuilder;
        private readonly ITvRoyaltyBusiness _tvRoyaltyBusiness;

        public PopTvRoyaltyController(OPContext opContext, ITvRoyaltyViewModelBuilder tvRoyaltyViewModelBuilder, ITvRoyaltyBusiness tvRoyaltyBusiness)
        {
            _opContext = opContext;
            _tvRoyaltyViewModelBuilder = tvRoyaltyViewModelBuilder;
            _tvRoyaltyBusiness = tvRoyaltyBusiness;
        }

        [HttpGet]
        public ViewResult Index()
        {
            var iGotShippingMethodEvaluation = _opContext.OF as IGotShippingMethodEvaluation;
            var tvForm = _tvRoyaltyViewModelBuilder.Build(iGotShippingMethodEvaluation);

            return View(tvForm);
        }

        [HttpPost]
        [ActionName("Index")]
        public ActionResult Validate(TvRoyaltyFormViewModel tvForm)
        {
            var popOrderForm = _opContext.OF as PopOrderForm;

            if (!ModelState.IsValid)
            {
                return View("Index", _tvRoyaltyViewModelBuilder.Build(tvForm));
            }

            var normalizedAddress = _tvRoyaltyBusiness.NormalizeAddress(tvForm.StreetAndNumber);
            
            var royaltyData = popOrderForm.GetPrivateData<TelevisualRoyaltyData>();
            var tvRoyalty = royaltyData.Value ?? new TVRoyalty();
            tvRoyalty.Gender = tvForm.Civility.ToString();
            tvRoyalty.LastName = tvForm.LastName;
            tvRoyalty.FirstName = tvForm.FirstName;
            tvRoyalty.StreetNo = normalizedAddress?.Num.ToString();
            tvRoyalty.StreetRange = normalizedAddress?.Btq;
            tvRoyalty.StreetType = normalizedAddress?.StreetTypeCode;
            tvRoyalty.AddressLine1 = normalizedAddress?.StreetName;
            tvRoyalty.AddressLine2 = tvForm.OptionalAddress;
            tvRoyalty.ZipCode = tvForm.PostalCode;
            tvRoyalty.City = tvForm.City;
            tvRoyalty.TVUse = tvForm.TerritoryOfUse.ToString();
            tvRoyalty.TVUse2 = tvForm.TvUse.ToString();
            tvRoyalty.BirthLocation = tvForm.BirthPlace;
            tvRoyalty.BirthState = tvForm.BirthDepartment;
            tvRoyalty.DOBYear = tvForm.BirthYear.ToString();
            tvRoyalty.DOBMonth = string.Format("{0:00}", tvForm.BirthMonth); // Le format officiel doit être une longueur de 2 paddé avec un zéro
            tvRoyalty.DOBDay = string.Format("{0:00}", tvForm.BirthDay);
            royaltyData.Value = tvRoyalty;

            var popData = popOrderForm.GetPrivateData<PopData>();
            popData.DisplayTvRoyaltyValidated = true;

            return Continue();
        }

    }
}
