using System;

namespace FnacDirect.OrderPipe.FDV.Model
{
    /// <summary>
    /// represente le formulaire principal du rapport
    /// </summary>
    [Serializable]
    public class PropCommForm
    {
        public string CurrentLiteralDateTime
        {
            get
            {
                return DateTime.Now.ToString("dd/MM/yyyy");
            }
        }

        public string OrderDate
        {
            get;
            set;
        } = string.Empty;

        #region customer info

        public string CustomerCompany
        {
            get;
            set;
        } = string.Empty;

        public string CustomerFirstName
        {
            get;
            set;
        } = string.Empty;

        public string CustomerLastName
        {
            get;
            set;
        } = string.Empty;

        public string CustomerFullName
        {
            get { return $"{CustomerFirstName} {CustomerLastName}"; }
        }

        public string CustomerNumber
        {
            get;
            set;
        } = string.Empty;

        public string CustomerZipCodeAndCity
        {
            get { return $"{CustomerZipCode} {CustomerCity}"; }
        }

        public string CustomerZipCode
        {
            get;
            set;
        } = string.Empty;

        public string CustomerAdress
        {
            get;
            set;
        } = string.Empty;

        public string CustomerCity
        {
            get;
            set;
        } = string.Empty;

        #endregion

        #region Store info

        public string StoreCode
        {
            get;
            set;
        } = string.Empty;

        public string StoreName
        {
            get;
            set;
        } = string.Empty;

        public string StoreAddress
        {
            get;
            set;
        } = string.Empty;

        public string StoreZipCode
        {
            get;
            set;
        } = string.Empty;

        public string StoreZipCodeAndCity
        {
            get { return $"{StoreZipCode} {StoreCity}"; }
        }

        public string StoreCity
        {
            get;
            set;
        } = string.Empty;

        public string StorePhone
        {
            get;
            set;
        } = string.Empty;

        public string StoreFax
        {
            get;
            set;
        } = string.Empty;

        public string StoreContacts
        {
            get { return $"{StorePhone} {(string.IsNullOrEmpty(StoreFax) ? string.Empty : $"-{StoreFax}")}"; }
        }

        #region LegalEntity

        public string StoreLegalEntity
        {
            get;
            set;
        } = string.Empty;

        public string StoreLegalEntityLegalForm
        {
            get;
            set;
        } = string.Empty;

        public string StoreLegalEntityCapital
        {
            get;
            set;
        } = string.Empty;

        public string StoreLegalCapitalInfo
        {
            get { return string.IsNullOrEmpty(StoreLegalEntityCapital) ? string.Empty : $"au capital de {StoreLegalEntityCapital}"; }
        }

        public string StoreLegalEntityAddressInfo
        {
            get { return string.IsNullOrEmpty(StoreLegalEntityAddress) ? string.Empty : $"Siège social : {StoreLegalEntityAddress}"; }
        }

        public string StoreLegalEntityRCS
        {
            get;
            set;
        } = string.Empty;

        public string StoreLegalEntityTVA
        {
            get;
            set;
        } = string.Empty;

        public string StoreLegalEntityAddress
        {
            get;
            set;
        } = string.Empty;

        public string StoreLegalEntityTVAInfo
        {
            get { return string.IsNullOrEmpty(StoreLegalEntityTVA) ? string.Empty : $"Identifiant TVA : {StoreLegalEntityTVA}"; }
        }

        public string StoreLegalEntitySiret
        {
            get;
            set;
        } = string.Empty;

        #endregion

        #endregion

        #region commercial

        public string CommercialFirstName
        {
            get;
            set;
        } = string.Empty;

        public string CommercialLastName
        {
            get;
            set;
        } = string.Empty;

        public string CommercialFullName
        {
            get { return $"Interlocuteur Fnac : {CommercialFirstName} {CommercialLastName}"; }
        }

        public string Comments
        {
            get;
            set;
        } = string.Empty;

        #endregion

        #region totals

        public decimal TotalShippingCost
        {
            get;
            set;
        }

        public decimal TotalShippingCostHT
        {
            get;
            set;
        }
        public decimal TotalPriceHT
        {
            get;
            set;
        }

        public decimal TotalPriceTTC
        {
            get;
            set;
        }

        public decimal TotalPriceTTCIncludeShippingCost
        {
            get { return TotalShippingCost + TotalPriceTTC; }
        }

        public decimal TotalPriceHTIncludeShippingCostHT
        {
            get { return TotalShippingCostHT + TotalPriceHT; }
        }
        #endregion
    }
}
