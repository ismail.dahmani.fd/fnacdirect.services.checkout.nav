﻿using System.Xml.Serialization;

namespace FnacDirect.OrderPipe.Base.Model
{
    /// <summary>
    /// Ensemble d'informations liées à l'éligibilité d'un client donné (private data).
    /// </summary>
    public class DeferredPaymentEligibilityData : GroupData
    {
        /// <summary>
        /// Obtient ou définit si le paiement différé est activé pour le client.
        /// </summary>
        [XmlIgnore]
        public bool IsEligible { get; set; }

        /// <summary>
        /// Obtient ou définit le délai du paiement différé en nombre de jours.
        /// </summary>
        [XmlIgnore]
        public int PaymentDelay { get; set; }

        /// <summary>
        /// Obtient ou définit le N° de paiement différé du client.
        /// </summary>
        [XmlIgnore]
        public string DeferredAccountNumber { get; set; }

        /// <summary>
        /// Obtient ou définit une valeur déterminant si les données d'éligibilité au paiement différé ont été chargées.
        /// </summary>
        [XmlIgnore]
        public bool IsComputed { get; set; }
    }
}
