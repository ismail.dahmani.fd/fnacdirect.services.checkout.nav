using FnacDarty.AddressValidator.Models;
using FnacDirect.Contracts.Online.Model;
using System.Collections.Generic;

namespace FnacDirect.OrderPipe.Base.Business.Shipping.Postal
{
    public class CapencyResult
    {
        public CapencyResult(AddressValidityType addressValidityType, params IAddress[] addresses)
        {
            ResultType = addressValidityType;
            Addresses = addresses;
        }

        public AddressValidityType ResultType { get; private set; }

        public IEnumerable<IAddress> Addresses { get; private set; }
        public AddressValidityResult GetAddressValidityResult()
        {
            var result = new AddressValidityResult();
            switch (ResultType)
            {
                case AddressValidityType.Valid:
                    result.AddressValidityResultType = AddressValidityResultType.Valid;
                    break;
                case AddressValidityType.Revised:
                case AddressValidityType.FoundToReCheck:
                case AddressValidityType.ToReCheck:
                    result.AddressValidityResultType = AddressValidityResultType.Recheck;
                    result.Addresses = Addresses;
                    break;
                case AddressValidityType.StreetNumberToReCheck:
                case AddressValidityType.Truncated:
                case AddressValidityType.Invalid:
                    result.AddressValidityResultType = AddressValidityResultType.Invalid;
                    break;
            }
            return result;
        }
    }
}
