using FnacDirect.OrderPipe.Base.Business.Helpers;
using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.Quote.Model.Enums;
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace FnacDirect.Quote.Model
{

    [Serializable]
    public class QuoteLineDetailCollection : List<QuoteLineDetail>
    {
        #region Fields

        private List<object> _pageAnchorList;
        private int pageSize = 5;
        private int _recordsCount;
        private decimal quoteDetailWrapPrice;

        #endregion

        #region Constructor

        public QuoteLineDetailCollection()
        {
            PageAnchorList = new List<object>();
            ShippingModeGlobalValues = new List<ShippingModeValue>();
        }

        #endregion

        #region Poperties

        [XmlIgnore()]
        private decimal quoteDetailaddressShippPrice;

        /// <summary>
        /// frais de livraison  de la ligne adresse
        /// </summary>
        [XmlIgnore()]
        public decimal QuoteDetailaddressShipPrice
        {
            get { return quoteDetailaddressShippPrice; }
            set { quoteDetailaddressShippPrice = value; }
        }

        public decimal QuoteDetailWrapPrice
        {
            get { return quoteDetailWrapPrice; }
            set { quoteDetailWrapPrice = value; }
        }

        [XmlIgnore()]
        public int PageSize
        {
            get { return pageSize; }
            set { pageSize = value; }
        }

        private List<ShippingModeValue> _shippingModeGlobalValues;
        [XmlIgnore()]
        public List<ShippingModeValue> ShippingModeGlobalValues
        {
            get { return _shippingModeGlobalValues; }
            set { _shippingModeGlobalValues = value; }
        }

        [XmlIgnore()]
        public int RecordsCount
        {
            get { return _recordsCount; }
            set
            {
                _recordsCount = value;

                if (_recordsCount > 0 && pageSize > 0)
                {
                    PageAnchorList = new List<object>();

                    var pageNumber = ((_recordsCount - (_recordsCount % pageSize)) / pageSize) + (_recordsCount % pageSize == 0 ? 0 : 1);

                    for (var i = 1; i <= pageNumber; i++)
                    {
                        PageAnchorList.Add((object)new KeyValuePair<int, int>(i, i));
                    }
                }
            }
        }

        public static QuoteLineDetailCollection BuildCollection(QuoteLineDetail item)
        {
            var coll = new QuoteLineDetailCollection
            {
                item
            };

            return coll;
        }

        [XmlIgnore()]
        public QuoteLineDetailAdressesCollection StandardQuoteLinesByAdress
        {
            get
            {
                var addressID = -1;
                var addressColl = new QuoteLineDetailCollection();
                var standardQuoteLinesByAdress = new QuoteLineDetailAdressesCollection();
                var coll = StandardQuoteLines;
                var resultColl = Utilities.SortableList<QuoteLineDetail>.OrderBy(coll, "AddressID", true);

                foreach (var standardQuoteLine in resultColl)
                {
                    if (addressID != standardQuoteLine.AddressID)
                    {
                        addressColl = new QuoteLineDetailCollection
                        {
                            AddressId = standardQuoteLine.AddressID,
                            AddressLabel = standardQuoteLine.AddressLabel
                        };
                        addressID = standardQuoteLine.AddressID;
                        addressColl.Add(standardQuoteLine);
                    }

                    if (!standardQuoteLinesByAdress.Contains(addressColl))
                    {
                        standardQuoteLinesByAdress.Add(addressColl);
                        standardQuoteLinesByAdress.AddressId = standardQuoteLine.AddressID;
                        standardQuoteLinesByAdress.AddressLabel = standardQuoteLine.AddressLabel;
                    }
                    else if (addressID == standardQuoteLine.AddressID)
                    {
                        standardQuoteLinesByAdress[standardQuoteLinesByAdress.IndexOf(addressColl)].Add(standardQuoteLine);

                    }

                    if (standardQuoteLine.IsAvailable.HasValue && (bool)(!standardQuoteLine.IsAvailable))
                    {
                        if (!addressColl.CompleteDisponibility)
                        {
                            addressColl.CompleteDisponibility = false;
                            standardQuoteLinesByAdress.CompleteDisponibility = false;
                        }
                    }

                }

                return standardQuoteLinesByAdress;
            }
        }

        [XmlIgnore()]
        public bool CompleteDisponibility
        {
            get { return _completeDisponibility; }
            set { _completeDisponibility = value; }
        }

        [XmlIgnore()]
        public QuoteLineDetailAdressesCollection CCvQuoteLinesByAdress
        {
            get
            {
                var addressID = -1;
                var addressColl = new QuoteLineDetailCollection();
                var ccvCollectionByAdresses = new QuoteLineDetailAdressesCollection();

                foreach (var standardQuoteLine in CCvQuoteLines)
                {
                    if (addressID != standardQuoteLine.AddressID)
                    {
                        addressColl = new QuoteLineDetailCollection
                        {
                            AddressId = standardQuoteLine.AddressID,
                            AddressLabel = standardQuoteLine.AddressLabel
                        };
                        addressID = standardQuoteLine.AddressID;
                        addressColl.Add(standardQuoteLine);

                        if (!ccvCollectionByAdresses.Contains(addressColl))
                        {
                            ccvCollectionByAdresses.Add(addressColl);
                        }
                    }
                    else if (addressID == standardQuoteLine.AddressID)
                    {
                        ccvCollectionByAdresses[ccvCollectionByAdresses.IndexOf(addressColl)].Add(standardQuoteLine);

                    }
                }

                return ccvCollectionByAdresses;
            }
        }

        [XmlIgnore()]
        public QuoteLineDetailCollection StandardQuoteLines
        {
            get
            {
                var result = new QuoteLineDetailCollection();

                result.AddRange(FindAll(delegate (QuoteLineDetail d) { return d.QuoteArticleType == QuoteArticleTypeEnum.Other; }));

                return result;
            }
        }

        [XmlIgnore()]
        public QuoteLineDetailCollection CCvQuoteLines
        {
            get
            {
                var result = new QuoteLineDetailCollection();

                result.AddRange(FindAll(delegate (QuoteLineDetail d) { return d.QuoteArticleType == QuoteArticleTypeEnum.CCV; }));

                return result;
            }
        }

        private int _addressID;
        private string _addressLabel;

        public int AddressId
        {
            get { return _addressID; }
            set { _addressID = value; }
        }

        [XmlIgnore()]
        public string AddressLabel
        {
            get { return _addressLabel; }
            set { _addressLabel = value; }
        }

        private decimal _volume;

        [XmlIgnore()]
        public decimal Volume
        {
            get { return _volume; }
            set { _volume = value; }
        }

        private decimal _quoteDetailaddressWrapPrice;
        private bool _completeDisponibility = true;

        /// <summary>
        /// frais d'emballage de la ligne adresse
        /// </summary>
        public decimal QuoteDetailaddressWrapPrice
        {
            get { return _quoteDetailaddressWrapPrice; }
            set { _quoteDetailaddressWrapPrice = value; }
        }

        private decimal quoteDetailaddressWrapPriceExcTVA;

        /// <summary>
        /// frais d'emballage de la ligne adresse
        /// </summary>
        public decimal QuoteDetailaddressWrapPriceExcTVA
        {
            get { return quoteDetailaddressWrapPriceExcTVA; }
            set { quoteDetailaddressWrapPriceExcTVA = value; }
        }

        [XmlIgnore()]
        public List<object> PageAnchorList
        {
            get { return _pageAnchorList; }
            set { _pageAnchorList = value; }
        }

        #endregion

        #region Methods

        public void Add(QuoteLineDetailCollection quoteLineDetailCollection)
        {
            foreach (var quoteLineDetail in quoteLineDetailCollection)
            {
                Add(quoteLineDetail);
            }
        }
        public void AddEntity(QuoteLineDetail entity)
        {
            var ind = -1;

            foreach (var quoteLineDetail in this)
            {
                if (quoteLineDetail.QuoteDetailID == entity.QuoteDetailID)
                {
                    ind = IndexOf(quoteLineDetail);
                }
            }

            switch (entity.Quantity)
            {
                case -1:
                    this[ind].Quantity--;
                    if (this[ind].Quantity == 0)
                    {
                        RemoveById(entity);
                    }
                    break;
                default:

                    if (ind == -1)
                    {
                        Add(entity);
                        break;
                    }

                    this[ind].Quantity++;
                    break;
            }
        }

        public void ClearRemise()
        {
            foreach (var quoteLineDetail in this)
            {
                quoteLineDetail.Remises.Clear();
            }
        }

        public void RemoveById(QuoteLineDetail deletedLine)
        {
            var todeleteIndex = -1;

            foreach (var quoteLineDetail in this)
            {
                if (quoteLineDetail.QuoteDetailID == deletedLine.QuoteDetailID)
                {
                    todeleteIndex = IndexOf(quoteLineDetail);
                }
            }
            // fix of delete from interne in list 
            if (todeleteIndex != -1)
            {
                RemoveAt(todeleteIndex);
            }
        }

        /// <summary>
        /// retourne le shippingGlobalPrice (b) maximum de la l'ancienne collection
        /// </summary>
        /// <returns>le quotelinedetail qui a le b maximum</returns>
        public QuoteLineDetail GetMaxOldGlobalShippingPrice(int idAddress)
        {
            QuoteLineDetail retour = null;
            decimal maxQuoteGlobalOldShippingPrice = 0;

            foreach (var quoteLineDetail in this)
            {
                if (((quoteLineDetail.AddressID == idAddress) &&
                    (quoteLineDetail.QuoteShippingModeValue.ShippingModeExcTaxValueOld >= maxQuoteGlobalOldShippingPrice))
                    || (retour == null))
                {
                    maxQuoteGlobalOldShippingPrice = quoteLineDetail.QuoteShippingModeValue.ShippingModeExcTaxValueOld;
                    retour = quoteLineDetail;
                }
            }

            return retour;
        }

        /// <summary>
        /// retourne le shippingGlobalPrice (b) maximum de la nouvelle collection
        /// </summary>
        /// <returns>le quotelinedetail qui a le b maximum</returns>
        public QuoteLineDetail GetMaxNewGlobalShippingPrice(int idAddress)
        {
            QuoteLineDetail retour = null;
            decimal max = 0;

            foreach (var quoteLineDetail in this)
            {
                if (((quoteLineDetail.AddressID == idAddress) &&
                    (quoteLineDetail.QuoteShippingModeValue.ShippingModeExcTaxValue >= max))
                    || (retour == null))
                {
                    max = quoteLineDetail.QuoteShippingModeValue.ShippingModeExcTaxValue;
                    retour = quoteLineDetail;
                }
            }

            return retour;
        }


        /// <summary>
        /// renvoit le prix de l'emballage de la collection 
        /// (il n'y a d'emballage que si il y a au moins un produit éditorial et la commande inférieure à 1.5 m^3)
        /// </summary>
        /// <returns>la valeur du prix de l'emballage</returns>
        public decimal GetWrapPrice()
        {
            decimal retour = 0;

            foreach (var quoteLineDetail in this)
            {
                if (quoteLineDetail.QuoteWrapPrice.HasValue && quoteLineDetail.QuoteWrapPrice.Value > 0)
                {
                    retour = quoteLineDetail.QuoteWrapPrice.Value;
                    break;
                }
            }

            return retour;
        }

        #endregion
    }

    public class QuoteLineDetailAdressesCollection : List<QuoteLineDetailCollection>
    {
        private int _addressId;
        private string _addressLabel;

        public int AddressId
        {
            get { return _addressId; }
            set { _addressId = value; }
        }

        private bool completeDisponibility = true;
        public bool CompleteDisponibility
        {
            get { return completeDisponibility; }
            set { completeDisponibility = value; }
        }

        private decimal volume;

        public decimal Volume
        {
            get { return volume; }
            set { volume = value; }
        }

        public string AddressLabel
        {
            get { return _addressLabel; }
            set { _addressLabel = value; }
        }

        private decimal quoteDetailaddressWrapPrice;

        /// <summary>
        /// frais d'emballage de la ligne adresse
        /// </summary>
        public decimal QuoteDetailaddressWrapPrice
        {
            get { return quoteDetailaddressWrapPrice; }
            set { quoteDetailaddressWrapPrice = value; }
        }

        private decimal quoteDetailaddressWrapPriceExcTVA;

        /// <summary>
        /// frais d'emballage de la ligne adresse
        /// </summary>
        public decimal QuoteDetailaddressWrapPriceExcTVA
        {
            get { return quoteDetailaddressWrapPriceExcTVA; }
            set { quoteDetailaddressWrapPriceExcTVA = value; }
        }

        public QuoteLineDetailAdressesCollection CalculateQuoteOf(QuoteForm quoteForm)
        {
            return UpdateLineGroupWithInfo(quoteForm, this);
        }

        public QuoteLineDetailAdressesCollection UpdateLineGroupWithInfo(QuoteForm quoteForm, QuoteLineDetailAdressesCollection addressGroupCollection)
        {
            #region calculate volume

            foreach (var addressColl in addressGroupCollection)
            {
                addressColl.QuoteDetailaddressWrapPrice = 0;
                addressColl.QuoteDetailaddressWrapPriceExcTVA = 0;

                foreach (var quoteFormLineGroup in quoteForm.LineGroups)
                {
                    if (quoteFormLineGroup.ShippingAddress.Identity == addressColl.AddressId)
                    {
                        addressColl.Volume = quoteFormLineGroup.Volume;

                        // frais d'emballage 
                        if (quoteFormLineGroup.OrderInfo.ChosenWrapMethod != 0)
                        {
                            if (quoteFormLineGroup.AvailableWraps != null)
                            {
                                foreach (var availableWrap in quoteFormLineGroup.AvailableWraps)
                                {
                                    if (quoteFormLineGroup.OrderInfo.ChosenWrapMethod == availableWrap.WrapId)
                                    {
                                        addressColl.QuoteDetailaddressWrapPrice += availableWrap.TotalUserPrice;
                                    }
                                }
                            }
                        }

                        addressColl.QuoteDetailaddressShipPrice = quoteFormLineGroup.GlobalPrices.TotalShipPriceUserEurNoVAT.GetValueOrDefault(0);
                    }
                }

                if (addressColl.Count > 0)
                {
                    // le frais d'emballage de l'adresse est dans chaque article
                    // du coup, il faut juste prendre le premier article
                    addressColl.QuoteDetailaddressWrapPriceExcTVA = addressColl[0].QuoteWrapPriceNoVAT.GetValueOrDefault(0);
                }
            }

            #endregion

            #region calculate frais de livraison par adresses

            foreach (var quoteLineDetailCollection in addressGroupCollection)
            {
                foreach (var lineGroup in quoteForm.LineGroups)
                {
                    if (quoteLineDetailCollection.AddressId == lineGroup.ShippingAddress.Identity)
                    {
                        if (null == lineGroup.ShippingModeValueCollection)
                        {
                            lineGroup.ShippingModeValueCollection = new List<ShippingModeValue>();
                        }

                        foreach (var sp in lineGroup.ShippingModeValueCollection)
                        {
                            if (!quoteLineDetailCollection.ShippingModeGlobalValues.Contains(sp))
                            {
                                quoteLineDetailCollection.ShippingModeGlobalValues.Add(sp);
                            }
                        }
                        break;
                    }
                }
            }

            #endregion

            //TODO : shippingMethods global
            //
            // calculé à partir des detail 
            //addressColl.ShippingModeGlobalValues 

            return addressGroupCollection;
        }

        #region not used

        //private List<ShippingModeValue> GetQuoteFormShippingModeValuesCollection(int addressId, List<ShippingModeValueCollection> shippingModeValuesCollection)
        //{
        //    bool existing = false;
        //    ShippingModeValueCollection colltofind = null;
        //    foreach (ShippingModeValueCollection shippingModeValueCollection in shippingModeValuesCollection)
        //    {
        //        if (shippingModeValueCollection.AdressId == addressId)
        //        {
        //            existing = true;
        //            colltofind = shippingModeValueCollection;
        //            break;
        //        }
        //    }
        //    if (colltofind == null)
        //        colltofind = new ShippingModeValueCollection();

        //    return colltofind;
        //}

        #endregion
    }

    [Serializable]
    public class QuoteLineService : Service
    {
        #region Members

        [OptionalField]
        private decimal m_unitPriceExclTax;

        #endregion Members

        /// <summary>
        /// l'identifiant unique du parent dans le cas d'un service ou de produit attahcé 
        /// </summary>
        public Guid QuoteDetailParentId
        {
            get { return quoteDetailParentID; }
            set { quoteDetailParentID = value; }
        }

        private Guid quoteDetailParentID;

        /// <summary>
        /// Gets or sets service unit price with not VAT 
        /// </summary>

        public decimal UnitPriceExclTax { get { return m_unitPriceExclTax; } set { m_unitPriceExclTax = value; } }
    }


    [Serializable]
    public class QuoteLineDetail : QuoteArticle
    {
        #region fields

        private string _quoteDetailArticleReference;
        private string _quoteDetailArticleLabel;
        private int _quoteDetailQuantity;
        /// <summary>
        /// Prix unitaire HT
        /// </summary>
        private decimal _quoteDetailUnitPriceExclTax;

        private decimal _quoteDetailVAT;
        /// <summary>
        /// Prix total HT
        /// </summary>
        private decimal _quoteDetailTotalPriceExclTax;
        private decimal _quoteDetailUnitDiscountAmountExclTax;
        /// <summary>
        /// total de Remise HT
        /// </summary>
        private decimal _quoteDetailTotalDiscountAmountExclTax;
        private int _quoteId;
        private Guid _quoteDetailId;

        /// <summary>
        /// frai livraison HT
        /// </summary>
        private decimal _quoteDetailShippingPriceExclTax;
        private int _indexer;
        private QuoteArticleTypeEnum _quoteArticleType;
        private QuotePostalAddress _addressOnCreation;
        private bool _hasEcoTaxPrice;

        [XmlIgnore()]
        public bool HasEcoTaxPrice
        {
            get
            {
                _hasEcoTaxPrice = EcoTaxPrice > 0;

                return _hasEcoTaxPrice;
            }
            set { _hasEcoTaxPrice = value; }
        }

        /// <summary>
        /// le prix ht discounté 
        /// </summary>
        public decimal? DiscountPriceUserEur
        {
            get
            {
                //calcul du nouveau prix apres discount
                return QuoteDetailOldUnitPriceExclTax - QuoteDetailDiscountAmount;
            }
        }

        /// <summary>
        /// le prix ttc discounté 
        /// </summary>
        [XmlIgnore]
        public decimal? DiscountPriceUserEurTTC
        {
            get
            {
                //calcul du nouveau prix apres discount
                return PriceHelpers.GetPriceTTC(DiscountPriceUserEur, VATRate);
            }
        }

        /// <summary>
        /// Eco Tax
        /// </summary>
        private decimal ecoTaxPrice;
        private decimal ecoTaxPriceNoVAT;
        private decimal ecoTaxTotalPrice;
        private decimal ecoTaxTotalPriceNoVAT;

        /// <summary>
        /// PMP : prix moyen pondéré ; à recupérer de la base STOCK 
        /// </summary>
        /// 
        public decimal QuoteDetailPmpArticle
        {
            get { return quoteDetailPmpArticle; }
            set { quoteDetailPmpArticle = value; }
        }

        /// <summary>
        /// Ecotax global par ligne 
        /// </summary>
        public decimal EcoTaxTotalPrice
        {
            get
            {
                ecoTaxTotalPrice = (decimal)(ecoTaxPrice * Quantity ?? 0);

                return ecoTaxTotalPrice;
            }
            set { ecoTaxTotalPrice = value; }
        }

        /// <summary>
        /// Ecotax global par ligne sans TVA
        /// </summary>
        public decimal EcoTaxTotalPriceNoVAT
        {
            get
            {
                ecoTaxTotalPriceNoVAT = (decimal)(ecoTaxPriceNoVAT * Quantity ?? 0);

                return ecoTaxTotalPriceNoVAT;
            }
            set { ecoTaxTotalPriceNoVAT = value; }
        }

        /// <summary>
        /// ecotax de la ligne du devis 
        /// </summary>
        public decimal EcoTaxPrice
        {
            get
            {
                if (EcoTaxEur != null)
                {
                    ecoTaxPrice = (decimal)EcoTaxEur;
                }

                return ecoTaxPrice;
            }
            set { ecoTaxPrice = value; }
        }

        /// <summary>
        /// ecotax de la ligne du devis sans la TVA
        /// </summary>
        public decimal EcoTaxPriceNoVAT
        {
            get
            {
                if (EcoTaxEurNoVAT != null)
                {
                    ecoTaxPriceNoVAT = (decimal)EcoTaxEurNoVAT;
                }

                return ecoTaxPriceNoVAT;
            }
            set { ecoTaxPriceNoVAT = value; }
        }

        #endregion

        #region Constructor

        public QuoteLineDetail()
        {
            QuoteDetailID = Guid.NewGuid();
        }

        #endregion

        #region Properties

        /// <summary>
        /// article TVA
        /// </summary>
        public decimal QuoteDetailVAT
        {
            get { return _quoteDetailVAT; }
            set { _quoteDetailVAT = value; }
        }

        /// <summary>
        /// article Reference 
        /// </summary>
        public string QuoteDetailArticleReference
        {
            get { return _quoteDetailArticleReference; }
            set { _quoteDetailArticleReference = value; }
        }

        /// <summary>
        /// article Label
        /// </summary>
        public string QuoteDetailArticleLabel
        {
            get { return _quoteDetailArticleLabel; }
            set { _quoteDetailArticleLabel = value; }
        }

        /// <summary>
        /// l'identifiant unique du parent dans le cas d'un service ou de produit attahcé 
        /// </summary>
        public Guid QuoteDetailParentId
        {
            get { return quoteDetailParentID; }
            set { quoteDetailParentID = value; }
        }

        private Guid quoteDetailParentID;

        /// <summary>
        /// quantité 
        /// </summary>
        public int QuoteDetailQuantity
        {
            get { return _quoteDetailQuantity; }
            set { _quoteDetailQuantity = value; }
        }

        /// <summary>
        /// Pris de la ligne de devis
        /// </summary>
        public decimal QuoteDetailUnitPriceExclTax
        {
            get
            {
                //on le récupère de l'ancien prix uniquement lors du premier passage
                if (_quoteDetailUnitPriceExclTax == 0)
                {
                    _quoteDetailUnitPriceExclTax = QuoteDetailOldUnitPriceExclTax;
                }
                else
                {
                    _quoteDetailUnitPriceExclTax = QuoteDetailOldUnitPriceExclTax - QuoteDetailDiscountAmount;
                }

                return _quoteDetailUnitPriceExclTax;
            }
            set { _quoteDetailUnitPriceExclTax = value; }
        }

        /// <summary>
        /// Pris de base (de depart) de la ligne de devis
        /// </summary>
        public decimal QuoteDetailOldUnitPriceExclTax
        {
            get { return PriceNoVATEur.GetValueOrDefault(); }
        }

        /// <summary>
        /// Valuer total prix de base * qantité 
        /// </summary>
        public decimal QuoteDetailTotalPriceExclTax
        {
            get
            {
                //_quoteDetailTotalPriceExclTax = (decimal)(QuoteDetailUnitPriceExclTax * QuoteDetailQuantity);
                _quoteDetailTotalPriceExclTax = (decimal)(QuoteDetailOldUnitPriceExclTax * Quantity ?? 0);

                return _quoteDetailTotalPriceExclTax;
            }
            set { _quoteDetailTotalPriceExclTax = value; }
        }

        /// <summary>
        /// Valuer total prix de base * qantité 
        /// </summary>
        public decimal QuoteDetailOldTotalPriceExclTax
        {
            get { return QuoteDetailOldUnitPriceExclTax * QuoteDetailQuantity; }
        }

        /// <summary>
        /// Valeur validée du dsicount Amount
        /// </summary>
        public decimal QuoteDetailUnitDiscountAmountExclTax
        {
            get { return _quoteDetailUnitDiscountAmountExclTax; }
            set { _quoteDetailUnitDiscountAmountExclTax = value; }
        }
        /// <summary>
        /// Valeur totale du dscount Amount validé 
        /// </summary>
        public decimal QuoteDetailTotalDiscountAmountExclTax
        {
            get
            {
                _quoteDetailTotalDiscountAmountExclTax = (decimal)(QuoteDetailDiscountAmount * Quantity);

                return _quoteDetailTotalDiscountAmountExclTax;
            }
            set
            {
                _quoteDetailTotalDiscountAmountExclTax = value;
            }
        }

        private decimal _subTotalTaxEclAfterDiscount;
        /// <summary>
        /// Total de la ligne apres application de la remise 
        /// </summary>
        public decimal SubTotalTaxEclAfterDiscount
        {
            get
            {
                _subTotalTaxEclAfterDiscount = QuoteDetailOldTotalPriceExclTax * (1 - QuoteDetailDiscountPercentage / 100);

                return _subTotalTaxEclAfterDiscount;
            }
            set
            {
                _subTotalTaxEclAfterDiscount = value;
            }
        }

        /// <summary>
        /// L'identifiant du devis 
        /// </summary>
        [XmlIgnore()]
        public int QuoteID
        {
            get { return _quoteId; }
            set { _quoteId = value; }
        }

        /// <summary>
        /// Identifiant unique de la ligne pour le devis 
        /// </summary>
        public Guid QuoteDetailID
        {
            get { return _quoteDetailId; }
            set { _quoteDetailId = value; }
        }

        private DateTime? _quoteDetailShippingTheoricDate;
        /// <summary>
        /// Date théorique de livraison 
        /// </summary>
        public DateTime? QuoteDetailShippingTheoricDate
        {
            get { return _quoteDetailShippingTheoricDate; }
            set { _quoteDetailShippingTheoricDate = value; }
        }

        [XmlIgnore]
        private string _quoteDetailShippingTheoreticalDateLabel;
        /// <summary>
        /// Date théorique de livraison
        /// </summary>
        [XmlIgnore]
        public string QuoteDetailShippingTheoreticalDateLabel
        {
            get { return _quoteDetailShippingTheoreticalDateLabel; }
            set { _quoteDetailShippingTheoreticalDateLabel = value; }
        }

        /// <summary>
        /// Label Etat de disponibilité de l'article de alligne
        /// </summary>
        public string QuoteDetailArticleAvailability
        {
            get { return AvailabilityLabel; }
            set { AvailabilityLabel = value; }
        }

        private int? _quoteDetailArticleAvailabilityID;
        /// <summary>
        /// Id de l'etat de disponibilité
        /// </summary>
        public int? QuoteDetailArticleAvailabilityID
        {
            get { return _quoteDetailArticleAvailabilityID; }
            set { _quoteDetailArticleAvailabilityID = value; }
        }

        private int? _quoteDetailArticleWrapId;
        /// <summary>
        /// Id de l'emballage cadeau
        /// </summary>
        public int? QuoteDetailArticleWrapId
        {
            get { return _quoteDetailArticleWrapId; }
            set { _quoteDetailArticleWrapId = value; }
        }

        #region a

        public decimal QuoteDetailShippingPrice(decimal vatRatePipe)
        {
            return PriceHelpers.GetPriceTTC(_quoteDetailShippingPriceExclTax, vatRatePipe);
        }

        public decimal QuoteDetailShippingPriceExclTax
        {
            get { return _quoteDetailShippingPriceExclTax; }
            set { _quoteDetailShippingPriceExclTax = value; }
        }

        private decimal _quoteDetailOldShippingPrice;
        public decimal QuoteDetailOldShippingPrice
        {
            get { return _quoteDetailOldShippingPrice; }
            set { _quoteDetailOldShippingPrice = value; }
        }

        private decimal _quoteDetailOldShippingPriceExclTax;
        /// <summary>
        /// Frais de livraison initial hors taxe
        /// </summary>
        public decimal QuoteDetailOldShippingPriceExclTax
        {
            get { return _quoteDetailOldShippingPriceExclTax; }
            set { _quoteDetailOldShippingPriceExclTax = value; }
        }

        #endregion a

        private ShippingModeValue _QuoteShippingModeValue = null;

        public ShippingModeValue QuoteShippingModeValue
        {
            get { return _QuoteShippingModeValue; }
            set { _QuoteShippingModeValue = value; }
        }

        private decimal? _quoteWrapPrice;

        public decimal? QuoteWrapPrice
        {
            get { return _quoteWrapPrice; }
            set { _quoteWrapPrice = value; }
        }

        private decimal? _quoteWrapPriceNoVAT;

        public decimal? QuoteWrapPriceNoVAT
        {
            get { return _quoteWrapPriceNoVAT; }
            set { _quoteWrapPriceNoVAT = value; }
        }

        [XmlIgnore()]
        public int Indexer
        {
            get { return _indexer; }
            set { _indexer = value; }
        }
        [XmlElement("TypeId")]
        public new int? TypeId
        {
            get { return base.TypeId; }
            set { base.TypeId = value; }
        }

        public QuoteArticleTypeEnum QuoteArticleType
        {
            get { return _quoteArticleType; }
            set { _quoteArticleType = value; }
        }

        [XmlArray]
        public QuoteLinePromotions QuoteLinePromotions { get; set; }

        #endregion Properties

        #region added for edit Discount

        private decimal quoteDetailOpcDiscountAmount;
        private decimal quoteDetailOpcDiscountPercentage;
        private decimal quoteDetailDiscountAmount;
        private decimal quoteDetailPmpArticle;
        private ModeViewEnum _modeView;
        private string _quoteComments;
        private int _addressId;
        private string _addressLabel;

        public QuoteLineDetail(string quoteDetailId, int i)
        {
            QuoteDetailID = new Guid(quoteDetailId);
            Quantity = i;
        }

        private decimal quoteDetailWrapPrice;
        private bool _quoteDetailDicountValidation = true;

        /// <summary>
        /// frais d'emballage de la ligne
        /// </summary>
        public decimal QuoteDetailWrapPrice
        {
            get { return quoteDetailWrapPrice; }
            set { quoteDetailWrapPrice = value; }
        }

        /// <summary>
        /// Remise en amount sur l'article de aligne 
        /// </summary>
        public decimal QuoteDetailOpcDiscountAmount
        {
            get { return quoteDetailOpcDiscountAmount; }
            set { quoteDetailOpcDiscountAmount = value; }
        }

        /// <summary>
        /// Remise OPC sur l'article de la ligne
        /// </summary>
        public decimal QuoteDetailOpcDiscountPercentage
        {
            get { return quoteDetailOpcDiscountPercentage; }
            set { quoteDetailOpcDiscountPercentage = value; }
        }

        /// <summary>
        /// Remise en amount sur la ligne
        /// </summary>
        public decimal QuoteDetailDiscountAmount
        {
            get { return quoteDetailDiscountAmount; }
            set { quoteDetailDiscountAmount = value; }
        }

        /// <summary>
        /// Pourcentage de la remise sur la ligne
        /// </summary>
        public decimal QuoteDetailDiscountPercentage
        {
            get
            {
                if (QuoteDetailOldUnitPriceExclTax != 0) // pour éviter la division par zéro
                {
                    return (QuoteDetailDiscountAmount / QuoteDetailOldUnitPriceExclTax) * 100;
                }
                else
                {
                    return 0;
                }
            }
        }

        [XmlIgnore()]
        public ModeViewEnum ModeView
        {
            get { return _modeView; }
            set { _modeView = value; }
        }
        /// <summary>
        /// Commentaire commercial sur la ligne 
        /// </summary>
        public string QuoteComments
        {
            get { return _quoteComments; }
            set { _quoteComments = value; }
        }

        public int AddressID
        {
            get { return _addressId; }
            set { _addressId = value; }
        }

        [XmlIgnore()]
        public string AddressLabel
        {
            get { return _addressLabel; }
            set { _addressLabel = value; }
        }

        /// <summary>
        /// Obtient ou définit si la ligne a été créée à partir de l'import des adresses
        /// </summary>
        [XmlElement("FromImportAddresses", IsNullable = true)]
        public bool? FromImportAddress { get; set; }

        /// <summary>
        /// Obtient ou définit le ProductId maître lors de l'import des adresses
        /// </summary>
        [XmlElement("ImportAddressMasterProductId", IsNullable = true)]
        public Guid? ImportAddressMasterQuoteDetailID { get; set; }

        /// <summary>
        /// Gets or sets address saved during quote creation
        /// </summary>        
        public QuotePostalAddress AddressOnCreation { get { return _addressOnCreation; } set { _addressOnCreation = value; } }

        /// <summary>
        /// Etat de la validation de la remise 
        /// </summary>
        public bool QuoteDetailDicountValidation
        {
            get { return _quoteDetailDicountValidation; }
            set { _quoteDetailDicountValidation = value; }
        }

        private string shippingDetailModeLabel;
        /// <summary>
        /// Label Mode de livraison de l'article du devis
        /// </summary>
        public string ShippingDetailModeLabel
        {
            get { return shippingDetailModeLabel; }
            set { shippingDetailModeLabel = value; }
        }

        private int shippingDetailModeType;
        private string _destEmail;
        private string _shippingdetailDelayLabel;
        private string _quoteDetailDicountValidationMessage;

        public QuoteLineDetail(string quoteDetailId, int adressId, int quantity)
        {
            QuoteDetailID = new Guid(quoteDetailId);
            Quantity = QuoteDetailQuantity = quantity;
            AddressID = adressId;
        }

        public QuoteLineDetail(string quoteDetailId, int adressId, int quantity, int prid)
        {
            QuoteDetailID = new Guid(quoteDetailId);
            Quantity = QuoteDetailQuantity = quantity;
            AddressID = adressId;
            ProductID = prid;
        }

        /// <summary>
        /// Mode de livraison de l'article du devis
        /// </summary>
        public int ShippingDetailModeType
        {
            get { return shippingDetailModeType; }
            set { shippingDetailModeType = value; }
        }

        public string DestEmail
        {
            get { return _destEmail; }
            set { _destEmail = value; }
        }

        public string ShippingdetailDelayLabel
        {
            get { return _shippingdetailDelayLabel; }
            set { _shippingdetailDelayLabel = value; }
        }

        public string QuoteDetailDicountValidationMessage
        {
            get { return _quoteDetailDicountValidationMessage; }
            set { _quoteDetailDicountValidationMessage = value; }
        }

        #endregion
    }
}
