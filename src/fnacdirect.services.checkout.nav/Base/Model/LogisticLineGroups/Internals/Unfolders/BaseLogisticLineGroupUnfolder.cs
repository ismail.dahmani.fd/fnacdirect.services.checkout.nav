﻿using System;
using System.Collections.Generic;
using FnacDirect.OrderPipe.Base.Model;

namespace FnacDirect.OrderPipe.Base.Business.LogisticLineGroups.Internals.Unfolders
{
    public abstract class BaseLogisticLineGroupUnfolder : ILogisticLineGroupUnfolder
    {
        public ILogisticLineGroupGenerationProcessContext Context { get; set; }
        public abstract IEnumerable<Article> Unfold(IEnumerable<Article> articles);
    }
}