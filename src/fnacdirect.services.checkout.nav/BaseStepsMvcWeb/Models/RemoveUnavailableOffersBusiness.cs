using System;
using System.Linq;
using FnacDirect.OrderPipe.Base.Business.ShoppingCart;
using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.OrderPipe.Base.Model.Facets;
using FnacDirect.OrderPipe.Base.Business.Helpers;

namespace FnacDirect.OrderPipe.Base.Business
{
    /// <summary>
    /// Business de suppression des offres MP de l'OrderForm
    /// </summary>
    /// <remarks>
    /// Le code a été déplacé de la Step RemoveUnavailableOffers vers cette classe afin de pouvoir l'utiliser 
    /// depuis la Step FilterMarketPlaceActivation sans dupliquer le code.
    /// </remarks>
    public class RemoveUnavailableOffersBusiness : IRemoveUnavailableOffersBusiness
    {
        private readonly Predicate<MarketPlaceArticle> _canDelete;
        private readonly Action<IGotLineGroups, MarketPlaceArticle> _onDeleteInIGotLinegroups;

        public RemoveUnavailableOffersBusiness(Predicate<MarketPlaceArticle> canDelete, Action<IGotLineGroups, MarketPlaceArticle> onDelete)
        {
            _canDelete = canDelete;
            _onDeleteInIGotLinegroups = onDelete;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="iGotLineGroups"></param>
        public void RemoveUnavailableOffers(IGotLineGroups iGotLineGroups)
        {
            foreach (var lineGroup in iGotLineGroups.LineGroups)
            {
                foreach (var marketPlaceArticle in lineGroup.Articles.OfType<MarketPlaceArticle>().ToList())
                {
                    if (!_canDelete(marketPlaceArticle))
                    {
                        continue;
                    }

                    lineGroup.Articles.Remove(marketPlaceArticle);

                    if (!iGotLineGroups.DeletedArticles.OfType<MarketPlaceArticle>().Where(mpArt => mpArt.Offer != null && marketPlaceArticle.Offer != null && mpArt.Offer.OfferId == marketPlaceArticle.Offer.OfferId).Any())
                    {
                        _onDeleteInIGotLinegroups(iGotLineGroups, marketPlaceArticle);
                    }
                }
            }

            if (iGotLineGroups.DeletedArticles.Count > 0)
            {
                iGotLineGroups.ArticlesModified = true;
                iGotLineGroups.DeletedArticles.Modified = true;
            }
        }
    }
}
