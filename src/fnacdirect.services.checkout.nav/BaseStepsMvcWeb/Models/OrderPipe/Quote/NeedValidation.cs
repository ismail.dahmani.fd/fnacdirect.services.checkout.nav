using System;
using System.Collections.Generic;
using System.Text;
using FnacDirect.OrderPipe;
using System.Xml;

namespace FnacDirect.Quote.Model
{


    public class AddressesValidityData : GroupData
    {
        private List<string> _changes = new List<string>();

        public List<string> Changes
        {
            get
            {
                return _changes;
            }
            set
            {
                _changes = value;
            }
        }
    }    
    public class NeedValidation: GroupData
    {               
        private List<NeedValidationEntry> _needValidationEntries;

        public NeedValidation()
        {
            _needValidationEntries = new List<NeedValidationEntry>();
        }

        public bool ValidationNeeded
        {
            get {
                if (null != _needValidationEntries.Find(delegate(NeedValidationEntry e)
                {
                    return e.NeedValidation == true;
                }))
                    return true;
                else
                    return false;                
            }            
        }
               

        public void Set(bool validationNeeded , string causeValidation )
        {
            var needValidationEntry = NeedValidationEntries.Find(delegate(NeedValidationEntry e)
            {
                return e.CauseValidation.Equals(causeValidation);
            });

            if (null != needValidationEntry)
                needValidationEntry.NeedValidation = validationNeeded;
            else
            {
                needValidationEntry = new NeedValidationEntry(causeValidation, validationNeeded);
                NeedValidationEntries.Add(needValidationEntry);
            }            
        }

        public List<NeedValidationEntry> GetCauses(bool needValidation)
        {
            return NeedValidationEntries.FindAll(delegate(NeedValidationEntry e)
            {
                return e.NeedValidation == needValidation;
            });
        }

        public List<NeedValidationEntry> NeedValidationEntries
        {
            get
            {
                if (null == _needValidationEntries)
                    _needValidationEntries = new List<NeedValidationEntry>();
                return _needValidationEntries;
            }
            set { _needValidationEntries = value; }
        }

        public void Clean()
        {
            NeedValidationEntries.Clear();
        }
    }

    public class NeedValidationEntry
    {
        private bool _needValidation;
        private string _causeValidation;

        public NeedValidationEntry() { }
        public NeedValidationEntry(string causeValidation, bool needValidation)
        {
            _needValidation = needValidation;
            _causeValidation = causeValidation;
        }

        public bool NeedValidation { get { return _needValidation; } set { _needValidation = value; } }

        public string CauseValidation { get { return _causeValidation; } set { _causeValidation = value; } }
    }
}
