using System;
using System.Xml.Serialization;
using System.Collections.Generic;

namespace FnacDirect.OrderPipe.Base.Model
{
    public class LogisticType
    {

        [XmlIgnore]
        private int? _Index;

        public int? Index
        {
            get
            {
                return _Index;
            }
            set
            {
                _Index = value;
            }
        }

        [XmlIgnore]
        private int? _InBasket;

        [XmlIgnore]
        public int? InBasket
        {
            get
            {
                return _InBasket;
            }
            set
            {
                _InBasket = value;
            }
        }

        [XmlIgnore]
        private ShippingMethod _DefaultShippingMethod;

        [XmlIgnore]
        public ShippingMethod DefaultShippingMethod
        {
            get
            {
                if (_DefaultShippingMethod == null)
                    _DefaultShippingMethod = new ShippingMethod();

                return _DefaultShippingMethod;
            }
            set
            {
                _DefaultShippingMethod = value;
            }
        }

        [XmlIgnore]
        private WrapMethod  _DefaultWrap;

        [XmlIgnore]
        public WrapMethod DefaultWrap
        {
            get
            {
                return _DefaultWrap;
            }
            set
            {
                _DefaultWrap = value;
            }
        }

        [XmlIgnore]
        private decimal? _PriceGlobalDBEur;

        [XmlIgnore]
        public decimal? PriceGlobalDBEur
        {
            get
            {
                return _PriceGlobalDBEur;
            }
            set
            {
                _PriceGlobalDBEur = value;
            }
        }

        [XmlIgnore]
        private decimal? _PriceGlobalEur;

        [XmlIgnore]
        public decimal? PriceGlobalEur
        {
            get
            {
                return _PriceGlobalEur;
            }
            set
            {
                _PriceGlobalEur = value;
            }
        }

        [XmlIgnore]
        private decimal? _PriceGlobalNoVatEUR;

        [XmlIgnore]
        public decimal? PriceGlobalNoVatEUR
        {
            get
            {
                return _PriceGlobalNoVatEUR;
            }
            set
            {
                _PriceGlobalNoVatEUR = value;
            }
        }

        [XmlIgnore]
        private decimal? _CurrentPriceNoVatEUR;

        [XmlIgnore]
        public decimal? CurrentPriceNoVatEUR
        {
            get
            {
                return _CurrentPriceNoVatEUR;
            }
            set
            {
                _CurrentPriceNoVatEUR = value;
            }
        }

        [XmlIgnore]
        private decimal? _CurrentPriceDbEUR;

        [XmlIgnore]
        public decimal? CurrentPriceDbEUR
        {
            get
            {
                return _CurrentPriceDbEUR;
            }
            set
            {
                _CurrentPriceDbEUR = value;
            }
        }

        [XmlIgnore]
        private decimal? _CurrentPriceEUR;

        [XmlIgnore]
        public decimal? CurrentPriceEUR
        {
            get
            {
                return _CurrentPriceEUR;
            }
            set
            {
                _CurrentPriceEUR = value;
            }
        }

        [XmlIgnore]
        private decimal? _PriceDetailEur;

        [XmlIgnore]
        public decimal? PriceDetailEur
        {
            get
            {
                return _PriceDetailEur;
            }
            set
            {
                _PriceDetailEur = value;
            }
        }

        [XmlIgnore]
        private decimal? _PriceDetailNoVatEUR;

        [XmlIgnore]
        public decimal? PriceDetailNoVatEUR
        {
            get
            {
                return _PriceDetailNoVatEUR;
            }
            set
            {
                _PriceDetailNoVatEUR = value;
            }
        }

        [XmlIgnore]
        private decimal? _PriceDetailDBEur;

        [XmlIgnore]
        public decimal? PriceDetailDBEur
        {
            get
            {
                return _PriceDetailDBEur;
            }
            set
            {
                _PriceDetailDBEur = value;
            }
        }


        [XmlIgnore]
        private int? _DelayInHours;


        [XmlIgnore]
        public int? DelayInHours
        {
            get
            {
                return _DelayInHours;
            }
            set
            {
                _DelayInHours = value;
            }
        }

        [XmlIgnore]
        private string _DelayLabel;

        [XmlIgnore]
        public string DelayLabel
        {
            get
            {
                return _DelayLabel;
            }
            set
            {
                _DelayLabel = value;
            }
        }

        [XmlIgnore]
        private WrapMethod _ChosenWrapMethod;

        [XmlIgnore]
        public WrapMethod ChosenWrapMethod
        {
            get
            {
                if (_ChosenWrapMethod == null)
                    _ChosenWrapMethod = new WrapMethod();

                return _ChosenWrapMethod;
            }
            set
            {
                _ChosenWrapMethod = value;
            }
        }

        [XmlIgnore]
        private ShippingMethod _ChosenShippingMethod;

        [Obsolete]
        public ShippingMethod ChosenShippingMethod
        {
            get
            {
                if (_ChosenShippingMethod == null)
                    _ChosenShippingMethod = new ShippingMethod();

                return _ChosenShippingMethod;
            }
            set
            {
                _ChosenShippingMethod = value;
            }
        }

        [XmlIgnore]
        private WrapMethodList _AvailableWrapMethod;

        [XmlIgnore]
        public WrapMethodList AvailableWrapMethod
        {
            get
            {
                if (_AvailableWrapMethod == null)
                    _AvailableWrapMethod = new WrapMethodList();

                return _AvailableWrapMethod;
            }
            set
            {
                _AvailableWrapMethod = value;
            }
        }

        [XmlIgnore]
        private ShippingMethodList _AvailableShippingMethods;

        [XmlIgnore]
        public ShippingMethodList AvailableShippingMethods
        {
            get
            {
                if (_AvailableShippingMethods == null)
                    _AvailableShippingMethods = new ShippingMethodList();

                return _AvailableShippingMethods;
            }
            set
            {
                _AvailableShippingMethods = value;
            }
        }

        private SplitMethodBase availableSplitMethod;

        [XmlIgnore]
        public SplitMethodBase AvailableSplitMethod
        {
            get { return availableSplitMethod; }
            set { availableSplitMethod = value; }
        }

        private int splitLevel;

        public int SplitLevel
        {
            get { return splitLevel; }
            set { splitLevel = value; }
        }

        private decimal priceSplitUserEur;

        [XmlIgnore]
        public decimal PriceSplitUserEur
        {
            get { return priceSplitUserEur; }
            set { priceSplitUserEur = value; }
        }

        private decimal priceSplitDBEur;

        [XmlIgnore]
        public decimal PriceSplitDBEur
        {
            get { return priceSplitDBEur; }
            set { priceSplitDBEur = value; }
        }

        private Dictionary<int,int> splitDelayRepartition = null;

        [XmlIgnore]
        public Dictionary<int, int> SplitDelayRepartition
        {
            get { return splitDelayRepartition; }
            set { splitDelayRepartition = value; }
        }
    }
}

