using System.Web.Http;

namespace FnacDirect.OrderPipe.BaseMvc.Web
{
    public class PopFdvHttpBatchHandler : PopHttpBatchHandler
    {
        protected override string Pipe => "popfdv";

        public PopFdvHttpBatchHandler(HttpServer httpServer)
            : base(httpServer)
        {

        }
    }
}
