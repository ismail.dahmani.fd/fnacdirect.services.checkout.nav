﻿using FnacDirect.OrderPipe.Base.LineGroups;
using System.Linq;
using System.Collections.Generic;

namespace FnacDirect.OrderPipe.Base.Model
{
    public class PspData : GroupData
    {
        public string PspSelectedLineGroupIdentifier { get; set; }

        public bool HasPSPBeenCalled { get; set; }

        public PspCallResultEnum PspCallResult { get; set; }
        public bool PspHasBeenCancelled { get; set; }

        public OrderTransactionInfoList OrderTransactionInfos { get; set; }

        public ILogisticLineGroup GetPspSelectedLineGroup(IEnumerable<ILogisticLineGroup> logisticLineGroups)
        {
            return logisticLineGroups.FirstOrDefault(l => l.UniqueId == PspSelectedLineGroupIdentifier);
        }
    }
}
