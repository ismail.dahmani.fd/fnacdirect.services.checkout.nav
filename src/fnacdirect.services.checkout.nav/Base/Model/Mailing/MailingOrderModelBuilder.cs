using FnacDirect.Customer.BLL;
using FnacDirect.Customer.Model;
using FnacDirect.OrderPipe.Base.BaseModel.Model;
using FnacDirect.OrderPipe.Base.Business.Helpers;
using FnacDirect.OrderPipe.Base.Business.OrderDetail;
using FnacDirect.OrderPipe.Base.Business.Shipping;
using FnacDirect.OrderPipe.Base.LineGroups;
using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.OrderPipe.Base.Model.BillingMethods;
using FnacDirect.OrderPipe.Base.Model.Facets;
using FnacDirect.OrderPipe.Base.Model.Solex;
using FnacDirect.OrderPipe.Core.Services;
using FnacDirect.Technical.Framework.Web;
using FnacDirect.Technical.Framework.Web.Mvc;
using FnacDirect.Technical.Framework.Web.UrlMgr;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using static FnacDirect.OrderPipe.Constants;

namespace FnacDirect.OrderPipe.Base.Business.Mailing
{
    public class MailingOrderModelBuilder : IMailingOrderModelBuilder
    {
        private readonly IOrderDetailService _orderDetailService;
        private readonly IUrlManager _urlManager;
        private readonly IApplicationContext _applicationContext;
        private readonly IPriceHelper _priceHelper;
        private readonly IUserExtendedContext _userExtendedContext;
        private readonly IShippingStateService _shippingStateService;
        private readonly IExternalCustomerService _externalCustomerService;
        private readonly IPricingService _priceBusiness;
        private readonly IPipeParametersProvider _pipeParametersProvider;
        private readonly ISwitchProvider _switchProvider;

        public MailingOrderModelBuilder(IUrlManager urlManager,
                                        IApplicationContext applicationContext,
                                        IUserExtendedContextProvider userExtendedContextProvider,
                                        IOrderDetailService orderDetailService,
                                        IShippingStateService shippingStateService,
                                        IExternalCustomerService externalCustomerService,
                                        IPipeParametersProvider pipeParametersProvider,
                                        IPricingServiceProvider pricingServiceProvider,
                                        ISwitchProvider switchProvider)
        {
            _urlManager = urlManager;
            _applicationContext = applicationContext;
            _priceHelper = _applicationContext.GetPriceHelper();
            _userExtendedContext = (IUserExtendedContext)userExtendedContextProvider.GetCurrentUserExtendedContext();
            _orderDetailService = orderDetailService;
            _shippingStateService = shippingStateService;
            _externalCustomerService = externalCustomerService;
            _priceBusiness = pricingServiceProvider.GetPricingService(applicationContext.GetSiteContext().MarketId);
            _pipeParametersProvider = pipeParametersProvider;
            _switchProvider = switchProvider;
        }

        public Order Build(PopOrderForm popOrderForm, bool isPro)
        {
            var contactEmailMD5 = new StringBuilder();
            using (var md5 = MD5.Create())
            {
                var data = md5.ComputeHash(Encoding.UTF8.GetBytes(popOrderForm.UserContextInformations.ContactEmail));
                for (var i = 0; i < data.Length; i++)
                {
                    contactEmailMD5.Append(data[i].ToString("x2"));
                }
            }

            var accountId = popOrderForm.UserContextInformations.AccountId;
            var allowMailType = (AllowMailType)(popOrderForm.UserContextInformations.CustomerEntity?.AllowMail ?? 0);

            var userInfos = new UserInfo
            {
                UserName = popOrderForm.UserContextInformations.FirstName,
                EmailMD5 = contactEmailMD5.ToString(),
                ScoringLevel = popOrderForm.UserContextInformations.MailMessageFlag,
                IsDeezerAccountSynchronized = !string.IsNullOrEmpty(_externalCustomerService.GetExternalToken(accountId, 1)),
                IsOptIn = allowMailType != AllowMailType.None //TODO Mutualiser la logique duplicate FDV
            };

            var appointmentData = popOrderForm.GetPrivateData<AppointmentData>("appointment", create: false);

            var orderGlobalInfos = new OrderGlobalInfo
            {
                IsPro = isPro,
                IsResocomRequired = false,
                ResocomValidationUrl = string.Empty,
                IsOnlyNumerical = popOrderForm.IsOnlyNumerical(),
                HasSameDayDeliveryAdvantageCode = popOrderForm.OrderGlobalInformations.AdvantageCode == SameDayDeliveryAdvantageCode(),
                HasAppointment = appointmentData != null && appointmentData.HasAppointmentArticleType.GetValueOrDefault(),
            };

            if (_switchProvider.IsEnabled("orderpipe.pop.taxcalculationservice.activation"))
            {
                orderGlobalInfos.TotalPrice = _priceHelper.Format(CalculateTotalPrice(popOrderForm.LogisticLineGroups), PriceFormat.Default);
            }
            else
            {
                orderGlobalInfos.TotalPrice = _priceHelper.Format(popOrderForm.GlobalPrices.TotalPriceEur.GetValueOrDefault(), PriceFormat.Default);
            }

            orderGlobalInfos.WrapPrice = popOrderForm.GlobalPrices.TotalShoppingCartBusinessWrapPriceUserEur.GetValueOrDefault() != decimal.Zero ? _priceHelper.Format(popOrderForm.GlobalPrices.TotalShoppingCartBusinessWrapPriceUserEur.GetValueOrDefault(), PriceFormat.Default) : string.Empty;
            orderGlobalInfos.HasManyShippingAddresses = _orderDetailService.HasManyShippingAddress(popOrderForm.ShippingMethodEvaluation);
            orderGlobalInfos.IsJukeBoxEnabled = _applicationContext.Operations["jukeboxmail"].Active && HasAudioCommande(popOrderForm);

            var totalShippingCostNoVAT = popOrderForm.GlobalPrices.TotalShipPriceUserEurNoVAT.GetValueOrDefault();
            orderGlobalInfos.TotalShippingPriceNoVat = _priceHelper.Format(totalShippingCostNoVAT, PriceFormat.Default);

            orderGlobalInfos.TotalPriceNoVat = _priceHelper.Format(CalculateTotalPriceNoVat(popOrderForm), PriceFormat.Default);
            orderGlobalInfos.AppointmentDeliveryInfo = BuildAppointmentDeliveryInfo(popOrderForm);
            orderGlobalInfos.VatRates = BuildVATRates(popOrderForm);

            var order = new Order
            {
                Culture = _applicationContext.GetCulture(),
                Address = popOrderForm.UserContextInformations.ContactEmail,
                OriginalDate = DateTime.Now.ToString(_applicationContext.MailConfiguration.DateFormat),
                ExpirationDate = DateTime.Now.AddHours(_applicationContext.MailConfiguration.ExpirationHourLength).ToString(_applicationContext.MailConfiguration.DateFormat),
                Template = _applicationContext.MailConfiguration.Templates.OrderConfirmation,
                RcpId = popOrderForm.UserContextInformations.AccountId,
                MainOrderReference = popOrderForm.OrderGlobalInformations.MainOrderUserReference,
                OrderExternalReference = popOrderForm.OrderGlobalInformations.OrderExternalReference,
                IsShopShipping = popOrderForm.ShippingMethodEvaluation.FnacCom.HasValue &&
                        popOrderForm.ShippingMethodEvaluation.FnacCom.Value.SelectedShippingMethodEvaluationType == BaseModel.ShippingMethodEvaluationType.Shop,
                IsShippingFly = popOrderForm.ShippingMethodEvaluation.FnacCom.HasValue &&
                        popOrderForm.ShippingMethodEvaluation.FnacCom.Value.SelectedShippingMethod == ShippingMethods.Fly5Hours,
            };

            order.User = userInfos;
            order.OrderInfo = orderGlobalInfos;
            order.OrderDetails = BuildOrderDetails(popOrderForm);
            order.BillingAddress = ConvertBillingAddress(popOrderForm.BillingAddress);
            order.Billing = BuildBillingInfo(popOrderForm);
            order.ShippingAddress = popOrderForm.IsOnlyNumerical() ? null : _orderDetailService.ComputeShippingAddress(popOrderForm.ShippingMethodEvaluation);
            order.Cgv = _orderDetailService.BuildCgv(popOrderForm);

            order.GuestInfos = GetGuestInfos(popOrderForm);

            return order;
        }

        private decimal CalculateTotalPrice(List<LogisticLineGroup> logisticLineGroups)
        {
            var totalPrice = 0.0m;

            foreach (var logisticLineGroup in logisticLineGroups)
            {
                var useVat = logisticLineGroup.VatCountry.UseVat;

                if (useVat)
                {
                    totalPrice += logisticLineGroup.GlobalPrices.TotalPriceEur.GetValueOrDefault();
                }
                else
                {
                    totalPrice += logisticLineGroup.GlobalPrices.TotalPriceEurNoVat;
                }
            }

            return totalPrice;
        }

        private static decimal CalculateTotalPriceNoVat(PopOrderForm popOrderForm)
        {
            var wrapCostNoVAT = popOrderForm.LineGroups.Fnac().Sum(lg => lg.GlobalPrices.TotalShoppingCartBusinessWrapPriceUserEur.GetValueOrDefault());
            var splitCostNoVAT = popOrderForm.LineGroups.Fnac().Sum(lg => lg.GlobalPrices.TotalSplitPriceDBNoVatEur.GetValueOrDefault());
            var totalShippingCostNoVAT = popOrderForm.GlobalPrices.TotalShipPriceUserEurNoVAT.GetValueOrDefault();

            return totalShippingCostNoVAT + popOrderForm.GlobalPrices.TotalShoppingCartBusinessPriceNoVatEur.GetValueOrDefault() + popOrderForm.GlobalPrices.TotalEcoTaxEurNoVAT.GetValueOrDefault() + splitCostNoVAT + wrapCostNoVAT;
        }

        private GuestInfos GetGuestInfos(PopOrderForm popOrderForm)
        {
            var isGuest = popOrderForm.UserContextInformations.IsGuest;
            var guestInfos = new GuestInfos() { IsGuest = isGuest };

            if (isGuest)
            {
                var guestData = popOrderForm.GetPrivateData<GuestData>(create: false);
                if (guestData != null)
                {
                    var mainOrderReference = popOrderForm.OrderGlobalInformations.MainOrderReference;

                    var guestOrderTrackingUrl = _urlManager.Sites.Account.GetPage("GuestOrderTracking").DefaultUrl;
                    guestOrderTrackingUrl.WithParam(new UrlWriterParameter("t", mainOrderReference.ToString()));

                    guestInfos.GuestOrderTrackingUrl = guestOrderTrackingUrl.ToString();
                    guestData.GuestOrderTrackingUrl = guestOrderTrackingUrl.ToString();

                    guestInfos.GuestCGULabel = _applicationContext.GetMessage("orderpipe.pop.mail.guest");
                }
            }

            return guestInfos;
        }

        private AppointmentDeliveryInfo BuildAppointmentDeliveryInfo(PopOrderForm popOrderForm)
        {
            var info = new AppointmentDeliveryInfo();

            var deliverySlotData = popOrderForm.GetPrivateData<DeliverySlotData>(create: false);

            var authorizedShippingMethods = new List<int>
            {
                ShippingMethods.ChronopostDelivery
            };

            if (deliverySlotData == null || !deliverySlotData.HasSelectedSlot)
            {
                return null;
            }

            if (!popOrderForm.ShippingMethodEvaluation.FnacCom.HasValue)
            {
                return null;
            }

            authorizedShippingMethods.AddRange(_pipeParametersProvider.GetAsEnumerable<int>("shipping.fly.ids"));

            if (!popOrderForm.ShippingMethodEvaluation.FnacCom.Value.SelectedShippingMethod.HasValue 
                    || !authorizedShippingMethods.Contains(popOrderForm.ShippingMethodEvaluation.FnacCom.Value.SelectedShippingMethod.Value))
            {
                return null;
            }

            info.DateMin = deliverySlotData.DeliverySlotSelected.StartDate;
            info.DateMax = deliverySlotData.DeliverySlotSelected.EndDate;

            return info;
        }

        public bool HasAudioCommande(PopOrderForm popOrderForm)
        {
            var hasAudioCommande = popOrderForm.LineGroups
                                              .Any(lg => lg.Articles
                                                           .Any(a => a.Family.GetValueOrDefault() == (int)FnacDirect.Contracts.Online.Model.ArticleGroup.Audio));
            return hasAudioCommande;
        }
        private string SameDayDeliveryAdvantageCode()
        {
            return _applicationContext.GetAppSetting("AdvantageCodeSDD");
        }

        private List<OrderDetail> BuildOrderDetails(PopOrderForm popOrderForm)
        {
            var orders = new List<OrderDetail>();
            var dle = ComputeDle(popOrderForm);

            orders.AddRange(popOrderForm.LogisticLineGroups.Select(logisticLineGroup => new OrderDetail
            {
                OrderReference = logisticLineGroup.OrderUserReference,
                OrderId = logisticLineGroup.OrderPk.GetValueOrDefault(),
                ShippingCost = GetShippingCost(logisticLineGroup),
                SellerName = logisticLineGroup.Seller.DisplayName ?? _applicationContext.GetMessage("orderpipe.pop.mail.seller.fnac"),
                SellerIsFnac = logisticLineGroup.IsFnacCom,
                IsMarketPlace = logisticLineGroup.IsMarketPlace,
                Articles = _orderDetailService.BuildArticles(logisticLineGroup.Articles, dle).ToList()
            }));
            return orders;
        }

        private string GetShippingCost(LogisticLineGroup logisticLineGroup)
        {
            var shippingCostOfferd = _applicationContext.GetMessage("orderpipe.pop.mail.shippingcost.offered");
            if (_userExtendedContext.Customer.IsAdherentOne && logisticLineGroup.GlobalPrices.TotalShipPriceUserEur.GetValueOrDefault() == decimal.Zero)
            {
                return shippingCostOfferd;
            }

            var shippingCost = 0.0m;
            var useVat = logisticLineGroup.VatCountry.UseVat;
            var totalShipPrice = logisticLineGroup.GlobalPrices.TotalShipPriceUserEur.GetValueOrDefault();
            var totalShipPriceNoVat = logisticLineGroup.GlobalPrices.TotalShipPriceUserEurNoVAT.GetValueOrDefault();

            if (!_pipeParametersProvider.Get("taxcalculationservice.forcelegacy", false)
                && _switchProvider.IsEnabled("orderpipe.pop.taxcalculationservice.activation"))
            {
                shippingCost = useVat ? totalShipPrice : totalShipPriceNoVat;
            }
            else
            {
                var shippingCountryId = logisticLineGroup.ShippingAddress.CountryId.GetValueOrDefault(0);
                var localCountryId = _applicationContext.GetCountryId();

                if (shippingCountryId == localCountryId)
                {

                    shippingCost = useVat ? totalShipPrice : totalShipPriceNoVat;
                }
                else
                {
                    var vatRateId = _pipeParametersProvider.Get("shipping.vat.rate.id", -1);
                    var shippingCountryVatRate = _priceBusiness.GetVATRate(shippingCountryId, vatRateId);
                    shippingCost = PriceHelpers.GetPriceTTC(totalShipPriceNoVat, shippingCountryVatRate);
                }
            }

            return _priceHelper.Format(shippingCost, PriceFormat.Default);
        }

        private TheoreticalDeliveryDateList ComputeDle(PopOrderForm popOrderForm)
        {
            var shippingAddress = popOrderForm.IsOnlyNumerical() ? null : _orderDetailService.ComputeShippingAddress(popOrderForm.ShippingMethodEvaluation);
            if (shippingAddress is Business.OrderDetail.RelayAddress relayAddress)
            {
                if (!CheckDleVisibility(relayAddress.ZipCode))
                {
                    return new TheoreticalDeliveryDateList();
                }
            }
            return _orderDetailService.ComputeTheoreticalDeliveryDate(popOrderForm);
        }

        private bool CheckDleVisibility(string zipCode)
        {
            return string.IsNullOrEmpty(_shippingStateService.GetStateByZipCode(zipCode));
        }

        private Business.OrderDetail.PostalAddress ConvertBillingAddress(BillingAddress billingAddress)
        {
            var postalAddress = new Business.OrderDetail.PostalAddress
            {
                Alias = billingAddress.Alias,
                FirstName = billingAddress.Firstname,
                LastName = billingAddress.Lastname,
                Company = billingAddress.Company,
                Address = _orderDetailService.FormatAddress(billingAddress.AddressLine1, billingAddress.AddressLine2, billingAddress.AddressLine3, billingAddress.AddressLine4).Trim(),
                ZipCode = billingAddress.ZipCode,
                City = billingAddress.City,
                State = billingAddress.State,
                Country = billingAddress.CountryLabel
            };
            return postalAddress;
        }

        private BillingInfo BuildBillingInfo(IGotBillingInformations billingInformation)
        {
            var billingInfo = new BillingInfo
            {
                IsPhoneBillingMethodUsed = billingInformation.OrderBillingMethods.OfType<CreditCardBillingMethod>().Any(bm => bm.NumByPhone.GetValueOrDefault()),
                IsCheckBillingMethoddUsed = billingInformation.OrderBillingMethods.Any(bm => bm.BillingMethodId == CheckBillingMethod.IdBillingMethod),
                IsVoucherBillingMethodUsed = billingInformation.OrderBillingMethods.Any(bm => bm.BillingMethodId == VoucherBillingMethod.IdBillingMethod),
                IsVirtualGiftCheckBillingMethodUsed = billingInformation.OrderBillingMethods.Any(bm => bm.BillingMethodId == VirtualGiftCheckBillingMethod.IdBillingMethod),
                IsOgoneCreditCardBillingMethodUsed = billingInformation.OrderBillingMethods.Any(bm => bm.BillingMethodId == OgoneCreditCardBillingMethod.IdBillingMethod),
                IsSipsCreditCardBillingMethodUsed = billingInformation.OrderBillingMethods.Any(bm => bm.BillingMethodId == SipsBillingMethod.IdBillingMethod),
                IsPayPalBillingMethodUsed = billingInformation.OrderBillingMethods.Any(bm => bm.BillingMethodId == PayPalBillingMethod.IdBillingMethod),
                IsPayByRefBillingMethodUsed = billingInformation.OrderBillingMethods.Any(bm => bm.BillingMethodId == PayByRefBillingMethod.IdBillingMethod),
                IsMBWayBillingMethodUsed = billingInformation.OrderBillingMethods.Any(bm => bm.BillingMethodId == MBWayBillingMethod.IdBillingMethod),
                IsFibBillingMethodUsed = billingInformation.OrderBillingMethods.Any(bm => bm.BillingMethodId == FibBillingMethod.IdBillingMethod),
                IsBankTransferMethodUsed = billingInformation.OrderBillingMethods.Any(bm => bm.BillingMethodId == BankTransferBillingMethod.IdBillingMethod),
                IsDeferredPaymentMethodUsed = billingInformation.OrderBillingMethods.Any(bm => bm.BillingMethodId == DefferedPaymentBillingMethod.IdBillingMethod),
                IsOrangeBillingMethodUsed = billingInformation.OrderBillingMethods.Any(bm => bm.BillingMethodId == OrangeBillingMethod.IdBillingMethod),
                PaymentMethods = billingInformation.OrderBillingMethods.Select(s => ProxyToPaymentMethod(billingInformation, s)).ToList()
            };

            return billingInfo;
        }

        private PaymentMethod ProxyToPaymentMethod(IGotBillingInformations iGotBillingInformations, BillingMethod billingMethod)
        {
            if (billingMethod is PayByRefBillingMethod payByRefBillingMethod)
            {
                return ToPaymentMethod(iGotBillingInformations, payByRefBillingMethod);
            }

            return ToPaymentMethod(iGotBillingInformations, billingMethod);
        }

        private PaymentMethod ToPaymentMethod(IGotBillingInformations iGotBillingInformations, BillingMethod billingMethod)
        {
            var paymentMethod = new PaymentMethod
            {
                Id = billingMethod.BillingMethodId,
                Type = GetBillingMethodLabel(billingMethod),
                Amount = _priceHelper.Format(iGotBillingInformations.BillingMethodConsolidation[billingMethod.BillingMethodId], PriceFormat.Default),
            };

            return paymentMethod;
        }

        private PaymentMethod ToPaymentMethod(IGotBillingInformations iGotBillingInformations, PayByRefBillingMethod payByRefBillingMethod)
        {
            var paymentMethod = new PayByRefPaymentMethod
            {
                Id = payByRefBillingMethod.BillingMethodId,
                Type = GetBillingMethodLabel(payByRefBillingMethod),
                Amount = _priceHelper.Format(iGotBillingInformations.BillingMethodConsolidation[payByRefBillingMethod.BillingMethodId], PriceFormat.Default),
                Reference = payByRefBillingMethod.Reference,
                MerchantId = iGotBillingInformations.OrderGlobalInformations.MerchantId
            };

            return paymentMethod;
        }

        private string GetBillingMethodLabel(BillingMethod billingMethod)
        {
            if (billingMethod.BillingMethodId == OgoneCreditCardBillingMethod.IdBillingMethod)
            {
                return _applicationContext.GetMessage("orderpipe.pop.mail.paymentmethod.ogonecreditcardbillingmethod");
            }
            if (billingMethod is CreditCardBillingMethod creditCardBillingMethod && creditCardBillingMethod.NumByPhone.GetValueOrDefault())
            {
                return _applicationContext.GetMessage("orderpipe.pop.mail.paymentmethod.byphone");
            }
            if (billingMethod.BillingMethodId == CheckBillingMethod.IdBillingMethod)
            {
                return _applicationContext.GetMessage("orderpipe.pop.mail.paymentmethod.checkbillingmethod");
            }
            if (billingMethod.BillingMethodId == VoucherBillingMethod.IdBillingMethod)
            {
                return _applicationContext.GetMessage("orderpipe.pop.mail.paymentmethod.voucherbillingmethod");
            }
            if (billingMethod.BillingMethodId == VirtualGiftCheckBillingMethod.IdBillingMethod)
            {
                return _applicationContext.GetMessage("orderpipe.pop.mail.paymentmethod.virtualgiftcheckbillingmethod");
            }
            if (billingMethod.BillingMethodId == SipsBillingMethod.IdBillingMethod)
            {
                return _applicationContext.GetMessage("orderpipe.pop.mail.paymentmethod.sipsbillingmethod");
            }
            if (billingMethod.BillingMethodId == PayPalBillingMethod.IdBillingMethod)
            {
                return _applicationContext.GetMessage("orderpipe.pop.mail.paymentmethod.paypalbillingmethod");
            }
            if (billingMethod.BillingMethodId == FibBillingMethod.IdBillingMethod)
            {
                return _applicationContext.GetMessage("orderpipe.pop.mail.paymentmethod.fibbillingmethod");
            }
            if (billingMethod.BillingMethodId == BankTransferBillingMethod.IdBillingMethod)
            {
                return _applicationContext.GetMessage("orderpipe.pop.mail.paymentmethod.banktransfer");
            }
            if (billingMethod.BillingMethodId == DefferedPaymentBillingMethod.IdBillingMethod)
            {
                return _applicationContext.GetMessage("orderpipe.pop.mail.paymentmethod.deferredpayment");
            }
            if (billingMethod.BillingMethodId == OrangeBillingMethod.IdBillingMethod)
            {
                return _applicationContext.GetMessage("orderpipe.pop.mail.paymentmethod.orangebillingmethod");
            }
            return string.Empty;
        }
        private List<VatRate> BuildVATRates(PopOrderForm popOrderForm)
        {
            var model = new List<VatRate>();
            foreach (var vat in popOrderForm.GlobalPrices.VATList)
            {
                if (vat.Total != decimal.Zero)
                {
                    var item = new VatRate()
                    {
                        Label = vat.RateLiteral,
                        Total = _priceHelper.Format(vat.Total, PriceFormat.Default),
                        Rate = vat.Rate
                    };
                    model.Add(item);
                }
            }

            return model;
        }
    }
}
