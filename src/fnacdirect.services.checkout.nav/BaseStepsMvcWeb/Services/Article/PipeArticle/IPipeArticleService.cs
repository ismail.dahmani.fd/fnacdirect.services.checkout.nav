using FnacDirect.Contracts.Online.Model;
using FnacDirect.OrderPipe.Base.Business;
using FnacDirect.OrderPipe.Base.Model;
using System.Collections.Generic;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Services.Article.Pipe
{
    public interface IPipeArticleService
    {
        BaseArticle GetArticle(int productId, string offerRef = "");
        StandardArticle GetStandardArticle(int productId);
        IEnumerable<StandardArticle> GetStandardArticles(IEnumerable<int> productIds);
        MarketPlaceArticle GetMarketplaceArticle(int productId, string offerRef);
        IEnumerable<FnacDirect.Contracts.Online.Model.Article> GetServices(StandardArticle standardArticle, ArticleFilter filter = ArticleFilter.NONE);
        CategoryLinkedArticleList GetLinkedArticles(StandardArticle standardArticle);
    }
}
