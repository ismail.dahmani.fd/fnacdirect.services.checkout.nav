using System;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using FnacDirect.OrderPipe.Config;
using FnacDirect.OrderPipe.CoreRouting;

namespace FnacDirect.OrderPipe.CoreMvcWeb.Routing
{
    public class MvcOrderPipeInteractiveStepHandlerStrategy : OrderPipeInteractiveStepHandlerStrategyBase<MvcUiDescriptor>
    {
        public override IHttpHandler GetHttpHandlerFor(MvcUiDescriptor mvcUiDescriptor, OPContext opContext, RequestContext requestContext)
        {
            var httpContext = new HttpContextWrapper(HttpContext.Current);

            if (requestContext.HttpContext.Request.IsAjaxRequest())
            {
                return new MvcHandler(requestContext);
            }

            if (requestContext.RouteData.GetRouteName() != mvcUiDescriptor.Route
                && mvcUiDescriptor.RedirectMode != RedirectMode.Transfert)
            {
                var urlHelper = new UrlHelper(requestContext);

                var routeValueDictionary = new RouteValueDictionary();

                if (mvcUiDescriptor.RouteValueProvider != null)
                {
                    routeValueDictionary = mvcUiDescriptor.RouteValueProvider.GetRouteValueFor(opContext.OF);
                }

                //TODO: Après TNR, doublon avec MvcOrderPipeStepRedirectionStrategy
                var queryStringToPreserve = HttpContext.Current.Request.QueryString.AllKeys
                    .Where(k => k != null
                        && k != "step"
                        && k != "reset"
                        && k != "context")
                    .ToList();

                if (mvcUiDescriptor.QueryStringMode == QueryStringMode.Whitelist.ToString())
                {
                    var whitelist = mvcUiDescriptor.QueryStringsToPreserve();

                    queryStringToPreserve.RemoveAll(k => !whitelist.Contains(k.ToLowerInvariant()));
                }

                foreach (var key in queryStringToPreserve)
                {
                    if (!routeValueDictionary.ContainsKey(key))
                    {
                        routeValueDictionary.Add(key, HttpContext.Current.Request.QueryString[key]);
                    }
                }

                var url = urlHelper.RouteUrl(mvcUiDescriptor.Route, routeValueDictionary);

                if (string.IsNullOrEmpty(url))
                {
                    url = Technical.Framework.Web.FnacContext.Current.UrlManagerNew.Sites.WWWDotNet.BaseRootUrl.ToString();
                }

                HttpContext.Current.Response.Redirect(url);
            }

            var routeData = RouteTable.Routes.GetRouteData(httpContext);

            if (routeData == null)
                throw new ArgumentNullException();

            routeData.Values["controller"] = mvcUiDescriptor.Controller;
            routeData.Values["action"] = mvcUiDescriptor.Action;

            routeData.DataTokens["area"] = mvcUiDescriptor.AreaName;
            routeData.DataTokens["Namespaces"] = "System.String[]";
            routeData.DataTokens["UseNamespaceFallback"] = false;

            return new MvcHandler(new RequestContext(httpContext, routeData));
        }
    }
}
