﻿namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Common
{
    public abstract class BaseChoiceData
    {
        public string Id { get; set; }
        public string Label { get; set; }
    }
}
