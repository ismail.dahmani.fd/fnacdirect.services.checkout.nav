using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace FnacDirect.OrderPipe
{
    /// <summary>
    /// Implémentation de base de IOPManager, utilisant le conteneur Spring pour instancier le pipe
    /// </summary>
    public class OPManager : IOPManager
    {
        private string _prefix = "pipe";

        public string Prefix
        {
            get
            {
                return _prefix;
            }
            set
            {
                _prefix = value;
            }
        }

        public void FlushPipeConfig()
        {
            _pipes.Clear();
            _pipeConfigurations.Clear();
            _pipeLightConfigurations.Clear();
        }

        private readonly Dictionary<string, OP> _pipes = new Dictionary<string, OP>();
        private readonly Dictionary<string, OP> _pipeConfigurations = new Dictionary<string, OP>();
        private readonly Dictionary<string, OP> _pipeLightConfigurations = new Dictionary<string, OP>();

        public IOP this[string name]
        {
            get
            {
                var key = _pipes.Keys.FirstOrDefault(k => string.Equals(name, k, StringComparison.InvariantCultureIgnoreCase));
                if (key == null)
                {
                    lock (this)
                    {
                        // double check after a lock
                        key = _pipes.Keys.FirstOrDefault(k => string.Equals(name, k, StringComparison.InvariantCultureIgnoreCase));
                        if (key == null)
                        {
                            if (_pipeConfigurations.Keys.FirstOrDefault(k => string.Equals(name, k, StringComparison.InvariantCultureIgnoreCase)) != null)
                            {
                                var op = _pipeConfigurations[name];
                                op.SetupSteps();
                                _pipes[name] = op;
                            }
                            else
                            {
                                var op = ObjectFactory.Get<OP>(Prefix + "." + name);
                                op.PipeName = name;
                                op.LoadConfiguration();
                                _pipeConfigurations[name] = op;
                                op.SetupSteps();
                                _pipes[name] = op;
                            }
                            key = name;
                        }
                    }
                }
                return _pipes[key];
            }
        }

        public IOP LoadConfigurationOnly(string pipeName)
        {
            var key = _pipeConfigurations.Keys.FirstOrDefault(k => string.Equals(pipeName, k, StringComparison.InvariantCultureIgnoreCase));
            if (key == null)
            {
                lock (this)
                {
                    if (_pipes.Keys.FirstOrDefault(k => string.Equals(pipeName, k, StringComparison.InvariantCultureIgnoreCase)) != null)
                    {
                        _pipeConfigurations[pipeName] = _pipes[pipeName];
                    }
                    else
                    {
                        var op = ObjectFactory.Get<OP>(Prefix + "." + pipeName);
                        op.PipeName = pipeName;
                        _pipeConfigurations[pipeName] = op;
                        op.LoadConfiguration();
                    }
                    key = pipeName;
                }
            }
            return _pipeConfigurations[key];
        }

        public IOP LoadConfigurationWithStepsOnly(string pipeName)
        {
            var found = _pipeLightConfigurations.ContainsKey(pipeName);
            if (!found)
            {
                lock (this)
                {
                    var op = ObjectFactory.Get<OP>(Prefix + "." + pipeName);
                    op.PipeName = pipeName;
                    op.LoadConfiguration();
                    op.SetupStepsWithoutInit();
                    _pipeLightConfigurations[pipeName] = op;
                }
            }
            return _pipeLightConfigurations[pipeName];
        }

        public bool IsKnown(string pipeName)
        {
            return ObjectFactory.ContainsDefinition(_prefix + "." + pipeName);
        }
    }
}
