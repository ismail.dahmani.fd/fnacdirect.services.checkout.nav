namespace FnacDirect.OrderPipe.Base.Model.FDV.Rebate
{
    public enum RemoteRebateStatus
    {
        New = 1,
        RejectedByResp,
        CancelledBySeller,
        Expired,
        CancelledByPipe,
        ValidatedByResp,
        ValidatedOnStation,
        ExpiredFromDatabase,
    }
}
