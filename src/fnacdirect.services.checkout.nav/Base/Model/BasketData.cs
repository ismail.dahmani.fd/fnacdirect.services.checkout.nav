﻿namespace FnacDirect.OrderPipe.Base.Steps
{
    public class BasketData : GroupData
    {
        /// <summary>
        /// Si le panier contient le max nombre d'article (100)
        /// </summary>
        public bool HasMaxItemsInBasket { get; set; }

        /// <summary>
        /// Si le denier article ajouté avec succès
        /// </summary>
        public bool HasLastItemInBasket { get; set; }
    }
}
