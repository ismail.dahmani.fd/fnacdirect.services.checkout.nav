using System.Linq;
using FnacDirect.Contracts.Online.Model;
using FnacDirect.OrderPipe.Base.DAL;
using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.StoreAvailability.Business;

namespace FnacDirect.OrderPipe.Base.Business
{
    public class StomEcoTaxBusiness : IStomEcoTaxBusiness
    {
        private readonly ICatalogDAL _catalogDal;
        private readonly ISTOMBusiness _stomBusiness;
        public StomEcoTaxBusiness(ICatalogDAL catalogDal, ISTOMBusiness stomBusiness)
        {
            _catalogDal = catalogDal;
            _stomBusiness = stomBusiness;
        }

        public bool LoadEcoTaxFromStom(StandardArticle art, string refUG)
        {
            var stocks = _stomBusiness.GetStockInfoBySKU(art.ReferenceGU, refUG);

            // On n'a pas d'info dans Stom sur l'UG courante, on va regarder sur l'UG 0104
            if (!stocks.Any())
                stocks = _stomBusiness.GetStockInfoBySKU(art.ReferenceGU, "0104").Where(s => s.LigneProduit == "1");

            if (stocks.Any())
            {
                var stock = stocks.First();
                art.EcoTaxCode = stock.EcoTaxSku;
                art.EcoTaxEur = stock.EcoTaxPrixVente;
                art.EcoTaxEurNoVAT = 0;

                if (art.SalesInfo != null)
                {
                    art.SalesInfo.EcoTaxCode = stock.EcoTaxSku;
                    art.SalesInfo.EcoTaxAmount = new EcoTax() { TTC = stock.EcoTaxPrixVente, HT = 0 };
                }

                return true;
            }

            return false;
        }

        public bool LoadEcoTaxBundleFromStom(StandardArticle art, string refUG)
        {
            if(art is Bundle bundle)
            {
                foreach (StandardArticle comp in bundle.BundleComponents)
                {
                    if (!LoadEcoTaxFromStom(comp, refUG))
                    {
                        return false;
                    }
                }

                bundle.EcoTaxCode = string.Empty;
                bundle.EcoTaxEur = bundle.BundleComponents.OfType<StandardArticle>().Sum(c => c.EcoTaxEur * c.Quantity);
                bundle.EcoTaxEurNoVAT = 0;

                return true;
            }

            return false;
        }
    }
}
