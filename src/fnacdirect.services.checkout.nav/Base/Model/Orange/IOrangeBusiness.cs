using FnacDirect.Orange.Model;

namespace FnacDirect.OrderPipe.Base.Business
{
    public interface IOrangeBusiness
    {
        string GetOrangeAuthUrl();
        string GetIse2FromAssociatedAccount(int accountId);
        bool EnableAccountOrange(string ise2, string accountReference);
        void AddOrangeTransaction(OrangeTransaction transaction);
        void CreateExternalTokenTypeOrange(string ise2, int accountId);
    }
}
