﻿using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.OrderPipe.Base.Model.LineGroups;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Common.Basket
{
    public interface ISellerInformationsViewModelBuilder
    {
        SellerInformationsViewModel Build(PopOrderForm popOrderForm, int sellerId);
    }
}
