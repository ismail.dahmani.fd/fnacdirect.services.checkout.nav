﻿namespace FnacDirect.OrderPipe.Base.Model
{
    public enum DiscountType
    {
        Article = 1,
        ShippingPrice = 2,
        SplitPrice = 3,
        WrapPrice = 4
    }
}
