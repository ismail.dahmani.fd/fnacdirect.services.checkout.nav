﻿
using System.Xml.Serialization;
namespace FnacDirect.OrderPipe.Base.Business.OrderDetail
{
    public class PostalAddress : BaseAddress
    {
        public PostalAddress()
        {
            Type = "PostalAddress";
        }

        public string Alias { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Company { get; set; }

        public string State { get; set; }

        public string Type { get; set; }
        public string Phone { get; set; }
    }
}
