using FnacDirect.OrderPipe.Base.Model.Tracking;
using FnacDirect.Technical.Framework.Configuration;
using Microsoft.Extensions.Configuration;

namespace Microsoft.Extensions.DependencyInjection
{
    public static class TrackingServiceCollectionExtension
    {
        public static IServiceCollection AddS2STracking(this IServiceCollection services, FnacDirect.Technical.Framework.Configuration.IConfigurationProvider configurationProvider)
        {
            var config = configurationProvider.Get("TrackingConfiguration");

            if (config != null)
            {
               
                services.AddKafkaMessageProducer("OrderPipe", (FnacDirect.Framework.PubSub.Producer.KafkaClientHandleConfig c)=>config.GetSection("KafkaProducer").Bind(c))
                    .AddEvent<BasketTracking>(config.GetSection("Basket"));
            }

            return services;
        }
    }
}
