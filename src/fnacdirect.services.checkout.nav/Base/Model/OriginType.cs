﻿namespace FnacDirect.OrderPipe.Base.BaseModel.Model
{
    public enum OriginType
    {
        Unknown = 0,
        MobileApp = 6000,
        IphoneApp = 11000,
        IpadApp = 11002,
        AndroidApp = 11003,
        SelfCheckoutIphoneApp = 21006,
        SelfCheckoutAndroidApp = 21007
    }
}
