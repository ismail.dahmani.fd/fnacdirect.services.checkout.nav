using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Basket
{
    public class AvailabilityViewModel
    {
        public bool HasQuantityMax { get; set; }

        public bool HasLimitedQuantity { get; set; }

        public string Availability { get; set; }

        /// <summary>
        /// FormAtHome ? (pour afficher bloc avec infos sur le téléchargement)
        /// </summary>
        public bool FormAtHome { get; set; }

        /// <summary>
        /// DematSoft ? (pour afficher bloc avec infos sur le téléchargement)
        /// </summary>
        public bool DematSoft { get; set; }

        /// <summary>
        /// Ebook ? (pour afficher bloc avec infos sur le téléchargement)
        /// </summary>
        public bool Numerical { get; set; }

        public bool IsPreOrder { get; set; }
    }

    public class ShopAvailabilityViewModel : AvailabilityViewModel
    {
        public bool ShipToStore
        {
            get
            {
                return true;
            }
        }

        public bool ClickAndCollect { get; set; }

        public string ShopName { get; set; }
        public string ShopAvailability { get; set; }

    }
}
