using FnacDirect.OrderPipe.Base.LineGroups;
using FnacDirect.OrderPipe.Base.Model.Facets;

namespace FnacDirect.OrderPipe.Base.Proxy.Ogone.PspId
{
    public interface IPspIdServiceFactory
    {
        IPspIdService GetInstance(string accountId, int? legalEntityId = null);
    }
}
