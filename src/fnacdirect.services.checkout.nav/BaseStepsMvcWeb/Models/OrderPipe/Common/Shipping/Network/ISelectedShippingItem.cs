﻿namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Common.Shipping
{
    public interface ISelectedShippingItem
    {
        string Key { get; }

        SelectedShippingItemSummary Summary { get; }
    }
}
