using FnacDirect.Customer.Model;
using FnacDirect.OrderPipe.Base.Business;
using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.OrderPipe.Base.Proxy.Appointment;
using System.Linq;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Payment
{
    public class PaymentHtmlTemplateViewModelBuilder : IPaymentHtmlTemplateViewModelBuilder
    {
        private readonly IAppointmentService _appointmentService;

        public PaymentHtmlTemplateViewModelBuilder(IAppointmentService appointmentService)
        {
            _appointmentService = appointmentService;
        }

        public PaymentHtmlTemplateViewModel Build(PopOrderForm popOrderForm, RootViewModel rootViewModel)
        {
            var paymentHtmlTemplateViewModel = new PaymentHtmlTemplateViewModel
            {
                Root = rootViewModel,
                OnlyShippedProducts = ComputeOnlyShippedProducts(popOrderForm)
            };

            OrderTransactionInfo oti = null;
            if (popOrderForm.OrderGlobalInformations.MainOrderTransactionInfos == null)
            {
                var pspData = popOrderForm.GetPrivateData<PspData>(false);

                if(pspData != null)
                {
                    oti = pspData.OrderTransactionInfos.GetByBillingId(OgoneCreditCardBillingMethod.IdBillingMethod);
                }
            }
            else
            {
                oti = popOrderForm.OrderGlobalInformations.MainOrderTransactionInfos.FirstOrDefault();
            }
            if (oti != null && oti.OrderTransaction != null)
            {
                var orderTransac = oti.OrderTransaction;
                var ogoneData = orderTransac.OrderTransactionOgone;
                var is3ds = ogoneData.Flag3D == "Y";
                var debitAmount = ogoneData.Amount;
                var realAmount = ogoneData.RealAmount;
                paymentHtmlTemplateViewModel.Is3ds = is3ds;
                paymentHtmlTemplateViewModel.DebitAmount = debitAmount;
                paymentHtmlTemplateViewModel.RealAmount = realAmount;
            }
            paymentHtmlTemplateViewModel.Root.BillingAddress.IsPreviewPayment = true;

            // Type debit
            var billingMethod = BillingMethodBusiness.GetOgoneCreditCardBillingMethod(popOrderForm);
            if(billingMethod != null && !string.IsNullOrEmpty(billingMethod.DebitType))
                paymentHtmlTemplateViewModel.DebitType = DebitHelper.GetDebitFromEnum(billingMethod.DebitType);

            return paymentHtmlTemplateViewModel;
        }

        private bool ComputeOnlyShippedProducts(PopOrderForm popOrderForm)
        {
            if (popOrderForm.LineGroups.Any(l => l.HasMarketPlaceArticle))
            {
                return false;
            }

            if (popOrderForm.LineGroups.Any(l => l.HasCollectInStoreArticles))
            {
                return false;
            }

            if(popOrderForm.LineGroups.Any(l => l.HasDematSoftArticles))
            {
                return false;
            }

            if (popOrderForm.LineGroups.Any(l => l.HasNumericalArticles))
            {
                return false;
            }

            if (popOrderForm.LineGroups.Any(l => l.HasDematSoftArticles))
            {
                return false;
            }

            var standardArticles = popOrderForm.LineGroups.SelectMany(l => l.Articles).OfType<StandardArticle>().ToList();

            var appointmentTypeIds = _appointmentService.GetAppointmentArticleTypeIds();
            
            if (standardArticles.Any(a => a.TypeId.HasValue && appointmentTypeIds.Contains(a.TypeId.Value)))
            {
                return false;
            }

            return true;
        }
    }
}
