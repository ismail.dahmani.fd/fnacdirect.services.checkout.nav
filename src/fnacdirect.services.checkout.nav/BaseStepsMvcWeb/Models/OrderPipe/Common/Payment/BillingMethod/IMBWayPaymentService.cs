using FnacDirect.OrderPipe.Base.Model;

namespace FnacDirect.OrderPipe.Base.Business.Payment
{
    public interface IMBWayPaymentService
    {
        bool IsAllowed(PopOrderForm popOrderForm);
    }
}
