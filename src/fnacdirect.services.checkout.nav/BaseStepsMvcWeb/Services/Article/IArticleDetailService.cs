using FnacDirect.Contracts.Online.IModel;
using FnacDirect.Contracts.Online.Model;
using FnacDirect.OrderPipe.Base.Model;
using DTO = FnacDirect.Contracts.Online.Model;

namespace FnacDirect.OrderPipe.Base.Business
{
    public interface IArticleDetailService
    {
        bool GetArticleDetailWithPrice(StandardArticle art, SiteContext siteContext, bool byReference);

        /// <summary>
        /// Fills in the details of the passed article.
        /// </summary>
        /// <param name="prmoArticle">The article.</param>
        /// <param name="siteContext">The site context.</param>
        /// <returns>True if the article details were fully loaded, false otherwise.</returns>
        bool GetArticleDetail(StandardArticle prmoArticle, SiteContext siteContext);

        /// <summary>
        /// Fills in the details of the passed article.
        /// </summary>
        /// <param name="prmoArticle">The article.</param>
        /// <param name="siteContext">The site context.</param>
        /// <param name="byReference">True if loading the details by Reference GU, false by Product ID.</param>
        /// <returns>True if the article details were fully loaded, false otherwise.</returns>
        bool GetArticleDetail(StandardArticle prmoArticle, SiteContext siteContext, bool byReference);

        /// <summary>
        /// Fills in the details of the passed article.
        /// </summary>
        /// <param name="prmoArticle">The article to fill.</param>
        /// <param name="siteContext">The site context.</param>
        /// <param name="article">The article DTO.</param>
        /// <returns>True if the article details were fully loaded, false otherwise.</returns>
        bool GetArticleDetail(StandardArticle prmoArticle, SiteContext siteContext, DTO.Article article);

        /// <summary>
        /// Returns the Article Reference.
        /// </summary>
        /// <param name="prmoArticle">The article for which to get the reference.</param>
        /// <param name="byReference">True to fill ArticleRefrence with Reference GU, false to fill Product ID.</param>
        /// <returns>The Article Reference, which can be null if prmoArticle is invalid.</returns>
        ArticleReference GetArticleReference(StandardArticle prmoArticle, bool byReference);

        /// <summary>
        /// Returns the articleDTO.
        /// </summary>
        /// <param name="prmoArticle">The article.</param>
        /// <param name="siteContext">The site context.</param>
        // <param name="ab">The Service ArticleBusiness3 Instance.</param>
        /// <param name="ar">The article reference.</param>
        /// <returns>The articleDTO.</returns>
        DTO.Article GetArticleFromService(StandardArticle prmoArticle, SiteContext siteContext, ArticleReference articleReference);
    }
}
