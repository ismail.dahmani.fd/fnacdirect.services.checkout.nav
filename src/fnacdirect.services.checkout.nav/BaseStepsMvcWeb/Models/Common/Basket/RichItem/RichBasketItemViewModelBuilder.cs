using FnacDirect.Membership.Model.Constants;
using FnacDirect.OrderPipe.Base.BaseModel;
using FnacDirect.OrderPipe.Base.BaseModel.Model;
using FnacDirect.OrderPipe.Base.Business;
using FnacDirect.OrderPipe.Base.Business.Operations;
using FnacDirect.OrderPipe.Base.Business.Shipping;
using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.OrderPipe.Base.Model.FDV;
using FnacDirect.OrderPipe.Base.Model.LineGroups;
using FnacDirect.OrderPipe.Base.Model.SelfCheckout;
using FnacDirect.OrderPipe.Base.Proxy.Membership;
using FnacDirect.OrderPipe.Base.ReducedPriceBooks;
using FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Basket.Children;
using FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Common.Basket;
using FnacDirect.OrderPipe.BaseMvc.Web.Services.Article.Availability;
using FnacDirect.Technical.Framework.Web.Mvc;
using FnacDirect.Technical.Framework.Web.UrlMgr;
using System;
using System.Collections.Generic;
using System.Linq;
using DTO = FnacDirect.Contracts.Online.Model;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Basket
{
    public class RichBasketItemViewModelBuilder : IRichBasketItemViewModelBuilder
    {
        private readonly IBasketItemViewModelBuilder _basketItemViewModelBuilder;
        private readonly Base.Proxy.Articles.IW3ArticleService _w3ArticleService;
        private readonly IArticleSubTitleService _articleSubTitleService;
        private readonly IApplicationContext _applicationContext;
        private readonly IEnumerable<int> _warantyTypes;
        private readonly IExpressPlusViewModelBuilder _expressPlusViewModelBuilder;
        private readonly RichBasketItemViewModelBuilderContext _context;
        private readonly IPriceViewModelBuilder _priceViewModelBuilder;
        private readonly ICompositeRichBasketItemChildrenViewModelBuilder _compositeRichBasketItemChildrenViewModelBuilder;
        private readonly IPromotionTemplateViewModelBuilder _promotionTemplateViewModelBuilder;
        private readonly ISwitchProvider _switchProvider;
        private readonly ISolexBusiness _solexBusiness;
        private readonly IPriceHelper _priceHelper;
        private readonly IArticleAvailabilityService _articleAvailabilityService;
        private readonly IAdherentCardArticleService _adherentCardArticleService;
        private readonly int _tryCardPrid;
        private readonly int _fnacPlusTryCardType;
        private readonly List<int> _fnacPlusCardTypes;
        private readonly int _donationTypeId;
        private readonly int _forcedMaxQuantity;
        private readonly bool _useMaxQuantityAllowedField;
        private const int DefaultMax = 999;
        private const string DefaultForcedMaxQuantity = "999";

        public RichBasketItemViewModelBuilder(IBasketItemViewModelBuilder basketItemViewModelBuilder,
            Base.Proxy.Articles.IW3ArticleService w3ArticleService,
            IArticleSubTitleService articleSubTitleService,
            IApplicationContext applicationContext,
            ISolexBusiness solexBusiness,
            IExpressPlusViewModelBuilder expressPlusViewModelBuilder,
            IPriceViewModelBuilder priceViewModelBuilder,
            RichBasketItemViewModelBuilderContext richBasketItemViewModelBuilderContext,
            IMembershipConfigurationService membershipConfigurationService,
            ICompositeRichBasketItemChildrenViewModelBuilder compositeRichBasketItemChildrenViewModelBuilder,
            IPromotionTemplateViewModelBuilder promotionTemplateViewModelBuilder,
            ISwitchProvider switchProvider,
            IArticleAvailabilityService articleAvailabilityService,
            IAdherentCardArticleService adherentCardArticleService)
        {
            _context = richBasketItemViewModelBuilderContext;
            _basketItemViewModelBuilder = basketItemViewModelBuilder;
            _w3ArticleService = w3ArticleService;
            _articleSubTitleService = articleSubTitleService;
            _applicationContext = applicationContext;
            _solexBusiness = solexBusiness;
            _expressPlusViewModelBuilder = expressPlusViewModelBuilder;
            _priceViewModelBuilder = priceViewModelBuilder;
            _tryCardPrid = membershipConfigurationService.GetTryCardPrid();
            _fnacPlusCardTypes = membershipConfigurationService.FnacPlusCardTypes;
            _fnacPlusTryCardType = membershipConfigurationService.FnacPlusTryCardType;
            _compositeRichBasketItemChildrenViewModelBuilder = compositeRichBasketItemChildrenViewModelBuilder;
            _promotionTemplateViewModelBuilder = promotionTemplateViewModelBuilder;
            _switchProvider = switchProvider;
            _articleAvailabilityService = articleAvailabilityService;
            _adherentCardArticleService = adherentCardArticleService;
            _warantyTypes = new List<int>()
            {
                //should be in config ?
                16011, 16101, 16102, 16103, 16104, 16105, 16106, 16111, 19001, 19007, 19008, 19002, 19006, 19005, 19011, 19009, 19010, 19004, 19003
            };
            _priceHelper = _applicationContext.GetPriceHelper();

            _donationTypeId = int.Parse(_applicationContext.GetAppSetting("DonationTypeId"));
            _forcedMaxQuantity = int.Parse(_applicationContext.GetAppSetting("ForcedMaxQuantity") ?? DefaultForcedMaxQuantity);
            _useMaxQuantityAllowedField = switchProvider.IsEnabled("orderpipe.pop.pomcore.usemaxquantityallowedfield");
        }

        private class DummyRichBasketItemViewModelBuilderCallbacks : IRichBasketItemViewModelBuilderCallbacks
        {
            public RichBasketItemViewModel BuildingMarketPlaceViewModel(RichBasketItemViewModel current, MarketPlaceArticle marketPlaceArticle)
            {
                return current;
            }

            public RichBasketItemViewModel BuildingStandardViewModel(RichBasketItemViewModel current, StandardArticle standardArticle)
            {
                return current;
            }

            public AvailabilityViewModel OnComputingAvailability(StandardArticle standardArticle)
            {
                return new AvailabilityViewModel();
            }
        }

        public RichBasketItemViewModel Build(BaseArticle baseArticle, IOF orderForm)
        {
            return Build(baseArticle, new DummyRichBasketItemViewModelBuilderCallbacks(), orderForm);
        }

        protected virtual RichBasketItemViewModel BuildItemViewModel(BasketItemWithPictureViewModel item)
        {
            return new RichBasketItemViewModel(item);
        }

        public virtual RichBasketItemViewModel Build(BaseArticle baseArticle, IRichBasketItemViewModelBuilderCallbacks callbacks, IOF orderForm)
        {
            var aboIlliData = _context.AboIlliData;
            BasketItemWithPictureViewModel sourceItem = null;
            //gestion de l'affichage des articles indispo 
            var isAvailable = false;

            var christmasData = orderForm.GetPrivateData<ChristmasData>(false);

            var uniqueId = string.Empty;
            var standardArticle = baseArticle as StandardArticle;

            if (standardArticle != null) //cas Fnac.com
            {
                if (ShouldIgnoreArticle(standardArticle))
                {
                    return null;
                }

                sourceItem = _basketItemViewModelBuilder.BuildItemStandardArticleWithPicture(standardArticle, _context.GlobalPromotions);
                isAvailable = standardArticle.Availability != StockAvailability.None;
                uniqueId = $"{baseArticle.SellerID.GetValueOrDefault()}-{Guid.Empty}-{baseArticle.ProductID.GetValueOrDefault()}";
            }
            else if (baseArticle is MarketPlaceArticle mpArticle) //cas MP
            {
                sourceItem = _basketItemViewModelBuilder.BuildItemMpArticleWithPicture(mpArticle);
                isAvailable = mpArticle.Quantity > 0 && mpArticle.AvailableQuantity > 0;
                uniqueId = $"{baseArticle.SellerID.GetValueOrDefault()}-{mpArticle.Offer.Reference}-{baseArticle.ProductID.GetValueOrDefault()}";
            }
            else
            {
                return null;
            }

            var basketItemViewModel = BuildItemViewModel(sourceItem);
            basketItemViewModel.Quantity.DefaultMax = DefaultMax;
            basketItemViewModel.UniqueId = uniqueId;
            basketItemViewModel.IsEligibleTo5pctReduction = false;

            basketItemViewModel.IsAvailable = isAvailable;
            var w3Article = _w3ArticleService.GetArticle(baseArticle.ToArticleReference());
            var dtoArticle = w3Article.DTO;


            if (baseArticle.Type == ArticleType.Saleable)
            {
                basketItemViewModel.ShowIfArticleIsFreePruduct = true;
                basketItemViewModel.ReferentialId = baseArticle.Referentiel;
                basketItemViewModel.ProductId = baseArticle.ProductID.GetValueOrDefault(0);
                basketItemViewModel.Title = baseArticle.Title;
                var urlWriter = w3Article.GetPageUrl();
                basketItemViewModel.ProductUrl = !UrlWriter.IsNullOrEmpty(urlWriter) ? urlWriter.ToString() : null;
                basketItemViewModel.SubTitle = _articleSubTitleService.BuildSubTitle(baseArticle, dtoArticle);

                basketItemViewModel.CanUpdate = standardArticle == null || !standardArticle.IsEbook;
                basketItemViewModel.CanDelete = true;

                basketItemViewModel.IsDonation = _donationTypeId == baseArticle.TypeId.GetValueOrDefault();

                if (baseArticle is Service || basketItemViewModel.IsDonation)
                {
                    basketItemViewModel.ProductUrl = null;
                }

                if (baseArticle is Service || _warantyTypes.Contains(baseArticle.TypeId.GetValueOrDefault()))
                {
                    basketItemViewModel.CanDelete = true;
                    basketItemViewModel.CanUpdate = false;
                }
                if (baseArticle.TypeId.HasValue && aboIlliData.TypeArtIds.Contains(baseArticle.TypeId.Value))
                {
                    basketItemViewModel.CanDelete = true;
                    basketItemViewModel.CanUpdate = false;
                    basketItemViewModel.ProductUrl = null;
                    basketItemViewModel.ExpressPlusInfo = BuildExpressPlusModel(aboIlliData);
                }

                if (standardArticle != null && _adherentCardArticleService.IsAdherentCard(standardArticle.ProductID.GetValueOrDefault(0)))
                {
                    basketItemViewModel.CanDelete = true;
                    basketItemViewModel.CanUpdate = false;
                    basketItemViewModel.ProductUrl = null;
                    basketItemViewModel.IsMembershipRenewCard = baseArticle.TypeId.GetValueOrDefault() == (int)CategoryCardType.Renew
                        || baseArticle.TypeId.GetValueOrDefault() == (int)CategoryCardType.RenouvellementFnacPlus
                        || baseArticle.TypeId.GetValueOrDefault() == (int)CategoryCardType.ConversionFnacPlus;
                    basketItemViewModel.IsMembershipRecruitCard = baseArticle.TypeId.GetValueOrDefault() == (int)CategoryCardType.Recruit
                        || baseArticle.TypeId.GetValueOrDefault() == (int)CategoryCardType.SouscriptionFnacPlusTrial
                        || baseArticle.TypeId.GetValueOrDefault() == (int)CategoryCardType.SouscriptionFnacPlusTrialDecouverte;
                    basketItemViewModel.IsTryCard = baseArticle.ProductID == _tryCardPrid;
                    basketItemViewModel.IsFnacPlusTryCard = baseArticle.TypeId.GetValueOrDefault() == _fnacPlusTryCardType;
                    basketItemViewModel.IsFnacPlusCard = _fnacPlusCardTypes.Contains(baseArticle.TypeId.GetValueOrDefault());
                }

                if (standardArticle != null)
                {
                    if (!standardArticle.EnableUpdateQuantity)
                    {
                        basketItemViewModel.CanUpdate = false;
                    }

                    basketItemViewModel.CanBeDelivredBeforChristmas = christmasData != null && christmasData.GetShippingBeforChristmasEligibility(standardArticle.ProductID.GetValueOrDefault());

                    basketItemViewModel.CanBeDelivredBeforChristmasPicto = christmasData?.PictoUiresourceKey != null ? _applicationContext.GetMessage(christmasData?.PictoUiresourceKey) : string.Empty;

                    basketItemViewModel.CanBeDelivredBeforChristmasWording = christmasData?.BasketUiResourceKey != null ? _applicationContext.GetMessage(christmasData?.BasketUiResourceKey) : string.Empty;

                    // les articles qui n'ont pas de gestion stock ou qui sont en précommandes ont une quantité max de DefaultMax
                    basketItemViewModel.Quantity.Max = DefaultMax;

                    basketItemViewModel = callbacks.BuildingStandardViewModel(basketItemViewModel, standardArticle);

                    if (!isAvailable)
                    {
                        basketItemViewModel.Availability = new AvailabilityViewModel()
                        {
                            Availability = _applicationContext.GetMessage("orderpipe.pop.basket.unavailable.article")
                        };
                    }
                    else
                    {
                        if (!(baseArticle.TypeId.HasValue && aboIlliData.TypeArtIds.Contains(baseArticle.TypeId.Value))
                            && !_adherentCardArticleService.IsAdherentCard(standardArticle.ProductID.GetValueOrDefault(0))
                            && baseArticle.TypeId != _donationTypeId
                            && orderForm.GetPrivateData<SelfCheckoutData>(false) == null)
                        {
                            basketItemViewModel.Availability = _articleAvailabilityService.GetAvailability(standardArticle, callbacks.OnComputingAvailability);

                            if (_useMaxQuantityAllowedField && standardArticle.HasLimitedQuantity && standardArticle.MaxQuantityAllowed > 0)
                            {
                                basketItemViewModel.Quantity.Max = standardArticle.MaxQuantityAllowed;
                            }
                            else if ((basketItemViewModel.Availability.HasQuantityMax || basketItemViewModel.Availability.HasLimitedQuantity) && standardArticle.AvailableStock > 0)
                            {
                                basketItemViewModel.Quantity.Max = standardArticle.AvailableStock;
                            }
                            else if (standardArticle.Availability == StockAvailability.ClickAndCollectOnly && orderForm is PopOrderForm)
                            {
                                var popOrderForm = orderForm as PopOrderForm;
                                basketItemViewModel.Quantity.Max = GetStomQuantity(popOrderForm, standardArticle);
                            }
                        }
                    }

                    var childrens = _compositeRichBasketItemChildrenViewModelBuilder.Build(standardArticle, _context.GlobalPromotions, _context.ApplyAdherentPromotion).ToArray();
                    var hasAnyCrossChildItem = childrens.Any(c => c is CrossSellRichItemChildrenViewModel);
                    basketItemViewModel.HasCrossSellChildItems = hasAnyCrossChildItem;
                    basketItemViewModel.WithChildren(childrens);
                }
                else if (baseArticle is MarketPlaceArticle)
                {
                    var siteContext = _applicationContext.GetSiteContext();
                    var marketPlaceArticle = baseArticle as MarketPlaceArticle;

                    basketItemViewModel.IsPureMarketPlace = marketPlaceArticle.Offer.ArticleReference.Catalog == DTO.ArticleCatalog.MarketPlace;

                    basketItemViewModel.AvailabilityFnacId = (int)AvailabilityEnum.Indisponible;
                    basketItemViewModel.TotalOfferCount = marketPlaceArticle.AvailableQuantity;
                    basketItemViewModel.IsNumerical = false;

                    basketItemViewModel.OfferId = marketPlaceArticle.Offer.Reference;

                    basketItemViewModel.Comments = marketPlaceArticle.Offer.Comments;
                    basketItemViewModel.Status = marketPlaceArticle.Offer.ProductSubStatusLabel;

                    basketItemViewModel = callbacks.BuildingMarketPlaceViewModel(basketItemViewModel, marketPlaceArticle);

                    basketItemViewModel.Quantity.Max = marketPlaceArticle.AvailableQuantity;

                    var availabilityLabel = isAvailable ? baseArticle.AvailabilityLabel : _applicationContext.GetMessage("orderpipe.pop.basket.unavailable.article");

                    basketItemViewModel.Availability = new AvailabilityViewModel()
                    {
                        Availability = baseArticle.AvailabilityLabel
                    };

                    basketItemViewModel.CanBeDelivredBeforChristmas = christmasData != null && christmasData.GetShippingBeforChristmasEligibility(marketPlaceArticle.ProductID.GetValueOrDefault());

                    basketItemViewModel.CanBeDelivredBeforChristmasPicto = christmasData?.PictoUiresourceKey != null ? _applicationContext.GetMessage(christmasData?.PictoUiresourceKey) : string.Empty;

                    basketItemViewModel.CanBeDelivredBeforChristmasWording = christmasData?.BasketUiResourceKey != null ? _applicationContext.GetMessage(christmasData?.BasketUiResourceKey) : string.Empty;
                }

                basketItemViewModel.Quantity.HasMax = true;
                basketItemViewModel.Quantity.Max = Math.Min(basketItemViewModel.Quantity.Max, _forcedMaxQuantity);
            }

            basketItemViewModel.Quantity.Count = baseArticle.Quantity.GetValueOrDefault(1);

            DefineArticleQuantityFromMax(baseArticle, basketItemViewModel);

            var shopInfosData = orderForm.GetPrivateData<ShopInfosData>(create: false);

            if (isAvailable || shopInfosData != null)
            {
                basketItemViewModel.Price = _priceViewModelBuilder.Build(baseArticle, _context.ApplyAdherentPromotion);
            }

            if (baseArticle.Type == ArticleType.Saleable && standardArticle != null)
            {
                var hstandardArticles = standardArticle.IsPE && standardArticle.IsBook && !standardArticle.IsNumerical;
                var isShopShipping = orderForm is PopOrderForm &&
                        (orderForm as PopOrderForm).ShippingMethodEvaluation.FnacCom.HasValue &&
                        (orderForm as PopOrderForm).ShippingMethodEvaluation.FnacCom.Value.SelectedShippingMethodEvaluationType == ShippingMethodEvaluationType.Shop;

                if (hstandardArticles)
                {
                    var reducedPriceData = orderForm.GetPrivateData<ReducedPriceData>(create: false);

                    if (_switchProvider.IsEnabled("orderpipe.pop.activatebook5pc") &&
                        reducedPriceData != null && reducedPriceData.ReducePriceList.TryGetValue(standardArticle.ProductID, out var reducedPrice))
                    {
                        basketItemViewModel.IsEligibleTo5pctReduction = !isShopShipping;
                        if (!isShopShipping)
                        {
                            var promotions = reducedPriceData.Promotions;
                            var templates = _promotionTemplateViewModelBuilder.Build(promotions);
                            foreach (var template in templates)
                            {
                                basketItemViewModel.Price.WithTemplate(template);
                            }
                        }

                        basketItemViewModel.ReducedPriceBook5pc = new ReducedPriceViewModel()
                        {
                            Price = _priceHelper.Format(basketItemViewModel.Quantity.Count * reducedPrice, PriceFormat.CurrencyAsDecimalSeparator),
                            Reduction = _priceHelper.Format(basketItemViewModel.Quantity.Count * (standardArticle.SalesInfo.StandardPrice.TTC - reducedPrice), PriceFormat.CurrencyAsDecimalSeparator)
                        };
                    }
                }

                basketItemViewModel.IsShopShipping = isShopShipping;
            }

            return basketItemViewModel;
        }

        protected bool ShouldIgnoreArticle(StandardArticle standardArticle) => standardArticle.Hidden;

        protected virtual void DefineArticleQuantityFromMax(BaseArticle baseArticle, RichBasketItemViewModel basketItemViewModel)
        {
            if (basketItemViewModel.Quantity.Max != 0)
            {
                baseArticle.Quantity = Math.Min(basketItemViewModel.Quantity.Max, baseArticle.Quantity.GetValueOrDefault(1));
            }

            if (baseArticle.CollectInShop && basketItemViewModel.Quantity.MaxClickAndCollect != 0)
            {
                baseArticle.Quantity = Math.Min(basketItemViewModel.Quantity.MaxClickAndCollect, baseArticle.Quantity.GetValueOrDefault(1));
            }
        }

        private ILineGroup GetLineGroup(PopOrderForm popOrderForm, StandardArticle standardArticle)
        {
            foreach (var lineGroup in popOrderForm.LineGroups)
                if (lineGroup.Articles.Any(a => a.Equals(standardArticle)))
                    return lineGroup;

            return null;
        }

        private int GetStomQuantity(PopOrderForm popOrderForm, StandardArticle standardArticle)
        {
            var parentLineGroup = GetLineGroup(popOrderForm, standardArticle);

            if (parentLineGroup != null)
            {
                var maybeShippingMethodEvaluationGroup = popOrderForm.ShippingMethodEvaluation.Seller(parentLineGroup.Seller.SellerId);

                if (maybeShippingMethodEvaluationGroup.HasValue)
                {
                    var shippingMethodEvaluationGroup = maybeShippingMethodEvaluationGroup.Value;

                    if (shippingMethodEvaluationGroup.SelectedShippingMethodEvaluationType == ShippingMethodEvaluationType.Shop)
                    {
                        var shopAddress = shippingMethodEvaluationGroup.ShopShippingMethodEvaluationItem.Value.ShippingAddress;

                        if (int.TryParse(shopAddress.RefUG, out var refUG))
                        {
                            var storeStockInformations = _solexBusiness.GetStoreStockInformations(refUG.ToString(), standardArticle);

                            if (storeStockInformations != null && storeStockInformations.TotalAvailableClickAndCollect > 0)
                            {
                                return storeStockInformations.TotalAvailableClickAndCollect;
                            }
                        }
                    }
                }
            }
            return DefaultMax;
        }

        public ExpressPlusViewModel BuildExpressPlusModel(AboIlliData aboIlliData)
        {
            return _expressPlusViewModelBuilder.Build(aboIlliData);
        }

        public decimal GetPrice(BaseArticle article)
        {
            var price = article.Quantity.GetValueOrDefault(0) * article.PriceUserEur.GetValueOrDefault(0);

            if (article is StandardArticle)
            {
                var stdArt = article as StandardArticle;
                if (stdArt.EcoTaxEur.HasValue)
                {
                    price += stdArt.EcoTaxEur.Value * stdArt.Quantity.Value;
                }

                foreach (var service in stdArt.Services)
                {
                    price += (service.Quantity.GetValueOrDefault(0) * service.PriceUserEur.GetValueOrDefault(0));
                }
            }
            return price;
        }
    }
}
