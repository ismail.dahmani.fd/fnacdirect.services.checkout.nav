using System;
using System.Collections.Generic;

namespace FnacDirect.OrderPipe.Base.Business.Operations
{
    /// <summary>
    /// 
    /// </summary>
    public class DeliveredForChrismasRule
    {
        /// <summary>
        /// Les identifiants de disponibilités élligibles au livré à temps
        /// </summary>
        public IEnumerable<int> ElligibleAvailibilityIds { get; set; }

        /// <summary>
        /// Les méthodes de livraison elligibles 
        /// </summary>
        public IEnumerable<int> ElligibleShippingMethods { get; set; }

        /// <summary>
        /// Indique à quelle phase nous sommes
        /// </summary>
        public string StepNumber { get; set; }

        /// <summary>
        /// Indique que c'est la dernière phase
        /// </summary>
        public bool IsLastStep { get; set; }

        /// <summary>
        /// Indique qu'il faut prendre en compte que les produits techniques
        /// </summary>
        public bool IsTechnicalOnly { get; set; }
        /// <summary>
        /// Date de debut de la phase
        /// </summary>
        public DateTime StartDate { get; set; }
        /// <summary>
        /// Date de fin de la phase 
        /// </summary>
        public DateTime EndDate { get; set; }
        /// <summary>
        /// Picto associé
        /// </summary>
        public string PictoUiressourceKey { get; set; }

        /// <summary>
        /// Picto pipe
        /// </summary>
        public string PipePictoUiresourceKey { get; set; }

        /// <summary>
        /// Wording associé 
        /// </summary>
        public string SearchWordingUiressourceKey { get; set; }

        /// <summary>
        /// Wording associé pour la FA
        /// </summary>
        public string ItemPageWordingUiressourceKey { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string ShippingPopinWordingUiressourceKey { get; set; }

        /// <summary>
        /// clé de la ressource pour le wording dans le layer.
        /// </summary>
        public string LayerWordingUiressourceKey { get; set; }
        /// <summary>
        /// clé de la ressource pour le wording dans le panier recap.
        /// </summary>
        public string BasketWordingUiressourceKey { get; set; }
        /// <summary>
        /// clé de la ressource pour le wording dans les pages livraison.
        /// </summary>
        public string ShippingWordingUiressourceKey { get; set; }
        /// <summary>
        /// Date de l'évènement pour la comparer avec les dates de livraion estimées.
        /// </summary>
        public DateTime EventDate { get; set; }


    }
}
