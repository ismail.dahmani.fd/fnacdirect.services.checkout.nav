using System.Xml.Serialization;

namespace FnacDirect.OrderPipe.Base.Model
{
    public class CCVAccountBillingData: GroupData
    {
        [XmlIgnore]
        public decimal CCVAccountBalance;

    }

    public class VirtualGiftCheckBillingMethod : BillingMethod
    {
        public const int IdBillingMethod = 4;

        [XmlIgnore]
        private decimal? _AmountEur;

        public decimal? AmountEur
        {
            get
            {
                return _AmountEur;
            }
            set
            {
                _AmountEur = value;
            }
        }

        public bool IsCreatedByFidelityEuros { get; set; }
       

        public VirtualGiftCheckBillingMethod()
        {
            BillingMethodId = IdBillingMethod;
            Name = "VirtualGiftCheckBillingMethod";
            Priority = 10;
            InitialStatus = 3;
            IsCreatedByFidelityEuros = false;
        }
    }

}
