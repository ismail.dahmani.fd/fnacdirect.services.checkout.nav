﻿namespace FnacDirect.OrderPipe.BaseMvc.Web.Services.ShipToStore
{
    public class StoreSearchGeoResult
    {
        private readonly double _distanceFromSearch;
        private readonly FnacStore.Model.FnacStore _fnacStore;
        private readonly string _name; 
        public FnacStore.Model.FnacStore FnacStore
        {
            get { return _fnacStore; }
        }

        public double DistanceFromSearch
        {
            get { return _distanceFromSearch; }
        }
        public string Name
        {
            get { return _name; }
        }

        public StoreSearchGeoResult(FnacStore.Model.FnacStore fnacStore, double distanceFromSearch)
        {
            _fnacStore = fnacStore;
            _distanceFromSearch = distanceFromSearch;
        }
        public StoreSearchGeoResult(FnacStore.Model.FnacStore fnacStore, double distanceFromSearch, string name)
        {
            _fnacStore = fnacStore;
            _distanceFromSearch = distanceFromSearch;
            _name = name; 
        }
    }
}