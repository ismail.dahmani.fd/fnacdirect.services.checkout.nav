using System.Xml.Serialization;

namespace FnacDirect.OrderPipe.Config
{
	public class PipeConfig
	{
		[XmlElement("Pipe")]
		public PipeList Pipe { get; set; }
	}
}