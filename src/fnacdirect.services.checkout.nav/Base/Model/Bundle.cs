using System;
using System.Xml.Serialization;

namespace FnacDirect.OrderPipe.Base.Model
{
    [Serializable]
    public class Bundle : StandardArticle, IBundle
    {
        [XmlIgnore]
        private ArticleList _bundleComponents;

        public ArticleList BundleComponents
        {
            get
            {
                if (_bundleComponents == null)
                    _bundleComponents = new ArticleList();
                return _bundleComponents;
            }
            set
            {
                _bundleComponents = value;
            }
        }

        public override StandardArticle Copy<T>()
        {
            var copy = base.Copy<T>();
            ((IBundle)copy).BundleComponents = BundleComponents;

            return copy;
        }
    }
}
