using System;

namespace FnacDirect.OrderPipe
{
    /// <summary>
    /// Interface définissant un module de sérialisation d'OrderForm
    /// Il y a une instance de sérialiser par pipe
    /// </summary>
    public interface IOFSerializer
    {
        /// <summary>
        /// Initialisation du module
        /// </summary>
        /// <param name="pipe">OrderPipe utilisant le sérialiseur</param>
        void Initialize(IOP pipe);

        /// <summary>
        /// Déserialisation de l'orderform
        /// </summary>
        /// <param name="ofs">OrderForm sérialisé</param>
        /// <returns>OrderForm désérialisé</returns>
        IOF Hydrate(string ofs);

        /// <summary>
        /// Serialisation de l'orderform
        /// </summary>
        /// <param name="of">OrderForm désérialisé</param>
        /// <returns>OrderForm sérialisé</returns>
        string Dehydrate(IOF of);
    }

}
