using FnacDirect.Contracts.Online.IModel;
using FnacDirect.Contracts.Online.Model;
using FnacDirect.OrderPipe.Base.Model;
using DTO = FnacDirect.Contracts.Online.Model;

namespace FnacDirect.OrderPipe.Base.Business
{
    public class ArticleDetailServiceWithShopGetArticleBehavior : ArticleDetailService, IArticleDetailServiceWithShopGetArticleBehavior
    {
        /// <summary>
        /// Charge l'article en autorisant les articles inactif
        /// </summary>
        /// <param name="siteContext"></param>
        /// <param name="ab"></param>
        /// <param name="ar"></param>
        /// <returns></returns>
        public override DTO.Article GetArticleFromService(StandardArticle prmoArticle, SiteContext siteContext, ArticleReference ar)
        {
            return _articleBusiness.GetArticle(siteContext, ar, ArticleLoadingOption.DefaultAndInactive);
        }
    }
}
