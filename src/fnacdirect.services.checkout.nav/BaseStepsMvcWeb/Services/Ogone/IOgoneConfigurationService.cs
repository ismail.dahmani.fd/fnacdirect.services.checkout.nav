using FnacDirect.Ogone.Configuration;
using FnacDirect.OrderPipe.Base.Model;
using System.Collections.Generic;

namespace FnacDirect.OrderPipe.Base.Proxy.Ogone
{
    public interface IOgoneConfigurationService
    {
        PspMode GetPspMode();
        bool GetWalletEnabled();
        
        decimal GetThresholdAmount();
        AutorisationRule GetTransactionRule(string culture, string debitType, string brand, string orderTransactionRuleType);
        NCErrorCode GetNcErrorCodeByKey(string ncErrorCode);
        string GetBrandExceptionListByCriteria(string culture, string debitType, string brand);
        bool GetBrandAliasByMerchantByCriteria(string culture, string debitType, string brand);
        string EncodeTo64(string pToEncode);

        NCErrorCode GetAliasGatewayNcErrorCodeByKey(string ncErrorCode);
        NCErrorCode GetDirectLinkNcErrorCodeByKey(string ncErrorCode);
        string ComputePspId(OrderTypeEnum orderType, IEnumerable<Article> articles, string accountId, OgoneConfigurationData ogoneConfigurationData);
        string GetPhoneCountryCodeToAddForInstallment();
    }
}
