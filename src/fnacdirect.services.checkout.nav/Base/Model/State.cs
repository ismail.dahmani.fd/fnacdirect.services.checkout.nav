﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FnacDirect.OrderPipe.Base.BaseModel.Model
{
   public class State
    {
        public string CodeState { get; set; }
        public string LabelState { get; set; }
    }
}
