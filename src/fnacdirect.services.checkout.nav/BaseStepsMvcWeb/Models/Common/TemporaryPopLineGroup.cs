﻿namespace FnacDirect.OrderPipe.Base.Model.LineGroups
{
    public class TemporaryPopLineGroup : PopLineGroup
    {
        public override bool IsTemporary
        {
            get
            {
                return true;
            }
        }
    }
}
