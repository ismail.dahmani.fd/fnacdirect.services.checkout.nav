using FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Common.Shipping.Relay;
using FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Common.Shipping.Shop;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Common.Shipping
{
    public class EligibleAddressViewModel : AddressViewModel
    {
        public EligibleAddressViewModel() { }
        public EligibleAddressViewModel(AddressViewModel addressViewModel)
            : base(addressViewModel) { }

        public bool IsEligible { get; set; }

        public bool IsSelected { get; set; }
    }

    public class RelayShopEligibleAddressViewModel : EligibleAddressViewModel
    {
        public RelayShopEligibleAddressViewModel() { }

        public RelayShopEligibleAddressViewModel(EligibleAddressViewModel eligibleAddressViewModel)
            : base(eligibleAddressViewModel) {

        }

        public ShopDetailsViewModel Shop { get; set; }

        public RelayShippingNetworkViewModel Relay { get; set; }
    }

}
