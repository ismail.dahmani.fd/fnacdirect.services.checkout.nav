﻿using System;

namespace FnacDirect.OrderPipe.Base.Business.LogisticLineGroups.Internals.Rules
{
    public interface ILogisticLineGroupGenerationRuleFactory
    {
        ILogisticLineGroupGenerationRule Get(ILogisticLineGroupGenerationRuleDescriptor logisticLineGroupGenerationRuleDescriptor);
    }
}
