using FnacDirect.Customer.Model;
using System.Collections.Generic;

namespace FnacDirect.OrderPipe.Base.Proxy.Customer
{
    public interface ICustomerCreditCardService
    {
        IEnumerable<CreditCardEntity> GetCustomerCreditCards(string userId);
        IEnumerable<CreditCardEntity> GetCustomerCreditCards(string userId, int sortType);
        IEnumerable<CreditCardEntity> GetCustomerCreditCards(string userId, CreditCardPaymentMethodType cardType);
        CreditCardEntity GetCustomerSelfCheckoutCreditCard(string accountReference);
        string GetDefault();
        CreditCardEntity Load(string creditCardReference);
        void SaveCustomerCreditCard(CreditCardEntity creditCardEntity, string accountReference);
        void SetDefault(string creditCardReference);
    }
}
