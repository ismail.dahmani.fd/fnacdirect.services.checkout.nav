﻿using FnacDirect.OrderPipe.Base.Model;
using System.Collections.Generic;

namespace FnacDirect.OrderPipe.BaseMvc.Web.CrossSellBasket
{
    public interface ICrossSellBasketViewModelBuilder
    {
        EmptyCrossSellBasketViewModel Build(IEnumerable<CrossSellBasketItemInput> basketItems, PopOrderForm orderForm);
    }
}
