namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Common.Shipping.Shop
{
    public class ExceptionnalClosingViewModel
    {
        public virtual bool IsOpeningDay
        {
            get
            {
                return false;
            }
        }

        public string Day { get; set; }
    }
}
