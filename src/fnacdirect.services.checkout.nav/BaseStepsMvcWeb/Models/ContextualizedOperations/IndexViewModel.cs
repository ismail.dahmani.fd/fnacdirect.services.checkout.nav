﻿namespace FnacDirect.OrderPipe.BaseMvc.Web.ContextualizedOperations
{
    public class IndexViewModel
    {
        public string PublicId { get; set; }
        public string Token { get; set; }

        public bool InvalidToken { get; set; }
    }
}
