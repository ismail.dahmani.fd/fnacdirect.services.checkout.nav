using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.DataLayer;
using System.Web.Mvc;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Services.Pop.DataLayer
{
    public interface IDataLayerService
    {
        void ComputeShoppingCart(DatalayerViewModel model, ViewResultBase viewResultBase, ControllerContext controllerContext);
        void ComputeTransaction(DatalayerViewModel datalayerViewModel, ViewResultBase viewResultBase, ControllerContext controllerContext);
    }
}
