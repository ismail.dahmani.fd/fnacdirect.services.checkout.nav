﻿
namespace FnacDirect.OrderPipe.Base.Model.Pop
{
    public class InvoicerEbookData : GroupData
    {
        public bool NeedInvoice { get; set; }

        public string Status { get; set; }
    }
}
