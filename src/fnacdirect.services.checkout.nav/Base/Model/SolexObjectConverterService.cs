using SolexModel = FnacDirect.OrderPipe.Base.Model.Solex;
using PopModel = FnacDirect.OrderPipe.Base.Model;
using FnacDirect.Solex.Shipping.Client.Contract;
using FnacDirect.OrderPipe.Base.BaseModel;
using System.Collections.Generic;
using FnacDirect.Technical.Framework.Web.Mvc;
using System.Linq;

namespace FnacDirect.OrderPipe.Base.Business.Shipping
{
    public class SolexObjectConverterService : ISolexObjectConverterService
    {
        private readonly ICountryService _countryService;

        public SolexObjectConverterService(ICountryService countryService)
        {
            _countryService = countryService;
        }

        /// <summary>
        /// Construit l'objet PostalAddress de Solex, à partir des données passés en paramètre.
        /// </summary>
        public PostalAddress BuildPostalAddress(ShippingMethodEvaluationGroup shippingMethodEvaluationGroup, SolexModel.DeliverySlotData deliverySlotData)
        {
            if (!shippingMethodEvaluationGroup.PostalShippingMethodEvaluationItem.HasValue)
            {
                return null;
            }

            var postalAddress = new PostalAddress();
            var countries = _countryService.GetCountries();

            var postalShippingAddress = shippingMethodEvaluationGroup.PostalShippingMethodEvaluationItem.Value.ShippingAddress;

            if (postalShippingAddress != null && postalShippingAddress.Identity != -1)
            {
                postalAddress.Country = countries.FirstOrDefault(c => c.ID == postalShippingAddress.CountryId)?.Code3;
                postalAddress.ZipCode = postalShippingAddress.ZipCode;
                postalAddress.City = postalShippingAddress.City;
                postalAddress.AddressLine = string.Format("{0} {1} {2} {3}", postalShippingAddress.AddressLine1, postalShippingAddress.AddressLine2, postalShippingAddress.AddressLine3, postalShippingAddress.AddressLine4).TrimEnd();
            }
            else
            {
                postalAddress.Default = true;
                postalAddress.Country = countries.FirstOrDefault(c => c.ID == postalShippingAddress.CountryId).Code3;
            }

            if (deliverySlotData != null && (deliverySlotData.HasSelectedSlot))
            {
                postalAddress.UserShippingMethodsBag = new Dictionary<int, SlotDescription>
                {
                    {
                        deliverySlotData.SlotShippingMethodId
                        , new SlotDescription() {
                            SlotType = deliverySlotData.DeliverySlotSelected.SlotType,
                            StartDate = deliverySlotData.DeliverySlotSelected.StartDate,
                            EndDate = deliverySlotData.DeliverySlotSelected.EndDate
                        }
                    }
                };
            }

            return postalAddress;
        }

        /// <summary>
        /// Construit l'objet ShopAddress de Solex, à partir des données passés en paramètre.
        /// Actuellement, seul les propriétés RefUG et Default sont renseignées dans l'objet retourné.
        /// </summary>
        public ShopAddress BuildShopAddress(ShippingMethodEvaluationGroup shippingMethodEvaluationGroup)
        {
            if (!shippingMethodEvaluationGroup.ShopShippingMethodEvaluationItem.HasValue)
            {
                return null;
            }

            var shopShippingMethodEvaluationItem = shippingMethodEvaluationGroup.ShopShippingMethodEvaluationItem.Value;
            var shopShippingAddress = shopShippingMethodEvaluationItem.ShippingAddress;
            var shopAddress = new ShopAddress();

            if (shopShippingAddress != null && shopShippingAddress.RefUG != null && !shopShippingMethodEvaluationItem.IsDummyAddress)
            {
                shopAddress.RefUG = shopShippingAddress.RefUG;
            }
            else
            {
                shopAddress.Default = true;
            }

            return shopAddress;
        }

        /// <summary>
        /// Construit l'objet RelayAddress de Solex, à partir des données passés en paramètre.
        /// Actuellement, seul les propriétés Id et Default sont renseignées dans l'objet retourné.
        /// </summary>
        public RelayAddress BuildRelayAddress(ShippingMethodEvaluationGroup shippingMethodEvaluationGroup)
        {
            if (!shippingMethodEvaluationGroup.RelayShippingMethodEvaluationItem.HasValue)
            {
                return null;
            }

            var relayShippingMethodEvaluationItem = shippingMethodEvaluationGroup.RelayShippingMethodEvaluationItem.Value;
            var relayShippingAddress = relayShippingMethodEvaluationItem.ShippingAddress;
            var relayAddress = new RelayAddress();

            if (relayShippingAddress != null && !relayShippingMethodEvaluationItem.IsDummyAddress)
            {
                relayAddress.Id = relayShippingAddress.Identity.GetValueOrDefault();
            }
            else
            {
                relayAddress.Default = true;
            }

            return relayAddress;
        }

        public IEnumerable<SolexModel.ShippingChoice> ConvertSolexChoicesToShippingChoices(IEnumerable<Choice> choices)
        {
            if (choices == null)
            {
                return Enumerable.Empty<SolexModel.ShippingChoice>();
            }

            return choices.Select(c => ConvertSolexChoiceToShippingChoice(c));
        }

        public SolexModel.ShippingChoice ConvertSolexChoiceToShippingChoice(Choice choice)
        {
            return new SolexModel.ShippingChoice
            {
                AltLabel = choice.AltLabel,
                Label = choice.Label,
                Description = choice.Description,
                MainShippingMethodId = choice.MainShippingMethodId,
                TotalPrice = new PopModel.GenericPrice { Cost = choice.TotalPrice },
                Parcels = ConvertSolexChoiceParcelsToShippingChoiceParcels(choice.Parcels),
                Groups = ConvertSolexChoiceGroupsToShippingChoiceGroups(choice.Groups),
                AppliedPromotions = ConvertSolexChoiceAppliedPromotionsToShippingChoiceAppliedPromotions(choice.AppliedPromotions),
                Weight = choice.Weight,
                CarbonFootprint = choice.CarbonFootprint,
            };
        }

        #region private methods

        private IEnumerable<SolexModel.Promotion> ConvertSolexChoiceAppliedPromotionsToShippingChoiceAppliedPromotions(IEnumerable<Promotion> appliedPromotions)
        {
            if (appliedPromotions == null)
            {
                return Enumerable.Empty<SolexModel.Promotion>();
            }

            return appliedPromotions.Select(promo => ConvertSolexPromotionToShippingChoicePromotion(promo));
        }

        private SolexModel.Promotion ConvertSolexPromotionToShippingChoicePromotion(Promotion promo)
        {
            return new SolexModel.Promotion
            {
                TriggerKey = promo.TriggerKey,
                Quantity = promo.Quantity,
                PromotionCode = promo.PromotionCode,
                ApplyToLogisticTypes = promo.ApplyToLogisticTypes,
                AdditionalRuleParam1 = promo.AdditionalRuleParam1,
                AdditionalRuleParam2 = promo.AdditionalRuleParam2
            };
        }

        private IEnumerable<SolexModel.Group> ConvertSolexChoiceGroupsToShippingChoiceGroups(IEnumerable<Group> groups)
        {
            if (groups == null)
            {
                return Enumerable.Empty<SolexModel.Group>();
            }

            return groups.Select(group => ConvertSolexChoiceGroupToShippingChoiceGroup(group));
        }

        private SolexModel.Group ConvertSolexChoiceGroupToShippingChoiceGroup(Group group)
        {
            return new SolexModel.Group
            {
                Id = group.Id,
                GlobalPrice = new PopModel.GenericPrice { Cost = group.GlobalPrice },
                Items = ConvertSolexItemsInGroupToShippingChoiceItemsInGroup(group.Items)
            };
        }

        private IEnumerable<SolexModel.ItemInGroup> ConvertSolexItemsInGroupToShippingChoiceItemsInGroup(IEnumerable<ItemInGroup> items)
        {
            if (items == null)
            {
                return Enumerable.Empty<SolexModel.ItemInGroup>();
            }
            return items.Select(item => ConvertSolexItemInGroupToShippingChoiceItemInGroup(item));
        }

        private SolexModel.ItemInGroup ConvertSolexItemInGroupToShippingChoiceItemInGroup(ItemInGroup item)
        {
            return new SolexModel.ItemInGroup
            {
                Identifier = ConvertSolexIdentifierToShippingChoiceIdentifier(item.Identifier),
                Quantity = item.Quantity,
                UnitPrice = new Model.GenericPrice { Cost = item.UnitPrice },
                ParcelId = item.ParcelId
            };
        }

        private SolexModel.Identifier ConvertSolexIdentifierToShippingChoiceIdentifier(Identifier identifier)
        {
            return new SolexModel.Identifier
            {
                Prid = identifier?.Prid,
                OfferReference = identifier?.OfferReference,
                PurId = identifier?.PurId
            };
        }

        private IDictionary<int, SolexModel.Parcel> ConvertSolexChoiceParcelsToShippingChoiceParcels(IDictionary<int, Parcel> parcels)
        {
            var solexModelParcels = new Dictionary<int, SolexModel.Parcel>();

            if (parcels == null)
            {
                return solexModelParcels;
            }

            foreach (var parcel in parcels)
            {
                solexModelParcels.Add(parcel.Key, ConvertSolexChoiceParcelToShippingChoiceParcel(parcel.Value));
            }

            return solexModelParcels;
        }

        private SolexModel.Parcel ConvertSolexChoiceParcelToShippingChoiceParcel(Parcel parcel)
        {
            var modelParcel = new SolexModel.Parcel
            {
                ShippingMethodId = parcel.ShippingMethodId,
                ShippingServiceId = parcel.ShippingServiceId,
                DelayInHours = parcel.DelayInHours,
                Source = ConvertSolexChoiceParcelSourceToShippingChoiceParcelSource(parcel.Source)
            };

            if(parcel.SelectedSlotSource != null)
            {
                modelParcel.SelectedSlotSource = ConvertSolexChoiceParcelSourceToShippingChoiceParcelSource(parcel.SelectedSlotSource);
            }

            return modelParcel;
        }

        private SolexModel.Source ConvertSolexChoiceParcelSourceToShippingChoiceParcelSource(Source source)
        {
            return new SolexModel.Source
            {
                DeliveryDateMin = source.DeliveryDateMin,
                DeliveryDateMax = source.DeliveryDateMax,
                ShippingDate = source.ShippingDate,
                OrderDateMax = source.OrderDateMax,
                StockLocation = new SolexModel.StockLocation
                {
                    Identifier = source.StockLocation.Identifier,
                    Type = source.StockLocation.Type
                },
                ShippingLocation = new SolexModel.ShippingLocation
                {
                    Identifier = source.ShippingLocation.Identifier,
                    Type = source.ShippingLocation.Type
                }
            };
        }

        #endregion
    }
}
