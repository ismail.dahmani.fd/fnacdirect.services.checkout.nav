using System;

namespace FnacDirect.OrderPipe.Base.Model.TaxCalculation
{
    public class TaxCalculationQueryItem : Item
    {
        public OrderPipeSalesInfo SalesInfo { get; set; }
    }

    public class TaxCalculationResultItem : Item
    {
        public TaxCalculationPriceResult ItemPrice { get; set; }
        public TaxCalculationPriceResult ShippingPrice { get; set; }
        public TaxCalculationPriceResult WrapCost { get; set; }
    }

    public abstract class Item
    {
        public Identifier Identifier { get; set; }
        public bool IsDematItem { get; set; }
        public bool IsService { get; set; }
        public int SellerId { get; set; }
    }

    public class Identifier
    {
        public int Prid { get; set; }
        public Guid? OfferReference { get; set; }

    }
}
