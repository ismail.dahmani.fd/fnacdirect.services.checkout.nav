﻿using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using FnacDirect.OrderPipe.Config;

namespace FnacDirect.OrderPipe.CoreRouting
{
    public class MvcOrderPipeRouteHandler : OrderPipeRouteHandler
    {
        protected override IHttpHandler GetHttpHandlerFor(PipeExecutionResult pipeExecutionResult, RequestContext requestContext)
        {
            return new MvcHandler(requestContext);
        }

        protected override IHttpHandler ApplyTransferifRequired(IHttpHandler httpHandler, PipeExecutionResult pipeExecutionResult, RequestContext requestContext)
        {
            return httpHandler;
        }
    }
}
