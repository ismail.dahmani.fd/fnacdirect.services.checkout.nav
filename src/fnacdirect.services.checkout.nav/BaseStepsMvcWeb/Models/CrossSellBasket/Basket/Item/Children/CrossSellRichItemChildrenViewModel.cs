using FnacDirect.Technical.Framework.Web.Mvc;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Basket.Children
{
    public class CrossSellRichItemChildrenViewModel : ChildBasketItemViewModel
    {
        public string QuantityLabel { get; set; }

        public int ProductId { get; set; }
        public string Title { get; set; }
        public string Price { get; set; }
        public string PriceNoVAT { get; set; }
        public string PriceToDisplay { get; set; }

        public string UnitPrice { get; set; }
        public string UnitPriceNoVAT { get; set; }
        public string UnitPriceToDisplay { get; set; }

        public string UnitMonthlyPrice { get; set; }
        public string UnitMonthlyPriceNoVAT { get; set; }
        public string UnitMonthlyPriceToDisplay { get; set; }

        public string MonthlyPrice { get; set; }
        public string MonthlyPriceNoVAT { get; set; }
        public string MonthlyPriceToDisplay { get; set; }

        public string MoreInfoLink { get; set; }
        public CrossSellRichItemChildrenViewModelType Type { get; set; }

        public PriceDetailsViewModel PriceDetails { get; set; }

        public bool IsCrossSell
        {
            get
            {
                return true;
            }
        }        
    }
}
