﻿using FnacDirect.OrderPipe.Config;

namespace FnacDirect.OrderPipe
{
    /// <summary>
    /// Classe fortement typée définissant une étape interactive.
    /// Le type des OrderForm, données de groupe et données d'étape sont spécifiés pour une 
    /// utilisation directe par les classes concrètes.    /// 
    /// </summary>
    /// <typeparam name="TOrderForm">Type de l'orderform</typeparam>
    /// <typeparam name="TStepData">Type de la donnée d'étape</typeparam>
    /// <typeparam name="TGroupData">Type de la donnée de groupe</typeparam>
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1005:AvoidExcessiveParametersOnGenericTypes")]
    public abstract class InteractiveStep<TOrderForm, TStepData, TGroupData> : Step<TOrderForm, TStepData, TGroupData>, IInteractiveStep
        where TOrderForm : OF
        where TStepData : StepData, new()
        where TGroupData : GroupData, new()
    {
        /// <summary>
        /// Par défaut, les étapes interactives ne sont pas accessible directement
        /// </summary>
        public virtual bool IsDirect(OPContext opContext)
        {
            return IsDirectDefault;
        }

        public bool SecurityAgnostic { get; set; }

        public bool Secured { get; set; }

        public bool Authenticated { get; set; }

        public virtual void Reset(OPContext opContext)
        {

        }

        public virtual UIDescriptor UIDescriptor { get; set; }

        public bool CanUseDirectAccess { get; set; }

        public bool IsDirectDefault { get; set; }

        public bool ExitOnContinue { get; set; }

        public string Pass2Shortcut { get; set; }

        /// <summary>
        /// Dans le cas ou l'on authorise pas un accès directe à l'étape, cette propriété
        /// permet de déterminer si l'étape en cours dans le chemin de fer est l'étape précédante
        /// ou suivante.
        /// </summary>
        public NoDirectAccessBehavior NoDirectAccessBehavior { get; set; }

        /// <summary>
        /// Exécution d'une étape du pipe
        /// </summary>
        /// <param name="context">Contexte d'exécution</param>
        /// <param name="res">Résultat du pipe</param>
        /// <param name="force">Vrai si il faut forcer l'affichage de cette étape</param>
        /// <returns></returns>
        public override bool Run(OPContext context, PipeResult res, bool force)
        {
            using (new Stopwatch("Running " + Name))
            {

                if (context.ContinueMode && context.ContinuingStep == Name)
                {
                    Stopwatch.Current.Result += "continue ";
                    res.LastStepReturn = Process(context);
                    Stopwatch.Current.Result += "process ";
                    // Retour Next, on continue l'exécution du pipe, sinon, on sort
                    context.ContinueMode = false;
                    if (res.LastStepReturn != StepReturn.Next || ExitOnContinue)
                    {
                        res.UIDescriptor = UIDescriptor;
                        res.CurrentStep = this;
                        context.OF.CurrentStep = null;
                        return true;
                    }
                }

                // Phase préalable
                PreCalc(context);
                Stopwatch.Current.Result += "precalc ";

                // Suppression des contraintes positionnées par l'étape courante
                CollectionUtils.RemoveItemsFromCollection(context.OF.Constraints, delegate (Constraint c)
                {
                    return c.ConstrainerStepName == Name;
                });

                // if we are in the Interactive Step Run and if the last step is a Forward, then we force the process
                if (res.LastStepReturn is StepReturnForward)
                {
                    force = ((StepReturnForward)res.LastStepReturn).DoForce;
                }

                // Phase de vérification
                context.ForceMode = force;
                StepReturn ret;
                try
                {
                    ret = Check(context);
                }
                finally
                {
                    context.ForceMode = false;
                }
                Stopwatch.Current.Result += "check ";
                res.LastStepReturn = ret;


                // Sortie de pipe : rembobinage
                if (ret == StepReturn.Rewind && !force)
                    return true;

                // Sortie de pipe : goto step
                if (ret is StepReturnForward && !force)
                    return true;

                if (ret == StepReturn.ForceSkip)
                {
                    ret = StepReturn.Skip;
                    force = false;
                }

                // Continuation avec exécution (éventuellement forcée)
                if (ret == StepReturn.Process || force)
                {
                    res.UIDescriptor = UIDescriptor;
                    res.CurrentStep = this;
                    Stopwatch.Current.Result += "redirecting ";
                    return true;
                }
                return false;
            }
        }
    }
}