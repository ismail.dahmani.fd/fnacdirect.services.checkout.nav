﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace FnacDirect.OrderPipe.CoreRouting.Rules
{
    public class RedirectHttpHandler : IHttpHandler
    {
        private readonly string _target;

        public RedirectHttpHandler(string target)
        {
            _target = target;
        }

        public bool IsReusable
        {
            get { return true; }
        }

        public void ProcessRequest(HttpContext context)
        {
            context.Response.Redirect(_target);
        }
    }
}
