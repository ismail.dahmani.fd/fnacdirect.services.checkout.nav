﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FnacDirect.OrderPipe.Base.Business.Neolane
{
    public interface INeolaneService
    {
        void Send(string content);
    }
}
