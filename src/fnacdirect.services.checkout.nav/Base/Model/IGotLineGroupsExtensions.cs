using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.OrderPipe.Base.Model.Facets;
using System.Linq;

namespace FnacDirect.OrderPipe
{
    public static class IGotLineGroupsExtensions
    {
        public static bool HasDematSoftArticle(this IGotLineGroups iContainsLineGroups)
        {
            return (iContainsLineGroups.OrderType & OrderTypeEnum.DematSoft) == OrderTypeEnum.DematSoft;
        }

        public static bool HasOnlyMarketPlaceArticle(this IGotLineGroups iContainsLineGroups)
        {
            return iContainsLineGroups.LineGroups.All(lg => lg.HasMarketPlaceArticle);
        }

        /// <summary>
        /// Obtient une valeur déterminant si le type de commande globale de l'OrderForm 
        /// correspond à des retraits en magasin.
        /// </summary>
        /// <returns></returns>
        public static bool HasClickAndCollectArticle(this IGotLineGroups iContainsLineGroups)
        {
            return iContainsLineGroups.LineGroups.Any(l => l.HasCollectInStoreArticles);
        }

        /// <summary>
        /// Obtient une valeur déterminant si le type de commande globale correspond à une commande Fnac (Fnac ou Pro).
        /// </summary>
        /// <returns></returns>
        public static bool HasFnacComArticle(this IGotLineGroups iContainsLineGroups)
        {
            return (iContainsLineGroups.OrderType & OrderTypeEnum.FnacCom) == OrderTypeEnum.FnacCom
                || (iContainsLineGroups.OrderType & OrderTypeEnum.Pro) == OrderTypeEnum.Pro;
        }

        /// <summary>
        /// Obtient une valeur déterminant si le type de commande globale de l'OrderForm 
        /// correspond à une commande sur l'espace vendeurs.
        /// </summary>
        /// <returns></returns>
        public static bool HasMarketPlaceArticle(this IGotLineGroups iContainsLineGroups)
        {
            return (iContainsLineGroups.OrderType & OrderTypeEnum.MarketPlace) == OrderTypeEnum.MarketPlace;
        }

        public static bool HasMarketPlaceAndClickAndCollectArticle(this IGotLineGroups iContainsLineGroups)
        {
            return iContainsLineGroups.HasClickAndCollectArticle() && iContainsLineGroups.HasMarketPlaceArticle();
        }

        public static bool HasMarketPlaceAndClickAndFnacComArticle(this IGotLineGroups iContainsLineGroups)
        {
            return (iContainsLineGroups.HasFnacComArticle() || iContainsLineGroups.HasClickAndCollectArticle()) && iContainsLineGroups.HasMarketPlaceArticle();
        }

        /// <summary>
        /// Obtient une valeur déterminant si la commande globale contient des articles FnacShop.
        /// </summary>
        /// <returns></returns>
        public static bool HasShopArticle(this IGotLineGroups iContainsLineGroups)
        {
            return (iContainsLineGroups.OrderType & OrderTypeEnum.IntraMag) == OrderTypeEnum.IntraMag || (iContainsLineGroups.OrderType & OrderTypeEnum.IntraMagPE) == OrderTypeEnum.IntraMagPE;
        }

        /// <summary>
        /// Obtient une valeur déterminant si la commande globale contient des articles FnacShop PT.
        /// </summary>
        /// <returns></returns>
        public static bool HasShopPTArticle(this IGotLineGroups iContainsLineGroups)
        {
            return (iContainsLineGroups.OrderType & OrderTypeEnum.IntraMag) == OrderTypeEnum.IntraMag;
        }

        /// <summary>
        /// Obtient une valeur déterminant si la commande globale contient des articles FnacShop PE.
        /// </summary>
        /// <returns></returns>
        public static bool HasShopPEArticle(this IGotLineGroups iContainsLineGroups)
        {
            return (iContainsLineGroups.OrderType & OrderTypeEnum.IntraMagPE) == OrderTypeEnum.IntraMagPE;
        }

        public static bool IsMixed(this IGotLineGroups iContainsLineGroups)
        {
            return (iContainsLineGroups.HasMarketPlaceArticle() && iContainsLineGroups.HasFnacComArticle());
        }

        /// <summary>
        /// Obtient une valeur déterminant si la commande globale est une commande mixte ClickAndCollect (ClickAndCollect + Fnac)
        /// </summary>
        /// <returns></returns>
        public static bool IsMixedClickAndCollect(this IGotLineGroups iContainsLineGroups)
        {
            return (iContainsLineGroups.HasClickAndCollectArticle() && iContainsLineGroups.HasFnacComArticle());
        }

        public static bool HasFnacComOrClickAndCollectArticle(this IGotLineGroups iContainsLineGroups)
        {
            return (iContainsLineGroups.HasClickAndCollectArticle() || iContainsLineGroups.HasFnacComArticle());
        }

        public static bool HasNumericalArticle(this IGotLineGroups iContainsLineGroups)
        {
            return (iContainsLineGroups.OrderType & OrderTypeEnum.Numerical) == OrderTypeEnum.Numerical;
        }

        public static bool IsOnlyNumerical(this IGotLineGroups iContainsLineGroups)
        {
            return ((iContainsLineGroups.HasNumericalArticle() || iContainsLineGroups.HasDematSoftArticle()) && !(iContainsLineGroups.HasFnacComArticle() || iContainsLineGroups.HasMarketPlaceArticle() || iContainsLineGroups.HasClickAndCollectArticle()));
        }

        /// <summary>
        /// Obtient une valeur déterminant si la commande est multi psp
        /// </summary>
        /// <returns></returns>
        public static bool IsMultiPspCommand(this IGotLineGroups iContainsLineGroups)
        {
            return (iContainsLineGroups.LineGroups.Count() > 1 && (iContainsLineGroups.HasDematSoftArticle() || iContainsLineGroups.HasNumericalArticle()) && !iContainsLineGroups.IsOnlyNumerical())
                   || iContainsLineGroups.IsMixed();
        }
    }
}
