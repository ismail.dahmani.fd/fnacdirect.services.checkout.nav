using System.Collections.Generic;
using System.Linq;
using FnacDirect.OrderPipe.Base.Model;

namespace FnacDirect.OrderPipe.Base.Business.ArticleConversions.OrderFormToShoppingCart
{
    public class OrderFormToShoppingCartArticleConverter : IArticleConverter<Article, ShoppingCartEntities.Article>
    {
        public ShoppingCartEntities.Article Convert(Article @from)
        {
            if (@from is Bundle)
                return Convert((Bundle)@from);

            if (@from is Service)
                return Convert((Service)@from);

            if (@from is StandardArticle)
                return Convert((StandardArticle)@from);

            if (@from is MarketPlaceArticle)
                return Convert((MarketPlaceArticle)@from);

            if (@from is Model.VirtualGiftCheck)
                return Convert((Model.VirtualGiftCheck)@from);

            return null;
        }

        public ShoppingCartEntities.MarketPlaceArticle Convert(MarketPlaceArticle @from)
        {
            var art = new ShoppingCartEntities.MarketPlaceArticle
            {
                InsertDate = @from.InsertDate,
                OriginCode = @from.OriginCode,
                ProductID = @from.ProductID,
                Quantity = @from.Quantity,
                Type = (int?)@from.Type,
                OfferId = @from.OfferId,
                OfferReference = @from.Offer.Reference.ToString(),
                Offer = @from.Offer,
                Referentiel = @from.Referentiel
            };

            if (@from.Reservation != null)
            {
                art.ReservationId = @from.Reservation.Id;
                art.ReservationQuantity = @from.Reservation.Quantity; // moins couteux de sauver la quantity que de faire une requete en base
            }
            else
            {
                art.ReservationId = null;
                art.ReservationQuantity = null;
            }

            art.OldPrice = @from.OldPrice;
            art.OldPriceRecordedDate = @from.OldPriceRecordedDate;

            return art;
        }

        public ShoppingCartEntities.VirtualGiftCheck Convert(Model.VirtualGiftCheck @from)
        {
            var art = new ShoppingCartEntities.VirtualGiftCheck
            {
                Amount = @from.Amount,
                OriginCode = @from.OriginCode,
                Format = @from.Format,
                FromEmail = @from.FromEmail,
                FromName = @from.FromName,
                LogType = @from.LogType,
                Message = @from.Message,
                Quantity = @from.Quantity,
                ProductID = @from.ProductID
            };

            art.Type = (int?)ArticleType.Saleable;

            return art;
        }

        private TResult Convert<TResult>(StandardArticle a)
            where TResult : ShoppingCartEntities.StandardArticle, new()
        {
            var art = new TResult();

            if (a.Remise != null)
            {
                art.RemiseMontantRemise = a.Remise.MontantRemise;
                art.RemiseMotifRemise = a.Remise.MotifRemise;
                art.RemiseOrderDiscount = a.Remise.OrderDiscountId;
                art.RemiseCodeMotif = a.Remise.CodeMotif;
            }
            else
            {
                art.RemiseMontantRemise = 0;
                art.RemiseMotifRemise = null;
                art.RemiseOrderDiscount = 0;
                art.RemiseCodeMotif = 0;
            }

            art.InsertDate = a.InsertDate;
            art.OriginCode = a.OriginCode;
            art.ProductID = a.ProductID;
            art.Quantity = a.Quantity;
            art.Type = (int?)a.Type;
            art.IsPosterioriSelling = a.IsPosterioriSelling;
            art.PosterioriMasterSKU = a.PosterioriMasterSKU;
            art.RefInvoicePosterioriService = a.RefInvoicePosterioriService;
            art.InvoiceIdPosterioriService = a.InvoiceIdPosterioriService;

            art.SplitLevel = a.SplitLevel;

            art.FatherProductId = a.FatherProductId;
            art.InsuranceStart = a.InsuranceStart;
            art.Services = ConvertServices(a.Services);
            if (art.Services.Count == 0) art.Services = null;

            art.ItemPhysicalType = a.ItemPhysicalType;

            art.OldPrice = a.OldPrice;
            art.OldPriceRecordedDate = a.OldPriceRecordedDate;
            art.DontCheckStock = true;

            art.CollectAvailabilityLabel = a.CollectAvailabilityLabel;
            art.ReferenceGU = a.ReferenceGU;

            art.Title = a.Title;
            art.PriceUserEur = a.PriceUserEur;
            art.CollectState = a.CollectInShop ? (int)ShoppingCartEntities.CollectInShopState.Immediate : (int)ShoppingCartEntities.CollectInShopState.None;

            

            return art;
        }

        private ShoppingCartEntities.StandardArticle Convert(StandardArticle @from)
        {
            return Convert<ShoppingCartEntities.StandardArticle>(@from);
        }

        private ShoppingCartEntities.Service Convert(Service a)
        {
            return Convert<ShoppingCartEntities.Service>(a);
        }

        private ShoppingCartEntities.Bundle Convert(Bundle a)
        {
            var bundle = Convert<ShoppingCartEntities.Bundle>(a);

            bundle.BundleComponents = ConvertArticles(a.BundleComponents);

            if (bundle.BundleComponents.Count == 0) 
                bundle.BundleComponents = null;

            return bundle;
        }

       

        public ShoppingCartEntities.Articles ConvertArticles(IEnumerable<Article> list)
        {
            var ret = new ShoppingCartEntities.Articles();

            foreach (var art2 in list.Select(Convert).Where(art2 => art2 != null && art2.Type != (int)ArticleType.Free))
            {
                ret.Add(art2);
            }

            return ret;
        }

        private ShoppingCartEntities.Services ConvertServices(IEnumerable<Service> list)
        {
            var ret = new ShoppingCartEntities.Services();

            foreach (var art2 in list.Select(Convert).Where(art2 => art2 != null))
            {
                ret.Add(art2);
            }

            return ret;
        }
    }
}
