using System;
using System.Collections.Generic;
using System.Linq;
using FnacDirect.OrderPipe.Base.Model.LineGroups;
using FnacDirect.Technical.Framework.Web;
using FnacDirect.Technical.Framework.Web.Mvc;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Basket
{
    public class TermsAndConditionsViewModelBuilder : ITermsAndConditionsViewModelBuilder
    {
        private readonly ISwitchProvider _switchProvider;
        private readonly IApplicationContext _applicationContext;
        private readonly IUrlManager _urlManager;

        public TermsAndConditionsViewModelBuilder(ISwitchProvider switchProvider, IApplicationContext applicationContext, IUrlManager urlManager)
        {
            _switchProvider = switchProvider;
            _applicationContext = applicationContext;
            _urlManager = urlManager;
        }

        public TermsAndConditionsViewModel Build(IEnumerable<ILineGroup> lineGroups)
        {
            if (!_switchProvider.IsEnabled("orderpipe.pop.basket.termsandcondition.activate"))
            {
                return null;
            }

            if (!lineGroups.Any(l => l.HasMarketPlaceArticle))
            {
                return null;
            }

            var termsAndConditionsLabel = _applicationContext.GetMessage("orderpipe.pop.termsandconditions.marketplace.label");
            var termsAndConditionsUrl = _urlManager.Sites.WWWDotNetSecure.GetPage("popcgvmarket").DefaultUrl.ToString();

            var termsAndConditionsViewModel = new TermsAndConditionsViewModel
            {
                TermsAndConditionsLabel = string.Format(termsAndConditionsLabel, termsAndConditionsUrl),
                TermsAndConditionsUrl = termsAndConditionsUrl
            };

            return termsAndConditionsViewModel;
        }
    }


}
