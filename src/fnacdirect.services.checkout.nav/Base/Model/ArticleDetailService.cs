using FnacDirect.Contracts.Online.IModel;
using FnacDirect.Contracts.Online.Model;
using FnacDirect.Opc.OpcCore.IBusiness;
using FnacDirect.OrderPipe.Base.LineGroups;
using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.Technical.Framework.CoreServices;
using FnacDirect.Technical.Framework.ServiceLocation;
using FnacDirect.Technical.Framework.Web;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ArticleType = FnacDirect.OrderPipe.Base.Model.ArticleType;
using DTO = FnacDirect.Contracts.Online.Model;

namespace FnacDirect.OrderPipe.Base.Business
{
    public class ArticleDetailService : IArticleDetailService
    {
        private const int EditorRoleId = 6;

        protected readonly IArticleBusiness3 _articleBusiness;

        public ArticleDetailService()
        {
            _articleBusiness = ServiceLocator.Current.GetInstance<IArticleBusiness3>();
        }

        /// <summary>
        /// Fills in the details of the passed article.
        /// </summary>
        /// <param name="prmoArticle">The article.</param>
        /// <param name="siteContext">The site context.</param>
        /// <returns>True if the article details were fully loaded, false otherwise.</returns>
        public bool GetArticleDetail(StandardArticle prmoArticle, SiteContext siteContext)
        {
            return GetArticleDetail(prmoArticle, siteContext, false);
        }

        public bool GetArticleDetailWithPrice(StandardArticle art, SiteContext siteContext, bool byReference)
        {
            var getresult = false;
            try
            {
                if (GetArticleDetail(art, siteContext, byReference))
                {
                    // First : Calls the pricer to check each Product availability
                    ICoreEngineBusiness bll;
                    var fac = new ServiceFactory<ICoreEngineBusiness>();
                    bll = fac.CreateInstance();

                    var ctx = new DTO.Context();
                    var line = new ShoppingCartLineItem();

                    ctx.ShoppingCart = new DTO.ShoppingCart
                    {
                        LineItems = new List<ShoppingCartLineItem>()
                    };
                    ctx.ShoppingCart.LineItems.Add(line);
                    ctx.Customer = new DTO.Customer();

                    //Prepares pricer call
                    line.Quantity = art.Quantity.Value;
                    line.Reference = new ArticleReference
                    {
                        PRID = art.ProductID.Value
                    };

                    var ar = new ArticleReference
                    {
                        PRID = art.ProductID.Value
                    };

                    var sart = bll.EvaluateProductLight(ctx, ar);

                    art.ShipPriceDBEur = sart.SalesInfo.StandardPrice.HT;
                    art.ShipPriceDBEur = sart.SalesInfo.StandardPrice.HT;

                    art.PriceDBEur = sart.SalesInfo.StandardPrice.TTC;
                    art.PriceNoVATEur = sart.SalesInfo.ReducedPrice.HT;
                    art.PriceRealEur = sart.SalesInfo.PublicPrice.TTC;
                    art.PriceUserEur = sart.SalesInfo.ReducedPrice.TTC;

                    //ecotax
                    if (sart.SalesInfo.EcoTaxAmount.HT.HasValue)
                    {
                        art.EcoTaxEur = sart.SalesInfo.EcoTaxAmount.TTC.Value;
                        art.EcoTaxEurNoVAT = sart.SalesInfo.EcoTaxAmount.HT.Value;
                    }
                }

                getresult = true;
            }
            catch (Exception ex)
            {
                Logging.Current.WriteError(ex, 11001);
                return false;
            }

            return getresult;
        }

        /// <summary>
        /// Fills in the details of the passed article.
        /// </summary>
        /// <param name="prmoArticle">The article.</param>
        /// <param name="siteContext">The site context.</param>
        /// <param name="byReference">True if loading the details by Reference GU, false by Product ID.</param>
        /// <returns>True if the article details were fully loaded, false otherwise.</returns>
        public virtual bool GetArticleDetail(StandardArticle prmoArticle, SiteContext siteContext, bool byReference)
        {
            try
            {
                var articleReference = GetArticleReference(prmoArticle, byReference);

                var article = GetArticleFromService(prmoArticle, siteContext, articleReference);

                return GetArticleDetail(prmoArticle, siteContext, article);
            }
            catch (Exception ex)
            {
                Logging.Current.WriteError(ex, 11001);
                return false;
            }
        }

        public ArticleReference GetArticleReference(StandardArticle prmoArticle, bool byReference)
        {
            if (prmoArticle.InitialProductId.HasValue && prmoArticle.InitialProductId.Value != prmoArticle.ProductID)
            {
                return null;
            }

            var articleReference = new ArticleReference();
            if (byReference)
            {
                articleReference.ReferenceGU = prmoArticle.ReferenceGU;
            }
            else
            {
                articleReference.PRID = prmoArticle.ProductID.Value;
            }

            return articleReference;
        }

        public bool GetArticleDetail(StandardArticle prmoArticle, SiteContext siteContext, DTO.Article article)
        {
            if (article == null)
            {
                return false;
            }

            prmoArticle.ArticleDTO = article;

            if (prmoArticle.ProductID != article.Reference.PRID.Value)
            {
                prmoArticle.InitialProductId = prmoArticle.ProductID;
            }

            prmoArticle.PurchaseId = article.Core.PurchaseId;
            prmoArticle.ProductID = article.Reference.PRID.Value;
            prmoArticle.ID = article.Reference.ID.Value;
            prmoArticle.Title = article.ExtendedProperties.Title;
            prmoArticle.TypeId = article.Core.Type.ID;
            prmoArticle.TypeLabel = article.Core.Type.Label;
            prmoArticle.Ean13 = article.Reference.Ean;
            prmoArticle.Family = (int)article.Core.Group;

            prmoArticle.ReferenceGU = article.Reference.ReferenceGU;

            if (article.ExtendedProperties.Serie != null)
            {
                prmoArticle.SerieTitle = article.ExtendedProperties.Serie.Label;
            }

            prmoArticle.Participant1 = string.Empty;
            prmoArticle.Participant1_id = string.Empty;
            prmoArticle.Participant2 = string.Empty;

            prmoArticle.SubTitle1 = article.ExtendedProperties.SubTitle1;

            SetPhysicalArticle(prmoArticle, article.Core);

            prmoArticle.RCPTax = article.Core.IsDisplayCopyTax ? article.Core.CopyTax : null;

            prmoArticle.CodeOP = article.Core.CodeOP;
            prmoArticle.ListNumber = article.Core.ListNumber;
            prmoArticle.HasGestStock = article.Core.HasStockGest;

            prmoArticle.PriceExternalRef = article.Core.PriceExternalRef;

            if (article.ExtendedProperties.Roles != null)
            {
                var participant1 = new StringBuilder();
                var participant1Id = new StringBuilder();
                var participant2 = new StringBuilder();

                foreach (var role in article.ExtendedProperties.Roles)
                {
                    foreach (var participant in role.Participants)
                    {
                        if (role.ID == EditorRoleId)
                        {
                            prmoArticle.Editor = participant.FirstName;
                            if (participant.FirstName.IndexOf("TOSHIBA", StringComparison.OrdinalIgnoreCase) >= 0)
                            {
                                prmoArticle.MarqueEditor = "Toshiba";
                            }
                        }

                        if (participant.Level == LevelParticipant.Primary)
                        {
                            participant1.AppendFormat(";{0} {1}", participant.FirstName, participant.LastName?.Replace(";", ""));
                            participant1Id.AppendFormat("/{0}", participant.ID);
                        }

                        if (participant.Level == LevelParticipant.Secondary)
                        {
                            participant2.AppendFormat(";{0} {1}", participant.FirstName, participant.LastName?.Replace(";", ""));
                        }
                    }
                }

                prmoArticle.Participant1 = participant1.ToString();
                prmoArticle.Participant1_id = participant1Id.ToString();
                prmoArticle.Participant2 = participant2.ToString();
            }

            if (!string.IsNullOrEmpty(prmoArticle.Participant1))
            {
                prmoArticle.Participant1 = prmoArticle.Participant1.Substring(1);
                prmoArticle.Participant1_id = prmoArticle.Participant1_id.Substring(1);
            }
            if (!string.IsNullOrEmpty(prmoArticle.Participant2))
            {
                prmoArticle.Participant2 = prmoArticle.Participant2.Substring(1);
            }
            prmoArticle.Label = (string.IsNullOrEmpty(prmoArticle.SerieTitle) ? "" : prmoArticle.SerieTitle + " - ") + prmoArticle.Title + " -- " + prmoArticle.Participant1;
            if (prmoArticle.Label.Length > 255)
            {
                prmoArticle.Label = prmoArticle.Label.Substring(0, 255);
            }

            prmoArticle.LogType = short.Parse(article.ExtendedProperties.LogisticID.ToString());

            if (!HasValidLogType(prmoArticle))
            {
                return false;
            }

            prmoArticle.IsForSale = (article.Core.SellStatus == SellStatusType.Active);
            prmoArticle.Format = article.ExtendedProperties.FormatId;
            prmoArticle.FormatLabel = article.ExtendedProperties.Format;

            prmoArticle.Language = article.ExtendedProperties.Language;
            prmoArticle.Content = article.ExtendedProperties.Content;
            if (article.ExtendedProperties.AnnouncementDate.HasValue)
            {
                prmoArticle.AnnouncementDate = article.ExtendedProperties.AnnouncementDate.Value;
                if (prmoArticle.AvailabilityId == (int)AvailabilityEnum.PreOrderRestockingPE
                    || prmoArticle.AvailabilityId == (int)AvailabilityEnum.PreOrderRestockingPT)
                {
                    if (prmoArticle.ArticleDTO.ExtendedProperties.RestockingDate.HasValue)
                    {
                        prmoArticle.AnnouncementDate = prmoArticle.ArticleDTO.ExtendedProperties.RestockingDate.Value;
                    }
                }
            }
            prmoArticle.CopyControl = (article.ExtendedProperties.HasCopyControl.HasValue && article.ExtendedProperties.HasCopyControl.Value ? "1" : "0");

            prmoArticle.SupportId = article.ExtendedProperties.FormatId;

            prmoArticle.IsBundle = article.Core.IsBundle;
            if (prmoArticle.IsBundle.Value && (article.Components == null || article.Components.Count == 0))
                throw new BusinessException("Article has no component but is marked as bundle.", "ID", article.Reference.ID.ToString(), "PRID", article.Reference.PRID.ToString());

            if (!prmoArticle.IsPosterioriSelling)
            {
                if (prmoArticle.IsBundle.GetValueOrDefault(false) && !((prmoArticle is Bundle) || (prmoArticle is Service)))//rajout du cas des bundle de service qui sont en fait de type service
                {
                    var exWarn = new BusinessException("Bundle is not of Bundle type");
                    exWarn.Data["Prid"] = prmoArticle.ProductID;
                    Logging.Current.WriteWarning("Bundle is not of Bundle type", exWarn);
                    return false;
                }
            }

            prmoArticle.DisplayAnnouncementDay = (article.ExtendedProperties.DisplayAnnouncementDay.HasValue && article.ExtendedProperties.DisplayAnnouncementDay.Value);

            if (article.Core?.Images?.Pictures?.Any() == true)
            {
                foreach (var picto in article.Core.Images.Pictures.OrderBy(p => (int)p.Type))
                {
                    if (picto.Type == PictureType.SmallScan)
                    {
                        prmoArticle.LittleScan = picto.Location;
                        prmoArticle.ScanURL = prmoArticle.LittleScan;
                        prmoArticle.ErgoBasketPictureURL = picto.Location;
                    }
                    else if (picto.Type == PictureType.Big_110x110)
                    {
                        prmoArticle.ErgoBasketPictureURL = picto.Location;
                    }
                    else if (picto.Type == PictureType.Principal_340x340)
                    {
                        prmoArticle.Picture_Principal_340x340 = picto.Location;
                    }
                }
            }

            prmoArticle.HasAccServ = HasLinkedItems(article, LinkedType.Services)
                || HasLinkedItems(article, LinkedType.ComplementaryAccessories)
                || HasLinkedItems(article, LinkedType.DedicatedAccessories)
                || HasLinkedItems(article, LinkedType.Formations);

            prmoArticle.ServiceDescriptorName = article.Core != null ? article.Core.ServiceDescriptorName : string.Empty;

            // Recherche des services
            // Si on ne les retrouve pas attachés à l'article, on le supprime
            // sinon, on récupère les informations attachées
            for (var iItemCounter = prmoArticle.Services.Count - 1; iItemCounter > -1; iItemCounter--)
            {
                var service = prmoArticle.Services[iItemCounter];
                var s = GetArticleFromService(service, siteContext, ArticleReference.FromPrid(service.ProductID.Value));

                if (s == null)
                {
                    prmoArticle.Services.Remove(service);
                }
                else
                {
                    service.ID = s.Reference.ID.Value;
                    service.ProductID = s.Reference.PRID.Value;
                    service.Title = s.Core.Title;
                    service.Label = service.Title;
                    service.IsBundle = s.Core.IsBundle;
                    service.Ean13 = s.Reference.Ean;
                    service.ReferenceGU = s.Reference.ReferenceGU;
                    service.TypeId = s.Core.Type.ID;
                    service.TypeLabel = s.Core.Type.Label;
                    service.LittleScan = "Shelf/Article/Type" + service.TypeId.Value + ".gif";
                    service.ScanURL = service.LittleScan;
                    service.LogType = short.Parse(s.ExtendedProperties.LogisticID.ToString());
                    if (!service.LogType.HasValue || service.LogType == 0)
                    {
                        service.LogType = 9; // Type logistique correspondant à un produit non expédié
                    }
                    service.Family = s.Core.Family.ID;
                    service.IsForSale = (s.Core.SellStatus == SellStatusType.Active);
                    service.Format = short.Parse(s.Core.Family.ID.ToString());
                    if (s.ExtendedProperties.AnnouncementDate.HasValue)
                    {
                        service.AnnouncementDate = s.ExtendedProperties.AnnouncementDate.Value;
                    }

                    service.ServiceDescriptorName = s.Core.ServiceDescriptorName;

                    var urlManager = ServiceLocator.Current.GetInstance<IUrlManager>();
                    var warrantiesLink = string.Empty;
                    var popWarrantiesPage = urlManager.Sites.WWWDotNetSecure.GetPage("popwarranties");
                    if (popWarrantiesPage != null)
                    {
                        warrantiesLink = popWarrantiesPage.DefaultUrl.ToString();
                    }

                    service.MoreInfoLink = s.MultimediaLinks != null && s.MultimediaLinks.Any(l => l.DataType == MultimediaHostingDataType.Popin)
                            ? urlManager.InitialPageUrl(false).WithPage(s.MultimediaLinks.FirstOrDefault(l => l.DataType == MultimediaHostingDataType.Popin).FullDataURL).ToString() : warrantiesLink;
                }
            }

            return true;
        }

        private bool HasLinkedItems(DTO.Article article, LinkedType type)
        {
            return article?.LinkedArticles != null
                && article.LinkedArticles.Contains(type)
                && article.LinkedArticles[type].Categories.Count > 0;
        }

        /// <summary>
        /// This Method sets the Article's ItemPhysicalType property and
        /// his SellerID to -1 if PhysicalType =  Numeric
        /// </summary>
        /// <param name="stdArt">The StandardArticle entry object</param>
        /// <param name="physType">The PhysicalType value to be set</param>
        private void SetPhysicalArticle(StandardArticle stdArt, CoreInfo pCoreInfo)
        {
            if (pCoreInfo != null)
            {
                stdArt.ItemPhysicalType = pCoreInfo.ItemPhysicalType;

                if (pCoreInfo.ItemPhysicalType == PhysicalType.Numeric)
                {
                    // Add new condition in order to distinct Demat between ebook and Software
                    if (pCoreInfo.Group == ArticleGroup.Software)
                    {
                        stdArt.SellerID = SellerIds.GetId(LineGroups.SellerType.Software);
                    }
                    else if (pCoreInfo.Group == ArticleGroup.Book)
                    {
                        stdArt.SellerID = SellerIds.GetId(LineGroups.SellerType.Numerical);
                    }
                }
            }
        }

        #region Surcharge FSH
        public virtual DTO.Article GetArticleFromService(StandardArticle prmoArticle, SiteContext siteContext, ArticleReference ar)
        {
            if (prmoArticle.Type.GetValueOrDefault(ArticleType.Saleable) == ArticleType.Free)
            {
                return _articleBusiness.GetArticle(siteContext, ar, ArticleLoadingOption.DefaultAndInactive); //siteContext required
            }
            else
            {
                return _articleBusiness.GetArticle(siteContext, ar); //siteContext required
            }
        }        

        protected virtual bool HasValidLogType(StandardArticle prmoArticle)
        {
            if ((prmoArticle != null) && (!prmoArticle.LogType.HasValue || prmoArticle.LogType.Value == 0))
            {
                return false;
            }

            return true;
        }

        #endregion
    }
}
