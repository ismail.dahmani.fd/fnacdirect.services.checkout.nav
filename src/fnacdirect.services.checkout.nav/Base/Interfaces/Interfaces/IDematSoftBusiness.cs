﻿using System.Collections.Generic;
using FnacDirect.OrderPipe.Base.Model;

namespace FnacDirect.OrderPipe.Base.Business
{
    public interface IDematSoftBusiness
    {
        IDictionary<long, bool> IsAvailable(ArticleList standardArticles);

        IDictionary<long, bool> GetDispoArticles(ArticleList standardArticles);
    }
}
