using FnacDirect.OrderPipe.Base.BaseModel;
using FnacDirect.OrderPipe.Base.Model.LineGroups;
using System.Collections.Generic;

namespace FnacDirect.OrderPipe.Base.Business.Shipping
{
    public interface IAppointmentDeliveryCalendarService
    {
        ItemsForAppointmentDeliveryRequest GetItemsAndPromotionForAppointmentDeliveryRequest(IEnumerable<ILineGroup> lineGroups, ShippingMethodEvaluationItem shippingMethodEvaluationItem, int shippingMethodId);
        ItemsForAppointmentDeliveryRequest GetItemsAndPromotionForAppointmentDeliveryRequest(IEnumerable<Model.Article> articles, ShippingMethodEvaluationItem shippingMethodEvaluationItem, int shippingMethodId);
        IEnumerable<Solex.AppointmentDelivery.Client.Contract.Promotion> GetPromotions(IEnumerable<Model.BaseArticle> baseArticles);
    }
}
