﻿using FnacDirect.OrderPipe.Base.LineGroups;
using System.Xml.Serialization;

namespace FnacDirect.OrderPipe.Base.Model
{
    public class SellerInformation
    {
        public SellerInformation()
        {
            SellerId = SellerIds.GetId(SellerType.Fnac);
        }

        public string DisplayName { get; set; }

        [XmlIgnore]
        public SellerType Type { get; private set; }

        [XmlIgnore]
        public SellerType SubType { get; private set; }

        private int _sellerId;
        private int _subSellerId;

        [XmlElement(ElementName = "SellerId")] // Pour assurer la retrocompatibilité de la sérialisation
        public int SubSellerId
        {
            get
            {
                return _subSellerId;
            }
            set
            {
                _subSellerId = value;
                _sellerId = SellerIds.GetParentSellerId(_subSellerId);
                SubType = SellerIds.GetType(_subSellerId);
                Type = SellerIds.GetType(_sellerId);
            }
        }

        [XmlIgnore]
        public int SellerId
        {
            get
            {
                return _sellerId;
            }
            set
            {
                SubSellerId = value;
            }
        }
    }
}
