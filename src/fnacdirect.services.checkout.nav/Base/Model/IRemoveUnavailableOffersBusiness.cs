using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.OrderPipe.Base.Model.Facets;

namespace FnacDirect.OrderPipe.Base.Business
{
    public interface IRemoveUnavailableOffersBusiness
    {
        void RemoveUnavailableOffers(IGotLineGroups iGotLineGroups);
    }
}
