﻿using StructureMap;

namespace FnacDirect.OrderPipe.CoreRouting
{
    public class StructureMapOrderPipeResetStrategyLocator : IOrderPipeResetStrategyLocator
    {
        private readonly IContainer _container;

        public StructureMapOrderPipeResetStrategyLocator(IContainer container)
        {
            _container = container;
        }

        public IOrderPipeResetStrategy GetOrderPipePipeRedirectionStrategyFor(OPContext opContext)
        {
            var instanceType = opContext.Pipe.UIDescriptor.GetType();
            var serviceType = typeof(IOrderPipeResetStrategy<>);
            var concreteServiceType = serviceType.MakeGenericType(instanceType);

            return _container.GetInstance(concreteServiceType) as IOrderPipeResetStrategy;
        }
    }
}
