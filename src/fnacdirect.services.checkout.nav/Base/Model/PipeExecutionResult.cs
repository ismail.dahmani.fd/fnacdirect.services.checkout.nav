﻿using FnacDirect.OrderPipe.Config;
namespace FnacDirect.OrderPipe.CoreRouting
{
    public class PipeExecutionResult
    {
        private readonly OPContext _opContext;

        public OPContext OpContext
        {
            get
            {
                return _opContext;
            }
        }

        private readonly UIDescriptor _currentUIDescriptor;

        public UIDescriptor CurrentUIDescriptor
        {
            get
            {
                return _currentUIDescriptor;
            }
        }

        public PipeExecutionResult(OPContext opContext, UIDescriptor currentUIDescriptor)
        {
            _opContext = opContext;
            _currentUIDescriptor = currentUIDescriptor;
        }
    }
}
