using FnacDirect.OrderPipe.Base.Model.LineGroups;
using System.Collections.Generic;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Common.Payment
{
    public interface ITermsAndConditionsBuilder
    {
        string GetTermsAndConditionsFor(IEnumerable<ILineGroup> lineGroups);
        TermsAndConditionsTypeViewModel GetTermsAndConditionsTypeViewModel(IEnumerable<ILineGroup> lineGroups);
    }
}
