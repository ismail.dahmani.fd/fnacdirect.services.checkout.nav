using FnacDirect.OrderPipe.Base.Model;


namespace FnacDirect.OrderPipe.Base.Business
{
    public interface IStomEcoTaxBusiness
    {
        /// <summary>
        /// Charge l'éco-taxe à partir de Stom sur l'article art.
        /// </summary>
        /// <param name="art"></param>
        /// <param name="refUG"></param>
        /// <returns>Vrai ssi l'éco-taxe a pur être chargée.</returns>
        bool LoadEcoTaxFromStom(StandardArticle art, string refUG);

        /// <summary>
        /// Charge l'éco-taxe à partir de Stom sur le bundle art.
        /// </summary>
        /// <param name="bundle"></param>
        /// <param name="refUG"></param>
        /// <returns>Vrai ssi l'éco-taxe a pur être chargée.</returns>
        bool LoadEcoTaxBundleFromStom(StandardArticle bundle, string refUG);
    }
}
