using System;
using FnacDirect.OrderPipe.Base.Business.ShoppingCart;
using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.OrderPipe.Base.Model.Facets;

namespace FnacDirect.OrderPipe.Base.Business
{
    public class RemoveUnavailableOffersBusinessProvider : IRemoveUnavailableOffersBusinessProvider
    {   
        public IRemoveUnavailableOffersBusiness GetRemoveUnavailableOffersBusiness(Predicate<MarketPlaceArticle> canDelete, Action<IGotLineGroups, MarketPlaceArticle> onDelete)
        {
            return new RemoveUnavailableOffersBusiness(canDelete, onDelete);
        }
    }
}
