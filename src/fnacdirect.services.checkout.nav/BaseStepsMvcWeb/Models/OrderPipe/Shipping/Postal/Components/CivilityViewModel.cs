﻿
namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Common
{
    public class CivilityViewModel
    {
        public string Id { get; set; }

        public string Label { get; set; }
    }
}
