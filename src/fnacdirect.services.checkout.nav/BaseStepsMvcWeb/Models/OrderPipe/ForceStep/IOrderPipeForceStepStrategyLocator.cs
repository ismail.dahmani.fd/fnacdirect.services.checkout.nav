﻿using FnacDirect.OrderPipe.Config;

namespace FnacDirect.OrderPipe.CoreRouting
{
    public interface IOrderPipeForceStepStrategyLocator
    {
        IOrderPipeForceStepStrategy GetOrderPipeForceStepStrategyFor<TDescriptor>(TDescriptor descriptor)
            where TDescriptor : class, IUiPipeDescriptor;
    }
}
