using FnacDirect.Customer.Model;
using FnacDirect.OrderPipe.Base.Model;

namespace FnacDirect.OrderPipe.Base.Business
{
    public interface IPromotionStoreService
    {
        string GetPromoCodeMotif(StandardArticle article, CustomerEntity customerEntity, bool basketContainsNewAdhThreeYears, string promotionCode);
        string GetPromoCodeType(StandardArticle article, CustomerEntity customerEntity, bool basketContainsNewAdhThreeYears, string promotionCode);
        bool IsAdherentPromotion(string promotionCode);
        int GetClientOrderDiscountCodeId();
        bool MustApplyPromotion(string pCodePromo);
    }
}
