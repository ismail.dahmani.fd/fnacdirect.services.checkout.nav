using FnacDirect.OrderPipe.Base.LineGroups;
using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.OrderPipe.Base.Model.Shipping.Postal.Appointment.Fly;
using FnacDirect.Solex.AppointmentDelivery.Client.Contract;
using System.Collections.Generic;

namespace FnacDirect.OrderPipe.Base.Business.Shipping.Postal.Appointment.Fly
{
    public interface IFlyBusiness
    {
        int SellerId { get; }
        IEnumerable<int> ShippingMethodIds { get; }

        IEnumerable<Model.Article> GetEligibleArticlesForFly(PopOrderForm popOrderForm, int sellerId);
        void SaveFlyOrder(PopOrderForm popOrderForm, IFlyForm flyForm);
        IFlyForm GetFlyOrder(PopOrderForm popOrderForm);
        IEnumerable<ILogisticLineGroup> GetFlyLogisticLineGroups(IEnumerable<ILogisticLineGroup> logisticLineGroups);
        QualifyingSurvey ToFlyQualifyingSurvey(FlyData flyData);
    }
}
