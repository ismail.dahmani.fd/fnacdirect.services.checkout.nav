using System.Web.Mvc;
using FnacDirect.OrderPipe.Base.Business;
using FnacDirect.OrderPipe.Base.Business.Helpers;
using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.Technical.Framework.Web.Mvc;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Common.Payment
{
    public class GiftCardViewModel
    {
        public GiftCardViewModel(string cn, decimal amount, decimal maxamount, bool spl, bool valid, string emitterName)
        {          
            var applicationContext = DependencyResolver.Current.GetService<IApplicationContext>();
            CardNumber = cn;
            Amount = PriceHelpers.GetRoundedPrice(amount);
            MaxAmount = PriceHelpers.GetRoundedPrice(maxamount);
            HasRemainingAmout = Amount < MaxAmount;
            Splitable = spl;
            Valid = valid;            
            CardType = GiftCardBusiness.GetGiftCardTypeFromEmitterName(emitterName);
            Label = applicationContext.GetMessage(CardType.HasValue ? string.Format("orderpipe.pop.payment.giftcard.{0}", (int)CardType) : "orderpipe.pop.payment.giftcard.default");
            GiftCardRemainingAmount = Amount <= MaxAmount ? PriceHelpers.GetRoundedPrice(MaxAmount - Amount) : 0m ;
        }
     
        public string CardNumber { get; set; }        
        public bool Splitable { get; set; }
        public decimal MaxAmount { get; set; }
        public decimal Amount { get; set; }
        public GiftCardType? CardType { get; set; }
        public string Label { get; set; }
        public bool Valid { get; set; }
        public bool HasRemainingAmout { get; set; }
        public decimal GiftCardRemainingAmount { get; set; }
    }
}
