using System.Web.Routing;
using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.OrderPipe.Core;
using StructureMap;
using FnacDirect.OrderPipe.CoreRouting.UiDescriptorRenderer;
using FnacDirect.OrderPipe.Context;
using FnacDirect.OrderPipe.Core.Services;
using StructureMap.Web.Pipeline;

namespace FnacDirect.OrderPipe.CoreRouting
{
    public static class OrderPipeCoreRoutingRegistryExtensions
    {
        public static void AddOrderPipeCoreRouting(this IRegistry registry)
        {
            registry.For<IOPManager>().Singleton().Use<OPManager>();

            registry.For<IOPContextProvider>().Use<UserContextOpContextProvider>();

            registry.For<IOrderPipeInteractiveStepHandlerStrategyLocator>().Use<StructureMapOrderPipeInteractiveStepHandlerStrategyLocator>();
            registry.For<IOrderPipeStepRedirectionStrategyLocator>().Use<StructureMapOrderPipeStepRedirectionStrategyLocator>();
            registry.For<IOrderPipeResetStrategyLocator>().Use<StructureMapOrderPipeResetStrategyLocator>();
            registry.For<IOrderPipeForceStepStrategyLocator>().Use<StructureMapOrderPipeForceStepStrategyLocator>();
            registry.For<IUiDescriptorRendererLocator>().Use<StructureMapUiDescriptorRendererLocator>();

            registry.For<IExecutePipeService>().Use<ExecutePipeService>();

            registry.For<OPContext>().LifecycleIs<HybridLifecycle>().Use("OPContext factory", c =>
            {
                var pipeName = RouteTable.Routes.GetPipeName();

                var opManager = c.GetInstance<IOPManager>();

                var pipe = opManager[pipeName];

                var opContextProvider = c.GetInstance<IOPContextProvider>();

                if (opContextProvider == null)
                    return null;

                return opContextProvider.GetCurrentContext(pipeName);
            });

            registry.For<IPipeParametersProvider>().LifecycleIs<HybridLifecycle>().Use("IPipeParametersProvider factory", c =>
            {
                var pipeName = RouteTable.Routes.GetPipeName();

                var opManager = c.GetInstance<IOPManager>();

                var pipe = opManager.LoadConfigurationOnly(pipeName);

                return new PipeParametersProvider(pipe);
            });

            registry.For<BaseOF>().LifecycleIs<HybridLifecycle>().Use("BaseOF factory", c =>
            {
                var opContext = c.GetInstance<OPContext>();

                if (opContext == null)
                    return null;

                return opContext.OF as BaseOF;
            });
        }
    }
}
