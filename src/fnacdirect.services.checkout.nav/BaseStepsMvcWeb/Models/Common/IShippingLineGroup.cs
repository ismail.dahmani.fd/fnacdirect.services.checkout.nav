﻿using FnacDirect.OrderPipe.Base.Model.LineGroups;
using FnacDirect.OrderPipe.Shipping.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FnacDirect.OrderPipe.Base.Model.FDV.LineGroups
{
    public interface IShippingLineGroup : ILineGroup
    {
        ShippingAddress ShippingAddress { get; set; }
        ShippingMethodResult ShippingMethodChoice { get; set; }
    }
}
