using FnacDirect.OrderPipe.Base.BaseModel;
using FnacDirect.OrderPipe.Base.LineGroups;
using System.Collections.Generic;

namespace FnacDirect.OrderPipe.Base.Model.LineGroups
{
    public interface ILineGroup : ILineGroupHeader
    {
        bool IsTemporary { get; }

        ArticleList Articles { get; }
        void RemoveArticlesById(IEnumerable<int?> articlesIdToRemove);
        bool HasShippingMethod(ShippingMethodEvaluation shippingMethodEvaluation, IEnumerable<int> shippingMethodIds);

        bool HasMarketPlaceArticle { get; }
        bool HasFnacComArticle { get; }
        bool HasCollectInStoreArticles { get; }
        IEnumerable<Article> CollectInStoreArticles { get; }
        bool HasShopArticle { get; }
        bool HasIntraMagPE { get; }
        bool HasNumericalArticles { get; }
        bool HasDematSoftArticles { get; }
        bool IsPro { get; }
        bool IsMixed { get; }
        bool GlobalShippingMethodChoiceIsSetManually { get; set; }
    }
}
