using FnacDirect;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace FnacDirect.OrderPipe.BaseMvc.Web.LayerBasket
{
    public class LayerBasketAreaRegistrationPart
    {
        private readonly ISwitchProvider _switchProvider;

        public LayerBasketAreaRegistrationPart(ISwitchProvider switchProvider)
        {
            _switchProvider = switchProvider;
        }

        public void RegisterArea(AreaRegistrationContext areaRegistrationContext)
        {
            areaRegistrationContext.MapRoute(
                name: "Layer Basket - View",
                url: "basket/view",
                defaults: new { controller = "LayerBasket", action = "Display", pipe = "basket" }
            );
        }
    }
}
