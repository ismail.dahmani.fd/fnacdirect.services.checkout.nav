namespace FnacDirect.OrderPipe.Base.Model.TaxCalculation
{
    public class TaxCalculationPriceResult
    {
        public OrderPipeSalesInfoPrice ReducedPriceToApply { get; set; }
        public OrderPipeSalesInfoPrice PublicPriceToApply { get; set; }
        public OrderPipeSalesInfoPrice StandardPriceToApply { get; set; }
        public OrderPipeSalesInfoPrice EcoTaxAmountToApply { get; set; }
        public TaxInfos TaxToApplyInfos { get; set; }
    }
}
