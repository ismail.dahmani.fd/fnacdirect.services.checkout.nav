using FnacDirect.OrderPipe.Base.Business.Mailing;
using System.Collections.Generic;
using System.IO;
using System.Net.Mail;
using System.Xml.Serialization;

namespace FnacDirect.OrderPipe.Base.Steps.Mailing
{
    public class PopMailData : StepData
    {
        [XmlIgnore]
        public Order Model { get; set; }

        public string MailContent { get; set; }

        public string NeolaneStream { get; set; }

        private List<SerializableLinkedResource> _serializableLinkedResources;

        public List<SerializableLinkedResource> SerializableLinkedResources
        {
            get
            {
                if (_serializableLinkedResources == null)
                {
                    _serializableLinkedResources = new List<SerializableLinkedResource>();
                }

                return _serializableLinkedResources;
            }
            set
            {
                _serializableLinkedResources = value;
            }
        }
    }
}
