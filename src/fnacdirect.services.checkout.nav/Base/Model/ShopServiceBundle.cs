using FnacDirect.OrderPipe.Base.Model;
using System;
using System.Xml.Serialization;

namespace FnacDirect.OrderPipe.FDV.Model
{
    [Serializable]
    public class ShopServiceBundle : ShopService, IBundle
    {
        [XmlIgnore]
        public ArticleList BundleComponents
        {
            get;
            set;
        }

        public override StandardArticle Copy<T>()
        {
            var copy = base.Copy<T>();
            ((IBundle)copy).BundleComponents = BundleComponents;

            return copy;
        }
    }
}
