﻿namespace FnacDirect.OrderPipe.Base.Model.TaxCalculation
{
    public enum GlobalPriceType
    {
        Shipping,
        Wrap,
        Split
    }
}
