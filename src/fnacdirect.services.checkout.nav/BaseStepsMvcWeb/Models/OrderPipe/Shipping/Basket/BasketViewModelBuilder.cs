using FnacDirect.OrderPipe.Base.Business;
using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.OrderPipe.Base.Steps.Pop;
using FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Common.Basket;
using FnacDirect.OrderPipe.BaseMvc.Web.Services.Pop;
using System.Linq;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Shipping
{
    public class BasketViewModelBuilder : IBasketViewModelBuilder
    {
        private readonly ISellerInformationsViewModelBuilder _sellerInformationsViewModelBuilder;
        private readonly IArticleImagePathService _articleImagePathService;
        private readonly IOrderExternalReferenceViewModelBuilder _orderExternalReferenceViewModelBuilder;
        private readonly IMembershipDateBusiness _dateBusiness;

        public BasketViewModelBuilder(ISellerInformationsViewModelBuilder sellerInformationsViewModelBuilder,
                                      IArticleImagePathService articleImagePathService,
                                      IOrderExternalReferenceViewModelBuilder orderExternalReferenceViewModelBuilder,
                                      IMembershipDateBusiness dateBusiness)
        {
            _sellerInformationsViewModelBuilder = sellerInformationsViewModelBuilder;
            _articleImagePathService = articleImagePathService;
            _orderExternalReferenceViewModelBuilder = orderExternalReferenceViewModelBuilder;
            _dateBusiness = dateBusiness;
        }

        public BasketViewModel BuildBasketViewModel(PopOrderForm popOrderForm, PopShippingSellerData currentPopShippingSellerData)
        {
            var basketViewModel = new BasketViewModel();

            var shippingMethodEvaluation = popOrderForm.ShippingMethodEvaluation;

            var sellerId = currentPopShippingSellerData.SellerId;
            var lineGroups = popOrderForm.LineGroups.Seller(sellerId);

            foreach (var lineGroup in lineGroups)
            {
                foreach (var article in lineGroup.Articles.OfType<BaseArticle>().Where(a => !a.LogType.HasValue || !shippingMethodEvaluation.ExcludedLogisticTypes.Contains(a.LogType.Value)))
                {
                    Promotion promotion = null;
                    promotion = popOrderForm.GlobalPromotions.FirstOrDefault(p => p.FreeArtId == article.ProductID);

                    basketViewModel.With(new BasketItemViewModel
                        {
                            Title = article.Title,
                            NormalImage = _articleImagePathService.BuildPath(article, promotion),
                            IsBook = article.IsBook,
                            IsEbook = article.IsEbook,
                            HasD3E = article.HasD3E,
                            HasRCP = article.HasRCP,

                    });
                }
            }

            basketViewModel.OrderExternalReference = _orderExternalReferenceViewModelBuilder.Build(popOrderForm);

            basketViewModel.DisplayBirthDatePopin = _dateBusiness.DisplayBirthDatePopin(popOrderForm);

            return basketViewModel;
        }
        public SellerInformationsViewModel BuildSellerViewModel(PopOrderForm popOrderForm, PopShippingSellerData currentPopShippingSellerData)
        {
            return _sellerInformationsViewModelBuilder.Build(popOrderForm, currentPopShippingSellerData.SellerId);
        }
    }
}
