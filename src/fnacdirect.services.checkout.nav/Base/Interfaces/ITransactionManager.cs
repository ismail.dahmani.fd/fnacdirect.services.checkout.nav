﻿namespace FnacDirect.OrderPipe
{
    public interface ITransactionManager
    {
        void Begin(OPContext context);
        void Commit(OPContext context);
        bool Dispose(OPContext context);
    }
}
