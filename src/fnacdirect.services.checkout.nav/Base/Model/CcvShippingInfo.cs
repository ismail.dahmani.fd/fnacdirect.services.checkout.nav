using System;

namespace FnacDirect.OrderPipe.Base.Model
{
    /// <summary>
    /// Infos sur la livraison multi-adresse des CCV
    /// </summary>
    [Serializable]
    public class CcvShippingInfo
    {
        private string _SelectedLineGroupUniqueId = string.Empty;
        /// <summary>
        /// Id du linegroup sélectionné dans le récapitulatif livraison
        /// </summary>
        public string SelectedLineGroupUniqueId
        {
            get { return _SelectedLineGroupUniqueId; }
            set { _SelectedLineGroupUniqueId = value; }
        }

        private CcvReceiverInfoCollection _CcvReceivers = new CcvReceiverInfoCollection();
        /// <summary>
        /// Carnet d'addresse pour tous les destinataires ccv disponibles
        /// (rangé par adresse e-mail)
        /// </summary>
        public CcvReceiverInfoCollection CcvReceivers
        {
            get { return _CcvReceivers; }
            set { _CcvReceivers = value; }
        }

        private string _Message = string.Empty;
        public string Message
        {
            get { return _Message; }
            set { _Message = value; }
        }

        private int _Format = 0;
        public int Format
        {
            get { return _Format; }
            set { _Format = value; }
        }
    }
}
