﻿using FnacDirect.OrderPipe.Base.Model;
using System;
using System.Collections.Generic;
using FnacDirect.OrderPipe.Shipping.Model;
using System.Linq;

namespace FnacDirect.OrderPipe.Base.Business.Shipping.Rules
{
    public class HandDeliveryShippingMethodBasketBuildingRule : BaseShippingMethodBasketBuildingRule
    {
        public override string SwitchId
        {
            get
            {
                return string.Empty;
            }
        }

        public override Func<ShippingMethodBasket, ShippingMethodBasket> GetModifier(IPrivateDataContainer privateDataContainer, IEnumerable<Article> articles, ShippingAddress shippingAddress)
        {
            var marketPlaceArticles = articles.OfType<MarketPlaceArticle>();

            if (marketPlaceArticles.Any(i => i.HandDelivery.GetValueOrDefault()))
            {
                var triggerShadowShippingMethodId = PipeParametersProvider.Get<int>("TriggerShadowShippingMethodIds");
                var triggerShadowShippingMethod = new SingleTriggerShadowShippingMethod(triggerShadowShippingMethodId);

                return shippingMethodBasket =>
                {
                    shippingMethodBasket.TriggerShadowShippingMethods = new List<ITriggerShadowShippingMethod>() { triggerShadowShippingMethod };
                    return shippingMethodBasket;
                };
            }

            return shippingMethodBasket => shippingMethodBasket;
        }
    }
}
