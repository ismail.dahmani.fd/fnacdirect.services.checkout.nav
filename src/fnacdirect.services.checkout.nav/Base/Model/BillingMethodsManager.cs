using System.Collections.Generic;
using System.Linq;
using System.Collections.ObjectModel;
using FnacDirect.Context;
using FnacDirect.OrderPipe.Base.DAL;
using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.Technical.Framework.ServiceLocation;
using System;
using FnacDirect.OrderPipe.Base.Model.Facets;
using FnacDirect.Technical.Framework.Web.Mvc;

namespace FnacDirect.OrderPipe.Base.Business
{
    public class BillingMethodsManager : KeyedCollection<int, BillingMethodDescriptor>, IBillingMethodsManager
    {
        private readonly IMaintenanceConfigurationService _maintenanceConfigurationService;
        private readonly IPaymentMethodDal _paymentMethodDal;
        private readonly IApplicationContextLight _applicationContextLight;
         

        public BillingMethodsManager()
        {
            _maintenanceConfigurationService = ServiceLocator.Current.GetInstance<IMaintenanceConfigurationService>();
            _paymentMethodDal = ServiceLocator.Current.GetInstance<IPaymentMethodDal>();
            _applicationContextLight = ServiceLocator.Current.GetInstance<IApplicationContextLight>();
        }

        public BillingMethodsManager(IMaintenanceConfigurationService maintenanceConfigurationService, IPaymentMethodDal paymentMethodDal, IApplicationContextLight applicationContextLight)
        {
            _maintenanceConfigurationService = maintenanceConfigurationService;
            _paymentMethodDal = paymentMethodDal;
            _applicationContextLight = applicationContextLight;
        }

        protected override int GetKeyForItem(BillingMethodDescriptor item)
        {
            return item.Id;
        }

        public void Fill(string config)
        {
            if (_maintenanceConfigurationService.Commerce)
            {
                return;
            }

            Clear();

            var culture = _applicationContextLight.GetCulture() ?? string.Empty;

            var billingMethodeDescriptors = _paymentMethodDal.GetBillingMethodDescriptors(config, culture);

            foreach (var billingMethodDescriptor in billingMethodeDescriptors)
            {
                Add(billingMethodDescriptor);
            }
        }

        public BillingMethodDescriptor GetBillingMethodDescriptor<T>() 
            where T : BillingMethod, new()
        {
            var dummyMethod = new T();

            return GetBillingMethodDescriptor(dummyMethod.BillingMethodId);
        }

        public BillingMethodDescriptor GetBillingMethodDescriptor(int billingMethodId)
        {
            if (!Contains(billingMethodId))
            {
                throw new ArgumentException($"billingMethodId is unknow {billingMethodId}, this method don't exist in database.");
            }

            var desc = this[billingMethodId];

            return desc;
        }

        public void RegisterBillingMethod<T>() where T:BillingMethod,new()
        {
            if (_maintenanceConfigurationService.Commerce)
            {
                return;
            }

            var method = new T();

            if (Contains(method.BillingMethodId))
            {
                var billingMethodDescriptor = this[method.BillingMethodId];

                billingMethodDescriptor.Name = method.Name;
                billingMethodDescriptor.BillingMethodType = method.BillingMethodType;


                if (billingMethodDescriptor.Type == null)
                {
                    billingMethodDescriptor.Type = method.GetType();
                }
                else
                {
                    throw BaseBusinessException.BuildException("Duplicate billingMethod", null, BusinessRange.Init, 1)
                        .WithData("Method", method.GetType().FullName);
                }
            }
            else
            {
                throw BaseBusinessException.BuildException("Unknown billingMethodId in database", null, BusinessRange.Init, 2)
                    .WithData("Method", method.GetType().FullName)
                    .WithData("MethodId", method.BillingMethodId);
            }
        }

        public List<BillingMethodDescriptor> ListRemainingMethods(int remainingMethodMask)
        {
            var ret = new List<BillingMethodDescriptor>();
            for (var i = 0; i < 31; i++)
            {
                var m = (1 << i);
                if ((remainingMethodMask & m) != 0 && Contains(m) && this[m].Type != null)
                {
                    ret.Add(this[m]);
                }
            }
            return ret;
        }

        public bool IsValidMethod(int mask, BillingMethodDescriptor desc)
        {
            return ((mask & desc.Id) != 0 && desc.Type != null && desc.IsActive);
        }

        public BillingMethodsManagerContext For<T>(IGotBillingInformations orderForm)
            where T : BillingMethod, new()
        {
            var billingMethodDescriptor = GetBillingMethodDescriptor<T>();

            return new BillingMethodsManagerContext(this, orderForm, billingMethodDescriptor);
        }

        public class BillingMethodsManagerContext
        {
            private readonly IGotBillingInformations _orderForm;
            private readonly BillingMethodsManager _billingMethodsManager;
            private readonly BillingMethodDescriptor _billingMethodDescriptor;

            private readonly IList<Func<bool>> _filters
                = new List<Func<bool>>();

            private readonly IList<Action<bool>> _afterFilters
                = new List<Action<bool>>();

            public BillingMethodsManagerContext(BillingMethodsManager billingMethodsManager, IGotBillingInformations orderForm, BillingMethodDescriptor billingMethodDescriptor)
            {
                _orderForm = orderForm;
                _billingMethodsManager = billingMethodsManager;
                _billingMethodDescriptor = billingMethodDescriptor;
            }

            public BillingMethodsManagerContext Filter(Func<bool> filter)
            {
                _filters.Add(filter);
                return this;
            }

            public BillingMethodsManagerContext AfterFilter(Action<bool> afterFilter)
            {
                _afterFilters.Add(afterFilter);
                return this;
            }

            public void Compute()
            {
                if (_billingMethodDescriptor == null)
                {
                    return;
                }
                var groupData = _orderForm.GetPrivateData<PaymentSelectionData>("payment");

                // Si la méthode est autorisée
                if (_billingMethodsManager.IsValidMethod(groupData.AllowedMethodMask, _billingMethodDescriptor))
                {
                    // et que le filtre retourne FAUX
                    if (!_filters.All(f => f()))
                    {
                        // on vire le bit correspondant
                        groupData.AllowedMethodMask &= ~_billingMethodDescriptor.Id;

                        foreach (var afterFilter in _afterFilters)
                        {
                            afterFilter(false);
                        }
                    }
                    else
                    {
                        foreach (var afterFilter in _afterFilters)
                        {
                            afterFilter(true);
                        }
                    }
                }

                if ((groupData.AllowedMethodMask & _billingMethodDescriptor.Id) == 0)
                {
                    var listBillingMethodToRemove = new List<int>();
                    foreach (var billingMethod in _orderForm.OrderBillingMethods)
                    {
                        if (billingMethod.BillingMethodId == _billingMethodDescriptor.Id)
                        {
                            listBillingMethodToRemove.Add(billingMethod.BillingMethodId);
                        }
                    }

                    listBillingMethodToRemove.ForEach(bm => _orderForm.RemoveBillingMethod(bm));

                    _orderForm.BillingMethodMask &= ~_billingMethodDescriptor.Id;
                }
            }
        }
    }
}
