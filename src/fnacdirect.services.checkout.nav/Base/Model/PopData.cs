
namespace FnacDirect.OrderPipe.Base.Model.Pop
{
    public class PopData : GroupData
    {
        public PopData()
        {
            GeoAutoSelectionEnabled = true;
        }

        public bool UseReactEvaluated { get; set; }
        public bool UseReact { get; set; }
        public int OldDeletedQuantity { get; set; }
        public bool DisplayBasketValidated { get; set; }
        public bool DisplayOnePageValidated { get; set; }
        public bool DisplayTvRoyaltyValidated { get; set; }
        public bool MissingBirthDateHasBeenProvided { get; set; }
        public bool MembershipBirthDateHasBeenConfirmed { get; set; }
        public bool MembershipCGVHasBeenConfirmed { get; set; }
        public bool MembershipHasOptInEmail { get; set; }

        public bool GeoAutoSelectionEnabled { get; set; }
        public bool ShippingAddressSetByUser { get; set; }
        public bool HasBillingAddress { get; set; }
        public bool BillingAddressSetByUser { get; set; }
        public bool HasLimitedQuantity { get; set; }
        public bool AdvertisingHasBenDisplayed { get; set; }
        public int BillingMethodDisabledForClickAndCollectMask { get; set; }
        public bool DisplayRemovedVoucherNotification { get; set; }
        public bool HasExcludedAvailabilitiesForDeliveryDates { get; set; }
        public bool AccountHasJusteBenCreated { get; set; }
        public bool UserSignedIn { get; set; }
        public string PaymentABTest { get; set; }
        public bool HasJustECCVInBasket { get; set; }
        
    }
}
