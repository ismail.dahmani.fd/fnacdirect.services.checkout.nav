namespace FnacDirect.OrderPipe
{
    public enum NoDirectAccessBehavior
    {
        Next,
        Previous
    }
}
