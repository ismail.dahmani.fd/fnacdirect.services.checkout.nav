using System.Linq;
using System.Collections.Generic;
using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.OrderPipe.Base.Model.TaxCalculation;
using FnacDirect.OrderPipe.Base.Model.LineGroups;
using Online = FnacDirect.Contracts.Online.Model;

namespace FnacDirect.OrderPipe.Base.Business.TaxCalculation
{
    public static class OrderPipeLineGroupQueryBuilder
    {
        public static OrderPipeLineGroupQuery BuildOrderPipeLineGroupsQuery(int shippingTaxId, decimal shippingCost, bool hasShippableItems)
        {
            var orderPipeLineGroupQuery = new OrderPipeLineGroupQuery()
            {
                HasShippableItems = hasShippableItems
            };
            orderPipeLineGroupQuery.AddShippingGlobalPrice(shippingCost, shippingTaxId);

            return orderPipeLineGroupQuery;
        }

        public static List<TaxCalculationQueryItem> BuildStandardItems(IEnumerable<ILineGroup> lineGroups, int sellerId)
        {
            var standardArticles = lineGroups.SelectMany(lg => lg.Articles.OfType<StandardArticle>().Where(sa => sa.Type != ArticleType.Free));
            var articleAndServices = GetFlatArticleAndServices(standardArticles);
            var orderPipeItems = BuildStandardArticles(articleAndServices, sellerId);

            return orderPipeItems;
        }

        public static List<TaxCalculationQueryItem> BuildStandardArticles(IEnumerable<StandardArticle> standardArticles, int sellerId)
        {
            var orderPipeItems = new List<TaxCalculationQueryItem>();

            foreach (var standardArticle in standardArticles)
            {
                var orderPipeItem = new TaxCalculationQueryItem
                {
                    Identifier = new Identifier
                    {
                        Prid = standardArticle.ProductID.GetValueOrDefault()
                    },
                    IsDematItem = standardArticle.IsNumerical,
                    SalesInfo = ConvertSalesInfoToOrderPipeSalesInfo(standardArticle.SalesInfo),
                    SellerId = sellerId
                };

                orderPipeItems.Add(orderPipeItem);
            }

            return orderPipeItems;
        }

        public static OrderContext BuildOrderContext(int marketId, int billingCountryId, int? shippingCountryId, bool isProfessionalAddress, string siteId, string pipeName, string customerTaxNumber)
        {
            return new OrderContext
            {
                MarketId = marketId,
                BillingCountryId = billingCountryId,
                ReceptionCountryId = shippingCountryId,
                IsProAddress = isProfessionalAddress,
                SiteId = siteId,
                PipeName = pipeName,
                CustomerTaxNumber = customerTaxNumber
            };
        }

        #region private method
        private static IEnumerable<StandardArticle> GetFlatArticleAndServices(IEnumerable<StandardArticle> standardArticles)
        {
            var services = standardArticles.SelectMany(art => art.Services).Where(s => s.Type != ArticleType.Free);

            var articleAndServices = standardArticles.Union(services);
            return articleAndServices;
        }

        private static OrderPipeSalesInfo ConvertSalesInfoToOrderPipeSalesInfo(Online.SalesInfo salesInfo)
        {
            var orderPipeSalesInfo = new OrderPipeSalesInfo();

            if (salesInfo != null)
            {
                orderPipeSalesInfo.TaxId = salesInfo.VATId;
                orderPipeSalesInfo.TaxRate = salesInfo.VATRate;
                orderPipeSalesInfo.ReducedPrice = new OrderPipeSalesInfoPrice { TTC = salesInfo.ReducedPrice.TTC, HT = salesInfo.ReducedPrice.HT };
                orderPipeSalesInfo.StandardPrice = new OrderPipeSalesInfoPrice { TTC = salesInfo.StandardPrice.TTC, HT = salesInfo.StandardPrice.HT };
                orderPipeSalesInfo.PublicPrice = new OrderPipeSalesInfoPrice { TTC = salesInfo.PublicPrice.TTC, HT = salesInfo.PublicPrice.HT };
                orderPipeSalesInfo.EcoTaxAmount = new OrderPipeSalesInfoPrice { };
                                             
                if (salesInfo.EcoTaxAmount.TTC.HasValue)
                {
                    orderPipeSalesInfo.EcoTaxAmount = new OrderPipeSalesInfoPrice
                    {
                        TTC = salesInfo.EcoTaxAmount.TTC.GetValueOrDefault(),
                        HT = salesInfo.EcoTaxAmount.HT.GetValueOrDefault()
                    };
                }
            }

            return orderPipeSalesInfo;
        }
        #endregion
    }
}
