namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Basket
{
    public class ReducedPriceViewModel
    {
        public string Price { get; set; }

        public string Reduction { get; set; }
    }
}
