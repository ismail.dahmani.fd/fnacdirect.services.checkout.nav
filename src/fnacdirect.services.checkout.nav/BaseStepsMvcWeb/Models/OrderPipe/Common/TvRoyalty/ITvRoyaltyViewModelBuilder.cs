﻿using FnacDirect.OrderPipe.Base.Model.Facets;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop
{
    public interface ITvRoyaltyViewModelBuilder
    {
        TvRoyaltyFormViewModel Build(TvRoyaltyFormViewModel viewModel);
        TvRoyaltyFormViewModel Build(IGotShippingMethodEvaluation iGotShippingMethodEvaluation);
    }
}
