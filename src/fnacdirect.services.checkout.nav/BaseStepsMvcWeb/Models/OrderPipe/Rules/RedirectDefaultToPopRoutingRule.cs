﻿using FnacDirect;
using FnacDirect.Technical.Framework.Web;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Routing;

namespace FnacDirect.OrderPipe.CoreRouting.Rules
{
    public class RedirectDefaultToPopRoutingRule : SwitchableRoutingRule
    {
        private readonly Func<IUrlManager> _urlManagerFactory;

        public RedirectDefaultToPopRoutingRule(ISwitchProvider switchProvider, Func<IUrlManager> urlManagerFactory)
            : base(switchProvider, "orderpipe.routing.rule.redirectdefaulttopop")
        {
            _urlManagerFactory = urlManagerFactory;
        }

        private static bool IsAjaxRequest(RequestContext requestContetx)
        {
            var request = requestContetx.HttpContext.Request;

            var query = request.QueryString;
            if (query != null)
            {
                if (query["X-Requested-With"] == "XMLHttpRequest")
                {
                    return true;
                }
            }

            var headers = request.Headers;
            if (headers != null)
            {
                if (headers["X-Requested-With"] == "XMLHttpRequest")
                {
                    return true;
                }
            }
            return false;
        }

        protected override IHttpHandler InnerApply(RequestContext requestContext)
        {
            var pipeName = RouteTable.Routes.GetPipeName();

            if (!IsAjaxRequest(requestContext))
            {
                if (pipeName.ToLowerInvariant() == "default"
                    || pipeName.ToLowerInvariant() == "mobile"
                    || pipeName.ToLowerInvariant() == "achatrapide"
                    || pipeName.ToLowerInvariant() == "achatrapidemobile")
                {
                    var urlManager = _urlManagerFactory();

                    var routeValueDictionary = new RouteValueDictionary();

                    foreach (string key in requestContext.HttpContext.Request.QueryString.Keys)
                    {
                        if (key == "pipe" || key == "reset")
                        {
                            continue;
                        }

                        routeValueDictionary.Add(key, requestContext.HttpContext.Request.QueryString.Get(key));
                    }

                    var url = urlManager.Sites.OrderPipe.GetPage("Root").DefaultUrl;

                    foreach (var parameter in routeValueDictionary)
                    {
                        url.WithParam(parameter.Key, parameter.Value.ToString());
                    }

                    return new RedirectHttpHandler(url.ToString());
                }
            }

            return null;
        }
    }
}
