﻿using System.Xml.Serialization;

namespace FnacDirect.OrderPipe.Base.ContextualizedOperation
{
    public class ContextualizedOperationData : GroupData
    {
        public string PublicId { get; set; }
        public string Token { get; set; }
        public int OperationId { get; set; }
        public int TokenId { get; set; }
        public int? Origin { get; set; }
    }
}
