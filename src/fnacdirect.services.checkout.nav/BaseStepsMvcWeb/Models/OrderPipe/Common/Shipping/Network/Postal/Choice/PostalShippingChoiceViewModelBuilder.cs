using FnacDirect.Contracts.Online.Model;
using FnacDirect.OrderPipe.Base.BaseModel;
using FnacDirect.OrderPipe.Base.Business;
using FnacDirect.OrderPipe.Base.Business.Helpers;
using FnacDirect.OrderPipe.Base.Business.Shipping;
using FnacDirect.OrderPipe.Base.Model.Solex;
using FnacDirect.OrderPipe.Core.Services;
using FnacDirect.Technical.Framework.Web.Mvc;
using System.Linq;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Common.Shipping.Postal
{
    public class PostalShippingChoiceViewModelBuilder : IPostalShippingChoiceViewModelBuilder
    {
        private readonly IPricingService _pricingService;
        private readonly IApplicationContext _applicationContext;
        private readonly IVatCountryCalculationService _vatCountryCalculationService;
        private readonly IPriceHelper _priceHelper;
        private readonly ISwitchProvider _switchProvider;
        private readonly IPipeParametersProvider _pipeParametersProvider;

        private readonly int _vatRateId;
        private readonly int _localCountry;

        public PostalShippingChoiceViewModelBuilder(IApplicationContext applicationContext,
                                                    IPricingServiceProvider pricingServiceProvider,
                                                    IPipeParametersProvider pipeParametersProvider,
                                                    IVatCountryCalculationService vatCountryCalculationService,
                                                    ISwitchProvider switchProvider)
        {
            _applicationContext = applicationContext;
            _pricingService = pricingServiceProvider.GetPricingService(_applicationContext.GetSiteContext().MarketId);
            _priceHelper = _applicationContext.GetPriceHelper();
            _vatCountryCalculationService = vatCountryCalculationService;
            _switchProvider = switchProvider;
            _pipeParametersProvider = pipeParametersProvider;

            _vatRateId = pipeParametersProvider.Get("shipping.vat.rate.id", -1);
            _localCountry = pipeParametersProvider.Get<int>("culture.localcountry.id");
        }

        public void BuildDelays(PostalShippingChoiceViewModel postalShippingChoiceViewModel, int articleCount, string label, int currentShippingMethodId, ShippingChoice choice)
        {
            if (!postalShippingChoiceViewModel.Delays.Any(d => d.Delay == label))
            {
                var delay = new DelayModel
                {
                    NbProducts = articleCount,
                    Delay = label
                };

                if (currentShippingMethodId == choice.MainShippingMethodId)
                {
                    postalShippingChoiceViewModel.Delays.Insert(0, delay);
                }
                else
                {
                    postalShippingChoiceViewModel.Delays.Add(delay);
                }
            }
            else
            {
                postalShippingChoiceViewModel.Delays.FirstOrDefault(d => d.Delay == label).NbProducts += articleCount;
            }
        }

        public PostalShippingChoiceViewModel Builder(ShippingChoice choice, PostalShippingMethodEvaluationItem postalShippingMethodEvaluationItem)
        {
            var isFnacPro = _applicationContext.GetSiteId() == (int)FnacSites.FNACPRO;

            var postalShippingChoiceViewModel = new PostalShippingChoiceViewModel
            {
                IsFreeShipping = choice.TotalPrice.Cost == 0M,
                TotalPrice = choice.TotalPrice.Cost,
                Weight = choice.Weight,
                CarbonFootprint = choice.CarbonFootprint ?? choice.DefaultChoice.CarbonFootprint,
            };

            // Using price cooming from taxcalculation nuget.
            if (!_pipeParametersProvider.Get("taxcalculationservice.forcelegacy", false)
                && _switchProvider.IsEnabled("orderpipe.pop.taxcalculationservice.activation"))
            {
                postalShippingChoiceViewModel.ShippingCost = _priceHelper.Format(choice.TotalPrice.CostToApply.GetValueOrDefault(), PriceFormat.CurrencyAsDecimalSeparator);
                postalShippingChoiceViewModel.ShippingCostModel = _priceHelper.GetDetails(choice.TotalPrice.CostToApply.GetValueOrDefault());
                postalShippingChoiceViewModel.ShippingCostNoVat = _priceHelper.Format(choice.TotalPrice.NoTaxCost.GetValueOrDefault(), PriceFormat.CurrencyAsDecimalSeparator);
                postalShippingChoiceViewModel.ShippingCostNoVatModel = _priceHelper.GetDetails(choice.TotalPrice.NoTaxCost.GetValueOrDefault());
                postalShippingChoiceViewModel.ShippingCostToDisplay = isFnacPro ? _priceHelper.Format(choice.TotalPrice.NoTaxCost.GetValueOrDefault(), PriceFormat.CurrencyAsDecimalSeparator)
                                                                                : _priceHelper.Format(choice.TotalPrice.CostToApply.GetValueOrDefault(), PriceFormat.CurrencyAsDecimalSeparator);
                postalShippingChoiceViewModel.ShippingCostModelToDisplay = isFnacPro ? _priceHelper.GetDetails(choice.TotalPrice.NoTaxCost.GetValueOrDefault())
                                                                                     : _priceHelper.GetDetails(choice.TotalPrice.CostToApply.GetValueOrDefault());
            }
            else
            {
                var vatRate = _pricingService.GetVATRate(_applicationContext.GetCountryId(), _vatRateId);
                var choiceTotalPriceNoVat = PriceHelpers.GetPriceHT(choice.TotalPrice.Cost, vatRate);

                //should use tax but it calculated below...
                var vatCountry = _vatCountryCalculationService.GetVatCountry(_localCountry, postalShippingMethodEvaluationItem.ShippingAddress);
                var price = _pricingService.GetPrice(choice.TotalPrice.Cost, null, vatCountry, _vatRateId, _localCountry);
                var shippingCost = _priceHelper.Format(price, PriceFormat.CurrencyAsDecimalSeparator);

                var shippingCostNoVat = _priceHelper.Format(choiceTotalPriceNoVat, PriceFormat.CurrencyAsDecimalSeparator);

                postalShippingChoiceViewModel.ShippingCost = shippingCost;
                postalShippingChoiceViewModel.ShippingCostModel = _priceHelper.GetDetails(choice.TotalPrice.Cost);
                postalShippingChoiceViewModel.ShippingCostNoVat = shippingCostNoVat;
                postalShippingChoiceViewModel.ShippingCostNoVatModel = _priceHelper.GetDetails(choiceTotalPriceNoVat);
                postalShippingChoiceViewModel.ShippingCostToDisplay = isFnacPro ? _priceHelper.Format(choiceTotalPriceNoVat, PriceFormat.CurrencyAsDecimalSeparator)
                                                                                : shippingCost;
                postalShippingChoiceViewModel.ShippingCostModelToDisplay = isFnacPro ? _priceHelper.GetDetails(choiceTotalPriceNoVat)
                                                                                     : _priceHelper.GetDetails(choice.TotalPrice.Cost);
            }

            return postalShippingChoiceViewModel;
        }

        public void CheckVatEligibility(PostalShippingChoiceViewModel postalShippingChoiceViewModel, PostalShippingMethodEvaluationItem postalShippingMethodEvaluationItem, ShippingChoice choice)
        {
            if (postalShippingMethodEvaluationItem.ShippingAddress != null)
            {
                var vatCountry = _vatCountryCalculationService.GetVatCountry(_localCountry, postalShippingMethodEvaluationItem.ShippingAddress);
                if (!vatCountry.UseVat)
                {
                    var vatRate = _pricingService.GetVATRate(_applicationContext.GetCountryId(), _vatRateId);
                    var choiceTotalPriceNoVat = PriceHelpers.GetPriceHT(choice.TotalPrice.Cost, vatRate);
                    postalShippingChoiceViewModel.ShippingCost = _priceHelper.Format(choiceTotalPriceNoVat, PriceFormat.CurrencyAsDecimalSeparator);
                    postalShippingChoiceViewModel.ShippingCostModel = _priceHelper.GetDetails(choiceTotalPriceNoVat);
                    postalShippingChoiceViewModel.ShippingCostToDisplay = postalShippingChoiceViewModel.ShippingCost;
                    postalShippingChoiceViewModel.ShippingCostModelToDisplay = postalShippingChoiceViewModel.ShippingCostModel;
                }
            }
        }
    }
}
