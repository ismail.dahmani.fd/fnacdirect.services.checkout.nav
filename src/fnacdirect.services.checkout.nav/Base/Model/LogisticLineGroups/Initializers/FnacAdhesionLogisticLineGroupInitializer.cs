using FnacDirect.OrderPipe.Base.BaseModel;
using FnacDirect.OrderPipe.Base.Business.LogisticLineGroups.Internals.Initializers;
using FnacDirect.OrderPipe.Base.LineGroups;
using FnacDirect.OrderPipe.Base.Model;
using System;
using System.Collections.Generic;
using System.Linq;

namespace FnacDirect.OrderPipe.Base.Business.LogisticLineGroups.Initializers
{
    public class FnacAdhesionLogisticLineGroupInitializer : BaseLogisticLineGroupInitializer
    {
        private readonly ILogisticLineGroupGenerator _logisticLineGroupGenerator;

        public FnacAdhesionLogisticLineGroupInitializer(ILogisticLineGroupGenerator logisticLineGroupGenerator)
        {
            _logisticLineGroupGenerator = logisticLineGroupGenerator;
        }

        public override IEnumerable<ILogisticLineGroup> Initialize(IDictionary<ILineGroupHeader, IEnumerable<Article>> selectedLineGroups)
        {
            var shippingMethodEvaluation = Context.IGotShippingMethodEvaluation.ShippingMethodEvaluation;

            var output = new List<ILogisticLineGroup>();

            foreach (var lineGroup in selectedLineGroups.Keys)
            {
                var articles = new List<Article>(selectedLineGroups[lineGroup]);
                var standardArticles = articles.OfType<StandardArticle>();
                var adhesionCardArticles = standardArticles.Where(a => a.IsAdhesionCard).ToList();
                var adhesionLogisticLineGroup = CreateLogisticLineGroup(lineGroup, shippingMethodEvaluation, adhesionCardArticles);

                ResetWrapInformationForAdhesionLogisticLineGroup(adhesionLogisticLineGroup);
                adhesionLogisticLineGroup.UniqueId = Guid.NewGuid().ToString();
                output.Add(adhesionLogisticLineGroup);
            }

            return output;
        }

        private LogisticLineGroup CreateLogisticLineGroup(ILineGroupHeader lineGroup, ShippingMethodEvaluation shippingMethodEvaluation, IEnumerable<StandardArticle> standardArticles)
        {
            var logisticLineGroup = _logisticLineGroupGenerator.Generate(lineGroup);

            logisticLineGroup.Informations.OrderType = (int)OrderInfoOrderTypeEnum.Standard;
            logisticLineGroup.OrderType = OrderTypeEnum.FnacCom;
            logisticLineGroup.Articles = standardArticles.ToList<Article>();

            Helper.CopyShippingMethodEvaluation(lineGroup, logisticLineGroup, shippingMethodEvaluation);

            return logisticLineGroup;
        }

        private void ResetWrapInformationForAdhesionLogisticLineGroup(LogisticLineGroup adhesionLogisticLineGroup)
        {
            adhesionLogisticLineGroup.Informations.ChosenWrapMethod = 0;
            adhesionLogisticLineGroup.GlobalPrices.TotalShoppingCartBusinessWrapPriceDBEur = 0;
            adhesionLogisticLineGroup.GlobalPrices.TotalShoppingCartBusinessWrapPriceNoVatEur = 0;
            adhesionLogisticLineGroup.GlobalPrices.TotalShoppingCartBusinessWrapPriceUserEur = 0;
        }
    }
}
