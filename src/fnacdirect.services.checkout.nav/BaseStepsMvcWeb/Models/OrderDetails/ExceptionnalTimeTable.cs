﻿using System.Collections.Generic;

namespace FnacDirect.OrderPipe.Base.Business.OrderDetail
{
    public class ExceptionnalTimeTable
    {
        public List<TimeTable> ExceptionnalOpening { get; set; }

        public List<TimeTable> ExceptionnalClosing { get; set; }       
    }
}
