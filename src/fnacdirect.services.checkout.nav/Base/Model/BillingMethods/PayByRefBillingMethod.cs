namespace FnacDirect.OrderPipe.Base.Model
{
    public class PayByRefBillingMethod : BillingMethod
    {
        public const int IdBillingMethod = 131072;

        public override bool IsPrimary => true;

        public static string BillingMethodName
        {
            get
            {
                return "PayByRefBillingMethod";
            }
        }

        public PayByRefBillingMethod()
        {
            BillingMethodId = IdBillingMethod;
            CanModifyAmountUp = true;
            CanModifyAmountDown = true;
        }

        private string _merchantId;
        public string MerchantId
        {
            get { return _merchantId; }
            set { _merchantId = value; }
        }

        /// <summary>
        /// IBAN utilisé pour le remboursement
        /// </summary>
        private string bankAccountNumber;
        public string BankAccountNumber
        {
            get { return bankAccountNumber; }
            set { bankAccountNumber = value; }
        }       

        /// <summary>
        /// Auto Increment utilisé par Fnac.com pour générer la réference
        /// </summary>
        private string _reference;
        public string Reference
        {
            get { return _reference; }
            set { _reference = value; }
        }      

        /// <summary>
        /// Reference envoyée au client et utilisée pour le payement
        /// </summary>
        private string _paymentReference;
        public string PaymentReference
        {
            get { return _paymentReference; }
            set { _paymentReference = value; }
        }

        public override bool UseTransaction
        {
            get
            {
                return true;
            }
        }

        public override bool IsGlobalPayment
        {
            get
            {
                return true;
            }
        }
    }
}

