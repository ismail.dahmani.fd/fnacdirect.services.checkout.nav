﻿
using FnacDirect.OrderPipe.Base.Model;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Common.Basket
{
    public interface IOrderExternalReferenceViewModelBuilder
    {
        OrderExternalReferenceViewModel Build(PopOrderForm popOrderForm);
    }
}
