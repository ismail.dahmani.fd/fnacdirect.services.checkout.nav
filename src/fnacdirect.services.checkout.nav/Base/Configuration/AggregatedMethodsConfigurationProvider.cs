using FnacDirect.OrderPipe.Base.Business.Shipping;
using FnacDirect.OrderPipe.Core.Services;
using FnacDirect.Technical.Framework.CoreServices;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace FnacDirect.OrderPipe.Base.Business.Configuration
{
    public class AggregatedMethodsConfigurationProvider : IAggregatedMethodConfigurationProvider
    {
        const string ParameterKey = "aggregatedmethods.configuration";

        public AggregatedMethodsConfigurationProvider(IPipeParametersProvider pipeParametersProvider)
        {
            var configurationJson = pipeParametersProvider.Get(ParameterKey, string.Empty);

            if (!string.IsNullOrWhiteSpace(configurationJson))
            {
                try
                {
                    AggregatedMethodsConfiguration = JsonConvert.DeserializeObject<IDictionary<int, AggregatedMethodConfiguration>>(configurationJson);
                }
                catch (Exception ex)
                {
                    Logging.Current.WriteError($"The global parameter \"{ParameterKey}\" value is not correct ({configurationJson})", ex);
                }
            }
        }

        public IDictionary<int, AggregatedMethodConfiguration> AggregatedMethodsConfiguration { get; } = new Dictionary<int, AggregatedMethodConfiguration>();
    }
}
