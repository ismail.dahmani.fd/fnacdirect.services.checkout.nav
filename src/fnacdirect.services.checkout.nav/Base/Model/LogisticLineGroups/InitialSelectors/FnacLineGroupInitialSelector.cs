﻿using FnacDirect.OrderPipe.Base.Business.LogisticLineGroups.Internals.InitialSelectors;
using FnacDirect.OrderPipe.Base.LineGroups;
using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.OrderPipe.Base.Model.LineGroups;
using FnacDirect.Technical.Framework.Switch;
using System.Collections.Generic;
using System.Linq;

namespace FnacDirect.OrderPipe.Base.Business.LogisticLineGroups.InitialSelectors
{
    public class FnacLineGroupInitialSelector : BaseLineGroupInitialSelector
    {
        public override IDictionary<ILineGroupHeader, IEnumerable<Article>> Select(IEnumerable<ILineGroup> lineGroups)
        {
            if (Switch.IsEnabled("orderpipe.pop.fdv.enable", false))
            {
                var output = new Dictionary<ILineGroupHeader, IEnumerable<Article>>();

                //on recupere les lineGroups fnac.com qui ne sont pas en intramag
                var shopLineGroups = lineGroups.Where(l => l.Seller.Type == SellerType.Fnac);

                foreach (var lineGroup in shopLineGroups)
                {
                    var articles = lineGroup.Articles.OfType<StandardArticle>();

                    if (articles.Any())
                    {
                        output.Add(lineGroup, articles);
                    }
                }

                return output;
            }

            return lineGroups.Where(l => l.Seller.Type == SellerType.Fnac).ToDictionary(l => l as ILineGroupHeader, l => l.Articles.OfType<Article>());
        }
    }
}
