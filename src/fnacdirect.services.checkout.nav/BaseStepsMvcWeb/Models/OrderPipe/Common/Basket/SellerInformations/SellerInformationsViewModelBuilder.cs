﻿using FnacDirect.OrderPipe.Base.LineGroups;
using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.OrderPipe.Base.Steps.Pop;
using FnacDirect.Technical.Framework.Web.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Common.Basket
{
    public class SellerInformationsViewModelBuilder : ISellerInformationsViewModelBuilder
    {
        private readonly IApplicationContext _applicationContext;
        private readonly ICountryService _countryService;

        public SellerInformationsViewModelBuilder(IApplicationContext applicationContext, ICountryService countryService)
        {
            _applicationContext = applicationContext;
            _countryService = countryService;
        }

        public SellerInformationsViewModel Build(PopOrderForm popOrderForm, int sellerId)
        {
            SellerInformationsViewModel sellerViewModel;

            if (SellerIds.GetType(sellerId) != SellerType.MarketPlace)
            {
                sellerViewModel = new SellerInformationsViewModel();
            }
            else
            {
                var lineGroups = popOrderForm.LineGroups.Seller(sellerId);
                var mpArticle = lineGroups.SelectMany(l => l.Articles).OfType<MarketPlaceArticle>().FirstOrDefault();
                // Dans le cas où les linegroups ne contiennent aucun article,
                // on va chercher dans les articles indispo le premier article indispo du vendeur
                if (mpArticle == null)
                {
                    mpArticle = popOrderForm.DeletedArticles.OfType<MarketPlaceArticle>().FirstOrDefault(a => a.SellerID == sellerId);
                }

                var sellerDisplayName = string.Empty;
                var country = string.Empty;
                var shouldDisplayCountry = false;
                var isPro = false;
                var rating = 0m;
                var salesNumber = new int?();
                var isTopSeller = false;

                if (mpArticle != null)
                {
                    var seller = mpArticle.Offer.Seller;
                    var siteCountryId = _applicationContext.GetSiteContext().CountryId;
                    shouldDisplayCountry = seller.ShippingCountryId.Value > 0 && siteCountryId != seller.ShippingCountryId.Value;

                    if (shouldDisplayCountry)
                    {
                        var countryEntity = _countryService.GetCountries().FirstOrDefault(c => c.ID == seller.ShippingCountryId.Value);

                        if (countryEntity != null)
                        {
                            country = countryEntity.Label;
                        }
                        else
                        {
                            shouldDisplayCountry = false;
                        }
                    }

                    var lineGroup = lineGroups.FirstOrDefault();

                    if (lineGroup != null)
                    {
                        sellerDisplayName = lineGroup.Seller.DisplayName;
                    }

                    isPro = seller.SellerType == 3;
                    rating = seller.ReliabilityRate;
                    salesNumber = seller.SalesNumber;
                    isTopSeller = seller.Prenium;
                }

                sellerViewModel = new MarketPlaceSellerInformationsViewModel(sellerDisplayName, country, shouldDisplayCountry, isPro)
                {
                    Rating = rating,
                    Sales = salesNumber,
                    IsTopSeller = isTopSeller
                };
            }

            var sellers = popOrderForm.LineGroups.Sellers().ToList();

            sellerViewModel.SellerIndex = sellers.IndexOf(sellerId) + 1; // Index affiché au client (commence à 1)
            sellerViewModel.TotalSeller = sellers.Count;
            sellerViewModel.HasManySeller = sellers.Count > 1;
            sellerViewModel.IsClickAndCollectOnly = popOrderForm.LineGroups.Any(lg => lg.Seller.SellerId == sellerId && lg.Seller.Type == SellerType.ClickAndCollectOnly);

            return sellerViewModel;
        }
    }
}
