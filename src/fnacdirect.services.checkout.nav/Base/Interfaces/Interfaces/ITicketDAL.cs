using System;

namespace FnacDirect.OrderPipe.Base.DAL
{
    public interface ITicketDAL
    {
        void AddTicket(int ticketType, int sourceProcessId, int ticketSequenceNumber, int ticketStatus, int associatedId, int associatedIdType, string refUg,
            string countryCode, string appScope, DateTime? groupDate, string checkoutNumber);

        void AddMainOrderDocument(int documentType, int mainOrderId, char[] documentContent, string documentData);
        void InsertGuptTicketNumber(int mainOrderId, string guptTicketId);
        string GetMainOrderDocumentByUserRef(string userRef);
        bool HasExistingTicketInserted(int ticketType, int associatedId, int associatedIdType);
    }
}
