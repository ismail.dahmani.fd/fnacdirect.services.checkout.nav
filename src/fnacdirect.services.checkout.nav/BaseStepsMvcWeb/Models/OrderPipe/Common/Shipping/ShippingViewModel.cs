using System;
using FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Common.Shipping.Relay;
using FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Common.Shipping.Shop;
using FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Common.Shipping.Postal;
using FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Common.Basket;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Common.Shipping
{
    public class ShippingViewModel
    {
        private readonly int _sellerId;
        private readonly ShippingViewModelMode _shippingViewModeMode;

        public ShippingViewModel(int sellerId, ShippingViewModelMode shippingViewModeMode)
        {
            _shippingViewModeMode = shippingViewModeMode;
            _sellerId = sellerId;

            Postal = Maybe<PostalShippingNetworkViewModel>.Empty();
            Shop = Maybe<ShopShippingNetworkViewModel>.Empty();
            Relay = Maybe<RelayShippingNetworkViewModel>.Empty();
        }

        public bool IsOneClick { get; set; }

        public int SellerId
        {
            get
            {
                return _sellerId;
            }
        }

        public int NetworksCount
        {
            get
            {
                var count = 0;

                if (Postal.HasValue)
                {
                    count++;
                }

                if (Relay.HasValue)
                {
                    count++;
                }

                if (Shop.HasValue)
                {
                    count++;
                }

                return count;
            }
        }

        public Maybe<PostalShippingNetworkViewModel> Postal { get; set; }

        public Maybe<RelayShippingNetworkViewModel> Relay { get; set; }

        public Maybe<ShopShippingNetworkViewModel> Shop { get; set; }

        public Maybe<BillingOnlyViewModel> BillingOnly { get; set; }

        public BillingAddressModel BillingAddressModel { get; set; }

        public ShippingViewModelMode Mode
        {
            get { return _shippingViewModeMode; }
        }

        public Maybe<ISelectedShippingItem> SelectedShippingItem
        {
            get
            {
                if (Mode == ShippingViewModelMode.Postal
                    && Postal.HasValue
                    && Postal.Value.Selected.HasValue)
                {
                    return Postal.Value.Selected.Value;
                }

                if (Mode == ShippingViewModelMode.Shop
                    && Shop.HasValue
                    && Shop.Value.Selected.HasValue)
                {
                    return Shop.Value.Selected.Value;
                }

                if (Mode == ShippingViewModelMode.Relay
                    && Relay.HasValue
                    && Relay.Value.Selected.HasValue)
                {
                    return Relay.Value.Selected.Value;
                }

                return Maybe<ISelectedShippingItem>.Empty();
            }
        }
      
        public ExpressPlusViewModel ExpressPlusInfo { get; set; }

        public bool HasExpressPlus { get; set; }

        public FnacPlusViewModel FnacPlusInfos { get; set; }

        public bool HasManyEligibleBillingAddresses { get; set; }

        public bool HasWrap { get; set; }

        public bool HasFnacPlusCardInBasket { get; set; }

        public bool IsFnacPlusUser { get; set; }

        public DateTime? BestCaseDeliveryDate
        {
            get
            {
                DateTime? dateTime = null;

                if(Postal.HasValue)
                {
                    foreach(var choice in Postal.Value.PostalShippingChoices)
                    {
                        if(choice.WorstDeliveryDate.HasValue
                            && (dateTime == null || choice.WorstDeliveryDate.Value < dateTime))
                        {
                            dateTime = choice.WorstDeliveryDate.Value;
                        }
                    }
                }

                if(Relay.HasValue && Relay.Value.Selected.HasValue)
                {
                    if(Relay.Value.Selected.Value.WorstDeliveryDate.HasValue
                        && (dateTime == null || Relay.Value.Selected.Value.WorstDeliveryDate.Value < dateTime))
                    {
                        dateTime = Relay.Value.Selected.Value.WorstDeliveryDate.Value;
                    }
                }

                if (Shop.HasValue && Shop.Value.Selected.HasValue)
                {
                    if (Shop.Value.Selected.Value.WorstDeliveryDate.HasValue
                        && (dateTime == null || Shop.Value.Selected.Value.WorstDeliveryDate.Value < dateTime))
                    {
                        dateTime = Shop.Value.Selected.Value.WorstDeliveryDate.Value;
                    }
                }

                return dateTime;
            }
        }

        public bool IsValid { get; set; }

        public string ChristmasWording { get; set; }

        public bool HasChristmasWording {
            get
            {
                return !string.IsNullOrEmpty(ChristmasWording);
            }
        }
    }
}
