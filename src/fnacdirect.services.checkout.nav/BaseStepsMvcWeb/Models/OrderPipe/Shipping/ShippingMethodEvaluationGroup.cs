using FnacDirect.OrderPipe.Base.LineGroups;
using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.OrderPipe.Base.Model.Shipping.ShippingMethod;
using FnacDirect.OrderPipe.Base.Model.Solex;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Xml;

namespace FnacDirect.OrderPipe.Base.BaseModel
{
    public class ShippingMethodEvaluationGroup
    {
        public ShippingMethodEvaluationGroup(params ShippingMethodEvaluationType[] allowedTypes) : this(SellerIds.GetId(SellerType.Fnac), allowedTypes)
        {
            _allowedTypes = allowedTypes;
        }

        public ShippingMethodEvaluationGroup(int sellerId, params ShippingMethodEvaluationType[] allowedTypes)
        {
            _sellerId = sellerId;
            _allowedTypes = allowedTypes;
        }

        private readonly int _sellerId;

        public int SellerId
        {
            get
            {
                return _sellerId;
            }
        }

        private const int RetraitChezLeVendeur = 28;

        private readonly IList<ShippingMethodEvaluationItem> _shippingMethodEvaluationItems
            = new List<ShippingMethodEvaluationItem>();

        private Maybe<int> _selectedShippingMethod = Maybe<int>.Empty();

        public int? SelectedShippingMethod
        {
            get { return _selectedShippingMethod != null && _selectedShippingMethod.HasValue ? _selectedShippingMethod.Value : new int?(); }
            private set { _selectedShippingMethod = value ?? Maybe<int>.Empty(); }
        }

        private readonly IDictionary<Guid, IDictionary<int, int>> _lineGroupLogisticTypeShippingMethodSelection
            = new Dictionary<Guid, IDictionary<int, int>>();

        private readonly IEnumerable<ShippingMethodEvaluationType> _allowedTypes;

        public IEnumerable<ShippingMethodEvaluationType> AllowedTypes
        {
            get
            {
                return _allowedTypes;
            }
        }

        private ShippingMethodEvaluationType _selectedShippingMethodEvaluationType;

        public ShippingMethodEvaluationType SelectedShippingMethodEvaluationType
        {
            get
            {
                return _selectedShippingMethodEvaluationType;
            }
        }

        public ShippingMethodEvaluationItem SelectedShippingMethodEvaluation
        {
            get
            {
                return _shippingMethodEvaluationItems.FirstOrDefault(i => i.ShippingMethodEvaluationType == _selectedShippingMethodEvaluationType);
            }
        }

        public ShippingMethodEvaluationGroup RemoveAddress<T>(T shippingAddress)
            where T : ShippingAddress
        {
            if (shippingAddress == null)
                return this;

            var shippingMethodEvaluationType = GetShippingMethodEvaluationTypeFor(shippingAddress);

            var shippingMethodEvaluationItem = _shippingMethodEvaluationItems.FirstOrDefault(i => i.ShippingMethodEvaluationType == shippingMethodEvaluationType);

            if (shippingMethodEvaluationItem != null)
            {
                _shippingMethodEvaluationItems.Remove(shippingMethodEvaluationItem);
            }

            return this;
        }

        public ShippingMethodEvaluationGroup ReplaceAddress<T>(T shippingAddress)
            where T : ShippingAddress
        {
            return ReplaceAddress(shippingAddress, false);
        }

        public ShippingMethodEvaluationGroup ReplaceAddress<T>(T shippingAddress, bool isDummy)
            where T : ShippingAddress
        {
            if (shippingAddress == null)
                return this;

            var shippingMethodEvaluationType = GetShippingMethodEvaluationTypeFor(shippingAddress);

            var shippingMethodEvaluationItem = _shippingMethodEvaluationItems.FirstOrDefault(i => i.ShippingMethodEvaluationType == shippingMethodEvaluationType);

            if (shippingMethodEvaluationItem != null)
            {
                _shippingMethodEvaluationItems.Remove(shippingMethodEvaluationItem);
            }

            switch (shippingMethodEvaluationType)
            {
                case ShippingMethodEvaluationType.Postal:
                    _shippingMethodEvaluationItems.Add(new PostalShippingMethodEvaluationItem(shippingAddress as PostalAddress, isDummy));
                    if (PostalShippingMethodEvaluationItem.HasValue)
                    {
                        PostalShippingMethodEvaluationItem.Value.SelectedShippingMethodId = SelectedShippingMethod;
                    }
                    break;
                case ShippingMethodEvaluationType.Shop:
                    _shippingMethodEvaluationItems.Add(new ShopShippingMethodEvaluationItem(shippingAddress as ShopAddress, isDummy));
                    break;
                case ShippingMethodEvaluationType.Relay:
                    _shippingMethodEvaluationItems.Add(new RelayShippingMethodEvaluationItem(shippingAddress as RelayAddress, isDummy));
                    break;
            }
            return this;
        }

        public ShippingMethodEvaluationGroup WithAddressIfNotSet<T>(T shippingAddress)
            where T : ShippingAddress
        {
            if (shippingAddress == null)
                return this;

            var shippingMethodEvaluationType = GetShippingMethodEvaluationTypeFor(shippingAddress);

            var shippingMethodEvaluationItem = _shippingMethodEvaluationItems.FirstOrDefault(i => i.ShippingMethodEvaluationType == shippingMethodEvaluationType);

            if (shippingMethodEvaluationItem != null
                && shippingMethodEvaluationItem.IsDummyAddress)
            {
                _shippingMethodEvaluationItems.Remove(shippingMethodEvaluationItem);
                shippingMethodEvaluationItem = null;
            }

            if (shippingMethodEvaluationItem == null)
            {
                switch (shippingMethodEvaluationType)
                {
                    case ShippingMethodEvaluationType.Postal:
                        _shippingMethodEvaluationItems.Add(new PostalShippingMethodEvaluationItem(shippingAddress as PostalAddress, true));
                        break;
                    case ShippingMethodEvaluationType.Shop:
                        _shippingMethodEvaluationItems.Add(new ShopShippingMethodEvaluationItem(shippingAddress as ShopAddress, true));
                        break;
                    case ShippingMethodEvaluationType.Relay:
                        _shippingMethodEvaluationItems.Add(new RelayShippingMethodEvaluationItem(shippingAddress as RelayAddress, true));
                        break;
                }
            }

            return this;
        }

        private static ShippingMethodEvaluationType GetShippingMethodEvaluationTypeFor<T>(T shippingAddress)
        {
            var type = shippingAddress.GetType();

            if (type == typeof(PostalAddress))
                return ShippingMethodEvaluationType.Postal;

            if (type == typeof(ShopAddress))
                return ShippingMethodEvaluationType.Shop;

            if (type == typeof(RelayAddress))
                return ShippingMethodEvaluationType.Relay;

            return ShippingMethodEvaluationType.Unknown;
        }

        public IEnumerable<ShippingMethodEvaluationItem> ShippingMethodEvaluationItems
        {
            get
            {
                return _shippingMethodEvaluationItems;
            }
        }

        private ShippingMethodEvaluationItem GetShippingMethodEvaluationItemFor(ShippingMethodEvaluationType shippingMethodEvaluationType)
        {
            return _shippingMethodEvaluationItems.FirstOrDefault(i => i.ShippingMethodEvaluationType == shippingMethodEvaluationType);
        }

        public Maybe<PostalShippingMethodEvaluationItem> PostalShippingMethodEvaluationItem
        {
            get
            {
                var shippingMethodEvaluationItem = GetShippingMethodEvaluationItemFor(ShippingMethodEvaluationType.Postal);

                return shippingMethodEvaluationItem == null
                    ? Maybe<PostalShippingMethodEvaluationItem>.Empty()
                    : Maybe<PostalShippingMethodEvaluationItem>.Some(shippingMethodEvaluationItem as PostalShippingMethodEvaluationItem);
            }
        }

        public Maybe<ShopShippingMethodEvaluationItem> ShopShippingMethodEvaluationItem
        {
            get
            {
                var shippingMethodEvaluationItem = GetShippingMethodEvaluationItemFor(ShippingMethodEvaluationType.Shop);

                return shippingMethodEvaluationItem == null
                    ? Maybe<ShopShippingMethodEvaluationItem>.Empty()
                    : Maybe<ShopShippingMethodEvaluationItem>.Some(shippingMethodEvaluationItem as ShopShippingMethodEvaluationItem);
            }
        }

        public Maybe<RelayShippingMethodEvaluationItem> RelayShippingMethodEvaluationItem
        {
            get
            {
                var shippingMethodEvaluationItem = GetShippingMethodEvaluationItemFor(ShippingMethodEvaluationType.Relay);

                return shippingMethodEvaluationItem == null
                    ? Maybe<RelayShippingMethodEvaluationItem>.Empty()
                    : Maybe<RelayShippingMethodEvaluationItem>.Some(shippingMethodEvaluationItem as RelayShippingMethodEvaluationItem);
            }
        }

        public void SelectShippingMethodEvaluationType(ShippingMethodEvaluationType shippingMethodEvaluationType)
        {
            _selectedShippingMethodEvaluationType = shippingMethodEvaluationType;

            SynchronizeSelectedShippingMethodEvaluationTypeWithSelectedShippingMethod();
        }

        public void SynchronizeSelectedShippingMethodEvaluationTypeWithSelectedShippingMethod()
        {
            if (_selectedShippingMethodEvaluationType == ShippingMethodEvaluationType.Unknown)
            {
                _selectedShippingMethod = null;
                return;
            }

            _selectedShippingMethod = Maybe<int>.Empty();

            var shippingMethodEvaluationItem = ShippingMethodEvaluationItems
                .Where(i => i.HasChoiceList)
                .FirstOrDefault(i => i.ShippingMethodEvaluationType == _selectedShippingMethodEvaluationType);

            if (shippingMethodEvaluationItem != null)
            {
                if (shippingMethodEvaluationItem.Choices != null
                    && shippingMethodEvaluationItem.Choices.Any())
                {
                    if(shippingMethodEvaluationItem.SelectedShippingMethodId.HasValue
                        && shippingMethodEvaluationItem.Choices.Any(choice => choice.MainShippingMethodId == shippingMethodEvaluationItem.SelectedShippingMethodId.Value))
                    {
                        _selectedShippingMethod = shippingMethodEvaluationItem.SelectedShippingMethodId;
                    }
                    else
                    {
                        _selectedShippingMethod = shippingMethodEvaluationItem.Choices.FirstOrDefault().MainShippingMethodId;
                    }
                    shippingMethodEvaluationItem.SelectedShippingMethodId = _selectedShippingMethod.Value;    
                }
                else
                {
                    var shippingMethodResults = shippingMethodEvaluationItem.GetShippingMethodResults();

                    if (shippingMethodResults.Any())
                    {
                        var shippingMethodResult = shippingMethodResults.First();

                        if (shippingMethodResult.SelectedChoice != null
                            && shippingMethodResult.SelectedChoice.Choice != null
                            && shippingMethodResult.SelectedChoice.Choice.HasValue)
                        {
                            _selectedShippingMethod = shippingMethodResult.SelectedChoice.Choice.Value;
                        }
                    }
                }
            }
        }


        public ShippingMethodEvaluationType GetShippingMethodEvaluationTypeForShippingMethodId(int shippingMethodId)
        {
            foreach (var shippingMethodEvaluationItem in _shippingMethodEvaluationItems)
            {
                if (shippingMethodEvaluationItem.ContainsShippingMethodId(shippingMethodId))
                {
                    return shippingMethodEvaluationItem.ShippingMethodEvaluationType;
                }
            }

            throw new InvalidOperationException();
        }

        public void WithShippingMethodFor(Guid lineGroupIdentifier, int logisticType, int shippingMethodId)
        {
            if (!_lineGroupLogisticTypeShippingMethodSelection.ContainsKey(lineGroupIdentifier))
            {
                _lineGroupLogisticTypeShippingMethodSelection.Add(lineGroupIdentifier, new Dictionary<int, int>());
            }

            var dictionary = _lineGroupLogisticTypeShippingMethodSelection[lineGroupIdentifier];

            if (dictionary.ContainsKey(logisticType))
            {
                dictionary[logisticType] = shippingMethodId;
            }
            else
            {
                dictionary.Add(logisticType, shippingMethodId);
            }
        }

        public IEnumerable<int> GetShippingMethodFor(string lineGroupIdentifier)
        {
            return GetShippingMethodFor(Guid.Parse(lineGroupIdentifier));
        }

        public IEnumerable<int> GetShippingMethodFor(Guid lineGroupIdentifier)
        {
            if (!_lineGroupLogisticTypeShippingMethodSelection.ContainsKey(lineGroupIdentifier))
                return Enumerable.Empty<int>();

            return _lineGroupLogisticTypeShippingMethodSelection[lineGroupIdentifier].Values;
        }

        public int? GetShippingMethodFor(Guid lineGroupIdentifier, int logisticType)
        {
            if (!_lineGroupLogisticTypeShippingMethodSelection.ContainsKey(lineGroupIdentifier))
                return null;

            var dictionary = _lineGroupLogisticTypeShippingMethodSelection[lineGroupIdentifier];

            if (!dictionary.ContainsKey(logisticType))
                return null;

            return dictionary[logisticType];
        }

        public void SetSelectedShippingMethod(int id)
        {
            SelectedShippingMethod = id;
        }

        public void TransfertShippingMethodsFor(Guid lineGroupIdentifier, Guid toLineGroupIdentifier)
        {
            if (lineGroupIdentifier == toLineGroupIdentifier)
                return;

            if (!_lineGroupLogisticTypeShippingMethodSelection.ContainsKey(lineGroupIdentifier))
                return;

            var dictionary = _lineGroupLogisticTypeShippingMethodSelection[lineGroupIdentifier];

            _lineGroupLogisticTypeShippingMethodSelection.Remove(lineGroupIdentifier);

            if (_lineGroupLogisticTypeShippingMethodSelection.ContainsKey(toLineGroupIdentifier))
            {
                foreach (var pair in dictionary)
                {
                    if (!_lineGroupLogisticTypeShippingMethodSelection[toLineGroupIdentifier].ContainsKey(pair.Key))
                    {
                        _lineGroupLogisticTypeShippingMethodSelection[toLineGroupIdentifier].Add(pair);
                    }
                }
            }
            else
            {
                _lineGroupLogisticTypeShippingMethodSelection.Add(toLineGroupIdentifier, dictionary);
            }
        }

        public void ReadXml(XmlReader reader)
        {
            var selectedShippingMethodEvaluationType = reader["st"];

            if (selectedShippingMethodEvaluationType != null)
            {
                _selectedShippingMethodEvaluationType = (ShippingMethodEvaluationType)Enum.Parse(typeof(ShippingMethodEvaluationType), selectedShippingMethodEvaluationType);
            }

            var selectedShippingMethod = reader["m"];

            if (selectedShippingMethod != null)
            {
                SelectedShippingMethod = int.Parse(selectedShippingMethod);
            }

            reader.ReadToNextElementOrEndElement();

            while (reader.NodeType == XmlNodeType.Element && reader.Name == "i")
            {
                var type = reader["t"];

                if (type != null)
                {
                    var shippingMethodEvaluationType =
                        (ShippingMethodEvaluationType)Enum.Parse(typeof(ShippingMethodEvaluationType), type);

                    var dummyAddress = reader["d"];

                    var identity = reader["id"];

                    if (dummyAddress != null)
                    {
                        var isDummyAddress = bool.Parse(dummyAddress);

                        var reference = reader["r"];
                        var selectedShippingMethodIdAsString = reader["s"];
                        int? selectedShippingMethodId = null;
                        if (int.TryParse(selectedShippingMethodIdAsString, out int tempSelectedShippingMethodId))
                        {
                            selectedShippingMethodId = tempSelectedShippingMethodId;
                        }

                        if (reference != null)
                        {
                            switch (shippingMethodEvaluationType)
                            {
                                case ShippingMethodEvaluationType.Shop:
                                    _shippingMethodEvaluationItems.Add(
                                        new ShopShippingMethodEvaluationItem(new ShopAddress
                                        {
                                            Identity = int.Parse(reference)
                                        }, isDummyAddress)
                                        {
                                            SelectedShippingMethodId = selectedShippingMethodId
                                        });
                                    break;
                                case ShippingMethodEvaluationType.Postal:
                                    _shippingMethodEvaluationItems.Add(
                                        new PostalShippingMethodEvaluationItem(new PostalAddress
                                        {
                                            Reference = reference,
                                            Identity = int.Parse(identity ?? "0")
                                        }, isDummyAddress)
                                        {
                                            SelectedShippingMethodId = selectedShippingMethodId
                                        });
                                    break;
                                case ShippingMethodEvaluationType.Relay:
                                    _shippingMethodEvaluationItems.Add(
                                        new RelayShippingMethodEvaluationItem(new RelayAddress
                                        {
                                            Identity = int.Parse(reference)
                                        }, isDummyAddress)
                                        {
                                            SelectedShippingMethodId = selectedShippingMethodId
                                        });
                                    break;
                            }
                        }
                    }
                }

                reader.ReadToNextElementOrEndElement();
            }

            while (reader.NodeType == XmlNodeType.Element && reader.Name == "lg")
            {
                var lineGroup = reader["id"];

                if (lineGroup == null)
                    continue;


                if (!Guid.TryParse(lineGroup, out var lineGroupIdentifier))
                    continue;

                _lineGroupLogisticTypeShippingMethodSelection.Add(lineGroupIdentifier,
                    new Dictionary<int, int>());

                while (reader.NodeType != XmlNodeType.EndElement)
                {
                    if (reader.NodeType == XmlNodeType.Element && reader.Name == "lt")
                    {
                        var logisticType = int.Parse(reader["id"]);
                        var shippingMethodId = int.Parse(reader["sid"]);

                        _lineGroupLogisticTypeShippingMethodSelection[lineGroupIdentifier].Add(
                            logisticType, shippingMethodId);
                    }

                    reader.Read();
                }

                reader.ReadToNextElementOrEndElement();
            }
        }

        public void WriteXml(XmlWriter writer)
        {
            writer.WriteAttributeString("st", SelectedShippingMethodEvaluationType.ToString());

            if (SelectedShippingMethod.HasValue)
            {
                writer.WriteAttributeString("m", SelectedShippingMethod.Value.ToString(CultureInfo.InvariantCulture));
            }

            foreach (var shippingMethodEvaluationItem in _shippingMethodEvaluationItems)
            {
                writer.WriteStartElement("i");
                writer.WriteAttributeString("t", shippingMethodEvaluationItem.ShippingMethodEvaluationType.ToString());
                writer.WriteAttributeString("d", shippingMethodEvaluationItem.IsDummyAddress.ToString());
                writer.WriteAttributeString("s", shippingMethodEvaluationItem.SelectedShippingMethodId.ToString());

                switch (shippingMethodEvaluationItem.ShippingMethodEvaluationType)
                {
                    case ShippingMethodEvaluationType.Postal:
                        writer.WriteAttributeString("r", shippingMethodEvaluationItem.ShippingAddress.Reference);
                        if (shippingMethodEvaluationItem.ShippingAddress.Identity != null)
                        {
                            writer.WriteAttributeString("id", shippingMethodEvaluationItem.ShippingAddress.Identity.Value.ToString(CultureInfo.InvariantCulture));
                        }
                        break;
                    case ShippingMethodEvaluationType.Shop:
                        if (shippingMethodEvaluationItem.ShippingAddress.Identity != null)
                        {
                            writer.WriteAttributeString("r", shippingMethodEvaluationItem.ShippingAddress.Identity.Value.ToString(CultureInfo.InvariantCulture));
                        }
                        else
                        {
                            writer.WriteAttributeString("r", "-1".ToString(CultureInfo.InvariantCulture));
                        }
                        break;
                    case ShippingMethodEvaluationType.Relay:
                        if (shippingMethodEvaluationItem.ShippingAddress.Identity != null)
                        {
                            writer.WriteAttributeString("r", shippingMethodEvaluationItem.ShippingAddress.Identity.Value.ToString(CultureInfo.InvariantCulture));
                        }
                        break;
                }

                writer.WriteEndElement();
            }

            foreach (var lineGroup in _lineGroupLogisticTypeShippingMethodSelection)
            {
                writer.WriteStartElement("lg");

                writer.WriteAttributeString("id", lineGroup.Key.ToString());

                foreach (var logisticType in lineGroup.Value)
                {
                    writer.WriteStartElement("lt");

                    writer.WriteAttributeString("id", logisticType.Key.ToString(CultureInfo.InvariantCulture));
                    writer.WriteAttributeString("sid", logisticType.Value.ToString(CultureInfo.InvariantCulture));

                    writer.WriteEndElement();
                }

                writer.WriteEndElement();
            }
        }

        public void Reset(ShippingMethodResetFlag resetFlag = ShippingMethodResetFlag.All)
        {
            if (resetFlag.HasFlag(ShippingMethodResetFlag.ResetPostalItems))
            {
                (_shippingMethodEvaluationItems as List<ShippingMethodEvaluationItem>)
                    .RemoveAll(a => a.ShippingMethodEvaluationType == ShippingMethodEvaluationType.Postal);
            }
            if (resetFlag.HasFlag(ShippingMethodResetFlag.ResetShopItems))
            {
                (_shippingMethodEvaluationItems as List<ShippingMethodEvaluationItem>)
                    .RemoveAll(a => a.ShippingMethodEvaluationType == ShippingMethodEvaluationType.Shop);
            }
            if (resetFlag.HasFlag(ShippingMethodResetFlag.ResetRelayItems))
            {
                (_shippingMethodEvaluationItems as List<ShippingMethodEvaluationItem>)
                    .RemoveAll(a => a.ShippingMethodEvaluationType == ShippingMethodEvaluationType.Relay);
            }
            if (resetFlag.HasFlag(ShippingMethodResetFlag.ResetSelectedShippingMethod))
            {
                _selectedShippingMethod = null;
            }
            if (resetFlag.HasFlag(ShippingMethodResetFlag.ResetSelectedShippingMethodEvaluationType))
            {
                _selectedShippingMethodEvaluationType = ShippingMethodEvaluationType.Unknown;
            }
            if (resetFlag.HasFlag(ShippingMethodResetFlag.ResetLineGroupLogisticTypeShippingMethodSelection))
            {
                _lineGroupLogisticTypeShippingMethodSelection.Clear();
            }
        }

        public override string ToString()
        {
            var str = new StringBuilder();
            str.AppendLine($"selectedShippingMethod : {(_selectedShippingMethod.HasValue ? _selectedShippingMethod.Value.ToString() : null) }");
            str.AppendLine($"selectedShippingMethodEvaluationType : {_selectedShippingMethodEvaluationType}");
            return str.ToString();
        }

        public bool ShippingExcludedWrap()
        {
            var selectedChoice = SelectedShippingMethodEvaluation?.Choices?.FirstOrDefault(c => c.MainShippingMethodId == SelectedShippingMethodEvaluation?.SelectedShippingMethodId);
            var mainShippingMethodId = selectedChoice?.SelectedChoice?.MainShippingMethodId;
            var shippingMethodEnum = (ShippingMethodEnum?)mainShippingMethodId;

            switch (shippingMethodEnum)
            {
                case ShippingMethodEnum.Livraison3heuresChrono:
                case ShippingMethodEnum.Coursier1012:
                case ShippingMethodEnum.Coursier1618:
                case ShippingMethodEnum.Coursier1921:
                case ShippingMethodEnum.Coursier1921Bis:
                    return true;
                default:
                    return false;
            }

        }

        public ShippingChoice GetSelectedChoice()
        {
            return SelectedShippingMethodEvaluation?.GetSelectedChoice();
        }
    }
}
