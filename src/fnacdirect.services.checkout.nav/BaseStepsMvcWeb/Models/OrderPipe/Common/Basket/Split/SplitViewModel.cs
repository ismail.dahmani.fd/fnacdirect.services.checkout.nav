
using FnacDirect.Technical.Framework.Web.Mvc;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Basket
{
    public class SplitViewModel
    {
        public string Price { get; set; }

        public PriceDetailsViewModel PriceModel { get; set; }

        public string PriceNoVAT { get; set; }

        public PriceDetailsViewModel PriceNoVATModel { get; set; }

        public string PriceToDisplay { get; set; }

        public PriceDetailsViewModel PriceModelToDisplay { get; set; }

        public bool IsFree { get; set; }

        public bool IsAvailable { get; set; }

        public bool IsApplied { get; set; }
    }
}
