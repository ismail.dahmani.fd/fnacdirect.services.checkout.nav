using System;
using System.Collections.ObjectModel;

namespace FnacDirect.OrderPipe.Config
{
	public class ForwardList : KeyedCollection<string, Forward>
	{
		protected override string GetKeyForItem(Forward item)
		{
			if (item == null)
				throw new ArgumentNullException("item");

			return item.Name;
		}

		// new est utilis� car on ne veut pas du 'set'
		public new string this[string key]
		{
			get
			{
				if (Contains(key))
					return base[key].Step;
				return null;
			}
		}
	}
}