﻿using FnacDirect.OrderPipe.Base.Model;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Common.Payment.BillingMethodFactories
{
    public class FnacCardMethodsViewModelFactory : BasePaymentViewModelFactory<FnacCardBillingMethod>
    {
        public override BillingMethodViewModel Get(string tabName, PaymentMethod paymentMethod, OPContext opContext, PopOrderForm popOrderForm)
        {
            var viewModel = new FnacCardMethodsViewModel();

            foreach (var credit in popOrderForm.AvailableCredits)
            {
                viewModel.Credits.Add(new CreditViewModel()
                {
                    Id = credit.Id,
                    Label = credit.Label,
                    CreditType = credit.CreditType.ToString()
                });
            }

            viewModel.Display = true;

            return viewModel;
        }
    }
}
