﻿namespace FnacDirect.OrderPipe.Base.BaseModel.Model.OrderDetails
{
    public class OrderDetailShopAddress : OrderDetailBaseAddress
    {
        public OrderDetailShopAddress()
        {
            Type = "ShopAddress";
        }

        public string Name { get; set; }       
    }
}
