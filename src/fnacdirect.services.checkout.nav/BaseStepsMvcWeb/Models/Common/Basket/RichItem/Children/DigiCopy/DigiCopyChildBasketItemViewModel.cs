﻿namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Basket.Children
{
    public class DigiCopyChildBasketItemViewModel : ChildBasketItemViewModel
    {
        public bool IsDigiCopy
        {
            get
            {
                return true;
            }
        }

        public PriceViewModel Price { get; set; }
    }
}
