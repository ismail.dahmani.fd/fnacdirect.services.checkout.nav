using System.Xml.Serialization;

namespace FnacDirect.OrderPipe.Config
{
	public class Forward
	{
		[XmlAttribute("name")]
		public string Name { get; set; }
        
		[XmlAttribute("step")]
		public string Step { get; set; }
	}
}