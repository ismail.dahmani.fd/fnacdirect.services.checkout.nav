using FnacDirect.Technical.Framework.CoreServices;
using System;
using System.Collections.Generic;

namespace FnacDirect.OrderPipe.Base.Model
{
    [Serializable]
    public class CryptoPaymentRequirement : GroupData
    {
        public SerializableDictionary<int, bool> RequirementsByCardTypeId { get; set; } = new SerializableDictionary<int, bool>();
    }
}
