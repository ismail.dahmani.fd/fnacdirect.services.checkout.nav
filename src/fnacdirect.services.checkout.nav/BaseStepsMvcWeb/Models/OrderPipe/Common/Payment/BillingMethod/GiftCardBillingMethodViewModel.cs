using System.Collections.Generic;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Common.Payment
{
    public class GiftCardBillingMethodViewModel : BillingMethodViewModel
    {
        public decimal RemainingAmount { get; set; }

        public bool HasMaximumAllowedAmountForClickAndCollect { get; set; } = false;
        public decimal? MaximumAllowedAmountForClickAndCollect { get; set; }

        public bool AddAllowed
        {
            get
            {
                return RemainingAmount > 0;
            }
        }

        //Liste des cartes cadeaux
        public List<GiftCardTypeViewModel> GiftCardTypes { get; set; } = new List<GiftCardTypeViewModel>();

        public void AddGiftCardTypeViewModel(string label, string value, string mask)
        {
            GiftCardTypes.Add(new GiftCardTypeViewModel(label, value, mask));
        }        

        //Liste des cartes cadeaux deja utilisé.
        public List<GiftCardViewModel> GiftCardsUsed { get; set; }

    }
}
