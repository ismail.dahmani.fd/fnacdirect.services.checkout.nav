using System;
using System.Collections.Generic;
using System.Text;

namespace FnacDirect.Quote.Model
{
    public class QuoteBinaryPdf
    {
        private int _QuoteID = 0;
        public int QuoteID
        {
            get { return _QuoteID; }
            set { _QuoteID = value; }
        }

        private byte[] _BinaryPdf = null;
        public byte[] BinaryPdf
        {
            get { return _BinaryPdf; }
            set { _BinaryPdf = value; }
        }

        private DateTime _CreationDate = DateTime.Now;
        public DateTime CreationDate
        {
            get { return _CreationDate; }
            set { _CreationDate = value; }
        }

        private DateTime _QuoteCreationDate = DateTime.Now;
        public DateTime QuoteCreationDate
        {
            get { return _QuoteCreationDate; }
            set { _QuoteCreationDate = value; }
        }
    }
}
