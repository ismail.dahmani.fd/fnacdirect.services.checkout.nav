using System.Collections.Generic;
using FnacDirect.OrderPipe.Base.LineGroups;
using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.Technical.Framework.Velocity;

namespace FnacDirect.OrderPipe.Base.Business.LogisticLineGroups.Internals.Initializers
{
    public abstract class BaseLogisticLineGroupInitializer : ILogisticLineGroupInitializer
    {
        public ILogisticLineGroupInitializerHelper Helper { get; set; }
        public ILogisticLineGroupGenerationProcessContext Context { get; set; }

        public abstract IEnumerable<ILogisticLineGroup> Initialize(IDictionary<ILineGroupHeader, IEnumerable<Article>> selectedLineGroups);

        public virtual IEnumerable<ILogisticLineGroup> SimulateInitialize(IDictionary<ILineGroupHeader, IEnumerable<Article>> selectedLineGroups)
        {
            var output = new List<ILogisticLineGroup>();

            foreach (var lineGroup in selectedLineGroups.Keys)
            {
                output.Add(new LogisticLineGroup());
            }

            return output;
        }
    }
}
