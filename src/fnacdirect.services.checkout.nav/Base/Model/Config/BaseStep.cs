using System.Xml.Serialization;
using FnacDirect.OrderPipe.Config;

namespace FnacDirect.OrderPipe.Core.Model.Config
{
	public class BaseStep
	{
		[XmlAttribute("name")]
		public string Name { get; set; }

		[XmlAttribute("group")]
		public string Group { get; set; }

		[XmlElement("Parameter")]
		public ParameterList Parameters { get; set; }

		[XmlAttribute("class")]
		public string Class { get; set; }

		[XmlAttribute("shortcuts")]
		public string Shortcuts { get; set; }

		[XmlAttribute("switchid")]
		public string SwitchId { get; set; }

        [XmlAttribute("switchidnegate")]
        public string SwitchIdNegate { get; set; }

        [XmlAttribute("switchmode")]
		public string SwitchMode { get; set; }
	}
}
