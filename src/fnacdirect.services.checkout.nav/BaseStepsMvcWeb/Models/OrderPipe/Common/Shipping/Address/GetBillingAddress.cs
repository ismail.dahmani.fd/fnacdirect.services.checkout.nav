using System;
using System.Collections.Generic;
using FnacDirect.OrderPipe.Base.Business;
using FnacDirect.OrderPipe.Base.Model;
using System.Xml.Serialization;
using FnacDirect.Customer.Model;
using FnacDirect.OrderPipe.Base.Proxy.Customer;
using FnacDirect.Technical.Framework.CoreServices;

namespace FnacDirect.OrderPipe.Base.Steps
{
    public class BillingAddressData : GroupData
    {
        [XmlIgnore]
        public bool BillingAddressModified = false;

        public bool IsBillingAddressSetByUser { get; set; }

        public BillingAddressData()
        {
            IsBillingAddressSetByUserFromVerifyPurchase = false;
            IsBillingAddressSetByUser = false;
        }

        public bool IsBillingAddressSetByUserFromVerifyPurchase { get; set; }
    }


    public class PhoneValidData : GroupData
    {
        public PhoneValidData()
        {
            IsPhoneB2BValid = false;
            IsPhoneAmountValid = false;
            IsPhoneCheckBillingValid = false;
            IsPhoneForShippingMethodValid = false;
            IsPhoneValid = false;
            PhoneIsRequired = false;
            IsPhoneNotCurrentCountryValid = false;
        }

        public bool PhoneIsRequired { get; set; }

        public bool IsPhoneValid { get; set; }

        public bool IsPhoneNotCurrentCountryValid { get; set; }

        public bool IsPhoneForShippingMethodValid { get; set; }

        public bool IsPhoneCheckBillingValid { get; set; }

        public bool IsPhoneAmountValid { get; set; }

        public bool IsPhoneB2BValid { get; set; }
    }

    public class SocialInforamtionValidData : GroupData
    {
        public SocialInforamtionValidData()
        {
            IsCompanyNameIsRequired = false;
            SocialInformationRequired = false;
        }

        public bool SocialInformationRequired { get; set; }

        public bool IsCompanyNameIsRequired { get; set; }
    }

    /// <step>
    ///     <description>
    ///         Réhydrate l'adresse de facturation.
    ///         Utilise l'adresse de facturation par défaut au besoin si disponible.
    ///     </description>
    ///     <data>
    ///         <data mode="in/out" category="orderform" type="BaseOF" field="BillingAddress"/>
    ///         <data mode="in" category="orderform" type="BaseOF" field="UserInfo.UID" />
    ///     </data>    
    /// </step>
    [Obsolete]
    public class GetBillingAddress : Step<BaseOF, NullStepData, BillingAddressData>
    {
        private readonly IAddressBookService _addressBookService;
        private readonly IAddressOPBusiness _addressOPBusiness;

        public override IEnumerable<Type> DataType
        {
            get
            {
                yield return typeof(BillingAddressData);
                yield return typeof(PhoneValidData);
            }
        }

        public GetBillingAddress(IAddressBookService addressBookService, IAddressOPBusiness addressOPBusiness)
        {
            _addressBookService = addressBookService;
            _addressOPBusiness = addressOPBusiness;
        }

        protected BaseBusiness BaseBusiness = new BaseBusiness();

        protected override void PreCalc(OPContext opContext, InnerData innerData)
        {
            var orderForm = innerData.OrderForm;

            if (orderForm.BillingAddress == null)
            {
                try
                {
                    AddressEntity billingAddress = null;
                    if (orderForm.BasketType == BasketType.FromShoppingCart)
                    {
                        billingAddress = GetDefaultBillingAddress(orderForm.UserInfo.UID);
                    }
                    else if (orderForm.PreviouslineGroups != null && orderForm.PreviouslineGroups.Count > 0)
                    {
                        var currentLineGroup = orderForm.PreviouslineGroups[0];
                        if (currentLineGroup != null)
                        {
                            billingAddress = _addressOPBusiness.GetBillingAddressInCustomerAddressBook(currentLineGroup.BillingAddress, orderForm.UserInfo.UID, opContext.SiteContext.Culture);
                            innerData.GroupData.IsBillingAddressSetByUser = true;
                        }
                    }

                    if (billingAddress != null)
                    {
                        var billing = new BillingAddress(billingAddress)
                        {
                            IsDefault = true
                        };
                        orderForm.BillingAddress = billing;
                    }

                    // Mantis , pas de seconde verification sur la meme adresse utilisée
                    if (orderForm.CalcShippingAddress is PostalAddress)
                    {
                        var postalAddress = orderForm.CalcShippingAddress as PostalAddress;
                        if (postalAddress.AddressEntity != null && billingAddress.Id == postalAddress.AddressEntity.Id)
                        {
                            orderForm.GetPrivateData<QasBillingData>("QasBillingGroup").AddressStatus =
                                orderForm.GetPrivateData<QasShippingData>("QasShippingGroup").AddressStatus;
                        }
                    }
                }
                catch (Exception ex)
                {
                    Logging.Current.WriteInformation("Erreur de chargement de l'adresse de facturation par défaut pour l'UID " + orderForm.UserInfo.UID, ex);
                }
            }

            LoadBillingAddress(orderForm, opContext);

            if (orderForm.BillingAddress != null && orderForm.AllShippingAddresses.Count > 0 && orderForm.AllShippingAddresses[0] is PostalAddress)
            {
                if (orderForm.BillingAddress.AddressEntity != null)
                    if (!orderForm.BillingAddress.AddressEntity.IsDefaultBilling && !innerData.GroupData.IsBillingAddressSetByUser && !Parameters.Get("b2B.quote", false) && (orderForm.BillingAddress.Reference != "ADD"))
                    {
                        orderForm.BillingAddress = null;
                    }
            }

            if (orderForm.BillingAddress == null && orderForm.CalcShippingAddress is PostalAddress)
            {
                var postalAddress = orderForm.CalcShippingAddress as PostalAddress;
                if (postalAddress.AddressEntity != null)
                {
                    orderForm.BillingAddress = new BillingAddress(postalAddress.AddressEntity);

                    orderForm.GetPrivateData<QasBillingData>("QasBillingGroup").AddressStatus =
                                orderForm.GetPrivateData<QasShippingData>("QasShippingGroup").AddressStatus;
                }
            }

            // we set the VatNumber if we have loaded the curstomer Entity
            if (orderForm.BillingAddress != null && orderForm.UserInfo.CustomerEntity != null
                && !string.IsNullOrEmpty(orderForm.UserInfo.CustomerEntity.VatNumber)
                && orderForm.UserInfo.CustomerEntity.VatNumber != "0")
            {
                orderForm.BillingAddress.VATNumber = orderForm.UserInfo.CustomerEntity.VatNumber;
            }
        }

        public virtual AddressEntity GetDefaultBillingAddress(string accountReference)
        {
            return _addressBookService.GetDefaultBillingAddress();
        }

        protected virtual void LoadBillingAddress(BaseOF pOrderForm, OPContext pContext)
        {
            BaseBusiness.LoadBillingAddress(pOrderForm, pContext);
        }
    }
}
