﻿using System.Collections.Generic;
using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Common.Basket;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Basket.Children
{
    public class ServicesRichBasketItemChildrenViewModelBuilder : IRichBasketItemChildrenViewModelBuilder
    {
        private readonly IBasketItemViewModelBuilder _basketItemViewModelBuilder;
        private readonly IPriceViewModelBuilder _priceViewModelBuilder;

        public ServicesRichBasketItemChildrenViewModelBuilder(IBasketItemViewModelBuilder basketItemViewModelBuilder, IPriceViewModelBuilder priceViewModelBuilder)
        {
            _basketItemViewModelBuilder = basketItemViewModelBuilder;
            _priceViewModelBuilder = priceViewModelBuilder;
        }

        public IEnumerable<ChildBasketItemViewModel> Build(StandardArticle standardArticle, IEnumerable<Promotion> globalPromotions, bool applyAdherentPromotion)
        {
            foreach (var service in standardArticle.Services)
            {
                var viewModel = _basketItemViewModelBuilder.BuildItemStandardArticleWithPicture(service, globalPromotions);

                yield return new ServiceChildBasketItemViewModel
                {
                    ProductId = service.ProductID.GetValueOrDefault(0),
                    Image = viewModel.NormalImage,
                    Title = viewModel.Title,
                    TypeId = standardArticle.TypeId,
                    Price = _priceViewModelBuilder.Build(service,applyAdherentPromotion),
                    MasterProductId = standardArticle.ProductID.GetValueOrDefault(0),
                    CanDelete = true,
                    AvailabilityId = viewModel.AvailabilityId,
                    TotalOfferCount = viewModel.TotalOfferCount,
                    IsNumerical = viewModel.IsNumerical,
                    MoreInfoLink = service.MoreInfoLink
                };
            }
        }
    }
}
