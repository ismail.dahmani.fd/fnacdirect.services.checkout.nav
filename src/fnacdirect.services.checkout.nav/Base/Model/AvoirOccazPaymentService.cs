using FnacDirect.Membership.Model.Entities;
using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.OrderPipe.Base.Model.Facets;
using FnacDirect.Technical.Framework.CoreServices;
using FnacDirect.Technical.Framework.Web.Mvc;
using System;
using System.Linq;

namespace FnacDirect.OrderPipe.Base.Business.Payment
{
    public class AvoirOccazPaymentService : IAvoirOccazPaymentService
    {
        private readonly IUserExtendedContextProvider _userExtendedContextProvider;
        private readonly ICheckArticleService _checkArticleService;
        private readonly ISwitchProvider _switchProvider;
        private readonly IPaymentEligibilityService _paymentEligibilityService;

        public AvoirOccazPaymentService(IUserExtendedContextProvider userExtendedContextProvider,
            ICheckArticleService checkArticleService,
            ISwitchProvider switchProvider,
            IPaymentEligibilityService paymentEligibilityService)
        {
            _userExtendedContextProvider = userExtendedContextProvider;
            _checkArticleService = checkArticleService;
            _switchProvider = switchProvider;
            _paymentEligibilityService = paymentEligibilityService;
        }

        public bool IsAllowed(IGotLineGroups iGotLineGroups, int delayAfterExpiration, bool disableClickAndCollectCheck = false, bool disableEccvCheck = false)
        {
            var rules = _paymentEligibilityService.GetRules("orderpipe.pop.payment.avoiroccaz.eligibilityrules",
                disableClickAndCollectCheck: disableClickAndCollectCheck,
                disableEccvCheck: disableEccvCheck);

            if (!_paymentEligibilityService.IsAllowed(iGotLineGroups, rules))
            {
                return false;
            }

            var userExtendedContext = _userExtendedContextProvider.GetCurrentUserExtendedContext();
            var customer = userExtendedContext.Customer;

            var behavior = ConfigurationManager.Current.AppSettings.Settings["front.account.adherent.behavior"].Value;
            if (string.Compare(behavior, "siebel", StringComparison.OrdinalIgnoreCase) != 0)
            {
                return false;
            }

            if ((!customer.IsValidAdherent || customer.MembershipContract == null || (customer.MembershipContract != null && customer.MembershipContract.AdhesionStatus == AdhesionStatus.OutDated
                    && customer.MembershipContract.AdhesionEndDate.AddMonths(delayAfterExpiration) > DateTime.Now))
                && !customer.IsValidGamer)
            {
                return false;
            }

            var hasMarketPlaceArticle = iGotLineGroups.LineGroups.GetArticlesOfType<MarketPlaceArticle>().Any();
            if (hasMarketPlaceArticle
                 && _switchProvider.IsEnabled("orderpipe.pop.payment.filtersecondaryformarketplace"))
            {
                return false;
            }

            var fidelityPointsOccazBusiness = new FidelityBusiness<AvoirOccazBillingMethod>();
            var amount = fidelityPointsOccazBusiness.GetAvailableAmount(customer);
            if (amount <= 0)
            {
                return false;
            }

            if (_checkArticleService.HasDonation(iGotLineGroups.Articles))
            {
                return false;
            }

            if (_checkArticleService.HasSensibleSupportId(iGotLineGroups.Articles))
            {
                return false;
            }

            return true;
        }
    }
}
