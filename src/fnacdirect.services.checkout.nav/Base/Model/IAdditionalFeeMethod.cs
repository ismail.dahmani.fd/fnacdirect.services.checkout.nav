﻿namespace FnacDirect.OrderPipe.Base.Model
{
    public interface IAdditionalFeeMethod
    {
        decimal? CurrentPriceTheoEUR { get; set; }
        decimal? CurrentPriceDbEUR { get; set; }
        decimal? CurrentPriceEUR { get; set; }
        decimal? CurrentPriceNoVatEUR { get; set; }
        int? Id { get; set; }
        string Label { get; set; }
        decimal? PriceDetailDBEur { get; set; }
        decimal? PriceDetailEur { get; set; }
        decimal? PriceDetailNoVatEUR { get; set; }
        decimal? PriceGlobalDBEur { get; set; }
        decimal? PriceGlobalEur { get; set; }
        decimal? PriceGlobalNoVatEUR { get; set; }
        string Reference { get; set; }
        int? TypeLogistic { get; set; }
        string VATCode { get; set; }
        decimal? VATRate { get; set; }
    }
}
