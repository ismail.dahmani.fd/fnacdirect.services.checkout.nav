﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Basket
{
    public class DonationViewModel
    {
        public int ProductId { get; set; }

        public string PictureUrl { get; set; }

        public string Information { get; set; }
        public string Title { get; set; }
        public string Subtitle { get; set; }
    }
}
