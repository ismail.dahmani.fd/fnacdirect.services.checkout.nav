using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Basket;
using System;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.OrderPipe.Basket
{
    internal class BasketViewModelBuilderCallbacks : IRichBasketItemViewModelBuilderCallbacks
    {
        private readonly Func<StandardArticle, AvailabilityViewModel> _onComputingAvailability;
        private readonly Func<RichBasketItemViewModel, StandardArticle, RichBasketItemViewModel> _buildingStandardViewModel;
        private readonly Func<RichBasketItemViewModel, MarketPlaceArticle, RichBasketItemViewModel> _buildingMarketPlaceViewModel;

        public BasketViewModelBuilderCallbacks(Func<StandardArticle, AvailabilityViewModel> onComputingAvailability,
                                               Func<RichBasketItemViewModel, StandardArticle, RichBasketItemViewModel> buildingStandardViewModel,
                                               Func<RichBasketItemViewModel, MarketPlaceArticle, RichBasketItemViewModel> buildingMarketPlaceViewModel)
        {
            _onComputingAvailability = onComputingAvailability;
            _buildingStandardViewModel = buildingStandardViewModel;
            _buildingMarketPlaceViewModel = buildingMarketPlaceViewModel;
        }

        public RichBasketItemViewModel BuildingStandardViewModel(RichBasketItemViewModel current, StandardArticle standardArticle)
        {
            return _buildingStandardViewModel(current, standardArticle);
        }

        public RichBasketItemViewModel BuildingMarketPlaceViewModel(RichBasketItemViewModel current, MarketPlaceArticle marketPlaceArticle)
        {
            return _buildingMarketPlaceViewModel(current, marketPlaceArticle);
        }

        public AvailabilityViewModel OnComputingAvailability(StandardArticle standardArticle)
        {
            return _onComputingAvailability(standardArticle);
        }
    }
}
