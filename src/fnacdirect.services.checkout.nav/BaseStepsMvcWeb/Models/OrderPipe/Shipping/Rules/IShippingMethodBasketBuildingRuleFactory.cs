﻿using FnacDirect.OrderPipe.Config;
using FnacDirect.OrderPipe.Core.Services;

namespace FnacDirect.OrderPipe.Base.Business.Shipping.Rules
{
    public interface IShippingMethodBasketBuildingRuleFactory
    {
        IShippingMethodBasketBuildingRule Get<T>(IPipeParametersProvider pipeParametersProvider)
            where T : BaseShippingMethodBasketBuildingRule;
    }
}
