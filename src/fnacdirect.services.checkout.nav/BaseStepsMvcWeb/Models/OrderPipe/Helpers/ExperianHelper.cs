﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FnacDirect.OrderPipe.Base.Model;
using Newtonsoft.Json;

namespace FnacDirect.OrderPipe.Base.Business.Helpers
{
    public class ExperianHelper
    {
        public static void SetExperianData(ISwitchProvider switchProvider, OF orderForm, NameValueCollection headers, string pinf)
        {
            var deviceInfosEnabled = switchProvider.IsEnabled("orderpipe.pop.enabledeviceinfos");
            if (deviceInfosEnabled && headers != null)
            {
                var expr_data = orderForm.GetPrivateData<ExperianData>();

                var headerDict = new Dictionary<string, string>();
                foreach (string headerKey in headers)
                    headerDict.Add(headerKey, headers[headerKey]);

                expr_data.Headers = JsonConvert.SerializeObject(headerDict, Formatting.None);
                if (!string.IsNullOrEmpty(pinf))
                    expr_data.Payload = pinf;
            }
        }
    }
}

