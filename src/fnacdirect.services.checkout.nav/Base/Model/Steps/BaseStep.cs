using FnacDirect.OrderPipe.Config;
using System.Collections.Generic;

namespace FnacDirect.OrderPipe
{
    /// <summary>
    /// Implémentation de base d'une étape
    /// </summary>
    public abstract class BaseStep : IStep
    {
        private IOP _pipe;

        protected ISwitchProvider SwitchProvider { get; private set; }

        public string Name { get; set; }

        public string Group { get; set; }

        protected BaseStep()
        {
            Name = GetType().Name;
            Parameters = new ParameterList();
        }

        public virtual void Init(IOP pipe, ISwitchProvider switchProvider)
        {
            _pipe = pipe;

            Init(switchProvider);
        }

        public virtual void Init(ISwitchProvider switchProvider)
        {
            SwitchProvider = switchProvider;

            Init();
        }

        protected virtual void Init()
        {
        }

        public abstract bool Run(OPContext context, PipeResult res, bool force);


        /// <summary>
        /// Retourne la liste des paramètres de l'étape, tels qu'indiqués dans la configuration
        /// </summary>
        public ParameterList Parameters { get; set; }

        /// <summary>
        /// Retourne les paramètres globaux associés au Pipe
        /// </summary>
        public ParameterList GlobalParameters
        {
            get
            {
                return _pipe.GlobalParameters;
            }
        }

        /// <summary>
        /// Liste des raccourcis auxquels appartient l'étape
        /// </summary>
        public List<string> Shortcuts { get; } = new List<string>();
    }
}
