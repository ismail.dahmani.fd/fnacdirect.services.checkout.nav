using FnacDirect.Contracts.Online.Model;
using FnacDirect.OrderPipe.Base.Business.Shipping.Rules;
using FnacDirect.OrderPipe.Base.LineGroups;
using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.OrderPipe.Base.Model.Facets;
using FnacDirect.OrderPipe.Base.Model.LineGroups;
using FnacDirect.OrderPipe.Core.Services;
using FnacDirect.OrderPipe.Shipping.Model;
using System.Collections.Generic;
using System.Linq;
using ArticleType = FnacDirect.OrderPipe.Base.Model.ArticleType;

namespace FnacDirect.OrderPipe.Base.Business.Shipping
{
    public class ShippingMethodBasketService : IShippingMethodBasketService
    {
        private readonly List<int> _freeCountries;
        private readonly List<int> _freeCountriesException;
        private readonly string _freeOpeCode;

        private readonly IList<IShippingMethodBasketBuildingRule> _shippingMethodBasketBuildingRules;

        public ShippingMethodBasketService(IShippingMethodBasketBuildingRuleFactory shippingMethodBasketBuildingRuleFactory, IPipeParametersProvider pipeParametersProvider)
        {
            _freeCountries = StringUtils.ConvertParameterToList<int>(pipeParametersProvider.Get("shipping.free.countries", "")) ?? new List<int>();
            _freeCountriesException = StringUtils.ConvertParameterToList<int>(pipeParametersProvider.Get("shipping.free.ope.countries", "")) ?? new List<int>();
            _freeOpeCode = pipeParametersProvider.Get("shipping.free.ope", "EDITOFRAISPORT1207-EURO");

            _shippingMethodBasketBuildingRules = new List<IShippingMethodBasketBuildingRule>()
        {
                shippingMethodBasketBuildingRuleFactory.Get<HandDeliveryShippingMethodBasketBuildingRule>(pipeParametersProvider),
                shippingMethodBasketBuildingRuleFactory.Get<SteedFromStoreShippingMethodBasketBuildingRule>(pipeParametersProvider),
                shippingMethodBasketBuildingRuleFactory.Get<NextDayDeliveryShippingMethodBasketBuildingRule>(pipeParametersProvider)
            }.Where(i => i != null).ToList();
        }

        public ShippingMethodBasket GetShippingMethodBasketForOrderAndAddress(IGotShippingMethodEvaluation iContainsShippingMethodEvaluation, ILineGroup lineGroup, ShippingAddress shippingAddress)
        {
            return GetShippingMethodBasketForOrderAndAddress(iContainsShippingMethodEvaluation, lineGroup.Yield(), shippingAddress);
        }

        private bool ProcessFreeShipping(IEnumerable<ILineGroupHeader> lineGroups, IEnumerable<FreeShipping> freeShippings, ShippingAddress shippingAddress, int logType)
        {
            if (shippingAddress != null
                && freeShippings != null
                && shippingAddress.CountryId.HasValue)
            {
                foreach (var freeShipping in freeShippings)
                {
                    if ((freeShipping.Promotion.Code != _freeOpeCode ||
                         !_freeCountriesException.Contains(shippingAddress.CountryId.Value)) &&
                        !_freeCountries.Contains(shippingAddress.CountryId.Value))
                    {
                        continue;
                    }

                    foreach (var lineGroup in lineGroups)
                    {
                        foreach (var logisticType in lineGroup.LogisticTypes)
                        {
                            if (logisticType.Index == null || logisticType.Index.Value != logType)
                            {
                                continue;
                            }

                            if (logisticType.Index.Value != (int)freeShipping.LogisticType && (int)freeShipping.LogisticType != 0)
                            {
                                continue;
                            }

                            return true;
                        }
                    }
                }
            }

            return false;
        }

        /// <summary>
        /// Contrôle le réseau d'expédition pour le retrait en magasin.
        /// </summary>
        /// <param name="shippingMethodBasket">Panier, niveau "Shipping Business".</param>
        /// <param name="shippingAddress">Adresse de livraison.</param>
        /// <param name="lineGroupHeader">LineGroup en cours de traitement.</param>
        private static void CheckNetworkForWithdrawalInStore(ShippingMethodBasket shippingMethodBasket, ShippingAddress shippingAddress, ILineGroupHeader lineGroupHeader)
        {
            // Définition d'un ShippingNetwork SPÉCIAL.
            // Commande standard + Livraison magasin = [ShippingNetworkId] 4 
            if (shippingAddress.Type == 3 && lineGroupHeader.OrderType == OrderTypeEnum.FnacCom)
            {
                // Commande de type standard + Livraison en magasin 
                shippingAddress.CalcNetwork = 4;
            }
            // Commande de type retrait en magasin = [ShippingNetworkId] 3 
            else if (shippingAddress.Type == 3 && lineGroupHeader.OrderType == OrderTypeEnum.CollectInStore)
            {
                // Retrait en magasin (Click&Collect)
                shippingAddress.CalcNetwork = 3;
            }
            else
            {
                shippingAddress.CalcNetwork = null;
            }

            // On utilise un ShippingNetwork spécial s'il en existe un de défini
            shippingMethodBasket.ShippingNetwork = shippingAddress.CalcNetwork.HasValue ? shippingAddress.CalcNetwork.Value : shippingAddress.Type;
        }

        public ShippingMethodBasket GetShippingMethodBasketForOrderAndAddress(IGotShippingMethodEvaluation iContainsShippingMethodEvaluation, IEnumerable<ILineGroup> lineGroups, ShippingAddress shippingAddress)
        {
            return GetShippingMethodBasketForOrderAndAddress(iContainsShippingMethodEvaluation,
                                                             lineGroups.ToDictionary(l => l as ILineGroupHeader, l => l.Articles as IEnumerable<Model.Article>),
                                                             shippingAddress);
        }

        public ShippingMethodBasket GetShippingMethodBasketForOrderAndAddress(IGotShippingMethodEvaluation iContainsShippingMethodEvaluation, IDictionary<ILineGroupHeader, IEnumerable<Model.Article>> orderInput, ShippingAddress shippingAddress)
        {
            var shippingMethodEvaluation = iContainsShippingMethodEvaluation.ShippingMethodEvaluation;
            var orderLine = orderInput.FirstOrDefault().Key;
            var isPopLineGroup = orderLine is PopLineGroup;

            var shippingMethodBasket = new ShippingMethodBasket
            {
                Items = new ShippingMethodBasketItemList()
            };

            if (orderLine == null)
            {
                return shippingMethodBasket;
            }

            // Regroupement des quantités par type logistique
            foreach (var baseArticle in orderInput.SelectMany(lg => lg.Value).OfType<BaseArticle>())
            {
                if (baseArticle.LogType == null)
                    continue;

                var shippingMethodBasketItem = shippingMethodBasket.Items.GetOrCreate(baseArticle.LogType.Value);

                if ((orderLine.OrderType == OrderTypeEnum.FnacCom || orderLine.OrderType == OrderTypeEnum.Pro) && shippingMethodBasketItem.Quantity == 0)// todo: merge simo 24-06-2010 
                    shippingMethodBasketItem.ApplyDefaultFreeShipping = ProcessFreeShipping(orderInput.Keys, shippingMethodEvaluation.FreeShipping, shippingAddress, baseArticle.LogType.Value);// todo: merge simo 24-06-2010

                if (baseArticle.Quantity != null)
                    shippingMethodBasketItem.Quantity += baseArticle.Quantity.Value;

                if (baseArticle.PriceUserEur != null)
                {
                    shippingMethodBasketItem.TotalArticlePrice += baseArticle.Quantity.Value * baseArticle.PriceUserEur.Value;

                    if (baseArticle.PriceUserEur.Value > shippingMethodBasketItem.MaxArticlePrice)
                        shippingMethodBasketItem.MaxArticlePrice = baseArticle.PriceUserEur.Value;

                }

                if (baseArticle.AvailabilityId != null)
                    shippingMethodBasketItem.AvailabilityCodes.Add(baseArticle.AvailabilityId.Value);

                if (baseArticle.ID != null)
                    shippingMethodBasketItem.ArticleIds.Add(baseArticle.ID.Value);

                if (baseArticle.TypeId != null)
                    shippingMethodBasketItem.ArticleTypeCodes.Add(baseArticle.TypeId.Value);

                if (baseArticle.Format != null)
                    shippingMethodBasketItem.ArticleFormatCodes.Add(baseArticle.Format.Value);

                shippingMethodBasketItem.HasBundle |= baseArticle is Bundle;

                // Manage StandardArticle services cases
                ManageStandardArticleServices(baseArticle, shippingMethodBasket, shippingMethodBasketItem);
            }

            if (shippingAddress != null)
            {
                // Contrôle le réseau d'expédition pour le retrait en magasin.
                CheckNetworkForWithdrawalInStore(shippingMethodBasket, shippingAddress, orderLine);

                shippingMethodBasket.ShippingZone = shippingAddress.ShippingZone.GetValueOrDefault(0);
            }

            if (shippingAddress is PostalAddress)
            {
                var postalAddress = (PostalAddress)(shippingAddress);

                shippingMethodBasket.PostalCode = postalAddress.ZipCode;

                var list = new List<string>();

                if (!string.IsNullOrEmpty(postalAddress.AddressLine1))
                    list.Add(postalAddress.AddressLine1);

                if (!string.IsNullOrEmpty(postalAddress.AddressLine2))
                    list.Add(postalAddress.AddressLine2);

                if (!string.IsNullOrEmpty(postalAddress.AddressLine3))
                    list.Add(postalAddress.AddressLine3);

                if (!string.IsNullOrEmpty(postalAddress.AddressLine4))
                    list.Add(postalAddress.AddressLine4);

                shippingMethodBasket.Address = list.ToArray();

                shippingMethodBasket.City = postalAddress.City;
            }
            else if(shippingAddress is ShopAddress)
            {
                var shopAddress = shippingAddress as ShopAddress;
                shippingMethodBasket.PostalCode = shopAddress.ZipCode;
            }

            switch (orderLine.OrderType)
            {
                case OrderTypeEnum.Numerical:
                    shippingMethodBasket.BasketType = ShippingBasketType.Normal;
                    shippingMethodBasket.Algorithm = ShippingCalcAlgo.MaxBBySM;

                    shippingMethodBasket.SellerId = SellerIds.GetId(LineGroups.SellerType.Numerical);
                    break;
                case OrderTypeEnum.Pro:
                case OrderTypeEnum.FnacCom:
                    shippingMethodBasket.BasketType = ShippingBasketType.Normal;
                    shippingMethodBasket.Algorithm = ShippingCalcAlgo.MaxBBySM;
                    if (isPopLineGroup)
                    {
                        shippingMethodBasket.IsManuallySet = shippingMethodEvaluation.FnacCom.HasValue && shippingMethodEvaluation.FnacCom.Value.SelectedShippingMethod.GetValueOrDefault(0) != 0;
                    }
                    else
                    {
                        shippingMethodBasket.IsManuallySet = (orderLine as LineGroup).GlobalShippingMethodChoiceIsSetManually;
                    }
                    shippingMethodBasket.SellerId = null;
                    break;
                default:
                    shippingMethodBasket.BasketType = ShippingBasketType.MarketPlace;
                    shippingMethodBasket.Algorithm = ShippingCalcAlgo.MaxB;
                    shippingMethodBasket.SellerId = orderLine.Seller.SellerId;
                    break;
            }

            foreach (var rule in _shippingMethodBasketBuildingRules)
            {
                rule.GetModifier(iContainsShippingMethodEvaluation, orderInput.SelectMany(i => i.Value), shippingAddress)(shippingMethodBasket);
            }

            // Manage shipping promotion rules
            var articles = orderInput.SelectMany(i => i.Value);
            ManageShippingPromotionRules(articles, shippingMethodBasket);

            if (isPopLineGroup
                || shippingMethodEvaluation.ShippingMethodEvaluationGroups.Any())
            {
                var maybeShippingMethodEvaluationGroup = shippingMethodEvaluation.GetShippingMethodEvaluationGroup(orderLine.Seller.SellerId);

                if (maybeShippingMethodEvaluationGroup.HasValue)
                {
                    var shippingMethodEvaluationGroup = maybeShippingMethodEvaluationGroup.Value;

                    shippingMethodBasket.ChosenShippingMethod = shippingMethodEvaluationGroup.SelectedShippingMethod;
                }
            }
            else
            {
                shippingMethodBasket.ChosenShippingMethod = (orderLine as LineGroup).GlobalShippingMethodChoice;
            }

            return shippingMethodBasket;
        }

        private void ManageStandardArticleServices(BaseArticle baseArticle, ShippingMethodBasket shippingMethodBasket, ShippingMethodBasketItem shippingMethodBasketItem)
        {
            var standardArticle = baseArticle as StandardArticle;

            if (standardArticle == null || standardArticle.Type != ArticleType.Saleable)
                return;

            foreach (var service in standardArticle.Services)
            {
                if (service.LogType != null)

                    shippingMethodBasketItem = shippingMethodBasket.Items.GetOrCreate(service.LogType.Value);
                if (service.Quantity != null)
                    shippingMethodBasketItem.Quantity += service.Quantity.Value;

                if (service.PriceUserEur != null)
                {
                    shippingMethodBasketItem.TotalArticlePrice += service.Quantity.Value * service.PriceUserEur.Value;

                    if (service.PriceUserEur.Value > shippingMethodBasketItem.MaxArticlePrice)
                        shippingMethodBasketItem.MaxArticlePrice = service.PriceUserEur.Value;
                }

                if (service.ID != null)
                    shippingMethodBasketItem.ArticleIds.Add(service.ID.Value);

                if (service.TypeId != null)
                    shippingMethodBasketItem.ArticleTypeCodes.Add(service.TypeId.Value);

                if (service.Format != null)
                    shippingMethodBasketItem.ArticleFormatCodes.Add(service.Format.Value);
            }
        }

        private void ManageShippingPromotionRules(IEnumerable<Model.Article> articles, ShippingMethodBasket shippingMethodBasket)
        {
            var baseArticles = articles.OfType<BaseArticle>().Where(art => art.ShippingPromotionRules != null);
            if (baseArticles != null && baseArticles.Any())
            {
                foreach (var baseArticle in baseArticles)
                {
                    foreach(var shippingPromotionRule in baseArticle.ShippingPromotionRules.Where(x => x != null))
                    {
                        var shippingPromotionTrigger = new ShippingPromotionTrigger
                        {
                            TriggerKey = shippingPromotionRule.TriggerKey,
                            PromoCode = shippingPromotionRule.Promotion != null ? shippingPromotionRule.Promotion.Code : string.Empty,
                            AdditionnalRuleParam1 = shippingPromotionRule.Parameter1,
                            AdditionnalRuleParam2 = shippingPromotionRule.Parameter2,
                            ApplyToLogisticTypes = shippingPromotionRule.LogisticFamily ?? new List<int>(),
                            Quantity = baseArticle.Quantity.GetValueOrDefault()
                        };

                        var existingTrigger = shippingMethodBasket.PromotionTriggers.FirstOrDefault(x => x.Equals(shippingPromotionTrigger,fullComparison:true));

                        if (existingTrigger == null)
                        {
                            shippingMethodBasket.PromotionTriggers.Add(shippingPromotionTrigger);
                        }
                        else
                        {
                            existingTrigger.Quantity += baseArticle.Quantity.GetValueOrDefault();
                        }
                    }
                }
            }
        }
    }
}
