using System.Xml.Serialization;

namespace FnacDirect.OrderPipe.Base.Model.OrderInformations
{
    /// <summary>
    /// Contains context informations about pipe execution.
    /// All data are recomputed at each pipe processing, the whole structure SHOULD NOT be serialized.
    /// </summary>
    public class OrderContextInformations : IOrderContextInformations
    {
        [XmlIgnore]
        public string Affiliate { get; set; }
        
        [XmlIgnore]
        public string Catalogue { get; set; }

        [XmlIgnore]
        public string Culture { get; set; }

        [XmlIgnore]
        public string IPAddress { get; set; }

        [XmlIgnore]
        public bool IsTransfertToShoppingCartEnabled { get; set; }

        [XmlIgnore]
        public bool OrderInSession { get; set; }

        [XmlIgnore]
        public string Site { get; set; }

        [XmlIgnore]
        public string WebFarm { get; set; }

        [XmlIgnore]
        public int? MasterOrder { get; set; }

        [XmlIgnore]
        public bool IsOrderWithAuthentication { get; set; }

        [XmlIgnore]
        public string SenderEmail { get; set; }
    }
}
