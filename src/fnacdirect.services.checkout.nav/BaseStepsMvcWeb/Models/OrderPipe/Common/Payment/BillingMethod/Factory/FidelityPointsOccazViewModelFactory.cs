using FnacDirect.OrderPipe.Base.Business;
using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.Technical.Framework.Web;
using FnacDirect.Technical.Framework.Web.Mvc;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web.Mvc;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Common.Payment.BillingMethodFactories
{
    public class FidelityPointsOccazViewModelFactory : BasePaymentViewModelFactory<FidelityPointsOccazBillingMethod>
    {
        private readonly IApplicationContext _applicationContext;
        private readonly IPriceHelper _priceHelper;

        public FidelityPointsOccazViewModelFactory(IApplicationContext applicationContext)
        {
            _priceHelper = applicationContext.GetPriceHelper();
            _applicationContext = applicationContext;
        }

        public override BillingMethodViewModel Get(string tabName, PaymentMethod paymentMethod, OPContext opContext, PopOrderForm popOrderForm)
        {
            var viewModel = new FidelityPointsOccazViewModel()
            {
                HasUniqueExpiration = true
            };

            var fidelityPointsOccazBillingMethod = popOrderForm.OrderBillingMethods.OfType<FidelityPointsOccazBillingMethod>().FirstOrDefault();

            var fidelityPointsOccazBusiness = new FidelityBusiness<FidelityPointsOccazBillingMethod>();

            var maxUsableAmount = (int)fidelityPointsOccazBusiness.GetMaxUsableAmount(VisitorAdapter.Current.GetVisitor().Customer, popOrderForm, 10);

            var availableAmount = fidelityPointsOccazBusiness.GetAvailableAmount(VisitorAdapter.Current.GetVisitor().Customer);

            viewModel.Amount = _priceHelper.Format(availableAmount, PriceFormat.IntegerOnly);

            var firstExpiry = fidelityPointsOccazBusiness.GetFirstExpiry(VisitorAdapter.Current.GetVisitor().Customer);

            if (firstExpiry != null)
            {
                if (maxUsableAmount > (int)firstExpiry.Amount && firstExpiry.Amount > 0)
                {
                    viewModel.HasUniqueExpiration = false;
                    viewModel.AmountToExpire = _priceHelper.Format((int)firstExpiry.Amount, PriceFormat.IntegerOnly);
                }

                if (firstExpiry.ExpirationDate <= DateTime.Now.AddDays(30))
                {
                    viewModel.ExpireInLessThanOneMonth = true;
                }

                var culture = CultureInfo.GetCultureInfo(_applicationContext.GetLocale());

                viewModel.Expiration = firstExpiry.ExpirationDate.ToString(culture.DateTimeFormat.ShortDatePattern);
            }

            if (fidelityPointsOccazBillingMethod != null
                && fidelityPointsOccazBillingMethod.Amount.GetValueOrDefault(0) > 0)
            {
                viewModel.Active = true;
                viewModel.Used = _priceHelper.Format(fidelityPointsOccazBillingMethod.Amount.GetValueOrDefault(0), PriceFormat.IntegerOnly);

                var remaining = maxUsableAmount - fidelityPointsOccazBillingMethod.Amount.GetValueOrDefault(0);

                if (remaining > 0)
                {
                    viewModel.Remaining = _priceHelper.Format(remaining, PriceFormat.IntegerOnly);
                }
            }
            else
            {
                var values = new List<SelectListItem>();

                for (var i = 10; i <= maxUsableAmount; i += 10)
                {
                    values.Add(new SelectListItem
                    {
                        Text = _priceHelper.Format(i, PriceFormat.IntegerOnly),
                        Value = i.ToString(),
                        Selected = i == maxUsableAmount
                    });
                }

                viewModel.Values = values;
            }

            viewModel.Display = true;

            return viewModel;
        }
    }
}
