namespace FnacDirect.OrderPipe.Base.Model
{
    public class OgoneCreditCardBillingMethod : CreditCardBillingMethod
    {
        public new const int IdBillingMethod = 1048576;

        public override string BillingMethodType { get { return "Ogone"; } }

        public new static string BillingMethodName
        {
            get
            {
                return "OgoneCreditCardBillingMethod";
            }
        }
        public static bool IsAliasGatewayProcess(string processName)
        {
            return processName == "AliasGateway";
        }

        public string Brand { get; set; }

        public string Pm { get; set; }

        public string OgoneAlias { get; set; }

        public string VirtualCard { get; set; }

        public string DebitType { get; set; }

        public bool AutorisationReceived { get; set; }

        public bool SavingCardAutorisation { get; set; }
        public bool SavingCardOnTacitAgreement { get; set; }
        public bool SavingCardOnCheck { get; set; }

        public string OgoneTransactionProcess { get; set; }

        public override bool UseTransaction
        {
            get
            {
                return true;
            }
        }
        public OgoneCreditCardBillingMethod()
        {
            BillingMethodId = IdBillingMethod;
            Name = BillingMethodName;
            UseMonetique = false;
        }

        public int? CGVVersion { get; set; }
    }
}
