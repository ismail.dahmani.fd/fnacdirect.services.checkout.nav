using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.OrderPipe.BaseMvc.Web.Controllers.Pop;
using System.Web.Mvc;
using System.Linq;
using FnacDirect.OrderPipe.BaseMvc.Web.Pop.Models.Quote;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Pop.Controllers.Quote
{
    public class QuoteErrorController : PopBaseController
    {
        private readonly OPContext _opContext;
        public QuoteErrorController(OPContext opContext)
        {
            _opContext = opContext;
        }

        [HttpGet]
        public ActionResult Index()
        {
            var popOrderForm = _opContext.OF as PopOrderForm;
            var errorMessage = popOrderForm.PipeMessages.Category("Quote.Message").FirstOrDefault();
            var quoteErrorViewModel = new QuoteErrorViewModel
            {
                QuoteError = errorMessage?.RessourceId
            };
            
            return View(quoteErrorViewModel);
        }
    }
}
