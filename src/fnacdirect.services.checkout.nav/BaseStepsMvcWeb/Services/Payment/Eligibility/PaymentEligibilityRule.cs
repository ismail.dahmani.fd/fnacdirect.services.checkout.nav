using System;

namespace FnacDirect.OrderPipe.Base.Business.Payment
{
    [Flags]
    public enum PaymentEligibilityRule : int
    {
        NONE                    = 0,
        ALL                     = ~0,

        ECCV_Forbidden                              = 1,
        ClickAndCollect_ArticleExclusion            = 2,
        ClickAndCollect_Forbidden                   = 4,
        Numerical_Forbidden                         = 8
    }


    public static class PaymentEligibilityRuleExtension
    {
        public static PaymentEligibilityRule Add(this PaymentEligibilityRule rules, params PaymentEligibilityRule[] rulesToAdd)
        {
            foreach(var ruleToAdd in rulesToAdd)
            {
                rules |= ruleToAdd;
            }
            return rules;
        }

        public static PaymentEligibilityRule Remove(this PaymentEligibilityRule rules, params PaymentEligibilityRule[] rulesToRemove)
        {
            foreach (var ruleToRemove in rulesToRemove)
            {
                rules &= ~ruleToRemove;
            }
            return rules;
        }
    }
}
