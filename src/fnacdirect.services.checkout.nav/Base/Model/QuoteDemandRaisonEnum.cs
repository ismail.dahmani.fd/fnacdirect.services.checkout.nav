using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FnacDirect.OrderPipe.Base
{
    public enum QuoteDemandRaisonEnum
    {
        OrderNewCustomer = 0,
        OrderVolume = 1,
        OrderIndisponibility = 2,
        OrderMax = 3,
        CallCenter = 4,
        NewQuote = 5
    }
}
