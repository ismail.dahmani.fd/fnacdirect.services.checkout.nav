using FnacDirect.Relay.Model;
using System.Collections.Generic;

namespace FnacDirect.OrderPipe.Base.Proxy.Customer
{
    public interface ICustomerRelayService
    {
        void AttachRelay(int relayId);
        RelayAddressEntity GetDefaultRelay();
        ShippingAddressType GetDefaultShippingAddressType();
        IEnumerable<RelayAddressEntity> GetRelayAddresses();
        void SetDefaultRelay(int relayId);
    }
}
