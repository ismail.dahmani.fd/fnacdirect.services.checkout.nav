using FnacDirect.OrderPipe.Base.Model;
using Article = FnacDirect.OrderPipe.Base.Model.Article;

namespace FnacDirect.OrderPipe.Base.Business.ArticleConversions.ShoppingCartToOrderForm
{
    public partial class ShoppingCartToOrderFormArticleConverter : IShoppingCartToOrderFormConverter
    {
        protected readonly IArticleDetailService _articleDetailService;
        
        public ShoppingCartToOrderFormArticleConverter(IArticleDetailService articleDetailService)
        {
            _articleDetailService = articleDetailService;
        }

        public Article Convert(ShoppingCartEntities.Article @from)
        {
            return Convert(@from, false);
        }

        public virtual Article Convert(ShoppingCartEntities.Article a, bool withStockCheck)
        {
            if (a is ShoppingCartEntities.Service)
                return Convert((ShoppingCartEntities.Service)a);

            if (a is ShoppingCartEntities.StandardArticle)
            {
                if (a is ShoppingCartEntities.Bundle)
                   return Convert((ShoppingCartEntities.Bundle)a);           

                return Convert((ShoppingCartEntities.StandardArticle)a);
            }

            if (a is ShoppingCartEntities.MarketPlaceArticle)
                return Convert((ShoppingCartEntities.MarketPlaceArticle)a);

            if (a is ShoppingCartEntities.VirtualGiftCheck)
                return Convert((ShoppingCartEntities.VirtualGiftCheck)a);

            return null;
        }

        protected Bundle Convert(ShoppingCartEntities.Bundle a)
        {
            var bundle = Convert<Bundle>(a);

            bundle.BundleComponents = ConvertArticles(a.BundleComponents);

            if (bundle.BundleComponents.Count == 0) 
                bundle.BundleComponents = null;

            return bundle;
        }        

        public ArticleList ConvertArticles(ShoppingCartEntities.Articles list, bool pWithStockCheck = false, string cartId = null)
        {
            var ret = new ArticleList();

            foreach (ShoppingCartEntities.Article art in list)
            {
                var art2 = Convert(art, pWithStockCheck);

                if (!string.IsNullOrEmpty(cartId))
                    art2.ShoppingCartOriginId = cartId;

                if (art2 != null) 
                    ret.Add(art2);
            }
            return ret;
        }
    }
}
