using System.Collections.Generic;
using System.Linq;
using FnacDirect.OrderPipe.Base.Business.LogisticLineGroups.Internals.InitialSelectors;
using FnacDirect.OrderPipe.Base.LineGroups;
using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.OrderPipe.Base.Model.LineGroups;

namespace FnacDirect.OrderPipe.Base.Business.LogisticLineGroups.InitialSelectors
{
    public class FnacAdhesionLogisticLineGroupInitialSelector : BaseLineGroupInitialSelector
    {
        public override IDictionary<ILineGroupHeader, IEnumerable<Article>> Select(IEnumerable<ILineGroup> lineGroups)
        {
            var output = new Dictionary<ILineGroupHeader, IEnumerable<Article>>();

            foreach (var lineGroup in lineGroups.Where(l => l.Seller.Type == SellerType.Fnac))
            {
                var articles = lineGroup.Articles.OfType<StandardArticle>().Where(a => a.IsAdhesionCard);

                if (articles.Any())
                {
                    output.Add(lineGroup, articles);
                }
            }

            return output;
        }
    }
}
