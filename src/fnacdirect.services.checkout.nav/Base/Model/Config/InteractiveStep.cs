using System;
using System.Xml.Serialization;
using FnacDirect.OrderPipe.Core.Model.Config;

namespace FnacDirect.OrderPipe.Config
{
	public class InteractiveStep : Core.Model.Config.Step
    {
		[XmlAttribute("security")]
		public SecurityMode SecurityMode { get; set; }

		[XmlAttribute("directaccess")]
		public bool DirectAccess { get; set; }

        [XmlAttribute("usedirectaccess")]
        public bool UseDirectAccess { get; set; }

		/// <summary>
		/// Dans le cas ou l'on authorise pas un accès directe à l'étape, cette propriété
		/// permet de déterminer si l'étape en cours dans le chemin de fer est l'étape précédante
		/// ou suivante. Les valeurs authorisées sont "previous" et "next". Si l'attribut n'est pas
		/// renseigné c'est la valeur "next" qui est utilisée par défaut.
		/// </summary>
		[XmlAttribute("nodirectaccessbehavior")]
		public string NoDirectAccessBehavior { get; set; }

		[XmlAttribute("pass2shortcut")]
		public string Pass2Shortcut { get; set; }

		public UIDescriptor UIDescriptor { get; set; }
	}
}
