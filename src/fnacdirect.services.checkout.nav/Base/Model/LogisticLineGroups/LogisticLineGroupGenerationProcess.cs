using FnacDirect.OrderPipe.Base.Business.LogisticLineGroups.Handlers;
using FnacDirect.OrderPipe.Base.Business.LogisticLineGroups.Internals.Handlers;
using FnacDirect.OrderPipe.Base.Business.LogisticLineGroups.Internals.Rules;
using FnacDirect.OrderPipe.Base.Business.LogisticLineGroups.Rules;
using FnacDirect.OrderPipe.Base.LineGroups;
using FnacDirect.OrderPipe.Base.Model.Facets;
using System.Collections.Generic;
using System.Linq;

namespace FnacDirect.OrderPipe.Base.Business.LogisticLineGroups
{
    public class LogisticLineGroupGenerationProcess : ILogisticLineGroupGenerationProcess
    {
        private readonly IEnumerable<ILogisticLineGroupGenerationRuleDescriptor> _logisticLineGroupGenerationRuleDescriptors;
        private readonly ILogisticLineGroupGenerationRuleFactory _logisticLineGroupGenerationRuleFactory;
        private readonly ILogisticLineGroupProcessCompletedHandlerFactory _logisticLineGroupProcessCompletedHandlerFactory;

        public LogisticLineGroupGenerationProcess(ILogisticLineGroupGenerationRuleFactory logisticLineGroupGenerationRuleFactory, ILogisticLineGroupProcessCompletedHandlerFactory logisticLineGroupProcessCompletedHandlerFactory)
        {
            _logisticLineGroupGenerationRuleFactory = logisticLineGroupGenerationRuleFactory;
            _logisticLineGroupProcessCompletedHandlerFactory = logisticLineGroupProcessCompletedHandlerFactory;

            _logisticLineGroupGenerationRuleDescriptors = new List<ILogisticLineGroupGenerationRuleDescriptor>()
            {
                new SelfCheckoutLogisticLineGroupGenerationRuleDescriptor(),
                new SteedFromStoreLogisticLineGroupGenerationRuleDescriptor(),
                new PreOrderSupplySourceLogisticLineGroupGenerationRuleDescriptor(),
                new SupplySourceLogisticLineGroupGenerationRuleDescriptor(),
                new ClickAndCollectLogisticLineGroupGenerationRuleDescriptor(),
                new FullStockFnacAdhesionLineGroupGenerationRuleDescriptor(),
                new ECCVLogisticLineGroupGenerationRuleDescriptor(),
                new FnacAdhesionLineGroupGenerationRuleDescriptor(),
                new MarketPlaceLogisticLineGroupGenerationRuleDescriptor(),
                new NumericalLogisticLineGroupGenerationRuleDescriptor(),
                new DematSoftLogisticLineGroupGenerationRuleDescriptor(),
                new VGCLogisticLineGroupGenerationRuleDescriptor(),
                new FnacB2BLogisticLineGroupGenerationRuleDescriptor(),
                new WSOrderProcessLogisticLineGroupGenerationRuleDescriptor(),
                new FnacLogisticLineGroupGenerationRuleDescriptor(),
                new IntramagPTLogisticLineGroupGenerationRuleDescriptor(),
                new IntramagPELogisticLineGroupGenerationRuleDescriptor(),
                new ShopSaleLogisticLineGroupGenerationRuleDescriptor(),
                new ClickAndMagLogisticLineGroupGenerationRuleDescriptor()                
            };
        }

        public IEnumerable<ILogisticLineGroup> Apply(MultiFacet<IGotLineGroups, IGotBillingInformations, IGotShippingMethodEvaluation> multiFacet)
        {
            var logisticLineGroups = new List<ILogisticLineGroup>();

            using (var logisticLineGroupGenerationProcessContext = new LogisticLineGroupGenerationProcessContext(multiFacet))
            {
                logisticLineGroupGenerationProcessContext.AddProcessCompletedHandler(new CompletedHandlerDescriptor<CalculateSubTotalsLogisticLineGroupHandler> { Priority = 3 });
                logisticLineGroupGenerationProcessContext.AddProcessCompletedHandler(new CompletedHandlerDescriptor<CalculateTotalsProcessCompletedHandler> { Priority = 2 });
                logisticLineGroupGenerationProcessContext.AddProcessCompletedHandler(new CompletedHandlerDescriptor<SplitBillingMethodByLineGroupProcessCompletedHandler> { Priority = 1 });

                foreach (var logisticLineGroupGenerationRuleDescriptor in _logisticLineGroupGenerationRuleDescriptors)
                {
                    var logisticLineGroupGenerationRule = _logisticLineGroupGenerationRuleFactory.Get(logisticLineGroupGenerationRuleDescriptor);

                    logisticLineGroups.AddRange(logisticLineGroupGenerationRule.Apply(logisticLineGroupGenerationProcessContext));
                }

                foreach (var type in logisticLineGroupGenerationProcessContext.ProcessCompletedHandlers.OrderByDescending(h => h.Priority))
                {
                    var processCompletedHandler = _logisticLineGroupProcessCompletedHandlerFactory.Get(type);

                    processCompletedHandler.OnGenerated(logisticLineGroupGenerationProcessContext, logisticLineGroups);
                }
            }

            if (!logisticLineGroups.Any())
            {
                throw BaseBusinessException
                    .BuildException("Process cannot conclude without generating logistic line group. Missing switches or class to handle specific cases ?", null, BusinessRange.Purchase, 0)
                    .WithData("pop", multiFacet.ToString());
            }

            return logisticLineGroups;
        }

        public IEnumerable<ILogisticLineGroup> SimulateInitialize(MultiFacet<IGotLineGroups, IGotBillingInformations, IGotShippingMethodEvaluation> multiFacet)
        {
            var output = new List<ILogisticLineGroup>();

            using (var logisticLineGroupGenerationProcessContext = new LogisticLineGroupGenerationProcessContext(multiFacet))
            {
                foreach (var logisticLineGroupGenerationRuleDescriptor in _logisticLineGroupGenerationRuleDescriptors)
                {
                    var logisticLineGroupGenerationRule = _logisticLineGroupGenerationRuleFactory.Get(logisticLineGroupGenerationRuleDescriptor);

                    output.AddRange(logisticLineGroupGenerationRule.SimulateInitialize(logisticLineGroupGenerationProcessContext));
                }
            }

            return output;
        }
    }
}
