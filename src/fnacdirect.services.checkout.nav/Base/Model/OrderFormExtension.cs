using FnacDirect.Contracts.Online.IModel;
using FnacDirect.Contracts.Online.Model;
using FnacDirect.ShoppingCartEntities;
using FnacDirect.Technical.Framework.ServiceLocation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FnacDirect.OrderPipe.Base.Business.ShoppingCart
{
    public static class OrderFormExtension
    {
        public static Orderform Fusion(Orderform of1, Orderform of2, FusionType fusion, FnacDirect.Contracts.Online.Model.SiteContext siteContext)
        {
            if (of1 == null && of2 == null)
                return new Orderform();
            if (of1 == null && of2 != null)
                return of2;
            if (of1 != null && of2 == null)
                return of1;

            // Par défaut, on prend comme origine le premier Orderform niveau données
            // On ne fusionne que les articles
            var of = new Orderform();
            of.Articles = new Articles();
            var articleBusiness = ServiceLocator.Current.GetInstance<IArticleBusiness3>(); 

            Action<Orderform, Orderform> alignShippingAddresses = (source, destination) =>
            {
                foreach (ShippingAddress shippingAddressOf1 in source.ShippingAddresses)
                {
                    var hasMatch = false;

                    foreach (ShippingAddress shippingAddressOf2 in destination.ShippingAddresses)
                    {
                        if (shippingAddressOf1.Identity.GetValueOrDefault() == shippingAddressOf2.Identity.GetValueOrDefault())
                        {
                            hasMatch = true;
                        }
                    }

                    if (!hasMatch)
                    {
                        destination.ShippingAddresses.Add(shippingAddressOf1);
                    }
                }
            };

            alignShippingAddresses(of1, of2);
            alignShippingAddresses(of2, of1);
            alignShippingAddresses(of1, of);

            var anyFusionDone = false;

            foreach (ShoppingCartEntities.Article art in of1.Articles)
            {
                if (art.ProductID.HasValue)
                {
                    var article = articleBusiness.GetArticle(siteContext, ArticleReference.FromPrid(art.ProductID.Value));

                    if (article != null)
                    {
                        art.TypeId = article.Core.Type.ID;
                    }
                }

                if (!ShouldBeFusioned(art, fusion))
                {
                    // Pour une fusion mobile, on zappe les articles différents de Standard ou MP
                    continue;
                }

                anyFusionDone = true;

                if (fusion == FusionType.Mobile && (art is StandardArticle || art is MarketPlaceArticle))
                    of.Articles.Add(art);
                else if (fusion == FusionType.Default)
                    of.Articles.Add(art);
            }

            if (of2.Articles == null || of2.Articles.Count == 0)
            {
                if (anyFusionDone)
                    return of1;
                else
                    return of;
            }

            foreach (ShoppingCartEntities.Article _art2 in of2.Articles)
            {
                if (_art2.ProductID.HasValue)
                {
                    var article = articleBusiness.GetArticle(siteContext, ArticleReference.FromPrid(_art2.ProductID.Value));

                    if (article != null)
                    {
                        _art2.TypeId = article.Core.Type.ID;
                    }
                }

                if (!ShouldBeFusioned(_art2, fusion))
                {
                    // Pour une fusion mobile, on zappe les articles différents de Standard ou MP
                    continue;
                }
                bool copyFound = false;
                for (int i = of.Articles.Count - 1; i > -1; i--)
                {
                    var _art1 = of.Articles[i];
                    if (AreArticlesEqual(_art1, _art2))
                    {
                        copyFound = true;
                        if (_art1.Quantity < _art2.Quantity)
                        {
                            of.Articles.RemoveAt(i);
                            of.Articles.Add(_art2);
                        }
                    }
                }
                if (!copyFound)
                {
                    of.Articles.Add(_art2);
                }
            }

            return of;
        }

        private static bool AreArticlesEqual(ShoppingCartEntities.Article x, ShoppingCartEntities.Article y)
        {
            if (x is MarketPlaceArticle && y is MarketPlaceArticle)
            {
                var mpX = x as MarketPlaceArticle;
                var mpY = y as MarketPlaceArticle;
                if (mpX.OfferId == mpY.OfferId && (mpX.OfferReference ?? string.Empty).Equals(mpY.OfferReference, StringComparison.InvariantCultureIgnoreCase))
                    return true;
                else
                    return false;
            }
            else if (x is ShoppingCartEntities.VirtualGiftCheck && y is ShoppingCartEntities.VirtualGiftCheck)
            {
                var vgcX = x as ShoppingCartEntities.VirtualGiftCheck;
                var vgcY = y as ShoppingCartEntities.VirtualGiftCheck;
                return vgcX.Amount == vgcY.Amount;
            }
            else
            {
                return x.ProductID == y.ProductID && x.GetType() == y.GetType();
            }
        }

        private static bool ShouldBeFusioned(ShoppingCartEntities.Article article, FusionType fusionType)
        {
            if (fusionType == FusionType.Default)
            {
                return true;
            }

            if (!(article is StandardArticle || article is MarketPlaceArticle))
            {
                return false;
            }

            var standardArticle = article as StandardArticle;

            if (standardArticle == null)
            {
                return true;
            }

            return true;
        }
    }
}
