using FnacDirect.Customer.Model;
using FnacDirect.OrderPipe.Base.Business;
using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.OrderPipe.Base.Proxy.Customer;
using FnacDirect.OrderPipe.BaseMvc.Web.Services.Pop.Payment;
using FnacDirect.Technical.Framework.Web.Mvc;
using System.Linq;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Common.Payment
{
    public class CustomerCreditCardBaseViewModelBuilder : ICustomerCreditCardBaseViewModelBuilder
    {
        private readonly ICustomerCreditCardService _customerCreditCardService;
        private readonly ICreditCardDescriptorBusiness _creditCardDescriptorBusiness;
        private readonly IApplicationContext _applicationContext;
        private readonly IPaymentMethodService _paymentMethodService;
        protected const string CreditCardPictoPattern = "decos/pipe/picto-carte-{0}.png";

        public CustomerCreditCardBaseViewModelBuilder(ICustomerCreditCardService customerCreditCardService,
                                                      ICreditCardDescriptorBusiness creditCardDescriptorBusiness,
                                                      IApplicationContext applicationContext,
                                                      IPaymentMethodService paymentMethodService)
        {
            _customerCreditCardService = customerCreditCardService;
            _creditCardDescriptorBusiness = creditCardDescriptorBusiness;
            _applicationContext = applicationContext;
            _paymentMethodService = paymentMethodService;
        }

        public CustomerCreditCardBaseViewModel Build(string userReference)
        {
            var customerCreditCards = _customerCreditCardService.GetCustomerCreditCards(userReference, CreditCardPaymentMethodType.Ogone)
                                                               .Where(cc => cc.TypeId != 11 && ((cc.CGVVersion.HasValue && cc.CGVVersion.Value == 1) || !cc.CGVVersion.HasValue)
                                                                                            && (cc.CBUsage & CBUsageFlags.SelfCheckout) != 0)
                                                               .OrderByDescending(c => c.CreateDate);

            var customerCreditCard = customerCreditCards.FirstOrDefault();

            var customerCreditCardBaseViewModel = new CustomerCreditCardBaseViewModel();

            var config = _applicationContext.GetAppSetting("config.shippingMethodSelection");

            if (customerCreditCard != null)
            {
                var creditCardDescriptor = _creditCardDescriptorBusiness.GetCreditCards(config, _applicationContext.GetSiteContext().Culture)
                                                                        .Find(x => x.Id == customerCreditCard.TypeId);
                if (creditCardDescriptor != null)
                {
                    customerCreditCardBaseViewModel.Brand = customerCreditCard.Brand;
                    customerCreditCardBaseViewModel.ExpirationDateFormat = $"{customerCreditCard.ExpireMonth}/{customerCreditCard.ExpireYear.ToString().Substring(2)}";
                    customerCreditCardBaseViewModel.Holder = customerCreditCard.Holder;
                    customerCreditCardBaseViewModel.TypeId = customerCreditCard.TypeId;
                    customerCreditCardBaseViewModel.Url = _applicationContext.GetImagePath() + string.Format(CreditCardPictoPattern, customerCreditCard.TypeId);
                    customerCreditCardBaseViewModel.MaskedCardNumber = _creditCardDescriptorBusiness.ApplyMask(creditCardDescriptor, "-", customerCreditCard.CardNumber);
                }
            }

            return customerCreditCardBaseViewModel;
        }

        public CustomerCreditCardViewModel BuildSelfCheckoutCreditCardViewModel(PopOrderForm popOrderForm, string userReference)
        {
            var customerCreditCard = _customerCreditCardService.GetCustomerSelfCheckoutCreditCard(userReference);
            var realAmount = popOrderForm.PrivateData.GetByType<PaymentSelectionData>().RemainingAmount * 100;

            var customerCreditCardViewModel = new CustomerCreditCardViewModel();

            var config = _applicationContext.GetAppSetting("config.shippingMethodSelection");

            if (customerCreditCard != null)
            {
                var creditCardDescriptor = _creditCardDescriptorBusiness.GetCreditCards(config, _applicationContext.GetSiteContext().Culture)
                                                                        .Find(x => x.Id == customerCreditCard.TypeId);

                var flag3DsEnable = _paymentMethodService.Is3DSecureEnable(customerCreditCard.TypeId, popOrderForm, realAmount);
                var d3dEnable = _paymentMethodService.IsDirectLink3DSecureEnable(customerCreditCard.TypeId, flag3DsEnable);

                if (creditCardDescriptor != null)
                {
                    customerCreditCardViewModel.Reference = customerCreditCard.Reference;
                    // Numéro de carte masqué par les "XXXX"
                    customerCreditCardViewModel.CardNumber = _creditCardDescriptorBusiness.ApplyMask(creditCardDescriptor, "-", customerCreditCard.CardNumber);
                    customerCreditCardViewModel.CardNo = customerCreditCard.CardNumber;
                    customerCreditCardViewModel.MaskedCardNumber = _paymentMethodService.ApplyMask(customerCreditCard.CardNumber);
                    customerCreditCardViewModel.Holder = customerCreditCard.Holder;
                    customerCreditCardViewModel.TypeId = customerCreditCard.TypeId;
                    customerCreditCardViewModel.Alias = customerCreditCard.OgoneCreditCardAlias;
                    customerCreditCardViewModel.OgoneTransactionProcess = (!d3dEnable ? null : creditCardDescriptor.TransactionProcess);
                    customerCreditCardViewModel.Is3DSecureTransaction = flag3DsEnable;
                    customerCreditCardViewModel.CryptoLength = creditCardDescriptor.CryptoLength;
                    // Date d'expiration au format Ogone (valeur du param ED envoyé à Alias Gateway)
                    customerCreditCardViewModel.ExpirationDate = $"{customerCreditCard.ExpireMonth}/{customerCreditCard.ExpireYear.ToString().Substring(2)}";
                    //Set initially IsCryptoRequired = true
                    customerCreditCardViewModel.IsCryptoRequired = true;

                    customerCreditCardViewModel.Brand = customerCreditCard.Brand;
                    customerCreditCardViewModel.ExpirationDateFormat = $"{customerCreditCard.ExpireMonth}/{customerCreditCard.ExpireYear.ToString().Substring(2)}";

                    customerCreditCardViewModel.Url = _applicationContext.GetImagePath() + string.Format(CreditCardPictoPattern, customerCreditCard.TypeId);

                    customerCreditCardViewModel.Label = string.Format("{0} {1}", customerCreditCardViewModel.MaskedCardNumber, customerCreditCardViewModel.Holder);
                }
            }
            return customerCreditCardViewModel;
        }
    }
}
