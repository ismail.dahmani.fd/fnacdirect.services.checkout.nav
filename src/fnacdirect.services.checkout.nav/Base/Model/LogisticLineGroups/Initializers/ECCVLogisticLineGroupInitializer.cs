using FnacDirect.OrderPipe.Base.BaseModel;
using FnacDirect.OrderPipe.Base.Business.LogisticLineGroups.Internals.Initializers;
using FnacDirect.OrderPipe.Base.LineGroups;
using FnacDirect.OrderPipe.Base.Model;
using System;
using System.Collections.Generic;
using System.Linq;

namespace FnacDirect.OrderPipe.Base.Business.LogisticLineGroups.Initializers
{
    public class ECCVLogisticLineGroupInitializer : BaseLogisticLineGroupInitializer
    {
        private readonly ILogisticLineGroupGenerator _logisticLineGroupGenerator;

        public ECCVLogisticLineGroupInitializer(ILogisticLineGroupGenerator logisticLineGroupGenerator)
        {
            _logisticLineGroupGenerator = logisticLineGroupGenerator;
        }


        public override IEnumerable<ILogisticLineGroup> Initialize(IDictionary<ILineGroupHeader, IEnumerable<Article>> selectedLineGroups)
        {
            var shippingMethodEvaluation = Context.IGotShippingMethodEvaluation.ShippingMethodEvaluation;

            var output = new List<ILogisticLineGroup>();

            foreach (var lineGroup in selectedLineGroups.Keys)
            {
                var articlesByPrid = SplitArticlesByPrid(selectedLineGroups[lineGroup]);

                foreach (var articles in articlesByPrid.Values)
                {
                    var logisticLineGroup = CreateLogisticLineGroup(lineGroup, articles, shippingMethodEvaluation);
                    output.Add(logisticLineGroup);

                    // Update shipping method
                    SetShippingMethod(logisticLineGroup.Articles, Constants.ShippingMethods.ServiceEnLigneNonExpedie);
                }
            }

            return output;
        }

        #region privates

        private LogisticLineGroup CreateLogisticLineGroup(ILineGroupHeader lineGroup, List<Article> articles, ShippingMethodEvaluation shippingMethodEvaluation)
        {
            var logisticLineGroup = _logisticLineGroupGenerator.Generate(lineGroup);
            logisticLineGroup.UniqueId = Guid.NewGuid().ToString();
            logisticLineGroup.Informations.OrderType = (int)OrderInfoOrderTypeEnum.Standard;
            logisticLineGroup.OrderType = OrderTypeEnum.FnacCom;
            logisticLineGroup.Articles = articles;

            Helper.CopyShippingMethodEvaluation(lineGroup, logisticLineGroup, shippingMethodEvaluation);

            return logisticLineGroup;
        }

        private void SetShippingMethod(List<Article> articles, int shippingMethod)
        {
            foreach (var article in articles.OfType<BaseArticle>())
            {
                article.ShipMethod = shippingMethod;
            }
        }

        private Dictionary<int, List<Article>> SplitArticlesByPrid(IEnumerable<Article> articles)
        {
            var result = new Dictionary<int, List<Article>>();

            foreach (var article in articles)
            {
                if (article.ProductID.HasValue)
                {
                    if (!result.ContainsKey(article.ProductID.Value))
                    {
                        result.Add(article.ProductID.Value, new List<Article>());
                    }

                    result[article.ProductID.Value].Add(article);
                }
                else
                    throw new Exception("ECCV Linegroup Initializer : Article with null prid");
            }

            return result;
        }
        #endregion
    }
}
