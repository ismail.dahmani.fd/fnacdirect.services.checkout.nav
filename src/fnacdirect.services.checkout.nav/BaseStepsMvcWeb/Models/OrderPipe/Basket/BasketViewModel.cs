using FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Common;
using FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Common.Basket;
using System;
using System.Collections.Generic;
using System.Linq;


namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Basket
{
    public class BasketViewModel
    {
        public bool HasFnac { get; set; }

        public bool HasInsurance { get; set; }

        public bool HasMarketPlace { get; set; }

        private readonly List<BasketSellerViewModel> _sellers
          = new List<BasketSellerViewModel>();

        public IReadOnlyCollection<BasketSellerViewModel> Sellers
        {
            get
            {
                return _sellers;
            }
        }

        public BasketViewModel WithSeller(params BasketSellerViewModel[] basketSellerViewModels)
        {
            foreach (var basketSeller in basketSellerViewModels)
            {
                if (basketSeller.Informations.IsFnac)
                {
                    HasFnac = true;
                }
                else
                {
                    HasMarketPlace = true;
                }
            }

            _sellers.AddRange(basketSellerViewModels);

            return this;
        }

        public bool HasManySeller
        {
            get
            {
                return _sellers.Count > 1;
            }
        }

        public bool DispayUnavailableOrLimitedQuantityWarning { get; set; }

        public bool HasAvailabledArticles
        {
            get { return _sellers.SelectMany(m => m.Items).Any(i => i.IsAvailable); }
        }

        public bool HasUnavailableItems
        {
            get
            {
                return _sellers.SelectMany(m => m.Items).Any(it => !it.IsAvailable);
            }
        }

        public ExpressPlusViewModel ExpressPlusInfo { get; set; }

        public bool HasAlreadyBenefitedOfOffer { get; set; }

        public bool HasAdherentCard { get; set; }

        public bool HasFnacPlusCard { get; set; }

        public bool HasFnacPlusTryCard { get; set; }
        public string FnacPlusTryCardDuration { get; set; }

        public bool HasTryAdherentCard { get; set; }

        public bool DisplayBirthDatePopin { get; set; }

        public bool HasTrialExpressPlus
        {
            get
            {
                if (_sellers.SelectMany(m => m.Items).Any(it => it.ExpressPlusInfo != null))
                {
                    return _sellers.SelectMany(m => m.Items).Where(it => it.ExpressPlusInfo != null && it.ExpressPlusInfo.IsTrial).Any();
                }
                return false;
            }
        }

        public List<int> MaxAccountDeletedArticles { get; set; }

        public ReducedPriceViewModel ReducedPriceBook5pc { get; set; }

        public bool HasDeezerOffer
        {
            get { return _sellers.Any(x => x.Items.Any(i => i.Price != null && i.Price.Templates.Any(t => string.Equals(t.ShoppingCartText, "deezer", StringComparison.InvariantCultureIgnoreCase)))); }
        }

        public bool HasOnlyDematerializedServices
        {
            get
            {
                return _sellers.All(s => s.Items.All(i => i.ProductUrl == null 
                                                        && (i.IsFnacPlusCard 
                                                            || i.IsFnacPlusTryCard 
                                                            || i.IsMembershipRecruitCard 
                                                            || i.IsMembershipRenewCard 
                                                            || i.IsTryCard )));
            }
        }

        public OrderExternalReferenceViewModel OrderExternalReference { get; set; }
        
        public string ChristmasWording { get; set; }

        public bool HasChristmasWording
        {
            get
            {
                return !string.IsNullOrEmpty(ChristmasWording);
            }
        }

        public int? AddStatus { get; set; }

        public RemindersViewModel Reminders { get; set; }
    }
}
