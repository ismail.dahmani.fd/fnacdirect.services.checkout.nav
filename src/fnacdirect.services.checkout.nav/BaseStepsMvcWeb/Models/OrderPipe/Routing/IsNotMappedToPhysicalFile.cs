﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Hosting;
using System.Web.Routing;

namespace FnacDirect.OrderPipe.CoreRouting
{
    public class IsNotMappedToPhysicalFile : IRouteConstraint
    {
        public bool Match(HttpContextBase httpContext, Route route, string parameterName, RouteValueDictionary values,
            RouteDirection routeDirection)
        {
            return !HostingEnvironment.VirtualPathProvider.FileExists(httpContext.Request.Url.LocalPath);
        }
    }
}
