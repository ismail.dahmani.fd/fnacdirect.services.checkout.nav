﻿
namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Common.Basket
{
    public class OrderExternalReferenceViewModel
    {
        public string Value { get; set; }
    }
}
