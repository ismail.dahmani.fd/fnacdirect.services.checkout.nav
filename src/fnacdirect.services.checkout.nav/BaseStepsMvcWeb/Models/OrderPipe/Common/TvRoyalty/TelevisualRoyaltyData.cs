﻿using FnacDirect.OrderPipe.Base.Model;

namespace FnacDirect.OrderPipe.Base.BaseModel.Model
{
    public class TelevisualRoyaltyData : CustomerData
    {
        private TVRoyalty _value;      

        public TVRoyalty Value
        {
            get { return _value; }
            set { _value = value; }
        }
    }
}
