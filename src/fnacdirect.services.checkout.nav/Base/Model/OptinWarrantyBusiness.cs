using FnacDirect.Contracts.Online.IModel;
using FnacDirect.Contracts.Online.Model;
using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.Technical.Framework.Web.Mvc;
using System.Collections.Generic;
using System.Linq;

namespace FnacDirect.OrderPipe.Base.Business
{
    public class OptinWarrantyBusiness : IOptinWarrantyBusiness
    {
        private readonly ISiteManagerBusiness2 _siteManagerBusiness2;
        private readonly IApplicationContext _applicationContext;

        public OptinWarrantyBusiness(ISiteManagerBusiness2 siteManagerBusiness2, IApplicationContext applicationContext)
        {
            _siteManagerBusiness2 = siteManagerBusiness2;
            _applicationContext = applicationContext;
        }

        /// <summary>
        /// Regarde si dans la liste d'articles fournie, on a au moins un article avec un service
        /// </summary>
        public bool HasWarrantyLinked(IEnumerable<StandardArticle> standardArticles)
        {
            var context = new FnacDirect.Contracts.Online.Model.Context
            {
                SiteContext = _applicationContext.GetSiteContext()
            };

            foreach (var article in standardArticles)
            {
                var hasWarrantyLinked = ArticleHasWarrantyLinked(context, article);

                if (hasWarrantyLinked)
                {
                    return true;
                }
            }
            return false;
        }
        
        /// <summary>
        /// Retourne la liste des articles ayant au moins un service associé.
        /// </summary>
        /// <returns>Liste des articles avec un service</returns>
        public IEnumerable<int> ArticlesWithWarrantyLinked(IEnumerable<StandardArticle> standardArticles)
        {
            var result = new List<int>();

            var context = new FnacDirect.Contracts.Online.Model.Context
            {
                SiteContext = _applicationContext.GetSiteContext()
            };

            foreach (var article in standardArticles)
            {
                var hasWarrantyLinked = ArticleHasWarrantyLinked(context, article);

                if (hasWarrantyLinked && article.ProductID.HasValue)
                {
                    result.Add(article.ProductID.Value);
                }
            }

            return result;
        }

        private bool ArticleHasWarrantyLinked(FnacDirect.Contracts.Online.Model.Context context, StandardArticle article)
        {
            var articleReference = new ArticleReference()
            {
                PRID = article.ProductID.GetValueOrDefault(),
                Catalog = ArticleCatalog.FnacDirect
            };
            var categoryLinkedArticle = _siteManagerBusiness2.GetLinkedArticles(context, articleReference, LinkedType.NodeServices, null, null, null);

            var hasWarrantyLinked = categoryLinkedArticle.Categories.Any(c => Constants.WarrantyTypes.Contains(c.ID))
                && !article.Services.Any(s => s.TypeId.HasValue && Constants.WarrantyTypes.Contains(s.TypeId.Value));
            return hasWarrantyLinked;
        }
    }
}

