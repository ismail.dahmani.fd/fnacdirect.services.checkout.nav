using FnacDirect.Front.WebBusiness.WebMobile;
using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.OrderPipe.Base.Steps.Pop;
using FnacDirect.OrderPipe.BaseMvc.Web.Controllers.Pop;
using FnacDirect.Technical.Framework.Web;
using System.Linq;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Pop.Models.Common
{
    public class NavigationViewModelBuilder : INavigationViewModelBuilder
    {
        private readonly IOrderPipeUrlBuilder _orderPipeUrlBuilder;
        private readonly IUrlManager _urlManager;

        public NavigationViewModelBuilder(IOrderPipeUrlBuilder orderPipeUrlBuilder, IUrlManager urlManager)
        {
            _orderPipeUrlBuilder = orderPipeUrlBuilder;
            _urlManager = urlManager;

        }
        public NavigationViewModel Build(PopOrderForm popOrderForm, string urlName)
        {
            var navigationViewModel = new NavigationViewModel { HasNextSeller = false };

            var popShippingData = popOrderForm.GetPrivateData<PopShippingData>(create: false);
            var firstPopShipingSellerData = popShippingData.Sellers.FirstOrDefault();

            if (firstPopShipingSellerData != null)
            {
                var popShippingRouteValueProvider = new PopShippingRouteValueProvider();
                var routeValues = popShippingRouteValueProvider.GetRouteValueFor(popOrderForm, firstPopShipingSellerData);

                navigationViewModel.BackLinkUrl = _orderPipeUrlBuilder.GetUrl(urlName, routeValues);
            }
            else
            {
                navigationViewModel.BackLinkUrl = _orderPipeUrlBuilder.GetUrl(urlName);
            }

            return navigationViewModel;
        }

        public NavigationViewModel BackToHome()
        {
            var navigationViewModel = new NavigationViewModel { HasNextSeller = false };

            navigationViewModel.BackLinkUrl = _urlManager.Sites.WWWDotNet.BaseRootUrl.FullPath;

            navigationViewModel.IsMobileApplication = MobileAppValues.IsMobileApplication();
            navigationViewModel.IsIos = MobileAppValues.IsIphoneApp();

            return navigationViewModel;
        }
    }
}
