namespace FnacDirect.OrderPipe.Base.Model
{
    /// <summary>
    /// voir avec la table tbl_OrderType dans la base COMMERCE
    /// </summary>
    public enum OrderInfoOrderTypeEnum
    {
        /// <summary>Standard</summary>
        Standard = 0,
        /// <summary>Chèque Cadeau Virtuel</summary>
        Ccv = 1,
        /// <summary>Retrait magasin</summary>
        RetraitMagasin = 2,
        /// <summary>Magasin / retrait magasin</summary>
        CMRetraitMagasin = 3,
        /// <summary>Magasin / livraison magasin</summary>
        CMLivraisonDomicile = 4,
        /// <summary>Market Place</summary>
        Marketplace = 5,
        /// <summary>Demat</summary>
        Demat = 6,
        /// <summary>Standard Pro</summary>
        StandardPro = 7,
        /// <summary>Chèque Cadeau Virtuel Pro</summary>
        CcvPro = 8,
        /// <summary>Client Magasin Pro</summary>
        CMRetraitMagasinPro = 9,
        /// <summary>StandardClient Domicile Pro</summary>
        CMLivraisonDomicilePro = 10,
        /// <summary>Réservation Pro</summary>
        ReservationPro = 11,
        /// <summary>Intra mag</summary>
        Intramag = 12,
        /// <summary>Demat Soft</summary>
        DematSoft = 13,
        /// <summary>Intra mag libre service</summary>
        IntramagPE = 14
    }
}
