﻿namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.OrderThankYou
{
    public class PayByRefViewModel
    {
        public string MerchantId { get; set; }
        public string Reference { get; set; }
        public string AmountEur { get; set; }
    }
}
