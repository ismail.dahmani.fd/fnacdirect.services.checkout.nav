﻿using System;
using FnacDirect.OrderPipe.Base.Business.Billing;
using FnacDirect.OrderPipe.Base.Model.Facets;
using FnacDirect.OrderPipe.Base.LineGroups;
using System.Collections.Generic;
using FnacDirect.OrderPipe.Base.Business.LogisticLineGroups.Internals.Handlers;

namespace FnacDirect.OrderPipe.Base.Business.LogisticLineGroups.Handlers
{
    public class SplitBillingMethodByLineGroupProcessCompletedHandler : ILogisticLineGroupProcessCompletedHandler
    {
        private readonly ISplitBillingMethodBusiness _splitBillingMethodBusiness;

        public SplitBillingMethodByLineGroupProcessCompletedHandler(ISplitBillingMethodBusiness splitBillingMethodBusiness)
        {
            _splitBillingMethodBusiness = splitBillingMethodBusiness;
        }

        public void OnGenerated(ILogisticLineGroupGenerationProcessContext logisticLineGroupGenerationProcessContext, IEnumerable<ILogisticLineGroup> logisticLineGroups)
        {
            _splitBillingMethodBusiness.SplitBillingMethodByLineGroup(logisticLineGroupGenerationProcessContext.IGotBillingInformations, logisticLineGroups);
        }
    }
}
