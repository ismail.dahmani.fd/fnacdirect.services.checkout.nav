using FnacDirect.OrderPipe.Base.LineGroups;
using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.OrderPipe.Base.Model.Facets;
using FnacDirect.Technical.Framework.CoreServices;
using System;
using System.Collections.Generic;
using System.Linq;

namespace FnacDirect.OrderPipe.Base.Business.Billing
{
    public class SplitBillingMethodBusiness : ISplitBillingMethodBusiness
    {
        public void SplitBillingMethodByLineGroup(IGotBillingInformations iGotBillingInformations, IEnumerable<ILogisticLineGroup> logisticLineGroups)
        {
            // Init
            // Reste à payer global
            iGotBillingInformations.RemainingTotal = iGotBillingInformations.GlobalPrices.TotalPriceEur.GetValueOrDefault(0);

            var remainingBillingMethod = new Dictionary<int, decimal>(iGotBillingInformations.BillingMethodConsolidation);

            // Reste à payer pour chaque linegroupe
            foreach (var lineGroup in logisticLineGroups)
            {
                lineGroup.RemainingTotal = lineGroup.GlobalPrices.TotalPriceEur.GetValueOrDefault(0);
                lineGroup.BillingMethodConsolidation.Clear();
            }
            ////////////////////////////////////////////////////////////////////////////////////////
            foreach (var voucherBillingMethod in iGotBillingInformations.OrderBillingMethods.OfType<VoucherBillingMethod>())
            {
                ILogisticLineGroup lineGroup = null;

                if (voucherBillingMethod.DematCondition == VoucherBillingMethod.DematConditionEnum.DematOnly)
                {
                    lineGroup = logisticLineGroups.FirstOrDefault(l => l.IsNumerical);
                }

                if (voucherBillingMethod.DematCondition == VoucherBillingMethod.DematConditionEnum.PhysicalOnly || voucherBillingMethod.DematCondition == VoucherBillingMethod.DematConditionEnum.Mixed)
                {
                    lineGroup = logisticLineGroups.FirstOrDefault(l => l.IsFnacCom);
                }

                if (lineGroup != null && !lineGroup.IsMarketPlace)
                {
                    lineGroup.BillingMethodConsolidation.Add(voucherBillingMethod.BillingMethodId, voucherBillingMethod.AmountEur.Value);
                    remainingBillingMethod[voucherBillingMethod.BillingMethodId] -= voucherBillingMethod.AmountEur.Value;
                    lineGroup.RemainingTotal -= voucherBillingMethod.AmountEur.Value;
                    iGotBillingInformations.RemainingTotal -= voucherBillingMethod.AmountEur.Value;
                }

                if (remainingBillingMethod[voucherBillingMethod.BillingMethodId] != 0)
                {
                    // Protection Level : Normally, we can't reach this C# code
                    var errorMessage = "the amount for the voucher named " + voucherBillingMethod.CheckNumber + " is highter than Fnac.com Articles total amount. VoucherAmount = " + voucherBillingMethod.AmountEur.Value + ", Fnac.com total amount = " + lineGroup.GlobalPrices.TotalPriceEur + ", total shipping amount : " + lineGroup.GlobalPrices.TotalShipPriceUserEur;
                    var errorApplyingVoucher = new Exception(errorMessage);

                    Logging.Current.WriteError(errorApplyingVoucher, LoggingCategory.Business);
                    throw errorApplyingVoucher;
                }
            }

            // Dispatch remainingBillingMethod pour le reste des lines groups
            var counter = 1;
            var copyRemainingBillingMethod = new Dictionary<int, decimal>(remainingBillingMethod);

            foreach (var lineGroup in logisticLineGroups)
            {
                var last = (counter == logisticLineGroups.Count());

                foreach (var keyValuePair in copyRemainingBillingMethod)
                {
                    if (remainingBillingMethod[keyValuePair.Key] <= 0 && keyValuePair.Key == VoucherBillingMethod.IdBillingMethod)
                    {
                        continue;
                    }

                    decimal val = 0;

                    if (!last)
                    {
                        if (iGotBillingInformations.RemainingTotal > 0 && keyValuePair.Value > 0)
                        {
                            val = lineGroup.RemainingTotal/iGotBillingInformations.RemainingTotal*keyValuePair.Value;
                        val = Math.Round(val, 2);
                        remainingBillingMethod[keyValuePair.Key] -= val;
                    }
                    }
                    else
                    {
                        val = remainingBillingMethod[keyValuePair.Key];
                    }

                    lineGroup.BillingMethodConsolidation.Add(keyValuePair.Key, val);
                }

                counter++;
            }

            foreach (var lg in logisticLineGroups)
            {
                if (lg.BillingMethodConsolidation.Count == 0 && lg.GlobalPrices.TotalPriceEur == 0)
                    lg.BillingMethodConsolidation.Add(FreeBillingMethod.IdBillingMethod, 0.0m);
            }
        }
    }
}
