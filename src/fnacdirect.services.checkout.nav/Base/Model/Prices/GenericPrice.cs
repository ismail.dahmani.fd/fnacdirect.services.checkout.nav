namespace FnacDirect.OrderPipe.Base.Model
{
    public class GenericPrice
    {
        public GenericPrice()
        {
        }

        public GenericPrice(GenericPrice source)
        {
            Cost = source.Cost;
            NoTaxCost = source.NoTaxCost;
            CostToApply = source.CostToApply;

            if (source.TaxInfos != null)
            {
                TaxInfos = new VatInfo(source.TaxInfos);
            }
        }

        public decimal Cost { get; set; }
        public decimal? NoTaxCost { get; set; }
        public decimal TaxCost => NoTaxCost.HasValue ? Cost - NoTaxCost.Value : 0.0m;
        public decimal? CostToApply { get; set; }
        public VatInfo TaxInfos { get; set; }

        public void ResetCost()
        {
            Cost = 0;
            NoTaxCost = 0;
            CostToApply = 0;
        }
    }
}
