using FnacDirect.OrderPipe.Base.Model.Fly;

namespace FnacDirect.OrderPipe.Base.Model.Shipping.Postal.Appointment.Fly
{
    public interface IFlyForm
    {
        int ShippingMethodId { get; set; }

        int ProductQuantityToTakeBack { get; }

        string PhoneNumber { get; }

        bool HasElevatorAccess { get; }

        bool HasStaircaseStep { get; }

        string AccessCode { get; }

        string Commentary { get; }

        int? FloorNumber { get; }

        int? StaircaseType { get; }

        CalendarSlotIdentifier CalendarSlotIdentifier { get; set; }

        FlyServiceByArticle[] SelectedFlyServices { get; set; }
    }
}
