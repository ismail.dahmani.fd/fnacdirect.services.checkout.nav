﻿using System;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Common.Shipping
{
    public class BillingAddressModel
    {
        public Maybe<AddressViewModel> BillingAddress { get; set; }

        public bool HasBillingAddress
        {
            get
            {
                return BillingAddress.HasValue;
            }
        }

        public bool HasShippingAddress { get; set; }

        public bool HasPostalShippingAddress { get; set; }

        public bool HasEligibleBillingAdress { get; set; }

        public bool DisplayMembershipMessage { get; set; }
    }
}
