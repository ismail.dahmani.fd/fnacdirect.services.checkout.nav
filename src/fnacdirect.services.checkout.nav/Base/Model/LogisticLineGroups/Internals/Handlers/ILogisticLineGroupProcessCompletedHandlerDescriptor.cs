﻿using StructureMap.Pipeline;
using System;

namespace FnacDirect.OrderPipe.Base.Business.LogisticLineGroups.Internals.Handlers
{
    public interface ILogisticLineGroupProcessCompletedHandlerDescriptor
    {
        int Priority { get; }

        Type Type { get; }

        ExplicitArguments GetExplicitArguments();
    }
}
