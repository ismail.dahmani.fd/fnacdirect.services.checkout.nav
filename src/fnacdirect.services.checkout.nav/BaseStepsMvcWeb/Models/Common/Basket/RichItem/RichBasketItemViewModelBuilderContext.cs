﻿using FnacDirect.OrderPipe.Base.BaseModel.Model;
using FnacDirect.OrderPipe.Base.Model;
using System.Collections.Generic;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Basket
{
    public class RichBasketItemViewModelBuilderContext
    {
        private readonly IPrivateDataContainer _privateDataContainer;
        private readonly IEnumerable<Promotion> _globalPromotions;
        private readonly bool _applyAdherentPromotion;

        public RichBasketItemViewModelBuilderContext(IPrivateDataContainer privateDataContainer, IEnumerable<Promotion> globalPromotions, bool applyAdherentPromotion)
        {
            _applyAdherentPromotion = applyAdherentPromotion;
            _privateDataContainer = privateDataContainer;
            _globalPromotions = globalPromotions;
        }

        public AboIlliData AboIlliData => _privateDataContainer.GetPrivateData<AboIlliData>("AboIlliGroup");

        public IEnumerable<Promotion> GlobalPromotions => _globalPromotions;

        public bool ApplyAdherentPromotion => _applyAdherentPromotion;
    }
}
