﻿namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Payment
{
    public class DirectLink3DSecureViewModel
    {
        public DirectLink3DSecureViewModel()
        {
        }

        public string Html3DSecureInput { get; set; }
    }
}
