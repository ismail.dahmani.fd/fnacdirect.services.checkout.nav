using FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Basket;
using FnacDirect.Technical.Framework.Web.Mvc;
using System.Collections.Generic;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Common.Payment
{
    public class PaymentSummaryViewModel
    {
        private readonly List<int> _paymentCards = new List<int>();

        private readonly List<int> _PaymentMethods = new List<int>();

        public string Basket { get; set; }

        public string OldTotalCost { get; set; }

        public string OldTotalCostNoVAT { get; set; }

        public string OldTotalCostToDisplay { get; set; }

        public PriceDetailsViewModel OldTotalCostModel { get; set; }

        public PriceDetailsViewModel OldTotalCostNoVATModel { get; set; }

        public PriceDetailsViewModel OldTotalCostModelToDisplay { get; set; }

        public string TotalCost { get; set; }

        public string TotalCostNoVAT { get; set; }

        public string TotalCostToDisplay { get; set; }

        public PriceDetailsViewModel TotalCostModel { get; set; }

        public PriceDetailsViewModel TotalCostNoVATModel { get; set; }

        public PriceDetailsViewModel TotalCostModelToDisplay { get; set; }
        public string TotalShippingCost { get; set; }

        public string TotalShippingCostNoVAT { get; set; }

        public string TotalShippingCostToDisplay { get; set; }

        public PriceDetailsViewModel TotalShippingCostModel { get; set; }

        public PriceDetailsViewModel TotalShippingCostNoVATModel { get; set; }

        public PriceDetailsViewModel TotalShippingCostModelToDisplay { get; set; }

        public string TotalGlobal { get; set; }

        public string TotalGlobalNoVAT { get; set; }

        public string TotalGlobalToDisplay { get; set; }

        public PriceDetailsViewModel TotalGlobalModel { get; set; }

        public PriceDetailsViewModel TotalGlobalNoVATModel { get; set; }

        public PriceDetailsViewModel TotalGlobalModelToDisplay { get; set; }

        public string RemainingAmount { get; set; }

        public string PromoCode { get; set; }

        public string PromoCodeAmount { get; set; }

        public string TotalReduced { get; set; }

        public string TotalReducedNoVAT { get; set; }

        public PriceDetailsViewModel TotalReducedModel { get; set; }

        public PriceDetailsViewModel TotalReducedNoVATModel { get; set; }

        public string TotalReducedToDisplay { get; set; }

        public PriceDetailsViewModel TotalReducedModelToDisplay { get; set; }

        public bool IsFreeOrder { get; set; }

        public bool IsFreeShipping { get; set; }

        public string ShippingPush { get; set; }

        public bool ShowBookShopShippingPush { get; set; }

        public string ShopShippingPush { get; set; }

        public bool IsExpressPlus { get; set; }

        public bool IsFnacPlusOrHasFnacPlusCardInBasket { get; set; }

        public bool IsRelais { get; set; }

        public bool IsShop { get; set; }

        public string DifferedSavedAmount { get; set; }

        public string SplitCost { get; set; }

        public string SplitCostNoVAT { get; set; }

        public string SplitCostToDisplay { get; set; }

        public PriceDetailsViewModel SplitCostModel { get; set; }

        public PriceDetailsViewModel SplitCostNoVATModel { get; set; }

        public PriceDetailsViewModel SplitCostModelToDisplay { get; set; }

        public bool HasSplit { get; set; }

        public bool IsFreeSplit { get; set; }

        public string WrapCost { get; set; }

        public string WrapCostNoVAT { get; set; }

        public PriceDetailsViewModel WrapCostModel { get; set; }

        public PriceDetailsViewModel WrapCostNoVATModel { get; set; }

        public string WrapCostToDisplay { get; set; }

        public PriceDetailsViewModel WrapCostModelToDisplay { get; set; }

        public bool HasWrap { get; set; }

        public bool IsFreeWrap { get; set; }

        public bool IsPayButtonActive { get; set; }

        public bool HasHideBillingInfo { get; set; }

        public int ProductsCount { get; set; }

        public TermsAndConditionsViewModel TermsAndConditions { get; set; }

        public bool ShouldDisplayWrap { get; set; }

        public void AddPaymentCards(IEnumerable<int> ids)
        {
            _paymentCards.AddRange(ids);
        }

        public List<int> PaymentCards
        {
            get { return _paymentCards; }

        }

        public void AddPaymentMethods(IEnumerable<int> ids)
        {
            _PaymentMethods.AddRange(ids);
        }

        public List<int> PaymentMethods
        {
            get { return _PaymentMethods; }

        }

        private readonly IList<KeyValuePair<string, string>> _secondaryMethods
            = new List<KeyValuePair<string, string>>();

        public IList<KeyValuePair<string, string>> SecondaryMethods
        {
            get
            {
                return _secondaryMethods;
            }
        }

        public ReducedPriceViewModel ReducedPriceBook5pc { get; set; }

        public List<VatRate> VatRates { get; set; }
    }

    public class VatRate
    {
        public string Label { get; set; }

        public string Total { get; set; }

        public decimal Rate { get; set; }
    }
}
