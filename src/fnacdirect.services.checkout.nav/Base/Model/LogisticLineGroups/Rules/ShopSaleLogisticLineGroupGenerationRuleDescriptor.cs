using FnacDirect.OrderPipe.Base.Business.LogisticLineGroups.Handlers;
using FnacDirect.OrderPipe.Base.Business.LogisticLineGroups.Initializers;
using FnacDirect.OrderPipe.Base.Business.LogisticLineGroups.InitialSelectors;
using FnacDirect.OrderPipe.Base.Business.LogisticLineGroups.Unfolders;
using System;
using System.Collections.Generic;

namespace FnacDirect.OrderPipe.Base.Business.LogisticLineGroups.Rules
{
    /// <summary>
    /// Gère les LineGroup intra mag.
    /// </summary>
    public class ShopSaleLogisticLineGroupGenerationRuleDescriptor
        : BaseLogisticLineGroupGenerationRuleDescriptor<ShopSaleLineGroupInitialSelector, ShopSaleLogisticLineGroupInitializer, ServiceLogisticLineGroupUnfolder>
    {
        public override IEnumerable<Type> LogisticLineGroupGeneratedHandlers
        {
            get
            {
                yield return typeof(UpdateShippingMethodChoiceLogisticLineGroupHandler);
                yield return typeof(UpdateShippingUserCostProcessCompletedHandler);
                yield return typeof(CalculateSubTotalsLogisticLineGroupHandler);
            }
        }

        public override string SwitchId => "orderpipe.logisticlinegroupgenerationrule.shopsale";
    }
}
