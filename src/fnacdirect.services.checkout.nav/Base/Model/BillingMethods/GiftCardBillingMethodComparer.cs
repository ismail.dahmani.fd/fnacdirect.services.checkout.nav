using System.Collections.Generic;

namespace FnacDirect.OrderPipe.Base.Model
{
    public class GiftCardBillingMethodComparer : IComparer<GiftCardBillingMethod> // cette classe herite de IComparer : c'est cette classe qui est appel�e par les m�thodes de tri pour savoir comment comparer les �l�ments
    {

        #region IComparer<GiftCardBillingMethod> Members

        public int Compare(GiftCardBillingMethod x, GiftCardBillingMethod y)
        {
            if (x == null)
                return -1;

            if (y == null)
                return 1;

            return x.CardNumber.CompareTo(y.CardNumber);
        }

        #endregion
    }
}
