using FnacDirect.OrderPipe.Base.Business.LogisticLineGroups;
using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.OrderPipe.Base.Model.Facets;
using FnacDirect.OrderPipe.Base.Model.LineGroups;
using FnacDirect.OrderPipe.Base.Proxy.Membership;
using FnacDirect.Technical.Framework.Utils;
using FnacDirect.Technical.Framework.Web.Mvc;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace FnacDirect.OrderPipe.Base.Business.Payment
{
    public enum ECBEligibilityResult
    {
        Eligible,
        Error,
        MultipleLineGroup,
        ClicAndCollect
    }

    public class CheckCreditCardsEligibilityBusiness : ICheckCreditCardsEligibilityBusiness
    {
        private const string CAIXA_SWITCH_NAME = "op.caixa.enabled";

        private readonly Regex _customerAllowedForPaypalViaOgone;
        private readonly OPContext _opContext;
        private readonly ISwitchProvider _switchProvider;
        private readonly IBillingMethodService _billingMethodService;
        private readonly ICheckOriginService _checkOriginService;
        private readonly ILogisticLineGroupGenerationProcess _logisticLineGroupGenerationProcess;
        private readonly IAdherentCardArticleService _adherentCardArticleService;
        private readonly RangeSet<int> _eccvTypeIds;
        private readonly RangeSet<int> _excludedTypes;
        private readonly RangeSet<int> _excludedTypes2;
        private readonly RangeSet<int> _caixaEligibleCountriesIds;

        private readonly decimal _postFinanceMinimumAmount;
        private readonly decimal _caixaMinimumAmount;
        private readonly decimal _caixaMaximumAmount;

        public CheckCreditCardsEligibilityBusiness(OPContext opContext,
            ISwitchProvider switchProvider,
            IBillingMethodService billingMethodService,
            IApplicationContext applicationContext,
            ICheckOriginService checkOriginService,
            ILogisticLineGroupGenerationProcess logisticLineGroupGenerationProcess,
            IAdherentCardArticleService adherentCardArticleService)
        {
            _opContext = opContext;
            _customerAllowedForPaypalViaOgone = new Regex(opContext.Pipe.GlobalParameters.Get("ogone.customer.allowed.paypalviaogone.regularexpression", @"[\D]"));
            _switchProvider = switchProvider;
            _billingMethodService = billingMethodService;
            _checkOriginService = checkOriginService;
            _logisticLineGroupGenerationProcess = logisticLineGroupGenerationProcess;
            _eccvTypeIds = opContext.Pipe.GlobalParameters.GetAsRangeSet<int>("eccv.typeid");

            _excludedTypes = RangeSet.Parse<int>(applicationContext.GetAppSetting("orderpipe.pop.oneclick.excludedtypes"));
            _excludedTypes2 = RangeSet.Parse<int>(applicationContext.GetAppSetting("orderpipe.pop.onepage.excludedtypes"));

            _postFinanceMinimumAmount = opContext.Pipe.GlobalParameters.Get<decimal>("payment.postfinance.minimumamount", decimal.MaxValue);

            _caixaMinimumAmount = opContext.Pipe.GlobalParameters.Get<decimal>("payment.caixa.minimumamount", decimal.MaxValue);
            _caixaMaximumAmount = opContext.Pipe.GlobalParameters.Get<decimal>("payment.caixa.maximumamount", decimal.MaxValue);
            _caixaEligibleCountriesIds = opContext.Pipe.GlobalParameters.GetAsRangeSet<int>("caixa.eligible.countries");

            _adherentCardArticleService = adherentCardArticleService;
        }

        public bool IsPaypalViaOgoneEligible()
        {
            if (!(_opContext.OF is IGotUserContextInformations userContext))
            {
                return false;
            }

            if (!(_opContext.OF is IGotMobileInformations mobileInformations))
            {
                return false;
            }

            if (!(_opContext.OF is IGotLineGroups lineGroups))
            {
                return false;
            }

            if (_checkOriginService.IsFromCallCenter(_opContext.OF as PopOrderForm))
            {
                return false;
            }

            if (_checkOriginService.IsIphoneOrIpad(mobileInformations))
            {
                return false;
            }

            var isMarketPlaceMonoSellerEnabled = _switchProvider.IsEnabled("orderpipe.pop.payment.paypal.filter.marketplacemonoseller"); // Ajouter le switch
            if (isMarketPlaceMonoSellerEnabled)
            {
                if (!IsMarketPlaceMonoSellerEligible(lineGroups.LineGroups))
                {
                    return false;
                }
            }
            else
            {
                if (lineGroups.HasMarketPlaceArticle())
                {
                    return false;
                }
            }

            // Enlever pour les commandes C&C
            var bypassClickAndCollect = _switchProvider.IsEnabled("orderpipe.pop.payment.paypal.filter.bypassclickandcollect");
            if (lineGroups.HasClickAndCollectArticle() && !bypassClickAndCollect)
            {
                return false;
            }

            // Enlever si Articles Numériques
            if (lineGroups.HasNumericalArticle())
            {
                return false;
            }

            // Enlever si Articles Dématérialisés
            if (lineGroups.HasDematSoftArticle())
            {
                return false;
            }

            // L'utilisateur n'est pas autorisé à utiliser PaypalViaOgone
            var accountId = userContext.UserContextInformations.AccountId;
            if (!_customerAllowedForPaypalViaOgone.IsMatch(accountId.ToString()))
            {
                return false;
            }

            // Enlever si le panier contient un ECCV
            var hasEccvInBasket = lineGroups.Articles.Any(a => a.TypeId.HasValue && _eccvTypeIds.Contains(a.TypeId.Value));
            var bypassEccv = _switchProvider.IsEnabled("orderpipe.pop.payment.paypal.filter.bypasseccv");
            if (hasEccvInBasket && !bypassEccv)
            {
                return false;
            }

            return true;
        }

        public ECBEligibilityResult CheckECBEligibility()
        {
            var billingInformationsContainer = _opContext.OF as IGotBillingInformations;

            var allowECBForClicAndCollect = _switchProvider.IsEnabled("orderpipe.cc.allowecb");

            if (!(_opContext.OF is IGotLineGroups lineGroupsContainer))
            {
                return ECBEligibilityResult.Error;
            }

            if ((lineGroupsContainer.LineGroups.Count() > 1 && billingInformationsContainer.EnableOgoneCreditCard) || lineGroupsContainer.IsMultiPspCommand())
            {
                return ECBEligibilityResult.MultipleLineGroup;
            }

            if (lineGroupsContainer.HasClickAndCollectArticle() && !allowECBForClicAndCollect)
            {
                return ECBEligibilityResult.ClicAndCollect;
            }

            return ECBEligibilityResult.Eligible;
        }

        public bool CheckPayByPhoneEligibility()
        {
            return _billingMethodService.IsPhonePaymentAllowed(_opContext, _opContext.OF as PopOrderForm);
        }

        private bool IsMarketPlaceMonoSellerEligible(IEnumerable<ILineGroup> lineGroups)
        {
            if (lineGroups.Count() != 1)
                return false;

            var articles = lineGroups.Where(lg => lg.HasMarketPlaceArticle).SelectMany(lg => lg.Articles);
            var containsExcludedArticles = articles.Any(art => _excludedTypes.Contains(art.TypeId.GetValueOrDefault())
                                                            || _excludedTypes2.Contains(art.TypeId.GetValueOrDefault()));

            return !containsExcludedArticles;
        }

        public bool CheckPostFinanceEligibility()
        {
            if (!_switchProvider.IsEnabled("orderpipe.pop.payment.postfinance.enabled"))
            {
                return false;
            }

            if (!(_opContext.OF is IGotBillingInformations gotBillingInformation))
            {
                return false;
            }

            if (gotBillingInformation.RemainingGlobalPricesForPrimaryMethods() < _postFinanceMinimumAmount)
            {
                return false;
            }

            return true;
        }

        public bool IsCaixaEligible()
        {
            if (!_switchProvider.IsEnabled(CAIXA_SWITCH_NAME))
            {
                return false;
            }

            if (!(_opContext.OF is PopOrderForm popOrderForm))
            {
                return false;
            }

            if (_checkOriginService.IsFromCallCenter(popOrderForm))
            {
                return false;
            }

            var hasAnotherBillingMethod = popOrderForm.OrderBillingMethods.Any(a => a.BillingMethodId != (int)CreditCardTypeEnum.VisaFnac);
            if (hasAnotherBillingMethod)
            {
                return false;
            }

            var remainingGlobalPrices = popOrderForm.RemainingGlobalPricesForPrimaryMethods();
            if (!(remainingGlobalPrices > _caixaMinimumAmount && remainingGlobalPrices < _caixaMaximumAmount))
            {
                return false;
            }

            var billingAddress = popOrderForm.BillingAddress;
            if (!_caixaEligibleCountriesIds.Contains((int)billingAddress.CountryId))
            {
                return false;
            }

            var selectedShippingMethodEvaluations = popOrderForm.ShippingMethodEvaluation.ShippingMethodEvaluationGroups
                                                    .Select(s => s.SelectedShippingMethodEvaluation)
                                                    .Where(s => s?.ShippingAddress?.CountryId != null)
                                                    .ToList();

            if (!selectedShippingMethodEvaluations.IsNullOrEmpty())
            {
                if (selectedShippingMethodEvaluations.Any(s => !_caixaEligibleCountriesIds.Contains((int)s.ShippingAddress.CountryId)))
                {
                    return false;
                }

                if (selectedShippingMethodEvaluations.Any(s => s.SelectedShippingMethodId == Constants.ShippingMethods.LivraisonDeuxHeuresChrono))
                {
                    return false;
                }
            }

            if (popOrderForm.HasClickAndCollectArticle())
            {
                return false;
            }

            if (popOrderForm.LineGroups.GetArticles().OfType<StandardArticle>().Any(a => _adherentCardArticleService.IsAdherentCard(a.ProductID.GetValueOrDefault(0))))
            {
                return false;
            }

            var multiFacet = MultiFacet<IGotLineGroups, IGotBillingInformations, IGotShippingMethodEvaluation>.From(popOrderForm);
            var isMonologistic = _logisticLineGroupGenerationProcess.SimulateInitialize(multiFacet).Count() == 1;
            return isMonologistic;
        }
    }
}
