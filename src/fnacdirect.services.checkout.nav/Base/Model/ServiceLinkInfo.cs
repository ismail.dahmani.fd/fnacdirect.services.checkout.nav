using System;
using System.Collections.Generic;

namespace FnacDirect.OrderPipe.Base.Model
{
    [Serializable]
    public class ServiceLinkInfo
    {
        private int _MasterPrid;
        public int MasterPrid
        {
            get { return _MasterPrid; }
            set { _MasterPrid = value; }
        }

        private List<OrderLine> _ListOrders;
        public List<OrderLine> ListOrders
        {
            get { return _ListOrders; }
            set { _ListOrders = value; }
        }

        private int _OdtId;
        public int OdtId
        {
            get { return _OdtId; }
            set { _OdtId = value; }
        }

        private string _GenUID;
        public string GenUID
        {
            get { return _GenUID; }
            set { _GenUID = value; }
        }

        private int _IndexOrd;
        public int IndexOrd
        {
            get { return _IndexOrd; }
            set { _IndexOrd = value; }
        }
    }
}
