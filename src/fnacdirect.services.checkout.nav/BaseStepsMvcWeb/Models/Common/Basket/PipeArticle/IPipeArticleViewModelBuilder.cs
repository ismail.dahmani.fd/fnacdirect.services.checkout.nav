using FnacDirect.OrderPipe.Base.Model;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Common.Basket.PipeArticle
{
    public interface IPipeArticleViewModelBuilder
    {
        PipeArticleViewModel Build(BaseArticle baseArticle);
    }
}
