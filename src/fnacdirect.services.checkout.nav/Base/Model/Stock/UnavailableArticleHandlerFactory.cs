using FnacDirect.OrderPipe.Base.Business.Shipping.Shop;
using FnacDirect.OrderPipe.Base.Model.Facets;
using FnacDirect.OrderPipe.Base.Proxy.Store;
using System;
using System.Collections.Generic;

namespace FnacDirect.OrderPipe.Base.Business.Stock
{
    public class UnavailableArticleHandlerFactory : IUnavailableArticleHandlerFactory
    {
        private readonly IShipToStoreAvailabilityService _shipToStoreAvailabilityService;
        private readonly IFnacStoreService _fnacStoreService;
        private readonly ISwitchProvider _switchProvider;

        public UnavailableArticleHandlerFactory(IShipToStoreAvailabilityService shipToStoreAvailabilityService,
                                                IFnacStoreService fnacStoreService,
                                                ISwitchProvider switchProvider)
        {
            _shipToStoreAvailabilityService = shipToStoreAvailabilityService;
            _fnacStoreService = fnacStoreService;
            _switchProvider = switchProvider;
        }

        public IUnavailableArticleHandler Get(IGotLineGroups iGotLineGroups)
        {
            var handlers = new List<Func<IGotLineGroups, IUnavailableArticleHandler>>
                {
                    args => new ClickAndCollectOnlyUnavailableArticleHandler(_shipToStoreAvailabilityService, _fnacStoreService, _switchProvider, args),
                    args => new DefaultUnavailableArticleHandler(args)
                };

            return new CompositeUnavailableArticleHandler(handlers, iGotLineGroups);
        }
    }
}
