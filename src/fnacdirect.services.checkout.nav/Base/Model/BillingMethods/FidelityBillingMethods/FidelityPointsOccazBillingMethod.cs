using System;
using System.Xml.Serialization;

namespace FnacDirect.OrderPipe.Base.Model
{
    public class FidelityPointsOccazBillingMethod : FidelityBillingMethod
    {
        public static int IdBillingMethod
        {
            get { return 262144; }
        }

        public FidelityPointsOccazBillingMethod()
        {
            BillingMethodId = IdBillingMethod;
            Priority = 10;
            CanModifyAmountDown = false;
            CanModifyAmountUp = false;
            AdvantageCodeType = Membership.Model.Constants.AdvantageCodeType.SIEBEL_CUMUL_GAMING; 
        }
    }
}