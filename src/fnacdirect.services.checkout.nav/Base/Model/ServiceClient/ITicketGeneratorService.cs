namespace FnacDirect.OrderPipe.Base.Business.Tickets.ServiceClient
{
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ServiceModel.ServiceContractAttribute(ConfigurationName = "FnacDirect.OrderPipe.Base.Business.Tickets.ServiceClient.ITicketGeneratorService")]
    public interface ITicketGeneratorService
    {

        [System.ServiceModel.OperationContractAttribute(Action = "http://tempuri.org/ITicketGeneratorService/GenerateTGVTicket", ReplyAction = "http://tempuri.org/ITicketGeneratorService/GenerateTGVTicketResponse")]
        FnacDirect.OrderPipe.Base.Business.Tickets.ServiceClient.GeneratedTicket GenerateTGVTicket(string refgu, string numCaisse);
    }
}
