using System.Collections.Generic;
using System.Linq;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Common.Payment
{
    public class BillingMethodViewModel
    {
        public int Id { get; set; }

        public string Type { get; set; }

        public string Label { get; set; }

        public string Name { get; set; }

        public string TermsAndConditions { get; set; }

        public int Order { get; set; }

        public string TotalGlobal { get; set; }

        public bool Display { get; set; }

        public bool IsSelected { get; set; }     

        public bool DisplayAlone { get; set; }

        public bool DisplayFirst { get; set; }
    }
}
