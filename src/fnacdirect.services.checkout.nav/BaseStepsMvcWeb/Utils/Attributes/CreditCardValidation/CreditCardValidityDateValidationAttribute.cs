﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using StructureMap;
using FnacDirect.Context;
using FnacDirect.Contracts.Online.Model;
using FnacDirect.Customer.Model;
using FnacDirect.OrderPipe.Base.Business;
using FnacDirect.OrderPipe.Base.Business.Payment;
using FnacDirect.OrderPipe.BaseMvc.Web.Services.Pop;
using FnacDirect.Technical.Framework.Web.Mvc;
using FnacDirect.Technical.Framework.ServiceLocation;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Validations
{
    public class CreditCardValidityDateValidationAttribute : ValidationAttribute, IClientValidatable
    {
        private readonly string _propertyValidityMonth;
        private readonly string _propertyCreditCardType;
        private readonly string _propertyNumByPhone;

        private readonly IApplicationContext _applicationContext;
        private readonly IPaymentSelectionBusiness _paymentSelectionBusiness;
        private readonly SiteContext _siteContext;
        private readonly string _culture;
        private readonly string _configShippingMethodSelection;

        public CreditCardValidityDateValidationAttribute(string propertyValidityMonth, string propertyCreditCardType, string propertyNumByPhone)
        {
            _propertyValidityMonth = propertyValidityMonth;
            _propertyCreditCardType = propertyCreditCardType;
            _propertyNumByPhone = propertyNumByPhone;

            _applicationContext = ServiceLocator.Current.GetInstance<IApplicationContext>();
            _siteContext = _applicationContext.GetSiteContext();
            _culture = _siteContext != null ? _siteContext.Culture : "";
            _configShippingMethodSelection = _applicationContext.GetAppSetting("config.shippingMethodSelection");
            _paymentSelectionBusiness = ServiceLocator.Current.GetInstance<IPaymentSelectionBusiness>();
        }

        public IEnumerable<ModelClientValidationRule> GetClientValidationRules(ModelMetadata metadata, ControllerContext context)
        {
            var modelClientValidationRule = new ModelClientValidationRule
            {
                ErrorMessage = _paymentSelectionBusiness.GetErrorMessage("OP.CARD.ErrMsg.needexpdate"),
                ValidationType = "creditcardvaliditydatevalidation"
            };

            modelClientValidationRule.ValidationParameters["propertyvaliditymonth"] = "ValidityMonth";

            yield return modelClientValidationRule;
        }

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            //Check if NumByPhone is true 
            if (!string.IsNullOrEmpty(_propertyNumByPhone))
            {
                var propertyNumByPhone = validationContext.ObjectType.GetProperty(_propertyNumByPhone);

                if (propertyNumByPhone == null)
                    return ValidationResult.Success;

                var propertyNumByPhoneValue = propertyNumByPhone.GetGetMethod().Invoke(validationContext.ObjectInstance, null);

                if (!(propertyNumByPhoneValue is bool))
                    return ValidationResult.Success;

                var numByPhone = (bool)propertyNumByPhoneValue;

                if (numByPhone)
                {
                    return ValidationResult.Success;
                }

            }

            var creditCardDescriptor = new CustCreditCardDescriptor();
            var hasSelectedCreditCard = false;

            if (!string.IsNullOrEmpty(_propertyCreditCardType))
            {
                //Selected Credit Card Type 
                var propertyCreditCardTypeInfo = validationContext.ObjectType.GetProperty(_propertyCreditCardType);

                if (propertyCreditCardTypeInfo == null)
                    return ValidationResult.Success;

                var propertyCreditCardTypeValue = propertyCreditCardTypeInfo.GetGetMethod().Invoke(validationContext.ObjectInstance, null);

                if (!(propertyCreditCardTypeValue is int))
                    return ValidationResult.Success;

                var creditCardTypeId = (int)propertyCreditCardTypeValue;

                var creditCardDescriptorBusiness = ServiceLocator.Current.GetInstance<ICreditCardDescriptorBusiness>();
                creditCardDescriptor = creditCardDescriptorBusiness.GetCreditCardDescriptor(creditCardTypeId, _configShippingMethodSelection, _culture);

                if (!creditCardDescriptor.HasExpDate)
                {
                    return ValidationResult.Success;
                }

                hasSelectedCreditCard = true;
            }

            //Value : Expire Year
            if (!(value is string))
            {
                return new ValidationResult(_paymentSelectionBusiness.GetErrorMessage("OP.CARD.ErrMsg.needexpdate"));
            }

            if (!int.TryParse(value.ToString(), out var validityYear))
            {
                return new ValidationResult(_paymentSelectionBusiness.GetErrorMessage("OP.CARD.ErrMsg.needexpdate"));
            }

            if (validityYear == 0)
            {
                return new ValidationResult(_paymentSelectionBusiness.GetErrorMessage("OP.CARD.ErrMsg.needexpdate"));
            }

            //Expire Mouth
            var propertyValidityMonthInfo = validationContext.ObjectType.GetProperty(_propertyValidityMonth);

            if (propertyValidityMonthInfo == null)
                return ValidationResult.Success;

            var propertyValidityMonthValue = propertyValidityMonthInfo.GetGetMethod().Invoke(validationContext.ObjectInstance, null);

            if (!(propertyValidityMonthValue is string) && string.IsNullOrEmpty(propertyValidityMonthValue.ToString()))
                return ValidationResult.Success;

            if (!int.TryParse(propertyValidityMonthValue.ToString(), out var ValidityMonth))
            {
                return new ValidationResult(_paymentSelectionBusiness.GetErrorMessage("OP.CARD.ErrMsg.needexpdate"));
            }

            if (ValidityMonth == 0)
            {
                return new ValidationResult(_paymentSelectionBusiness.GetErrorMessage("OP.CARD.ErrMsg.needexpdate"));
            }

            return CheckCreditCardValidityDate(ValidityMonth, validityYear, creditCardDescriptor, hasSelectedCreditCard);
        }

        private ValidationResult CheckCreditCardValidityDate(int expireMonth, int expireYear, CustCreditCardDescriptor creditCardDescriptor, bool hasSelectedCreditCard)
        {
            ValidationResult validationResult;
            if (hasSelectedCreditCard)
            {
                validationResult = _paymentSelectionBusiness.CheckSipsCreditCardExpireDate(expireMonth, expireYear, creditCardDescriptor.HasExpDate);
            }
            else
            {
                validationResult = _paymentSelectionBusiness.CheckSipsCreditCardExpireDate(expireMonth, expireYear, true);
            }

            return validationResult;
        }
    }
}
