using FnacDirect.Basket.Business;
using FnacDirect.Basket.Model;
using FnacDirect.Context;
using FnacDirect.Contracts.Online.Model;
using FnacDirect.Front.WebBusiness;
using FnacDirect.OrderPipe.Base.Business;
using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.OrderPipe.Base.Model.Tracking;
using FnacDirect.OrderPipe.Base.Steps;
using FnacDirect.OrderPipe.BaseMvc.Web.Models.Tracking;
using FnacDirect.OrderPipe.BaseMvc.Web.Services.Tracking;
using FnacDirect.OrderPipe.CoreRouting;
using FnacDirect.Technical.Framework.Web;
using FnacDirect.Technical.Framework.Web.Mvc;
using FnacDirect.Technical.Framework.Web.Mvc.Security;
using FnacDirect.Technical.Framework.Web.UrlMgr;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web.Mvc;

namespace FnacDirect.OrderPipe.BaseMvc.Web.CrossSellBasket
{
    public class CrossSellBasketController : ViewEngineSpecificController<CrossSellBasketRazorViewEngine>
    {
        private const string PipeName = "basket";

        private readonly IArticleDetailService _articleDetailService;
        private readonly IBasketPersistService _basketPersistService;
        private readonly ICrossSellBasketViewModelBuilder _crossSellBasketViewModelBuilder;
        private readonly IExecutePipeService _executePipeService;
        private readonly ISwitchProvider _switchProvider;
        private readonly ITrackingService _trackingService;
        private readonly IUserContextProvider _userContextProvider;
        private readonly IW3ArticleService _w3ArticleService;

        protected UrlWriter OrderPipeUrl { get; }

        public CrossSellBasketController(IUrlManager urlManager,
                                         IUserContextProvider userContextProvider,
                                         IExecutePipeService executePipeService,
                                         IBasketPersistService basketPersistService,
                                         ICrossSellBasketViewModelBuilder crossSellBasketViewModelBuilder,
                                         IW3ArticleService w3ArticleService,
                                         ISwitchProvider switchProvider,
                                         IArticleDetailService articleDetailService,
                                         ITrackingService trackingService)
        {
            _articleDetailService = articleDetailService;
            _basketPersistService = basketPersistService;
            _crossSellBasketViewModelBuilder = crossSellBasketViewModelBuilder;
            _executePipeService = executePipeService;
            _switchProvider = switchProvider;
            _articleDetailService = articleDetailService;
            _userContextProvider = userContextProvider;
            _w3ArticleService = w3ArticleService;
            _trackingService = trackingService;
            OrderPipeUrl = urlManager.Sites.OrderPipe.GetPage("Root").DefaultUrl;

        }

        [HttpOptions]
        [CrossOriginResourceSharing(allowCredentials: true, exposeHeader: "Location")]
        public void Add() { }

        [HttpGet]
        [HasProductsQueryStringActionMethodSelector(inversion: true)]
        [CrossOriginResourceSharing(allowCredentials: true, exposeHeader: "Location")]
        public ActionResult Add(CrossSellBasketItemInput basketItem)
            => Add(new List<CrossSellBasketItemInput> { basketItem });

        [HttpPost]
        [CrossOriginResourceSharing(allowCredentials: true, exposeHeader: "Location")]
        public virtual ActionResult Add([ModelBinder(typeof(CrossSellBasketItemInputModelBinder))] List<CrossSellBasketItemInput> basketItems)
        {
            var userContext = _userContextProvider.GetCurrentUserContext();

            var normalizedItems = basketItems != null ? basketItems.Where(CanBeAdded) : Enumerable.Empty<CrossSellBasketItemInput>();

            if (!normalizedItems.Any())
            {
                return ComputeAddResult(null, Enumerable.Empty<CrossSellBasketItemInput>());
            }

            var itemIdentifiers = normalizedItems.Select(basketItem => BuildItemIdentifier(userContext, basketItem)).ToList();

            if (!_switchProvider.IsEnabled("ebook.basket.add"))
            {
                var items = itemIdentifiers.ToList();

                foreach (var item in items.Where(i => i.Type != ArticleBasketType.MPArticle))
                {
                    if (item.ItemArticleType == ItemType.Article)
                    {
                        var article = new StandardArticle
                        {
                            ProductID = item.Prid,
                            Quantity = item.Quantity
                        };

                        _articleDetailService.GetArticleDetailWithPrice(article, FnacContext.Current.SiteContext, false);

                        if (article.IsEbook)
                        {
                            itemIdentifiers.Remove(item);
                        }
                    }
                }
            }

            var executionResult = _executePipeService.ExecuteOrderPipe(PipeName);
            var orderForm = executionResult.OpContext.OF as PopOrderForm;
            var itemsInBasket = (orderForm != null) ? orderForm.LineGroups.GetArticles().Select(a => a.ProductID).ToList() : new List<int?>();

            if (!_switchProvider.IsEnabled("orderpipe.pop.fdv.enable") && orderForm != null)
            {
                foreach (var itemIdentifier in itemIdentifiers.ToList())
                {
                    if (itemIdentifier.ItemArticleType == ItemType.Service)
                    {
                        var matchingFather = orderForm.LineGroups
                            .GetArticles()
                            .OfType<StandardArticle>()
                            .FirstOrDefault(a => a.ProductID.GetValueOrDefault() == itemIdentifier.FatherId);

                        // Si le service est ajouté et que le produit maître n'est pas encore présent, on l'ajoute également
                        if (matchingFather == null)
                        {
                            if (!itemIdentifiers.Any(i => i.Type == ArticleBasketType.Standard && i.Prid == itemIdentifier.FatherId))
                            {
                                itemIdentifiers.Insert(
                                    0,
                                    BuildItemIdentifier(
                                        userContext,
                                        new CrossSellBasketItemInput { ProductId = itemIdentifier.FatherId }
                                    )
                                );
                            }
                        }
                        else if (matchingFather.Services.Any())
                        {
                            foreach (var service in matchingFather.Services)
                            {
                                _basketPersistService.Remove(
                                    BuildItemIdentifier(
                                        userContext,
                                        new CrossSellBasketItemInput
                                        {
                                            MasterProductId =1,
                                            ProductId = 1
                                        }
                                    )
                                );
                            }
                        }
                    }
                }

                orderForm.LineGroups.Clear();
            }

            var basketData = orderForm?.GetPrivateData<BasketData>(false);

            foreach (var itemIdentifier in itemIdentifiers)
            {
                if (basketData != null)
                {
                    if (
                        !basketData.HasMaxItemsInBasket ||
                        itemsInBasket.Contains(itemIdentifier.Prid) ||
                        itemIdentifier.ItemArticleType == ItemType.Service
                    )
                    {
                        _basketPersistService.AddItemTo(itemIdentifier);
                        basketData.HasLastItemInBasket = true;
                    }
                }
                else
                {
                    _basketPersistService.AddItemTo(itemIdentifier);
                }
            }

            if (!_switchProvider.IsEnabled("orderpipe.pop.fdv.enable"))
            {
                executionResult.OpContext.Pipe.Rewind(executionResult.OpContext, false);
                orderForm = executionResult.OpContext.OF as PopOrderForm;

                if (orderForm != null)
                {
                    W3ShoppingCart.IssueNbArtCookie(NumberOfArticlesInBasket(orderForm));
                }
                var trackingModel = itemIdentifiers.Select( i => new TrackingArticleModel()
                {
                    Offer = i.Offer.ToStringOrDefault(),
                    Prid = i.Prid
                }).ToList();

                _trackingService.TrackArticles(orderForm, trackingModel, EventTracking.AddBasket);
            }
            return ComputeAddResult(orderForm, normalizedItems);
        }

        [HttpGet]
        [HasProductsQueryStringActionMethodSelector]
        public ActionResult Add(string products, int shopId = 0)
        {
            var marketPlaceIdentifierRegex = new Regex(@"((\d)+)-([0-9A-F]{8}-[0-9A-F]{4}-[0-9A-F]{4}-[0-9A-F]{4}-[0-9A-F]{12})", RegexOptions.IgnoreCase);
            var fnacServiceRegex = new Regex(@"(\d+)\(((\d|\|)+)\)");

            var inputs = new List<CrossSellBasketItemInput>();

            var productInputs = products.Split(',');

            foreach (var productInput in productInputs)
            {
                var productId = 0;

                var marketPlaceMatch = marketPlaceIdentifierRegex.Match(productInput);

                if (marketPlaceMatch.Success && marketPlaceMatch.Groups.Count == 4)
                {
                    var offer = Guid.Empty;

                    if (int.TryParse(marketPlaceMatch.Groups[1].Value, out productId)
                        && Guid.TryParse(marketPlaceMatch.Groups[3].Value, out offer))
                    {
                        inputs.Add(new CrossSellBasketItemInput
                        {
                            ProductId = productId,
                            Offer = offer,
                            ShopId = shopId
                        });
                    }
                }
                else
                {
                    var fnacServiceMatch = fnacServiceRegex.Match(productInput);

                    if (fnacServiceMatch.Success && fnacServiceMatch.Groups.Count == 4)
                    {
                        var services = fnacServiceMatch.Groups[2].Value.Split('|');

                        if (int.TryParse(fnacServiceMatch.Groups[1].Value, out productId))
                        {
                            foreach (var service in services)
                            {
                                if (int.TryParse(service, out var serviceId))
                                {
                                    inputs.Add(new CrossSellBasketItemInput
                                    {
                                        MasterProductId = productId,
                                        ProductId = serviceId,
                                        ShopId = shopId
                                    });
                                }
                            }
                        }
                    }
                    else
                    {
                        if (int.TryParse(productInput, out productId))
                        {
                            inputs.Add(new CrossSellBasketItemInput
                            {
                                ProductId = productId,
                                ShopId = shopId
                            });
                        }
                    }
                }
            }

            return Add(inputs);
        }

        [HttpGet]
        public ActionResult ClickInStore(string basket)
        {
            var userContext = _userContextProvider.GetCurrentUserContext();
            var items = basket.Split(';');

            foreach (var item in items)
            {
                var parameters = item
                    .Split(',')
                    .Select(i => i.Split(':'))
                    .Where(i => !string.IsNullOrEmpty(i[1]))
                    .ToDictionary(i => i[0], i => i[1]);

                var productId = int.Parse(parameters["p"]);
                var quantity = int.Parse(parameters["q"]);

                if (parameters.ContainsKey("or"))
                {
                    _basketPersistService.AddItemTo(
                        BuildItemIdentifier(
                            userContext,
                            new CrossSellBasketItemInput
                            {
                                Offer = Guid.Parse(parameters["or"]),
                                ProductId = productId,
                                Quantity = quantity
                            }
                        )
                    );
                }
                else if (parameters.ContainsKey("pp"))
                {
                    _basketPersistService.AddItemTo(
                        BuildItemIdentifier(
                            userContext,
                            new CrossSellBasketItemInput
                            {
                                MasterProductId = int.Parse(parameters["pp"]),
                                ProductId = productId,
                                Quantity = quantity
                            }
                        )
                    );
                }
                else
                {
                    _basketPersistService.AddItemTo(
                        BuildItemIdentifier(
                            userContext,
                            new CrossSellBasketItemInput
                            {
                                ProductId = productId,
                                Quantity = quantity
                            }
                        )
                    );
                }
            }

            return Redirect(OrderPipeUrl.ToString());
        }

        [HttpOptions]
        [CrossOriginResourceSharing(allowCredentials: true, exposeHeader: "Location")]
        public void Delete() { }

        [HttpGet]
        public ActionResult Delete(CrossSellBasketItemInput basketItem)
            => Delete(new List<CrossSellBasketItemInput> { basketItem });

        [HttpPost]
        [CrossOriginResourceSharing("POST", allowCredentials: true)]
        public ActionResult Delete(IEnumerable<CrossSellBasketItemInput> basketItems)
        {
            var userContext = _userContextProvider.GetCurrentUserContext();
            var itemIdentifiers = basketItems.Select(basketItem => BuildItemIdentifier(userContext, basketItem));
            foreach (var itemIdentifier in itemIdentifiers)
            {
                _basketPersistService.Remove(itemIdentifier);
            }

            var executionResult = _executePipeService.ExecuteOrderPipe(PipeName);

            if (executionResult.OpContext.OF is PopOrderForm orderForm)
            {
                W3ShoppingCart.IssueNbArtCookie(NumberOfArticlesInBasket(orderForm));

                var trackingModels = itemIdentifiers.Select(i => new TrackingArticleModel()
                {
                    Offer = i.Offer.ToStringOrDefault(),
                    Prid = i.Prid
                }).ToList();
                _trackingService.TrackArticles(orderForm, trackingModels, EventTracking.RemoveBasket);
            }

            if (Request.IsAjaxRequest())
            {
                return new EmptyResult();
            }

            return Redirect(OrderPipeUrl.ToString());
        }

        [HttpGet]
        public ActionResult IntermediaryBasketLegacy()
        {
            var offer = Guid.Empty;

            if (Request.QueryString["prid"] == null || !int.TryParse(Request.QueryString["prid"], out var productId))
            {
                return Redirect(OrderPipeUrl.ToString());
            }

            if (Request.QueryString["offer"] != null)
            {
                Guid.TryParse(Request.QueryString["offer"], out offer);
            }

            var basketItem = new CrossSellBasketItemInput
            {
                Offer = offer,
                ProductId = productId
            };

            return Add(new List<CrossSellBasketItemInput>() { basketItem });
        }

        [HttpGet]
        public ActionResult IntermediaryBasketOnlyRedirectToOrderPipe()
            => Redirect(OrderPipeUrl.ToString());

        [HttpOptions]
        [CrossOriginResourceSharing(allowCredentials: true, exposeHeader: "Location")]
        public void Update() { }

        [HttpPost]
        [CrossOriginResourceSharing("POST", allowCredentials: true)]
        public ActionResult Update(IEnumerable<CrossSellBasketItemInput> basketItems)
        {
            var userContext = _userContextProvider.GetCurrentUserContext();

            foreach (var itemIdentifier in basketItems.Select(basketItem => BuildItemIdentifier(userContext, basketItem)))
            {
                _basketPersistService.AddItemTo(itemIdentifier);
            }

            var executionResult = _executePipeService.ExecuteOrderPipe(PipeName);

            if (executionResult.OpContext.OF is PopOrderForm popOrderForm)
            {
                W3ShoppingCart.IssueNbArtCookie(NumberOfArticlesInBasket(popOrderForm));
            }

            if (Request.IsAjaxRequest())
            {
                return new EmptyResult();
            }

            return Redirect(OrderPipeUrl.ToString());
        }

        protected virtual ActionResult ComputeAddResult(PopOrderForm orderForm, IEnumerable<CrossSellBasketItemInput> basketItems)
        {
            if (Request.IsAjaxRequest())
            {
                if (orderForm != null)
                {
                    var crossSellBasketViewModel = _crossSellBasketViewModelBuilder.Build(basketItems, orderForm);

                    if (crossSellBasketViewModel != null)
                    {
                        var acceptHeader = Request.Headers["Accept"];
                        if (!string.IsNullOrEmpty(acceptHeader) && acceptHeader.StartsWith("application/json"))
                        {
                            return Json(crossSellBasketViewModel);
                        }

                        return PartialView("_Add", crossSellBasketViewModel);
                    }
                }

                return new System.Web.Mvc.HttpUnauthorizedResult(OrderPipeUrl.ToString());
            }

            return Redirect(OrderPipeUrl.ToString());
        }

        private ItemIdentifier BuildItemIdentifier(IUserContext userContext, CrossSellBasketItemInput basketItem)
        {
            var itemArticleType = ItemType.Article;
            int? shopid = null;

            if (basketItem.MasterProductId != 0)
            {
                itemArticleType = ItemType.Service;
            }

            if (basketItem.ShopId != 0)
            {
                shopid = basketItem.ShopId;
            }

            var quantity = basketItem.Quantity;

            var identifier = new ItemIdentifier
            {
                FatherId = basketItem.MasterProductId,
                IsIdentified = userContext.IsIdentified,
                ItemArticleType = itemArticleType,
                Prid = basketItem.ProductId,
                Quantity = quantity,
                ShopId = shopid,
                SID = userContext.GetSessionId().ToString(),
                Type = ArticleBasketType.Standard,
                UID = userContext.GetUserGuid().ToString()
            };

            if (basketItem.Offer != Guid.Empty)
            {
                identifier.Offer = basketItem.Offer;
                identifier.Type = ArticleBasketType.MPArticle;
            }

            return identifier;
        }

        private bool CanBeAdded(CrossSellBasketItemInput item)
        {
            if (item.ProductId == 0)
            {
                return false;
            }

            if (item.MasterProductId == 0 && item.Offer == Guid.Empty)
            {
                var w3Article = _w3ArticleService.GetW3Article(new ArticleReference { PRID = item.ProductId }, true, false);

                return w3Article != null;
            }

            return true;
        }

        private int NumberOfArticlesInBasket(PopOrderForm orderForm)
        {
            var quantity = orderForm.LineGroups.GetArticles().Sum(a => a.Quantity.GetValueOrDefault())
                - orderForm.LineGroups.GetArticles().OfType<StandardArticle>().Where(a => a.Hidden).Sum(a => a.Quantity.GetValueOrDefault())
                + orderForm.LineGroups.GetArticles().OfType<StandardArticle>().Where(a => a.Services != null && a.Services.Any()).SelectMany(a => a.Services).Sum(service => service.Quantity.GetValueOrDefault());

            return quantity;
        }
    }
}
