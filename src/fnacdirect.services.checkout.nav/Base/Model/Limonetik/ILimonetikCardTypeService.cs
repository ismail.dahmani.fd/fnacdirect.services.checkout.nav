using System.Collections.Generic;

namespace FnacDirect.OrderPipe.Base.Proxy.Limonetik
{
    public interface ILimonetikCardTypeService
    {
        List<int> GetLimonetikCardTypeIds();
    }
}
