namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.OnePage.Address
{
    public enum AddressFormViewModelMode
    {
        Shipping,
        Billing
    }

    public class AddressFormViewModel
    {
        public AddressFormViewModelMode AddressFormViewModelMode { get; private set; }

        public AddressViewModel AddressViewModel { get; private set; }

        public QasViewModel QasViewModel { get; private set; }

        public bool IsAddressHasBeenAlreadyChosen { get; set; }

        public bool IsEligibleForDependingSellers { get; set; }

        public bool IsGuest { get; set; }

        public int SellerId { get; set; }

        public AddressFormViewModel(AddressFormViewModelMode addressFormViewModelMode, AddressViewModel addressViewModel)
            : this(addressFormViewModelMode, addressViewModel,new QasViewModel())
        {
           
        }

        public AddressFormViewModel(AddressFormViewModelMode addressFormViewModelMode, AddressViewModel addressViewModel, QasViewModel qasViewMode)
        {
            AddressFormViewModelMode = addressFormViewModelMode;
            AddressViewModel = addressViewModel;
            QasViewModel = qasViewMode;
            IsEligibleForDependingSellers = true;
            SellerId = AddressViewModel?.SellerId ?? SellerId;
        }
    }
}
