﻿using FnacDirect.Basket.Business;
using FnacDirect.Context;
using FnacDirect.ShoppingCartEntities;
using FnacDirect.Technical.Framework.Web;
using FnacDirect.Technical.Framework.Web.Mvc;
using System.Web.Mvc;

namespace FnacDirect.OrderPipe.BaseMvc.Web.CrossSellBasket
{
    public class BasketManagementController : ViewEngineSpecificController<CrossSellBasketRazorViewEngine>
    {
        private readonly IUrlManager _urlManager;
        private readonly IBasketPersistService _basketPersistService;
        private readonly IUserContextProvider _userContextProvider;

        private readonly string _orderPipeUrl;

        private const string PipeName = "basket";

        public BasketManagementController(IUrlManager urlManager, 
                                          IBasketPersistService basketPersistService,
                                          IUserContextProvider userContextProvider)
        {
            _urlManager = urlManager;
            _orderPipeUrl = _urlManager.Sites.OrderPipe.GetPage("Root").DefaultUrl.ToString();
            _userContextProvider = userContextProvider;
            _basketPersistService = basketPersistService;
        }

        [HttpGet]
        public ActionResult Clean(string id)
        {
            if(string.IsNullOrEmpty(id))
            {
                var userContext = _userContextProvider.GetCurrentUserContext();

                _basketPersistService.BasketBus.Clear(ShoppingCartType.StandardBasket, userContext.GetSessionId().ToString());
                _basketPersistService.BasketBus.Clear(ShoppingCartType.StandardBasket, userContext.GetUserGuid().ToString());

            }
            else
            {
                _basketPersistService.BasketBus.Clear(ShoppingCartType.StandardBasket, id);
            }

            return Redirect(_orderPipeUrl);
        }
    }
}
