﻿using System.Collections.ObjectModel;

namespace FnacDirect.OrderPipe.Config
{
    public class MvcSlotList : KeyedCollection<string, MvcSlot>
    {
        protected override string GetKeyForItem(MvcSlot item)
        {
            return item.Name;
        }
    }
}