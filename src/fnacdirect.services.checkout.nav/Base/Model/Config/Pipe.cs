using System.Collections.Generic;
using System.ComponentModel;
using System.Xml.Serialization;
using FnacDirect.OrderPipe.Core.Model.Config;

namespace FnacDirect.OrderPipe.Config
{
	public class Pipe
	{
		private string _name = "Default";

		[XmlAttribute("name")]
		[DefaultValue("Default")]
		public string Name
		{
			get { return _name; }
			set { _name = value; }
		}

		public string DefaultAssembly { get; set; }

		public string OrderFormClass { get; set; }

		public UIPipeDescriptor UIDescriptor { get; set; }

		[XmlArrayItem(Type = typeof(Step), ElementName = "Step")]
		[XmlArrayItem(Type = typeof(InteractiveStep), ElementName = "InteractiveStep")]
		[XmlArrayItem(Type = typeof(BeginTransactionStep), ElementName = "BeginTransaction")]
		[XmlArrayItem(Type = typeof(EndTransactionStep), ElementName = "EndTransaction")]
		[XmlArrayItem(Type = typeof(SetErrorForwardStep), ElementName = "SetErrorOnForward")]
		[XmlArrayItem(Type = typeof(GroupStep), ElementName = "GroupStep")]
		public List<Core.Model.Config.BaseStep> Steps { get; set; }
        
		[XmlArray("Parameters")]
		[XmlArrayItem("Parameter")]
		public ParameterList Parameters { get; set; }
	}
}
