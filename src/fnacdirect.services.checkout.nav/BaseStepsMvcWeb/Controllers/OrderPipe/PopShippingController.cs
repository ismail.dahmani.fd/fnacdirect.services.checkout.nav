using FnacDirect.OrderPipe.Base.Business.Shipping.Relay;
using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.OrderPipe.Base.Model.Adherent;
using FnacDirect.OrderPipe.Base.Model.Pop;
using FnacDirect.OrderPipe.Base.Steps.Pop;
using FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Common.Shipping;
using FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.DataLayer;
using FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Omniture;
using FnacDirect.OrderPipe.BaseMvc.Web.Pop.Models.Shipping;
using FnacDirect.OrderPipe.BaseMvc.Web.Services.Pop.DataLayer;
using FnacDirect.OrderPipe.BaseMvc.Web.Services.Pop.Shipping;
using FnacDirect.Technical.Framework.Web.Mvc;
using System.Globalization;
using System.Linq;
using System.Web.Mvc;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Controllers.Pop
{
    public class PopShippingController : PopBaseController
    {
        private readonly OPContext _opContext;
        private readonly IAddressSelectorService _addressSelectorService;
        private readonly IRelayCellphoneService _relayCellphoneService;
        private readonly IShippingViewModelBuilder _shippingViewModelBuilder;
        private readonly IShippingPageRootViewModelBuilder _shippingPageRootViewModelBuilder;
        private readonly IOmnitureViewModelBuilder _omnitureViewModelBuilder;
        private readonly IApplicationContext _applicationContext;
        private readonly IDataLayerService _dataLayerService;

        public PopShippingController(OPContext opContext,
                                     IShippingViewModelBuilder shippingViewModelBuilder,
                                     IAddressSelectorService addressSelectorService,
                                     IRelayCellphoneService relayCellphoneService,
                                     IOmnitureViewModelBuilder omnitureViewModelBuilder,
                                     IApplicationContext applicationContext,
                                     IShippingPageRootViewModelBuilder shippingPageRootViewModelBuilder,
                                     IDataLayerService dataLayerService
            )
        {
            _opContext = opContext;
            _shippingViewModelBuilder = shippingViewModelBuilder;
            _addressSelectorService = addressSelectorService;
            _relayCellphoneService = relayCellphoneService;
            _omnitureViewModelBuilder = omnitureViewModelBuilder;
            _applicationContext = applicationContext;
            _shippingPageRootViewModelBuilder = shippingPageRootViewModelBuilder;
            _dataLayerService = dataLayerService;
        }

        [HttpGet]
        public ActionResult Index(int sellerIndex)
        {
            var popOrderForm = _opContext.OF as PopOrderForm;

            var popData = popOrderForm.GetPrivateData<PopData>(create: false);
            var popShippingData = popOrderForm.GetPrivateData<PopShippingData>();
            var popMembershipData = popOrderForm.GetPrivateData<PopMembershipData>(false);

            var popShippingSellerData = popShippingData.GetCurrentShippingSellerData();

            if (popShippingSellerData == null
                || sellerIndex != popShippingData.Sellers.IndexOf(popShippingSellerData))
            {
                return null;
            }

            var rootViewModel = _shippingPageRootViewModelBuilder.GetRootViewModel(popOrderForm, popShippingSellerData.SellerId, (a, b) => Url.RouteUrl(a, b));

            if (popMembershipData != null && popMembershipData.DisplayNotification)
            {
                popMembershipData.NotificationHasBeenDisplayed = true;

                Rewind();
            }

            if (rootViewModel.Shipping != null)
            {
                rootViewModel.Shipping.HasManyEligibleBillingAddresses = popShippingData.HasManyEligibleBillingAddresses;
            }

            if (popData != null && popData.UseReact)
            {
                var basketAmount = rootViewModel.PaymentSummary.TotalGlobal;

                // TODO remove it after DataLayer is definitively used:
                rootViewModel.Tracking = _omnitureViewModelBuilder.BuildTracking(popOrderForm, TrackedPage.popShipping, basketAmount);

                var trackingModel = new DatalayerViewModel()
                {
                    PageType = PageTypeEnum.PopShipping,
                    PopOrderForm = popOrderForm,
                };

                var viewResult = View("_Poc/Index", rootViewModel);

                //Datalayer and Tracking switchabled
                _dataLayerService.ComputeShoppingCart(trackingModel, viewResult as ViewResultBase, ControllerContext);

                return viewResult;
            }

            return View(rootViewModel);
        }

        [HttpPost]
        [ActionName("Index")]
        public ActionResult Validate(int sellerIndex, string relayCellphone)
        {
            var popOrderForm = _opContext.OF as PopOrderForm;

            var popShippingData = popOrderForm.GetPrivateData<PopShippingData>();

            var popShippingSellerData = popShippingData.GetCurrentShippingSellerData();

            if (!string.IsNullOrEmpty(relayCellphone) && !_relayCellphoneService.SetRelayCellPhone(relayCellphone, popOrderForm))
            {
                var rootViewModel = _shippingPageRootViewModelBuilder.GetRootViewModel(popOrderForm, popShippingSellerData.SellerId, (a, b) => Url.RouteUrl(a, b));

                if (rootViewModel.Shipping.Relay.HasValue && rootViewModel.Shipping.Relay.Value.Selected.HasValue)
                {
                    rootViewModel.Shipping.Relay.Value.Selected.Value.CellPhoneNumber = relayCellphone;
                }

                return View("Index", rootViewModel);
            }

            if (popShippingData.Sellers.IndexOf(popShippingSellerData) != sellerIndex)
            {
                return RewindAndRedirect();
            }

            var sellerId = popShippingSellerData.SellerId;

            var isValid = _shippingViewModelBuilder.IsValid(popOrderForm, sellerId);

            if (!isValid)
            {
                ModelState.AddModelError(string.Empty, "Vous devez sélectionner un mode de livraion valide.");

                var rootViewModel = _shippingPageRootViewModelBuilder.GetRootViewModel(popOrderForm, popShippingSellerData.SellerId, (a, b) => Url.RouteUrl(a, b));

                return View("Index", rootViewModel);
            }

            var nextSellers = popShippingData.Sellers.Where(seller => !seller.ShippingChoiceValidated && popShippingData.Sellers.IndexOf(seller) > sellerIndex).Select(s => s.SellerId);
            if (nextSellers.Any())
            {
                _addressSelectorService.SelectAddressForSellers(_opContext, popOrderForm, sellerId, nextSellers);
            }
            popShippingSellerData.UserValidated = true;
            popShippingSellerData.ShippingChoiceValidated = true;
            popShippingSellerData.Forced = false;
            popShippingSellerData.UserValidating = true;

            return Continue();
        }
    }
}
