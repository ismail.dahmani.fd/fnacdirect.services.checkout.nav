﻿namespace FnacDirect.OrderPipe.Base.Proxy
{
    public interface ICustomerContextProvider
    {
        ICustomerContext GetCurrentCustomerContext();
    }
}
