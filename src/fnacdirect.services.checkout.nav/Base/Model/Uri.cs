﻿using System.Xml.Serialization;

namespace FnacDirect.OrderPipe.Config
{
    public class Uri
    {
        [XmlAttribute("site")]
        public string Site { get; set; }
        [XmlAttribute("page")]
        public string Page { get; set; }
        [XmlAttribute("folder")]
        public string Folder { get; set; }
        [XmlAttribute("path")]
        public string Path { get; set; }
    }
}
