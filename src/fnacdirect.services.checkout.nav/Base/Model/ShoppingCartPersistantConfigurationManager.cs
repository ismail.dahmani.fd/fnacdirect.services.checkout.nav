using FnacDirect.Front.WebBusiness.Configuration;
using FnacDirect.IdentityImpersonation;

namespace FnacDirect.OrderPipe.Base.Business
{
    public class ShoppingCartPersistantConfigurationManager
    {
        public static bool IsActive()
        {
            if (!Technical.Framework.Switch.Switch.IsEnabled("shoppingCart.persistant"))
            {
                return false;
            }

            var identityImpersonator =
              IdentityImpersonator.GetIdentityImpersonatorFromCookie(System.Web.HttpContext.Current.Request.Cookies[WebSiteConfiguration.Value.IdentityImpersonator.CookieName],
                                                                    WebSiteConfiguration.Value.IdentityImpersonator.Base64Key);

            if (identityImpersonator != null)
            {
                if (!Technical.Framework.Switch.Switch.IsEnabled("shoppingCart.persistantForCallCenter"))
                {
                    return false;
                }
            }

            return true;
        }
    }

    public interface IShoppingCartPersistantConfigurationService
    {
        bool IsActive { get; }
    }

    public class ShoppingCartPersistantConfigurationService : IShoppingCartPersistantConfigurationService
    {
        public bool IsActive
        {
            get
            {
                return ShoppingCartPersistantConfigurationManager.IsActive();
            }
        }
    }
}
