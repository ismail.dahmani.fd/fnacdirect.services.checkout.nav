using System.Linq;
using FnacDirect.Technical.Framework.Web.Mvc;
using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.Contracts.Online.Model;
using FnacDirect.Context;
using FnacDirect.Contracts.Online.IModel;
using FnacDirect.OrderPipe.Base.Business;
using FnacDirect.OrderPipe.BaseMvc.Web.Helpers;
using FnacDirect.Technical.Framework.CoreServices;
using System.Collections.Generic;
using FnacDirect.Technical.Framework.Web;
using FnacDirect.Technical.Framework.Web.Configuration;
using System;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Basket
{
    public class PriceViewModelBuilder : IPriceViewModelBuilder
    {
        private readonly IApplicationContext _applicationContext;
        private readonly IPriceHelper _priceHelper;
        private readonly IUserContextProvider _userContextProvider;
        private readonly IArticleDetailService _articleDetailService;
        private readonly ISiteManagerBusiness2 _siteManagerBusiness2;
        private readonly IMarketplacePromotionBusiness _marketplacePromotionBusiness;
        private readonly IPromotionTemplateViewModelBuilder _promotionTemplateViewModelBuilder;
        protected readonly ISwitchProvider _switchProvider;
        private readonly IArticleBusiness3 _articleBusiness3; 

        public PriceViewModelBuilder(IApplicationContext applicationContext,
                                     IUserContextProvider userContextProvider,
                                     IArticleDetailService articleDetailService,
                                     ISiteManagerBusiness2 siteManagerBusiness2,
                                     IMarketplacePromotionBusiness marketplacePromotionBusiness,
                                     IPromotionTemplateViewModelBuilder promotionTemplateViewModelBuilder,
                                     ISwitchProvider switchProvider,
                                     IArticleBusiness3 articleBusiness3)
        {
            _applicationContext = applicationContext;
            _priceHelper = applicationContext.GetPriceHelper();
            _userContextProvider = userContextProvider;
            _articleDetailService = articleDetailService;
            _siteManagerBusiness2 = siteManagerBusiness2;
            _marketplacePromotionBusiness = marketplacePromotionBusiness;
            _promotionTemplateViewModelBuilder = promotionTemplateViewModelBuilder;
            _switchProvider = switchProvider;
            _articleBusiness3 = articleBusiness3; 
        }

        public virtual PriceViewModel Build(BaseArticle baseArticle, bool applyAdherentPromotion)
        {
            return Build(baseArticle, applyAdherentPromotion, PriceFormat.CurrencyAsDecimalSeparator);
        }

        protected PriceViewModel Build(BaseArticle baseArticle, bool applyAdherentPromotion, PriceFormat priceFormat)
        {
            var isFnacPro = _applicationContext.GetSiteId() == (int)FnacSites.FNACPRO;

            var priceViewModel = new PriceViewModel();

            if (baseArticle.Type == Base.Model.ArticleType.Free)
            {
                priceViewModel.Free = true;
                priceViewModel.Price = _priceHelper.Format(0);
                return priceViewModel;
            }

            var oldPrice = baseArticle.PriceDBEur.GetValueOrDefault();
            var price = baseArticle.PriceUserEur.GetValueOrDefault();
            var priceNoVat = baseArticle.PriceNoVATEur.GetValueOrDefault();
            decimal? oldPriceNoVat = null;

            if (baseArticle.SalesInfo != null && baseArticle.SalesInfo.StandardPrice != null)
            {
                oldPriceNoVat = baseArticle.SalesInfo.StandardPrice.HT;
            }

            priceViewModel.HasRCP = baseArticle.HasRCP;

            if (baseArticle is StandardArticle)
            {
                var standardArticle = baseArticle as StandardArticle;

                if (standardArticle.IsDematSoft || standardArticle.IsNumerical)
                {
                    priceViewModel.Numerical = true;
                }

                var copyTax = string.Empty;

                if (priceViewModel.HasRCP)
                {
                    copyTax = _priceHelper.Format(standardArticle.RCPTax.Value * baseArticle.Quantity.GetValueOrDefault(), PriceFormat.Default);
                }

                var ecoTax = string.Empty;
                var ecoTaxNoVat = string.Empty;

                if (standardArticle.EcoTaxEur.HasValue)
                {
                    priceViewModel.HasEcoTax = true;
                    ecoTax = _priceHelper.Format(standardArticle.EcoTaxEur.Value * baseArticle.Quantity.GetValueOrDefault(), PriceFormat.Default);

                    price += standardArticle.EcoTaxEur.Value;
                    oldPrice += standardArticle.EcoTaxEur.Value;
                }

                if (standardArticle.EcoTaxEurNoVAT.HasValue)
                {
                    priceViewModel.HasEcoTax = true;
                    ecoTaxNoVat = _priceHelper.Format(standardArticle.EcoTaxEurNoVAT.Value * baseArticle.Quantity.GetValueOrDefault(), PriceFormat.Default);

                    priceNoVat += standardArticle.EcoTaxEurNoVAT.Value;
                    oldPriceNoVat += standardArticle.EcoTaxEurNoVAT.Value;
                }

                if (priceViewModel.HasEcoTax && priceViewModel.HasRCP)
                {
                    priceViewModel.EcoTaxAndCopyTaxLabel = _applicationContext.GetMessage("orderpipe.pop.basket.price.ecotaxcopytax", ecoTax, copyTax);
                    priceViewModel.EcoTaxNoVatAndCopyTaxLabel = _applicationContext.GetMessage("orderpipe.pop.basket.price.ecotaxcopytax", ecoTaxNoVat, copyTax);
                }
                else if (priceViewModel.HasEcoTax)
                {
                    priceViewModel.EcoTaxAndCopyTaxLabel = _applicationContext.GetMessage("orderpipe.pop.basket.price.ecotax", ecoTax);
                    priceViewModel.EcoTaxNoVatAndCopyTaxLabel = _applicationContext.GetMessage("orderpipe.pop.basket.price.ecotax", ecoTaxNoVat);
                }
                else if (priceViewModel.HasRCP)
                {
                    priceViewModel.EcoTaxAndCopyTaxLabel = _applicationContext.GetMessage("orderpipe.pop.basket.price.copytax", copyTax);
                }

                if (standardArticle.SalesInfo?.Promotions?.Count > 0)
                {
                    if (_switchProvider.IsEnabled("orderpipe.pop.promo.usetemplate"))
                    {
                        var templates = _promotionTemplateViewModelBuilder.Build(standardArticle.SalesInfo?.Promotions);

                        foreach (var template in templates)
                        {
                            priceViewModel.WithTemplate(template);
                        }
                    }
                    else
                    {
                        var usages = standardArticle.SalesInfo.Promotions.Select(p => p.Usage).Where(u => u != PromotionUsage.Undefined).Distinct().ToList();

                        foreach (var usage in usages)
                        {
                            priceViewModel.WithUsage(new PromotionViewModel()
                            {
                                Usage = usage,
                                Push = ComputePromotionPush(usage)
                            });
                        }
                    }

                    foreach (var promotion in standardArticle.SalesInfo.Promotions)
                    {
                        if (!string.IsNullOrEmpty(promotion.ShoppingCartText))
                        {
                            priceViewModel.WithPromotion(promotion.ShoppingCartText);
                        }
                    }


                }
            }
            else if (baseArticle is MarketPlaceArticle marketPlaceArticle)
            {
                if (marketPlaceArticle.SalesInfo?.Promotions != null)
                {
                    ApplyPromotionsOnPriceViewModel(priceViewModel, marketPlaceArticle);
                }

                if (marketPlaceArticle.ShippingPromotionRules != null)
                {
                    foreach (var shippingPromotionRule in marketPlaceArticle.ShippingPromotionRules)
                    {
                        if (shippingPromotionRule.Promotion != null
                            && !string.IsNullOrEmpty(shippingPromotionRule.Promotion.ShoppingCartText))
                        {
                            priceViewModel.WithPromotion(shippingPromotionRule.Promotion.ShoppingCartText);
                        }
                    }
                }


                var hasPromoAdhe = marketPlaceArticle.Offer.Promotions.Any(p => p.PromoTypeId == 1);

                if (hasPromoAdhe && applyAdherentPromotion)
                {
                    priceViewModel.WithUsage(new PromotionViewModel
                    {
                        Usage = PromotionUsage.Member,
                        Push = ComputePromotionPush(PromotionUsage.Member)
                    });
                }
            }

            price = price * baseArticle.Quantity.GetValueOrDefault();

            oldPrice = oldPrice * baseArticle.Quantity.GetValueOrDefault();

            priceViewModel.Price = _priceHelper.Format(price, priceFormat);
            priceViewModel.PriceModel = _priceHelper.GetDetails(price);

            priceNoVat = priceNoVat * baseArticle.Quantity.GetValueOrDefault();
            oldPriceNoVat = oldPriceNoVat * baseArticle.Quantity.GetValueOrDefault();
            priceViewModel.PriceNoVat = _priceHelper.Format(priceNoVat, priceFormat);
            priceViewModel.PriceNoVatModel = _priceHelper.GetDetails(priceNoVat);

            priceViewModel.PriceModelToDisplay = isFnacPro ? _priceHelper.GetDetails(priceNoVat) : _priceHelper.GetDetails(price);

            if (baseArticle is Service)
            {
                SetServiceMonthlyPrices(baseArticle, isFnacPro, priceViewModel);
            }

            priceViewModel.Free = price == 0;

            if (oldPrice != price)
            {
                priceViewModel.OldPrice = _priceHelper.Format(oldPrice, priceFormat);
                priceViewModel.OldPriceModel = _priceHelper.GetDetails(oldPrice);
            }

            if (oldPriceNoVat.HasValue && oldPriceNoVat.Value != priceNoVat)
            {
                priceViewModel.OldPriceNoVat = _priceHelper.Format(oldPriceNoVat.Value, priceFormat);
                priceViewModel.OldPriceNoVatModel = _priceHelper.GetDetails(oldPriceNoVat.Value);
            }

            priceViewModel.OldPriceModelToDisplay = isFnacPro ? priceViewModel.OldPriceNoVatModel : priceViewModel.OldPriceModel;

            BuildD3E(priceViewModel, baseArticle);
            BuildDEA(priceViewModel, baseArticle);
            BuildRCP(priceViewModel, baseArticle);

            return priceViewModel;
        }

        #region privates

        protected virtual void SetServiceMonthlyPrices(BaseArticle baseArticle, bool isFnacPro, PriceViewModel priceViewModel)
        {
            priceViewModel.MonthlyPrice = GetMonthlyPrice(baseArticle);
            priceViewModel.MonthlyPriceNoVAT = GetMonthlyPrice(baseArticle, false);
            priceViewModel.MonthlyPriceModelToDisplay = GetMonthlyPriceModelToDisplay(baseArticle, isFnacPro);
        }

        protected virtual void ApplyPromotionsOnPriceViewModel(PriceViewModel priceViewModel, MarketPlaceArticle marketPlaceArticle)
        {
            foreach (var promotion in marketPlaceArticle.SalesInfo.Promotions)
            {
                var push = _marketplacePromotionBusiness.GetPromotionPush(promotion,
                                                                          _applicationContext.GetLocale(),
                                                                          FnacContext.Current.ConfigurationOpcPush.Select(o => o.Value.ToMarketPlacePromotionPush()).ToList());

                if (push.DisplayTemplate != PromotionTemplate.Undefined
                    && push.Visibility.Contains(OnlineVisibilityPlace.PIPE))
                {
                    priceViewModel.WithTemplate(new PromotionTemplateViewModel
                    {
                        CssClass = push.CssClass,
                        Label = push.ShortLabel,
                        IsFirst = !priceViewModel.Templates.Any(),
                        ShoppingCartText = promotion.ShoppingCartText
                    });
                }
            }
        }

        private string ComputePromotionPush(PromotionUsage usage)
        {
            if (usage == PromotionUsage.PrivateSale && _userContextProvider.GetCurrentUserContext().IsIdentified)
            {
                return _applicationContext.TryGetMessage(string.Format("orderpipe.pop.basket.price.promotion.push.{0}.identified", (int)usage));
            }
            return _applicationContext.TryGetMessage(string.Format("orderpipe.pop.basket.price.promotion.push.{0}", (int)usage));
        }

        private string GetMonthlyPrice(BaseArticle baseArticle, bool withVAT = true)
        {
            var context = new FnacDirect.Contracts.Online.Model.Context()
            {
                SiteContext = _applicationContext.GetSiteContext()
            };

            var isArticleDTO = _articleDetailService.GetArticleDetail(baseArticle as StandardArticle, _applicationContext.GetSiteContext());
            if (isArticleDTO && baseArticle.ArticleDTO != null)
            {
                var articleReference = new ArticleReference(baseArticle.ProductID.GetValueOrDefault(), null, null, null, ArticleCatalog.FnacDirect);
             
                var article = _articleBusiness3.GetArticle(_applicationContext.GetSiteContext(), articleReference);

                var salesArticle = _siteManagerBusiness2.GetSalesArticles(context, new List<ArticleReference> { article.Reference }, false).FirstOrDefault();

                var price = withVAT ? salesArticle.SalesInfo.ReducedPrice.TTC : salesArticle.SalesInfo.ReducedPrice.HT;

                return _priceHelper.Format(article.ContractValidity.ToMonthlyPrice(price) * baseArticle.Quantity ?? 1);
            }

            return _priceHelper.Format(0);
        }

        private PriceDetailsViewModel GetMonthlyPriceModelToDisplay(BaseArticle baseArticle, bool isFnacPro)
        {
            var context = new FnacDirect.Contracts.Online.Model.Context()
            {
                SiteContext = _applicationContext.GetSiteContext()
            };

            var isArticleDTO = _articleDetailService.GetArticleDetail(baseArticle as StandardArticle, _applicationContext.GetSiteContext());
            if (isArticleDTO && baseArticle.ArticleDTO != null)
            {
                var articleReference = new ArticleReference(baseArticle.ProductID.GetValueOrDefault(), null, null, null, ArticleCatalog.FnacDirect);
          
                var article = _articleBusiness3.GetArticle(_applicationContext.GetSiteContext(), articleReference);

                var salesArticle = _siteManagerBusiness2.GetSalesArticles(context, new List<ArticleReference> { article.Reference }, false).FirstOrDefault();

                var price = isFnacPro ? salesArticle.SalesInfo.ReducedPrice.TTC : salesArticle.SalesInfo.ReducedPrice.HT;

                return _priceHelper.GetDetails(article.ContractValidity.ToMonthlyPrice(price) * baseArticle.Quantity ?? 1);
            }
            return _priceHelper.GetDetails(0);
        }

        private void BuildD3E(PriceViewModel priceViewModel, BaseArticle baseArticle)
        {
            if (baseArticle.D3ETax.HasValue)
            {
                priceViewModel.DeeePrice = _priceHelper.GetDetails(baseArticle.D3ETax.GetValueOrDefault());
            }
        }

        private void BuildDEA(PriceViewModel priceViewModel, BaseArticle baseArticle)
        {
            if (baseArticle.DEATax.HasValue)
            {
                priceViewModel.DeaPrice = _priceHelper.GetDetails(baseArticle.DEATax.GetValueOrDefault());
            }
        }

        private void BuildRCP(PriceViewModel priceViewModel, BaseArticle baseArticle)
        {
            if (baseArticle.RCPTax.HasValue)
            {
                priceViewModel.RCPPrice = _priceHelper.GetDetails(baseArticle.RCPTax.GetValueOrDefault());
            }
        }

        #endregion

    }
}
