using Common.Logging;
using FnacDirect.OrderPipe.Base.BaseModel.Model;
using FnacDirect.OrderPipe.Base.LineGroups;
using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.OrderPipe.Base.Model.FDV;
using FnacDirect.OrderPipe.Base.Model.SelfCheckout;
using FnacDirect.OrderPipe.Base.Model.ThankYou;
using FnacDirect.OrderPipe.Core.Services;
using FnacDirect.OrderPipe.FDV.Model;
using FnacDirect.Technical.Framework.Web.Mvc;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;

namespace FnacDirect.OrderPipe.Base.Business
{
    public class SelfCheckoutService : ISelfCheckoutService
    {
        private readonly IShopTicketBuilderService _shopTicketBuilderService;
        private readonly IPipeParametersProvider _pipeParametersProvider;

        public SelfCheckoutService(IShopTicketBuilderService shopTicketBuilderService,
            IPipeParametersProvider pipeParametersProvider)
        {
            _shopTicketBuilderService = shopTicketBuilderService;
            _pipeParametersProvider = pipeParametersProvider;
        }

        public void ResetSelfCheckoutOrderForm(PopOrderForm orderForm, bool resetPrivateData)
        {
            var lineGroupsToRemove = orderForm.LineGroups.ToList();

            foreach (var lg in lineGroupsToRemove)
            {
                orderForm.RemoveLineGroup(lg);
            }

            var logisticLineGroupsToRemove = orderForm.LogisticLineGroups.ToList();

            foreach (var llg in logisticLineGroupsToRemove)
            {
                orderForm.RemoveLogisticLineGroup(llg);
            }

            orderForm.ClearBillingMethods();
            orderForm.MainBillingMethod = null;

            var selfCheckoutData = orderForm.GetPrivateData<SelfCheckoutData>(create: false);
            if (selfCheckoutData != null)
            {
                selfCheckoutData.CCVRequirementDone = false;
                selfCheckoutData.DisplaySelfCheckoutValidated = false;
            }

            if (resetPrivateData)
            {
                var savedGroupDatas = orderForm.PrivateData
                    .Where(x => x is SelfCheckoutData
                        || x is ThankYouOrderDetailData
                        || x is TrackingData
                        || x is DirectLinkData)
                    .ToList();
                orderForm.PrivateData.Clear();
                foreach (var savedGroupData in savedGroupDatas)
                {
                    orderForm.PrivateData.Add(savedGroupData);
                }
            }
        }

        public ShopTicket CreateShopTicketFromSelfCheckoutOrderForm(PopOrderForm popOrderForm)
        {
            var shopTicket = new ShopTicket();

            var shopInfoData = popOrderForm.GetPrivateData<ShopInfosData>(create: false);
            shopTicket.ShopName = shopInfoData.Store.Name;
            shopTicket.ShopTel = shopInfoData.Store.Phone;

            var numCaissier = _pipeParametersProvider.Get<string>("numCaissier");

            if (!string.IsNullOrWhiteSpace(numCaissier))
            {
                shopTicket.NumCaissier = numCaissier.PadLeft(4, '0');
            }

            shopTicket.RefUg = shopInfoData.Store.RefUG.ToString("0000");
            shopTicket.DateTicket = DateTime.Now;

            var intraMagPELogisticLineGroup = popOrderForm.LogisticLineGroups.Where(lg => lg.IsIntramagPE);
            shopTicket.Orders = _shopTicketBuilderService.GetIntraMagPEOrders(intraMagPELogisticLineGroup, popOrderForm).ToList();

            var articles = intraMagPELogisticLineGroup.SelectMany(lg => lg.Articles.OfType<StandardArticle>().Select(art => art));
            var services = articles.SelectMany(art => art.Services);
            var articlesAndServices = articles.Concat(services);

            shopTicket.VATLines = _shopTicketBuilderService.GetVATLines(articlesAndServices).ToList();

            shopTicket.Total = new ShopTicketTotal()
            {
                TotalAmount = popOrderForm.GlobalPrices.TotalPriceEur.Value,
                NbArticles = shopTicket.Orders.Sum(o => o.NbArticles),
                PaymentDatas = new List<ShopTicketPaymentData>()
            };

            shopTicket.Customer = _shopTicketBuilderService.GetCustomerShopTicket(popOrderForm, popOrderForm.BillingAddress);

            return shopTicket;
        }
    }
}
