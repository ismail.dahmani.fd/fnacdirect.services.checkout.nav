﻿using FnacDirect.OrderPipe.Base.BaseModel.Model;
using System;
namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Common.Basket
{
    public interface IExpressPlusViewModelBuilder
    {
        ExpressPlusViewModel Build(AboIlliData aboIlliData);
    }
}
