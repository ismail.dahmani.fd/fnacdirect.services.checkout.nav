﻿namespace FnacDirect.OrderPipe.Base.Model.Shipping.ShippingMethod
{
    public class ShippingFnacPlusMember : GroupData
    {
        public int? FnacPlusCardIdFromVirtualExpressFnacPlus { get; set; }
    }
}