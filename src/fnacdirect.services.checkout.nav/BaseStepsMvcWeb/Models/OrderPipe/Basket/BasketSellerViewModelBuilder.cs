using FnacDirect.OrderPipe.Base.BaseModel;
using FnacDirect.OrderPipe.Base.Business.Shipping;
using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.OrderPipe.Base.Model.LineGroups;
using FnacDirect.OrderPipe.Base.Proxy.Membership;
using FnacDirect.OrderPipe.BaseMvc.Web.Models.OrderPipe.Basket;
using FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Common.Basket;
using FnacDirect.OrderPipe.Core.Services;
using FnacDirect.Technical.Framework.Web.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Basket
{
    public class BasketSellerViewModelBuilder : IBasketSellerViewModelBuilder
    {
        private const string DefaultForcedMaxQuantity = "999";
        private readonly ISellerInformationsViewModelBuilder _sellerInformationsViewModelBuilder;
        private readonly IRichBasketItemViewModelBuilderProvider _basketItemViewModelBuilderProvider;
        private readonly IAdherentCardArticleService _adherentCardArticleService;
        private readonly IPromotionTemplateViewModelBuilder _promotionTemplateViewModelBuilder;
        private readonly ISolexBusiness _solexBusiness;
        private readonly IShopAvailabilityViewModelBuilder _shopAvailabilityViewModelBuilder;
        private readonly IShippingEvaluationBusiness _shippingEvaluationBusiness;
        private readonly bool _useMaxQuantityAllowedField;
        private readonly int _forcedMaxQuantity;

        public BasketSellerViewModelBuilder(ISellerInformationsViewModelBuilder sellerInformationsViewModelBuilder,
                                          IRichBasketItemViewModelBuilderProvider basketItemViewModelBuilderProvider,
                                          IApplicationContext applicationContext,
                                          IAdherentCardArticleService adherentCardArticleService,
                                          IPromotionTemplateViewModelBuilder promotionTemplateViewModelBuilder,
                                          ISolexBusiness solexBusiness,
                                          ISwitchProvider switchProvider,
                                          IShopAvailabilityViewModelBuilder shopAvailabilityViewModelBuilder,
                                          IShippingEvaluationBusiness shippingEvaluationBusiness)
        {
            _adherentCardArticleService = adherentCardArticleService;
            _sellerInformationsViewModelBuilder = sellerInformationsViewModelBuilder;
            _basketItemViewModelBuilderProvider = basketItemViewModelBuilderProvider;
            _solexBusiness = solexBusiness;
            _promotionTemplateViewModelBuilder = promotionTemplateViewModelBuilder;
            _shopAvailabilityViewModelBuilder = shopAvailabilityViewModelBuilder;
            _shippingEvaluationBusiness = shippingEvaluationBusiness;
            _useMaxQuantityAllowedField = switchProvider.IsEnabled("orderpipe.pop.pomcore.usemaxquantityallowedfield");
            _forcedMaxQuantity = int.Parse(applicationContext.GetAppSetting("ForcedMaxQuantity") ?? DefaultForcedMaxQuantity);
        }

        public BasketSellerViewModel Build(PopOrderForm popOrderForm, int seller, List<BaseArticle> articles)
        {
            var basketSellerViewModel = new BasketSellerViewModel()
            {
                SellerId = seller,
                Informations = _sellerInformationsViewModelBuilder.Build(popOrderForm, seller)
            };

            var basketItemViewModelBuilder = GetBasketItemViewModelBuilder(popOrderForm);

            foreach (var article in articles)
            {
                var callbacks = new BasketViewModelBuilderCallbacks(
                    (standardArticle) => OnComputingAvailabilityCallback(popOrderForm, standardArticle),
                    (viewModel, standardArticle) => OnBuildingStandardViewModelCallback(popOrderForm, viewModel, standardArticle),
                    (viewModel, marketPlaceArticle) => OnBuildingMarketPlaceViewModelCallback(popOrderForm, seller, viewModel, marketPlaceArticle));

                var basketItemViewModel = basketItemViewModelBuilder.Build(article, callbacks, popOrderForm);

                if (basketItemViewModel != null)
                {
                    basketSellerViewModel.WithItem(basketItemViewModel);
                }
            }

            //nettoyer les OPS Dezzer en double
            _promotionTemplateViewModelBuilder.CleanDeezerPromotions(basketSellerViewModel.Items.ToList());

            return basketSellerViewModel;
        }

        #region callbacks

        internal AvailabilityViewModel OnComputingAvailabilityCallback(PopOrderForm popOrderForm, StandardArticle standardArticle)
        {
            var parentLineGroup = GetParentLineGroup(popOrderForm, standardArticle);
            if (parentLineGroup == null)
            {
                return new AvailabilityViewModel();
            }

            var maybeShippingMethodEvaluationGroup = popOrderForm.ShippingMethodEvaluation.Seller(parentLineGroup.Seller.SellerId);
            if (!maybeShippingMethodEvaluationGroup.HasValue)
            {
                return new AvailabilityViewModel();
            }
            var shippingMethodEvaluationGroup = maybeShippingMethodEvaluationGroup.Value;

            var isArticleShippedInStore = _shippingEvaluationBusiness.IsArticleShippedInStore(standardArticle, shippingMethodEvaluationGroup);
            if (isArticleShippedInStore)
            {
                return _shopAvailabilityViewModelBuilder.Build(standardArticle, parentLineGroup, shippingMethodEvaluationGroup);
            }

            return new AvailabilityViewModel();
        }

        private RichBasketItemViewModel OnBuildingStandardViewModelCallback(PopOrderForm popOrderForm, RichBasketItemViewModel viewModel, StandardArticle standardArticle)
        {
            var parentLineGroup = GetParentLineGroup(popOrderForm, standardArticle);
            if (parentLineGroup == null)
            {
                return viewModel;
            }

            var maybeShippingMethodEvaluationGroup = popOrderForm.ShippingMethodEvaluation.Seller(parentLineGroup.Seller.SellerId);
            if (!maybeShippingMethodEvaluationGroup.HasValue)
            {
                return viewModel;
            }

            var shippingMethodEvaluationGroup = maybeShippingMethodEvaluationGroup.Value;
            if (shippingMethodEvaluationGroup.SelectedShippingMethodEvaluationType != ShippingMethodEvaluationType.Shop
                || shippingMethodEvaluationGroup.SelectedShippingMethodEvaluation.IsDummyAddress)
            {
                return viewModel;
            }

            var shopAddress = shippingMethodEvaluationGroup.ShopShippingMethodEvaluationItem.Value.ShippingAddress;
            if (!int.TryParse(shopAddress.RefUG, out var refUG))
            {
                return viewModel;
            }

            var storeStockInformations = _solexBusiness.GetStoreStockInformations(refUG.ToString(), standardArticle);
            if (storeStockInformations == null
                || storeStockInformations.TotalAvailableClickAndCollect <= 0)
            {
                return viewModel;
            }

            viewModel.Quantity.ClickAndCollectEnabled = true;

            if (_useMaxQuantityAllowedField)
            {
                viewModel.Quantity.MaxClickAndCollect = standardArticle.HasExceededQuantity ? storeStockInformations.TotalAvailableClickAndCollect : viewModel.Quantity.DefaultMax;

                if (standardArticle.HasLimitedQuantity || standardArticle.Availability == StockAvailability.ClickAndCollectOnly)
                {
                    viewModel.Quantity.HasMax = true;

                    if (standardArticle.HasLimitedQuantity)
                    {
                        viewModel.Quantity.MaxClickAndCollect = Math.Min(storeStockInformations.TotalAvailableClickAndCollect, standardArticle.MaxQuantityAllowed);
                    }
                    else
                    {
                        viewModel.Quantity.MaxClickAndCollect = storeStockInformations.TotalAvailableClickAndCollect;
                    }
                }
            }
            else
            {
                viewModel.Quantity.MaxClickAndCollect = storeStockInformations.TotalAvailableClickAndCollect;

                if (standardArticle.Availability == StockAvailability.ClickAndCollectOnly)
                {
                    viewModel.Quantity.Max = storeStockInformations.TotalAvailableClickAndCollect;
                    viewModel.Quantity.HasMax = true;
                }
            }

            viewModel.Quantity.MaxClickAndCollect = Math.Min(viewModel.Quantity.MaxClickAndCollect, _forcedMaxQuantity);

            return viewModel;
        }

        private RichBasketItemViewModel OnBuildingMarketPlaceViewModelCallback(PopOrderForm popOrderForm, int seller, RichBasketItemViewModel viewModel, MarketPlaceArticle marketPlaceArticle)
        {
            var shippings = new List<string>();

            var maybeShippingMethodEvaluationGroup = popOrderForm.ShippingMethodEvaluation.GetShippingMethodEvaluationGroup(seller);

            if (maybeShippingMethodEvaluationGroup.HasValue)
            {
                var shippingMethodEvaluationGroup = maybeShippingMethodEvaluationGroup.Value;

                foreach (var shippingMethodEvaluationItem in shippingMethodEvaluationGroup.ShippingMethodEvaluationItems)
                {
                    foreach (var shippingMethodResult in shippingMethodEvaluationItem.GetShippingMethodResults())
                    {
                        foreach (var shippingCost in shippingMethodResult.ShippingCosts)
                        {
                            foreach (var detail in shippingCost.Details)
                            {
                                shippings.Add(detail.ShippingMethod.Label);
                            }
                        }
                    }
                }
            }

            var availableShippings = string.Join(" - ", shippings.Distinct().ToArray());

            viewModel.AvailableShippings = availableShippings;

            return viewModel;
        }

        #endregion

        #region privates

        private IRichBasketItemViewModelBuilder GetBasketItemViewModelBuilder(PopOrderForm popOrderForm)
        {
            var hasAdherentCardInBasket = popOrderForm.LineGroups.Where(lg => lg.HasFnacComArticle).SelectMany(lg => lg.Articles).Any(a => _adherentCardArticleService.IsAdherentCard(a.ProductID.GetValueOrDefault(0)));
            var applyAdherentPromotion = popOrderForm.UserContextInformations.IsAdherent || hasAdherentCardInBasket;

            var context = new RichBasketItemViewModelBuilderContext(popOrderForm, popOrderForm.GlobalPromotions, applyAdherentPromotion);
            return _basketItemViewModelBuilderProvider.Get(context);
        }

        private ILineGroup GetParentLineGroup(PopOrderForm popOrderForm, StandardArticle standardArticle)
        {
            ILineGroup parentLineGroup = null;

            foreach (var lineGroup in popOrderForm.LineGroups)
            {
                if (lineGroup.Articles.Any(a => a == standardArticle))
                {
                    parentLineGroup = lineGroup;
                    break;
                }
            }

            return parentLineGroup;
        }

        #endregion
    }
}
