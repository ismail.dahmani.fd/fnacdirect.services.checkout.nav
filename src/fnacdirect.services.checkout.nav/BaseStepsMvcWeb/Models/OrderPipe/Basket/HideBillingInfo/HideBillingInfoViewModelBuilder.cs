using FnacDirect.OrderPipe.Base.Model;
using System.Linq;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Basket
{
    /// <summary>
    /// HideBillingInfo previously called GiftOption
    /// </summary>
    public class HideBillingInfoViewModelBuilder : IHideBillingInfoViewModelBuilder
    {
        public HideBillingInfoViewModel Build(PopOrderForm popOrderForm)
        {
            var result = new HideBillingInfoViewModel();

            var lineGroups = popOrderForm.LineGroups;
            
            result.IsApplied = lineGroups.Any(lg => lg.Informations.GiftOption);

            return result;
        }
    }
}
