using FnacDirect.OrderPipe.Base.Model.Facets;

namespace FnacDirect.OrderPipe.Base.Business
{
    public interface IGuestService
    {
        bool IsEligible(IGotLineGroups lineGroups, OPContext oPContext);
    }
}
