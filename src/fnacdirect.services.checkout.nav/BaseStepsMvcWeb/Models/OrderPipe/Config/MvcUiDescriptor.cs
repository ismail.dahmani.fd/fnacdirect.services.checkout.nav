using System.Web;
using System.Linq;
using System.Web.Mvc;
using System.Xml.Serialization;
using FnacDirect.Technical.Framework.Web.UrlMgr;
using FnacDirect.OrderPipe.CoreMvcWeb.Routing;
using System;
using System.Collections.Generic;

namespace FnacDirect.OrderPipe.Config
{
    public class MvcUiDescriptor : UIDescriptor
    {
        public MvcUiDescriptor()
        {
            _routeValueProvider = new Lazy<IMvcOrderPipeStepRedirectionRouteValueProvider>(() =>
            {
                var selectedType = (from assembly in AppDomain.CurrentDomain.GetAssemblies()
                            where assembly.FullName.Contains("FnacDirect.OrderPipe")
                            from type in assembly.GetTypes()
                            where type.FullName.Contains(RouteValueProviderType)
                            select type).FirstOrDefault();

                return Activator.CreateInstance(selectedType) as IMvcOrderPipeStepRedirectionRouteValueProvider;
            });
        }

        public string QueryStringMode { get; set; }

        public string QueryStringWhitelist { get; set; }

        public IEnumerable<string> QueryStringsToPreserve()
        {
            if(string.IsNullOrEmpty(QueryStringWhitelist))
            {
                return Enumerable.Empty<string>();
            }

            return QueryStringWhitelist.Split('|').Select(k => k.ToLowerInvariant()).ToList();
        }

        public string Controller { get; set; }

        public string Action { get; set; }

        public string Route { get; set; }

        public string AreaName { get; set; }

        public string RenderView { get; set; }

        public string RouteValueProviderType { get; set; }

        private readonly Lazy<IMvcOrderPipeStepRedirectionRouteValueProvider> _routeValueProvider;

        public IMvcOrderPipeStepRedirectionRouteValueProvider RouteValueProvider
        {
            get
            {
                if(RouteValueProviderType == null)
                {
                    return null;
                }

                return _routeValueProvider.Value;
            }
        }

        [XmlElement("Slot")]
        public MvcSlotList Slots { get; set; }
        
        public override T GetRedirect<T>()
        {
            if (typeof(T) == typeof(string))
            {
                return GetRedirectAsString() as T;
            }

            if (typeof(T) == typeof(UrlWriter))
            {
                return GetUrlWriter() as T;
            }

            return base.GetRedirect<T>();
        }

        private string GetRedirectAsString()
        {
            var urlHelper = new UrlHelper(HttpContext.Current.Request.RequestContext);

            var redirection = urlHelper.RouteUrl(Route);

            if (redirection != null)
            {
                return redirection;
            }

            return null;
        }

        private UrlWriter GetUrlWriter()
        {
            var urlHelper = new UrlHelper(HttpContext.Current.Request.RequestContext);

            var redirection = urlHelper.RouteUrl(Route);

            if (redirection != null)
            {
                var applicationName = HttpContext.Current.Request.ApplicationPath;

                if (redirection.StartsWith(applicationName))
                    redirection = redirection.Remove(0, applicationName.Length);

                return UriConverter.Convert(new Uri()
                {
                    Path = redirection
                });
            }

            return null;
        }
    }
}