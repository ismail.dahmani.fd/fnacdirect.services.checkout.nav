using FnacDirect.OrderPipe.Base.Model;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.DataLayer
{
    public class DatalayerViewModel
    {
        public PageTypeEnum PageType { get; set; }
        public PopOrderForm PopOrderForm { get; set; }
        public OPContext OpContext { get; set; }
    }
}
