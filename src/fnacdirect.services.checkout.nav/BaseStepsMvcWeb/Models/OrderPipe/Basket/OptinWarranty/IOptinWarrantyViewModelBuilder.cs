using FnacDirect.OrderPipe.Base.Model;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Basket
{
    public interface IOptinWarrantyViewModelBuilder
    {
        OptinWarrantyViewModel Build(PopOrderForm orderform, int productid);
    }
}
