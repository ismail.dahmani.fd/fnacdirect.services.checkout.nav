﻿using FnacDirect.OrderPipe.Base.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FnacDirect.OrderPipe.Base.LineGroups
{
    public class PreviousLineGroup
    {
        public PreviousLineGroup()
        {
            Articles = new ArticleList();
        }

        public int? OrderPk { get; set; }

        public Guid OrderReference { get; set; }

        public string OrderMessage { get; set; }

        public ShippingAddress ShippingAddress { get; set; }

        public BillingAddress BillingAddress { get; set; }

        public ArticleList Articles { get; set; }
    }
}
