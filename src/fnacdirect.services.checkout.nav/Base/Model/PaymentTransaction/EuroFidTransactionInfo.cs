namespace FnacDirect.OrderPipe.Base.Model.FDV
{
    public class EuroFidTransactionInfo
    {
        /// <summary>
        /// Order Id remonté par EuroFid
        /// </summary>
        public int OrderSiebelId { get; set; }

        /// <summary>
        /// Transaction Id remonté par EuroFid
        /// </summary>
        public int? TransactionId { get; set; }
       
        public string AdherentNumber { get; set; }

        public string ContractNumber { get; set; }
       
        public string CardNumber { get; set; }
    }
}
