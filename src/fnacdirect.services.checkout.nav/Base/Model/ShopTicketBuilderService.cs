using FnacDirect.Customer.Model;
using FnacDirect.OrderPipe.Base.LineGroups;
using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.OrderPipe.Base.Model.Facets;
using FnacDirect.OrderPipe.Base.Model.SelfCheckout;
using FnacDirect.OrderPipe.FDV.Model;
using FnacDirect.Technical.Framework.Web.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FnacDirect.OrderPipe.Base.Business
{
    public class ShopTicketBuilderService : IShopTicketBuilderService
    {
        private readonly IPromotionStoreService _promotionStoreService;
        private readonly IApplicationContext _applicationContext;

        public ShopTicketBuilderService(IPromotionStoreService promotionStoreService, IApplicationContext applicationContext)
        {
            _promotionStoreService = promotionStoreService;
            _applicationContext = applicationContext;
        }

        public IEnumerable<ShopTicketOrder> GetIntraMagPEOrders(IEnumerable<LogisticLineGroup> logisticLineGroups, IGotUserContextInformations gotUserContextInformations)
        {
            var shopTicketOrders = new List<ShopTicketOrder>();
            var sellerData = gotUserContextInformations.GetPrivateData<SellerData>(create: false);
            var customerEntity = gotUserContextInformations.UserContextInformations.CustomerEntity;
            var allArticles = logisticLineGroups.SelectMany(lg => lg.Articles);

            foreach (var logisticLineGroup in logisticLineGroups)
            {
                var order = new ShopTicketOrder
                {
                    OrderReference = logisticLineGroup.OrderUserReference,
                    GuptReference = logisticLineGroup.IsIntraMag ? logisticLineGroup.GUPTReference : null,
                    SubTotalAmount = logisticLineGroup.Articles.OfType<StandardArticle>().Select(a => a.Quantity.GetValueOrDefault() * (a.PriceUserEur.GetValueOrDefault() + a.EcoTaxEur.GetValueOrDefault())).Sum(),
                    ShippingCost = 0m,
                    WrappingCost = logisticLineGroup.GlobalPrices?.TotalShoppingCartBusinessWrapPriceUserEur ?? 0,
                    IsFnacCom = logisticLineGroup.IsFnacCom,
                    IsIntramagPE = logisticLineGroup.IsIntramagPE,
                    IsMP = logisticLineGroup.IsMarketPlace,
                    Articles = new List<ShopTicketArticle>(),
                };

                var standardArticles = logisticLineGroup.Articles.OfType<StandardArticle>();

                order.Articles.AddRange(GetStandardArticleShopTickets(standardArticles, customerEntity));
                var allArticleServices = standardArticles.SelectMany(art => art.Services.Select(ser => ser));
                order.Articles.AddRange(GetServicesArticleShopTickets(allArticleServices));
                order.NbArticles = order.Articles.Sum(art => art.Quantity);
                order.RemiseAmount = order.Articles.Sum(art => art.RemiseAmount);

                shopTicketOrders.Add(order);
            }

            return shopTicketOrders;
        }

        public IEnumerable<ShopTicketVATLine> GetVATLines(IEnumerable<StandardArticle> articles)
        {
            var shopTicketVATLines = new List<ShopTicketVATLine>();

            foreach (var article in articles)
            {
                var isShopTicketVATLineExist = shopTicketVATLines.Any(l => l.TvaTsa.GetValueOrDefault(0) == article.VATRate.GetValueOrDefault(0));
                var shopTicketVATLine = isShopTicketVATLineExist ?
                                            shopTicketVATLines.First(l => l.TvaTsa.GetValueOrDefault(0) == article.VATRate.GetValueOrDefault(0)) :
                                            new ShopTicketVATLine { TvaTsa = article.VATRate, MtHT = 0, MtTTC = 0 };

                shopTicketVATLine.MtTTC += (article.PriceUserEur.GetValueOrDefault(0) + article.EcoTaxEur.GetValueOrDefault(0)) * article.Quantity.GetValueOrDefault(1);

                if (!isShopTicketVATLineExist)
                {
                    shopTicketVATLines.Add(shopTicketVATLine);
                }
            }

            ComputeTVAAndHT(shopTicketVATLines);

            return shopTicketVATLines;
        }

        public ShopTicketCustomerData GetCustomerShopTicket(IGotUserContextInformations gotUserContextInformations, BillingAddress billingAddress)
        {
            var customerShopTicket = new ShopTicketCustomerData();

            var usercontextInformation = gotUserContextInformations.UserContextInformations;

            if (usercontextInformation.IsAdherent)
            {
                AdherentInformationForCustomerShopTicket(customerShopTicket, usercontextInformation);
            }

            if (billingAddress != null)
            {
                customerShopTicket.FirstName = billingAddress.Firstname;
                customerShopTicket.LastName = billingAddress.Lastname;
                customerShopTicket.Company = billingAddress.Company;
                customerShopTicket.ZipCode = billingAddress.ZipCode;
            }

            return customerShopTicket;
        }
        
        #region private methods

        private IEnumerable<ShopTicketArticle> GetStandardArticleShopTickets(IEnumerable<StandardArticle> articles, CustomerEntity customerEntity)
        {
            var shopTicketArticles = new List<ShopTicketArticle>();
            foreach (var article in articles)
            {
                var shopTicketArticle = new ShopTicketArticle
                {
                    Title = string.IsNullOrWhiteSpace(article.Title) ? string.Empty : article.Title.PadRight(20, ' ').Substring(0, 20),
                    TotalAmount = article.PriceDBEur.Value * article.Quantity.Value,
                    EAN = article.Ean13,
                    Quantity = article.Quantity.Value,
                    UnitAmount = article.PriceDBEur.Value,
                    Type = string.Concat("0", article.CodeSegment)
                };

                if (article.HasRemiseClient)
                {
                    shopTicketArticle.RemiseAmount = article.Remise.MontantRemise;
                    shopTicketArticle.RemiseCodeMotif = article.Remise.CodeMotif.ToString();
                    shopTicketArticle.RemiseCodeType = _promotionStoreService.GetClientOrderDiscountCodeId().ToString();
                }
                else if (article.Promotions != null && article.Promotions.Count > 0 && article.PriceDBEur.Value - article.PriceUserEur.Value > decimal.Zero)
                {
                    shopTicketArticle.RemiseAmount = (article.PriceDBEur.Value - article.PriceUserEur.Value) * article.Quantity.GetValueOrDefault(1);
                    shopTicketArticle.RemiseCodeMotif = _promotionStoreService.GetPromoCodeMotif(article, customerEntity, false, article.Promotions[0].Code);
                    shopTicketArticle.RemiseCodeType = _promotionStoreService.GetPromoCodeType(article, customerEntity, false, article.Promotions[0].Code);
                }

                if (article.EcoTaxEur.GetValueOrDefault(0) > decimal.Zero)
                {
                    shopTicketArticle.Components = GetEcoTaxShopTicket(article);
                }

                shopTicketArticles.Add(shopTicketArticle);

            }

            return shopTicketArticles;
        }

        private IEnumerable<ShopTicketArticle> GetServicesArticleShopTickets(IEnumerable<Service> services)
        {
            var serviceShopTickets = new List<ShopTicketArticle>();

            foreach (var service in services)
            {
                var serviceShopTicket = new ShopTicketArticle()
                {
                    Title = string.IsNullOrWhiteSpace(service.Title) ? string.Empty : service.Title.PadRight(20, ' ').Substring(0, 20),
                    TotalAmount = service.PriceDBEur.Value * service.Quantity.Value,
                    EAN = service.Ean13,
                    Quantity = service.Quantity.Value,
                    UnitAmount = service.PriceDBEur.Value,
                    Type = string.Concat("0", service.CodeSegment)
                };

                if (service.EcoTaxEur.GetValueOrDefault(0) > decimal.Zero)
                {
                    serviceShopTicket.Components = GetEcoTaxShopTicket(service);
                }

                serviceShopTickets.Add(serviceShopTicket);
            }

            return serviceShopTickets;
        }

        private List<ShopTicketArticle> GetEcoTaxShopTicket(StandardArticle standardArticle)
        {
            return new List<ShopTicketArticle>
                    {
                        new ShopTicketArticle
                        {
                        TotalAmount = standardArticle.EcoTaxEur.Value * standardArticle.Quantity.Value,
                        Title = standardArticle.EcoTaxCode
                        }
                    };
        }

        private void ComputeTVAAndHT(IEnumerable<ShopTicketVATLine> shopTicketVATLines)
        {
            foreach (var shopTicketVATLine in shopTicketVATLines)
            {
                shopTicketVATLine.MtTVA = Math.Round(shopTicketVATLine.MtTTC.GetValueOrDefault(0) - (shopTicketVATLine.MtTTC.GetValueOrDefault(0) / (1 + shopTicketVATLine.TvaTsa.GetValueOrDefault(0) / 100)), 2);
                shopTicketVATLine.MtHT = shopTicketVATLine.MtTTC.GetValueOrDefault(0) - shopTicketVATLine.MtTVA.GetValueOrDefault(0);
            }
        }

        private void AdherentInformationForCustomerShopTicket(ShopTicketCustomerData customerShopTicket, Model.UserInformations.IUserContextInformations usercontextInformation)
        {
            customerShopTicket.IsAdherent = true;
            customerShopTicket.AdherentNumber = usercontextInformation.CustomerEntity.AdherentNumber;
            customerShopTicket.AdherentCardNumber = usercontextInformation.CustomerEntity.AdherentCardNumber;
            if (usercontextInformation.CustomerEntity.AdhesionType.GetValueOrDefault(0) == 80)
            {
                customerShopTicket.AdherentProgramType = _applicationContext.GetMessage("Ticket.Facturette.Adherent.One");
            }
            else if (usercontextInformation.CustomerEntity.FidelityProgramDuration == 3)
            {
                customerShopTicket.AdherentProgramType = _applicationContext.GetMessage("Ticket.Facturette.Adherent.ThreeYears");
            }
            else
            {
                customerShopTicket.AdherentProgramType = _applicationContext.GetMessage("Ticket.Facturette.Adherent.OneYear");
            }
        }

        #endregion
    }
}
