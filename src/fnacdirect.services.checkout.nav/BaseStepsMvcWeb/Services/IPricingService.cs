using System.Collections.Generic;
using FnacDirect.Contracts.Online.Model;
using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.OrderPipe.Base.Model.Facets;
using Article = FnacDirect.OrderPipe.Base.Model.Article;

namespace FnacDirect.OrderPipe.Base.Business
{
    public interface IPricingService
    {
        FnacDirect.Contracts.Online.Model.ShoppingCart GetArticlesPrices(SiteContext siteContext, int localCountry, IEnumerable<Article> articles, string advantageCode, FnacDirect.Contracts.Online.Model.Customer dtoCustomer, BaseOF orderForm, GetArticlesPricesParameters getArticlesPricesParameters);

        VatInfo GetVATInfo(int countryId, int prmiVatId);
        decimal GetVATRate(int? countryId, int prmiVatId);

        decimal GetPrice(Price price, VatCountry vatCountry, int vatId, int localCountry);

        decimal GetPriceHT(decimal priceTTC, decimal vatRate);
        decimal GetPriceHT(decimal priceTTC, int vatId, int localCountry);
        decimal GetPrice(decimal priceTTC, decimal? priceHT, VatCountry vatCountry, int vatId, int localCountry);
        decimal GetPrice(decimal priceTTC, decimal? priceHT, bool isCountryUseVAT, int? vatCountryId, int vatId, int localCountryId);

        decimal GetPriceEbook(decimal priceTTC, decimal? priceHT, bool isCountryUseVAT, int vatId, int localCountryId);

        FnacDirect.Contracts.Online.Model.ShoppingCart GetArticlesPrices(SiteContext siteContext,
            int localCountry,
            FnacDirect.Contracts.Online.Model.Customer dtoCustomer,
            MultiFacet<IGotLineGroups, IGotMobileInformations, IGotBillingInformations, IGotShippingMethodEvaluation> multiFacets,
            GetArticlesPricesParameters getArticlesPricesParameters,
            bool useCache);

        decimal GetRoundedPrice(decimal priceRealEur);
        FnacDirect.Contracts.Online.Model.ShoppingCart GetArticlesPrices(SiteContext siteContext,
            int localCountry,
            FnacDirect.Contracts.Online.Model.Customer dtoCustomer,
            MultiFacet<IGotLineGroups, IGotMobileInformations, IGotBillingInformations, IGotShippingMethodEvaluation> multiFacets,
            IEnumerable<Article> articles,
            List<ShoppingCartLineItem> replacedArticles,
            GetArticlesPricesParameters getArticlesPricesParameters,
            bool useCache,
            ShopAddress shippingShop = null);

        FnacDirect.Contracts.Online.Model.ShoppingCart GetArticlesPrices(SiteContext siteContext,
            IEnumerable<Article> articles);
    }
}
