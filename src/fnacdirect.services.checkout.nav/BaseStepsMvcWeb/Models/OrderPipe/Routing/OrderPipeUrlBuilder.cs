using FnacDirect.OrderPipe.Config;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace FnacDirect.OrderPipe
{
    public class OrderPipeUrlBuilder : IOrderPipeUrlBuilder
    {
        private readonly Func<OPContext> _opContextProvider;

        public OrderPipeUrlBuilder(Func<OPContext> opContextProvider)
        {
            _opContextProvider = opContextProvider;
        }

        public string GetUrl(string stepName)
        {
            return GetUrl(stepName, new RouteValueDictionary());
        }

        public string GetUrl(string stepName, RouteValueDictionary routeValues)
        {
            var urlHelper = new UrlHelper(HttpContext.Current.Request.RequestContext);

            var queryStrings = HttpContext.Current.Request.QueryString;

            foreach(string key in queryStrings.Keys)
            {
                /* TODO: Après TNR, au lieu de tester les valeurs,
                 * faire une extension de QueryString qui permet de récupérer tout les éléments en filtrant les null
                 * (et d'autres valeurs spécifiés en paramètre)
                */
                if (key == null)
                {
                    continue;
                }

                if(!routeValues.ContainsKey(key))
                {
                    routeValues.Add(key, queryStrings[key]);
                }
            }

            var opContext = _opContextProvider();

            var pipe = opContext.Pipe;

            var displayShippingAddressUiDescriptor = pipe.Steps.OfType<IInteractiveStep>().FirstOrDefault(s => s.Name == stepName).UIDescriptor as MvcUiDescriptor;

            return urlHelper.RouteUrl(displayShippingAddressUiDescriptor.Route, routeValues);
        }
    }
}
