﻿using System.Xml.Serialization;

namespace FnacDirect.OrderPipe.Base.Business.Mailing
{
    [XmlInclude(typeof(PayByRefPaymentMethod))]
    public class PaymentMethod
    {
        [XmlIgnore]
        public int Id { get; set; }
       
        public string Type { get; set; }
        
        public string Amount { get; set; }
    }
    
    public class PayByRefPaymentMethod : PaymentMethod
    {
        public string MerchantId { get; set; }

        public string Reference { get; set; }
    }
}
