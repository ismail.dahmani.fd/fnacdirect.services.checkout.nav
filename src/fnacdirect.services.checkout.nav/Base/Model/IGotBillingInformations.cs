using FnacDirect.Contracts.Online.Model;
using FnacDirect.Front.Meteor.Business.Models;
using FnacDirect.OrderPipe.Base.Model.OrderInformations;
using System;
using System.Collections.Generic;

namespace FnacDirect.OrderPipe.Base.Model.Facets
{
    public interface IGotBillingInformations : IOF
    {
        IEnumerable<BillingMethod> OrderBillingMethods { get; }
        void AddBillingMethod(BillingMethod billingMethod);
        void RemoveBillingMethod(int billingMethodId);
        void RemoveBillingMethod(Predicate<BillingMethod> predicate);
        void SortOrderBillingMethods();
        void ClearBillingMethods();
        decimal? RemainingGlobalPricesForPrimaryMethods();

        BillingMethod MainBillingMethod { get; set; }

        int BillingMethodMask { get; set; }

        Dictionary<int, decimal> BillingMethodConsolidation { get; set; }

        bool EnableOgoneCreditCard { get; set; }

        IOrderGlobalInformations OrderGlobalInformations { get; }

        IEnumerable<Promotion> GlobalPromotions { get; }
        void AddGlobalPromotion(Promotion promotion);

        GlobalPrices GlobalPrices { get; }

        XmlOrderForm XmlOrder { get; set; }

        List<int> HiddenCreditCards { get; }

        decimal RemainingTotal { get; set; }
        
        IList<Credit> AvailableCredits { get; }
    }
}
