namespace FnacDirect.OrderPipe.Base.Model.Pop
{
    public enum AppliablePath
    {
        Full = 0,
        OnePage = 1,
        Payment = 2
    }
}
