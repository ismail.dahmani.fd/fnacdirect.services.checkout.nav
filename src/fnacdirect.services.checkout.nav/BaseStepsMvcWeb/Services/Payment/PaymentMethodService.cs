using FnacDirect.Customer.Model;
using FnacDirect.OrderPipe.Base.Business;
using FnacDirect.OrderPipe.Base.Business.InstallmentPayment;
using FnacDirect.OrderPipe.Base.Business.LogisticLineGroups;
using FnacDirect.OrderPipe.Base.Business.Ogone;
using FnacDirect.OrderPipe.Base.Business.Payment;
using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.OrderPipe.Base.Model.LineGroups;
using FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Common.Payment;
using FnacDirect.Technical.Framework.Web.Mvc;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Services.Pop.Payment
{
    using Base.Model.Facets;
    using FnacDirect.OrderPipe.Base.Model.FDV;
    using Ogone.BLL;
    using System;

    public class PaymentMethodService : IPaymentMethodService
    {
        private readonly IBillingMethodService _billingMethodService;
        private readonly IApplicationContext _applicationContext;
        private readonly ICreditCardDescriptorBusiness _creditCardDescriptorBusiness;
        private readonly ICheckCreditCardsEligibilityBusinessProvider _checkCreditCardsEligibilityBusinessProvider;
        private readonly IInstallmentService _installmentService;
        private readonly IOgoneBusiness3DSecure _ogoneBusiness3DSecure;
        private readonly IOgoneBusiness _ogoneBusiness;
        private readonly IOrderTransactionManager _orderTransactionManager;
        private readonly ISwitchProvider _switchProvider;
        private readonly IDeferredPaymentBusiness _deferredPaymentBusiness;
        private readonly ILogisticLineGroupGenerationProcess _logisticLineGroupGenerationProcess;
        private readonly IUserExtendedContextProvider _userExtendedContextProvider;

        public PaymentMethodService(IApplicationContext applicationContext,
                                    ICreditCardDescriptorBusiness creditCardDescriptorBusiness,
                                    ICheckCreditCardsEligibilityBusinessProvider checkCreditCardsEligibilityBusinessProvider,
                                    IOgoneBusiness3DSecure ogoneBusiness3DSecure,
                                    IOgoneBusiness ogoneBusiness,
                                    IInstallmentService installmentService,
                                    IOrderTransactionManager orderTransactionManager,
                                    ISwitchProvider switchProvider,
                                    IBillingMethodService billingMethodeService,
                                    IDeferredPaymentBusiness deferredPaymentBusiness,
                                    ILogisticLineGroupGenerationProcess logisticLineGroupGenerationProcess,
                                    IUserExtendedContextProvider userExtendedContextProvider)
        {
            _applicationContext = applicationContext;
            _creditCardDescriptorBusiness = creditCardDescriptorBusiness;
            _checkCreditCardsEligibilityBusinessProvider = checkCreditCardsEligibilityBusinessProvider;
            _installmentService = installmentService;
            _ogoneBusiness3DSecure = ogoneBusiness3DSecure;
            _ogoneBusiness = ogoneBusiness;
            _orderTransactionManager = orderTransactionManager;
            _switchProvider = switchProvider;
            _billingMethodService = billingMethodeService;
            _deferredPaymentBusiness = deferredPaymentBusiness;
            _logisticLineGroupGenerationProcess = logisticLineGroupGenerationProcess;
            _userExtendedContextProvider = userExtendedContextProvider;
        }

        public List<CustCreditCardDescriptor> GetCreditCardsDescriptor()
        {
            var siteContext = _applicationContext.GetSiteContext();
            var culture = siteContext != null ? siteContext.Culture : "";
            var config = _applicationContext.GetAppSetting("config.shippingMethodSelection");

            return _creditCardDescriptorBusiness.GetCreditCards(config, culture);
        }

        public string ApplyMask(string cardNumber)
        {
            return $"** {Regex.Match(cardNumber, @"\d+$").Value.PadLeft(4, '*')}";
        }

        public IEnumerable<int> GetEligiblePaymentMethodesIds(OPContext opContext, string config)
        {
            var iGotBillingInformations = (IGotBillingInformations)opContext.OF;
            var iGotLineGroups = (IGotLineGroups)opContext.OF;
            var iGotShippingMethodEvaluation = (IGotShippingMethodEvaluation)opContext.OF;
            var iGotUserContextInformations = (IGotUserContextInformations)opContext.OF;
            var popOrderForm = (PopOrderForm)opContext.OF;

            var globalParameters = opContext.Pipe.GlobalParameters;
            var allowedMethodMask = globalParameters.Get("mask.payment.method.to.display", -1);
            var basketAmountGreaterThan = globalParameters.Get("basket.amountnovat.grather.than", 140m);
            var anonymousBillingMethodsMaxAmount = opContext.Pipe.GlobalParameters.Get<int>("anonymous.billing.methods.max.amount", 1000);

            var billingMethodsManager = ((BaseOP)opContext.Pipe).BillingMethodsManager;

            var remainingMethods = billingMethodsManager.ListRemainingMethods(allowedMethodMask);

            var result = remainingMethods.Select(l => l.Id).ToList();

            if (result.Contains(OrangeBillingMethod.IdBillingMethod)
                && !_billingMethodService.IsOrangeAllowed(iGotBillingInformations, iGotLineGroups, iGotShippingMethodEvaluation))
            {
                result.Remove(OrangeBillingMethod.IdBillingMethod);
            }

            if (result.Contains(VirtualGiftCheckBillingMethod.IdBillingMethod)
                && !_billingMethodService.IsCcvAllowed(popOrderForm, popOrderForm.LineGroups, popOrderForm.GlobalPrices.TotalPriceEur.GetValueOrDefault(), anonymousBillingMethodsMaxAmount, popOrderForm.UserContextInformations.IsGuest))
            {
                result.Remove(VirtualGiftCheckBillingMethod.IdBillingMethod);
            }

            if (result.Contains(CheckBillingMethod.IdBillingMethod)
                && !_billingMethodService.IsCheckAllowed(iGotBillingInformations, iGotLineGroups, iGotUserContextInformations, iGotShippingMethodEvaluation))
            {
                result.Remove(CheckBillingMethod.IdBillingMethod);
            }

            if (result.Contains(BankTransferBillingMethod.IdBillingMethod)
                && !_billingMethodService.IsBankAllowed(iGotLineGroups, iGotUserContextInformations, iGotShippingMethodEvaluation))
            {
                result.Remove(BankTransferBillingMethod.IdBillingMethod);
            }

            if (result.Contains(DefferedPaymentBillingMethod.IdBillingMethod) &&
                ((iGotBillingInformations.GlobalPrices.TotalPriceEurNoVat < basketAmountGreaterThan) ||
                 !_deferredPaymentBusiness.GetDeferredPaymentEligibility(iGotUserContextInformations.UserContextInformations.AccountId,
                                                                        iGotBillingInformations.GlobalPrices.TotalPriceEur ?? 0,
                                                                        iGotUserContextInformations.UserContextInformations.IdentityImpersonator != null
                 ).IsEligible))
            {
                result.Remove(DefferedPaymentBillingMethod.IdBillingMethod);
            }

            return result;
        }

        /// <summary>
        /// Return credit cards ID from CreditCardDescriptorBusiness 
        /// </summary>
        /// <param name="opContext"></param>
        /// <param name="popOrderForm"></param>
        /// <param name="config"></param>
        /// <param name="culture"></param>
        /// <returns></returns>
        public IEnumerable<int> CreditCards(OPContext opContext, PopOrderForm popOrderForm, string config, string culture)
        {
            var checkCreditCardsEligibilityBusiness = _checkCreditCardsEligibilityBusinessProvider.GetCheckCreditCardsEligibilityBusinessInstance(opContext);

            var eligibilities = new Dictionary<int, Func<bool>>
            {
                { (int)CreditCardTypeEnum.ECarteBleue, () => checkCreditCardsEligibilityBusiness.CheckECBEligibility() == ECBEligibilityResult.Eligible },
                { (int)CreditCardTypeEnum.PayPal, checkCreditCardsEligibilityBusiness.IsPaypalViaOgoneEligible },
                { (int)CreditCardTypeEnum.PaiementTelephone, checkCreditCardsEligibilityBusiness.CheckPayByPhoneEligibility},
                { (int)CreditCardTypeEnum.PostFinance, checkCreditCardsEligibilityBusiness.CheckPostFinanceEligibility },
                { (int)CreditCardTypeEnum.FacilyPay3x, () => false },
                { (int)CreditCardTypeEnum.FacilyPay3xSansFrais, () => false },
                { (int)CreditCardTypeEnum.FacilyPay4x, () => false },
                { (int)CreditCardTypeEnum.FacilyPay4xSansFrais, () => false },
                { (int)CreditCardTypeEnum.FacilyPay3xCacf, () => false },
                { (int)CreditCardTypeEnum.FacilyPay3xSansFraisCacf, () => false },
                { (int)CreditCardTypeEnum.FacilyPay4xCacf, () => false },
                { (int)CreditCardTypeEnum.FacilyPay4xSansFraisCacf, () => false },
                { (int)CreditCardTypeEnum.FacilyPayXx, () => false },
                { (int)CreditCardTypeEnum.VisaFnac, () => checkCreditCardsEligibilityBusiness.IsCaixaEligible() },
                { (int)CreditCardTypeEnum.AmericanExpress, () => false },
                { (int)CreditCardTypeEnum.CacfCreditAvecReprise, () => false },
                { (int)CreditCardTypeEnum.CacfCreditSansReprise, () => false },
            };

            if (_switchProvider.IsEnabled("orderpipe.pop.payment.checkamex"))
            {
                var multiFacet = MultiFacet<IGotLineGroups, IGotBillingInformations, IGotUserContextInformations>.From(popOrderForm);
                eligibilities[(int)CreditCardTypeEnum.AmericanExpress] = () => _billingMethodService.IsOgoneAllowed(multiFacet.Facet1, multiFacet.Facet2, multiFacet.Facet3);
            }

            if (_switchProvider.IsEnabled("orderpipe.pop.payment.cacfcredit.enabled"))
            {
                eligibilities[(int)CreditCardTypeEnum.CacfCreditAvecReprise] = () => true;
                eligibilities[(int)CreditCardTypeEnum.CacfCreditSansReprise] = () => true;
            }

            var orderFormMultiFacets = MultiFacet<IGotLineGroups, IGotBillingInformations, IGotUserContextInformations, IGotShippingMethodEvaluation>.From(popOrderForm);
            
            var parameters = new InstallmentServiceParameters(orderFormMultiFacets, _userExtendedContextProvider, _applicationContext.GetLocalCountry());
            
            var eligibleInstallmentPaymentCreditCards = _installmentService.GetEligibleCreditCardIds(parameters);

            var overrideCacfForMP = popOrderForm.HasMarketPlaceArticle() && _switchProvider.IsEnabled("orderpipe.pop.payment.installmentpayments.overridecacfmp");
            
            if (_switchProvider.IsEnabled("orderpipe.pop.payment.installmentpayments.cacf.enabled") && !overrideCacfForMP)
            {
                var multiFacet = MultiFacet<IGotLineGroups, IGotBillingInformations, IGotShippingMethodEvaluation>.From(popOrderForm);

                if (_logisticLineGroupGenerationProcess.SimulateInitialize(multiFacet).Count() == 1)
                {
                    eligibilities[(int)CreditCardTypeEnum.FacilyPay3xCacf] = () => eligibleInstallmentPaymentCreditCards.Offers.Any(x => x.CreditCardTypeId == (int)CreditCardTypeEnum.FacilyPay3xCacf);
                    eligibilities[(int)CreditCardTypeEnum.FacilyPay3xSansFraisCacf] = () => eligibleInstallmentPaymentCreditCards.Offers.Any(x => x.CreditCardTypeId == (int)CreditCardTypeEnum.FacilyPay3xSansFraisCacf);
                    eligibilities[(int)CreditCardTypeEnum.FacilyPay4xCacf] = () => eligibleInstallmentPaymentCreditCards.Offers.Any(x => x.CreditCardTypeId == (int)CreditCardTypeEnum.FacilyPay4xCacf);
                    eligibilities[(int)CreditCardTypeEnum.FacilyPay4xSansFraisCacf] = () => eligibleInstallmentPaymentCreditCards.Offers.Any(x => x.CreditCardTypeId == (int)CreditCardTypeEnum.FacilyPay4xSansFraisCacf);
                }
            }
            else
            {
                eligibilities[(int)CreditCardTypeEnum.FacilyPay3x] = () => eligibleInstallmentPaymentCreditCards.Offers.Any();
                eligibilities[(int)CreditCardTypeEnum.FacilyPay3xSansFrais] = () => eligibleInstallmentPaymentCreditCards.Offers
                    .Any(x => x.CreditCardTypeId == (int)CreditCardTypeEnum.FacilyPay3xSansFrais || x.CreditCardTypeId == (int)CreditCardTypeEnum.FacilyPay4xSansFrais);
            }

            eligibilities[(int)CreditCardTypeEnum.FacilyPayXx] = () => eligibleInstallmentPaymentCreditCards.Offers.Any(x => x.CreditCardTypeId == (int)CreditCardTypeEnum.FacilyPayXx);

            var specialCardIds = opContext.Pipe.GlobalParameters.GetAsEnumerable<int>("credit.cards.check.excluded.ids");
            var custCreditCardsDescriptors = _creditCardDescriptorBusiness.GetCreditCards(config, culture);

            var creditCards = from custCreditCardDescriptor in custCreditCardsDescriptors
                              where !eligibilities.ContainsKey(custCreditCardDescriptor.Id) || eligibilities[custCreditCardDescriptor.Id]()
                              where !specialCardIds.Contains(custCreditCardDescriptor.Id)
                              select custCreditCardDescriptor.Id;

            return creditCards;
        }

        public ILineGroup GetHighAmountLineGroupWithoutBillingMethod(List<PopLineGroup> lineGroups)
        {
            var HightAmountLineGroup = (from lg in lineGroups
                                        orderby lg.GlobalPrices.TotalPriceEur descending
                                        select lg).FirstOrDefault();

            return HightAmountLineGroup;
        }

        public bool IsDirectLink3DSecureEnable(int creditCardTypeId, bool flag3dsEnable)
        {
            var isD3DEnable = !(!_switchProvider.IsEnabled("op.ogone.enableDirectlink3DSecure", false) && flag3dsEnable);
            return isD3DEnable;
        }

        public bool Is3DSecureEnable(int creditCardTypeId, PopOrderForm popOrderForm, decimal realAmount)
        {
            if (_ogoneBusiness3DSecure.Get3DSecureInfos(realAmount, creditCardTypeId, popOrderForm as IGotUserContextInformations,
                (popOrderForm as IGotLineGroups).LineGroups,
                (popOrderForm as IGotShippingMethodEvaluation).ShippingMethodEvaluation,
                popOrderForm.UserContextInformations.IsGuest) == "Y")
                return true;
            else
                return false;
        }

        public void SetAliasGatewayViewModel(OgoneBillingMethodViewModel oBillingViewModel, OPContext opContext, PopOrderForm popOrderForm)
        {
            if (oBillingViewModel.CreditCards.Any(c => OgoneCreditCardBillingMethod.IsAliasGatewayProcess(c.TransactionProcess)))
            {

                var pspLineGroup = GetHighAmountLineGroupWithoutBillingMethod(popOrderForm.LineGroups);
                var ogoneConfigurationData = popOrderForm.GetPrivateData<OgoneConfigurationData>(false);

                oBillingViewModel.OgoneViewModel.OgoneAliasGatewayNewCardViewModel.Url = _orderTransactionManager.GetOgoneAliasGatewayUrl(_applicationContext.GetSiteContext().Culture);

                //get pspid with ogonebusiness
                var pspId = _ogoneBusiness.GetPspId(pspLineGroup.OrderType, pspLineGroup.Articles, ogoneConfigurationData);

                // Build Parameters for Customer Cards (SHASign include Alias)
                foreach (var cccViewModel in oBillingViewModel.CustomerCreditCards.
                    Where(cc => !cc.IsDumy && OgoneCreditCardBillingMethod.IsAliasGatewayProcess(cc.OgoneTransactionProcess)))
                {
                    var aliasGatewayParam = _ogoneBusiness.BuildOgoneAliasGatewayParameters(pspId, opContext.Pipe.PipeName, _applicationContext.GetSiteContext().Culture, cccViewModel.Alias);
                    cccViewModel.SHASign = _ogoneBusiness.GetSHASignParameterValue(aliasGatewayParam);
                }

                // Build Parameters for New Card
                var parametersForNewCard = _ogoneBusiness.BuildOgoneAliasGatewayParameters(pspId, opContext.Pipe.PipeName, _applicationContext.GetSiteContext().Culture);
                oBillingViewModel.OgoneViewModel.OgoneAliasGatewayNewCardViewModel.Parameters = parametersForNewCard;
            }
        }

        public OgoneAliasGatewayViewModel SetAliasGatewayViewModelForSelfCheckout(CustomerCreditCardViewModel customerCreditCardViewModel, OPContext opContext, PopOrderForm popOrderForm)
        {
            var pspLineGroup = GetHighAmountLineGroupWithoutBillingMethod(popOrderForm.LineGroups);

            var ogoneAliasGatewayViewModel = new OgoneAliasGatewayViewModel
            {
                Url = _orderTransactionManager.GetOgoneAliasGatewayUrl(_applicationContext.GetSiteContext().Culture),
            };

            var shopInfosData = popOrderForm.GetPrivateData<ShopInfosData>(create: false);

            var ogoneConfigurationData = popOrderForm.GetPrivateData<OgoneConfigurationData>(create: false);

            var pspId = _ogoneBusiness.GetPspId(pspLineGroup.OrderType, pspLineGroup.Articles, ogoneConfigurationData);

            var aliasGatewayParam = _ogoneBusiness.BuildOgoneAliasGatewayParameters(pspId, opContext.Pipe.PipeName, _applicationContext.GetSiteContext().Culture, customerCreditCardViewModel.Alias);

            customerCreditCardViewModel.SHASign = _ogoneBusiness.GetSHASignParameterValue(aliasGatewayParam);
            ogoneAliasGatewayViewModel.Parameters = aliasGatewayParam;
            return ogoneAliasGatewayViewModel;
        }
    }
}
