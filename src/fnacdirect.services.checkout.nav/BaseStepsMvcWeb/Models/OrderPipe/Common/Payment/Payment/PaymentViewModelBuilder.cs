using FnacDirect.Contracts.Online.Model;
using FnacDirect.OrderPipe.Base.Business;
using FnacDirect.OrderPipe.Base.Business.LogisticLineGroups;
using FnacDirect.OrderPipe.Base.Business.Payment;
using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.OrderPipe.Base.Model.BillingMethods;
using FnacDirect.OrderPipe.Base.Model.Facets;
using FnacDirect.OrderPipe.Base.Model.Pop;
using FnacDirect.OrderPipe.Base.OneClick;
using FnacDirect.OrderPipe.Base.Proxy.Customer;
using FnacDirect.OrderPipe.Base.Proxy.Nav;
using FnacDirect.OrderPipe.BaseMvc.Web.Models.OrderPipe.Common.Payment;
using FnacDirect.Technical.Framework.Web.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Common.Payment
{
    public class PaymentViewModelBuilder : IPaymentViewModelBuilder
    {
        private readonly IApplicationContext _applicationContext;
        private readonly ITermsAndConditionsBuilder _termsAndConditionsBuilder;
        private readonly IPaymentMethodViewModelFactory _paymentMethodViewModelFactory;
        private readonly IBillingMethodService _billingMethodService;
        private readonly ISummaryViewModelBuilder _summaryViewModelBuilder;
        private readonly IKoboOrangeService _koboOrangeService;
        private readonly IUserExtendedContextProvider _userExtendedContextProvider;
        private readonly ILogisticLineGroupGenerationProcess _logisticLineGroupGenerationProcess;
        private readonly ISwitchProvider _switchProvider;

        private readonly bool _orangePaymentEnable;

        public PaymentViewModelBuilder(IApplicationContext applicationContext,
                                        ITermsAndConditionsBuilder termsAndConditionsBuilder,
                                        ICustomerCreditCardService customerCreditCardService,
                                        IPaymentMethodViewModelFactory paymentMethodViewModelFactory,
                                        IBillingMethodService billingMethodService,
                                        ISummaryViewModelBuilder summaryViewModelBuilder,
                                        IKoboOrangeService koboOrangeService,
                                        IUserExtendedContextProvider userExtendedContextProvider,
                                        ISwitchProvider switchProvider,
                                        ILogisticLineGroupGenerationProcess logisticLineGroupGenerationProcess)
        {
            _applicationContext = applicationContext;
            _termsAndConditionsBuilder = termsAndConditionsBuilder;
            _paymentMethodViewModelFactory = paymentMethodViewModelFactory;
            _billingMethodService = billingMethodService;
            _summaryViewModelBuilder = summaryViewModelBuilder;
            _koboOrangeService = koboOrangeService;
            _userExtendedContextProvider = userExtendedContextProvider;
            _logisticLineGroupGenerationProcess = logisticLineGroupGenerationProcess;
            _switchProvider = switchProvider;
            _orangePaymentEnable = switchProvider.IsEnabled("orderpipe.pop.payment.orange");
        }

        public PaymentViewModel Build(OPContext opContext, PopOrderForm popOrderForm, string selectedTab, string selectedPaymentMethod)
        {
            var model = Build(opContext, popOrderForm, selectedTab);
            if (!string.IsNullOrEmpty(selectedPaymentMethod)
                && model.Tabs.Any(tab => tab.Name.Equals(selectedTab, StringComparison.InvariantCultureIgnoreCase)
                    && tab.BillingMethods.Any(bm => bm.Name.Equals(selectedPaymentMethod, StringComparison.InvariantCultureIgnoreCase))))
            {
                model.Tabs.SelectMany(tab => tab.BillingMethods).ToList().ForEach(bm => bm.IsSelected = false);
                model.Tabs.SelectMany(tab => tab.BillingMethods).First(bm => bm.Name.Equals(selectedPaymentMethod, StringComparison.InvariantCultureIgnoreCase)).IsSelected = true;
            }
            return model;
        }

        public PaymentViewModel Build(OPContext opContext, PopOrderForm popOrderForm, string selectedTab)
        {
            var model = Build(opContext, popOrderForm);
            if (!string.IsNullOrEmpty(selectedTab) && model.Tabs.Any(tab => tab.Name.Equals(selectedTab, StringComparison.InvariantCultureIgnoreCase)))
            {
                model.Tabs.ToList().ForEach(t => t.IsSelected = false);
                model.Tabs.First(tab => tab.Name.Equals(selectedTab, StringComparison.InvariantCultureIgnoreCase)).IsSelected = true;
            }
            return model;
        }

        public PaymentViewModel Build(OPContext opContext, PopOrderForm popOrderForm)
        {
            var paymentViewModel = new PaymentViewModel();

            var oneClickData = popOrderForm.GetPrivateData<OneClickData>(create: false);
            if (oneClickData != null && oneClickData.IsAvailable.HasValue)
            {
                paymentViewModel.IsOneClick = oneClickData.IsAvailable.Value;
            }

            paymentViewModel.DisplayCodePromo = popOrderForm.QuoteInfos == null || popOrderForm.QuoteInfos.QuoteReference == null;

            paymentViewModel.Summary = _summaryViewModelBuilder.Build(opContext, popOrderForm);

            var total = paymentViewModel.Summary.TotalGlobal;

            if (!string.IsNullOrEmpty(paymentViewModel.Summary.RemainingAmount))
            {
                total = paymentViewModel.Summary.RemainingAmount;
            }

            paymentViewModel.Tabs = BuildTabs(opContext, popOrderForm, "tabs.group.payment.methods", total);

            paymentViewModel.UnavailableTabs = BuildUnavailableTabs(opContext, popOrderForm, paymentViewModel.Tabs);

            //for react view
            paymentViewModel.TermsAndConditions = _termsAndConditionsBuilder.GetTermsAndConditionsTypeViewModel(popOrderForm.LineGroups);
            paymentViewModel.SecondaryBillingMethodsTabs = BuildTabs(opContext, popOrderForm, "tabs.group.secondary.payment.methods", string.Empty);

            paymentViewModel.Messages = BuildPaymentMessages(opContext, popOrderForm);

            return paymentViewModel;
        }

        public MessageViewModel BuildPaymentMessages(OPContext opContext, PopOrderForm popOrderForm)
        {
            return new MessageViewModel
            {
                MainPaymentMessage = BuildMainPaymentMessages(opContext, popOrderForm),
                SecondaryPaymentMessage = BuildSecondaryPaymentMessages(popOrderForm, opContext)
            };
        }

        public string BuildSecondaryPaymentMessages(PopOrderForm popOrderForm, OPContext opContext)
        {
            var message = string.Empty;

            var fidelityAllowedDelayAfterExpiration = opContext.Pipe.GlobalParameters.GetAsInt("fidelity.advantages.alloweddelay.after.expiration");

            var disabledBillingMethod = new List<string>();

            var userExtendedContext = _userExtendedContextProvider.GetCurrentUserExtendedContext();
            var customer = userExtendedContext.Customer;
            var isGuest = popOrderForm.UserContextInformations.IsGuest;

            var anonymousBillingMethodsMaxAmount = opContext.Pipe.GlobalParameters.Get<int>("anonymous.billing.methods.max.amount", 1000);

            var paymentSelectionData = popOrderForm.GetPrivateData<PaymentSelectionData>("payment");

            var eccvTypeIds = opContext.Pipe.GlobalParameters.GetAsRangeSet<int>("eccv.typeid");

            var hasECCVInBasket = popOrderForm.LineGroups.GetArticles().Any(a => a.TypeId.HasValue && eccvTypeIds.Contains(a.TypeId.Value));

            var multipleAmountFidelityEuros = opContext.Pipe.GlobalParameters.Get("fidelity.advantages.multiple.amount", 10m);

            if (hasECCVInBasket)
            {
                var disabledBillingMethods = paymentSelectionData.AllowedMethodMask ^ paymentSelectionData.RemainingMethodMask;

                //VirtualGiftCheckBillingMethod
                if (((disabledBillingMethods & VirtualGiftCheckBillingMethod.IdBillingMethod) != 0 || (paymentSelectionData.RemainingMethodMask & VirtualGiftCheckBillingMethod.IdBillingMethod) == 0)
                        && _billingMethodService.IsCcvAllowed(popOrderForm, popOrderForm.LineGroups, popOrderForm.GlobalPrices.TotalPriceEur.GetValueOrDefault(), anonymousBillingMethodsMaxAmount, isGuest, true, true, true))
                {
                    disabledBillingMethod.Add(_applicationContext.GetMessage("orderpipe.pop.payment.billingmethod.ccv"));
                }

                //GiftCardBillingMethod
                if (((disabledBillingMethods & GiftCardBillingMethod.IdBillingMethod) != 0 || (paymentSelectionData.RemainingMethodMask & GiftCardBillingMethod.IdBillingMethod) == 0)
                       && _billingMethodService.IsGiftCardAllowed(popOrderForm, popOrderForm.GlobalPrices.TotalPriceEur.GetValueOrDefault(), anonymousBillingMethodsMaxAmount, isGuest, true, true, true))
                {
                    disabledBillingMethod.Add(_applicationContext.GetMessage("orderpipe.pop.payment.billingmethod.giftcard"));
                }

                //FidelityEurosBillingMethod
                if (((disabledBillingMethods & FidelityEurosBillingMethod.IdBillingMethod) != 0 || (paymentSelectionData.RemainingMethodMask & GiftCardBillingMethod.IdBillingMethod) == 0)
                        && _billingMethodService.IsFidelityEurosAllowed(popOrderForm, popOrderForm, fidelityAllowedDelayAfterExpiration, multipleAmountFidelityEuros, true))
                {
                    var fidelityEuroBusiness = new FidelityBusiness<FidelityEurosBillingMethod>();
                    var amount = fidelityEuroBusiness.GetAvailableAmount(customer);
                    if (amount >= multipleAmountFidelityEuros)
                    {
                        disabledBillingMethod.Add(_applicationContext.GetMessage("orderpipe.pop.payment.billingmethod.cagnotte"));
                    }
                }

                //FidelityPointsOccazBillingMethod
                if (((disabledBillingMethods & FidelityPointsOccazBillingMethod.IdBillingMethod) != 0 || (paymentSelectionData.RemainingMethodMask & GiftCardBillingMethod.IdBillingMethod) == 0)
                        && _billingMethodService.IsFidelityPointsOccazAllowed(popOrderForm, popOrderForm, popOrderForm.UserContextInformations, true, true))
                {
                    disabledBillingMethod.Add(_applicationContext.GetMessage("orderpipe.pop.payment.billingmethod.gaming"));
                }

                //AvoirOccazBillingMethod
                if (((disabledBillingMethods & AvoirOccazBillingMethod.IdBillingMethod) != 0 || (paymentSelectionData.RemainingMethodMask & GiftCardBillingMethod.IdBillingMethod) == 0)
                        && _billingMethodService.IsAvoirOccazAllowed(popOrderForm, fidelityAllowedDelayAfterExpiration, true, true))
                {
                    disabledBillingMethod.Add(_applicationContext.GetMessage("orderpipe.pop.payment.billingmethod.avoirgaming"));
                }

                if (disabledBillingMethod.Any())
                {
                    message = _applicationContext.GetMessage("orderpipe.pop.payment.noteligible.with.eccv.secondary", string.Join(", ", disabledBillingMethod));
                }
            }
            else if (popOrderForm.HasClickAndCollectArticle())
            {
                var billingMethodDisabledForClickAndCollectMask = (popOrderForm.GetPrivateData<PopData>(create: false)?.BillingMethodDisabledForClickAndCollectMask).GetValueOrDefault();

                if (!popOrderForm.OrderBillingMethods.Exists(bm => bm.BillingMethodId == VirtualGiftCheckBillingMethod.IdBillingMethod) && (((billingMethodDisabledForClickAndCollectMask & VirtualGiftCheckBillingMethod.IdBillingMethod) != 0
                        && _billingMethodService.IsCcvAllowed(popOrderForm, popOrderForm.LineGroups, popOrderForm.GlobalPrices.TotalPriceEur.GetValueOrDefault(), anonymousBillingMethodsMaxAmount, false, disableClickAndCollectCheck: true))
                    || (paymentSelectionData.RemainingMethodMask & VirtualGiftCheckBillingMethod.IdBillingMethod) == 0))
                {
                    disabledBillingMethod.Add(_applicationContext.GetMessage("orderpipe.pop.payment.billingmethod.ccv"));
                }

                if (!popOrderForm.OrderBillingMethods.Exists(bm => bm.BillingMethodId == GiftCardBillingMethod.IdBillingMethod) && (((billingMethodDisabledForClickAndCollectMask & GiftCardBillingMethod.IdBillingMethod) != 0
                        && _billingMethodService.IsGiftCardAllowed(popOrderForm, popOrderForm.GlobalPrices.TotalPriceEur.GetValueOrDefault(), anonymousBillingMethodsMaxAmount, false, disableClickAndCollectCheck: true))
                    || (paymentSelectionData.RemainingMethodMask & VirtualGiftCheckBillingMethod.IdBillingMethod) == 0))
                {
                    disabledBillingMethod.Add(_applicationContext.GetMessage("orderpipe.pop.payment.billingmethod.giftcard"));
                }

                if (!popOrderForm.OrderBillingMethods.Exists(bm => bm.BillingMethodId == FidelityEurosBillingMethod.IdBillingMethod) && (billingMethodDisabledForClickAndCollectMask & FidelityEurosBillingMethod.IdBillingMethod) != 0 && _billingMethodService.IsFidelityEurosAllowed(popOrderForm, popOrderForm, fidelityAllowedDelayAfterExpiration, multipleAmountFidelityEuros))
                {
                    var fidelityEuroBusiness = new FidelityBusiness<FidelityEurosBillingMethod>();
                    var amount = fidelityEuroBusiness.GetAvailableAmount(customer);
                    if (amount >= multipleAmountFidelityEuros)
                    {
                        disabledBillingMethod.Add(_applicationContext.GetMessage("orderpipe.pop.payment.billingmethod.cagnotte"));
                    }
                }
                if (!popOrderForm.OrderBillingMethods.Exists(bm => bm.BillingMethodId == FidelityPointsOccazBillingMethod.IdBillingMethod) && (billingMethodDisabledForClickAndCollectMask & FidelityPointsOccazBillingMethod.IdBillingMethod) != 0 && _billingMethodService.IsFidelityPointsOccazAllowed(popOrderForm, popOrderForm, popOrderForm.UserContextInformations, true))
                {
                    disabledBillingMethod.Add(_applicationContext.GetMessage("orderpipe.pop.payment.billingmethod.gaming"));
                }

                if (!popOrderForm.OrderBillingMethods.Exists(bm => bm.BillingMethodId == AvoirOccazBillingMethod.IdBillingMethod) && (billingMethodDisabledForClickAndCollectMask & AvoirOccazBillingMethod.IdBillingMethod) != 0 && _billingMethodService.IsAvoirOccazAllowed(popOrderForm, fidelityAllowedDelayAfterExpiration, true))
                {
                    var avoirOccazBusiness = new FidelityBusiness<AvoirOccazBillingMethod>();

                    var amount = avoirOccazBusiness.GetAvailableAmount(customer);
                    if (amount >= 10)
                    {
                        disabledBillingMethod.Add(_applicationContext.GetMessage("orderpipe.pop.payment.billingmethod.avoirgaming"));
                    }
                }
                if (disabledBillingMethod.Any())
                {
                    if (popOrderForm.LineGroups.Fnac().SelectMany(l => l.Articles).Any(a => a.SteedFromStore))
                    {
                        message = _applicationContext.GetMessage("orderpipe.pop.payment.noteligible.with.steedfromstore.secondary", string.Join(", ", disabledBillingMethod));
                    }
                    else
                    {
                        message = _applicationContext.GetMessage("orderpipe.pop.payment.noteligible.with.clickandcollect.secondary", string.Join(", ", disabledBillingMethod));
                    }
                }
            }
            else if (popOrderForm.OrderBillingMethods.OfType<VoucherBillingMethod>().Any())
            {
                var disabledBillingMethods = paymentSelectionData.AllowedMethodMask ^ paymentSelectionData.RemainingMethodMask;

                if (!popOrderForm.OrderBillingMethods.Exists(bm => bm.BillingMethodId == VirtualGiftCheckBillingMethod.IdBillingMethod) && (((disabledBillingMethods & VirtualGiftCheckBillingMethod.IdBillingMethod) != 0
                        && _billingMethodService.IsCcvAllowed(popOrderForm, popOrderForm.LineGroups, popOrderForm.GlobalPrices.TotalPriceEur.GetValueOrDefault(), anonymousBillingMethodsMaxAmount, isGuest))
                    || (paymentSelectionData.RemainingMethodMask & VirtualGiftCheckBillingMethod.IdBillingMethod) == 0))
                {
                    disabledBillingMethod.Add(_applicationContext.GetMessage("orderpipe.pop.payment.billingmethod.ccv"));
                }

                if (!popOrderForm.OrderBillingMethods.Exists(bm => bm.BillingMethodId == GiftCardBillingMethod.IdBillingMethod) && (((disabledBillingMethods & GiftCardBillingMethod.IdBillingMethod) != 0
                        && _billingMethodService.IsGiftCardAllowed(popOrderForm, popOrderForm.GlobalPrices.TotalPriceEur.GetValueOrDefault(), anonymousBillingMethodsMaxAmount, isGuest))
                    || (paymentSelectionData.RemainingMethodMask & GiftCardBillingMethod.IdBillingMethod) == 0))
                {
                    disabledBillingMethod.Add(_applicationContext.GetMessage("orderpipe.pop.payment.billingmethod.giftcard"));
                }

                if (!popOrderForm.OrderBillingMethods.Exists(bm => bm.BillingMethodId == FidelityEurosBillingMethod.IdBillingMethod) && (disabledBillingMethods & FidelityEurosBillingMethod.IdBillingMethod) != 0 && _billingMethodService.IsFidelityEurosAllowed(popOrderForm, popOrderForm, fidelityAllowedDelayAfterExpiration, multipleAmountFidelityEuros))
                {
                    var fidelityEuroBusiness = new FidelityBusiness<FidelityEurosBillingMethod>();
                    var amount = fidelityEuroBusiness.GetAvailableAmount(customer);
                    if (amount >= multipleAmountFidelityEuros)
                    {
                        disabledBillingMethod.Add(_applicationContext.GetMessage("orderpipe.pop.payment.billingmethod.cagnotte"));
                    }
                }

                if (!popOrderForm.OrderBillingMethods.Exists(bm => bm.BillingMethodId == FidelityPointsOccazBillingMethod.IdBillingMethod) && (disabledBillingMethods & FidelityPointsOccazBillingMethod.IdBillingMethod) != 0 && _billingMethodService.IsFidelityPointsOccazAllowed(popOrderForm, popOrderForm, popOrderForm.UserContextInformations))
                {
                    disabledBillingMethod.Add(_applicationContext.GetMessage("orderpipe.pop.payment.billingmethod.gaming"));
                }

                if (!popOrderForm.OrderBillingMethods.Exists(bm => bm.BillingMethodId == AvoirOccazBillingMethod.IdBillingMethod) && (disabledBillingMethods & AvoirOccazBillingMethod.IdBillingMethod) != 0 && _billingMethodService.IsAvoirOccazAllowed(popOrderForm, fidelityAllowedDelayAfterExpiration))
                {
                    var avoirOccazBusiness = new FidelityBusiness<AvoirOccazBillingMethod>();

                    var amount = avoirOccazBusiness.GetAvailableAmount(customer);
                    if (amount >= 10)
                    {
                        disabledBillingMethod.Add(_applicationContext.GetMessage("orderpipe.pop.payment.billingmethod.avoirgaming"));
                    }
                }
                if (disabledBillingMethod.Any())
                {
                    message = _applicationContext.GetMessage("orderpipe.pop.payment.noteligible.with.voucher", string.Join(", ", disabledBillingMethod));
                }
            }
            else if (popOrderForm.GlobalPrices.TotalPriceEur > anonymousBillingMethodsMaxAmount)
            {
                if (!_billingMethodService.IsCcvAllowed(popOrderForm, popOrderForm.LineGroups, popOrderForm.GlobalPrices.TotalPriceEur.GetValueOrDefault(), anonymousBillingMethodsMaxAmount, isGuest, false, false) && _billingMethodService.IsCcvAllowed(popOrderForm, popOrderForm.LineGroups, popOrderForm.GlobalPrices.TotalPriceEur.GetValueOrDefault(), anonymousBillingMethodsMaxAmount, isGuest, false, true))
                {
                    disabledBillingMethod.Add(_applicationContext.GetMessage("orderpipe.pop.payment.billingmethod.ccv"));
                }

                if (!_billingMethodService.IsGiftCardAllowed(popOrderForm, popOrderForm.GlobalPrices.TotalPriceEur.GetValueOrDefault(), anonymousBillingMethodsMaxAmount, isGuest, false, false) && _billingMethodService.IsGiftCardAllowed(popOrderForm, popOrderForm.GlobalPrices.TotalPriceEur.GetValueOrDefault(), anonymousBillingMethodsMaxAmount, isGuest, false, true))
                {
                    disabledBillingMethod.Add(_applicationContext.GetMessage("orderpipe.pop.payment.billingmethod.giftcard"));
                }
                if (disabledBillingMethod.Any())
                {
                    message = _applicationContext.GetMessage("orderpipe.pop.payment.noteligible.with.greater.max.amount",
                            string.Join(_applicationContext.GetMessage("orderpipe.pop.payment.noteligible.with.greater.max.amount.separator"), disabledBillingMethod));
                }
            }

            return message;
        }

        public string BuildMainPaymentMessages(OPContext opContext, PopOrderForm popOrderForm)
        {
            var message = string.Empty;

            var disabledBillingMethod = new List<string>();

            var eccvTypeIds = opContext.Pipe.GlobalParameters.GetAsRangeSet<int>("eccv.typeid");

            var hasECCVInBasket = popOrderForm.LineGroups.GetArticles().Any(a => a.TypeId.HasValue && eccvTypeIds.Contains(a.TypeId.Value));

            if (hasECCVInBasket)
            {
                message = _applicationContext.GetMessage("orderpipe.pop.payment.noteligible.with.eccv.main");
            }
            else if (popOrderForm.HasClickAndCollectArticle())
            {
                var popData = popOrderForm.GetPrivateData<PopData>(create: false);

                if (popData != null && (popData.BillingMethodDisabledForClickAndCollectMask & PayPalBillingMethod.IdBillingMethod) != 0 && _billingMethodService.IsPaypalAllowed(popOrderForm))
                {
                    disabledBillingMethod.Add(_applicationContext.GetMessage("orderpipe.pop.payment.billingmethod.paypal"));
                }

                if (_billingMethodService.IsPhonePaymentAllowed(opContext, popOrderForm))
                {
                    disabledBillingMethod.Add(_applicationContext.GetMessage("orderpipe.pop.payment.billingmethod.phone"));
                }

                if (disabledBillingMethod.Any())
                {
                    if (popOrderForm.LineGroups.Fnac().SelectMany(l => l.Articles).Any(a => a.SteedFromStore))
                    {
                        message = _applicationContext.GetMessage("orderpipe.pop.payment.noteligible.with.steedfromstore.main", string.Join(", ", disabledBillingMethod));
                    }
                    else
                    {
                        message = _applicationContext.GetMessage("orderpipe.pop.payment.noteligible.with.clickandcollect.main", string.Join(", ", disabledBillingMethod));
                    }
                }
            }
            return message;
        }

        public PromotionViewModel BuildPromotionCode(string code, string description, string errorMessage)
        {
            return new PromotionViewModel
            {
                Code = code,
                Description = description,
                ErrorMessage = errorMessage
            };
        }

        public PromotionViewModel BuildPromotionCode(PopOrderForm popOrderForm)
        {
            var advantageCode = popOrderForm.OrderGlobalInformations.AdvantageCode;

            //Advantage Code ?
            if (!string.IsNullOrEmpty(advantageCode))
            {
                var advantageCodeDescription = string.Empty;
                var art = popOrderForm.LineGroups.SelectMany(lg => lg.Articles.OfType<BaseArticle>()).FirstOrDefault(a => a.Promotions.Any(p => advantageCode.Equals(p.AdvantageCode, StringComparison.CurrentCultureIgnoreCase)));

                var isShippingPromo = false;
                if (art == null) //Si l'article est null on vérifie que la promotion n'est pas appliqué au frais de ports.
                {
                    art = popOrderForm.LineGroups.SelectMany(lg => lg.Articles.OfType<StandardArticle>()).FirstOrDefault(a => a.ShippingPromotionRules.Any(spr => spr.Promotion.AdvantageCode != null && spr.Promotion.AdvantageCode.Equals(advantageCode, StringComparison.CurrentCultureIgnoreCase)));
                    isShippingPromo = art != null;
                }

                //Promotion applied ?
                if (art != null)
                {
                    if (art.Promotions.Any() && !isShippingPromo)
                    {
                        advantageCodeDescription = art.Promotions.First(p => p.AdvantageCode != null && p.AdvantageCode.Equals(advantageCode, StringComparison.CurrentCultureIgnoreCase)).DisplayText;
                    }
                    else
                    {
                        advantageCodeDescription = art.ShippingPromotionRules.First(spr => spr.Promotion.AdvantageCode != null && spr.Promotion.AdvantageCode.Equals(advantageCode, StringComparison.CurrentCultureIgnoreCase)).Promotion.ArticleText;
                    }
                }
                else
                {
                    if ((ShoppingCartMessageCode)popOrderForm.OrderGlobalInformations.MessageCodes == ShoppingCartMessageCode.AdvantageCodeValidAndApplied)
                    {
                        return BuildPromotionCode(advantageCode, string.Empty, string.Empty);
                    }
                    popOrderForm.OrderGlobalInformations.AdvantageCode = string.Empty;
                    return BuildPromotionCode(string.Empty, string.Empty, string.Empty);
                }

                return BuildPromotionCode(advantageCode, advantageCodeDescription, string.Empty);
            }

            //CUU ?
            var voucherBillingMethod = popOrderForm.OrderBillingMethods.OfType<VoucherBillingMethod>().FirstOrDefault();

            var popData = popOrderForm.GetPrivateData<PopData>(create: false);

            if (voucherBillingMethod != null)
            {
                return BuildPromotionCode(voucherBillingMethod.CheckNumber, string.Empty, string.Empty);
            }

            if (popData != null && popData.DisplayRemovedVoucherNotification && popOrderForm.HasClickAndCollectArticle() && (popData.BillingMethodDisabledForClickAndCollectMask & VoucherBillingMethod.IdBillingMethod) != 0)
            {
                return BuildPromotionCode(string.Empty, string.Empty, _applicationContext.GetMessage("orderpipe.pop.payment.noteligible.voucher.with.clickandcollect"));
            }

            return BuildPromotionCode(string.Empty, string.Empty, string.Empty);
        }

        public IEnumerable<UnavailablePaymentViewModel> BuildUnavailableTabs(OPContext opContext, PopOrderForm popOrderForm, IEnumerable<TabPaymentMethodsModel> visibleTabs)
        {
            if (!_switchProvider.IsEnabled("orderpipe.pop.unavailable.payment.tabs") || opContext.Pipe.GlobalParameters["tabs.group.payment.methods.showIfUnavailable"] == null)
            {
                return Enumerable.Empty<UnavailablePaymentViewModel>();
            }
            var configuration = opContext.Pipe.GlobalParameters.GetAs<TabsGroupPaymentMethods>("tabs.group.payment.methods.showIfUnavailable");

            var tabsConfig = configuration.TabGroup.OrderBy(t => t.Position);
            var remainingTabs = tabsConfig
                .Where(m => !visibleTabs.Select(t => t.Name).Contains(m.Name));

            return remainingTabs
                .Select(t => new UnavailablePaymentViewModel()
                {
                    Type = t.Type,
                    Name = t.Name
                });
        }

        public IEnumerable<TabPaymentMethodsModel> BuildTabs(OPContext opContext, PopOrderForm popOrderForm, string parameterName, string totalGlobal)
        {
            var tabs = new List<TabPaymentMethodsModel>();
            var paymentSelectionData = popOrderForm.GetPrivateData<PaymentSelectionData>("payment");
            var listBillingDescription = paymentSelectionData.RemainingMethods;
            var multiFacet = MultiFacet<IGotLineGroups, IGotBillingInformations, IGotShippingMethodEvaluation>.From(popOrderForm);

            if (listBillingDescription == null)
            {
                return tabs;
            }

            if (_orangePaymentEnable)
            {
                var orangeData = popOrderForm.GetPrivateData<OrangeData>("OrangeData", create: false);
                if (orangeData != null)
                {
                    orangeData.ShowOrangeBanner = _koboOrangeService.IsValidOrangeRequest();
                }
            }

            var tabsConfig = opContext.Pipe.GlobalParameters.GetAs<TabsGroupPaymentMethods>(parameterName);
            var billingMethodsManager = ((BaseOP)opContext.Pipe).BillingMethodsManager;

            foreach (var orderBillingMethod in popOrderForm.OrderBillingMethods)
            {
                if (!listBillingDescription.Any(x => x.Id == orderBillingMethod.BillingMethodId && x.Name == orderBillingMethod.Name))
                {
                    listBillingDescription.Add(billingMethodsManager.GetBillingMethodDescriptor(orderBillingMethod.BillingMethodId));
                }
            }

            var isCacf = _switchProvider.IsEnabled("orderpipe.pop.payment.installmentpayments.cacf.enabled");

            foreach (var tabConfig in tabsConfig.TabGroup.OrderBy(tab => tab.Position))
            {
                var tabModel = new TabPaymentMethodsModel
                {
                    Name = tabConfig.Name,
                    Type = tabConfig.Type
                };

                var tabShouldBeDisplayedFirst = false;

                foreach (var paymentMethod in tabConfig.PaymentMethod)
                {
                    var billingDescription = listBillingDescription.FirstOrDefault(bd => bd.Name == paymentMethod.Name || bd.Type.Name == paymentMethod.Name);

                    if (billingDescription == null)
                    {
                        continue;
                    }

                    if (tabConfig.Name == "Installment")
                    {
                        if (isCacf && popOrderForm.OrderBillingMethods.Any())
                        {
                            continue;
                        }

                        if (isCacf && _logisticLineGroupGenerationProcess.SimulateInitialize(multiFacet).Count() > 1)
                        {
                            continue;
                        }
                    }

                    //could it be move into a builder "BillingMethodViewModelBuilder"?
                    var billingMethodViewModel = _paymentMethodViewModelFactory.GetBillingMethodViewModel(billingDescription, paymentMethod, popOrderForm, opContext, tabConfig.Name);

                    //could it be move into BillingMethodViewModelBuilder ?
                    billingMethodViewModel.TermsAndConditions = _termsAndConditionsBuilder.GetTermsAndConditionsFor(popOrderForm.LineGroups);

                    //could it be move into BillingMethodViewModelBuilder as well ?
                    if (!string.IsNullOrEmpty(totalGlobal))
                    {
                        billingMethodViewModel.TotalGlobal = totalGlobal;
                    }

                    if (billingMethodViewModel.Display)
                    {
                        tabModel.AddBillingMethod(billingMethodViewModel);
                        tabModel.IsSelected = billingMethodViewModel.IsSelected;
                    }

                    if (billingMethodViewModel.DisplayFirst)
                    {
                        tabShouldBeDisplayedFirst = true;
                    }
                }

                if (tabModel.BillingMethods.Any())
                {
                    if (tabShouldBeDisplayedFirst)
                    {
                        tabs.Insert(0, tabModel);
                    }
                    else
                    {
                        tabs.Add(tabModel);
                    }
                }
            }

            var tabToDisplayAlone = tabs.FirstOrDefault(t => t.BillingMethods.Any(bm => bm.DisplayAlone));
            if (tabToDisplayAlone != null)
            {
                return tabToDisplayAlone.Yield();
            }

            return tabs;
        }
    }
}
