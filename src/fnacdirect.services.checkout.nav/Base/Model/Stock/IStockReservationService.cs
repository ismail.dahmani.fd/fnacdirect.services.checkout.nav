﻿using FnacDirect.OrderPipe.Base.LineGroups;
using System.Collections.Generic;

namespace FnacDirect.OrderPipe.Base.Business.Stock
{
    public interface IStockReservationService
    {
        void Reserve(IEnumerable<ILogisticLineGroup> lineGroups, string market, int userid);
    }
}
