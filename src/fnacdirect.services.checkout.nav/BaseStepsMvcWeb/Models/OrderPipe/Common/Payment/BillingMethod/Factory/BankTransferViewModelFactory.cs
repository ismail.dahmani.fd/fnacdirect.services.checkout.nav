﻿using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.OrderPipe.Core.Services;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Common.Payment.BillingMethodFactories
{
    public class BankTransferViewModelFactory : BasePaymentViewModelFactory<BankTransferBillingMethod>
    {
        private readonly IPipeParametersProvider _pipeParametersProvider;

        public BankTransferViewModelFactory(IPipeParametersProvider pipeParametersProvider)
        {
            _pipeParametersProvider = pipeParametersProvider;
        }

        public override BillingMethodViewModel Get(string tabName, PaymentMethod paymentMethod, OPContext opContext, PopOrderForm popOrderForm)
        {
            return new BankTransfertMethodViewModel()
            {
                TransfertAccount = _pipeParametersProvider.Get<string>("billing.banktransfer.account"),
                Display = true
            };
        }
    }
}
