namespace FnacDirect.OrderPipe.Base.Model
{
    public class AccountId
    {
        public int? AccountPk { get; set; } = null;
        public int? AccountStatus { get; set; } = null;
    }
}
