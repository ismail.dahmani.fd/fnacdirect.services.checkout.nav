﻿using System.Collections.Generic;
using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.Technical.Framework.CoreServices;

namespace FnacDirect.OrderPipe.Base.Business
{
    public class AdditionalFeeBusiness : IAdditionalFeeBusiness
    {
        public enum FeeCalculationRule { Max, Sum, MaxB }

        public delegate IEnumerable<IAdditionalFeeMethod> GetAvailableFeeListDelegate(LogisticType logtype);
        public delegate int? GetChosenFeeDelegate(LogisticType logtype);

        public void CalculateGlobalFeePrices(LogisticTypeList logTypes, FeeCalculationRule calcRule, GetAvailableFeeListDelegate delegAvailable, GetChosenFeeDelegate delegChosen, out decimal nTotalshippingPriceDbEUR, out decimal nTotalshippingPriceUserEUR, out decimal nTotalshippingPriceUserNoVatEUR)
        {

            nTotalshippingPriceDbEUR = 0m;
            nTotalshippingPriceUserEUR = 0m;
            nTotalshippingPriceUserNoVatEUR = 0m;

            switch (calcRule)
            {
                case FeeCalculationRule.MaxB:
                    {
                        // pass 1
                        IAdditionalFeeMethod oMaxMethod = null;

                        foreach (var lt in logTypes)
                        {
                            var oCurrentShipMethod = GetChosenMethod(delegAvailable, delegChosen, lt);

                            if (oCurrentShipMethod != null)
                            {
                                if (oMaxMethod == null || (oCurrentShipMethod.PriceGlobalEur.Value > oMaxMethod.PriceGlobalEur.Value))
                                {
                                    oMaxMethod = oCurrentShipMethod;
                                }
                            }
                        }

                        // pass 2

                        foreach (var lt in logTypes)
                        {
                            var oCurrentShipMethod = GetChosenMethod(delegAvailable, delegChosen, lt);

                            foreach (var oShipMethod in delegAvailable(lt))
                            {
                                if (oShipMethod != oMaxMethod)
                                {
                                    oShipMethod.CurrentPriceEUR -= oShipMethod.PriceGlobalEur.Value;
                                    oShipMethod.PriceGlobalEur = 0;
                                    oShipMethod.PriceGlobalNoVatEUR = 0;
                                }
                            }
                            if (oCurrentShipMethod != null)
                            {
                                nTotalshippingPriceDbEUR += oCurrentShipMethod.CurrentPriceDbEUR.GetValueOrDefault(0);
                                nTotalshippingPriceUserEUR += oCurrentShipMethod.CurrentPriceEUR.GetValueOrDefault(0);
                                nTotalshippingPriceUserNoVatEUR += oCurrentShipMethod.CurrentPriceNoVatEUR.GetValueOrDefault(0);
                            }
                        }
                    }
                    break;
                case FeeCalculationRule.Max:
                    {

                        // pass 1
                        IAdditionalFeeMethod oMaxMethod = null;

                        foreach (var lt in logTypes)
                        {
                            var oCurrentShipMethod = GetChosenMethod(delegAvailable, delegChosen, lt);

                            if (oCurrentShipMethod != null)
                            {
                                if (oMaxMethod == null || (oCurrentShipMethod.CurrentPriceEUR.Value > oMaxMethod.CurrentPriceEUR.Value))
                                {
                                    oMaxMethod = oCurrentShipMethod;
                                }
                            }
                        }

                        // pass 2

                        foreach (var lt in logTypes)
                        { 
                            var oCurrentShipMethod = GetChosenMethod(delegAvailable, delegChosen, lt);

                            foreach (var oShipMethod in delegAvailable(lt))
                            {
                                if (oShipMethod != oMaxMethod)
                                {
                                    oShipMethod.CurrentPriceDbEUR = 0;
                                    oShipMethod.CurrentPriceEUR = 0;
                                    oShipMethod.CurrentPriceNoVatEUR = 0;
                                    oShipMethod.PriceDetailEur = 0;
                                    oShipMethod.PriceDetailNoVatEUR = 0;
                                    oShipMethod.PriceGlobalEur = 0;
                                    oShipMethod.PriceGlobalNoVatEUR = 0;
                                }
                            }
                            if (oCurrentShipMethod != null)
                            {
                                nTotalshippingPriceDbEUR += oCurrentShipMethod.CurrentPriceDbEUR.GetValueOrDefault(0);
                                nTotalshippingPriceUserEUR += oCurrentShipMethod.CurrentPriceEUR.GetValueOrDefault(0);
                                nTotalshippingPriceUserNoVatEUR += oCurrentShipMethod.CurrentPriceNoVatEUR.GetValueOrDefault(0);
                            }
                        }


                    }
                    break;

                case FeeCalculationRule.Sum:

                    foreach (var lt in logTypes)
                    {
                        var oCurrentShipMethod = GetChosenMethod(delegAvailable, delegChosen, lt);

                        if (oCurrentShipMethod != null)
                        {
                            nTotalshippingPriceDbEUR += oCurrentShipMethod.CurrentPriceDbEUR.GetValueOrDefault(0);
                            nTotalshippingPriceUserEUR += oCurrentShipMethod.CurrentPriceEUR.GetValueOrDefault(0);
                            nTotalshippingPriceUserNoVatEUR += oCurrentShipMethod.CurrentPriceNoVatEUR.GetValueOrDefault(0);
                        }
                    }
                    break;

            }
        }

        private static IAdditionalFeeMethod GetChosenMethod(GetAvailableFeeListDelegate delegAvailable, GetChosenFeeDelegate delegChosen, LogisticType lt)
        {
            IAdditionalFeeMethod oCurrentShipMethod = null;

            var chosen = delegChosen(lt);
            foreach (var oShipMethod in delegAvailable(lt))
            {
                if (chosen.HasValue && oShipMethod.Id.HasValue &&
                    (chosen.Value == oShipMethod.Id.Value))
                {
                    oCurrentShipMethod = oShipMethod;
                    break;
                }
            }
            return oCurrentShipMethod;
        }

        public void CalculateFeePrices(ArticleList articles, LogisticTypeList logTypes, GetAvailableFeeListDelegate deleg)
        {
            foreach (var art in articles)
            {

                int nQty;
                LogisticType dicLT;

                if (art.Type == ArticleType.Saleable)
                {
                    nQty = art.Quantity ?? 0;

                    if (art.LogType.HasValue)
                    {
                        dicLT = logTypes.GetByIndex(art.LogType.Value);
                        if (dicLT != null && dicLT.InBasket == 1)
                            AddAdditionnalFeePrice(nQty, deleg(dicLT));
                    }
                    else
                    {
                        throw new BusinessException("Item has no logistic type");
                    }

                    if (art is StandardArticle stdArt)
                    {
                        foreach (var service in stdArt.Services)
                        {
                            if (service.Type == ArticleType.Saleable)
                            {
                                nQty = (service.Quantity ?? 0);
                                if (service.LogType.HasValue)
                                {
                                    dicLT = logTypes.GetByIndex(service.LogType.Value);
                                    if (dicLT != null && dicLT.InBasket == 1)
                                        AddAdditionnalFeePrice(nQty, deleg(dicLT));
                                }
                                else
                                {
                                    throw new BusinessException("Item has no logistic type");
                                }
                            }
                        }
                    }
                }
                else
                {
                    if (art.LogType.HasValue)
                    {
                        dicLT = logTypes.GetByIndex(art.LogType.Value);
                        if (dicLT != null && dicLT.InBasket == 1)
                        {
                            foreach (var oFeeMeth in deleg(dicLT))
                            {
                                if (!oFeeMeth.CurrentPriceDbEUR.HasValue)
                                {
                                    // database price
                                    oFeeMeth.CurrentPriceDbEUR = 0m;
                                    // theoritical price
                                    oFeeMeth.CurrentPriceTheoEUR = 0m;
                                    // actual price 
                                    oFeeMeth.CurrentPriceEUR = 0m;
                                    // no vat price 
                                    oFeeMeth.CurrentPriceNoVatEUR = 0m;
                                }

                            }
                        }
                    }


                }
            }
        }

        private void AddAdditionnalFeePrice(int nQty, IEnumerable<IAdditionalFeeMethod> availableMethod)
        {
            foreach (var oFeeMeth in availableMethod)
            {
                // add the item contribution in each available shipping method
                if (!oFeeMeth.CurrentPriceDbEUR.HasValue)
                {
                    // database price
                    oFeeMeth.CurrentPriceDbEUR = oFeeMeth.PriceGlobalDBEur ?? 0m;
                    // theoritical price
                    oFeeMeth.CurrentPriceTheoEUR = oFeeMeth.PriceGlobalDBEur ?? 0m;
                    // actual price 
                    oFeeMeth.CurrentPriceEUR = oFeeMeth.PriceGlobalEur ?? 0m;
                    // no vat price 
                    oFeeMeth.CurrentPriceNoVatEUR = oFeeMeth.PriceGlobalNoVatEUR;
                }
                // add the a*n of ax+b
                // database price 
                oFeeMeth.CurrentPriceTheoEUR += nQty * oFeeMeth.PriceDetailDBEur.GetValueOrDefault(0);
                // database price 
                oFeeMeth.CurrentPriceDbEUR += nQty * oFeeMeth.PriceDetailDBEur.GetValueOrDefault(0);
                // actual price 
                oFeeMeth.CurrentPriceEUR += nQty * oFeeMeth.PriceDetailEur.GetValueOrDefault(0);
                // no vat price 
                oFeeMeth.CurrentPriceNoVatEUR += nQty * oFeeMeth.PriceDetailNoVatEUR.GetValueOrDefault(0);

            }
        }


    }
}
