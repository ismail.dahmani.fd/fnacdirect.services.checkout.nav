﻿namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Basket
{
    public interface IRichBasketItemViewModelBuilderProvider
    {
        IRichBasketItemViewModelBuilder Get(RichBasketItemViewModelBuilderContext context);
    }
}
