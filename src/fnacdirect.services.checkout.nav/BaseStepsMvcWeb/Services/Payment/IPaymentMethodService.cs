using FnacDirect.Customer.Model;
using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.OrderPipe.Base.Model.LineGroups;
using FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Common.Payment;
using System.Collections.Generic;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Services.Pop.Payment
{
    public interface IPaymentMethodService
    {
        List<CustCreditCardDescriptor> GetCreditCardsDescriptor();
        string ApplyMask(string cardNumber);
        IEnumerable<int> CreditCards(OPContext opContext, PopOrderForm popOrderForm, string config, string culture);
        IEnumerable<int> GetEligiblePaymentMethodesIds(OPContext opContext, string config);
        ILineGroup GetHighAmountLineGroupWithoutBillingMethod(List<PopLineGroup> lineGroups);
        bool IsDirectLink3DSecureEnable(int creditCardTypeId, bool flag3dsEnable);
        bool Is3DSecureEnable(int creditCardTypeId, PopOrderForm popOrderForm, decimal realAmount);
        void SetAliasGatewayViewModel(OgoneBillingMethodViewModel oBillingViewModel, OPContext opContext, PopOrderForm popOrderForm);
        OgoneAliasGatewayViewModel SetAliasGatewayViewModelForSelfCheckout(CustomerCreditCardViewModel customerCreditCardViewModel, OPContext opContext, PopOrderForm popOrderForm);
    }
}
