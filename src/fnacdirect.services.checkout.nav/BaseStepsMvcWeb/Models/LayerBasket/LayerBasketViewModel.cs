using FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Basket;
using FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Basket.Children;
using System.Collections.Generic;
using System.Linq;


namespace FnacDirect.OrderPipe.BaseMvc.Web.LayerBasket
{
    public class LayerBasketViewModel
    {
        public IReadOnlyCollection<RichBasketItemViewModel> Articles { get; set; }

        public string OrderPipeUrl { get; set; }
        public string OrderPipeOneclickUrl { get; set; }

        public LayerBasketViewModel()
        {
            Articles = Enumerable.Empty<RichBasketItemViewModel>().ToList().AsReadOnly();
        }
        
        public string Total { get; set; }

        public string TotalNoVAT { get; set; }

        public string LiteralTotal { get; set; }

        public bool IsFree { get; set; }

        public int Count
        {
            get
            {
                return (Articles.Sum(a => (a.Children.Where(c => c.GetType() != typeof(CrossSellRichItemChildrenViewModel)).Count() * a.Quantity.Count) + a.Quantity.Count));
            }
        }

        public bool IsShop { get; set; }
        public bool HasAsideItems { get; set; }

        public ReducedPriceViewModel ReducedPriceBook5pc { get; set; }

        public string TotalReduced { get; set; }

        public string TotalReducedNoVAT { get; set; }

        public string DifferedSavedAmount { get; set; }

        public string ChristmasWording { get; set; }

        public bool HasChristmasWording
        {
            get
            {
                return !string.IsNullOrEmpty(ChristmasWording);
            }
        }
    }
}
