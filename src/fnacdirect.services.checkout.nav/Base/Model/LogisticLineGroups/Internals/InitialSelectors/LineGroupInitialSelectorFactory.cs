﻿using System;

namespace FnacDirect.OrderPipe.Base.Business.LogisticLineGroups.Internals.InitialSelectors
{
    public class LineGroupInitialSelectorFactory<T> : ILineGroupInitialSelectorFactory<T>
            where T : BaseLineGroupInitialSelector
    {
        private readonly Func<T> _factory;

        public LineGroupInitialSelectorFactory(Func<T> factory)
        {
            _factory = factory;
        }

        public ILineGroupInitialSelector Get(ILogisticLineGroupGenerationProcessContext context)
        {
            var instance = _factory();

            instance.Context = context;

            return instance;
        }
    }
}
