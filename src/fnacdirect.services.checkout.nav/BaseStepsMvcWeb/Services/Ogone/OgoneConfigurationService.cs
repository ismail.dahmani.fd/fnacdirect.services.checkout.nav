using FnacDirect.Ogone.BLL;
using FnacDirect.Ogone.Configuration;
using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.OrderPipe.Core.Services;
using System.Text.RegularExpressions;
using System.Collections.Generic;

namespace FnacDirect.OrderPipe.Base.Proxy.Ogone
{
    public class OgoneConfigurationService : IOgoneConfigurationService
    {
        private const string PSPID_FNAC = "PSPIDFNAC";
        private const string PSPID_MP = "PSPIDMP";
        private const string PSPID_DEMAT = "PSPIDDEMAT";
        private const string PSPID_EBOOK = "PSPIDEBOOK";
        private const string PSPID_FNAC_MOTO = "PSPIDMOTO";
        private const string PSPID_MP_MOTO = "PSPIDMPMOTO";
        private readonly IPipeParametersProvider _pipeParameterProvider;
        private readonly IOgoneToolManager _ogoneToolManager;
        private readonly IOgoneConfigurationProvider _ogoneConfigurationProvider;
        private readonly ISwitchProvider _switchProvider;

        public OgoneConfigurationService(IOgoneConfigurationProvider ogoneConfigurationProvider,
            IPipeParametersProvider pipeParameterProvider,
            IOgoneToolManager ogoneToolManager,
            ISwitchProvider switchProvider)
        {
            _ogoneConfigurationProvider = ogoneConfigurationProvider;
            _pipeParameterProvider = pipeParameterProvider;
            _ogoneToolManager = ogoneToolManager;
            _switchProvider = switchProvider;
        }

        private AOgoneConfigurationManager ConfigurationManager
        {
            get { return _ogoneConfigurationProvider.GetOgoneConfigurationManager(); }
        }

        public PspMode GetPspMode()
        {
            return ConfigurationManager.PspMode;
        }

        public bool GetWalletEnabled()
        {
            return ConfigurationManager.WalletEnabled;
        }

        public decimal GetThresholdAmount()
        {
            return ConfigurationManager.ThresholdAmount;
        }

        public AutorisationRule GetTransactionRule(string culture, string debitType, string brand, string orderTransactionRuleType)
        {
            return ConfigurationManager.GetTransactionRule(culture, debitType, brand, orderTransactionRuleType);
        }

        public NCErrorCode GetNcErrorCodeByKey(string ncErrorCode)
        {
            return ConfigurationManager.GetNCErrorCodeByKey(ncErrorCode);
        }

        public string GetBrandExceptionListByCriteria(string culture, string debitType, string brand)
        {
            return ConfigurationManager.GetBrandExceptionListByCriteria(culture, debitType, brand);
        }

        public bool GetBrandAliasByMerchantByCriteria(string culture, string debitType, string brand)
        {
            return ConfigurationManager.GetBrandAliasByMerchantByCriteria(culture, debitType, brand);
        }

        public string EncodeTo64(string pToEncode)
        {
            return _ogoneToolManager.EncodeTo64(pToEncode);
        }

        #region Ogone AliasGateway
        public NCErrorCode GetAliasGatewayNcErrorCodeByKey(string ncErrorCode)
        {
            return _ogoneConfigurationProvider.GetOgoneAliasConfigurationManager().GetNCErrorCodeByKey(ncErrorCode);
        }
        #endregion Ogone AliasGateway

        #region Ogone DirectLink
        public NCErrorCode GetDirectLinkNcErrorCodeByKey(string ncErrorCode)
        {
            return _ogoneConfigurationProvider.GetOgoneDirectLinkConfigurationManager().GetNCErrorCodeByKey(ncErrorCode);
        }


        #endregion Ogone DirectLink 

        public string ComputePspId(OrderTypeEnum orderType, IEnumerable<Article> articles, string accountId, OgoneConfigurationData ogoneConfigurationData)
        {
            string pspIdKey;
            var isfromCallCenter = ogoneConfigurationData != null && ogoneConfigurationData.IsfromCallCenter;
            switch (orderType)
            {
                case OrderTypeEnum.MarketPlace:
                    pspIdKey = isfromCallCenter ? PSPID_MP_MOTO : PSPID_MP;
                    break;
                case OrderTypeEnum.Numerical:
                    pspIdKey = articles.AreAllEbooks() ? PSPID_EBOOK : PSPID_DEMAT;
                    break;
                case OrderTypeEnum.DematSoft:
                    pspIdKey = PSPID_DEMAT;
                    break;
                default:
                    pspIdKey = isfromCallCenter ? PSPID_FNAC_MOTO : PSPID_FNAC;
                    break;
            }

            return ConfigurationManager.InputParams[pspIdKey].Value;
        }

        public string GetPhoneCountryCodeToAddForInstallment()
        {
            return ConfigurationManager.PhoneCountryCodeToAddForInstallment;
        }
    }
}
