using FnacDirect.OrderPipe.Base.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FnacDirect.OrderPipe.Base.Business.Mailing
{
    public interface IMailingOrderModelBuilder
    {
        Order Build(PopOrderForm popOrderForm, bool isPro);
    }
}
