using System.Linq;
using FnacDirect.OrderPipe.Base.Proxy.Membership;
using FnacDirect.Technical.Framework.Web.Mvc;
using FnacDirect.Basket.Business;

namespace FnacDirect.OrderPipe.Base.Business.ShoppingCart
{
    public class ShoppingCartService : IShoppingCartService
    {
        private readonly IApplicationContext _applicationContext;
        private readonly IShoppingCartPersistantConfigurationService _shoppingCartPersistantConfigurationService;
        private readonly IMembershipConfigurationService _membershipConfigurationService;

        public ShoppingCartService(IApplicationContext applicationContext,
                                   IShoppingCartPersistantConfigurationService shoppingCartPersistantConfigurationService,
                                   IMembershipConfigurationService membershipConfigurationService)
        {
            _applicationContext = applicationContext;
            _shoppingCartPersistantConfigurationService = shoppingCartPersistantConfigurationService;
            _membershipConfigurationService = membershipConfigurationService;
        }

        /// <summary>
        /// Retourne tous les orderforms (SID et UID et fusionné) du ShoppingCart
        /// (utilisé par le panier persistant pour les transferts entre Pipe et ShoppingCart)
        /// </summary>
        public void RetrieveShoppingCartOrderforms(string uid, string sid, ShoppingCartEntities.ShoppingCartType typeOfCart, out ShoppingCartEntities.Orderform ofSID, out ShoppingCartEntities.Orderform ofUID, out ShoppingCartEntities.Orderform ofFusionned, out bool ofUIDExists, ShoppingCartEntities.FusionType fusionType = ShoppingCartEntities.FusionType.Default)
        {
            ofUID = null;
            ofSID = BasketBusiness.Load(typeOfCart, sid);
            ofUIDExists = false;
            if (!string.IsNullOrEmpty(uid))
            {
                ofUID = BasketBusiness.Load(typeOfCart, uid);
                if (ofUID != null)
                {
                    ofFusionned = OrderFormExtension.Fusion(ofSID, ofUID, fusionType, _applicationContext.GetSiteContext());

                    if (_shoppingCartPersistantConfigurationService.IsActive)
                    {
                        // Il faut éviter de se retrouver avec deux cartes adhérentes (ex une adhésion + une renouvellement) dans le même panier
                        var adherentCardArticle = _membershipConfigurationService.GetAdherentCardArticles();

                        if (adherentCardArticle != null)
                        {
                            var cards = ofFusionned.Articles
                                .Cast<ShoppingCartEntities.Article>()
                                .Where(a => a.ProductID.HasValue && adherentCardArticle.Any(c => c.Prid == a.ProductID.Value))
                                .OrderByDescending(a => a.InsertDate)
                                .ToList();

                            if (cards.Count > 1)
                            {
                                foreach (var card in cards.Skip(1).ToList())
                                {
                                    ofFusionned.Articles.Remove(card);
                                }
                            }
                        }
                    }

                    var subscriptions = ofFusionned.Articles
                        .Cast<ShoppingCartEntities.Article>()
                        .Where(a => a.ProductID.HasValue && a.ProductID.Value == 6967027)
                        .OrderByDescending(a => a.InsertDate)
                        .ToList();

                    if (subscriptions.Count > 1)
                    {
                        foreach (var card in subscriptions.Skip(1).ToList())
                        {
                            ofFusionned.Articles.Remove(card);
                        }
                    }

                    subscriptions = ofFusionned.Articles
                         .Cast<ShoppingCartEntities.Article>()
                         .Where(a => a.ProductID.HasValue && a.ProductID.Value == 6967027)
                         .OrderByDescending(a => a.InsertDate)
                         .ToList();

                    if (subscriptions.Count == 1 && subscriptions[0].Quantity > 1)
                    {
                        subscriptions[0].Quantity = 1;
                    }

                    var unknownArticles = ofFusionned.Articles.Cast<ShoppingCartEntities.Article>().Where(a => a.ProductID.GetValueOrDefault() == 0).ToList();
                    unknownArticles.ForEach(ofFusionned.Articles.Remove);

                    ofUIDExists = true;
                }
                else
                    ofFusionned = ofSID;
            }
            else
            {
                ofFusionned = ofSID;
            }
        }
    }
}
