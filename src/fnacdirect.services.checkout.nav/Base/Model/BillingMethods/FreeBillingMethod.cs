namespace FnacDirect.OrderPipe.Base.Model
{
    public class FreeBillingMethod : BillingMethod
    {
        public const int IdBillingMethod = 4;

        public override bool IsPrimary => true;

        public static string BillingMethodName
        {
            get
            {
                return "FreeBillingMethod";
            }
        }

        public FreeBillingMethod()
        {
            Name = BillingMethodName;
            BillingMethodId = IdBillingMethod;
            CanModifyAmountUp = false;
            CanModifyAmountDown = false;
            ImplyValidateOrder = false;
        }        
    }
}
