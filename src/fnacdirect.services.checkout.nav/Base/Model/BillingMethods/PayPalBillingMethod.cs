using System.Xml.Serialization;

namespace FnacDirect.OrderPipe.Base.Model
{
    public class PayPalBillingMethod : BillingMethod
    {
        public const int IdBillingMethod = 64;
        public override  string BillingMethodType
        {
            get { return "Paypal"; }
        }

        [XmlIgnore]
        private string _token;

        public string Token
        {
            get { return _token; }
            set { _token = value; }
        }

        [XmlIgnore]
        private string _payerid;

        public string PayerId
        {
            get { return _payerid; }
            set { _payerid = value; }
        }

        public PayPalBillingMethod()
        {
            BillingMethodId = IdBillingMethod;
        }

        public PayPalBillingMethod(string token) : this()
        {
            _token = token;
        }

        public PayPalBillingMethod(string token, string payerid) : this()
        {
            _token = token;
            _payerid = payerid;
        }
    }
}
