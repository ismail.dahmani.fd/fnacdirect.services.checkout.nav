using FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Omniture;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Pop.Models.SelfCheckout
{
    public class RootViewModel
    {
        public SelfCheckoutOrderThankYouViewModel SelfCheckoutOrderThankYouViewModel { get; set; }
        public TrackingViewModel Tracking { get; set; }
    }
}
