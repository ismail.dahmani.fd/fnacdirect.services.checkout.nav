namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Common
{
    public class RemindersViewModel
    {
        public bool ECCV { get; set; }
        public bool Ebook { get; set; }
        public bool DematSoft { get; set; }
        public bool AdherentCard { get; set; }
        public bool Appointment { get; set; }
        public bool Service { get; set; }
        public bool ExpressPlus { get; set; }
        public bool Donation { get; set; }
        public bool GamingSubscription { get; set; }
    }
}
