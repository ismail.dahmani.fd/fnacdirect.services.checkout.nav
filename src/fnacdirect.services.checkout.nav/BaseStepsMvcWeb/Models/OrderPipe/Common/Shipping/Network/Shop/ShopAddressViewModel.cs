﻿using FnacDirect.OrderPipe.Base.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Common.Shipping.Shop
{
    public class ShopAddressViewModel
    {

        public ShopAddressViewModel(ShopAddress shopAddress)
        {
            Name = shopAddress.Name;

            Address = shopAddress.Address;
            ZipCode = shopAddress.ZipCode;
            City = shopAddress.City;

            Id = shopAddress.Identity.GetValueOrDefault();
        }

        public ShopAddressViewModel(FnacStore.Model.FnacStore fnacStore)
        {
            Name = fnacStore.Name;
            Address = fnacStore.Address;
            ZipCode = fnacStore.ZipCode;
            City = fnacStore.City.Name;
            Id = fnacStore.Id;
            Organization = fnacStore.Organization.ToString();
        }

        public string Name { get; set; }

        public string Address { get; set; }

        public string ZipCode { get; set; }

        public string City { get; set; }

        public int Id { get; set; }
        public string Organization { get; private set; }
    }
}
