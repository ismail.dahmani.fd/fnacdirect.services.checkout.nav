using FnacDirect.OrderPipe.Base.Model;
using System;
using System.Collections.Generic;

namespace FnacDirect.OrderPipe.Base.Business.Payment
{
    public class CreditCardTransactionBusiness : ICreditCardTransactionBusiness
    {
        private ISwitchProvider _switchProvider;

        public CreditCardTransactionBusiness(ISwitchProvider switchProvider)
        {
            _switchProvider = switchProvider;
        }

        public bool MustAddMainOrderTransaction(OgoneCreditCardBillingMethod billingMethod)
        {
            var creditCardConditions = new Dictionary<int, Func<bool>>();

            if (_switchProvider.IsEnabled("op.ogone.v2.amex.specificrules"))
            {
                creditCardConditions.Add((int)CreditCardTypeEnum.AmericanExpress, () => true);
            }

            if (billingMethod == null || !billingMethod.TypeId.HasValue)
            {
                return false;
            }

            var currentCarteTypeId = billingMethod.TypeId.Value;
            if (creditCardConditions.ContainsKey(currentCarteTypeId))
            {
                return creditCardConditions[currentCarteTypeId]();
            }

            return false;
        }

        public bool CanSetOrderTransactionStatusOnInitialized(OgoneCreditCardBillingMethod billingMethod)
        {
            var creditCardConditions = new Dictionary<int, Func<bool>>();

            if (_switchProvider.IsEnabled("op.ogone.v2.amex.specificrules"))
            {
                creditCardConditions.Add((int)CreditCardTypeEnum.AmericanExpress, () => false);
            }

            if (billingMethod == null || !billingMethod.TypeId.HasValue)
            {
                return true;
            }

            var currentCarteTypeId = billingMethod.TypeId.Value;
            if (creditCardConditions.ContainsKey(currentCarteTypeId))
            {
                return creditCardConditions[currentCarteTypeId]();
            }

            return true;
        }
    }
}
