using FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Common.Shipping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Payment
{
    public class BasketViewModel
    {
        public bool DisplayBirthDatePopin { get; set; }

        private readonly List<BasketSellerViewModel> _sellers
            = new List<BasketSellerViewModel>();

        public IReadOnlyCollection<BasketSellerViewModel> Sellers
        {
            get
            {
                return _sellers;
            }
        }

        public BasketViewModel WithSeller(params BasketSellerViewModel[] basketSellerViewModels)
        {
            _sellers.AddRange(basketSellerViewModels);

            return this;
        }
    }
}
