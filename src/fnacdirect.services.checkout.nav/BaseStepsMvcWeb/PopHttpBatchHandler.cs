using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.Batch;

namespace FnacDirect.OrderPipe.BaseMvc.Web
{
    public class PopHttpBatchHandler : DefaultHttpBatchHandler
    {
        protected virtual string Pipe => "pop";

        public PopHttpBatchHandler(HttpServer httpServer)
            : base(httpServer)
        {

        }

        public override Task<IList<HttpResponseMessage>> ExecuteRequestMessagesAsync(IEnumerable<HttpRequestMessage> requests, CancellationToken cancellationToken)
        {
            HttpContext.Current.Items["pipe"] = Pipe;

            return base.ExecuteRequestMessagesAsync(requests, cancellationToken);
        }

        public override async Task<IList<HttpRequestMessage>> ParseBatchRequestsAsync(HttpRequestMessage request, CancellationToken cancellationToken)
        {
            var subRequests = await base.ParseBatchRequestsAsync(request, cancellationToken);
            var regex = new Regex($"orderpipe\\/api\\/v\\d*\\/{Pipe}\\/");

            foreach (var subRequest in subRequests)
            {
                if (!regex.IsMatch(subRequest.RequestUri.ToString().ToLowerInvariant()))
                {
                    throw new HttpResponseException(request.CreateErrorResponse(HttpStatusCode.NotFound, "Invalid batch target"));
                }
            }

            return subRequests;
        }
    }
}
