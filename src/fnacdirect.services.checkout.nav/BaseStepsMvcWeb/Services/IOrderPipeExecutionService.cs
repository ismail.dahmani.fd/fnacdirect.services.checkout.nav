using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FnacDirect.OrderPipe.Core.Services
{
    public interface IOrderPipeExecutionService
    {
        void Rewind();
        OPContext GetOpContext();
        void Persist();
    }
}
