using FnacDirect.Contracts.Online.Model;
using System.Threading.Tasks;

namespace FnacDirect.OrderPipe.Base.Business.Shipping.Postal
{
    public interface ICapencyService
    {
        Task<CapencyResult> VerifyAsync(IAddress address);
    }
}
