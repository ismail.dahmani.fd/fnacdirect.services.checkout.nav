using System.Collections.Generic;
using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.OrderPipe.Base.Model.Facets;
using FnacDirect.OrderPipe.Base.Model.LineGroups;
using FnacDirect.OrderPipe.Base.Model.UserInformations;

namespace FnacDirect.OrderPipe.Base.Business.Payment
{
    public interface IBillingMethodService
    {
        bool IsCcvAllowed(PopOrderForm popOrderForm, IEnumerable<ILineGroup> lineGroups, decimal totalPriceEur, int anonymousBillingMethodsMaxAmount, bool isGuest, bool disableClickAndCollectCheck = false, bool disableMaxAmount = false, bool disableECCVCheck = false);
        bool IsGiftCardAllowed(IGotLineGroups iGotLineGroups, decimal totalPriceEur, int anonymousBillingMethodsMaxAmount, bool isGuest, bool disableClickAndCollectCheck = false, bool disableMaxAmount = false, bool disableECCVCheck = false);
        bool IsFidelityEurosAllowed(IGotLineGroups iGotLineGroups, IGotBillingInformations iGotBillingInformations, int delayAfterExpiration, decimal multipleAmount, bool disableECCVCheck = false);
        bool IsFidelityPointsOccazAllowed(IGotBillingInformations iGotBillingInformations, IGotLineGroups iGotLineGroups, IUserContextInformations userContext, bool disableClickAndCollectCheck = false, bool disableECCVCheck = false);
        bool IsAvoirOccazAllowed(IGotLineGroups iGotLineGroups, int delayAfterExpiration, bool disableClickAndCollectCheck = false, bool disableECCVCheck = false);
        bool IsPaypalAllowed(PopOrderForm popOrderForm);
        bool IsPhonePaymentAllowed(OPContext opContext, PopOrderForm popOrderForm, bool disableECCVCheck = false);
        bool IsCuuAllowed(IGotLineGroups iGotLineGroups, bool disableClickAndCollectCheck = false, bool disableECCVCheck = false);
        bool IsOgoneAllowed(IGotLineGroups iContainsLineGroups, IGotBillingInformations iContainsBillingInformations, IGotUserContextInformations iContainsUserContextInformations);
        bool IsOrangeAllowed(IGotBillingInformations iGotBillingInformations, IGotLineGroups iGotLineGroups, IGotShippingMethodEvaluation shippingMethodEvaluation);
        bool IsCheckAllowed(IGotBillingInformations iGotBillingInformations, IGotLineGroups iGotLineGroups, IGotUserContextInformations iGotUserContextInformations, IGotShippingMethodEvaluation shippingMethodEvaluation);
        bool IsBankAllowed(IGotLineGroups iGotLineGroups, IGotUserContextInformations iGotUserContextInformations, IGotShippingMethodEvaluation shippingMethodEvaluation);
        bool IsDeferredPaymentAllowed(PopOrderForm popOrderForm, IGotBillingInformations iGotBillingInformations, IGotUserContextInformations iGotUserContextInformations);
        bool HasSensibleSupportId(IEnumerable<Article> articles);
        bool IsMBWayAllowed(PopOrderForm popOrderForm);

    }
}
