using System;
using System.Collections.Generic;
using FnacDirect.OrderPipe.Base.Model;

namespace FnacDirect.OrderPipe.Base.DAL
{
    public interface IPaymentMethodDal
    {
        IDictionary<int, int> GetPaymentMethodMatrix(string config);

        List<BillingMethodDescriptor> GetBillingMethodDescriptors(string config, string culture);

        int AddCreditCard(int orderId, int accountId, int? accountCreditCardId, int orderCreditCardId, string alias,
            string encryptedCardNumber, string verificationCode, string expireDate, string cardHolder,
            string creditCardStatusId, int billingMethodId);

        void AddOrderCreditInfo(int orderId, string codeBareme);
        void AddCheck(int orderId, string checkNumber, decimal? expectedAmountEuro);
        void AddOrderTransactionIngenico(Guid reference, int? orderId, string amount, string currencyCode, string amount2, string currencyCode2, string responseCode, string c3error, string ticketAvailable, string pan, string numAuto, string signatureDemande, string termNum, string typeMedia, string iso2, string cmc7, string cardType, string ssCarte, string timeLoc, string dateFinValidite, string codeService, string typeForcage, string tenderIdent, string certificatVA, string dateTrns, string heureTrns, string pisteK, string depassPuce, string incidentCam, string condSaisie, string optionChoisie, string optionLibelle, string numContexte, string codeRejet, string caiEmetteur, string caiAuto, string trnsNum, string numFile, string userData1, string userData2, string numDossier, string typeFacture, string axis, string numFileV5, string ffu, string paymentId, string charityAmount);
        void UpdateOrderTransactionIngenico(Guid reference, int? orderId, string amount, string currencyCode, string amount2, string currencyCode2, string responseCode, string c3error, string ticketAvailable, string pan, string numAuto, string signatureDemande, string termNum, string typeMedia, string iso2, string cmc7, string cardType, string ssCarte, string timeLoc, string dateFinValidite, string codeService, string typeForcage, string tenderIdent, string certificatVA, string dateTrns, string heureTrns, string pisteK, string depassPuce, string incidentCam, string condSaisie, string optionChoisie, string optionLibelle, string numContexte, string codeRejet, string caiEmetteur, string caiAuto, string trnsNum, string numFile, string userData1, string userData2, string numDossier, string typeFacture, string axis, string numFileV5, string ffu, string paymentId, string charityAmount);
        void UpdateCreditCardIngenico(int orderId, string cardNumber, string expirationDate, char ingenicoCardType);
        void AddPaymentByReference(int? orderId, Guid transactionReference, string payementReference, string merchantId);
        void AddOrderTransactionSiebel(int? orderId, Guid reference, string numAdherent, string numContract, string advantageType, decimal amount, string cardNumber);
        void UpdateOrderTransactionSiebel(Guid reference, int siebelOrderId, string siebeltransactionId);
    }
}
