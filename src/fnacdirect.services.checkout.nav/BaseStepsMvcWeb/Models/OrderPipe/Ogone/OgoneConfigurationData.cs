namespace FnacDirect.OrderPipe.Base.Model
{
    public class OgoneConfigurationData : GroupData
    {
        public bool IsOgoneV2Enable { get; set; } = false;
        
        public OgoneFieldGroup AllowedFieldGroup { get; set; } = OgoneFieldGroup.None;

        public bool IsfromCallCenter { get; set; }

        public bool ShouldUseCustomerCreditCards { get; set; }

        public void Reset()
        {
            IsOgoneV2Enable = false;
            AllowedFieldGroup = OgoneFieldGroup.None;
            IsfromCallCenter = false;
            ShouldUseCustomerCreditCards = true;
        }
    }
}

