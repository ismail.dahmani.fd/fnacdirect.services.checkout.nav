using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace FnacDirect.OrderPipe.Base.Model
{
   public class FnacCardCetelemBillingMethod : UnicreCreditCardBillingMethod
    {
        public new const int IdBillingMethod = 8388608;

        [XmlIgnore]
        private string _CreditType;

        public string CreditType
        {
            get
            {
                return _CreditType;
            }
            set
            {
                _CreditType = value;
            }
        }

        public FnacCardCetelemBillingMethod()
        {
            BillingMethodId = IdBillingMethod;
            CanModifyAmountUp = true;
            CanModifyAmountDown = true;
            UseMonetique = true;
            PaymentAction = Unicre.Model.PaymentActionEnum.AuthorizationValidation;
            OperationType = Ogone.Model.TransactionOperationType.SAL;
        }
    }
}
