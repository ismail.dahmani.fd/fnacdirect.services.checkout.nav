using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Payment;
using System;
using System.Web.Routing;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Pop.Models.Payment
{
    public interface IPaymentPageRootViewModelBuilder
    {
        RootViewModel GetRootViewModel(PopOrderForm popOrderForm, Func<string, RouteValueDictionary, string> getUrl);
    }
}
