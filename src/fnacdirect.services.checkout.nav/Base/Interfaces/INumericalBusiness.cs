﻿using FnacDirect.Contracts.Online.Model;
using FnacDirect.OrderPipe.Base.Model;

namespace FnacDirect.OrderPipe.Base.Business
{
    public interface INumericalBusiness
    {
        void GetNumericalAddOn(StandardArticle art, SiteContext siteContext);
        bool IsAuthorizedToSell(int countryId, int marketId, int retailerId);
    }
}
