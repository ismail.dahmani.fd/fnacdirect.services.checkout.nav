﻿using FnacDirect.OrderPipe.Base.Business.LogisticLineGroups.Initializers;

namespace FnacDirect.OrderPipe.Base.Business.LogisticLineGroups.Rules
{
    public class WSOrderProcessLogisticLineGroupGenerationRuleDescriptor
         : BaseFnacLogisticLineGroupGenerationRuleDescriptor<WSOrderProcessLogisticLineGroupInitializer>
    {
        public override string SwitchId
        {
            get
            {
                return "orderpipe.logisticlinegroupgenerationrule.WsOrderProcess";
            }
        }
    }
}
