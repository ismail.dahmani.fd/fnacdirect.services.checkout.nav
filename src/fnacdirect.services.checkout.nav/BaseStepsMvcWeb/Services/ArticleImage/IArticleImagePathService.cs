﻿using FnacDirect.OrderPipe.Base.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Services.Pop
{
    public interface IArticleImagePathService
    {
        string BuildPath(BaseArticle article);
        string BuildPath(BaseArticle article, Promotion promotion);
    }
}
