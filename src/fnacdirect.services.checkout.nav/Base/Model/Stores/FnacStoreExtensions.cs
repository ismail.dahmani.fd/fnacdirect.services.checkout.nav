using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.OrderPipe.Base.Model.Facets;
using FnacDirect.OrderPipe.Base.Model.LineGroups;
using System.Collections.Generic;
using System.Linq;

namespace FnacDirect.FnacStore.Model
{
    public static class FnacStoreExtensions
    {
        public static bool IsValidFor(this FnacStore fnacStore, IGotLineGroups iGotLineGroups)
        {
            return IsValidFor(fnacStore, iGotLineGroups.LineGroups);
        }

        public static bool IsValidFor(this FnacStore fnacStore, IEnumerable<ILineGroup> lineGroups)
        {
            var isDeliveryOrClickAndCollectPE = fnacStore.IsDeliveryOrClickAndCollectPE();
            var isDeliveryOrClickAndCollectPT = fnacStore.IsDeliveryOrClickAndCollectPT();

            if (isDeliveryOrClickAndCollectPE || isDeliveryOrClickAndCollectPT)
            {
                var anyPE = lineGroups.SelectMany(l => l.Articles).OfType<StandardArticle>().Any(a => a.IsPE);
                var anyPT = lineGroups.SelectMany(l => l.Articles).OfType<StandardArticle>().Any(a => a.IsPT);
                               
                return (anyPE ? isDeliveryOrClickAndCollectPE : true) && (anyPT ? isDeliveryOrClickAndCollectPT : true);
            }

            return false;
        }

        public static bool IsDeliveryOrClickAndCollectPE(this FnacStore fnacStore)
        {
            return fnacStore.IsDeliveryPE || fnacStore.IsReclaimAtStoreStoragePE;
        }

        public static bool IsDeliveryOrClickAndCollectPT(this FnacStore fnacStore)
        {
            return fnacStore.IsDeliveryPT || fnacStore.IsReclaimAtStoreStorage;
        }
    }
}
