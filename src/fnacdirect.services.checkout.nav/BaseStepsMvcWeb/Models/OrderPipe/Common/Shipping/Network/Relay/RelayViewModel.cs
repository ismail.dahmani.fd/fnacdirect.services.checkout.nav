using FnacDirect.OrderPipe.Base.Business.Availability;
using System.Linq;
using FnacDirect.Relay.Model;
using System.Collections.Generic;
using System;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Common.Shipping.Relay
{
    public class RelayViewModel : RelayAddressViewModel
    {
        public RelayViewModel(RelayAddressEntity relayAddressEntity, string shortEstimatedDeliveryDate, IEnumerable<PopAvailabilityProcessorOutput> longEstimatedDeliveryDate, bool displayLongEstimatedDeliveryDates)
            : base(relayAddressEntity)
        {
            ShortEstimatedDeliveryDate = shortEstimatedDeliveryDate;
            LongEstimatedDeliveryDate = longEstimatedDeliveryDate;
            DisplayLongEstimatedDeliveryDates = displayLongEstimatedDeliveryDates;
        }

        public string ShippingCost { get; set; }

        public DateTime? WorstDeliveryDate { get; set; }

        public bool BeforeXmas
        {
            get
            {
                return LongEstimatedDeliveryDate.ComputeBeforeXmas();
            }
        }

        public string PictoLabel
        {
            get
            {
                return LongEstimatedDeliveryDate.GetPictoLabel();
            }
        }

        public bool IsFreeShipping { get; set; }

        public bool IsSelected { get; set; }

        public bool IsFavorite { get; set; }

        public string ShortEstimatedDeliveryDate { get; set; }

        public IEnumerable<PopAvailabilityProcessorOutput> LongEstimatedDeliveryDate { get; set; }

        public bool HasManyLongEstimatedDeliveryDate
        {
            get
            {
                return LongEstimatedDeliveryDate.Count() > 1;
            }
        }

        public bool DisplayLongEstimatedDeliveryDates { get; set; }
    }
}
