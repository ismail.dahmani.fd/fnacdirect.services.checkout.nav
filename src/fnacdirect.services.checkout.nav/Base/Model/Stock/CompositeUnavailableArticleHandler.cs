using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.OrderPipe.Base.Model.Facets;
using System.Collections.Generic;
using System;
using FnacDirect.OrderPipe.Base.Model.LineGroups;

namespace FnacDirect.OrderPipe.Base.Business.Stock
{
    public class CompositeUnavailableArticleHandler : IUnavailableArticleHandler
    {
        private readonly IEnumerable<Func<IGotLineGroups, IUnavailableArticleHandler>> _handlerFactories;
        private readonly IGotLineGroups _iGotLineGroups;

        public CompositeUnavailableArticleHandler(IEnumerable<Func<IGotLineGroups, IUnavailableArticleHandler>> handlerFactories,
                                                  IGotLineGroups iGotLineGroups)
        {
            _handlerFactories = handlerFactories;
            _iGotLineGroups = iGotLineGroups;
        }

        public bool Handle(ILineGroup parentLineGroup, StandardArticle standardArticle)
        {
            foreach (var handlerFactory in _handlerFactories)
            {
                var handler = handlerFactory(_iGotLineGroups);

                if (handler.Handle(parentLineGroup, standardArticle))
                {
                    return true;
                }
            }

            return false;
        }
    }
}
