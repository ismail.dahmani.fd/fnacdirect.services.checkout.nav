using FnacDirect.OrderPipe.BaseMvc.Web.Pop.Models.Common;
using System.Collections.Generic;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Models
{
    public interface IBaseApiRootViewModel
    {
        List<PipeMessageViewModel> PipeMessages { get; }
    }
}
