using System.Collections.Generic;
using FnacDirect.Contracts.Online.Model;

namespace FnacDirect.OrderPipe.Base.Business.Shipping.Postal
{
    public class AddressValidityResult
    {
        public AddressValidityResult()
        {
            Addresses = new List<IAddress>();
        }
        public AddressValidityResult(AddressValidityResultType addressValidityResultType)
        {
            AddressValidityResultType = addressValidityResultType;
            Addresses = new List<IAddress>();
        }

        public AddressValidityResultType AddressValidityResultType { get; set; }

        public IEnumerable<IAddress> Addresses { get; set; }
    }
}