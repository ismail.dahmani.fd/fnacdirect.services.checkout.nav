﻿namespace FnacDirect.OrderPipe.Base.Model
{
    public enum LogType
    {
        None = 0,
        ProduitsEditorial = 1,
        Famille1 = 2,
        Famille2 = 3,
        Famille3 = 4,
        Famille4 = 5,
        Famille5 = 6,
        Famille6 = 7,
        Famille7 = 8,
        ServicesDivers = 9,
        Family8 = 10,
        Spectacles = 1001,
        ProduitsDemat = 1002
    }
}
