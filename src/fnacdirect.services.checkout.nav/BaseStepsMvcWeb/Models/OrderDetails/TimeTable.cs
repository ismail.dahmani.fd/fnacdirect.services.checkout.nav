﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace FnacDirect.OrderPipe.Base.Business.OrderDetail
{
    public class TimeTable
    {
        [XmlAttribute]
        public string Day { get; set; }

        [XmlAttribute]
        public string Comment { get; set; }

        public List<OpeningPeriod> OpeningPeriods { get; set; }
    }
}
