﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Common.Payment
{
    public class DeferredPaymentBillingMethodViewModel : BillingMethodViewModel
    {
        /// <summary>
        /// Obtient ou définit si le paiement différé est activé pour le client en cours
        /// </summary>
        public bool IsEligible { get; set; }
        /// <summary>
        /// Délai du paiement différé
        /// </summary>
        public int PaymentDelay { get; set; }
        /// <summary>
        /// N° de paiement différé du client
        /// </summary>
        public string DeferredAccountNumber { get; set; }
    }
}
