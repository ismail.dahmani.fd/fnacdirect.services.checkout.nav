
using System.ComponentModel.DataAnnotations;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Common.Payment
{
    public class MBWayBillingMethodViewModel : BillingMethodViewModel
    {        
        public string PhoneNumber { get; set; }
    }
}
