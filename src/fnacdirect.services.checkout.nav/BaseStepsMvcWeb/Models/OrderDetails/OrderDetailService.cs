using FnacDirect.Contracts.Online.Model;
using FnacDirect.OrderPipe.Base.BaseModel;
using FnacDirect.OrderPipe.Base.BaseModel.Model;
using FnacDirect.OrderPipe.Base.Business.Availability;
using FnacDirect.OrderPipe.Base.Business.Shipping.Shop;
using FnacDirect.OrderPipe.Base.LineGroups;
using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.OrderPipe.Base.Model.LineGroups;
using FnacDirect.OrderPipe.Base.Proxy.Appointment;
using FnacDirect.OrderPipe.Base.Proxy.Articles;
using FnacDirect.OrderPipe.Base.Proxy.Membership;
using FnacDirect.OrderPipe.Base.Proxy.Store;
using FnacDirect.Technical.Framework.Utils;
using FnacDirect.Technical.Framework.Web;
using FnacDirect.Technical.Framework.Web.Mvc;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;

namespace FnacDirect.OrderPipe.Base.Business.OrderDetail
{
    public class OrderDetailService : IOrderDetailService
    {
        private readonly IFnacStoreService _fnacStoreService;
        private readonly IPopAvailabilityProcessor _popAvailabilityProcessor;
        private readonly IShipToStoreAvailabilityService _shipToStoreAvailabilityService;
        private readonly IApplicationContext _applicationContext;
        private readonly IPriceHelper _priceHelper;
        private readonly IUrlManager _urlManager;
        private readonly IW3ArticleService _w3ArticleService;
        private readonly IAdherentCardArticleService _adherentCardArticleService;
        private readonly ISwitchProvider _switchProvider;

        private readonly RangeSet<int> _appointmentArticleTypeIds;
        private readonly IEnumerable<int> _warantyTypes;

        public OrderDetailService(IFnacStoreService fnacStoreService,
            IPopAvailabilityProcessor popAvailabilityProcessor,
            IShipToStoreAvailabilityService shipToStoreAvailabilityService,
            IAppointmentService appointmentService,
            IApplicationContext applicationContext,
            IUrlManager urlManager,
            IW3ArticleService w3ArticleService,
            IAdherentCardArticleService adherentCardArticleService,
            ISwitchProvider switchProvider)
        {
            _fnacStoreService = fnacStoreService;
            _popAvailabilityProcessor = popAvailabilityProcessor;
            _shipToStoreAvailabilityService = shipToStoreAvailabilityService;
            _applicationContext = applicationContext;
            _priceHelper = _applicationContext.GetPriceHelper();
            _urlManager = urlManager;
            _w3ArticleService = w3ArticleService;
            _adherentCardArticleService = adherentCardArticleService;
            _switchProvider = switchProvider;
            _appointmentArticleTypeIds = appointmentService.GetAppointmentArticleTypeIds();

            _warantyTypes = new List<int>
            {
                19001, 19002, 19005, 19006, 19007, 19008, 19009, 19010, 19011
            };
        }

        public BaseAddress ComputeShippingAddress(ShippingMethodEvaluation shippingMethodEvaluation)
        {
            if (HasManyShippingAddress(shippingMethodEvaluation))
            {
                return null;
            }

            var shippingAddresses = shippingMethodEvaluation.ShippingMethodEvaluationGroups.Select(GetShippingAddress);

            var shippingAddress = shippingAddresses.FirstOrDefault();

            if (shippingAddress is ShopAddress)
            {
                (shippingAddress as ShopAddress).Schedule = BuildSchedule(Convert.ToInt32(shippingAddress.Id));
            }

            return shippingAddress;
        }

        public BaseAddress ComputeShippingAddressForSeller(ShippingMethodEvaluation shippingMethodEvaluation, ILogisticLineGroup logisticLineGroup, int sellerId)
        {
            if (logisticLineGroup.Articles.OfType<StandardArticle>().Any()
                && logisticLineGroup.Articles.OfType<StandardArticle>().Where(a => a.LogType.HasValue).All(a => shippingMethodEvaluation.ExcludedLogisticTypes.Contains(a.LogType.Value)))
            {
                return null;
            }

            if (logisticLineGroup.Articles.OfType<MarketPlaceArticle>().Any()
                && logisticLineGroup.Articles.OfType<MarketPlaceArticle>().Where(a => a.LogType.HasValue).All(a => shippingMethodEvaluation.ExcludedLogisticTypes.Contains(a.LogType.Value)))
            {
                return null;
            }

            var shippingAddresses = shippingMethodEvaluation.ShippingMethodEvaluationGroups.Where(smg => smg.SellerId == sellerId).Select(GetShippingAddress);

            var shippingAddress = shippingAddresses.FirstOrDefault();

            if (shippingAddress is ShopAddress)
            {
                (shippingAddress as ShopAddress).Schedule = BuildSchedule(Convert.ToInt32(shippingAddress.Id));
            }

            return shippingAddress;
        }

        public bool HasManyShippingAddress(ShippingMethodEvaluation shippingMethodEvaluation)
        {
            var shippingAddresses = shippingMethodEvaluation.ShippingMethodEvaluationGroups.Where(smg => smg.SelectedShippingMethodEvaluationType != ShippingMethodEvaluationType.Unknown).Select(GetShippingAddress);

            return shippingAddresses.Select(a => a.Id).Distinct().Count() > 1;
        }

        private BaseAddress GetShippingAddress(ShippingMethodEvaluationGroup shippingEvaluationGroup)
        {
            switch (shippingEvaluationGroup.SelectedShippingMethodEvaluationType)
            {
                case ShippingMethodEvaluationType.Postal:
                    var postalShippingAddress = shippingEvaluationGroup.PostalShippingMethodEvaluationItem.Value.ShippingAddress;
                    return new PostalAddress
                    {
                        Id = postalShippingAddress.AddressEntity?.AddressBookReference ?? string.Empty,
                        Alias = postalShippingAddress.Alias,
                        FirstName = postalShippingAddress.Firstname,
                        LastName = postalShippingAddress.LastName,
                        Company = postalShippingAddress.Company,
                        Address = FormatAddress(postalShippingAddress.AddressLine1, postalShippingAddress.AddressLine2, postalShippingAddress.AddressLine3, postalShippingAddress.AddressLine4).Trim(),
                        ZipCode = postalShippingAddress.ZipCode,
                        City = postalShippingAddress.City,
                        State = postalShippingAddress.State,
                        Phone = string.IsNullOrEmpty(postalShippingAddress.CellPhone) ? postalShippingAddress.Phone : postalShippingAddress.CellPhone,
                        Country = postalShippingAddress.CountryLabel
                    };
                case ShippingMethodEvaluationType.Relay:
                    var relayShippingAddress = shippingEvaluationGroup.RelayShippingMethodEvaluationItem.Value.ShippingAddress;
                    return new RelayAddress
                    {
                        Id = relayShippingAddress.Identity.GetValueOrDefault().ToString(CultureInfo.InvariantCulture),

                        Name = UpperFirstLetterOnly(relayShippingAddress.LastName),
                        Address = FormatAddress(relayShippingAddress.AddressLine1, relayShippingAddress.AddressLine2, relayShippingAddress.AddressLine3, relayShippingAddress.AddressLine4).Trim(),
                        ZipCode = relayShippingAddress.ZipCode,
                        City = relayShippingAddress.City,
                        State = relayShippingAddress.State,
                        Country = relayShippingAddress.CountryLabel
                    };
                case ShippingMethodEvaluationType.Shop:
                    var shopShippingAddress = shippingEvaluationGroup.ShopShippingMethodEvaluationItem.Value.ShippingAddress;
                    return new ShopAddress
                    {
                        Id = shopShippingAddress.Identity.GetValueOrDefault().ToString(CultureInfo.InvariantCulture),
                        Address = shopShippingAddress.Address,
                        City = shopShippingAddress.City,
                        Country = shopShippingAddress.CountryLabel,
                        ZipCode = shopShippingAddress.ZipCode,
                        Name = shopShippingAddress.Name,
                    };
                default:
                    return null;
            }
        }

        private Schedule BuildSchedule(int storeId)
        {
            var fnacStore = _fnacStoreService.GetStore(storeId);

            var timeTables = fnacStore.Timetables.Where(t => t.OpeningPeriods.Any()).Select(t => new TimeTable
            {
                Day = Enum.Parse(typeof(DayOfWeek), t.DayOfWeek.ToString(CultureInfo.InvariantCulture)).ToString(),
                OpeningPeriods = t.OpeningPeriods.Select(op => new OpeningPeriod
                {
                    Opening = op.Opening.ToString("hh\\:mm"),
                    Closing = op.Closing.ToString("hh\\:mm")
                }).ToList()
            }).ToList();


            var exceptionnalOpening = fnacStore.TimetableExceptionnals.Where(t => t.IsOpened).Select(t => new TimeTable
            {
                Day = t.Day.ToShortDateString(),
                Comment = t.Comment,
                OpeningPeriods = t.OpeningPeriods.Select(op => new OpeningPeriod
                {
                    Opening = op.Opening.ToString("hh\\:mm"),
                    Closing = op.Closing.ToString("hh\\:mm"),
                }).ToList()
            }).ToList();

            var exceptionnalClosing = fnacStore.TimetableExceptionnals.Where(t => !t.IsOpened).Select(t => new TimeTable
            {
                Day = t.Day.ToShortDateString(),
                Comment = t.Comment,
            }).ToList();

            return new Schedule
            {
                TimeTables = timeTables,
                ExceptionnalTimeTables = new ExceptionnalTimeTable
                {
                    ExceptionnalOpening = exceptionnalOpening,
                    ExceptionnalClosing = exceptionnalClosing
                }
            };
        }

        public string FormatAddress(string addressLine1, string addressLine2, string addressLine3, string addressLine4)
        {
            var addresses = (new[]{ addressLine1,
                                    addressLine2,
                                    addressLine3,
                                    addressLine4})
                                 .Where(s => !string.IsNullOrEmpty(s)).ToArray();

            return string.Join(" ", addresses);
        }

        public string UpperFirstLetterOnly(string str)
        {
            if (!string.IsNullOrEmpty(str) && str.Length > 1)
            {
                var tmp = str.ToLowerInvariant();

                return tmp[0].ToString(CultureInfo.InvariantCulture).ToUpperInvariant() + tmp.Substring(1, tmp.Length - 1);
            }

            return str;
        }

        public TheoreticalDeliveryDateList ComputeTheoreticalDeliveryDate(PopOrderForm popOrderForm)
        {
            return ComputeTheoreticalDeliveryDate(popOrderForm.LogisticLineGroups, popOrderForm.ShippingMethodEvaluation);
        }

        public TheoreticalDeliveryDateList ComputeTheoreticalDeliveryDate(IEnumerable<ILogisticLineGroup> logisticLineGroups, ShippingMethodEvaluation shippingMethodEvaluation)
        {
            var result = new TheoreticalDeliveryDateList();
            if (_switchProvider.IsEnabled("orderpipe.pop.meteor.availabilities"))
            {
                var availabilityProcessorOutputs = new List<PopAvailabilityProcessorOutput>();

                foreach (var shippingMethodEvaluationGroup in shippingMethodEvaluation.ShippingMethodEvaluationGroups)
                {
                    var lineGroupsMatched = logisticLineGroups.Where(l => l.Seller.SellerId == shippingMethodEvaluationGroup.SellerId);

                    if (!_switchProvider.IsEnabled("orderpipe.pop.shipping.dlemp.enable"))
                    {
                        lineGroupsMatched = lineGroupsMatched.Where(l => l.Seller.Type != LineGroups.SellerType.MarketPlace);
                    }

                    switch (shippingMethodEvaluationGroup.SelectedShippingMethodEvaluationType)
                    {
                        case ShippingMethodEvaluationType.Relay:

                            var popRelayAvailabilityProcessorInputs = ComputeRelayAvailabilityProcessorInput(shippingMethodEvaluationGroup, lineGroupsMatched, shippingMethodEvaluation.ExcludedLogisticTypes);
                            if (popRelayAvailabilityProcessorInputs != null && popRelayAvailabilityProcessorInputs.Any())
                            {
                                var longGlobalEstimatedDeliveryDate = _popAvailabilityProcessor.GetLongGlobalEstimatedDeliveryDate(popRelayAvailabilityProcessorInputs, false);
                                if (longGlobalEstimatedDeliveryDate != null)
                                {
                                    availabilityProcessorOutputs.AddRange(longGlobalEstimatedDeliveryDate);
                                }
                            }
                            break;
                        case ShippingMethodEvaluationType.Shop:

                            var popShopAvailabilityProcessorInputs = ComputeShopPopAvailabilityProcessorInput(shippingMethodEvaluationGroup.ShopShippingMethodEvaluationItem.Value.ShippingAddress.Identity.GetValueOrDefault(), lineGroupsMatched, shippingMethodEvaluation.ExcludedLogisticTypes);

                            if (popShopAvailabilityProcessorInputs != null && popShopAvailabilityProcessorInputs.Any())
                            {
                                availabilityProcessorOutputs.AddRange(_popAvailabilityProcessor.GetLongGlobalEstimatedDeliveryDateForStore(lineGroupsMatched.SelectMany(l => l.Articles), popShopAvailabilityProcessorInputs, shippingMethodEvaluationGroup.ShopShippingMethodEvaluationItem.Value.GetDefaultShippingMethodId().FirstOrDefault(), shippingMethodEvaluationGroup.ShopShippingMethodEvaluationItem.Value.ShippingAddress.Identity.GetValueOrDefault(), false));
                            }
                            break;
                        case ShippingMethodEvaluationType.Postal:

                            var availabilityList = new List<PopAvailabilityProcessorInput>();
                            foreach (var lineGroup in lineGroupsMatched)
                            {
                                var selectedChoice = lineGroup.SelectedShippingChoice;

                                if (selectedChoice != null && selectedChoice.Groups != null)
                                {
                                    foreach (var group in selectedChoice.Groups)
                                    {
                                        foreach (var item in group.Items)
                                        {
                                            var parcel = selectedChoice.Parcels[item.ParcelId];
                                            var articles = lineGroup.Articles;
                                            if (!_switchProvider.IsEnabled("orderpipe.pop.shipping.dlemp.enable"))
                                            {
                                                articles = articles.Where(a => a is StandardArticle).ToList();
                                            }
                                            var tmp = new PopAvailabilityProcessorInputListBuilder(articles, parcel.ShippingMethodId)
                                                .ExcludeLogisticTypes(shippingMethodEvaluation.ExcludedLogisticTypes)
                                                .FilterArticles((article) => item.Identifier.Prid == article.ProductID)
                                                .WithShippingMethodId()
                                                .BuildList();

                                            availabilityList.AddRange(tmp);
                                        }
                                    }
                                }
                            }
                            availabilityProcessorOutputs.AddRange(_popAvailabilityProcessor.GetLongGlobalEstimatedDeliveryDate(availabilityList, false));
                            break;
                    }
                }
                foreach (var availabilityProcessorOutput in availabilityProcessorOutputs)
                {
                    foreach (var availabilityItemInfo in availabilityProcessorOutput.Items)
                    {
                        var deliveryDate = UpperFirstLetterOnly(Regex.Replace(availabilityProcessorOutput.Key, @"<(.|\n)*?>", string.Empty));
                        result.Add(availabilityItemInfo.Id, availabilityItemInfo.OfferRef, deliveryDate, availabilityItemInfo.EstimatedDeliveryStartDate, availabilityItemInfo.EstimatedDeliveryEndDate);
                    }
                }
            }
            return result;
        }

        private IEnumerable<PopAvailabilityProcessorInput> ComputeRelayAvailabilityProcessorInput(ShippingMethodEvaluationGroup shippingMethodEvaluationGroup, IEnumerable<ILogisticLineGroup> logisticLineGroups, IEnumerable<int> excludedLogisticTypes)
        {
            var relayShippingMethodEvaluationItem = shippingMethodEvaluationGroup.RelayShippingMethodEvaluationItem.Value;

            var defaultShippingMethodId = relayShippingMethodEvaluationItem.GetDefaultShippingMethodId().FirstOrDefault();

            if (defaultShippingMethodId == 0)
            {
                return Enumerable.Empty<PopAvailabilityProcessorInput>();
            }


            var articles = logisticLineGroups.SelectMany(l => l.Articles);
            if (!_switchProvider.IsEnabled("orderpipe.pop.shipping.dlemp.enable"))
            {
                articles = articles.Where(a => a is StandardArticle);
            }
            return new PopAvailabilityProcessorInputListBuilder(articles, defaultShippingMethodId)
                .ExcludeLogisticTypes(excludedLogisticTypes)
                .WithShippingMethodId()
                .BuildList();
        }

        private IEnumerable<ShipToStoreAvailability> ComputeShopPopAvailabilityProcessorInput(int storeId, IEnumerable<ILogisticLineGroup> logisticLineGroups, IEnumerable<int> excludedLogisticTypes)
        {
            var fnacStore = _fnacStoreService.GetStore(storeId);
            return _shipToStoreAvailabilityService.GetAvailabilities(fnacStore, logisticLineGroups.SelectMany(l => l.Articles), excludedLogisticTypes, null).ToList();
        }

        private string GetTheoreticalDeliveryDate(Model.BaseArticle article, TheoreticalDeliveryDateList theoreticalDeliveryDates)
        {
            if (_appointmentArticleTypeIds.Contains(article.TypeId.GetValueOrDefault()))
            {
                return _applicationContext.GetMessage("orderpipe.pop.mail.formathome.theoricaldeliverydate");
            }

            var date = theoreticalDeliveryDates.Get(article);
            if(!date.HasValue)
            {
                return null;
            }

            return date.Value.DeliveryDate;
        }

        public IEnumerable<Article> BuildArticles(IEnumerable<Model.Article> articles, TheoreticalDeliveryDateList theoreticalDeliveryDates)
        {
            var orderedArticles = new List<Article>();

            foreach (var article in articles)
            {
                if (article is StandardArticle)
                {
                    var standardArticle = article as StandardArticle;

                    if (standardArticle.Hidden)
                    {
                        continue;
                    }

                    orderedArticles.Add(new Article
                    {
                        Quantity = standardArticle.Quantity.GetValueOrDefault().ToString(CultureInfo.InvariantCulture),
                        Price = _priceHelper.Format((standardArticle.PriceUserEur.GetValueOrDefault() + standardArticle.EcoTaxEur.GetValueOrDefault()) * standardArticle.Quantity.GetValueOrDefault(), PriceFormat.Default),
                        OldPrice = _priceHelper.Format((standardArticle.PriceDBEur.GetValueOrDefault() + standardArticle.EcoTaxEur.GetValueOrDefault()) * standardArticle.Quantity.GetValueOrDefault(), PriceFormat.Default),
                        PriceNoVATEur = _priceHelper.Format(standardArticle.PriceNoVATEur.GetValueOrDefault(), PriceFormat.Default),
                        IsNumerical = standardArticle.IsNumerical,
                        FormatLabel = standardArticle.FormatLabel,
                        SerieTitle = standardArticle.SerieTitle,
                        Title = standardArticle.Title,
                        DigitalLockerMatched = GetDigitalLockerMatched(standardArticle.ToArticleReference()),
                        HasUpcDeezerReference = GetDeezerReference(standardArticle.ToArticleReference()),
                        IsPreOrder = standardArticle.AvailabilityId.GetValueOrDefault() == (int)AvailabilityEnum.PreOrder || standardArticle.AvailabilityId.GetValueOrDefault() == (int)AvailabilityEnum.PreOrderBis,
                        PicturUrl = GeneratePictureUrl(standardArticle.ErgoBasketPictureURL),
                        Services = BuildServices(standardArticle),
                        Participants = BuildParticipants(standardArticle),
                        TheoreticalDeliveryDate = GetTheoreticalDeliveryDate(standardArticle, theoreticalDeliveryDates),
                        TypeId = standardArticle.TypeId,
                        TypeLabel = standardArticle.TypeLabel,
                        ShippingMethodId = standardArticle.ShipMethod
                    });

                }
                else if (article is MarketPlaceArticle)
                {
                    var marketPlaceArticle = article as MarketPlaceArticle;

                    var orderArticle = new Article
                    {
                        Quantity = marketPlaceArticle.Quantity.GetValueOrDefault().ToString(CultureInfo.InvariantCulture),
                        Price = _priceHelper.Format(marketPlaceArticle.PriceUserEur.GetValueOrDefault() * marketPlaceArticle.Quantity.GetValueOrDefault(), PriceFormat.Default),
                        FormatLabel = marketPlaceArticle.FormatLabel,
                        SerieTitle = marketPlaceArticle.SerieTitle,
                        Title = marketPlaceArticle.Title,
                        PicturUrl = GeneratePictureUrl(marketPlaceArticle.ErgoBasketPictureURL),
                        Participants = BuildParticipants(marketPlaceArticle),
                        ShippingMethodId = marketPlaceArticle.ShipMethod
                    };

                    if (_switchProvider.IsEnabled("orderpipe.pop.shipping.dlemp.enable"))
                    {
                        orderArticle.TheoreticalDeliveryDate = GetTheoreticalDeliveryDate(marketPlaceArticle, theoreticalDeliveryDates);
                    }

                    orderedArticles.Add(orderArticle);
                }
            }
            return orderedArticles;
        }

        private bool GetDeezerReference(ArticleReference articleReference)
        {
            var w3Article = _w3ArticleService.GetArticleNoPrice(articleReference);

            return !string.IsNullOrEmpty(w3Article?.UpcDeezerReference);
        }

        private static List<Participant> BuildParticipants(Model.BaseArticle standardArticle)
        {
            var participants = new List<Participant>();

            if (string.IsNullOrEmpty(standardArticle.Participant1))
            {
                return participants;
            }

            var participantsNames = standardArticle.Participant1.Split(';');
            var participantsIds = standardArticle.Participant1_id.Split('/');
            var participantsCount = participantsNames.Length;


            for (var i = 0; i < participantsCount; i++)
            {
                participants.Add(new Participant
                {
                    Id = Convert.ToInt32(participantsIds.GetValue(i)),
                    Name = participantsNames.GetValue(i).ToString()
                });

            }
            return participants;
        }

        private List<Service> BuildServices(StandardArticle standardArticle)
        {
            return standardArticle.Services.Select(s => new Service
            {
                Title = s.Label,
                Quantity = s.Quantity.GetValueOrDefault().ToString(CultureInfo.InvariantCulture),
                PicturUrl = GeneratePictureUrl(s.ErgoBasketPictureURL),
                Price = _priceHelper.Format((s.PriceUserEur.GetValueOrDefault() + s.EcoTaxEur.GetValueOrDefault()) * s.Quantity.GetValueOrDefault(), PriceFormat.Default),
                OldPrice = _priceHelper.Format((s.PriceDBEur.GetValueOrDefault() + s.EcoTaxEur.GetValueOrDefault()) * s.Quantity.GetValueOrDefault(), PriceFormat.Default),
                TypeId = s.TypeId.ToString(),
                Debut = ServiceStartWarding(s.TypeId.GetValueOrDefault())
            }).ToList();
        }

        private string ServiceStartWarding(int serviceType)
        {
            switch (serviceType)
            {
                case 19007:
                case 19008:
                case 19009:
                case 19010:
                    return _applicationContext.GetMessage("orderpipe.pop.mail.service.start.insurance");
                case 19011:
                    return _applicationContext.GetMessage("orderpipe.pop.mail.service.start.refund");
            }
            return string.Empty;
        }

        public string GeneratePictureUrl(Model.BaseArticle article)
        {
            if (article is Model.Service)
            {
                return string.Format("{0}pictos/{1}.png", _applicationContext.GetImagePathWithCulture(), article.TypeId.Value);
            }

            return GeneratePictureUrl(article.ErgoBasketPictureURL);
        }

        public string GeneratePictureUrl(string pictureUrl)
        {
            var multimediaPath = _urlManager.Sites.Multimedia.BaseRootUrl;

            if (string.IsNullOrEmpty(pictureUrl))
            {
                return multimediaPath.WithPage("img/catalog/noscan_47x70.gif").ToString();
            }

            if (Uri.IsWellFormedUriString(pictureUrl, UriKind.Absolute))
            {
                return pictureUrl.Replace("http://", "https://");
            }

            try
            {
                return multimediaPath.WithPage(pictureUrl).ToString();
            }
            catch (Exception)
            {
                return multimediaPath.WithPage("img/catalog/noscan_47x70.gif").ToString();
            }
        }

        private bool GetDigitalLockerMatched(ArticleReference articleReference)
        {
            var w3Article = _w3ArticleService.GetArticle(articleReference);

            return w3Article != null && w3Article.DigitalLockerMatched;
        }

        private bool HasWaranty(IEnumerable<ILineGroup> lineGroups)
        {
            return AllServices(lineGroups).Any(s => s.TypeId.HasValue && _warantyTypes.Contains(s.TypeId.Value));
        }

        private IEnumerable<Model.Service> AllServices(IEnumerable<ILineGroup> lineGroups)
        {
            var articlesServices = lineGroups.SelectMany(lg => lg.Articles.OfType<StandardArticle>().SelectMany(art => art.Services)).ToList();
            var standAloneServices = lineGroups.SelectMany(lg => lg.Articles.OfType<Model.Service>()).ToList();
            return articlesServices.Union(standAloneServices);
        }

        private IEnumerable<Model.Article> Appointments(IEnumerable<ILineGroup> lineGroups)
        {
            return lineGroups.SelectMany(lg => lg.Articles).Where(a => _appointmentArticleTypeIds.Contains(a.TypeId.GetValueOrDefault()));
        }

        private List<Service> BuildCgvService(IEnumerable<ILineGroup> lineGroups)
        {

            return AllServices(lineGroups).Union(Appointments(lineGroups)).Select(service => new Service
            {
                Title = service.TypeLabel,
                CgvUrl = BuildCgvPdfLink(string.Format("cgvservice{0}", service.TypeId))
            }).ToList();
        }

        private string BuildCgvPdfLink(string page)
        {
            return _urlManager.Sites.CgvPdf.GetPage(page).DefaultUrl.ToString();
        }

        public Cgv BuildCgv(PopOrderForm popOrderForm)
        {
            var hasFnacplusCardInBasket = (popOrderForm.GetPrivateData<PushFnacPlusAdherentData>("PushFnacPlusAdherentGroup", create: false)?.HasFnacPlusCardInBasket).GetValueOrDefault();
            var hasExpressPlus = (popOrderForm.GetPrivateData<AboIlliData>("AboIlliGroup", create: false)?.HasAboIlliInBasket).GetValueOrDefault();
            var hasAdherentCard = popOrderForm.LineGroups.Where(lg => lg.HasFnacComArticle).SelectMany(lg => lg.Articles).Any(a => _adherentCardArticleService.IsAdherentCard(a.ProductID.GetValueOrDefault(0)));
            return new Cgv
            {
                HasEbook = new CgvItem
                {
                    Value = popOrderForm.HasNumericalArticle(),
                    CgvUrl = popOrderForm.HasNumericalArticle() ? BuildCgvPdfLink("cgvdematbookfnac") : null
                },
                HasDematSoft = new CgvItem
                {
                    Value = popOrderForm.HasDematSoftArticle(),
                    CgvUrl = popOrderForm.HasDematSoftArticle() ? BuildCgvPdfLink("cgvdematsoftfnac") : null
                },
                HasMpArticles = new CgvItem
                {
                    Value = popOrderForm.HasMarketPlaceArticle(),
                    CgvUrl = popOrderForm.HasMarketPlaceArticle() ? BuildCgvPdfLink("cgvmarketfnac") : null
                },
                HasAdherentCard = new CgvItem
                {
                    Value = hasAdherentCard,
                    CgvUrl = hasAdherentCard ? BuildCgvPdfLink("cgvadhfnac") : null
                },
                HasExpressplus = new CgvItem
                {
                    Value = hasExpressPlus,
                    CgvUrl = hasExpressPlus ? BuildCgvPdfLink("cgvexpressfnac") : null
                },
                HasFnacplus = new CgvItem
                {
                    Value = hasFnacplusCardInBasket,
                    CgvUrl = hasFnacplusCardInBasket ? BuildCgvPdfLink("cgvfnacplusfnac") : null
                },
                HasFnacArticles = new CgvItem
                {
                    Value = popOrderForm.HasFnacComOrClickAndCollectArticle(),
                    CgvUrl = popOrderForm.HasFnacComOrClickAndCollectArticle() ? BuildCgvPdfLink("cgvproduitsfnac") : null
                },
                HasGuarantee = new CgvItem
                {
                    Value = HasWaranty(popOrderForm.LineGroups)
                },
                Services = BuildCgvService(popOrderForm.LineGroups)
            };
        }

    }
}
