﻿
namespace FnacDirect.OrderPipe.Base.Model.Pop
{
    public class SplitData : GroupData
    {
        public bool IsAvailable { get; set; }

        public bool IsApplied { get; set; }

        public bool HasBeenPreSelected { get; set; }

        public decimal Price { get; set; }

        public decimal PriceNoVAT { get; set; }

    }
}
