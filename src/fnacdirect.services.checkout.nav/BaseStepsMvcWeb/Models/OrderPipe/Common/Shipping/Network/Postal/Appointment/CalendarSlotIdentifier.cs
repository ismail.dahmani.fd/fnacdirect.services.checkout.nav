﻿using System;

namespace FnacDirect.OrderPipe.Base.Model.Shipping.Postal.Appointment.Fly
{
    public class CalendarSlotIdentifier
    {
        public string PartnerId { get; set; }

        public string PartnerUniqueIdentifier { get; set; }

        public DateTime StartDate { get; set; }

        public DateTime EndDate { get; set; }

        public string HoursTitle { get; set; }

        public double Price { get; set; }

    }
}
