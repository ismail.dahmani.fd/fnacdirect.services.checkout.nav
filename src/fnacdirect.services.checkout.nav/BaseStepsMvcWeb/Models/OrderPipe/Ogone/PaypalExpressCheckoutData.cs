namespace FnacDirect.OrderPipe.Base.Model
{
    public class PaypalExpressCheckoutData : GroupData
    {
        public long? PayId { get; set; }

        public string TxToken { get; set; }

        public string Alias { get; set; }

        public string OrderId { get; set; }

        public bool IsInitialized
        {
            get
            {
                return PayId.HasValue
                    && !string.IsNullOrEmpty(TxToken)
                    && !string.IsNullOrEmpty(Alias)
                    && !string.IsNullOrEmpty(OrderId);
            }
        }
    }
}
