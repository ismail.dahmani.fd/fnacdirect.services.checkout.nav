using System.Collections.Generic;
using System.Linq;
using FnacDirect.OrderPipe.BaseMvc.Web.Models.Common;
using FnacDirect.Technical.Framework.Web.Mvc.CustomizedContainers;
using FnacDirect.Technical.Framework.Web.Mvc.DataAnnotations;
using FnacDirect.Customer.Model;
using FnacDirect.Technical.Framework.Web.Mvc;
using FnacDirect.OrderPipe.Base.Model;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.OnePage.Address
{
    public class AddressViewModel : IUseACustomizedContainer, IAddressViewModelFieldsValidation
    {

        public string AddressReference { get; set; }

        [Required(ErrorMessage = "orderpipe.pop.addresses.firstnamerequired")]
        [RegularExpression(@"^[^0-9().:]+$", ErrorMessage = "orderpipe.pop.format.onlyletters")]
        [StringLength(50)]
        public string FirstName { get; set; }

        [Required(ErrorMessage = "orderpipe.pop.addresses.lastnamerequired")]
        [RegularExpression(@"^[^0-9().:]+$", ErrorMessage = "orderpipe.pop.format.onlyletters")]
        [StringLength(50)]
        public string LastName { get; set; }

        [Required(ErrorMessage = "orderpipe.pop.addresses.civilityrequired")]
        public string GenderId { get; set; }

        [Required(ErrorMessage = "orderpipe.pop.addresses.cellphonerequired")]
        [StringLength(20)]
        [CountryAwarePhoneValidation("CountryId", "orderpipe.pop.format.phone.invalidformatforselectedcountry")]
        public string Phone { get; set; }

        [StringLength(50)]
        public string AddressName { get; set; }

        [Required(ErrorMessage = "orderpipe.pop.addresses.addressrequired")]
        [RegularExpression(@"^[^():]+$", ErrorMessage = "orderpipe.pop.format.onlyletters")]
        public string Address { get; set; }

        [StringLength(100)]
        public string Floor { get; set; }

        [StringLength(100)]
        public string AddressComplimentary { get; set; }

        public bool CompanyNameIsRequired { get; set; }

        [StringLength(50)]
        public string CompanyName { get; set; }

        [Required(ErrorMessage = "orderpipe.pop.addresses.cityrequired")]
        [StringLength(30)]
        public string City { get; set; }

        [StringLength(30)]
        public string State { get; set; }

        [Required(ErrorMessage = "orderpipe.pop.addresses.zipcoderequired")]
        [StringLength(20)]
        [CountryAwareZipCodeValidation("CountryId", "orderpipe.pop.format.zipcode.invalidformatforselectedcountry")]
        public string ZipCode { get; set; }

        public string Country { get; set; }

        [RegularExpression(@"^[1-9][0-9]*$", ErrorMessage = "orderpipe.pop.addresses.countryrequired")]
        public int CountryId { get; set; }

        public bool DisplayDocumentCountryId { get; set; }

        public string DocumentCountry { get; set; }

        public int DocumentCountryId { get; set; }

        public bool ByPassQas { get; set; }

        public int? SellerId { get; set; }

        public bool UseAsDefaultBillingAddress { get; set; }

        public bool HasMissingInformations { get; set; }

        public IEnumerable<CountryViewModel> Countries { get; set; }

        public IEnumerable<StateViewModel> States { get; set; }

        public IEnumerable<CivilityViewModel> Genders { get; set; }

        [RequiredGuest("orderpipe.pop.addresses.email.required")]
        [EmailAddress(ErrorMessage = "orderpipe.pop.login.email.valid")]
        public string Email { get; set; }

        [RequiredGuest("orderpipe.pop.addresses.confirm.email.required")]
        [EmailAddress(ErrorMessage = "orderpipe.pop.login.email.valid")]
        [System.ComponentModel.DataAnnotations.Compare(otherProperty: "Email", ErrorMessage = "orderpipe.pop.addresses.confirm.email.compare")]
        public string ConfirmEmail { get; set; }

        public string TaxIdNumber { get; set; }

        public string TaxNumberType { get; set; }

        public IEnumerable<TaxNumberTypeViewModel> TaxNumberTypeViewModels { get; set; }

        public int LegalStatus { get; set; }

        public IEnumerable<LegalStatusViewModel> LegalStatusViewModels { get; set; }

        public string CustomizedContainer
        {
            get { return "PopAddress"; }
        }

        public bool UseAsDefaultAddress { get; set; }

        public bool CountryIsWebSiteCountry { get; set; }

        public bool CountryIsInEU { get; set; }

        public AddressViewModel()
        {
            Countries = new List<CountryViewModel>();
            States = new List<StateViewModel>();
            Genders = new List<CivilityViewModel>();
            LegalStatusViewModels = new List<LegalStatusViewModel>();
            TaxNumberTypeViewModels = new List<TaxNumberTypeViewModel>();
        }

        public AddressEntity ToAddressEntity(ICountryService countryService)
        {
            var selectedCountry = countryService.GetCountries().FirstOrDefault(c => c.ID == CountryId);
            return new AddressEntity
            {
                AddressLine1 = Address,
                AddressLine2 = Floor,
                AddressLine3 = AddressComplimentary,
                Zipcode = ZipCode,
                City = City,
                CountryId = CountryId,
                State = State,
                Company = CompanyName,
                CountryCode2 = selectedCountry?.Code2,
                ShippingZone = selectedCountry?.ShippingZone ?? 0,
                TaxIdNumber = TaxIdNumber,
                EMail = Email,
                CellPhone = Phone
            };
        }

        public BillingAddress ToBillingAddress(ICountryService countryService)
        {
            var address = ToAddressEntity(countryService);
            return new BillingAddress(address);
        }
    }

   
}
