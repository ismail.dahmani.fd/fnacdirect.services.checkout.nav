using FnacDirect.Customer.Model;
using FnacDirect.OrderPipe.Base.Business.PaymentConfiguration;
using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.OrderPipe.Base.Model.Facets;
using FnacDirect.OrderPipe.Base.OneClick;
using FnacDirect.Technical.Framework.Utils;

namespace FnacDirect.OrderPipe.Base.Business.Ogone
{
    public class OgoneCvcStatusService : IOgoneCvcStatusService
    {
        private readonly IPaymentConfigurationBusiness _paymentConfigurationBusiness;
        private readonly IDateTimeProvider _dateTimeProvider;
        
        public OgoneCvcStatusService(IPaymentConfigurationBusiness paymentConfigurationBusiness,
                                     IDateTimeProvider dateTimeProvider)
        {
            _paymentConfigurationBusiness = paymentConfigurationBusiness;
            _dateTimeProvider = dateTimeProvider;
        }

        public bool IsRequiredForCardType(int creditCardTypeId,
                                          IGotUserContextInformations gotUserContextInformations,
                                          IGotLineGroups gotLineGroups,
                                          IGotShippingMethodEvaluation gotShippingMethodEvaluation,
                                          IGotBillingInformations gotBillingInformations)
        {
            var totalAmount = decimal.MaxValue;
            var data = gotBillingInformations.GetPrivateData<PaymentSelectionData>(create: false);
            if (data != null)
            {
                totalAmount = data.RemainingAmount;
            }

            return _paymentConfigurationBusiness.GetPaymentSecurityConfiguration(gotLineGroups.LineGroups,
                                                                                 gotUserContextInformations.UserContextInformations,
                                                                                 gotShippingMethodEvaluation.ShippingMethodEvaluation,
                                                                                 totalAmount,
                                                                                 creditCardTypeId) != OrderPipe.PaymentConfiguration.Results.SecurityType.Nothing;

        }

        public bool IsRequiredForCard(CreditCardEntity customerCreditCard,
                                      IGotUserContextInformations gotUserContextInformations,
                                      IGotLineGroups gotLineGroups,
                                      IGotShippingMethodEvaluation gotShippingMethodEvaluation,
                                      IGotBillingInformations gotBillingInformations)
        {
            const int maxSecondsSinceAliasCreation = 120;

            var now = _dateTimeProvider.Now();

            if ((now - customerCreditCard.CreateDate).TotalSeconds > maxSecondsSinceAliasCreation)
            {
                if (!IsOneClick(gotUserContextInformations))
                {
                    return true;
                }
                else
                {
                    var isValidatedUseForPreviousOrder = !customerCreditCard.IsValidatedUsedForPreviousOrder.HasValue
                                                            || customerCreditCard.IsValidatedUsedForPreviousOrder.Value;

                    if (isValidatedUseForPreviousOrder)
                    {
                        return IsRequiredForCardType(customerCreditCard.TypeId,
                                                     gotUserContextInformations,
                                                     gotLineGroups,
                                                     gotShippingMethodEvaluation,
                                                     gotBillingInformations);
                    }
                    else
                    {
                        return true;
                    }
                }
            }

            return false;
        }

        private static bool IsOneClick(IGotUserContextInformations gotUserContextInformations)
        {
            var oneClickData = gotUserContextInformations.GetPrivateData<OneClickData>(create: false);

            return oneClickData != null && oneClickData.IsAvailable.GetValueOrDefault();
        }
    }
}
