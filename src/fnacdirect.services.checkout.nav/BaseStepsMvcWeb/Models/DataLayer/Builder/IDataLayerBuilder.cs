using FnacDarty.FnacCom.DataLayer.Models.Request;
using FnacDirect.OrderPipe.Base.Model;
using System.Collections.Generic;
using System.Web.Mvc;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.DataLayer
{
    public interface IDataLayerBuilder
    {
        CartRequest BuildCartRequest(PopOrderForm popOrderForm);

        PageRequest BuildPageRequest(DatalayerViewModel model, ViewResultBase viewResultBase, ControllerContext controllerContext);

        UserRequest BuildUserRequest();

        TransactionRequest BuildTransactionRequest(DatalayerViewModel model);

        IEnumerable<OrderRequest> BuildOrderRequests(DatalayerViewModel model);
    }
}
