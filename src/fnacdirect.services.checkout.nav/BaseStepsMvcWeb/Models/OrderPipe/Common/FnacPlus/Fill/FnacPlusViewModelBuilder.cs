using FnacDirect.Contracts.Online.IModel;
using FnacDirect.Membership.Model.Constants;
using FnacDirect.OrderPipe.Base.BaseModel.Model;
using FnacDirect.OrderPipe.Base.Business;
using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.OrderPipe.Base.Proxy.Membership;
using FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Membership;
using FnacDirect.Technical.Framework.Web;
using FnacDirect.Technical.Framework.Web.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Common.Basket
{
    public class FnacPlusViewModelBuilder : IFnacPlusViewModelBuilder
    {
        private readonly IPriceHelper _priceHelper;
        private readonly IApplicationContext _applicationContext;
        private IMembershipConfigurationService _membershipConfigurationService;

        public FnacPlusViewModelBuilder(IMembershipConfigurationService membershipConfigurationService, IApplicationContext applicationContext)
        {
            _membershipConfigurationService = membershipConfigurationService;
            _applicationContext = applicationContext;
            _priceHelper = _applicationContext.GetPriceHelper();
        }

        public void Fill(IGotFnacPlusData viewModel, PushFnacPlusAdherentData sourceData)
        {
            // Discounts
            viewModel.FnacPlusDifferedDiscount = _priceHelper.Format(sourceData.SimulateDifferedAmount, PriceFormat.CurrencyAsDecimalSeparator);
            viewModel.FnacPlusImmediateDiscount = _priceHelper.Format(sourceData.SimulateSavingAmount, PriceFormat.CurrencyAsDecimalSeparator);
            viewModel.HasFnacPlusDiscount = (sourceData.SimulateSavingAmount + sourceData.SimulateDifferedAmount > 0);
            viewModel.HasFnacPlusImmediateDiscount = sourceData.SimulateSavingAmount > 0;
            viewModel.HasFnacPlusDifferedDiscount = sourceData.SimulateDifferedAmount > 0;

            // Global stats
            viewModel.IsFnacPlusEnabled = _membershipConfigurationService.IsFnacPlusEnabled;
            viewModel.IsFnacPlusCanalPlayEnabled = _membershipConfigurationService.IsFnacPlusCanalPlayEnabled;
            viewModel.HasFnacPlusCardInBasket = sourceData.HasFnacPlusCardInBasket;
            viewModel.HasAdherentCardInBasket = sourceData.HasAdhCardInBasket;

            viewModel.IsEligibleEssaiFnacPlus = sourceData.IsEligibleEssaiFnacPlus;
            viewModel.IsEligiblePushConfirm = sourceData.IsEligiblePushConfirm;
            viewModel.IsEligiblePushRecruit = sourceData.IsEligiblePushRecruit;
            viewModel.IsEligiblePushRenouvAdh = sourceData.IsEligiblePushRenouvAdh;
            viewModel.IsEligiblePushRenouvFnacPlus = sourceData.IsEligiblePushRenouvFnacPlus;

            // Fnac+ card
            if (sourceData.CardPlusInfos != null)
            {
                viewModel.CardPlusInfos = new MembershipPushCardViewModel
                {
                    Prid = sourceData.CardPlusInfos.Prid,
                    Label = sourceData.CardPlusInfos.Label,
                    HasOPC = sourceData.CardPlusInfos.HasOPC,
                    StandardPrice = _priceHelper.Format(sourceData.CardPlusInfos.StandardPrice, PriceFormat.CurrencyAsDecimalSeparator),
                    PriceWithOffer = _priceHelper.Format(sourceData.CardPlusInfos.PriceWithOffer, PriceFormat.CurrencyAsDecimalSeparator),
                    ValidityDuration = sourceData.CardPlusInfos.ValidityDuration,
                    ValidityType = sourceData.CardPlusInfos.ValidityType,
                    HasRenewCard = sourceData.RenewCardPlusInfos != null,
                    IsTryCard = sourceData.CardPlusInfos.IsTryCard
                };

                if (sourceData.RenewCardPlusInfos != null)
                {
                    viewModel.CardPlusInfos.RenewCardHasOPC = sourceData.RenewCardPlusInfos.HasOPC;
                    viewModel.CardPlusInfos.RenewCardStandardPrice = _priceHelper.Format(sourceData.RenewCardPlusInfos.StandardPrice, PriceFormat.CurrencyAsDecimalSeparator);
                    viewModel.CardPlusInfos.RenewCardPriceWithOffer = _priceHelper.Format(sourceData.RenewCardPlusInfos.PriceWithOffer, PriceFormat.CurrencyAsDecimalSeparator);
                }
            }
        }
    }
}
