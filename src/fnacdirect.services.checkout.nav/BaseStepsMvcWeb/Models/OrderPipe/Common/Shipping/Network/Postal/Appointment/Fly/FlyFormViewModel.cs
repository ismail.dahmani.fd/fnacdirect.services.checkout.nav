using FnacDirect.OrderPipe.Base.Model.Fly;
using FnacDirect.OrderPipe.Base.Model.Shipping.Postal.Appointment.Fly;
using System.ComponentModel.DataAnnotations;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Orderpipe.Common.Shipping.Network.Postal.Appointment.Fly
{
    public class FlyFormViewModel : IFlyForm
    {
        [Required]
        [Display(Name = "Services")]
        public FlyServiceByArticle[] SelectedFlyServices { get; set; }

        [Required]
        [Display(Name = "Slot")]
        public CalendarSlotIdentifier CalendarSlotIdentifier { get; set; }

        [Required]
        [Display(Name = "Moyen de livraison")]
        public int ShippingMethodId { get; set; }

        [Required]
        [Display(Name = "Nombre d'anciens produits à reprendre")]
        public int ProductQuantityToTakeBack { get; set; }

        [Required(AllowEmptyStrings = false)]
        [Display(Name = "Numéro de téléphone")]
        public string PhoneNumber { get; set; }

        [Required]
        [Display(Name = "Ascenseur")]
        public bool HasElevatorAccess { get; set; }

        [Required]
        [Display(Name = "Trois marches")]
        public bool HasStaircaseStep { get; set; }

        [MaxLength(5)]
        [Display(Name = "Code d'accès")]
        public string AccessCode { get; set; }

        [MaxLength(156)]
        [Display(Name = "Commentaires")]
        public string Commentary { get; set; }

        public int? FloorNumber { get; set; }

        public int? StaircaseType { get; set; }
    }
}
