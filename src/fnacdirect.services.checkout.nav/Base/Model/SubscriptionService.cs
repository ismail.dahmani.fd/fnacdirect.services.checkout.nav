using System.Collections.Generic;
using System.Linq;
using FnacDirect.Subscriptions.Business;
using FnacDirect.Subscriptions.Models;
using FnacDirect.Technical.Framework.Web.Mvc;

namespace FnacDirect.OrderPipe.Base.Business
{
    public class SubscriptionService : ISubscriptionService
    {
        private readonly IApplicationContext _applicationContext;
        private readonly ISubscriptionBusiness _subscriptionBusiness;

        public SubscriptionService(ISubscriptionBusiness subscriptionBusiness,
                                   IApplicationContext applicationContext)
        {
            _applicationContext = applicationContext;
            _subscriptionBusiness = subscriptionBusiness;
        }

        public Subscription GetSubscriptionByTypeId(int accountId, int typeId)
        {
            var contextInfo = new ContextInfo() { Culture = _applicationContext.GetSiteContext().Culture, Market = _applicationContext.GetSiteContext().Market };
            var subscriptions = _subscriptionBusiness.GetSubscriptionsInfoByTypeId(contextInfo, accountId, typeId);

            if (subscriptions != null && subscriptions.Any())
            {
                return subscriptions.FirstOrDefault();
            }

            return null;
        }

        public List<Subscription> GetSubscriptions(int accountId)
        {
            var contextInfo = new ContextInfo() { Culture = _applicationContext.GetSiteContext().Culture, Market = _applicationContext.GetSiteContext().Market };
            return _subscriptionBusiness.GetSubscriptions(contextInfo, accountId);
        }
    }
}
