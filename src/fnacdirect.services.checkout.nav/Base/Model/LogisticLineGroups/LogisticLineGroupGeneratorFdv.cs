using FnacDirect.OrderPipe.Base.LineGroups;
using FnacDirect.Technical.Framework.Velocity;

namespace FnacDirect.OrderPipe.Base.Business.LogisticLineGroups
{
    public class LogisticLineGroupGeneratorFdv : LogisticLineGroupGenerator
    {
        public LogisticLineGroupGeneratorFdv(ICloneService cloneService)
            : base(cloneService)
        {

        }

        protected override void SetLineGroupUniqueId(ILineGroupHeader lineGroupHeader, LogisticLineGroup logisticLineGroup)
        {
            //Pour Fnacshop on ne veux pas écraser le UniqueId du LineGroup pour eviter les doublons
            //On laisse donc vide cette methode
        }
    }
}
