using System;
using FnacDirect.Unicre.Model;
using FnacDirect.Ogone.Model;


namespace FnacDirect.OrderPipe.Base.Model
{
    public class UnicreCreditCardBillingMethod : BillingMethod
    {
        public const int IdBillingMethod = 1;

        public override bool IsPrimary => true;

        public static string BillingMethodName
        {
            get
            {
                return "UnicreCreditCardBillingMethod";
            }
        }
        private DateTime transactionDate;
         
        public DateTime TransactionDate
        {
            get { return transactionDate; }
            set { transactionDate = value; }
        }

        private int transactionId;

        public int TransactionId
        {
            get { return transactionId; }
            set { transactionId = value; }
        }

        private string paymentMean;

        public string PaymentMean
        {
            get { return paymentMean; }
            set { paymentMean = value; }
        }

        public UnicreCreditCardBillingMethod()
        {
            BillingMethodId = SipsBillingMethod.IdBillingMethod;
            CanModifyAmountUp = true;
            CanModifyAmountDown = true;
            UseMonetique = false;
            PaymentAction = PaymentActionEnum.Authorization;
            OperationType = TransactionOperationType.INIT;
        }

        public override bool UseTransaction
        {
            get
            {
                return true;
            }
        }

        public PaymentActionEnum PaymentAction { get; set; }

        public TransactionOperationType OperationType { get; set; }
    }
}
