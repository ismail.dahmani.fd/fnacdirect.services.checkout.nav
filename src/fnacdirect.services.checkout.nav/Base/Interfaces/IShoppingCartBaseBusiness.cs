using System.Collections.Generic;
using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.OrderPipe.Base.Model.Facets;
using FnacDirect.OrderPipe.Base.Model.LineGroups;

namespace FnacDirect.OrderPipe.Base.Business
{
    public interface IShoppingCartBaseBusiness
    {
        void TransfertOrderFormToShoppingCart(MultiFacet<IGotLineGroups, IGotUserContextInformations> multiFacets);
        void EmptyShoppingCart(IGotUserContextInformations igotUserContext, IEnumerable<ILineGroup> lineGroups);
        void AddArticleToFnacBasket(IGotLineGroups iGotLineGroups, StandardArticle article);
    }
}
