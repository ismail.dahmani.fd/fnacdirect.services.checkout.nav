﻿using FnacDirect.FnacStore.Business;
using FnacDirect.OrderPipe.Base.Business.Shipping.Shop;
using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.OrderPipe.Base.Model.ClickAndCollect;
using FnacDirect.OrderPipe.Base.Model.LineGroups;
using FnacDirect.OrderPipe.Shipping.Model;
using System;
using System.Collections.Generic;
using System.Linq;

namespace FnacDirect.OrderPipe.Base.Business.Shipping.Rules
{
    public class NextDayDeliveryShippingMethodBasketBuildingRule : BaseShippingMethodBasketBuildingRule
    {
        private readonly IShipToStoreAvailabilityService _shipToStoreAvailabilityService;
        private readonly IFnacStoreBusiness _fnacStoreBusiness;

        public NextDayDeliveryShippingMethodBasketBuildingRule(IFnacStoreBusiness fnacStoreBusiness, IShipToStoreAvailabilityService shipToStoreAvailabilityService)
        {
            _fnacStoreBusiness = fnacStoreBusiness;
            _shipToStoreAvailabilityService = shipToStoreAvailabilityService;
        }

        public override string SwitchId
        {
            get
            {
                return "orderpipe.pop.nextdaydelivery";
            }
        }

        public override Func<ShippingMethodBasket, ShippingMethodBasket> GetModifier(IPrivateDataContainer privateDataContainer, IEnumerable<Article> articles, ShippingAddress shippingAddress)
        {
            var shopAddress = shippingAddress as ShopAddress;

            if(shopAddress == null)
            {
                return shippingMethodBasket => shippingMethodBasket;
            }

            var shopId = shopAddress.Identity.GetValueOrDefault();

            if(shopId <= 0)
            {
                return shippingMethodBasket => shippingMethodBasket;
            }

            var store = _fnacStoreBusiness.GetStoreById(shopId);

            if(store == null)
            {
                return shippingMethodBasket => shippingMethodBasket;
            }

            if(!store.NextDayDelivery)
            {
                return shippingMethodBasket => shippingMethodBasket;
            }

            var shippingMethods = PipeParametersProvider.GetAsEnumerable<int>("shippingmethodid.nextdaydelivery");

            return shippingMethodBasket =>
            {
                foreach(var shippingMethod in shippingMethods)
                {
                    shippingMethodBasket.TriggerShadowShippingMethods.Add(new SingleTriggerShadowShippingMethod(shippingMethod));
                }
                return shippingMethodBasket;
            };
        }
    }
}
