using FnacDirect.OrderPipe.Base.Model;
using System.Collections.Generic;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Common.Shipping
{
    public class AddressViewModel
    {
        public AddressViewModel() { }

        public AddressViewModel(PostalAddress postalAddress)
        {
            Id = postalAddress.Identity;
            Reference = postalAddress.Reference;

            if (string.IsNullOrEmpty(postalAddress.Alias))
            {
                Alias = $"{postalAddress.Firstname} {postalAddress.LastName} {postalAddress.City}";
            }
            else
            {
                Alias = postalAddress.Alias;
            }

            ShouldDisplayCompany = !string.IsNullOrEmpty(postalAddress.Company);
            CompanyName = postalAddress.Company;

            AddressLine1 = postalAddress.AddressLine1;

            ShouldDisplayAddressLine2 = !string.IsNullOrEmpty(postalAddress.AddressLine2);
            AddressLine2 = postalAddress.AddressLine2;

            ShouldDisplayAddressLine3 = !string.IsNullOrEmpty(postalAddress.AddressLine3);
            AddressLine3 = postalAddress.AddressLine3;

            City = postalAddress.City;
            ZipCode = postalAddress.ZipCode;
            Lastname = postalAddress.LastName;
            Firstname = postalAddress.Firstname;
            GenderId = postalAddress.GenderId;
            CellPhone = postalAddress.CellPhone;
            Phone = postalAddress.GetPhone();

            Country = postalAddress.CountryLabel;
            CountryId = postalAddress.CountryId.GetValueOrDefault();
            State = postalAddress.State;

            TaxIdNumber = postalAddress.TaxNumber;
            var TaxNumberTypeUnknow = "-1"; //Todo maybe use e kind of OnePage.Address.TaxNumberType.Unknow but it is 99 change it to -1 and prey or create missing with -1
            TaxNumberType = string.IsNullOrEmpty(postalAddress.TaxNumberType) ? TaxNumberTypeUnknow : postalAddress.TaxNumberType;
            DocumentCountryId = postalAddress.DocumentCountryId;

            LegalStatus = postalAddress.LegalStatus;
            IsPostal = true;
            IsRelay = false;
            IsStore = false;
            IsOversea = postalAddress.IsOversea;
        }

        public AddressViewModel(AddressViewModel addressViewModel)
        {
            AddressLine1 = addressViewModel.AddressLine1;
            AddressLine2 = addressViewModel.AddressLine2;
            AddressLine3 = addressViewModel.AddressLine3;
            Alias = addressViewModel.Alias;
            City = addressViewModel.City;
            CompanyName = addressViewModel.CompanyName;
            Country = addressViewModel.Country;
            CountryId = addressViewModel.CountryId;
            State = addressViewModel.State;
            Phone = addressViewModel.Phone;
            CellPhone = addressViewModel.CellPhone;
            GenderId = addressViewModel.GenderId;
            Firstname = addressViewModel.Firstname;
            Id = addressViewModel.Id;
            Lastname = addressViewModel.Lastname;
            Reference = addressViewModel.Reference;
            ShouldDisplayAddressLine2 = addressViewModel.ShouldDisplayAddressLine2;
            ShouldDisplayAddressLine3 = addressViewModel.ShouldDisplayAddressLine3;
            ShouldDisplayCompany = addressViewModel.ShouldDisplayCompany;
            ShouldDisplayCountry = addressViewModel.ShouldDisplayCountry;
            ZipCode = addressViewModel.ZipCode;
            HasMissingInformations = addressViewModel.HasMissingInformations;
            TaxIdNumber = addressViewModel.TaxIdNumber;
            TaxNumberType = addressViewModel.TaxNumberType;
            DocumentCountryId = addressViewModel.DocumentCountryId;
            IsPostal = addressViewModel.IsPostal;
            IsRelay = addressViewModel.IsRelay;
            IsStore = addressViewModel.IsStore;
            Timetables = addressViewModel.Timetables;
            ExceptionnalTimetables = addressViewModel.ExceptionnalTimetables;
            Email = addressViewModel.Email;
            ConfirmEmail = addressViewModel.ConfirmEmail;
            LegalStatus = addressViewModel.LegalStatus;
            IsShippingAndBillingAddresseAreEqual = addressViewModel.IsShippingAndBillingAddresseAreEqual;
            IsOversea = addressViewModel.IsOversea;
        }

        public int? Id { get; set; }

        public string Reference { get; set; }

        public string Alias { get; set; }

        //TODO update all reference of Alias to use AddressName think about reference in web.sln
        public string AddressName { get { return Alias; } }

        public int GenderId { get; set; }

        public string Firstname { get; set; }

        public string Lastname { get; set; }

        public bool ShouldDisplayCompany { get; set; }

        public string CompanyName { get; set; }

        //TODO : This should be removed tu alway use CompanyName, Update all AddressViewModel.Company in thw web.sln
        public string Company { get { return CompanyName; } }

        public string AddressLine1 { get; set; }

        public bool ShouldDisplayAddressLine2 { get; set; }

        public string AddressLine2 { get; set; }

        public bool ShouldDisplayAddressLine3 { get; set; }

        public string AddressLine3 { get; set; }

        public string ZipCode { get; set; }

        public string City { get; set; }

        public bool ShouldDisplayCountry { get; set; }

        public int CountryId { get; set; }

        public string TaxNumberType { get; set; }

        public int DocumentCountryId { get; set; }

        public int LegalStatus { get; set; } = 1;

        public string State { get; set; }

        public string Country { get; set; }

        public string Phone { get; set; }

        public string CellPhone { get; set; }

        public string Email { get; set; }

        public string ConfirmEmail { get; set; }

        public bool IsShippingAndBillingAddresseAreEqual { get; set; }

        public bool IsPreviewPayment { get; set; }

        public bool HasMissingInformations { get; set; }

        public string TaxIdNumber { get; set; }

        public bool IsPostal { get; set; }

        public bool IsRelay { get; set; }

        public bool IsStore { get; set; }

        public int NetworkId
        {
            get
            {
                return IsPostal ? (int)Customer.Model.Constants.ShippingNetwork.Postal :
                       IsRelay  ? (int)Customer.Model.Constants.ShippingNetwork.Relay :
                       IsStore  ? (int)Customer.Model.Constants.ShippingNetwork.Store :
                       0;
            }
        }

        public bool IsOversea { get; set; }

        public IEnumerable<TimetableViewModel> Timetables { get; set; }

        public IEnumerable<Shop.ExceptionnalClosingViewModel> ExceptionnalTimetables { get; set; }
    }
}
