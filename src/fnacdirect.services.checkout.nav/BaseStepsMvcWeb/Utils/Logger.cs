namespace FnacDirect.OrderPipe
{
    using System.Collections.Generic;
    using System.Globalization;
    using System.Threading;

    public static class Logger
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1303:Do not pass literals as localized parameters", MessageId = "System.Web.TraceContext.Write(System.String)")]
        public static void Log(string s)
        {
            var msg = string.Format(CultureInfo.InvariantCulture, "[{0}]{1}", Thread.CurrentThread.ManagedThreadId, s);
            System.Diagnostics.Debug.WriteLine(msg);
            if (System.Web.HttpContext.Current != null)
            {
                System.Web.HttpContext.Current.Trace.Write(msg);
                Logs.Add(msg);
            }
        }

        public static IList<string> Logs
        {
            get
            {
                //RMN
                IList<string> logs = new List<string>();

                if (System.Web.HttpContext.Current.Session != null)
                {
                    logs = (IList<string>)System.Web.HttpContext.Current.Session.Contents[typeof(Logger).FullName];
                    if (logs == null)
                    {
                        logs = new List<string>();
                        System.Web.HttpContext.Current.Session.Contents[typeof(Logger).FullName] = logs;
                    }
                }
                return logs;
            }
        }
    }
}
