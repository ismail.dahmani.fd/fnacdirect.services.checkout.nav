using System;
using System.Collections.Generic;
using System.Text;
using FnacDirect.Customer.Model;

namespace FnacDirect.Quote.Model
{
    public class QuoteAddressEntity:AddressEntity
    {
        public QuoteAddressEntity()
        {
        }

        public QuoteAddressEntity(AddressEntity addressEntity)
        {
            ModelHelper.Copy(addressEntity, this);
        }
    }
    
    public class QuoteAddressEntityCollection : List<QuoteAddressEntity>
    {
        public QuoteAddressEntity FindAddress(string addressBookReference)
        {
            var l_result =
            Find(delegate(QuoteAddressEntity item)
            {
                return item.AddressBookReference == addressBookReference;
            });

            return l_result;
        }
    }
}
