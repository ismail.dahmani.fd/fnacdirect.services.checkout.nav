﻿using FnacDirect.OrderPipe.Base.LineGroups;
using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.OrderPipe.CM.Business;
using FnacDirect.Technical.Framework.Utils;

namespace FnacDirect.OrderPipe.FDV.Business
{
    public interface IGuptCMBusinessProvider
    {
        IGuptCMBusiness GetGuptCMBusiness(ILogisticLineGroup lg);
    }
}
