using FnacDirect.OrderPipe.Base.OneClick;

namespace FnacDirect.OrderPipe.Base.Model
{
    public static class OFExtensions
    {
        public static bool IsOneClick(this IOF oF)
        {
            var oneClickData = oF.GetPrivateData<OneClickData>(create: false);
            return oneClickData != null
                && oneClickData.IsAvailable.HasValue
                && oneClickData.IsAvailable.Value;
        }
    }
}
