using FnacDirect.OrderPipe.Base.Business.Payment;
using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.OrderPipe.Base.Model.Facets;
using FnacDirect.OrderPipe.Base.Proxy.Limonetik;
using FnacDirect.OrderPipe.Core.Services;
using FnacDirect.Technical.Framework.Web.Mvc;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace FnacDirect.OrderPipe.Base.Business.Limonetik
{
    public class LimonetikBusiness : ILimonetikBusiness
    {
        internal const string LimonetikSwitchName = "op.limonetik.enabled";
        internal const string LimonetikAllowedShippingIdsParam = "limonetik.allowed.shippingids";
        internal const string LimonetikAllowedCustomerRegexParam = "limonetik.allowedcustomer.regularexpression";
        internal const string LimonetikSpecificAllowedCustomerRegexParam = "limonetik.specific.allowedcustomer.regularexpression";
        internal const string LimonetikSpecificListParam = "limonetik.specific.list";
        internal const string AnonymousBillingMethodsMaxAmountParam = "anonymous.billing.methods.max.amount";
        internal const string OpPopAnonymousBillingMethodsMaxAmountSwitchName = "orderpipe.pop.max.amount.anonymous.billing.methods";
        internal const string ECCVTypeIdsParam = "eccv.typeid";
        internal const string DonationTypeIdAppSetting = "DonationTypeId";
        internal const int DefaultMaxAmountAllowed = 1000;

        private readonly ISwitchProvider _switchProvider;
        private readonly ILimonetikCardTypeService _limonetikCardTypeService;
        private readonly IPipeParametersProvider _pipeParametersProvider;
        private readonly ICheckOriginService _checkOriginService;
        private readonly int _donationTypeId;

        public LimonetikBusiness(ISwitchProvider switchProvider,
                                 ILimonetikCardTypeService limonetikCardTypeService,
                                 IApplicationContext applicationContext,
                                 IPipeParametersProvider pipeParametersProvider,
                                 ICheckOriginService checkOriginService)
        {
            _switchProvider = switchProvider;
            _limonetikCardTypeService = limonetikCardTypeService;
            _donationTypeId = int.Parse(applicationContext.GetAppSetting(DonationTypeIdAppSetting));
            _pipeParametersProvider = pipeParametersProvider;
            _checkOriginService = checkOriginService;
        }

        public bool IsEnabled(OPContext opContext)
        {
            if (!_switchProvider.IsEnabled(LimonetikSwitchName))
                return false;

            if (_checkOriginService.IsFromCallCenter(opContext.OF as IGotUserContextInformations))
                return false;

            var accountId = (opContext.OF as IGotUserContextInformations).UserContextInformations.AccountId.ToString();
            var allowedCustomerRegex = _pipeParametersProvider.Get(LimonetikAllowedCustomerRegexParam, string.Empty);
            if (!IsCustomerAllowed(accountId, allowedCustomerRegex))
                return false;

            var gotLineGroups = opContext.OF as IGotLineGroups;

            // si le panier contient des MP
            if (gotLineGroups.LineGroups.GetArticlesOfType<MarketPlaceArticle>().Any())
                return false;

			// si ClickAndCollect
            if (gotLineGroups.HasClickAndCollectArticle())
                return false;

            if (gotLineGroups.LineGroups.Any(lg => lg.HasDematSoftArticles || lg.HasNumericalArticles))
                return false;

            if (gotLineGroups.IsMixed())
                return false;

            var eccvTypeIds = _pipeParametersProvider.GetAsRangeSet<int>(ECCVTypeIdsParam);
            var hasECCVInBasket = gotLineGroups.Articles.Any(a => a.TypeId.HasValue && eccvTypeIds.Contains(a.TypeId.Value));
            if (hasECCVInBasket)
                return false;

            var anonymousBillingMethodsMaxAmount = _pipeParametersProvider.Get<int>(AnonymousBillingMethodsMaxAmountParam, DefaultMaxAmountAllowed);
            var gotBillingInformation = opContext.OF as IGotBillingInformations;
            if (_switchProvider.IsEnabled(OpPopAnonymousBillingMethodsMaxAmountSwitchName)
                && gotBillingInformation.GlobalPrices.TotalPriceEur.GetValueOrDefault() > anonymousBillingMethodsMaxAmount)
                return false;

            var hasDonationArticle = gotLineGroups.Articles.Any(a => a.TypeId.HasValue && a.TypeId.Value == _donationTypeId);
            if (hasDonationArticle)
                return false;

            var allowedShippingIds = _pipeParametersProvider.GetAsRangeSet<int>(LimonetikAllowedShippingIdsParam);
            if (allowedShippingIds != null && !allowedShippingIds.IsEmpty)
            {
                foreach (var lg in gotLineGroups.LineGroups)
                {
                    foreach (var article in lg.Articles.OfType<BaseArticle>())
                    {
                        if (article.AvailabilityId != null && !allowedShippingIds.Contains(article.AvailabilityId.GetValueOrDefault(0)))
                        {
                            return false;
                        }
                    }
                }
            }

            return true;
        }

        public List<int> GetValidLimonetikCardTypes(OPContext opContext)
        {
            var limonetikSpecificCardTypeList = _pipeParametersProvider.GetAsRangeSet<int>(LimonetikSpecificListParam);
            var limonetikCardTypeList = _limonetikCardTypeService.GetLimonetikCardTypeIds();

            var specificCustomerRegex = _pipeParametersProvider.Get(LimonetikSpecificAllowedCustomerRegexParam, string.Empty);
            var accountId = (opContext.OF as IGotUserContextInformations).UserContextInformations.AccountId.ToString();

            return limonetikCardTypeList.Where(x => !limonetikSpecificCardTypeList.Contains(x) || IsCustomerAllowed(accountId, specificCustomerRegex)).ToList();
        }

        private static bool IsCustomerAllowed(string accountId, string regexString)
            => string.IsNullOrEmpty(regexString) || Regex.IsMatch(accountId, regexString);
    }
}
