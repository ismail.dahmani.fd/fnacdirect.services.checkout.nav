using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.Solex.AppointmentDelivery.Client.Contract;
using System;
using System.Collections.Generic;

namespace FnacDirect.OrderPipe.Base.Business.Shipping
{
    public interface IAppointmentDeliveryCalendarBusiness
    {
        CalendarQuery CreateGetCalendarQuery(DeliveryAddress deliveryAddress, List<Item> items, DateTime startDate, DateTime endDate, IEnumerable<Solex.AppointmentDelivery.Client.Contract.Promotion> promotions, int shippingMethodId);
        CalendarResult GetCalendarResult(CalendarQuery query);
        CalendarResult GetCalendarResult(DeliveryAddress deliveryAddress, List<Item> items, DateTime startDateReference, DateTime endDateMaximum, int pageSize, int pageIndex, IEnumerable<Solex.AppointmentDelivery.Client.Contract.Promotion> promotions, int shippingMethodId);
        List<Model.Solex.DeliverySlot>[] GetSortingSlotsTabs(IEnumerable<Model.Solex.ShippingChoice> choices, Model.Solex.DeliverySlotData deliverySlotData, IEnumerable<DeliverySlot> resultSlots, int pageSize, string sortingCriteria);
        PreOrderResult GetPreOrderResult(Model.Solex.DeliverySlot deliverySlotSelected, PostalAddress postalAddress, List<Item> items);
        Model.Solex.DeliverySlot CheckSlotAvailability(Model.Solex.DeliverySlot deliverySlotSelected, DeliveryAddress deliveryAddress, List<Item> items, IEnumerable<Solex.AppointmentDelivery.Client.Contract.Promotion> promotions, int shippingMethodId);
        IEnumerable<Solex.AppointmentDelivery.Client.Contract.Promotion> GetPromotions(IEnumerable<Model.Article> articles);
        AppointmentDeliverySlotsSortingResult GetSortedSlotsFromChoices(IEnumerable<Model.Solex.ShippingChoice> choices, PostalAddress shippingAddress, PopOrderForm popOrderForm, IEnumerable<Model.Article> articles, HashSet<int> shippingMethods, Model.Solex.DeliverySlotData deliverySlotData, int pageIndex, int pageSize, string sortingCriteria, DateTime endDateMaximum);
    }
}
