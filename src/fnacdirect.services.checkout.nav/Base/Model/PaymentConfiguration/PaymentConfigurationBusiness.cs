using FnacDirect.Ogone.Model;
using FnacDirect.OrderPipe.Base.BaseModel;
using FnacDirect.OrderPipe.Base.DAL;
using FnacDirect.OrderPipe.Base.LineGroups;
using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.OrderPipe.Base.Model.LineGroups;
using FnacDirect.OrderPipe.Base.Model.Solex;
using FnacDirect.OrderPipe.Base.Model.UserInformations;
using FnacDirect.OrderPipe.Base.OneClick;
using FnacDirect.OrderPipe.PaymentConfiguration.Criterias;
using FnacDirect.OrderPipe.PaymentConfiguration.Front;
using FnacDirect.OrderPipe.PaymentConfiguration.Models;
using FnacDirect.OrderPipe.PaymentConfiguration.Results;
using FnacDirect.Technical.Framework.CoreServices;
using FnacDirect.Technical.Framework.Web.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;

namespace FnacDirect.OrderPipe.Base.Business.PaymentConfiguration
{
    public class PaymentConfigurationBusiness : IPaymentConfigurationBusiness
    {
        private readonly IPaymentConfigurationService _paymentConfigurationService;
        private readonly Func<OPContext> _opContextFactory;
        private readonly IPaymentDAL _paymentDal;
        private readonly ICreditCardDescriptorBusiness _creditCardDescriptorBusiness;
        private readonly IApplicationContext _applicationContext;

        private readonly string CCVClickAndCollectBrand = "CCVONLY";
        private readonly short ExemptionDefaultResult = 4; // 04 = 3DS obligatoire

        private readonly HashSet<int> _excludedFromCBControlBillingMethods = new HashSet<int>
        {
            VirtualGiftCheckBillingMethod.IdBillingMethod,
            GiftCardBillingMethod.IdBillingMethod,
            AvoirOccazBillingMethod.IdBillingMethod,
            EurosFnacBillingMethod.IdBillingMethod,
            FidelityEurosBillingMethod.IdBillingMethod,
            FidelityPointsBillingMethod.IdBillingMethod,
            FidelityPointsOccazBillingMethod.IdBillingMethod
        };

        public PaymentConfigurationBusiness(IPaymentConfigurationService paymentConfigurationService,
                                            Func<OPContext> opContextFactory,
                                            IPaymentDAL paymentDal,
                                            ICreditCardDescriptorBusiness creditCardDescriptorBusiness,
                                            IApplicationContext applicationContext)
        {
            _paymentConfigurationService = paymentConfigurationService;
            _opContextFactory = opContextFactory;
            _paymentDal = paymentDal;
            _creditCardDescriptorBusiness = creditCardDescriptorBusiness;
            _applicationContext = applicationContext;
        }

        public IDictionary<string, AuthorizationAmountofLogisticalOrderResult> GetPaymentAuthorizationConfiguration(IEnumerable<ILogisticLineGroup> logisticLineGroups,
                                                                                     IUserContextInformations userContextInformations,
                                                                                     decimal totalAmount,
                                                                                     int creditCardTypeId)
        {
            try
            {
                var order = PreparePaymentConfigurationOrderInput(logisticLineGroups, userContextInformations, totalAmount, creditCardTypeId);
                var authorisationResult = _paymentConfigurationService.GetAuthorizationConfiguration(order);
                return authorisationResult;
            }
            catch (Exception exception)
            {
                Logging.Current.WriteError("Failure in AuthorizationAmountofLogisticalOrderResult. Fallbacked all orders to Amount", exception);
                return logisticLineGroups.Select(x => x.UniqueId)
                                         .ToDictionary(x => x, x => new AuthorizationAmountofLogisticalOrderResult() { Value = AuthorizationLogisticalOrderType.Amount });
            }
        }

        public SecurityType GetPaymentSecurityConfiguration(IEnumerable<ILineGroup> lineGroups,
                                                            IUserContextInformations userContextInformations,
                                                            ShippingMethodEvaluation shippingMethodEvaluation,
                                                            decimal totalAmount,
                                                            int creditCardTypeId)
        {
            try
            {
                var order = PreparePaymentConfigurationOrderInput(lineGroups, userContextInformations, shippingMethodEvaluation, totalAmount, creditCardTypeId);
                if (ShouldSetAuthorizationAmountFromService(order))
                {
                    SetAuthorizationAmountFromService(order);
                }

                var sercurityResult = _paymentConfigurationService.GetSecurityConfiguration(order);
                return sercurityResult.SecurityTypeToApply;
            }
            catch (Exception exception)
            {
                Logging.Current.WriteError("Failure in GetPaymentSecurityConfiguration. Fallback to 3DS", exception);
                return SecurityType._3DS;
            }
        }

        public SecurityType GetPaymentSecurityConfiguration(IEnumerable<ILogisticLineGroup> logisticLineGroups,
                                                            IUserContextInformations userContextInformations,
                                                            decimal totalAmount,
                                                            int creditCardTypeId)
        {
            try
            {
                var order = PreparePaymentConfigurationOrderInput(logisticLineGroups, userContextInformations, totalAmount, creditCardTypeId);
                if (ShouldSetAuthorizationAmountFromService(order))
                {
                    SetAuthorizationAmountFromService(order);
                }

                var sercurityResult = _paymentConfigurationService.GetSecurityConfiguration(order);
                return sercurityResult.SecurityTypeToApply;
            }
            catch (Exception exception)
            {
                Logging.Current.WriteError("Failure in GetPaymentSecurityConfiguration. Fallback to 3DS", exception);
                return SecurityType._3DS;
            }
        }

        public short GetPaymentExemptionLevel(IEnumerable<ILogisticLineGroup> logisticLineGroups,
                                                            IUserContextInformations userContextInformations,
                                                            decimal totalAmount,
                                                            int creditCardTypeId)
        {
            try
            {
                var order = PreparePaymentConfigurationOrderInput(logisticLineGroups, userContextInformations, totalAmount, creditCardTypeId);
                if (ShouldSetAuthorizationAmountFromService(order))
                {
                    SetAuthorizationAmountFromService(order);
                }

                var result = _paymentConfigurationService.GetExemptionConfiguration(order);
                return result.ExemptionLevel;
            }
            catch (Exception exception)
            {
                Logging.Current.WriteError("Failure in GetPaymentExemptionConfiguration. Fallback to exemption level 4", exception);
                return ExemptionDefaultResult;
            }
        }

        private Order InitializeOrder(IUserContextInformations userContextInformations, decimal totalAmount)
        {
            var userInformation = new UserInformations()
            {
                AccountCreationDate = userContextInformations.IsGuest ? DateTime.Now : userContextInformations.CustomerEntity.CreateDate,
                ReliabilityIndicator = userContextInformations.UserConfidence,
                HasAnyShippedOrder = new Lazy<bool>(() =>
                {
                    if (userContextInformations.IsGuest)
                    {
                        return false;
                    }

                    var orderId = _paymentDal.GetLatestShippedOrder(userContextInformations.AccountId);

                    return orderId.GetValueOrDefault() != 0;
                })
            };

            var order = new Order
            {
                TotalAmount = totalAmount,
                OrderDateTime = DateTime.Now,
                UserInformations = userInformation
            };

            var opContext = _opContextFactory();

            var oneClickData = opContext.OF.GetPrivateData<OneClickData>(create: false);
            var isOneClick = oneClickData != null && oneClickData.IsAvailable.GetValueOrDefault();

            if (string.Equals(opContext.Pipe.PipeName, "selfcheckout", StringComparison.InvariantCultureIgnoreCase))
            {
                order.PipeRoute = "selfcheckout";
            }
            else if (userContextInformations.IsGuest)
            {
                order.PipeRoute = "guest";
            }
            else if (isOneClick)
            {
                order.PipeRoute = "oneclick";
            }
            else
            {
                order.PipeRoute = opContext.Pipe.PipeName;
            }

            return order;
        }

        protected internal Order PreparePaymentConfigurationOrderInput(IEnumerable<ILineGroup> lineGroups,
                                                                       IUserContextInformations userContextInformations,
                                                                       ShippingMethodEvaluation shippingMethodEvaluation,
                                                                       decimal totalAmount,
                                                                       int creditCardTypeId)
        {
            var logisticalOrders = new List<LogisticalOrder>();

            foreach (var lineGroup in lineGroups)
            {
                var logisticalOrder = GetLogisticalOrder(lineGroup, shippingMethodEvaluation);

                logisticalOrders.Add(logisticalOrder);
            }

            var order = InitializeOrder(userContextInformations, totalAmount);
            order.LogisticOrders = logisticalOrders;

            var config = _applicationContext.GetAppSetting("config.shippingMethodSelection");
            var creditCardDescriptors = _creditCardDescriptorBusiness.GetCreditCards(config, _applicationContext.GetSiteContext().Culture);
            var creditCardDescriptor = creditCardDescriptors.FirstOrDefault(x => x.Id == creditCardTypeId);

            if (creditCardDescriptor != null)
            {
                order.CreditCardBrand = creditCardDescriptor.Label;
            }
            else
            {
                order.CreditCardBrand = string.Empty;
            }

            return order;
        }

        protected internal Order PreparePaymentConfigurationOrderInput(IEnumerable<ILogisticLineGroup> logisticLineGroups,
                                                                       IUserContextInformations userContextInformations,
                                                                       decimal totalAmount,
                                                                       int creditCardTypeId)
        {
            var logisticalOrders = new List<LogisticalOrder>();

            foreach (var logisticLineGroup in logisticLineGroups)
            {
                var logisticalOrder = GetLogisticalOrder(logisticLineGroup);

                logisticalOrders.Add(logisticalOrder);
            }

            var order = InitializeOrder(userContextInformations, totalAmount);
            order.LogisticOrders = logisticalOrders;

            var config = _applicationContext.GetAppSetting("config.shippingMethodSelection");
            var creditCardDescriptors = _creditCardDescriptorBusiness.GetCreditCards(config, _applicationContext.GetSiteContext().Culture);
            var creditCardDescriptor = creditCardDescriptors.FirstOrDefault(x => x.Id == creditCardTypeId);

            if (creditCardDescriptor != null)
            {
                order.CreditCardBrand = creditCardDescriptor.Label;
            }
            else
            {
                if (ShouldBypassCBCheck(logisticLineGroups))
                    order.CreditCardBrand = CCVClickAndCollectBrand;
                else
                    order.CreditCardBrand = string.Empty;
            }

            return order;
        }

        private bool ShouldBypassCBCheck(IEnumerable<ILogisticLineGroup> logisticLineGroups)
        {
            if (logisticLineGroups.Any() && logisticLineGroups.Count() == 1)
            {
                return logisticLineGroups.First().BillingMethodConsolidation.All(bmc => _excludedFromCBControlBillingMethods.Contains(bmc.Key));
            }

            return false;
        }

        private LogisticalOrder GetLogisticalOrder(ILineGroup lineGroup,
                                                   ShippingMethodEvaluation shippingMethodEvaluation)
        {
            ShippingChoice shippingChoice = null;

            if (shippingMethodEvaluation != null)
            {
                var maybeShippingMethodEvaluationGroup = shippingMethodEvaluation.Seller(lineGroup.Seller.SellerId);

                if (maybeShippingMethodEvaluationGroup != null
                    && maybeShippingMethodEvaluationGroup.HasValue)
                {
                    var shippingMethodEvaluationGroup = maybeShippingMethodEvaluationGroup.Value;

                    var shippingMethodEvaluationItem = shippingMethodEvaluationGroup.SelectedShippingMethodEvaluation;

                    if (shippingMethodEvaluationItem != null)
                    {
                        shippingChoice = shippingMethodEvaluationItem.Choices.FirstOrDefault(choice => choice.MainShippingMethodId == shippingMethodEvaluationItem.SelectedShippingMethodId);
                    }
                }
            }

            var articlesInfos = GetArticlesInfos(lineGroup.Articles, shippingChoice);

            var logisticalOrder = new LogisticalOrder()
            {
                Identifier = lineGroup.UniqueId.ToString(),
                OrderType = (LogisticalOrderType)lineGroup.Informations.OrderType,
                Amount = lineGroup.GlobalPrices.TotalPriceEur.GetValueOrDefault(),
                ArticlesInfos = articlesInfos,
            };

            return logisticalOrder;
        }

        private LogisticalOrder GetLogisticalOrder(ILogisticLineGroup logisticLineGroup)
        {
            var articlesInfos = GetArticlesInfos(logisticLineGroup.Articles, logisticLineGroup.SelectedShippingChoice);

            var logisticalOrder = new LogisticalOrder()
            {
                Identifier = logisticLineGroup.UniqueId.ToString(),
                OrderType = (LogisticalOrderType)logisticLineGroup.Informations.OrderType,
                ArticlesInfos = articlesInfos,
                Amount = logisticLineGroup.GlobalPrices.TotalPriceEur.GetValueOrDefault(),
                AuthorizationAmount = GetAuthorizationAmount(logisticLineGroup)
            };

            return logisticalOrder;
        }

        private List<ArticleInfos> GetArticlesInfos(List<Article> articles, ShippingChoice shippingChoice)
        {
            var articlesInfos = new List<ArticleInfos>();

            foreach (var article in articles)
            {
                var articleInfos = GetArticleInfos(article);

                if (shippingChoice != null)
                {
                    var itemInGroup = FindItemInChoice(shippingChoice, article);

                    if (itemInGroup != null && shippingChoice.Parcels.TryGetValue(itemInGroup.ParcelId, out var parcel))
                    {
                        articleInfos.ShippingMethodId = parcel.ShippingMethodId;
                        articleInfos.ShippingServiceId = parcel.ShippingServiceId;
                    }
                }

                articlesInfos.Add(articleInfos);
            }

            return articlesInfos;
        }

        private ItemInGroup FindItemInChoice(ShippingChoice choice, Article article)
        {
            foreach (var group in choice.Groups)
            {
                foreach (var item in group.Items)
                {
                    if (article is StandardArticle standardArticle && item.Identifier.Prid == standardArticle.ProductID)
                    {
                        return item;
                    }
                    else if (article is MarketPlaceArticle marketPlaceArticle && item.Identifier.OfferReference == marketPlaceArticle.Offer.Reference)
                    {
                        return item;
                    }
                }
            }

            return null;
        }

        private ArticleInfos GetArticleInfos(Article article)
        {
            var articleInfos = new ArticleInfos();

            if (article is StandardArticle && !(article is Service))
            {
                var standardArticle = article as StandardArticle;
                if (standardArticle.TypeId.HasValue)
                {
                    articleInfos.TypeId = standardArticle.TypeId.Value;
                    articleInfos.AvailabilityId = standardArticle.AvailabilityId.GetValueOrDefault();
                }
            }

            if (article is MarketPlaceArticle)
            {
                var marketPlaceArticle = article as MarketPlaceArticle;
                if (marketPlaceArticle.TypeId.HasValue)
                {
                    articleInfos.TypeId = marketPlaceArticle.TypeId.Value;
                    articleInfos.AvailabilityId = marketPlaceArticle.IsPreOrder && marketPlaceArticle.AvailabilityId.HasValue  ? marketPlaceArticle.AvailabilityId.Value : (int)AvailabilityEnum.StockPE;
                }
            }

            return articleInfos;
        }

        public bool ShouldControlCreditCard(IEnumerable<ILogisticLineGroup> logisticLineGroups,
                                            IUserContextInformations userContextInformations,
                                            decimal totalAmount,
                                            int creditCardTypeId)
        {
            try
            {
                var order = PreparePaymentConfigurationOrderInput(logisticLineGroups, userContextInformations, totalAmount, creditCardTypeId);
                if (ShouldSetAuthorizationAmountFromService(order))
                {
                    SetAuthorizationAmountFromService(order);
                }

                var creditCardControlResult = _paymentConfigurationService.GetCreditCardControlConfiguration(order);
                return creditCardControlResult.ShouldControlCreditCard;
            }
            catch (Exception exception)
            {
                Logging.Current.WriteError("Failure in ShouldControlCreditCard. Fallbacked to true (credit card should be controled)", exception);
                return true;
            }
        }


        private AuthorizationLogisticalOrderType? GetAuthorizationLogisticalOrderType(AutorisationAmount authorizationAmount)
        {
            switch (authorizationAmount)
            {
                case AutorisationAmount.OneEuro:
                    return AuthorizationLogisticalOrderType.OneEuro;
                case AutorisationAmount.HighestLogiticAmount:
                case AutorisationAmount.TotalAmount:
                    return AuthorizationLogisticalOrderType.Amount;
                case AutorisationAmount.ZeroEuro:
                    return AuthorizationLogisticalOrderType.ZeroEuro;
                default:
                    return null;
            }
        }

        private bool ShouldSetAuthorizationAmountFromService(Order order)
        {
            // Si aucune logistic order posséde une AuthorizationAmount, on suppose que le service n'a jamais été appellé.
            return !order.LogisticOrders.Empty()
                && order.LogisticOrders.All(lo => !lo.AuthorizationAmount.HasValue);
        }

        private void SetAuthorizationAmountFromService(Order order)
        {
            var authorisationResult = _paymentConfigurationService.GetAuthorizationConfiguration(order);

            foreach(var kvp in authorisationResult)
            {
                var logisticOrderIdentifier = kvp.Key;
                var authorizationResult = kvp.Value;

                var currentLogisticalOrder = order.LogisticOrders.FirstOrDefault(x => x.Identifier == logisticOrderIdentifier);

                if (currentLogisticalOrder == null)
                {
                    continue;
                }

                currentLogisticalOrder.AuthorizationAmount = authorizationResult.Value;
            }
        }

        private AuthorizationLogisticalOrderType? GetAuthorizationAmount(ILogisticLineGroup logisticLineGroup)
        {
            var orderTransactionInfos = logisticLineGroup.Informations?.OrderTransactionInfos?.GetByBillingId(OgoneCreditCardBillingMethod.IdBillingMethod);
            if (orderTransactionInfos == null)
            {
                return null;
            }

            var authorizationAmount = orderTransactionInfos.OrderTransaction?.AutorisationAmount;
            if (!authorizationAmount.HasValue)
            {
                return null;
            }

            return GetAuthorizationLogisticalOrderType(authorizationAmount.Value);
        }
    }
}
