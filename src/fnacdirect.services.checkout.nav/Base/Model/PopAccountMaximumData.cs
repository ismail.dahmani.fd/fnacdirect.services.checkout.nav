using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FnacDirect.OrderPipe.Base.Model.Pop
{
    public class PopAccountMaximumData : GroupData
    {
        public List<int> MaxAccountDeletedArticles { get; set; } = new List<int>();
        public bool HasAlreadyBenefitedOfOffer { get; set; }
        public bool HasNoArticles { get; set; }
    }
}
