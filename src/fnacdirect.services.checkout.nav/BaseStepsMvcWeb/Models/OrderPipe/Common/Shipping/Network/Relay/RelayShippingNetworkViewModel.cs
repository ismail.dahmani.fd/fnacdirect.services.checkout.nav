using System.Collections.Generic;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Common.Shipping.Relay
{
    public class RelayShippingNetworkViewModel : ShippingNetworkViewModel<RelayDetailsViewModel>
    {
        private readonly List<RelayViewModel> _relayViewModels = new List<RelayViewModel>();

        public IReadOnlyCollection<RelayViewModel> Relays
        {
            get
            {
                return _relayViewModels.AsReadOnly();
            }
        }

        public bool HasMarketingEvent { get; set; }

        public string MarketingEventPicto { get; set; }

        public RelayShippingNetworkViewModel With(params RelayViewModel[] shopViewModels)
        {
            _relayViewModels.AddRange(shopViewModels);
            return this;
        }

        public string WarningMessage { get; set; }
    }
}
