﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Common.Shipping.Shop
{
    public class ShopDetailsViewModel : ShopViewModel, ISelectedShippingItem
    {
        public ShopDetailsViewModel(FnacStore.Model.FnacStore fnacStore)
            : base(fnacStore)
        {
            Timetables = Enumerable.Empty<TimetableViewModel>();
            ExceptionnalTimetables = Enumerable.Empty<ExceptionnalClosingViewModel>();
        }

        public string ShortTimetable { get; set; }

        public double Latitude { get; set; }

        public double Longitude { get; set; }

        public string Distance { get; set; }

        public double DistanceInMeters { get; set; }
        
        public IEnumerable<TimetableViewModel> Timetables { get; set; }

        public IEnumerable<ExceptionnalClosingViewModel> ExceptionnalTimetables { get; set; }

        public string Key
        {
            get
            {
                return "Shop";
            }
        }

        public SelectedShippingItemSummary Summary { get; set; }

        public int RefUG { get; set; }
    }
}
