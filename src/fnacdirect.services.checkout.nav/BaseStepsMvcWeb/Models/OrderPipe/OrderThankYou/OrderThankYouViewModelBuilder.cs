using FnacDirect.Context;
using FnacDirect.Customer.BLL.Authentication;
using FnacDirect.Customer.Segmentation.Business.Interfaces;
using FnacDirect.Membership.Model.Constants;
using FnacDirect.Ogone.Model;
using FnacDirect.OrderPipe.Base.BaseModel.Model;
using FnacDirect.OrderPipe.Base.BaseModel.Model.OrderDetails;
using FnacDirect.OrderPipe.Base.Business;
using FnacDirect.OrderPipe.Base.Business.Shipping;
using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.OrderPipe.Base.Model.BillingMethods;
using FnacDirect.OrderPipe.Base.Model.Facets;
using FnacDirect.OrderPipe.Base.Model.Pop;
using FnacDirect.OrderPipe.Base.Model.Solex;
using FnacDirect.OrderPipe.Base.Model.ThankYou;
using FnacDirect.OrderPipe.Base.Proxy.Appointment;
using FnacDirect.OrderPipe.Base.Proxy.Store;
using FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Common.Payment;
using FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Common.Shipping.Shop;
using FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Solex.Appointment;
using FnacDirect.OrderPipe.BaseMvc.Web.Services.ShipToStore;
using FnacDirect.Technical.Framework.Utils;
using FnacDirect.Technical.Framework.Web.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using Vanilla.Middle.Arborescence;
using Vanilla.Middle.Arborescence.Enums;
using static FnacDirect.OrderPipe.Constants;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.OrderThankYou
{
    public class OrderThankYouViewModelBuilder : IOrderThankYouViewModelBuilder
    {
        private readonly ITvRoyaltyViewModelBuilder _tvRoyaltyViewModelBuilder;
        private readonly IShopShippingNetworkViewModelBuilder _shopShippingNetworkViewModelBuilder;
        private readonly IArticleDetailService _articleDetailService;
        private readonly IApplicationContext _applicationContext;
        private readonly IAppointmentService _appointmentService;
        private readonly IPriceHelper _priceHelper;
        private readonly IStoreSearchService _storeSearchService;
        private readonly IFnacStoreService _fnacStoreService;
        private readonly ISwitchProvider _switchProvider;
        private readonly int _donationTypeId;
        private readonly IAppointmentDeliveryService _appointmentDeliveryService;
        private readonly IUserExtendedContext _userExtendedContext;
        private readonly IAccountIdentityProviderAssociationService _accountIdentityProviderAssociationService;
        private readonly ITvRoyaltyBusiness _tvRoyaltyBusiness;
        private readonly IUserContext _userContext;
        private readonly ISegmentationService _segmentationService;
        private readonly IMarketPlaceServiceProvider _marketPlaceServiceProvider;
        private readonly int _canalPlayNodeId;
        private const string SegmentCustomerOkCPlay = "OKCPLAY";

        public OrderThankYouViewModelBuilder(IArticleDetailService articleDetailService, IApplicationContext applicationContext, IAppointmentService appointmentService,
                                             ITvRoyaltyViewModelBuilder tvRoyaltyViewModelBuilder, IStoreSearchService storeSearchService, IShopShippingNetworkViewModelBuilder shopShippingNetworkViewModelBuilder,
                                             IFnacStoreService fnacStoreService, ISwitchProvider switchProvider, IUserExtendedContext userExtendedContext,
                                             IAccountIdentityProviderAssociationService accountIdentityProviderAssociationService, ITvRoyaltyBusiness tvRoyaltyBusiness, ISegmentationService segmentationService, 
                                             IUserContext userContext, IMarketPlaceServiceProvider marketPlaceServiceProvider, IAppointmentDeliveryService appointmentDeliveryService)
        {
            _articleDetailService = articleDetailService;
            _applicationContext = applicationContext;
            _appointmentService = appointmentService;
            _tvRoyaltyViewModelBuilder = tvRoyaltyViewModelBuilder;
            _priceHelper = applicationContext.GetPriceHelper();
            _storeSearchService = storeSearchService;
            _shopShippingNetworkViewModelBuilder = shopShippingNetworkViewModelBuilder;
            _fnacStoreService = fnacStoreService;
            _switchProvider = switchProvider;
            _userExtendedContext = userExtendedContext;
            _accountIdentityProviderAssociationService = accountIdentityProviderAssociationService;
            _tvRoyaltyBusiness = tvRoyaltyBusiness;
            _userContext = userContext;
            _segmentationService = segmentationService;
            _marketPlaceServiceProvider = marketPlaceServiceProvider;
            _appointmentDeliveryService = appointmentDeliveryService;
            _donationTypeId = int.Parse(_applicationContext.GetAppSetting("DonationTypeId"));
            _canalPlayNodeId = int.Parse(_applicationContext.GetAppSetting("CanalPlayNodeId"));
        }

        public OrderThankYouViewModel Build(OPContext opContext, PopOrderForm popOrderForm, RangeSet<int> tvArticleTypesIds)
        {
            var contactEmail = popOrderForm.UserContextInformations.ContactEmail;
            var popData = popOrderForm.GetPrivateData<PopData>(false);
            var trackingData = popOrderForm.GetPrivateData<TrackingData>();
            var thankYouOrderDetailData = popOrderForm.GetPrivateData<ThankYouOrderDetailData>();
            var adherentdata = popOrderForm.GetPrivateData<AdherentData>();
            var siteContext = _applicationContext.GetSiteContext();
            var isGuest = popOrderForm.UserContextInformations.IsGuest;

            var marketPlaceService = _marketPlaceServiceProvider.GetMarketPlaceService(opContext.SiteContext);


            var orderThankYouViewModel = new OrderThankYouViewModel(contactEmail, popOrderForm.OrderGlobalInformations.MainOrderUserReference)
            {
                Advertising = new AdvertisingViewModel()
            };


            if (_switchProvider.IsEnabled("front.adherents.CupEnabled") && adherentdata != null)
            {
                if (_userExtendedContext.Visitor.Customer.IsAdhesionCardOwner)
                {
                    var isAdherentCupAccount = _userExtendedContext.Visitor.Customer.IsAdherentCupAccount;
                    orderThankYouViewModel.WithCup();
                    orderThankYouViewModel.Cup = new CupViewModel
                    {
                        FirstName = adherentdata.Name,
                        HasCupAccount = isAdherentCupAccount,
                        HasFinarefAccount = _userExtendedContext.Visitor.Customer.IsAdherentFinarefAccount,
                        IsValidAdherent = _userExtendedContext.Visitor.Customer.IsValidAdherent
                    };
                }
            }

            var trackingOrder = trackingData?.Value;

            if (trackingOrder != null)
            {
                orderThankYouViewModel.TotalPrice = trackingOrder.TotalPrice;
                orderThankYouViewModel.Advertising.ContactEmail = contactEmail;
                orderThankYouViewModel.Advertising.TotalPrice = Math.Round(trackingOrder.TotalPrice, 2, MidpointRounding.AwayFromZero);
                orderThankYouViewModel.Advertising.DisplayZeGive = !siteContext.Market.Equals("BE", StringComparison.CurrentCultureIgnoreCase);
            }

            var displayIndex = popOrderForm.LineGroups.Count(l => !l.HasMarketPlaceArticle) <= 1;
            var displaySeller = popOrderForm.LineGroups.Any(l => l.HasMarketPlaceArticle);

            if ((popOrderForm as IGotBillingInformations).OrderGlobalInformations.OgoneDirectLinkUnfinished)
            {
                orderThankYouViewModel.IsUncertainOrder = true;
            }

            var pad = popOrderForm.GetPrivateData<PushAdherentData>("PushAdherentGroup");

            if (pad.RealDifferedAmount > 0)
            {
                orderThankYouViewModel.WithOtyDifferedAmount();
                orderThankYouViewModel.DifferedAmount = _priceHelper.Format(pad.RealDifferedAmount, PriceFormat.CurrencyAsDecimalSeparator);
            }
            if (pad.categoryCardType == CategoryCardType.Recruit)
            {
                orderThankYouViewModel.DisplayMembershipMessage = true;
            }

            var articles = popOrderForm.LineGroups.SelectMany(l => l.Articles).ToList();

            foreach (var article in articles)
            { 
                if (article is StandardArticle standardArticle)
                {
                    _articleDetailService.GetArticleDetail(standardArticle, siteContext);
                }
                else if(article is MarketPlaceArticle marketPlaceArticle)
                {
                    marketPlaceService.GetArticleDetail(marketPlaceArticle);
                }
            }

            foreach (var thankYouOrderDetail in thankYouOrderDetailData.ThankYouOrdersDetails.OrderByDescending(o => o.IsFnac))
            {
                var logisticLineGroup = popOrderForm.LogisticLineGroups.FirstOrDefault(l => l.OrderReference == thankYouOrderDetail.OrderReference);

                if (logisticLineGroup == null)
                {
                    continue;
                }

                var orderReferenceViewModel = new OrderReferenceViewModel
                {
                    Reference = thankYouOrderDetail.OrderUserReference,
                    DisplayIndex = displayIndex,
                    DisplaySeller = displaySeller,
                    IsFnac = thankYouOrderDetail.IsFnac,
                    Seller = thankYouOrderDetail.Seller
                };
                var fnacLineGroup = popOrderForm.ShippingMethodEvaluation.FnacCom.Value;

                if (fnacLineGroup != null &&
                    fnacLineGroup.SelectedShippingMethodEvaluationType == Base.BaseModel.ShippingMethodEvaluationType.Postal &&
                    fnacLineGroup.PostalShippingMethodEvaluationItem.HasValue &&
                    fnacLineGroup.PostalShippingMethodEvaluationItem.Value.SelectedShippingMethodId.HasValue &&
                    _appointmentDeliveryService.AggregatedShippingMethodIds.Contains(fnacLineGroup.PostalShippingMethodEvaluationItem.Value.SelectedShippingMethodId.Value))
                {                    
                    var deliverySlotData = popOrderForm.GetPrivateData<DeliverySlotData>(false);

                    if (deliverySlotData != null && deliverySlotData.HasSelectedSlot)
                    {
                        var starDate = deliverySlotData.DeliverySlotSelected.StartDate.ToUniversalTime();
                        var endDate = deliverySlotData.DeliverySlotSelected.EndDate.ToUniversalTime();

                        var isSameDeliveryDateForAllArticles = IsSameDeliveryDateForAllArticles(thankYouOrderDetail, deliverySlotData);

                        orderReferenceViewModel.DeliverySlotViewModel = new DeliverySlotViewModel()
                        {
                            SlotStartDate = starDate.ToString("s") + "Z",
                            SlotEndDate = endDate.ToString("s") + "Z",
                            IsSameDeliveryDateForAllArticles = isSameDeliveryDateForAllArticles
                        };
                    }                    
                }

                orderThankYouViewModel.WithReference(orderReferenceViewModel);

                //Add External Reference 
                orderThankYouViewModel.OrderExternalReference = popOrderForm.OrderGlobalInformations.OrderExternalReference;

                var model = new OrderDetailViewModel();

                var isFnac = thankYouOrderDetail.IsFnac;
                model.Seller = thankYouOrderDetail.Seller;

                model.DisplaySeller = popOrderForm.LineGroups.Any(l => l.HasMarketPlaceArticle);
                model.IsFnac = isFnac;
                model.References = new List<string>
                {
                    thankYouOrderDetail.OrderUserReference
                };
                var address = thankYouOrderDetail.ShippingAddress;
                if (address is OrderDetailPostalAddress postalAddress)
                {
                    model.ShippingMode = OrderDetailShippingModeEnum.Postal;
                    model.Address = new OrderDetailAddress()
                    {
                        Firstname = postalAddress.FirstName,
                        Lastname = postalAddress.LastName,
                        Address = postalAddress.Address,
                        ZipCode = postalAddress.ZipCode,
                        Country = postalAddress.Country,
                        City = postalAddress.City,
                        State = postalAddress.State,
                        Company = postalAddress.Company,
                    };
                }
                else if (address is OrderDetailRelayAddress relayAddress)
                {
                    model.ShippingMode = OrderDetailShippingModeEnum.Relay;
                    model.Address = new OrderDetailAddress()
                    {
                        Name = relayAddress.Name,
                        Address = relayAddress.Address,
                        ZipCode = relayAddress.ZipCode,
                        Country = relayAddress.Country,
                        City = relayAddress.City,
                        State = relayAddress.State,
                    };
                }
                else if (address is OrderDetailShopAddress shopAddress)
                {
                    model.ShippingMode = OrderDetailShippingModeEnum.Shop;
                    model.Address = new OrderDetailAddress
                    {
                        Name = shopAddress.Name,
                        Address = shopAddress.Address,
                        ZipCode = shopAddress.ZipCode,
                        Country = shopAddress.Country,
                        City = shopAddress.City,
                    };
                }

                foreach (var article in thankYouOrderDetail.Articles)
                {
                    var originalArticle = popOrderForm.LineGroups
                                                      .SelectMany(l => l.Articles)
                                                      .FirstOrDefault(a => a.ProductID.GetValueOrDefault() == article.Prid);

                    if (originalArticle == null)
                    {
                        originalArticle = popOrderForm.LineGroups
                                                .SelectMany(l => l.Articles)
                                                .OfType<StandardArticle>()
                                                .SelectMany(a => a.Services)
                                                .FirstOrDefault(a => a.ProductID.GetValueOrDefault() == article.Prid);
                    }

                    var articleViewModel = model.WithArticle(article, originalArticle, logisticLineGroup.IsClickAndCollect);

                    if (logisticLineGroup.IsSteedFromStore)
                    {
                        articleViewModel.OrderDetailArticleShippingMode = OrderDetailArticleShippingModeEnum.SteedFromStore;
                    }

                    //Donation's part
                    if (originalArticle.TypeId == _donationTypeId)
                    {
                        //Remplissage du modèle
                        var donationModel = new DonationViewModel()
                        {
                            Amount = originalArticle.Quantity.GetValueOrDefault(), //The donation cost 1€
                            PictureUrl = article.UrlPicture,
                            Name = article.Title
                        };
                        orderThankYouViewModel.WithDonation(donationModel);
                    }
                }

                orderThankYouViewModel.WithDetail(model, isFnac);
            }

            orderThankYouViewModel.DisplayDeliveryDates = !(popData?.HasExcludedAvailabilitiesForDeliveryDates).GetValueOrDefault();

            var standardArticles = popOrderForm.LineGroups.SelectMany(l => l.Articles).OfType<StandardArticle>().ToList();

            if (standardArticles.Any(a => a.IsDematSoft))
            {
                orderThankYouViewModel.WithDematSoft();
            }

            if (standardArticles.Any(a => a.IsEbook))
            {
                orderThankYouViewModel.WithEbook();
            }

            var appointmentTypeIds = _appointmentService.GetAppointmentArticleTypeIds();

            if (standardArticles.Any(a => a.TypeId.HasValue && appointmentTypeIds.Contains(a.TypeId.Value)))
            {
                orderThankYouViewModel.WithFormAtHome();
            }

            var segments = _segmentationService.GetSegmentsByCustomer(_userContext.GetUserId());

            if (segments == null || !segments.Any(s => s.Code.Equals(SegmentCustomerOkCPlay)))
            {
                foreach (var standardArticle in standardArticles)
                {
                    if (standardArticle.ID.HasValue)
                    {
                        var list = TreeServices.Leaf.GetLeavesToObject(LeafType.Article, standardArticle.ID.Value, standardArticle.Referentiel);
                        standardArticle.TreeNodes = list.Select(leaf => leaf.TreeNode.Target).ToList();
                        if (standardArticle.TreeNodes.Any(t => t.Id == _canalPlayNodeId))
                        {
                            orderThankYouViewModel.WithCanalPlay();
                        }
                    }
                }
            }

            var shipToStoreInfoDictionary = new Dictionary<int, ShipToStoreInformation>();

            foreach (var orderDetail in thankYouOrderDetailData.ThankYouOrdersDetails)
            {
                if (!(orderDetail.ShippingAddress is OrderDetailShopAddress))
                {
                    continue;
                }
                var storeId = int.Parse(orderDetail.ShippingAddress.Id);

                if (!shipToStoreInfoDictionary.TryGetValue(storeId, out var info))
                {
                    info = new ShipToStoreInformation();
                    shipToStoreInfoDictionary.Add(storeId, info);

                    var fnacStore = _storeSearchService.GetStore(storeId);
                    var shopDetails = _shopShippingNetworkViewModelBuilder.BuildDetails(fnacStore, 0, null);

                    info.Name = shopDetails.Name;
                    info.Timetables = shopDetails.Timetables;
                    info.ExceptionnalTimetables = shopDetails.ExceptionnalTimetables;

                    if (fnacStore.SpaceId.HasValue)
                    {
                        info.UrlStorePage = _fnacStoreService.GetLayoutUrlBySpaceId(fnacStore.SpaceId.Value);
                    }
                }

                info.IsClickAndCollect = info.IsClickAndCollect || orderDetail.IsClickAndCollect;
                info.IsShipToStore = info.IsShipToStore || orderDetail.IsShipToStore;
            }

            orderThankYouViewModel.WithShipToStoreInformation(shipToStoreInfoDictionary.Values);

            if (popOrderForm.OrderBillingMethods.OfType<CreditCardBillingMethod>().Any(c => c.NumByPhone.HasValue && c.NumByPhone.Value))
            {
                orderThankYouViewModel.WithPaybyPhone();
            }

            if (popOrderForm.OrderBillingMethods.OfType<PayByRefBillingMethod>().Any())
            {
                var payByRefBillingMethod = popOrderForm.OrderBillingMethods.OfType<PayByRefBillingMethod>().First();

                var merchantId = popOrderForm.OrderGlobalInformations.MerchantId;
                var reference = payByRefBillingMethod.Reference;
                var amountEur = _priceHelper.Format(payByRefBillingMethod.Amount.GetValueOrDefault(0));

                orderThankYouViewModel.WithPayByRef(merchantId, reference, amountEur);
            }

            if (popOrderForm.OrderBillingMethods.OfType<MBWayBillingMethod>().Any())
            {
                var mBWayBillingMethod = popOrderForm.OrderBillingMethods.OfType<MBWayBillingMethod>().First();

                var phoneNumber = mBWayBillingMethod.PhoneNumber;

                orderThankYouViewModel.WithMBWay(phoneNumber);
            }

            if (popOrderForm.OrderBillingMethods.OfType<OgoneCreditCardBillingMethod>().Any())
            {
                var billingMethod = popOrderForm.OrderBillingMethods.OfType<OgoneCreditCardBillingMethod>().First();
                var billingTypeId = (CreditCardTypeEnum)billingMethod.TypeId.Value;

                var pspData = popOrderForm.GetPrivateData<PspData>(true);
                var orderTransactionInfos = pspData.OrderTransactionInfos;
                var orderTransaction = orderTransactionInfos.GetByBillingId(OgoneCreditCardBillingMethod.IdBillingMethod).OrderTransaction;

                if (billingTypeId == CreditCardTypeEnum.VisaFnac && orderTransaction.OrderTransactionOgone.Status == OgoneStatusCode.AuthorizationWaiting)
                    orderThankYouViewModel.HasCaixaUncertain = true;
            }

            if (popOrderForm.OrderBillingMethods.OfType<FibBillingMethod>().Any())
            {
                var fibBillingMethod = popOrderForm.OrderBillingMethods.OfType<FibBillingMethod>().First();

                var priceFormat = _priceHelper.Format(fibBillingMethod.Amount.GetValueOrDefault(0));

                orderThankYouViewModel.WithFib(priceFormat, popOrderForm.OrderGlobalInformations.MainOrderUserReference, popOrderForm.LogisticLineGroups.Select(l => l.OrderUserReference));
            }

            if (popOrderForm.OrderBillingMethods.OfType<BankTransferBillingMethod>().Any())
            {
                var bankTransferVm = new BankTransfertMethodViewModel
                {
                    TransfertAccount = opContext.Pipe.GlobalParameters["billing.banktransfer.account"]
                };
                orderThankYouViewModel.WithBankTransfer(bankTransferVm);
            }
            if (popOrderForm.OrderBillingMethods.OfType<CheckBillingMethod>().Any())
            {
                orderThankYouViewModel.WithCheckPayment();
            }
            if (popOrderForm.OrderBillingMethods.OfType<DefferedPaymentBillingMethod>().Any())
            {
                orderThankYouViewModel.WithDeferredPayment();
            }
            if (popOrderForm.OrderBillingMethods.OfType<OrangeBillingMethod>().Any())
            {
                orderThankYouViewModel.WithKoboOrange();
            }
            if (popOrderForm.LogisticLineGroups.Any(l => l.IsSteedFromStore))
            {
                var logisticLineGroup = popOrderForm.LogisticLineGroups.FirstOrDefault(l => l.IsSteedFromStore);

                if (logisticLineGroup != null)
                {
                    var orderDetail = thankYouOrderDetailData.ThankYouOrdersDetails.FirstOrDefault(d => d.OrderReference == logisticLineGroup.OrderReference);

                    var address = orderDetail?.ShippingAddress;
                    if (address is OrderDetailPostalAddress postalAddress)
                    {
                        orderThankYouViewModel.WithSteedFromStore("", postalAddress.Phone);
                    }
                }
            }

            //TvRoyalty

            var articlesWithoutCollectInStore = popOrderForm.LineGroups.Where(l => !l.HasCollectInStoreArticles)
                                                .SelectMany(l => l.Articles);

            var tvRoyaltyRequired = _switchProvider.IsEnabled("orderpipe.pop.tvroyalty")
                && articlesWithoutCollectInStore.Any(art => _tvRoyaltyBusiness.DoesArticleNeedTVRoyaltyOTY(art, tvArticleTypesIds));

            if (tvRoyaltyRequired)
            {
                orderThankYouViewModel.WithTvRoyalty();
                orderThankYouViewModel.TvRoyaltyFormViewModel = _tvRoyaltyViewModelBuilder.Build(popOrderForm);
            }

            if (popOrderForm.UserContextInformations.ThankYouMessageFlag != 0)
            {
                orderThankYouViewModel.WithErrorScoring();
            }

            if (popOrderForm.IsOnlyNumerical())
            {
                orderThankYouViewModel.IsBillingOnly();
            }
            else if (!popOrderForm.HasMarketPlaceArticle())
            {
                var nonBillingOnlyLineGroups = (from lineGroup in popOrderForm.LineGroups
                                                where !lineGroup.Articles.OfType<BaseArticle>().Where(a => a.LogType.HasValue).All(a => popOrderForm.ShippingMethodEvaluation.ExcludedLogisticTypes.Contains(a.LogType.Value))
                                                where !lineGroup.HasDematSoftArticles
                                                where !lineGroup.HasNumericalArticles
                                                select lineGroup).ToList();

                if (!nonBillingOnlyLineGroups.Any())
                {
                    orderThankYouViewModel.IsBillingOnly();
                }
            }

            orderThankYouViewModel.HasAdvertising = ComputeHasAdvertising(opContext);

            //OPC Deezer
            if (_switchProvider.IsEnabled("orderpipe.pop.promo.activatedeezer"))
            {
                foreach (var orderDetail in thankYouOrderDetailData.ThankYouOrdersDetails)
                {
                    foreach (var article in orderDetail.Articles)
                    {
                        if (article.Promotions?.Count > 0)
                        {
                            var promotion = article.Promotions.FirstOrDefault(p => p.DisplayTemplate == FnacDirect.Contracts.Online.Model.PromotionTemplate.RefundOfferPartner && p.ShoppingCartText != null && p.ShoppingCartText.Contains(ShoppingCardDeezer));
                            if (promotion != null)
                            {
                                var pushFnacPlusAdherentData = popOrderForm.GetPrivateData<PushFnacPlusAdherentData>(false);

                                if (pushFnacPlusAdherentData != null
                                    && (pushFnacPlusAdherentData.HasFnacPlusCardInBasket || pushFnacPlusAdherentData.HasFnacPlusTryCardInBasket)
                                    )
                                {
                                    orderThankYouViewModel.WithDeezer(true);
                                }
                                else
                                {
                                    orderThankYouViewModel.WithDeezer(false);
                                }
                            }

                        }
                    }
                }
            }

            orderThankYouViewModel.HasIdentityClash = thankYouOrderDetailData.IsIdentityClash;

            orderThankYouViewModel.NeedPassword = false;
            if (!string.IsNullOrEmpty(contactEmail))
            {
                var accountInfo = _accountIdentityProviderAssociationService.GetExternalAccountsAssociatedToEmail(contactEmail);
                orderThankYouViewModel.NeedPassword = accountInfo.LoginType == Customer.Model.LoginType.SocialConnect
                                                    && standardArticles.Any(a => a.IsEbook);
            }

            var guestInfosViewModel = new GuestInfosViewModel { IsGuest = false };
            var guestData = popOrderForm.GetPrivateData<GuestData>(false);

            if (isGuest && guestData != null)
            {
                guestInfosViewModel.IsGuest = true;
                guestInfosViewModel.GuestOrderTrackingUrl = guestData.GuestOrderTrackingUrl;
            }

            orderThankYouViewModel.GuestInfosViewModel = guestInfosViewModel;

            return orderThankYouViewModel;
        }

        private bool ComputeHasAdvertising(OPContext opContext)
        {
            var popOrderForm = opContext.OF as PopOrderForm;
            var popData = popOrderForm.GetPrivateData<PopData>(false);

            return popOrderForm.OrderGlobalInformations.OrderInserted && IsAdvertisingActivated((popData?.AdvertisingHasBenDisplayed).GetValueOrDefault());
        }

        private bool IsAdvertisingActivated(bool advertisingHasBenDisplayed)
        {
            return _switchProvider.IsEnabled("orderpipe.pop.display.advertising.on.payment") &&
                   !advertisingHasBenDisplayed;
        }

        private bool IsSameDeliveryDateForAllArticles(ThankYouOrderDetail model, DeliverySlotData deliverySlot)
        {
            var startDate = deliverySlot.DeliverySlotSelected.StartDate.ToUniversalTime();
            var endDate = deliverySlot.DeliverySlotSelected.EndDate.ToUniversalTime();

            return model.Articles
                .Where(a => !string.IsNullOrEmpty(a.TheoreticalDeliveryDate))
                .All(a => (a.TheoreticalDeliveryStartDate == startDate)
                    && (a.TheoreticalDeliveryEndDate == endDate));
        }
    }
}
