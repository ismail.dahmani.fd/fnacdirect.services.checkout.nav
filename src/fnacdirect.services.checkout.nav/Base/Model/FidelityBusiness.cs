using FnacDirect.Membership.Model.Entities;
using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.VirtualGiftCheck.BLL;
using FnacDirect.Contracts.Online.Model;
using FnacDirect.Membership.Business;
using FnacDirect.Membership.Model.Constants;
using FnacDirect.Technical.Framework.Web;
using FnacDirect.Technical.Framework.ServiceLocation;
using FnacDirect.OrderPipe.Base.Model.Facets;
using System.Linq;
using System;

namespace FnacDirect.OrderPipe.Base.Business
{
    public class FidelityBusiness<TFidelityBillingMethod> where TFidelityBillingMethod : FidelityBillingMethod, new()
    {
        TFidelityBillingMethod _TFidelityBillingMethod = new TFidelityBillingMethod();

        private readonly ICommonMemberShipBusiness _commonMemberShipBusiness;

        public FidelityBusiness()
        {
            _commonMemberShipBusiness = ServiceLocator.Current.GetInstance<ICommonMemberShipBusiness>();
        }

        public FidelityBusiness(AdvantageCodeType advantageCodeType)
        {
            _commonMemberShipBusiness = ServiceLocator.Current.GetInstance<ICommonMemberShipBusiness>();
            _TFidelityBillingMethod.AdvantageCodeType = advantageCodeType;
        }

        public bool Add(IGotBillingInformations orderForm, Contract contract, decimal amount)
        {
            var result = _commonMemberShipBusiness.PreviewBurn(contract, _TFidelityBillingMethod.AdvantageCodeType, amount);
            if (result.AmountBurned == 0)
            {
                orderForm.UserMessages.Add("pf", UserMessage.MessageLevel.Error, "OP.PF.ErrMsg.notEnoughPoints");
                return false;
            }

            _TFidelityBillingMethod.Amount = result.Value;
            _TFidelityBillingMethod.PointsQuantity = (long)result.AmountBurned;
            _TFidelityBillingMethod.BirthDate = contract.BirthDate;
            _TFidelityBillingMethod.ContractId = contract.Identifier;
            
            if (orderForm.OrderBillingMethods.FirstOrDefault(delegate(BillingMethod bm){ return bm as TFidelityBillingMethod != null;}) != null)
            {
                return false;
            }            

            _TFidelityBillingMethod.Valid = true;

            orderForm.AddBillingMethod(_TFidelityBillingMethod);

            return true;
        }

        public bool Remove(IGotBillingInformations orderForm)
        {
            var toRemove = orderForm.OrderBillingMethods.OfType<TFidelityBillingMethod>().ToList();

            foreach(var billingMethod in toRemove)
                {
                orderForm.RemoveBillingMethod(billingMethod.BillingMethodId);
        }

            return toRemove.Any();
        }

        public bool Modify(IGotBillingInformations orderForm, Contract contract, decimal amount)
        {
            Remove(orderForm);
            return Add(orderForm, contract, amount);
        }

        public bool Transform(IGotUserContextInformations iGotUserContextInformations, IGotContextInformations iGotContextInformations, TFidelityBillingMethod m, Contract contract, ref VirtualGiftCheckBillingMethod vgcbm)
        {   
            var userContextInformations = iGotUserContextInformations.UserContextInformations;

            var ccvacc = new Account(userContextInformations.CustomerEntity);

            if (ccvacc.IsBlackListed)
            {
                iGotContextInformations.AddErrorMessage("pf", "OP.PF.ErrMsg.invalid");
                return false;
            }

            var ret = _commonMemberShipBusiness.Burn(contract, _TFidelityBillingMethod.AdvantageCodeType, m.Amount.Value, userContextInformations.CustomerEntity, iGotContextInformations.OrderContextInformations.Culture);
            if (ret.Status == FnacPointsBurnReturnCode.InsufficientPoints)
            {
                ccvacc.RegisterFailure();
                iGotContextInformations.AddErrorMessage("pf", "OP.PF.ErrMsg.notEnoughPoints");
                return false;
            }

            if (ret.Status == FnacPointsBurnReturnCode.ErrorWS)
            {
                iGotContextInformations.AddErrorMessage("pf", "OP.PF.ErrMsg.Technical");
                return false;
            }
           
            if (ret.Status == FnacPointsBurnReturnCode.BurnOk)
            {
                if (vgcbm == null)
                {
                    vgcbm = new VirtualGiftCheckBillingMethod();
                }

                if (!vgcbm.Amount.HasValue)
                    vgcbm.Amount = 0.0m;

                if (!vgcbm.AmountEur.HasValue)
                    vgcbm.AmountEur = 0.0m;

                vgcbm.Amount += m.Amount.Value;
                vgcbm.AmountEur += m.Amount.Value;
                vgcbm.UserGUID = userContextInformations.UID;
                vgcbm.IsCreatedByFidelityEuros = true;
            }

            return true;
        }

        public Contract GetCurrentContract(IW3Customer currentCustomer)
        {
            if (currentCustomer == null)
            {
                return null;
            }
            // TODO: revoir cela
            // Suite au merge avec RebornBE, il semble qu'on ait perdu des bouts
            // sur la gestion des comptes -1: fix rapide pour la non régression
            if (currentCustomer.DTO == null)
            {
                return null;
            }
            if (_TFidelityBillingMethod.AdvantageCodeType == AdvantageCodeType.SIEBEL_CUMUL_GAMING || _TFidelityBillingMethod.AdvantageCodeType == AdvantageCodeType.SIEBEL_AVOIRS_GAMING)
            {
                return currentCustomer.OccazContract;
            }
            return currentCustomer.MembershipContract;
        }

        public BenefitCounter GetBenefitCounter(IW3Customer currentCustomer)
        {
            if (currentCustomer == null)
            {
                return null;
            }
            var currectContract = GetCurrentContract(currentCustomer);
            if (currectContract == null)
            {
                return null;
            }
            return _commonMemberShipBusiness.GetBenefitCounterByAdvantageCodeType(currectContract, _TFidelityBillingMethod.AdvantageCodeType);
        }

        public BenefitExpiration GetFirstExpiry(IW3Customer currentCustomer)
        {
            if (currentCustomer == null)
            {
                return null;
            }
            var benefitCounter = GetBenefitCounter(currentCustomer);
            if (benefitCounter == null)
            {
                return null;
            }
            return BenefitCounterBusiness.Instance.GetFirstFidelityBenefitExpiration(benefitCounter);
        }

        public decimal ExpirationPoint(BenefitExpiration benefitExpiration)
        {
            return _commonMemberShipBusiness.GetExpirationPoint(benefitExpiration);
        }

        public decimal GetAvailableAmount(IW3Customer currentCustomer)
        {
            if (currentCustomer == null)
            {
                return 0;
            }
            var benefitCounter = GetBenefitCounter(currentCustomer);
            if (benefitCounter == null)
            {
                return 0;
            }
            switch (_TFidelityBillingMethod.AdvantageCodeType)
            {
                case AdvantageCodeType.SIEBEL_AVOIRS_GAMING:
                case AdvantageCodeType.SIEBEL_CUMUL_GAMING:
                case AdvantageCodeType.SIEBEL_PF:
                    return _commonMemberShipBusiness.GetAvailableChangeAmount(benefitCounter);                    
                case AdvantageCodeType.SIEBEL_EUROS:
                    return _commonMemberShipBusiness.GetAvailableQuantity(benefitCounter);                    
                default:
                    return 0;
            }
        }

        public decimal GetUsableAmount(IW3Customer currentCustomer, decimal multiple)
        {
            if (currentCustomer == null)
            {
                return 0;
            }
            var benefitCounter = GetBenefitCounter(currentCustomer);
            if (benefitCounter == null)
            {
                return 0;
            }
            switch (_TFidelityBillingMethod.AdvantageCodeType)
            {
                case AdvantageCodeType.SIEBEL_EUROS:
                    if(multiple >= 1)
                        return _commonMemberShipBusiness.GetAvailableChangeAmount(benefitCounter, Convert.ToInt32(multiple));
                    else
                        return _commonMemberShipBusiness.GetAvailableChangeAmount(benefitCounter);
                default:
                    return 0;
            }
        }

        public decimal GetMaxUsableAmount(IW3Customer currentCustomer, IGotBillingInformations orderForm, decimal multiple)
        {
            if (currentCustomer == null || orderForm == null)
            {
                return 0;
            }
            var availableAmount = GetAvailableAmount(currentCustomer);
            var usableAmount = GetUsableAmount(currentCustomer, multiple);
            var currentBillingMethod = GetCurrentBillingMethod(orderForm);
            decimal maxAmount = 0;
            switch (_TFidelityBillingMethod.AdvantageCodeType)
            {
                case AdvantageCodeType.SIEBEL_AVOIRS_GAMING:
                    maxAmount = orderForm.PrivateData.GetByType<PaymentSelectionData>().RemainingAmount
                                    + (currentBillingMethod != null ? currentBillingMethod.Amount.Value : 0);
                    return maxAmount > availableAmount ? availableAmount : maxAmount;
                case AdvantageCodeType.SIEBEL_CUMUL_GAMING:
                case AdvantageCodeType.SIEBEL_PF:
                    maxAmount = orderForm.PrivateData.GetByType<PaymentSelectionData>().RemainingAmount
                                    + (currentBillingMethod != null ? currentBillingMethod.Amount.Value : 0);
                    maxAmount = maxAmount - maxAmount % multiple;
                    return maxAmount > availableAmount ? availableAmount : maxAmount;
                case AdvantageCodeType.SIEBEL_EUROS:
                    maxAmount = orderForm.PrivateData.GetByType<PaymentSelectionData>().RemainingAmount
                                    + (currentBillingMethod != null ? currentBillingMethod.Amount.Value : 0);
                    maxAmount = maxAmount - maxAmount % multiple;
                    return maxAmount > usableAmount ? usableAmount : maxAmount;
                default:
                    return 0;
            }
        }

        public BillingMethod GetCurrentBillingMethod(IGotBillingInformations orderForm)
        {
            if (orderForm == null)
            {
                return null;
            }

            return orderForm.OrderBillingMethods.FirstOrDefault(bm => bm as TFidelityBillingMethod != null);
        }
    }
}
