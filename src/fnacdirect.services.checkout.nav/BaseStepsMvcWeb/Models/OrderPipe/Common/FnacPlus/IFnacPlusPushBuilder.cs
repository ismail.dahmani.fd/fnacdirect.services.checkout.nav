﻿using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Common.Basket;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Common.Shipping
{
    public interface IFnacPlusPushBuilder
    {
        FnacPlusViewModel Build(PopOrderForm popOrderForm);
    }
}