using FnacDirect.Contracts.Online.Model;
using FnacDirect.InstallmentPayment.Models;
using FnacDirect.InstallmentPayment.Models.Enums;
using FnacDirect.OrderPipe.BaseMvc.Web.Validations;
using FnacDirect.Technical.Framework.Web.Mvc.DataAnnotations;
using System;
using System.Collections.Generic;
using System.Linq;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Common.Payment
{
    public class CreditCardBillingMethodsViewModel : BillingMethodViewModel
    {
        public bool RegisterCreditCardPreChecked { get; set; }

        private List<CreditCardViewModel> _creditCards = new List<CreditCardViewModel>();

        private List<CustomerCreditCardViewModel> _customerCreditCards = new List<CustomerCreditCardViewModel>();

        public IReadOnlyCollection<CreditCardViewModel> CreditCards
        {
            get
            {
                return _creditCards.AsReadOnly();
            }
        }

        public IReadOnlyCollection<CustomerCreditCardViewModel> CustomerCreditCards
        {
            get
            {
                return _customerCreditCards.AsReadOnly();
            }
        }

        public void AddCustomerCreditCards(IEnumerable<CustomerCreditCardViewModel> creditCards)
        {
            _customerCreditCards.AddRange(creditCards);
        }

        public void AddCreditCard(CreditCardViewModel creditCard)
        {
            _creditCards.Add(creditCard);
        }

        public void RemoveCreditCard(CreditCardViewModel creditCard)
        {
            _creditCards.Remove(creditCard);
        }

        public void SortCreditCards()
        {
            _creditCards = _creditCards.OrderBy(x => x.Order).ToList();
        }
    }

    #region Model du formulaire SIPS
    public class SipsBillingMethodViewModel : CreditCardBillingMethodsViewModel
    {
        private readonly IList<CreditViewModel> _credits
            = new List<CreditViewModel>();

        public IList<CreditViewModel> Credits
        {
            get { return _credits; }
        }

        public SipsBillingMethodViewModel()
        {
            SipsViewModel = new SipsViewModel();
        }

        public SipsViewModel SipsViewModel { get; set; }
    }

    public class CreditViewModel
    {
        public string Id { get; set; }
        public string Label { get; set; }
        public string CreditType { get; set; }
    }

    public class SipsViewModel
    {
        public int SelectedCreditCardId { get; set; }

        [CreditCardHolderInformationsValidation]
        public string Holder { get; set; }

        [CreditCardNumberValidation("SelectedCreditCardId")]
        public string CardNo { get; set; }

        public int CardNoLength { get; set; }

        public string ValidityMonth { get; set; }

        [CreditCardValidityDateValidation("ValidityMonth", "SelectedCreditCardId", "NumByPhone")]
        public string ValidityYear { get; set; }

        [CreditCardCryptoValidation("SelectedCreditCardId")]
        public string Cvv { get; set; }

        public string CreditId { get; set; }

        public bool NumByPhone { get; set; }
    }
    #endregion

    #region Model du formulaire OGONE
    public class OgoneBillingMethodViewModel : CreditCardBillingMethodsViewModel
    {
        public OgoneBillingMethodViewModel()
        {
            OgoneViewModel = new OgoneViewModel();
        }

        public OgoneViewModel OgoneViewModel { get; set; }

        public bool DisplayTacitAgreement { get; set; }

        public bool IsClicInStore { get; set; }

        public decimal RealAmount { get; set; }
        public decimal DebitAmount { get; set; }
        public bool IsECardAutorised { get; set; }

        public bool DisableCustomerCards { get; set; }
        public bool DisplaySaveCreditCard { get; set; } = true;
    }

    public class InstallmentPaymentOfferViewModel : Offer
    {
        public int InstallmentCount { get; private set; }

        public InstallmentPaymentOfferViewModel(Offer offer) : base()
        {
            CreditCardTypeId = offer.CreditCardTypeId;
            OfferName = offer.OfferName;
            Installments = offer.Installments.ToList();
            Cost = offer.Cost;

            InstallmentCount = Installments.Count();
        }
    } 


    public class InstallmentPaymentBillingMethodViewModel : BillingMethodViewModel
    {
        public InstallmentPaymentBillingMethodViewModel(PartnerOffer partnerOffer, ISwitchProvider switchProvider)
        {
            Offers = partnerOffer.Offers.Select(o => new InstallmentPaymentOfferViewModel(o)).ToList();
            PartnerName = partnerOffer.PartnerName;
            Display = Offers.Any();

            if (IsOney()
                && switchProvider.IsEnabled("orderpipe.pop.payment.installmentpayments.oney.displayoneline"))
            {
                FilterOfferInstallmentToOneElement();
            }
        }

        private void FilterOfferInstallmentToOneElement()
        {
            foreach(var offer in Offers)
            {
                if (!offer.Installments.Any())
                {
                    continue;
                }

                offer.Installments = offer.Installments.Take(1);
            }
        }

        private bool IsOney() => InstallmentPartnerEnum.Oney.ToString().Equals(PartnerName, StringComparison.InvariantCultureIgnoreCase);

        public IEnumerable<InstallmentPaymentOfferViewModel> Offers { get; set; }

        public string PartnerName { get; set; }

        public int MaxInstallmentCount
        {
            get
            {
                return !Offers.Any() ? 0 : Offers.Max(x => x.Installments == null ? 0 : x.Installments.Count());
            }
        }
    }


    public class UnicreBillingMethodViewModel : CreditCardBillingMethodsViewModel
    {       
        public int SelectedCreditCardId { get; set; }

        public bool DisplayTacitAgreement { get; set; }       
    }

    public abstract class BaseOgoneViewModel
    {
        public int SelectedCreditCardId { get; set; }
    }

    public class OgoneViewModel : BaseOgoneViewModel
    {
        public OgoneViewModel()
        {
            ogoneAliasGatewayNewCardViewModel = new OgoneAliasGatewayViewModel();
        }

        public string SelectedCreditCardReference { get; set; }

        [CreditCardCryptoValidation("SelectedCreditCardReference")]
        public string SelectedCreditCardCVC { get; set; }

        public bool RegisterCreditCard { get; set; }

        private OgoneAliasGatewayViewModel ogoneAliasGatewayNewCardViewModel;
        public OgoneAliasGatewayViewModel OgoneAliasGatewayNewCardViewModel
        {
            get { return ogoneAliasGatewayNewCardViewModel; }
        }
    }

    public class CaixaViewModel : BaseOgoneViewModel
    {
        [Required]
        public DateTime BirthDate { get; set; }

        [Required]
        public string NIF { get; set; }

        public bool? CGV { get; set; }

        public bool? Optin { get; set; }
    }

    /// <summary>
    /// Model du formulaire de saisie de la CB
    /// </summary>
    public class OgoneAliasGatewayViewModel
    {
        public bool IsEmpty
        {
            get
            {
                return string.IsNullOrEmpty(Holder)
                    && string.IsNullOrEmpty(CardNumber)
                    && string.IsNullOrEmpty(ValidityMonth)
                    && string.IsNullOrEmpty(ValidityYear)
                    && string.IsNullOrEmpty(CVC);
            }
        }

        #region New Card Infos
        [CreditCardHolderInformationsValidation]
        public string Holder { get; set; }

        [CreditCardNumberValidation("SelectedCreditCardId")]
        public string CardNumber { get; set; }

        public int CardNoLength { get; set; }

        public string ValidityMonth { get; set; }

        [CreditCardValidityDateValidation("ValidityMonth", "SelectedCreditCardId", "")]
        public string ValidityYear { get; set; }

        [CreditCardCryptoValidation("SelectedCreditCardId")]
        public string CVC { get; set; }

        public string CreditId { get; set; }

        public string Alias { get; set; }

        public string ECardPrefixes { get; set; }

        #endregion New Card Infos

        #region Alias Gateway parameters

        public string Url { get; set; }
        public SortedList<string, string> Parameters { get; set; }

        #endregion Alias Gateway parameters
    }
    #endregion

    public class CreditCardCetelemMethodeViewModel : UnicreBillingMethodViewModel
    {
        public List<Credit> CreditCardCetelem { get; set; }

        [Required(ErrorMessage = "orderpipe.pop.payment.cetelem.required")]
        public int SelectedCreditCardTypeId { get; set; }
    }

    public class LimonetikBillingMethodViewModel : BillingMethodViewModel
    {
        public int GiftCardId { get; set; }

        public List<GiftCardTypeViewModel> GiftCardTypes { get; set; }

        public void AddGiftCardTypeViewModel(string label, string value)
        {
            if (GiftCardTypes == null)
            {
                GiftCardTypes = new List<GiftCardTypeViewModel>();
            }

            GiftCardTypes.Add(new GiftCardTypeViewModel(label, value));
        }
    }
}
