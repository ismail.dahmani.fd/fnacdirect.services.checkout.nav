using Newtonsoft.Json;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Omniture
{
    public class ProductTrackingViewModel
    {
        [JsonProperty("prid")]
        public string Prid { get; set; }

        [JsonProperty("seller")]
        public string Seller { get; set; }

        [JsonProperty("productType")]
        public string Type { get; set; }

        [JsonProperty("productStatus")]
        public string State { get; set; }

        [JsonProperty("quantity")]
        public int Quantity { get; set; }

        [JsonProperty("price")]
        public decimal UnitPrice { get; set; }

        [JsonProperty("codeStock")]
        public string Stock { get; set; }

        [JsonProperty("product_shipping_method")]
        public string ProductShippingMethod { get; set; }

        [JsonProperty("withdrawal_shop_name")]
        public string WithdrawalShopName { get; set; }
    }
}
