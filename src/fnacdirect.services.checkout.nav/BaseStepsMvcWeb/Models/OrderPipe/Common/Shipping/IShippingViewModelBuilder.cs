﻿using FnacDirect.OrderPipe.Base.Model;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Common.Shipping
{
    public interface IShippingViewModelBuilder
    {
        ShippingViewModel Build(PopOrderForm popOrderForm, int sellerId);
        bool IsValid(PopOrderForm popOrderForm, int sellerId);
    }
}
