using System;
using FnacDirect.Customer.Model;
using FnacDirect.Contracts.Online.Model;
using System.Collections.Generic;

namespace FnacDirect.OrderPipe.CM.Model
{
    public class CreateAccountData : GroupData
    {
        private string _cellPhone;

        public string Firstname { get; set; }

        public string Lastname { get; set; }

        public string PostalCode { get; set; }

        public string Email { get; set; }

        public string AdhNumber { get; set; }

        public string CardNumber { get; set; }
        public string Offer { get; set; }
        public bool IsValidCard { get; set; }

        public string GeneratedPassword { get; set; }

        public string Address1 { get; set; }

        public string Address2 { get; set; }
        public string Address3 { get; set; }

        public string City { get; set; }

        public DateTime? Birthdate { get; set; }

        public string Gender { get; set; }

        public string UID { get; set; }
        public string Company { get; set; }
        public string CellPhone
        {
            get { return _cellPhone; }
            set { _cellPhone = value; }
        }

        public int? AllowEmail { get; set; }

        public bool AllowSMS { get; set; }

        public string AlertMode { get; set; }

        public string CardStatus { get; set; }

        public string AdhesionStatus { get; set; }
        public string CountryId { get; set; }
        public DateTime? LastUpdated { get; set; }

        /// <summary>
        /// Nettoie l'objet ...
        /// </summary>
        public void clean()
        {
            Address1 = string.Empty;
            Address2 = string.Empty;
            AdhNumber = string.Empty;
            Birthdate = null;
            CardNumber = string.Empty;
            City = string.Empty;
            Email = string.Empty;
            Firstname = string.Empty;
            Gender = string.Empty;
            GeneratedPassword = string.Empty;
            Lastname = string.Empty;
            PostalCode = string.Empty;
            UID = string.Empty;
            _cellPhone = string.Empty;
            AllowEmail = null;
            AllowSMS = false;
            AlertMode = string.Empty;
            CardStatus = string.Empty;
            AdhesionStatus = string.Empty;
            LastUpdated = null;
        }
    }
}
