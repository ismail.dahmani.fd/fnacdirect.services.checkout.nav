using System.Xml.Serialization;

namespace FnacDirect.OrderPipe.Base.Model
{
    public class VirtualGiftCheck : Article
    {
        [XmlIgnore]
        private string _Message;

        public string Message
        {
            get
            {
                return _Message;
            }
            set
            {
                _Message = value;
            }
        }
        [XmlIgnore]
        private string _CheckIds;

        [XmlIgnore]
        public string CheckIds
        {
            get
            {
                return _CheckIds;
            }
            set
            {
                _CheckIds = value;
            }
        }

        [XmlIgnore]
        private string _DestEMail;

        public string DestEMail
        {
            get
            {
                return _DestEMail;
            }
            set
            {
                _DestEMail = value;
            }
        }

        [XmlIgnore]
        private int? _DestGender;
        public int? DestGender
        {
            get { return _DestGender; }
            set { _DestGender = value; }
        }

        [XmlIgnore]
        private string _DestLastName;
        public string DestLastName
        {
            get { return _DestLastName; }
            set { _DestLastName = value; }
        }

        [XmlIgnore]
        private string _DestFirstName;
        public string DestFirstName
        {
            get { return _DestFirstName; }
            set { _DestFirstName = value; }
        }

        [XmlIgnore]
        private string _DestCompanyName;
        public string DestCompanyName
        {
            get { return _DestCompanyName; }
            set { _DestCompanyName = value; }
        }

        [XmlIgnore]
        private string _FromEmail;

        public string FromEmail
        {
            get
            {
                return _FromEmail;
            }
            set
            {
                _FromEmail = value;
            }
        }
        [XmlIgnore]
        private string _FromName;

        public string FromName
        {
            get
            {
                return _FromName;
            }
            set
            {
                _FromName = value;
            }
        }

        [XmlIgnore]
        private int? _Format;

        public int? Format
        {
            get
            {
                return _Format;
            }
            set
            {
                _Format = value;
            }
        }

        [XmlIgnore]
        private decimal? _Amount;

        public decimal? Amount
        {
            get
            {
                return _Amount;
            }
            set
            {
                _Amount = value;
            }
        }
    }
}
