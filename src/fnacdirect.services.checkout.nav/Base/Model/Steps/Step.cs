using System;
using System.Collections.Generic;
using FnacDirect.OrderPipe.Config;
using FnacDirect.OrderPipe.Core.Steps;

namespace FnacDirect.OrderPipe
{
    /// <summary>
    /// Implémentation de base d'une étape
    /// </summary>
    public abstract class Step : BaseStep
    {
        public virtual IEnumerable<Type> DataType { get { return null; } }

        /// <summary>
        /// Méthode appelée par l'OrderPipe pour effectuer les traitements non conditionnels de l'étape
        /// </summary>
        /// <param name="opContext">Contexte d'exécution de l'OrderPipe</param>
        public virtual void PreCalc(OPContext opContext)
        {

        }

        /// <summary>
        /// Méthode appelée par l'OrderPipe permettant de déterminer si l'OrderPipe doit exécuter Process
        /// </summary>
        /// <param name="opContext">Contexte d'exécution de l'OrderPipe</param>
        /// <returns>Une instance de StepReturn, spécifiant le comportement que doit adopter le pipe</returns>
        public virtual StepReturn Check(OPContext opContext)
        {
            return StepReturn.Process;
        }

        /// <summary>
        /// Méthode appelée par l'OrderPipe effectuant le traitement final de l'étape
        /// </summary>
        /// <param name="opContext">Contexte d'exécution de l'OrderPipe</param>
        /// <returns>Une instance de StepReturn indiquant à l'OrderPipe comment continuer son exécution</returns>
        public virtual StepReturn Process(OPContext opContext)
        {
            return StepReturn.Next;
        }

        /// <summary>
        /// Ajoute une contrainte globale
        /// </summary>
        /// <param name="opContext">Contexte d'exécution du Pipe</param>
        /// <param name="constraintName">Nom de la contrainte à placer</param>
        public Constraint AddGlobalConstraint(OPContext opContext, string constraintName)
        {
            return OP.AddGlobalConstraint(opContext.OF, Name, constraintName);
        }

        /// <summary>
        /// Lis une contraite globale
        /// </summary>
        /// <param name="opContext">Contexte d'exécution du Pipe</param>
        /// <param name="constraintName">Nom de la contrainte à récupérer</param>
        /// <returns></returns>
        public Constraint GetGlobalConstraint(OPContext opContext, string constraintName)
        {
            return OP.GetGlobalConstraint(opContext.OF, constraintName);
        }

        /// <summary>
        /// Retourne la liste des redirections définies dans la configuration
        /// </summary>
        public ForwardList Forwards { get; set; }

        protected StepReturnForward GetForward(string name)
        {
            return new StepReturnForward(Forwards[name]);
        }

        protected StepReturnForward GetForwardNoForce(string name)
        {
            return new StepReturnForward(Forwards[name], false);
        }

        /// <summary>
        /// Exécution d'une étape du pipe
        /// </summary>
        /// <param name="context">Contexte d'exécution</param>
        /// <param name="res">Résultat du pipe</param>
        /// <param name="force">Vrai si il faut forcer l'affichage de cette étape</param>
        /// <returns></returns>
        public override bool Run(OPContext context, PipeResult res, bool force)
        {
            using (new Stopwatch("Running " + Name))
            {
                // Phase préalable
                PreCalc(context);
                Stopwatch.Current.Result += "precalc ";

                // Suppression des contraintes positionnées par l'étape courante
                CollectionUtils.RemoveItemsFromCollection(context.OF.Constraints, c => c.ConstrainerStepName == Name);

                // Phase de vérification
                var stepReturn = Check(context);
                Stopwatch.Current.Result += "check ";
                res.LastStepReturn = stepReturn;

                // Sortie de pipe : rembobinage
                if (stepReturn == StepReturn.Rewind)
                {
                    return true;
                }

                // Sortie de pipe : goto step
                if (stepReturn is StepReturnForward)
                {
                    return true;
                }

                // Continuation avec exécution
                if (stepReturn == StepReturn.Process)
                {
                    // Phase application
                    stepReturn = Process(context);
                    Stopwatch.Current.Result += "process ";
                    res.LastStepReturn = stepReturn;

                    // Sortie de pipe : rembobinage
                    if (stepReturn == StepReturn.Rewind)
                    {
                        return true;
                    }

                    // Sortie de pipe : goto step
                    if (stepReturn is StepReturnForward)
                    {
                        return true;
                    }

                }

                return false;
            }
        }
    }

    /// <summary>
    /// Classe fortement typée définissant une étape.
    /// Le type des OrderForm, données de groupe et données d'étape sont spécifiés pour une 
    /// utilisation directe par les classes concrètes
    /// </summary>
    /// <typeparam name="TOrderForm">Type de l'orderform</typeparam>
    /// <typeparam name="TStepData">Type de la donnée d'étape</typeparam>
    /// <typeparam name="TGroupData">Type de la donnée de groupe</typeparam>
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1005:AvoidExcessiveParametersOnGenericTypes")]
    public abstract class Step<TOrderForm, TStepData, TGroupData> : Step
        where TOrderForm : IOF
        where TStepData : StepData, new()
        where TGroupData : GroupData, new()
    {
        /// <summary>
        /// Classe d'accès aux données contextuelle permettant une inialisation lazy des données d'étape et de groupe
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1034:NestedTypesShouldNotBeVisible")]
        public class InnerData
        {
            /// <summary>
            /// OrderForm
            /// </summary>
            public TOrderForm OrderForm { get; }

            private TStepData _data;
            /// <summary>
            /// Donnée d'étape
            /// </summary>
            public TStepData Data
            {
                get
                {
                    return _data ?? (_data = OrderForm.GetPrivateData<TStepData>(_step.Name));
                }
            }

            private TGroupData _groupData;
            /// <summary>
            /// Données de groupe
            /// </summary>
            public TGroupData GroupData
            {
                get
                {
                    return _groupData ?? (_groupData = OrderForm.GetPrivateData<TGroupData>(_step.Group));
                }
            }

            private readonly Step<TOrderForm, TStepData, TGroupData> _step;

            public InnerData(Step<TOrderForm, TStepData, TGroupData> step, TOrderForm orderForm)
            {
                _step = step;
                OrderForm = orderForm;
            }
        }

        protected T GetGroupData<T>(IPrivateDataContainer privateDataContainer)
            where T : Data, new()
        {
            if (string.IsNullOrEmpty(Group))
            {
                return privateDataContainer.GetPrivateData<T>();
            }

            return privateDataContainer.GetPrivateData<T>(Group);
        }

        public override IEnumerable<Type> DataType
        {
            get
            {
                return new[] { typeof(TOrderForm), typeof(TStepData), typeof(TGroupData) };
            }
        }

        public override void PreCalc(OPContext opContext)
        {
            PreCalc(opContext, GetInnerData(opContext));
        }

        /// <summary>
        /// Retourne les données contextuelle encapsulées dans un InnerData permettant une initialisation lazy.
        /// </summary>
        /// <param name="opContext">Contexte d'exécution de l'OrderPipe</param>
        /// <returns>Les données contextuelles encapsulées</returns>
        public InnerData GetInnerData(OPContext opContext)
        {
            return new InnerData(this, (TOrderForm)opContext.OF);
        }

        public override StepReturn Check(OPContext opContext)
        {
            return Check(opContext, GetInnerData(opContext));
        }

        public override StepReturn Process(OPContext opContext)
        {
            return Process(opContext, GetInnerData(opContext));
        }

        /// <summary>
        /// Méthode appelée par l'OrderPipe pour effectuer les traitements non conditionnels de l'étape
        /// </summary>
        /// <param name="opContext">Contexte d'exécution de l'OrderPipe</param>
        /// <param name="innerData">Accès direct aux données contextuelle : OrderForm, GroupData, StepData</param>
        protected virtual void PreCalc(OPContext opContext, InnerData innerData)
        {

        }

        /// <summary>
        /// Méthode appelée par l'OrderPipe permettant de déterminer si l'OrderPipe doit exécuter Process
        /// </summary>
        /// <param name="opContext">Contexte d'exécution de l'OrderPipe</param>
        /// <param name="innerData">Accès direct aux données contextuelle : OrderForm, GroupData, StepData</param>
        /// <returns>Une instance de StepReturn indiquant à l'OrderPipe comment continuer son exécution</returns>
        protected virtual StepReturn Check(OPContext opContext, InnerData innerData)
        {
            return StepReturn.Process;
        }

        /// <summary>
        /// Méthode appelée par l'OrderPipe effectuant le traitement final de l'étape
        /// </summary>
        /// <param name="opContext">Contexte d'exécution de l'OrderPipe</param>
        /// <param name="innerData">Accès direct aux données contextuelle : OrderForm, GroupData, StepData</param>
        /// <returns>Une instance de StepReturn indiquant à l'OrderPipe comment continuer son exécution</returns>
        protected virtual StepReturn Process(OPContext opContext, InnerData innerData)
        {
            return StepReturn.Next;
        }
    }
}
