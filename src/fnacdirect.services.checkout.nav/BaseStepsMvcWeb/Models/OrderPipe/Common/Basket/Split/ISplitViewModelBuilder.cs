using FnacDirect.OrderPipe.Base.Model;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Basket
{
    public interface ISplitViewModelBuilder
    {
        SplitViewModel Build(PopOrderForm popOrderForm);

        decimal SplitCost(PopOrderForm popOrderForm);

        decimal SplitCostNoVAT(PopOrderForm popOrderForm);
    }
}
