using FnacDirect.OrderPipe.Base.Business.LogisticLineGroups.Internals.InitialSelectors;
using FnacDirect.OrderPipe.Base.LineGroups;
using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.OrderPipe.Base.Model.LineGroups;
using System.Collections.Generic;
using System.Linq;

namespace FnacDirect.OrderPipe.Base.Business.LogisticLineGroups.InitialSelectors
{
    public class ShopSaleLineGroupInitialSelector : BaseLineGroupInitialSelector
    {
        private readonly ISwitchProvider _switchProvider;


        public ShopSaleLineGroupInitialSelector(ISwitchProvider switchProvider)
        {
            _switchProvider = switchProvider;
        }


        public override IDictionary<ILineGroupHeader, IEnumerable<Article>> Select(IEnumerable<ILineGroup> lineGroups)
        {
            var output = new Dictionary<ILineGroupHeader, IEnumerable<Article>>();

            // Si le contournement de la GUPT est activé, on traîte les LineGroup
            // Sinon, ce sont IntramagPELogisticLineGroupGenerationRuleDescriptor et IntramagPTLogisticLineGroupGenerationRuleDescriptor qui s'en chargent
            if (_switchProvider.IsEnabled("orderpipe.pop.basket.bypassGuptForSelfService", false))
            {
                // On récupère les LineGroups IntraMag
                return lineGroups.Where(lg => lg.HasShopArticle).ToDictionary(lg => lg as ILineGroupHeader, lg => lg.Articles.OfType<Article>());
            }

            return output;
        }
    }
}
