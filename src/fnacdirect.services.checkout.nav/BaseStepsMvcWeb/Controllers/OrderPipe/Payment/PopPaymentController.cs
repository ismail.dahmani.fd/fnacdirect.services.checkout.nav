using FnacDirect.Context;
using FnacDirect.OrderPipe.Base.Business.Helpers;
using FnacDirect.OrderPipe.Base.Business.Payment;
using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.OrderPipe.Base.Model.Pop;
using FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Common.Payment;
using FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.DataLayer;
using FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Membership;
using FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Omniture;
using FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Payment;
using FnacDirect.OrderPipe.BaseMvc.Web.Pop.Models.Payment;
using FnacDirect.OrderPipe.BaseMvc.Web.Services.Pop.DataLayer;
using FnacDirect.Technical.Framework.Web;
using FnacDirect.Technical.Framework.Web.Mvc;
using System.Web.Mvc;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Controllers.Pop
{
    public class PopPaymentController : PopPaymentBaseController
    {
        private readonly OPContext _opContext;
        private readonly IPaymentHtmlTemplateViewModelBuilder _paymentHtmlTemplateViewModelBuilder;
        private readonly IApplicationContext _applicationContext;
        private readonly IExecutionContextProvider _executionContextProvider;
        private readonly ISwitchProvider _switchProvider;
        private readonly IPaymentPageRootViewModelBuilder _paymentPageRootViewModelBuilder;
        private readonly IOmnitureViewModelBuilder _omnitureViewModelBuilder;
        private readonly IMembershipViewModelBuilder _membershipViewModelBuilder;
        private readonly IDataLayerService _dataLayerService;

        public PopPaymentController(OPContext opContext,
                                    IPaymentViewModelBuilder paymentViewModelBuilder,
                                    IPaymentSelectionBusiness paymentSelectionBusiness,
                                    IPaymentHtmlTemplateViewModelBuilder paymentHtmlTemplateViewModelBuilder,
                                    IApplicationContext applicationContext,
                                    IExecutionContextProvider executionContextProvider,
                                    IUrlManager urlManager,
                                    IPaymentPageRootViewModelBuilder paymentPageRootViewModelBuilder,
                                    IOmnitureViewModelBuilder omnitureViewModelBuilder,
                                    ISwitchProvider switchProvider,
                                    IMembershipViewModelBuilder membershipViewModelBuilder,
                                    IDataLayerService dataLayerService)
            : base(paymentSelectionBusiness, urlManager, paymentViewModelBuilder, opContext)
        {
            _opContext = opContext;
            _applicationContext = applicationContext;
            _executionContextProvider = executionContextProvider;
            _paymentHtmlTemplateViewModelBuilder = paymentHtmlTemplateViewModelBuilder;
            _omnitureViewModelBuilder = omnitureViewModelBuilder;
            _switchProvider = switchProvider;
            _paymentPageRootViewModelBuilder = paymentPageRootViewModelBuilder;
            _membershipViewModelBuilder = membershipViewModelBuilder;
            _dataLayerService = dataLayerService;
        }

        [HttpGet]
        public ActionResult Index()
        {
            var popOrderForm = _opContext.OF as PopOrderForm;
            var popData = popOrderForm.GetPrivateData<PopData>(create: false);

            // on supprime les messages d'erreur qui concernent le stock
            popOrderForm.PipeMessages.ClearForCategory("Stock.Message");

            var rootViewModel = _paymentPageRootViewModelBuilder.GetRootViewModel(popOrderForm, (a, b) => Url.RouteUrl(a, b));
            rootViewModel.Membership = _membershipViewModelBuilder.Build(popOrderForm);

            if (popData != null && popData.UseReact)
            {
                var basketAmount = rootViewModel.Payment.Summary.TotalGlobal;

                // TODO remove it after DataLayer is definitively used:
                rootViewModel.Tracking = _omnitureViewModelBuilder.BuildTracking(popOrderForm, TrackedPage.popPayment, basketAmount);

                var trackingModel = new DatalayerViewModel()
                {
                    PageType = PageTypeEnum.PopPayment,
                    PopOrderForm = popOrderForm,
                };

                var viewResult = View("_Poc/Index", rootViewModel);

                //Datalayer and Tracking switchabled
                _dataLayerService.ComputeShoppingCart(trackingModel, viewResult as ViewResultBase, ControllerContext);

                return viewResult;
            }

            return View("Index", rootViewModel);
        }

        [HttpPost]
        public ActionResult Index(int? paymentMethodId, string pinf)
        {
            var popOrderForm = _opContext.OF as PopOrderForm;

            if (PaymentValidation(paymentMethodId, _opContext))
            {
                var popData = popOrderForm.GetPrivateData<PopData>();

                popData.DisplayOnePageValidated = true;
                // Récupération des informations pour le scoring BATS
                var headers = _executionContextProvider.GetCurrentExecutionContext().GetHeaders();
                ExperianHelper.SetExperianData(_switchProvider, popOrderForm, headers, pinf);

                return Continue();
            }
            else
            {
                var rootViewModel = _paymentPageRootViewModelBuilder.GetRootViewModel(popOrderForm, (a, b) => Url.RouteUrl(a, b));

                return View("Index", rootViewModel);
            }
        }

        public ActionResult Preview()
        {
            var popOrderForm = _opContext.OF as PopOrderForm;

            var rootViewModel = _paymentPageRootViewModelBuilder.GetRootViewModel(popOrderForm, (a, b) => Url.RouteUrl(a, b));

            var paymentHtmlTemplateViewModel = _paymentHtmlTemplateViewModelBuilder.Build(popOrderForm, rootViewModel);

            ViewBag.IsPreviewPayment = true;

            return View("PaymentHtmlTemplate", paymentHtmlTemplateViewModel);
        }

        /// <summary>
        /// Action permettant d'afficher le html pour saisir le code 3DSecure, retourné par Ogone DirectLink
        /// </summary>
        /// <returns></returns>
        public ActionResult Preview3DS()
        {
            var popOrderForm = _opContext.OF as PopOrderForm;

            var ogoneDLData = popOrderForm.GetPrivateData<DirectLinkData>();
            var html3DSecureInput = string.Empty;
            if (ogoneDLData != null)
                html3DSecureInput = ogoneDLData.ResponseDirectLink["3DSHtmlAnswer"];

            var directLink3DSViewModel = new DirectLink3DSecureViewModel
            {
                Html3DSecureInput = html3DSecureInput
            };
            ViewBag.IsPreviewPayment = true;

            return View("Payment3DSecure", directLink3DSViewModel);
        }
    }
}
