using System.Collections.Generic;

namespace FnacDirect.OrderPipe.Base.Model.TaxCalculation
{
    public class TaxCalculationAvailableWrapResult
    {
        public int Id { get; set; }
        public GenericPrice PriceGlobal { get; set; }
        public GenericPrice PriceDetail { get; set; }
        public TaxInfos TaxInfos { get; set; }
    }
}
