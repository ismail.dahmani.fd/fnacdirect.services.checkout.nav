using System.Linq;
using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.OrderPipe.Base.Model.Facets;
using FnacDirect.OrderPipe.Base.LineGroups;
using System.Collections.Generic;

namespace FnacDirect.OrderPipe.Base.Business
{
    public class BillingMethodBusiness : IBillingMethodBusiness
    {
        public static OgoneCreditCardBillingMethod GetOgoneCreditCardBillingMethod (IGotBillingInformations gotBillingInformations) 
        {
            OgoneCreditCardBillingMethod ogoneBillingMethod = null;
            if (gotBillingInformations.OrderBillingMethods != null && gotBillingInformations.OrderBillingMethods.Any())
            {
                ogoneBillingMethod = gotBillingInformations.OrderBillingMethods.FirstOrDefault(bm => bm.BillingMethodId == OgoneCreditCardBillingMethod.IdBillingMethod) as OgoneCreditCardBillingMethod;
            }
            return ogoneBillingMethod; 
        }

        public OgoneCreditCardBillingMethod GetOgoneBillingMethod(IGotBillingInformations gotBillingInformations)
        {
            return GetOgoneCreditCardBillingMethod(gotBillingInformations);
        }

		public ILogisticLineGroup GetHighAmountLineGroup(IEnumerable<ILogisticLineGroup> logisticLineGroups)
		{
			var HightAmountLineGroup = (from lg in logisticLineGroups
                                        where lg.BillingMethodConsolidation.ContainsKey(OgoneCreditCardBillingMethod.IdBillingMethod)
										orderby lg.BillingMethodConsolidation[OgoneCreditCardBillingMethod.IdBillingMethod] descending
										select lg
											  ).FirstOrDefault();
			HightAmountLineGroup.Informations.OrderTransactionInfos.GetByBillingId(OgoneCreditCardBillingMethod.IdBillingMethod).IsSelectedForPsp = true;
			return HightAmountLineGroup;
		}

        public T GetBillingMethod<T>(IGotBillingInformations orderForm, int idBillingMethod) where T : BillingMethod
        {
            T billingMethod = null;

            if (orderForm.OrderBillingMethods != null && orderForm.OrderBillingMethods.Any())
            {
                billingMethod = orderForm.OrderBillingMethods.FirstOrDefault(bm => bm.BillingMethodId == idBillingMethod) as T;
            }

            return billingMethod;
        }

        public T GetBillingMethod<T>(IGotBillingInformations orderForm) where T : BillingMethod
        {
            T billingMethod = null;

            if (orderForm.OrderBillingMethods != null && orderForm.OrderBillingMethods.Any())
            {
                billingMethod = orderForm.OrderBillingMethods.FirstOrDefault(bm => bm is T) as T;
            }

            return billingMethod;
        }
    }
}
