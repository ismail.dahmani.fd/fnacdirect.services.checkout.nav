using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.OrderPipe.Base.Model.Facets;
using FnacDirect.OrderPipe.Base.Model.Pop;
using System.Globalization;
using System.Linq;

namespace FnacDirect.OrderPipe.Base.Business
{
    public class BasketQuantity
    {
        public static bool ModifyExceededQuantityArticleAndGetRewindStatus(IGotLineGroups iContainsLineGroups, BaseArticle baseArticle, int quantity, bool addPrivateData)
        {
            var rewindPipe = false;

            if (!iContainsLineGroups.PrivateData.Contains(baseArticle.ProductID.Value + OPConstantes.DP_FloatingBasketExceedQty) && addPrivateData)
            {
                var stringValueData = new StringValueData
                {
                    Name = baseArticle.ProductID.Value + OPConstantes.DP_FloatingBasketExceedQty,
                    Value = quantity.ToString(CultureInfo.InvariantCulture)
                };
                iContainsLineGroups.PrivateData.Add(stringValueData);
            }

            var popData = iContainsLineGroups.GetPrivateData<PopData>(false);
            if (popData != null)
            {
                popData.HasLimitedQuantity = true;
            }

            var standardArticle = baseArticle as StandardArticle;

            if (standardArticle != null)
            {
                standardArticle.Availability = StockAvailability.Central;
            }

            var basketBus = new Basket.Business.BasketBusiness();


            basketBus.AddItemTo(iContainsLineGroups.SID, baseArticle.ProductID.Value, (short)(quantity - baseArticle.Quantity.Value));

            if (standardArticle?.Services.Any() == true)
            {
                foreach (var service in standardArticle.Services)
                {
                    service.Quantity = quantity;
                }
            }

            if (baseArticle.Quantity != quantity)
            {
                rewindPipe = true;
                if (standardArticle != null)
                {
                    standardArticle.HasExceededQuantity = true;
                }

                // on supprime les anciens messages d'erreur
                iContainsLineGroups.PipeMessages.ClearForCategory("Stock.Message");

                var pipeMessage = iContainsLineGroups.AddWarningMessage("Stock.Message", "orderpipe.basket.dropdownquantity.maxnumber", baseArticle.Title, quantity.ToString());
                pipeMessage.Behavior = PipeMessageBehavior.Persist;
            }

            baseArticle.Quantity = quantity;

            //on modifie aussi les LineGroups
            foreach (var lineGroup in iContainsLineGroups.LineGroups)
            {
                foreach (var article in CollectionUtils.FilterOnType<BaseArticle>(lineGroup.Articles))
                {
                    if (baseArticle.ProductID != article.ProductID || article.ID != baseArticle.ID)
                    {
                        continue;
                    }

                    article.Quantity = quantity;
                    break;
                }
            }

            return rewindPipe;
        }

        public static bool HasQuantityFromPrivateData(BaseOF orderForm, BaseArticle baseArticle)
        {
            var resultat = false;

            if (baseArticle != null && orderForm != null && baseArticle.ProductID.HasValue)
            {
                resultat = orderForm.PrivateData.Contains(baseArticle.ProductID.Value + OPConstantes.DP_FloatingBasketExceedQty);
            }

            return resultat;
        }

        public static int GetQuantityFromPrivateData(BaseOF orderForm, BaseArticle art)
        {
            var Resultat = 0;
            if (HasQuantityFromPrivateData(orderForm, art))
            {
                if (orderForm.PrivateData[art.ProductID.Value + OPConstantes.DP_FloatingBasketExceedQty] is StringValueData Sv)
                    int.TryParse(Sv.Value, out Resultat);
            }
            return Resultat;
        }

        public static void RemoveQuantityFromPrivateData(BaseOF orderForm, BaseArticle art)
        {
            if (HasQuantityFromPrivateData(orderForm, art))
                orderForm.PrivateData.Remove(art.ProductID.Value + OPConstantes.DP_FloatingBasketExceedQty);
        }
    }
}
