using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.OrderPipe.Base.Proxy.Appointment;
using FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Basket;
using FnacDirect.Technical.Framework.Utils;
using FnacDirect.Technical.Framework.Web.Mvc;
using System;
using System.Globalization;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Services.Article.Availability
{
    public class ArticleAvailabilityService : IArticleAvailabilityService
    {
        private readonly IApplicationContext _applicationContext;

        private readonly bool _useMaxQuantityAllowedField;
        private readonly RangeSet<int> _appointmentArticleTypeIds;


        public ArticleAvailabilityService(
            IApplicationContext applicationContext,
            IAppointmentService appointmentService,
            ISwitchProvider switchProvider
            )
        {
            _applicationContext = applicationContext;

            _appointmentArticleTypeIds = appointmentService.GetAppointmentArticleTypeIds();
            _useMaxQuantityAllowedField = switchProvider.IsEnabled("orderpipe.pop.pomcore.usemaxquantityallowedfield");
        }

        public AvailabilityViewModel GetAvailability(StandardArticle standardArticle, Func<StandardArticle, AvailabilityViewModel> onComputingAvailability = null)
        {
            var availabilityViewModel = onComputingAvailability != null ? onComputingAvailability(standardArticle) : new AvailabilityViewModel();
            var articleDTO = standardArticle.ArticleDTO;

            if (_appointmentArticleTypeIds.Contains(standardArticle.TypeId.GetValueOrDefault()))
            {
                availabilityViewModel.FormAtHome = true;
                availabilityViewModel.Availability = _applicationContext.GetMessage("orderpipe.pop.basket.availability.formathome");
                return availabilityViewModel;
            }

            if (standardArticle.IsDematSoft)
            {
                availabilityViewModel.DematSoft = true;
                availabilityViewModel.Availability = _applicationContext.GetMessage("orderpipe.pop.basket.availability.download");
                return availabilityViewModel;
            }

            if (standardArticle.AvailabilityId == (int)AvailabilityEnum.PreOrderRestockingPE
                || standardArticle.AvailabilityId == (int)AvailabilityEnum.PreOrderRestockingPT)
            {
                if (articleDTO.ExtendedProperties.RestockingDate.HasValue)
                {
                    availabilityViewModel.Availability = string.Format("{0} {1}", standardArticle.AvailabilityLabel, standardArticle.ArticleDTO.ExtendedProperties.RestockingDate.Value.ToShortDateString());
                }
                return availabilityViewModel;
            }

            if (standardArticle.AvailabilityId == (int)AvailabilityEnum.PreOrder || standardArticle.AvailabilityId == (int)AvailabilityEnum.PreOrderBis)
            {
                var locale = _applicationContext.GetLocale();

                var preOrder = _applicationContext.GetMessage("orderpipe.pop.basket.availability.preorder");

                if (standardArticle.IsNumerical)
                {
                    availabilityViewModel.IsPreOrder = true;
                    availabilityViewModel.Numerical = true;
                    preOrder = _applicationContext.GetMessage("orderpipe.pop.basket.availability.numerical.preorder");
                }

                if (articleDTO?.ExtendedProperties?.DisplayAnnouncementDay != null
                    && articleDTO.ExtendedProperties.DisplayAnnouncementDay.Value)
                {
                    var preOrderFullDate = _applicationContext.GetMessage("orderpipe.pop.basket.availability.preorder.fulldate");
                    var date = standardArticle.AnnouncementDate.ToString("dd MMMM yyyy", CultureInfo.GetCultureInfo(locale));
                    availabilityViewModel.Availability = string.Format(preOrder, string.Format(preOrderFullDate, date));
                }
                else
                {
                    var preOrderMonth = _applicationContext.GetMessage("orderpipe.pop.basket.availability.preorder.month");
                    var month = standardArticle.AnnouncementDate.ToString("MMMM yyyy", CultureInfo.GetCultureInfo(locale));
                    availabilityViewModel.Availability = string.Format(preOrder, string.Format(preOrderMonth, month));
                }
                return availabilityViewModel;
            }

            if (standardArticle.IsNumerical)
            {
                availabilityViewModel.Numerical = true;
                availabilityViewModel.Availability = _applicationContext.GetMessage("orderpipe.pop.basket.availability.download");
                return availabilityViewModel;
            }

            if (standardArticle is Service)
            {
                availabilityViewModel.Availability = string.Empty;
                return availabilityViewModel;
            }

            if (_useMaxQuantityAllowedField)
            {
                if ((standardArticle.AvailabilityId == (int)AvailabilityEnum.StockPE || standardArticle.AvailabilityId == (int)AvailabilityEnum.StockPT)
                && standardArticle.AvailableStockToDisplay <= 5 && standardArticle.AvailableStockToDisplay > 0)
                {
                    availabilityViewModel.Availability = _applicationContext.GetMessage("orderpipe.pop.basket.availability.stock.limited", standardArticle.AvailableStockToDisplay);
                }
                else
                {
                    availabilityViewModel.Availability = standardArticle.AvailabilityLabel;
                }
                availabilityViewModel.HasQuantityMax = standardArticle.HasExceededQuantity || standardArticle.HasLimitedQuantity;
                return availabilityViewModel;
            }

            if ((standardArticle.AvailabilityId == 99 || standardArticle.AvailabilityId == 199) && standardArticle.AvailableStockToDisplay <= 5 && standardArticle.AvailableStockToDisplay > 0)
            {
                availabilityViewModel.HasLimitedQuantity = true;
                availabilityViewModel.Availability = _applicationContext.GetMessage("orderpipe.pop.basket.availability.stock.limited", standardArticle.AvailableStockToDisplay);
                availabilityViewModel.HasQuantityMax = standardArticle.HasExceededQuantity || standardArticle.HasLimitedQuantity;
                return availabilityViewModel;
            }

            availabilityViewModel.Availability = standardArticle.AvailabilityLabel;
            availabilityViewModel.HasQuantityMax = standardArticle.HasExceededQuantity || standardArticle.HasLimitedQuantity;
            return availabilityViewModel;
        }


    }
}
