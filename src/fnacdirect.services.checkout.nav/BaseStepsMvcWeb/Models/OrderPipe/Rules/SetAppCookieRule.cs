using FnacDirect.Technical.Framework.Web;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace FnacDirect.OrderPipe.CoreRouting.Rules
{
    public class SetAppCookieRule : IRoutingRule
    {
        public bool CanSkip
        {
            get
            {
                return false;
            }
        }

        public IHttpHandler Apply(System.Web.Routing.RequestContext requestContext)
        {
            var request = requestContext.HttpContext.Request;

            if (request.QueryString[ParametersName.MobileAppOrigin] != null)
            {
                var value = request.QueryString[ParametersName.MobileAppOrigin];

                requestContext.HttpContext.Response.SetCookie(new HttpCookie(ParametersName.MobileAppOrigin, value));
            }

            return null;
        }
    }
}
