﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace FnacDirect.OrderPipe.Base.Model
{
    public class SortedCreditCardData : GroupData
    {
        [XmlIgnore]
        private Dictionary<string, Dictionary<string, List<int>>> _sortedCreditCards;

        [XmlIgnore]
        public Dictionary<string, Dictionary<string, List<int>>> SortedCreditCards
        {
            get
            {
                if (_sortedCreditCards == null)
                {
                    _sortedCreditCards = new Dictionary<string, Dictionary<string, List<int>>>();
                }

                return _sortedCreditCards;
            }
        }
    }
}
