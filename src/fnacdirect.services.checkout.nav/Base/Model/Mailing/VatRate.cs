namespace FnacDirect.OrderPipe.Base.Business.Mailing
{
    public class VatRate
    {
        public string Label { get; set; }

        public string Total { get; set; }

        public decimal Rate { get; set; }
    }
}
