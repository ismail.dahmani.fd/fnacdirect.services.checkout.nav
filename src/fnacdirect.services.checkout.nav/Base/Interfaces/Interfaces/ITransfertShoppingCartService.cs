﻿using System;
using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.OrderPipe.Base.Model.Facets;

namespace FnacDirect.OrderPipe.Base.Business.ShoppingCart
{
    public interface ITransfertShoppingCartService
    {
        void TransfertOrderToOrderForm(BaseOF of, Guid g, int referenceType, OPContext opContext);

        void TransfertOrderToPopOrderForm(PopOrderForm popOrderForm, Guid g, int referenceType);

        void AddServiceToBasket(IGotLineGroups orderForm, Article masterArticle, int servId);

        void AddServiceToBasket<T>(IGotLineGroups orderForm, Article masterArticle, int servId) where T : Service, new();
        
        void AddArticleToBasket(OPContext opcontext, int productId, int quantity);

        void AddMarketPlaceArticleToBasket(BaseOF orderForm, int productId, string offerReference, int quantity, OPContext context);
        
        TransferShoppingCartResult TransfertShoppingCartToOrderForm(string uid, string sid, ShoppingCartEntities.ShoppingCartType shoppingCartType, ShoppingCartEntities.FusionType fusionType);
    }
}
