﻿using FnacDirect.Contracts.Online.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace FnacDirect.OrderPipe.Base.ReducedPriceBooks
{
    public class ReducedPriceData : GroupData
    {
        public ReducedPriceData()
        {
            ReducePriceList = new Dictionary<int?, decimal>();
        }

        [XmlIgnore]
        public Dictionary<int?, decimal> ReducePriceList { get; set; }

        [XmlIgnore]
        public decimal Reduction { get; set; }

        [XmlIgnore]
        public IEnumerable<Promotion> Promotions { get; set; }

    }
}