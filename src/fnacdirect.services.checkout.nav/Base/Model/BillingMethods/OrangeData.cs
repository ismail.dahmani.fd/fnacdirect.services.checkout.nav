namespace FnacDirect.OrderPipe.Base.Model.BillingMethods
{
    public class OrangeData : GroupData
    {
        public string OrangeAuthUrl { get; set; }
        public bool IsAssociated { get; set; }
        public string Ise2 { get; set; }
        public string OrangeResult { get; set; }
        public string AccountStatus { get; set; }
        public string Option { get; set; }
        public string Status { get; set; }
        public int AccountId { get; set; }
        public bool ShowOrangeBanner { get; set; }
        public string AccountReference { get; set; }
    }
}
