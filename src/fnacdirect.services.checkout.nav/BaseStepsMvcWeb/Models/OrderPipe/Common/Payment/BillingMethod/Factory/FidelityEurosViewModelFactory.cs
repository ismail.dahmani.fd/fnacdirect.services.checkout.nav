using FnacDirect.Membership.Model.Entities;
using FnacDirect.OrderPipe.Base.Business;
using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.OrderPipe.Core.Services;
using FnacDirect.Technical.Framework.Web;
using FnacDirect.Technical.Framework.Web.Mvc;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web.Mvc;
using FnacDirect.Technical.Framework.Switch;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Common.Payment.BillingMethodFactories
{
    public class FidelityEurosViewModelFactory : BasePaymentViewModelFactory<FidelityEurosBillingMethod>
    {
        private readonly IPipeParametersProvider _pipeParametersProvider;
        private readonly IUserExtendedContextProvider _userExtendedContextProvider;
        private readonly IPriceHelper _priceHelper;
        private readonly IApplicationContext _applicationContext;
        private readonly IVisitorAdapter _visitorAdapter;
        private readonly ISwitchProvider _switchProvider;

        public FidelityEurosViewModelFactory(IPipeParametersProvider pipeParametersProvider,
                                             IUserExtendedContextProvider userExtendedContextProvider,
                                             IApplicationContext applicationContext,
                                             IVisitorAdapter visitorAdapter,
                                             ISwitchProvider switchProvider)
        {
            _pipeParametersProvider = pipeParametersProvider;
            _userExtendedContextProvider = userExtendedContextProvider;
            _priceHelper = applicationContext.GetPriceHelper();
            _applicationContext = applicationContext;
            _switchProvider = switchProvider;
            _visitorAdapter = visitorAdapter;
        }

        public override BillingMethodViewModel Get(string tabName, PaymentMethod paymentMethod, OPContext opContext, PopOrderForm popOrderForm)
        {
            var userExtendedContext = _userExtendedContextProvider.GetCurrentUserExtendedContext();

            var fidelityAllowedDelayAfterExpiration = _pipeParametersProvider.Get<int>("fidelity.advantages.alloweddelay.after.expiration");
            var fidelityDropdownList = _pipeParametersProvider.Get("fidelity.advantages.dropdownlist",true);

            var viewModel = new FidelityEurosViewModel();
            if ((userExtendedContext.Customer.IsValidAdherent || (userExtendedContext.Customer.MembershipContract != null
                && userExtendedContext.Customer.MembershipContract.AdhesionStatus == AdhesionStatus.OutDated
                && userExtendedContext.Customer.MembershipContract.AdhesionEndDate.AddMonths(fidelityAllowedDelayAfterExpiration) > DateTime.Now)))
            {
                var fidelityEuroBusiness = new FidelityBusiness<FidelityEurosBillingMethod>();
                var amount = fidelityEuroBusiness.GetAvailableAmount(userExtendedContext.Customer);
                var multipleAmount = _pipeParametersProvider.Get("fidelity.advantages.multiple.amount", 10m);
                viewModel.MinAmount = multipleAmount;

                if (amount >= multipleAmount)
                {
                    var maxUsableAmount = fidelityEuroBusiness.GetMaxUsableAmount(_visitorAdapter.GetVisitor().Customer, popOrderForm, multipleAmount);

                    //C&C
                    if (popOrderForm.HasClickAndCollectArticle()
                        && TryGetMaximumAllowedAmountForClickAndCollect(popOrderForm, out var maximumAllowedAmount))
                    {
                        viewModel.HasMaximumAllowedAmountForClickAndCollect = true;
                        viewModel.MaximumAllowedAmountForClickAndCollect = maximumAllowedAmount;
                        maxUsableAmount = Math.Min(maxUsableAmount, maximumAllowedAmount.Value);
                    }

                    viewModel.Amount = GetPriceFormat(amount);
                    viewModel.FormattedAmount = GetPriceFormat(amount);
                    var firstExpiry = fidelityEuroBusiness.GetFirstExpiry(_visitorAdapter.GetVisitor().Customer);
                    viewModel.HasExpiration = firstExpiry != null;

                    if (firstExpiry != null)
                    {
                        var amountNearExpiration = fidelityEuroBusiness.ExpirationPoint(firstExpiry);
                        viewModel.AmountToExpire = GetPriceFormat(amountNearExpiration);
                        viewModel.HasUniqueExpiration = maxUsableAmount <= firstExpiry.Amount;

                        if (firstExpiry.ExpirationDate <= DateTime.Now.AddDays(30))
                        {
                            viewModel.ExpireInLessThanOneMonth = true;
                        }
                        var culture = CultureInfo.GetCultureInfo(_applicationContext.GetLocale());
                        viewModel.Expiration = firstExpiry.ExpirationDate.ToString(culture.DateTimeFormat.ShortDatePattern);

                        if (!_switchProvider.IsEnabled("orderpipe.pop.showFirstExpirationIfAmountZero"))
                        {
                            viewModel.HasExpiration = amountNearExpiration != 0;
                        }
                    }

                    var currentBillingMethod = fidelityEuroBusiness.GetCurrentBillingMethod(popOrderForm);

                    if (currentBillingMethod != null)
                    {
                        var usedAmount = fidelityEuroBusiness.GetCurrentBillingMethod(popOrderForm).Amount;
                        if (usedAmount.HasValue)
                        {
                            viewModel.Used = GetPriceFormat(usedAmount.Value);
                            viewModel.Active = true;
                        }
                        var remaining = amount - currentBillingMethod.Amount.GetValueOrDefault(0);

                        if (remaining > 0)
                        {
                            viewModel.Remaining = GetPriceFormat(remaining);
                        }
                    }
                    else
                    {
                        viewModel.Step = multipleAmount;

                        if (fidelityDropdownList)
                        {
                            var usableAmounts = new List<SelectListItem>();

                            if (multipleAmount >= 1)
                            {
                                for (var i = multipleAmount; i <= maxUsableAmount; i += multipleAmount)
                                {
                                    usableAmounts.Add(new SelectListItem()
                                    {
                                        Text = GetPriceFormat(i),
                                        Value = i.ToString(CultureInfo.InvariantCulture),
                                        Selected = false
                                    });
                                }
                                if (usableAmounts.Any()) { usableAmounts.Last().Selected = true; }
                            }
                            else
                            {
                                usableAmounts.Add(new SelectListItem()
                                {
                                    Text = GetPriceFormat(maxUsableAmount),
                                    Value = maxUsableAmount.ToString(CultureInfo.InvariantCulture),
                                    Selected = true
                                });
                            }
                            viewModel.Values = usableAmounts;
                        }
                        viewModel.Active = false;
                    }
                    viewModel.MaxUsableAmount = GetPriceFormat(maxUsableAmount);
                    var totalPriceEur = popOrderForm.GlobalPrices.TotalPriceEur;
                    viewModel.MaxAmount = CalculateMaxAmount(multipleAmount, totalPriceEur, amount);
                }
                viewModel.Display = true;
            }
            return viewModel;
        }
        /// Example: multipleAmount = 0.01 And maxAmount = 10.09 => result maxAmount = 10.09
        /// Example: multipleAmount = 1 And maxAmount = 10.09 => result maxAmount = 10
        private decimal CalculateMaxAmount(decimal multipleAmount, decimal? totalPriceEur, decimal amount)
        {
            var maxAmount = totalPriceEur.HasValue ? Math.Min(amount, totalPriceEur.Value) : amount;
            var isInteger = (multipleAmount % 1) == 0; 

            if (isInteger)
                maxAmount = Math.Floor(maxAmount); 

            return maxAmount;
        }

        private string GetPriceFormat(decimal value)
        {
            if (value % 1 > 0)
                return _priceHelper.Format(value, PriceFormat.CurrencyAsDecimalSeparator);
            else
                return _priceHelper.Format((int)value, PriceFormat.IntegerOnly);
        }

        private bool TryGetMaximumAllowedAmountForClickAndCollect(PopOrderForm popOrderForm, out decimal? maximumAllowedAmount)
        {
            // Si C&C
            if (_switchProvider.IsEnabled("fidelityeuros.constraint.clickandcollect.maxamount.enabled")
                && popOrderForm.HasClickAndCollectArticle())
            {
                maximumAllowedAmount = _pipeParametersProvider.Get("fidelityeuros.constraint.clickandcollect.maxamount", int.MaxValue);
                return true;
            }

            maximumAllowedAmount = null;
            return false;
        }
    }
}
