﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FnacDirect.OrderPipe.Core.Model.Attributes
{
    [AttributeUsage(AttributeTargets.Property)]
    public class NoReset : Attribute { }
}
