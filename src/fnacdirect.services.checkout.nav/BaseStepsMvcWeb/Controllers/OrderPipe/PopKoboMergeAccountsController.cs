﻿using System.Web.Mvc;
using Fnac.DEMAT.Delivery.Contracts;
using FnacDirect.Front.eBook.Business;
using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Kobo;
using FnacDirect.Technical.Framework.Web.Mvc;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Controllers.Pop
{
    public class PopKoboMergeAccountsController : PopBaseController
    {
        private readonly IKoboMergeAccountBuilder _koboMergeAccountBuilder;
        private readonly IAccountBusiness _accountBusiness;
        private readonly IApplicationContext _applicationContext;

        public PopKoboMergeAccountsController(IKoboMergeAccountBuilder koboMergeAccountBuilder, IAccountBusiness accountBusiness, IApplicationContext applicationContext)
        {
            _koboMergeAccountBuilder = koboMergeAccountBuilder;
            _accountBusiness = accountBusiness;
            _applicationContext = applicationContext;
        }

        [HttpGet]
        public ActionResult Index()
        {
            var popOrderForm = GetOpContext().OF as PopOrderForm;

            return View(_koboMergeAccountBuilder.Build(popOrderForm.UserContextInformations));
        }

        [HttpPost]
        public ActionResult Index(KoboMergeAccountViewModel koboAccount)
        {
            if (!ModelState.IsValid)
            {
                return View(koboAccount);
            }
            MergeResponse response = null;
            if (Technical.Framework.Switch.Switch.IsEnabled("ebook.kobo"))
            {
                response = _accountBusiness.MergeAccountKobo(koboAccount.UserReference, koboAccount.Password, koboAccount.Email);
            }
            //the service can return null object so check it
            if (response == null)
            {
                ModelState.AddModelError("", _applicationContext.GetMessage("orderpipe.pop.kobo.technicalerror"));
                return View(koboAccount);
            }

            switch (response.Result)
            {
                case MergeResultType.AuthenticationFailed:
                    ModelState.AddModelError("", _applicationContext.GetMessage("orderpipe.pop.kobo.authenticationfailed"));
                    return View(koboAccount);
                case MergeResultType.Error:
                    ModelState.AddModelError("", _applicationContext.GetMessage("orderpipe.pop.kobo.technicalerror"));
                    return View(koboAccount);
                case MergeResultType.UserCanNotBeMerged:
                    ModelState.AddModelError("", _applicationContext.GetMessage("orderpipe.pop.kobo.accountscannotbemerged"));
                    return View(koboAccount);
            }

            return Continue();
        }
    }
}
