using FnacDirect.OrderPipe.Base.BaseModel;
using FnacDirect.OrderPipe.Base.Business.Shipping.Shop;
using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.OrderPipe.Base.Model.LineGroups;
using FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Basket;
using FnacDirect.Technical.Framework.CoreServices;
using FnacDirect.Technical.Framework.Web.Http.Services;
using FnacDirect.Technical.Framework.Web.Mvc;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.OrderPipe.Basket
{
    internal class ShopAvailabilityViewModelBuilder : IShopAvailabilityViewModelBuilder
    {
        private readonly IApplicationContext _applicationContext;
        private readonly IShipToStoreAvailabilityService _shipToStoreAvailabilityService;
        private readonly ISwitchProvider _switchProvider;
        private readonly ILogger _logger;

        public ShopAvailabilityViewModelBuilder(IApplicationContext applicationContext,
                                          IShipToStoreAvailabilityService shipToStoreAvailabilityService,
                                          ISwitchProvider switchProvider,
                                          ILogger logger)
        {
            _shipToStoreAvailabilityService = shipToStoreAvailabilityService;
            _applicationContext = applicationContext;
            _switchProvider = switchProvider;
            _logger = logger;
        }

        public ShopAvailabilityViewModel Build(StandardArticle standardArticle, ILineGroup parentLineGroup, ShippingMethodEvaluationGroup shippingMethodEvaluationGroup)
        {
            var shopAvailabilityViewModel = new ShopAvailabilityViewModel();

            var shopShippingMethodEvaluationItem = shippingMethodEvaluationGroup.ShopShippingMethodEvaluationItem.Value;
            var shopAddress = shopShippingMethodEvaluationItem.ShippingAddress;

            shopAvailabilityViewModel.ShopAvailability = _applicationContext.GetMessage("orderpipe.pop.basket.availability.shiptostore");
            shopAvailabilityViewModel.ShopName = shopAddress.GetFormatName();

            if (!int.TryParse(shopAddress.RefUG, out var refUG))
            {
                var xmlSerializer = new XmlSerializer(typeof(ShopAddress));
                var stringBuilder = new StringBuilder();
                using (var textWriter = new StringWriter(stringBuilder))
                {
                    xmlSerializer.Serialize(textWriter, shopAddress);
                }

                _logger.WriteWarning(new Exception(), $"ShopAddress with invalid RefUG: {stringBuilder}", 0);

                return shopAvailabilityViewModel;
            }

            if (HasClickAndCollectShipToStoreAvailability(standardArticle, parentLineGroup.Articles, shopShippingMethodEvaluationItem, shopAddress.Identity.GetValueOrDefault(), refUG))
            {
                shopAvailabilityViewModel.ShopAvailability = _applicationContext.GetMessage("orderpipe.pop.basket.availability.clickandcollect");
                shopAvailabilityViewModel.ClickAndCollect = true;
            }
            else if (standardArticle.IsAvailableLessThanOneDay())
            {
                shopAvailabilityViewModel.ShopAvailability = _applicationContext.GetMessage("orderpipe.pop.basket.availability.shiptostore.1j");
            }

            return shopAvailabilityViewModel;
        }

        private bool HasClickAndCollectShipToStoreAvailability(StandardArticle standardArticle, ArticleList articles, ShopShippingMethodEvaluationItem shopShippingMethodEvaluationItem,
            int shopIdentity, int refUG)
        {
            IEnumerable<ShipToStoreAvailability> shopAvailabilities;
            if (_switchProvider.IsEnabled("orderpipe.pop.fdv.enable", false))
            {
                shopAvailabilities = _shipToStoreAvailabilityService.GetAvailabilitiesForShop(articles, shopShippingMethodEvaluationItem, Enumerable.Empty<int>());
            }
            else
            {
                var fnacStore = new FnacStore.Model.FnacStore()
                {
                    Id = shopIdentity,
                    RefUG = refUG
                };
                shopAvailabilities = _shipToStoreAvailabilityService.GetAvailabilities(fnacStore, articles);
            }

            return shopAvailabilities.Where(a => a.ReferenceGu == standardArticle.ReferenceGU).OfType<ClickAndCollectShipToStoreAvailability>().Any();
        }
    }
}
