using System;
using System.Linq;
using System.Collections.Generic;
using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.Technical.Framework.Configuration;
using FnacDirect.Technical.Framework.Web.Helpers;

namespace FnacDirect.OrderPipe.Base.Business.Operations
{
    public class DeliveredForChristmasService : IDeliveredForChristmasService
    {
        private readonly DeliveredForChrismasConfigurationWrapper _chrismasConfigWrapper;

        public DeliveredForChristmasService(IConfigurationProvider configurationProvider, IContextSimulator contextSimulator)
        {
            _chrismasConfigWrapper = new DeliveredForChrismasConfigurationWrapper(contextSimulator, configurationProvider.GetAs<DeliveredForChrismasConfiguration>("DeliveredForChrismas"));
        }

        /// <summary>
        /// Get elligible shipping methods for christmas
        /// </summary>
        /// <returns>List of shipping methods Ids</returns>
        public List<int> GetElligibleShippingMethods()
        {
            return _chrismasConfigWrapper.ElligibleShippingMethods?.ToList();
        }

        /// <summary>
        /// Get availability wording
        /// </summary>
        /// <param name="articles">Articles</param>
        /// <param name="typeWording">Basket / Layer</param>
        /// <returns></returns>
        public string GetWordingWithAvailibility(IEnumerable<Article> articles, TypeWording typeWording)
        {
            if (!articles.Any())
            {
                return string.Empty;
            }

            if (!_chrismasConfigWrapper.HasRules || !_chrismasConfigWrapper.HasCurrentPhase)
            {
                return string.Empty;
            }

            if (articles.Any(a => CanArticleBeDelivredBeforChristmas(a)))
            {
                if (typeWording == TypeWording.Basket)
                {
                    return _chrismasConfigWrapper.CurrentPhase.BasketWordingUiressourceKey;
                }
                else if (typeWording == TypeWording.Layer)
                {
                    return _chrismasConfigWrapper.CurrentPhase.LayerWordingUiressourceKey;
                }
            }

            return string.Empty;
        }

        /// <summary>
        /// Used to flague eligible articles on basket
        /// </summary>
        /// <param name="article"></param>
        /// <returns></returns>
        public bool CanArticleBeDelivredBeforChristmas(Article article)
        {
            if ((article is StandardArticle && !_chrismasConfigWrapper.IsAllowedCatalog(1)) || (article is MarketPlaceArticle && !_chrismasConfigWrapper.IsAllowedCatalog(3)))
            {
                return false;
            }

            if (_chrismasConfigWrapper.IsExcludedFamily(article.ArticleDTO.Core.Family.ID))
            {
                return false;
            }

            if (!_chrismasConfigWrapper.IsElligibleAvailability((article as BaseArticle).AvailabilityId.GetValueOrDefault()))
            {
                return false;
            }

            if (!(!_chrismasConfigWrapper.CurrentPhase.IsTechnicalOnly || (_chrismasConfigWrapper.CurrentPhase.IsTechnicalOnly && (article as BaseArticle).IsPT)))
            {
                return false;
            }

            return true;
        }


        public DateTime? GetEventDateOfCurrentPhase()
        {
            if (!_chrismasConfigWrapper.HasRules || !_chrismasConfigWrapper.HasCurrentPhase)
            {
                return null;
            }

            return _chrismasConfigWrapper.CurrentPhase.EventDate;
        }

        public bool IsSplitAutoAndFree()
        {
            var splitStartDate = _chrismasConfigWrapper.SplitStartDate;

            var splitEndDate = _chrismasConfigWrapper.SplitEndDate;

            if (!splitStartDate.HasValue || !splitEndDate.HasValue)
            {
                return false;
            }

            var now = DateTime.Now;

            return splitStartDate.Value <= now && splitEndDate.Value >= now;
        }

        public string GetPictoUiresourceKey()
        {
            if (!_chrismasConfigWrapper.HasRules || !_chrismasConfigWrapper.HasCurrentPhase)
            {
                return string.Empty;
            }

            return _chrismasConfigWrapper.CurrentPhase.PipePictoUiresourceKey;
        }

        public string GetBasketUiresourceKey()
        {
            if (!_chrismasConfigWrapper.HasRules || !_chrismasConfigWrapper.HasCurrentPhase)
            {
                return string.Empty;
            }

            return _chrismasConfigWrapper.CurrentPhase.BasketWordingUiressourceKey;
        }

        public string GetShippingWording()
        {
            if (!_chrismasConfigWrapper.HasRules || !_chrismasConfigWrapper.HasCurrentPhase)
            {
                return string.Empty;
            }

            return _chrismasConfigWrapper.CurrentPhase.ShippingWordingUiressourceKey;
        }

        public bool HasCurrentPhase()
        {
            return _chrismasConfigWrapper.HasCurrentPhase;
        }
    }
}
