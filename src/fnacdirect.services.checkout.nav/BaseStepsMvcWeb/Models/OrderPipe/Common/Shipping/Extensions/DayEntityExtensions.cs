﻿using FnacDirect.FnacStore.Model;
using FnacDirect.Relay.Model;
using FnacDirect.Technical.Framework.Web.Mvc;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Common.Shipping
{
    public static class DayEntityExtensions
    {
        public static IEnumerable<TimetableViewModel> ConvertToTimetableViewModel(this IEnumerable<DayEntity> openingHours, IApplicationContext applicationContext)
        {
            if (applicationContext != null && openingHours != null)
            {
                var uiResourceService = applicationContext.GetUiResourceService();
                var locale = applicationContext.GetLocale();
                var dayOfWeek = (int)DateTime.Today.DayOfWeek;

                return (from openingHour in openingHours
                        let dayId = openingHour.DayId % 7
                        let isToday = dayId == dayOfWeek
                        let day = uiResourceService.GetUIResourceValue("orderpipe.pop.shipping.store.timetable.dayofweek." + dayId.ToString(CultureInfo.InvariantCulture), locale)
                        let amOpening = !openingHour.IsOpenedAM ? null : new FnacOpeningPeriod(openingHour.AMOpening, openingHour.AMClosing)
                        let pmOpening = !openingHour.IsOpenedPM ? null : new FnacOpeningPeriod(openingHour.PMOpening, openingHour.PMClosing)
                        where amOpening !=  null || pmOpening != null
                        select new TimetableViewModel(isToday, day, new[] { amOpening, pmOpening })).ToArray();
            }

            return Enumerable.Empty<TimetableViewModel>();
        }
    }
}
