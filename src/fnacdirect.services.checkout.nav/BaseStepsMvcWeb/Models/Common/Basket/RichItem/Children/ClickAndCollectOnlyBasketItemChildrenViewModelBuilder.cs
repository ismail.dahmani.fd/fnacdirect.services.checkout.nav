using System.Collections.Generic;
using System.Linq;
using FnacDirect.OrderPipe.Base.Model;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Basket.Children
{
    public class ClickAndCollectBasketItemChildrenViewModelBuilder : ICrossSellRichBasketItemChildrenViewModelBuilder
    {

        public IEnumerable<CrossSellRichItemChildrenViewModel> Build(StandardArticle standardArticle)
        {
            return Build(standardArticle, Enumerable.Empty<Base.Model.Promotion>(), false).OfType<CrossSellRichItemChildrenViewModel>();
        }

        public IEnumerable<ChildBasketItemViewModel> Build(StandardArticle standardArticle, IEnumerable<Base.Model.Promotion> globalPromotions, bool applyAdherentPromotion)
        {
            if (standardArticle.Availability == StockAvailability.ClickAndCollectOnly)
            {                
                yield return new CrossSellRichItemChildrenViewModel
                {
                    IsDummy = true
                };
            }
        }
    }
}
