using FnacDirect.OrderPipe.Base.Model;
using System.Linq;
using System.Collections.Generic;

namespace FnacDirect.OrderPipe.Base.Business.Shipping.Shop
{
    public abstract class ShipToStoreAvailability
    {
        private readonly string _referenceGu;

        public int ShippingMethodId { get; set; }

        public string ReferenceGu
        {
            get { return _referenceGu; }
        }

        protected ShipToStoreAvailability(string referenceGu)
        {
            _referenceGu = referenceGu;
        }

        protected ShipToStoreAvailability(string referenceGu, int shippingMethodId) : this(referenceGu)
        {
            ShippingMethodId = shippingMethodId;
        }

        public StandardArticle FindArticle(IEnumerable<Article> articles)
        {
            return articles.OfType<StandardArticle>().FirstOrDefault(a => a.ReferenceGU == _referenceGu);
        }
    }
}
