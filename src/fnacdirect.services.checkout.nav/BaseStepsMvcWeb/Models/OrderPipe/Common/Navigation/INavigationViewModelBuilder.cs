using FnacDirect.OrderPipe.Base.Model;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Pop.Models.Common
{
    public interface INavigationViewModelBuilder
    {
        NavigationViewModel Build(PopOrderForm popOrderForm, string urlName);
        NavigationViewModel BackToHome();
    }
}
