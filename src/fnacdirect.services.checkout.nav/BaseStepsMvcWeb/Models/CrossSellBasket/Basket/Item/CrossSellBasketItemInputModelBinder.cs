﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web.Mvc;

namespace FnacDirect.OrderPipe.BaseMvc.Web.CrossSellBasket
{
    public class CrossSellBasketItemInputModelBinder : DefaultModelBinder
    {
        public override object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext)
        {
            if (controllerContext.HttpContext.Request.Headers.AllKeys.Contains("Content-Type")
         && controllerContext.HttpContext.Request.Headers.GetValues("Content-Type").Any()
         && controllerContext.HttpContext.Request.Headers.GetValues("Content-Type")[0] == "application/x-www-form-urlencoded")
            {
                return ParseArticleReferences(controllerContext.HttpContext.Request.Form.ToString());
            }

            return base.BindModel(controllerContext, bindingContext);
        }

        private List<CrossSellBasketItemInput> ParseArticleReferences(string contextValue)
        {
            var parameters = Uri.UnescapeDataString(contextValue).Split('&');
            var articleIndexRegex = new Regex(@"(.*)\[(\d+)\]");

            var items = new Dictionary<int, CrossSellBasketItemInput>();

            foreach (var parameter in parameters)
            {
                var parameterSplitted = parameter.Split('=');

                if (parameterSplitted.Length != 2)
                {
                    continue;
                }
                var key = parameterSplitted[0].ToLowerInvariant();
                var valueAsString = parameterSplitted[1].ToLowerInvariant();

                var articleIndexRegexResult = articleIndexRegex.Match(key);

                if (articleIndexRegexResult == null
                    || !articleIndexRegexResult.Success
                    || articleIndexRegexResult.Groups.Count != 3)
                {
                    continue;
                }
                var propertyName = articleIndexRegexResult.Groups[1].Value;
                var indexAsString = articleIndexRegexResult.Groups[2].Value;

                if (!int.TryParse(indexAsString, out var index))
                {
                    continue;
                }

                CrossSellBasketItemInput item;

                if (items.ContainsKey(index))
                {
                    item = items[index];
                }
                else
                {
                    item = new CrossSellBasketItemInput();
                    items.Add(index, item);
                }

                if (propertyName == "productid")
                {
                    if (int.TryParse(valueAsString, out var productId))
                    {
                        item.ProductId = productId;
                    }
                }
                else if (propertyName == "quantity")
                {
                    if (int.TryParse(valueAsString, out var quantity))
                    {
                        item.Quantity = quantity;
                    }
                }
            }

            return new List<CrossSellBasketItemInput>(items.Values);
        }
    }
}
