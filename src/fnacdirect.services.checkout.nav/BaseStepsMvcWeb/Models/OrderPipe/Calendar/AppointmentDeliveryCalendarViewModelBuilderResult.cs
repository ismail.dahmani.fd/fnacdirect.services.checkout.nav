using FnacDirect.OrderPipe.Base.Business.Shipping;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Solex.Appointment
{
    public struct AppointmentDeliveryCalendarViewModelBuilderResult
    {
        public bool IsSuccess { get; set; }
        public AppointmentDeliverySlotsSortingResult SlotsData { get; set; }
        public AppointmentDeliveryCalendarViewModel ViewModel { get; set; }
        public string ErrorMessage { get; set; }
    }
}
