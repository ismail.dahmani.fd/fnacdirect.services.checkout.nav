using FnacDirect.Front.Meteor.Business.Models;
using FnacDirect.OrderPipe.Base.BaseModel.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Serialization;

namespace FnacDirect.OrderPipe.Base.Model
{
    /// <summary>
    /// Classe inermidiare entre OF et BaseOF
    /// </summary>
    public class BasketOF : OF
    {
        private readonly ArticleList _articles = new ArticleList();

        public ArticleList Articles
        {
            get { return _articles; }
        }
        
        private readonly ArticleList _deletedArticles = new ArticleList();

        public ArticleList DeletedArticles
        {
            get { return _deletedArticles; }
        }

        private readonly ArticleList _removedArticles = new ArticleList();                

		/// <summary>
		/// Renvoie la liste des articles qui viennent d'être supprimés (intentionnellement, à la main) dans le pipe
		/// </summary>
		[XmlIgnore]
		public ArticleList RemovedArticles
		{
			get { return _removedArticles; }
		}
        
        private LineGroupList _lineGroups = new LineGroupList();

        public LineGroupList LineGroups
        {
            get { return _lineGroups; }
            set { _lineGroups = value; }
        }

        private LineGroupList _previouslineGroups = new LineGroupList();

        public LineGroupList PreviouslineGroups
        {
            get { return _previouslineGroups; }
            set { _previouslineGroups = value; }
        }

        private List<LineGroup> _fnacArticles = new List<LineGroup>();

        [XmlIgnore]
        public List<LineGroup> FnacArticles
        {
            get { return _fnacArticles; }
            set { _fnacArticles = value; }
        }

        private List<LineGroup> _ccvArticles = new List<LineGroup>();
        [XmlIgnore]
        public List<LineGroup> CcvArticles
        {
            get { return _ccvArticles; }
            set { _ccvArticles = value; }
        }

        private CcvShippingInfo _ccvShippingInfo = new CcvShippingInfo();
        public CcvShippingInfo CcvShippingInfo
        {
            get { return _ccvShippingInfo; }
            set { _ccvShippingInfo = value; }
        }

        private MultiShippingInfo _multiShippingInfo = new MultiShippingInfo();
        public MultiShippingInfo MultiShippingInfo
        {
            get { return _multiShippingInfo; }
            set { _multiShippingInfo = value; }
        }
        
        private readonly PromotionList _globalPromotions = new PromotionList();

        [XmlIgnore]
        public PromotionList GlobalPromotions
        {
            get { return _globalPromotions; }
        }

        private readonly GlobalPrices _globalPrices = new GlobalPrices();

        public GlobalPrices GlobalPrices
        {
            get { return _globalPrices; }
        }

        private UserInfo _userInfo = new UserInfo();

        [XmlElement]
        public UserInfo UserInfo
        {
            get { return _userInfo; }
            set { _userInfo = value; }
        }

        [XmlIgnore]
        public XmlOrderForm XmlOrder { get; set; }

        public OrderTypeEnum OrderType { get; set; }

        [XmlIgnore]
        public bool HasOnlyMarketPlaceArticle
        {
            get { return LineGroups.All(lg => lg.HasMarketPlaceArticle); }
        }

        /// <summary>
        /// Cette propriété est censée déterminer si plusieurs vendeurs sont concernés par cette commande.
        /// Cela se traduit par l'évaluation du nombre de LineGroups.
        /// <remarks>
        /// </remarks>
        /// </summary>
        [XmlIgnore]
        public bool HasMultipleVendor
        {
            get { return (LineGroups != null && LineGroups.Count > 1); }
        }

        [XmlIgnore]
        public VatCountry VatCountry { get; set; }

        public bool HasFnacComArticle
        {
            get
            {
                return (OrderType & OrderTypeEnum.FnacCom) == OrderTypeEnum.FnacCom
                    || (OrderType & OrderTypeEnum.Pro) == OrderTypeEnum.Pro;
            }
        }

        public bool HasMarketPlaceArticle
        {
            get
            {
                return (OrderType & OrderTypeEnum.MarketPlace) == OrderTypeEnum.MarketPlace;
        }
        }

        /// <summary>
        /// Obtient une valeur déterminant si le type de commande globale de l'OrderForm 
        /// correspond à des retraits en magasin.
        /// </summary>
        /// <returns></returns>
        public bool HasClickAndCollectArticle
        {
            get
            {
                return (OrderType & OrderTypeEnum.CollectInStore) == OrderTypeEnum.CollectInStore;
            }
        }

        public bool HasDematSoftArticle
        {
            get
            {
                return (OrderType & OrderTypeEnum.DematSoft) == OrderTypeEnum.DematSoft;
            }
        }

        public bool IsMixedClickAndCollect
        {
            get
            {
                return (HasClickAndCollectArticle && HasFnacComArticle);
        }
        }

        /// <summary>
        /// cette propriété permet de savoir si le panier contien des abonnements (Express+ ou adhésion pour le moment).
        /// il faut enrechir la liste avec les nouveaux types d'abonnements
        /// </summary>
        [XmlIgnore]
        public bool HasSubscription
        {
            get
            {
                //vérifier si le panier contient Express+
                var aid = GetPrivateData<AboIlliData>("AboIlliGroup", true);
                var hasExpressPlus = aid != null && aid.HasAboIlliInBasket;

                //vérifier si le panier contient une adhésion
                var hasAdhesion = false;
                foreach (var Art in Articles)
                {
                    if (Art.Family.GetValueOrDefault(0) == (int)FnacDirect.Contracts.Online.Model.ArticleGroup.AdherentCard)
                    {
                        hasAdhesion = true;
                        break;
                    }
                }
                if (!hasAdhesion)
                {
                    var Pad = PrivateData.GetByType<PushAdherentData>();
                    hasAdhesion = Pad != null && Pad.categoryCardType.HasValue;
                }

                //retourner le résultat
                return (hasAdhesion || hasExpressPlus);
            }
        }

        [XmlIgnore]
        public bool IsVoluminous
        {
            get
            {
                return LineGroups.Any(lg => lg.IsVoluminous);
            }
        }

        public bool HasMarketPlaceAndClickAndCollectArticle
        {
            get
            {
                return HasClickAndCollectArticle && HasMarketPlaceArticle;
            }
        }

        public override OFRoute GetRoute()
        {
            if (this.IsOneClick())
            {
                return OFRoute.OneClick;
            }

            return OFRoute.Unknown;
        }
    }

    [Flags]
    [Serializable]
    public enum BasketType : int
    {
        FromShoppingCart = 0,
        FromOrderRef = 1,
        FromMainOrderRef = 2
    }
}
