﻿using FnacDirect.OrderPipe.Base.LineGroups;
using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.OrderPipe.Base.Proxy;
using FnacDirect.OrderPipe.CM.Business;
using FnacDirect.Technical.Framework.Utils;

namespace FnacDirect.OrderPipe.FDV.Business
{
    public class GuptCMBusinessProvider : IGuptCMBusinessProvider
    {
        private readonly IClickAndMagService _clickAndMagService;

        public GuptCMBusinessProvider(IClickAndMagService clickAndMagService)
        {
            _clickAndMagService = clickAndMagService;
        }

        public IGuptCMBusiness GetGuptCMBusiness(ILogisticLineGroup lg)
        {
            if (lg.OrderType == OrderTypeEnum.FnacCom)
                return new GuptCMBusinessStandard(_clickAndMagService);
            else
                return new GuptCMBusinessMP(_clickAndMagService);
        }
    }
}
