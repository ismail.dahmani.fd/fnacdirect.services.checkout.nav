﻿using FnacDirect.OrderPipe.Base.Model;

namespace FnacDirect.OrderPipe.Base.Business.Payment
{
    public interface IDeferredPaymentBusiness
    {
        DeferredPaymentEligibility GetDeferredPaymentEligibility(int accountId, decimal orderTotalPrice, bool fromCallCenter);
    }
}
