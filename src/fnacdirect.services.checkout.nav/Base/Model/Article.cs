using FnacDirect.OrderPipe.FDV.Model;
using System;
using System.Xml.Serialization;
using DTO = FnacDirect.Contracts.Online.Model;
using FnacDirect.Contracts.Online.Model;
using System.Text;

namespace FnacDirect.OrderPipe.Base.Model
{
    [XmlInclude(typeof(StandardArticle))]
    [XmlInclude(typeof(VirtualGiftCheck))]
    [XmlInclude(typeof(Service))]
    [XmlInclude(typeof(ShopService))]
    [XmlInclude(typeof(ShopServiceBundle))]
    [XmlInclude(typeof(Bundle))]
    [XmlInclude(typeof(BaseArticle))]
    [XmlInclude(typeof(MarketPlaceArticle))]
    public abstract class Article
    {
        [XmlIgnore]
        public DTO.Article ArticleDTO { get; set; }

        public int? SellerID { get; set; }

        [XmlIgnore]
        public bool SellerIDSpecified => SellerID.HasValue;

        public ShippingAddress Address { get; set; }

        public int Referentiel { get; set; } = 1;

        public int? ProductID { get; set; }

        public virtual ArticleReference ToArticleReference() => new ArticleReference(ProductID, null, null, null);

        [XmlIgnore]
        public int? ID { get; set; }

        [XmlIgnore]
        public int? OrderDetailPk { get; set; }

        public DateTime InsertDate { get; set; }

        public int? Quantity { get; set; }

        public virtual int GetQuantity() => Quantity.GetValueOrDefault(1);

        [XmlIgnore]
        public int? MaximumQuantity { get; set; }

        [XmlIgnore]
        public int? LogType { get; set; }

        /// <summary>
        /// 1 = Produit achetable
        /// 2 = Produit gratuit
        /// </summary>
        public ArticleType? Type { get; set; }

        public string OriginCode { get; set; }

        public string ExternalOrderDetailReference { get; set; }

        [XmlIgnore]
        public int? TypeId { get; set; }

        [XmlIgnore]
        public string TypeLabel { get; set; }

        [XmlIgnore]
        public int? Family { get; set; }

        [XmlIgnore]
        public UniverseInfo UniverseInfo { get; set; }

        [XmlIgnore]
        public int? DistributorAvailabilityTypeId { get; set; }

        protected Article()
        {
            UniverseInfo = null;
            Type = ArticleType.Saleable;
        }

        [XmlIgnore]
        public string DeliveryDate { get; set; } = "";

        /// <summary>
        /// Obtient ou définit une valeur déterminant si l'article doit être retiré en magasin.
        /// </summary>
        public bool CollectInShop { get; set; }

        /// <summary>
        /// Obtient ou définit une valeur déterminant si l'article doit être livré par coursier depuis le stock magasin
        /// </summary>
        [XmlIgnore]
        public bool SteedFromStore { get; set; }

        /// <summary>
        /// Obtient ou définit le libellé de disponibilité de l'article à retirer en magasin.
        /// </summary>
        public string CollectAvailabilityLabel { get; set; }

        /// <summary>
        /// Obtient ou définit une valeur déterminant si l'article est disponible en stock.
        /// </summary>
        public bool IsAvailableInStore { get; set; }

        /// <summary>
        /// Obitent ou définit l'ID do'rigine de l'article. Suite à la persistance du panier, permet de savoir si l'article vient du panier SID ou UID
        /// </summary>
        public string ShoppingCartOriginId { get; set; }

        /// <summary>
        /// Obtient ou définit une valeur déterminant si l'article est acheté directement depuis le stock magasin ou le rayon
        /// </summary>
        [XmlIgnore]
        public bool ShopFromStore { get; set; }

        /// <summary>
        /// Est-ce que c'est un Plus produit ? (produit promotionnel ou cadeau suite à l'achat d'un autre produit).
        /// </summary>
        [XmlIgnore]
        public bool IsPlusProduit => Type == ArticleType.Free;


        public override string ToString()
        {
            var article = new StringBuilder();
            article.AppendLine($"ObjectType :{GetType()}");
            article.AppendLine($"ProductID :{ProductID}");
            article.AppendLine($"Qty :{Quantity}");
            article.AppendLine($"IsAvailableInStore :{IsAvailableInStore}");
            article.AppendLine($"Type :{Type}");
            article.AppendLine($"LogType :{LogType}");
            article.AppendLine($"Family :{Family}");
            return article.ToString();
        }
    }
}
