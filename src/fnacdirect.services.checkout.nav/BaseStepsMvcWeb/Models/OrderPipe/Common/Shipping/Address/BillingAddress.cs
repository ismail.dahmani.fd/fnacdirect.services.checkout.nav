using System.Xml.Serialization;
using FnacDirect.Customer.Model;

namespace FnacDirect.OrderPipe.Base.Model
{
    public class BillingAddress
    {
        public int? Identity { get; set; }

        public int? Type { get; set; }

        public string Reference { get; set; }

        [XmlIgnore]
        public string AddressBookReference { get; set; }

        [XmlIgnore]
        public string Alias { get; set; }

        [XmlIgnore]
        public string VATNumber { get; set; }

        [XmlIgnore]
        public string Company { get; set; }

        [XmlIgnore]
        public string GenderLabel { get; set; }

        
        public string Firstname { get; set; }

        
        public string Lastname { get; set; }

        
        public string AddressLine1 { get; set; }

        
        public string AddressLine2 { get; set; }

        
        public string AddressLine3 { get; set; }

        
        public string ZipCode { get; set; }

        
        public string City { get; set; }

        
        public string State { get; set; }

        
        public int? CountryId { get; set; }

        
        public string CountryLabel { get; set; }

        [XmlIgnore]
        public string CountryCode3 { get; set; }

        [XmlIgnore]
        public string CountryCode2 { get; set; }

        [XmlIgnore]
        public int? BillingZone { get; set; }

        public string Phone { get; set; }

        
        public string CellPhone { get; set; }

        [XmlIgnore]
        public string Fax { get; set; }

        [XmlIgnore]
        public int? BillingAddressPK { get; set; }

        [XmlIgnore]
        public string TaxIdNumberType { get; set; }

        [XmlIgnore]
        public string TaxIdNumber { get; set; }
              
        [XmlIgnore]
        public int DocumentCountryId { get; set; }

        [XmlIgnore]
        public string AddressLine4 { get; set; }

        [XmlIgnore]
        public int NationalityCountry { get; set; }

        [XmlIgnore]
        public int LegalStatus { get; set; }

        
        public int GenderId { get; set; }

        [XmlIgnore]
        public bool IsDefault { get; set; }

        public string Email { get; set; }

        public string ConfirmEmail { get; set; }

        public AddressEntity AddressEntity { get; set; }

        [XmlIgnore]
        public bool IsCountryUseVAT { get; set; }

        public BillingAddress() { }

        public BillingAddress(AddressEntity address)
        {
            IsDefault = address.IsDefaultBilling;
            Identity = address.AddressBookId;
            Reference = address.AddressBookReference;
            Type = 1;
            Alias = address.AliasName;
            Company = address.Company;
            GenderId = address.GenderId;
            GenderLabel = address.GenderLabel;
            Firstname = address.FirstName;
            Lastname = address.LastName;
            AddressLine1 = address.AddressLine1;
            AddressLine2 = address.AddressLine2;
            AddressLine3 = address.AddressLine3;
            AddressLine4 = address.AddressLine4;
            ZipCode = address.Zipcode;
            City = address.City;
            State = address.State;
            CountryId = address.CountryId;
            CountryLabel = address.CountryLabel;
            CountryCode2 = address.CountryCode2;
            Phone = string.IsNullOrEmpty(address.Tel) ? address.CellPhone : address.Tel;
            CellPhone = address.CellPhone;
            Fax = address.Fax;
            VATNumber = address.VatNumber;
            TaxIdNumber = address.TaxIdNumber;
            TaxIdNumberType = address.TaxIdNumberType;
            NationalityCountry = address.NationalityCountryId;
            LegalStatus = address.LegalStatusId;
            IsCountryUseVAT = address.UseVat;
            AddressEntity = address;
            Email = address.EMail;
            ConfirmEmail = address.EMail;
            DocumentCountryId = address.DocumentCountryId;
        }

        public bool IsDummy()
        {
            return !Identity.HasValue || Identity.Value == -1;
        }

        //TODO Merge duplicate Convert in Billing and PostalAddress
        public IAddressViewModelFieldsValidation Convert()
        {
            IAddressViewModelFieldsValidation address = new AddressViewModelFieldsValidationDto(this);

            return address;
        }

    }
}
