﻿using FnacDirect.Technical.Framework.Utils;
using System;
using System.Collections.Generic;

namespace FnacDirect.OrderPipe.Core.Services
{
    public interface IPipeParametersProvider
    {
        T Get<T>(string key);
        T Get<T>(string key, T defaultValue);
        IEnumerable<T> GetAsEnumerable<T>(string key) where T : IComparable<T>;
        IDictionary<string, T> GetAsDictionary<T>(string key) where T : IComparable<T>;
        RangeSet<T> GetAsRangeSet<T>(string key) where T : IComparable<T>;
    }
}
