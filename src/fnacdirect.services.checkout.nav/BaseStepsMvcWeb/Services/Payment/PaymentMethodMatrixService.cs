﻿using FnacDirect.OrderPipe.Base.DAL;
using FnacDirect.Technical.Framework.Caching2;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FnacDirect.OrderPipe.Base.Business.Payment
{
    public class PaymentMethodMatrixService : IPaymentMethodMatrixService
    {
        private readonly IPaymentMethodDal _paymentMethodDal;
        private readonly ICacheService _cacheService;

        public PaymentMethodMatrixService(IPaymentMethodDal paymentMethodDal, ICacheService cacheService)
        {
            _paymentMethodDal = paymentMethodDal;
            _cacheService = cacheService;
        }

        public int GetRemainingMethodMask(int currentMethodsMask, int allowedMethodsMask, string configuration)
        {
            var matrix = _cacheService.Get(new CacheKey("PaymentMethodMatrix", configuration),
                                                                                    () => _paymentMethodDal.GetPaymentMethodMatrix(configuration));

            foreach (var entry in matrix)
            {
                allowedMethodsMask = FilterAllowedMethodsMask(currentMethodsMask, allowedMethodsMask, entry.Key, entry.Value);
            }

            if(allowedMethodsMask == 0)
            {
                return allowedMethodsMask;
            }

            var outputMask = allowedMethodsMask;

            foreach (var entry in matrix)
            {
                outputMask = ApplyCombination(currentMethodsMask, allowedMethodsMask, outputMask, entry.Key, entry.Value);
            }

            return outputMask;
        }

        private int FilterAllowedMethodsMask(int currentMethodsMask, int allowedMethodsMask, int billingMethodId, int matrixValue)
        {
            if (currentMethodsMask == 0)
            {
                return allowedMethodsMask;
            }

            var alreadyUsed = (currentMethodsMask & billingMethodId) != 0;

            if(alreadyUsed)
            {
                return allowedMethodsMask & matrixValue;
            }

            return allowedMethodsMask;
        }

        private int ApplyCombination(int currentMethodsMask, int originalAllowedMethodsMask, int allowedMethodsMask, int billingMethodId, int matrixValue)
        {
            if (currentMethodsMask == 0)
            {
                return allowedMethodsMask;
            }

            var alreadyUsed = (currentMethodsMask & billingMethodId) != 0;
            var canBeUsed = (originalAllowedMethodsMask & matrixValue) != 0;
            var canBeUsedWithItSelf = (matrixValue & billingMethodId) != 0;

            if(canBeUsed)
            {
                if(alreadyUsed && !canBeUsedWithItSelf)
                {
                    if ((allowedMethodsMask & billingMethodId) != 0)
                    {
                        return allowedMethodsMask - billingMethodId;
                    }
                }

                return allowedMethodsMask;
            }

            return allowedMethodsMask;
        }
    }
}
