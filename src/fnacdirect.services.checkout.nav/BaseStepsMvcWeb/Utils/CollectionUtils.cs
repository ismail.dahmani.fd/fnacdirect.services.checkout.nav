namespace FnacDirect.OrderPipe
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq;

    public static class CollectionUtils
    {
        public static IEnumerable<T> CheckNullList<T, TCollection>(ref TCollection obj)
            where T : class
            where TCollection : class, ICollection<T>
        {
            var col = (ICollection<T>)obj;
            if (col != null && col.Count == 0)
                obj = null;
            if (obj != null)
                return (IEnumerable<T>)col;
            else
                return new List<T>();
        }

        public static IEnumerable<T> FilterOnType<T>(IEnumerable en) where T : class
        {
            foreach (var o in en)
            {
                if (o is T t)
                    yield return t;
            }
        }

        public static int RemoveItemsFromCollection<T>(ICollection<T> collection, Predicate<T> decide) where T : class
        {
            if (collection == null || decide == null)
                return 0;

            var removed = new List<T>();
            foreach (var item in collection)
            {
                if (decide(item))
                    removed.Add(item);
            }

            foreach (var item in removed)
            {
                collection.Remove(item);
            }

            var ret = removed.Count;
            removed.Clear();
            return ret;
        }

        public static int RemoveItems<T>(IList collection, Predicate<T> decide) where T : class
        {
            if (collection == null || decide == null)
                return 0;

            var removed = new List<T>();
            foreach (var obj in collection)
            {
                if (obj is T item)
                {
                    if (decide(item))
                        removed.Add(item);
                }
            }

            foreach (var item in removed)
            {
                collection.Remove(item);
            }
            var ret = removed.Count;
            removed.Clear();
            return ret;
        }

        /// <summary>
        /// Retourne la plage de donn�es sp�cifi�e.
        /// </summary>
        /// <typeparam name="TSource"></typeparam>
        /// <param name="source"></param>
        /// <param name="page"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        public static IEnumerable<TSource> Page<TSource>(this IEnumerable<TSource> source, int pageNumber, int pageSize)
        {
            if (pageNumber == int.MinValue)
                throw new ArgumentOutOfRangeException("pageNumber", "input must be greater than Int32.MinValue");

            return source.Skip((pageNumber - 1) * pageSize).Take(pageSize);
        
        }
        /// <summary>
        /// D�coupe la collection � des sous collections de taille ChunkSize
        /// </summary>
        /// <typeparam name="TSource"></typeparam>
        /// <param name="source"></param>
        /// <param name="ChunkSize"></param>
        /// <returns></returns>
        public static List<List<TSource>> Split<TSource>(this IEnumerable<TSource> source, int chunkSize)
        {          
            return source
                 .Select((x, i) => new
                 {
                     Data = x,
                     IndexGroup = i / chunkSize
                 })
                 .GroupBy(x => x.IndexGroup, x => x.Data)
                 .Select(x => x.Select(v => v).ToList()).ToList();            
        }

    }
}
