using System;
using System.Xml.Serialization;

namespace FnacDirect.OrderPipe.Base.Model
{
    public class FidelityPointsBillingMethod : FidelityBillingMethod
    {
        public static int IdBillingMethod
        {
            get { return 65536; }
        }

        public FidelityPointsBillingMethod()
        {
            BillingMethodId = IdBillingMethod;
            Priority = 10;
            CanModifyAmountDown = false;
            CanModifyAmountUp = false;
            AdvantageCodeType = Membership.Model.Constants.AdvantageCodeType.SIEBEL_PF;
        }
    }
}