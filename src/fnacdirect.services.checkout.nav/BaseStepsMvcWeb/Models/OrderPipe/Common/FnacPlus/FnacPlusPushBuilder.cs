using FnacDirect.OrderPipe.Base.BaseModel.Model;
using FnacDirect.OrderPipe.Base.LineGroups;
using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.OrderPipe.Base.Model.ClickAndCollect;
using FnacDirect.OrderPipe.Base.Model.Pop;
using FnacDirect.OrderPipe.Base.Steps.Pop;
using FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Common.Basket;
using System.Linq;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Common.Shipping
{
    public class FnacPlusPushBuilder : IFnacPlusPushBuilder
    {
        private readonly IFnacPlusViewModelBuilder _fnacplusViewModelBuilder;
        private readonly ISwitchProvider _switchProvider;

        public FnacPlusPushBuilder(IFnacPlusViewModelBuilder fnacplusViewModelBuilder, ISwitchProvider switchProvider)
        {
            _fnacplusViewModelBuilder = fnacplusViewModelBuilder;
            _switchProvider = switchProvider;
        }

        public FnacPlusViewModel Build(PopOrderForm popOrderForm)
        {
            var fnacPlusInfos = new FnacPlusViewModel();

            var isGuest = popOrderForm.UserContextInformations.IsGuest;

            const int fnaccomSellerId = 0;

            var fnacPlusData = popOrderForm.GetPrivateData<PushFnacPlusAdherentData>("PushFnacPlusAdherentGroup", create: false);

            if (fnacPlusData == null)
            {
                return fnacPlusInfos;
            }
       
            _fnacplusViewModelBuilder.Fill(fnacPlusInfos, fnacPlusData);
            fnacPlusInfos.CanAddCardFromPopin = !fnacPlusData.HasFnacPlusCardInBasket;

            var excludedLogTypes = popOrderForm.ShippingMethodEvaluation.ExcludedLogisticTypes;

            var popShippingData = popOrderForm.GetPrivateData<PopShippingData>();
            var popShippingSellerData = popShippingData.GetCurrentShippingSellerData();

            fnacPlusInfos.IsFnacSeller = popShippingSellerData != null ? popShippingSellerData.SellerId == fnaccomSellerId : false;

            fnacPlusInfos.ShowPush =
                (fnacPlusData.IsEligibleEssaiFnacPlus
                    || fnacPlusData.IsEligiblePushRecruit)
                && !fnacPlusData.HasFnacPlusCardInBasket
                && ((popOrderForm.Path == AppliablePath.Full)
                    || (popOrderForm.Path == AppliablePath.OnePage))
                && !isGuest;

            fnacPlusInfos.IsFullMP = popOrderForm.HasOnlyMarketPlaceArticle();

            fnacPlusInfos.ShowPopin = fnacPlusInfos.ShowPush
                && !fnacPlusData.HasFnacPlusCardInBasket
                && !fnacPlusData.IsRemovedFromShippingStep
                && ((_switchProvider.IsEnabled("orderpipe.pop.shipping.fnacplus.popin") && !fnacPlusInfos.IsFullMP)
                    || (_switchProvider.IsEnabled("orderpipe.pop.shipping.fnacplus.popin.mp") && fnacPlusInfos.IsFullMP))
                && !isGuest;


            var steedFromStoreData = popOrderForm.GetPrivateData<SteedFromStoreData>(false);

            if (steedFromStoreData == null || !steedFromStoreData.StoreId.HasValue)
            {
                fnacPlusInfos.IsSteedFromStoreAvailable = false;
            }
            else
            {
                fnacPlusInfos.IsSteedFromStoreAvailable = true;
            }

            return fnacPlusInfos;
        }
    }
}
