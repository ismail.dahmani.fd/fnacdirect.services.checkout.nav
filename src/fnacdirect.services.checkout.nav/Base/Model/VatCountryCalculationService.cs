﻿using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.OrderPipe.Base.Proxy.Application;
using System.Collections.Generic;
using System.Linq;

namespace FnacDirect.OrderPipe.Base.Business.Shipping
{
    public class VatCountryCalculationService : IVatCountryCalculationService
    {
        private const int FranceCountryId = 74;
        private List<int> SpainCountries = new List<int>()
            {
                197,246,247,248
            };
        private readonly ICountryWithVatService _countryWithVatService;

        public VatCountryCalculationService(ICountryWithVatService countryWithVatService)
        {
            _countryWithVatService = countryWithVatService;
        }

        public VatCountry GetVatCountry(int localCountryId, ShippingAddress shippingAddress)
        {
            return GetVatCountry(localCountryId, null, shippingAddress);
        }

        public VatCountry GetVatCountry(int localCountryId, BillingAddress billingAddress, ShippingAddress shippingAddress)
        {
            var vatCountry = new VatCountry();

            var addressCountryId = shippingAddress?.CountryId ?? billingAddress?.CountryId ?? -1;
            if (addressCountryId == -1)
                return vatCountry;

            var shipToEurope = shippingAddress?.IsCountryUseVAT ?? billingAddress?.IsCountryUseVAT ?? false;
            var shipToFrance = addressCountryId == FranceCountryId;

            var isLocallyInSpain = SpainCountries.Any(id => id == localCountryId);
            var doNotShipToSpain = SpainCountries.All(id => id != addressCountryId);
            var legalStatus = shippingAddress?.LegalStatus ?? billingAddress?.LegalStatus;
            const int ENTREPRISE = 2;
            var spainSpecificCase = isLocallyInSpain && doNotShipToSpain && legalStatus == ENTREPRISE;

            vatCountry.UseVat = shipToEurope && !spainSpecificCase;

            if (shipToEurope)
            {
                if (shipToFrance || IsFlagIntra(addressCountryId))
                    vatCountry.VatCountryId = addressCountryId;
                else
                    vatCountry.VatCountryId = localCountryId;
            }
            else
                vatCountry.VatCountryId = null;


            return vatCountry;
        }

        private bool IsFlagIntra(int addressCountryId)
        {
            return _countryWithVatService.Get()[addressCountryId].FlagVatIntra == 1;
        }
    }
}
