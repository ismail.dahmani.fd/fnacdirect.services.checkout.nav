﻿using System;

namespace FnacDirect.OrderPipe.Base.BaseModel
{
    public class ShippingMethodEvaluationDeliveryDate
    {
        public string ReferenceGu { get; set; }

        public DateTime? MinDeliveryDate { get; set; }

        public DateTime? MaxDeliveryDate { get; set; }
    }
}
