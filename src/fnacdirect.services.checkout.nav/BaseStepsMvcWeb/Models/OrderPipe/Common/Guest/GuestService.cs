using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.OrderPipe.Base.Model.Facets;
using FnacDirect.OrderPipe.Base.Proxy.Appointment;
using FnacDirect.OrderPipe.Base.Proxy.Membership;
using System.Linq;

namespace FnacDirect.OrderPipe.Base.Business
{
    public class GuestService : IGuestService
    {
        private readonly IFormAtHomeService _formAtHomeService;
        private readonly ISwitchProvider _switchProvider;
        private readonly IAdherentCardArticleService _adherentCardArticleService;

        public GuestService(IFormAtHomeService formAtHomeService,
                            ISwitchProvider switchProvider,
                            IAdherentCardArticleService adherentCardArticleService)
        {
            _formAtHomeService = formAtHomeService;
            _switchProvider = switchProvider;
            _adherentCardArticleService = adherentCardArticleService;
        }

        public bool IsEligible(IGotLineGroups lineGroups, OPContext opContext)
        {
            if(!_switchProvider.IsEnabled("orderpipe.pop.guest"))
            {
                return false;
            }

            var eccvTypeIds = opContext.Pipe.GlobalParameters.GetAsRangeSet<int>("eccv.typeid");
            var hasECCVInBasket = lineGroups.Articles.Any(a => a.TypeId.HasValue && eccvTypeIds.Contains(a.TypeId.Value));

            var hasServiceInBasket = lineGroups.Articles.OfType<StandardArticle>().Any(a => a.Services.Any());

            var hasAdhesionCardInBasket = lineGroups.Articles.OfType<StandardArticle>().Any(a => _adherentCardArticleService.IsAdherentCard(a.ProductID.GetValueOrDefault(0)));

            var typeIdFormAtHome = _formAtHomeService.GetRelatedArticleTypeIds();
            var hasFormAtHomeInBasket = lineGroups.Articles.Any(a => a.TypeId.HasValue && typeIdFormAtHome.Contains(a.TypeId.Value));

            if (!lineGroups.HasMarketPlaceArticle()         // Ne contient pas de MP
                    && !lineGroups.HasNumericalArticle()    // Ne contient pas de Produit Numériques
                    && !lineGroups.HasDematSoftArticle()    // Ne contient pas de Démat Soft
                    && !hasECCVInBasket                     // Ne contient pas de E-CCV
                    && !hasServiceInBasket                  // Ne contient pas de Services/Assurances
                    && !hasAdhesionCardInBasket             // Ne contient pas de Carte Adhérent
                    && !hasFormAtHomeInBasket)              // Ne contient pas de Formations
            {
                return true;
            }

            return false;
        }
    }
}
