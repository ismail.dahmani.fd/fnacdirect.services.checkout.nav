using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.OrderPipe.Base.Model.Solex;
using System.Collections.Generic;

namespace FnacDirect.OrderPipe.Base.Business.Shipping
{
    public interface IAppointmentDeliveryService
    {
        void SynchronizeSelectedChoiceAccordingToSelectedDeliverySlot(IEnumerable<ShippingChoice> choices, DeliverySlotData deliverySlotData);

        HashSet<int> ShippingMethodsFly { get; }
        HashSet<int> ShippingMethodsCourier { get; }
        HashSet<int> ShippingMethods { get; }

        HashSet<int> ShippingMethodsThatOpensCalendar { get; }
        HashSet<int> ShippingMethodsPriorityOrder { get; }

        HashSet<int> AggregatedShippingMethodIds { get; }

        DeliverySlot GetDefaultSlotForShippingMethod(int mainShippingMethodId);

        DeliverySlot GetDeliverySlot(PopOrderForm popOrderForm, string partnerId, string partnerUniqueIdentifier);
        void SetSelectedDeliverySlot(PopOrderForm popOrderForm, DeliverySlot selectedDeliverySlot, int sellerId, int shippingMethodId);
    }
}
