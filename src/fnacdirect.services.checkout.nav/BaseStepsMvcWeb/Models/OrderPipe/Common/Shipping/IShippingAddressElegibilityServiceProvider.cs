namespace FnacDirect.OrderPipe.Base.Business.Shipping
{
    public interface IShippingAddressElegibilityServiceProvider
    {
        IShippingAddressElegibilityService GetShippingAddressElegibilityServiceFor(OPContext opContext);
    }
}
