﻿using System;

namespace FnacDirect.OrderPipe.Base.Business
{
    public interface IAdherentBusiness
    {
        bool IsAdherentHolderWithFnacCard(string adherentNumber, DateTime adherentBirthdate);
    }
}
