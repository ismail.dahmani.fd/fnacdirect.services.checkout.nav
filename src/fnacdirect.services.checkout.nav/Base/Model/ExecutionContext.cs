using System;
using System.Linq;
using System.Collections.Generic;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace FnacDirect.OrderPipe
{
    public class ExecutionContext : IXmlSerializable
    {
        private readonly IDictionary<string, string> _list = new Dictionary<string, string>(StringComparer.InvariantCultureIgnoreCase);
        
        public XmlSchema GetSchema()
        {
            return null;
        }

        public void Set(string name, string value)
        {
            if(!_list.ContainsKey(name))
            {
                _list.Add(name, value);
            }
        }

        public bool IsIn(string name)
        {
            return _list.Keys.Any(i => string.Compare(i, name, StringComparison.InvariantCultureIgnoreCase) == 0);
        }

        public string GetValue(string name)
        {
            return _list.FirstOrDefault(i => string.Compare(i.Key, name, StringComparison.InvariantCultureIgnoreCase) == 0).Value;
        }

        public void Remove(string name)
        {
            foreach(var key in _list.Keys.Where(i => string.Compare(i, name, StringComparison.InvariantCultureIgnoreCase) == 0).ToList())
            {
                _list.Remove(key);
            }
        }

        public void ReadXml(XmlReader reader)
        {
            _list.Clear();

            var isEmpty = reader.IsEmptyElement;

            reader.Read();

            if (isEmpty)
            {
                return;
            }

            while (reader.NodeType == XmlNodeType.Whitespace)
            {
                reader.Read();
            }

            while (reader.NodeType == XmlNodeType.Element && reader.Name == "context")
            {
                var contextName = reader["name"];
                var contextValue = reader["value"];

                _list.Add(contextName, contextValue);

                reader.ReadToNextElementOrEndElement();
            }

            if (reader.NodeType == XmlNodeType.EndElement)
            {
                reader.ReadEndElement();
            }
        }

        public void WriteXml(XmlWriter writer)
        {
            foreach (var item in _list)
            {
                writer.WriteStartElement("context");

                writer.WriteAttributeString("name", item.Key);
                writer.WriteAttributeString("value", item.Value);

                writer.WriteEndElement();
            }
        }
    }
}
