using System;
using System.Collections.Generic;
using System.Linq;
using Common.Logging;
using FnacDirect.Contracts.Online.Model;
using FnacDirect.OrderPipe.Base.BaseModel;
using FnacDirect.OrderPipe.Base.DAL;
using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.OrderPipe.Base.Model.LineGroups;
using FnacDirect.OrderPipe.Base.Model.Solex;
using FnacDirect.Technical.Framework.Web.Mvc;

namespace FnacDirect.OrderPipe.Base.Business.Quote
{
    public class OrderWithPendingPaymentBusiness : IOrderWithPendingPaymentBusiness
    {
        private readonly IApplicationContext _applicationContext;
        private readonly IArticleDetailService _articleDetailService;
        private readonly IPaymentDAL _paymentDal;
        private readonly ILog _log;
        private readonly IPricingService _pricingService;
        private readonly ISwitchProvider _switchProvider;

        public OrderWithPendingPaymentBusiness(ILog log,
            IPaymentDAL commerceDal,
            IApplicationContext applicationContext,
            IArticleDetailService articleDetailService,
            IPricingServiceProvider pricingServiceProvider,
            ISwitchProvider switchProvider)
        {
            _log = log;
            _paymentDal = commerceDal;
            _applicationContext = applicationContext;
            _articleDetailService = articleDetailService;
            _switchProvider = switchProvider;

            var marketId = applicationContext.GetSiteContext().MarketId;
            _pricingService = pricingServiceProvider.GetPricingService(marketId);
        }

        public PopOrderForm GetOrderByOrderReference(string orderReference, PopOrderForm popOrderForm)
        {
            try
            {
                _paymentDal.GetOrderByOrderReference(orderReference, popOrderForm);

                if (popOrderForm?.LineGroups == null)
                    return null;

                foreach (var lineGroup in popOrderForm.LineGroups)
                {
                    if (lineGroup == null)
                        return null;

                    SetOrderType(lineGroup);

                    AssociateServicesToArticles(lineGroup);

                    foreach (var standardArticle in lineGroup.Articles.OfType<StandardArticle>())
                        _articleDetailService.GetArticleDetail(standardArticle, _applicationContext.GetSiteContext());


                    if (!_switchProvider.IsEnabled("orderpipe.pop.taxcalculationservice.email.activation"))
                    {
                        GetArticlesPriceHt(lineGroup);
                    }

                    SetSelectedShippingMethod(popOrderForm, lineGroup);
                }
            }
            catch (Exception exception)
            {
                _log.Error("Error while retrieving quote order", exception);
            }
            return popOrderForm;
        }

        private void GetArticlesPriceHt(PopLineGroup lineGroup)
        {
            foreach (var article in lineGroup.Articles.OfType<StandardArticle>())
            {
                if (article.PriceNoVATEur == null)
                {
                    article.PriceNoVATEur = _pricingService.GetPriceHT(article.PriceUserEur.GetValueOrDefault(0),
                        article.VATRate.GetValueOrDefault(0));
                }

                if (article.EcoTaxEurNoVAT == null)
                {
                    article.EcoTaxEurNoVAT = _pricingService.GetPriceHT(article.EcoTaxEur.GetValueOrDefault(0),
                        article.VATRate.GetValueOrDefault(0));
                }

                foreach (var service in article.Services)
                {
                    service.PriceNoVATEur = _pricingService.GetPriceHT(service.PriceUserEur.GetValueOrDefault(0),
                        article.VATRate.GetValueOrDefault(0));
                    service.EcoTaxEurNoVAT = _pricingService.GetPriceHT(service.EcoTaxEur.GetValueOrDefault(0),
                        article.VATRate.GetValueOrDefault(0));

                    service.SalesInfo.StandardPrice = new Price { HT = service.PriceNoVATEur.Value };
                }
            }
        }

        private void SetSelectedShippingMethod(PopOrderForm popOrderForm, PopLineGroup lineGroup)
        {
            if (popOrderForm.ShippingMethodEvaluation.FnacCom.HasValue)
            {
                var fnacComShippingMethodEvaluation = popOrderForm.ShippingMethodEvaluation.FnacCom.Value;
                if (fnacComShippingMethodEvaluation.PostalShippingMethodEvaluationItem.HasValue)
                {
                    fnacComShippingMethodEvaluation.SelectShippingMethodEvaluationType(ShippingMethodEvaluationType
                        .Postal);
                    var postalShippingMethodEvaluationItem =
                        fnacComShippingMethodEvaluation.PostalShippingMethodEvaluationItem.Value;

                    var choices = new List<ShippingChoice>();
                    var choice = popOrderForm.LogisticLineGroups.FirstOrDefault().SelectedShippingChoice;

                    foreach (var article in lineGroup.Articles.OfType<StandardArticle>())
                    {
                        var group = choice.Groups.FirstOrDefault(g => g.Id == article.LogType);
                        SetGroupsAndParcelForArticle(article, choice, group);
                    }
                    choice.MainShippingMethodId = choice.Parcels.FirstOrDefault().Value.ShippingMethodId;
                    // Calculé sous la forme a x + b
                    choice.TotalPrice.Cost = choice.Groups.Sum(g => g.GlobalPrice.Cost)
                                           + choice.Groups.SelectMany(g => g.Items).Sum(item => (item.UnitPrice.Cost * item.Quantity));
                    choices.Add(choice);
                    postalShippingMethodEvaluationItem.Choices = choices;
                    postalShippingMethodEvaluationItem.SelectedShippingMethodId = choice.MainShippingMethodId;
                }
            }
        }

        private void SetOrderType(PopLineGroup lineGroup)
        {
            if (lineGroup.Informations.OrderType == (int)OrderInfoOrderTypeEnum.StandardPro)
                lineGroup.OrderType = OrderTypeEnum.Pro;
            else
                lineGroup.OrderType = OrderTypeEnum.FnacCom;
        }

        private void AssociateServicesToArticles(PopLineGroup lineGroup)
        {
            var articlesToDelete = new List<Service>();

            foreach (var service in lineGroup.Articles.OfType<Service>())
            {
                articlesToDelete.Add(service);

                if (service.MasterArticles.Any())
                {
                    var masterArticle = lineGroup.Articles.OfType<StandardArticle>()
                        .FirstOrDefault(a => a.OrderDetailPk == service.MasterArticles[0].OrderDetailPk);

                    if (masterArticle != null)
                        masterArticle.Services.Add(service);
                }
            }

            foreach (var serviceToDelete in articlesToDelete)
                lineGroup.Articles.Remove(serviceToDelete);
        }

        private int SetGroupsAndParcelForArticle(StandardArticle article, ShippingChoice choice, Group group)
        {
            var parcelId = -1;

            var item = new ItemInGroup
            {
                Identifier = new Identifier
                {
                    Prid = article.ProductID
                },
                Quantity = article.Quantity.GetValueOrDefault(),
                UnitPrice = new GenericPrice { Cost = article.ShipPriceDBEur.GetValueOrDefault() }
            };

            var existingParcel = choice.Parcels.FirstOrDefault(p => p.Value.ShippingMethodId == article.ShipMethod
                                                                    && p.Value.Source.ShippingDate == article.TheoreticalExpeditionDate
                                                                    && p.Value.Source.DeliveryDateMin == article.TheoreticalDeliveryDate);
            if (existingParcel.Value != null)
            {
                parcelId = existingParcel.Key;
                item.ParcelId = parcelId;
            }
            else
            {
                var parcel = new Parcel
                {
                    ShippingMethodId = article.ShipMethod.GetValueOrDefault(),
                    DelayInHours = article.ShipDelay.GetValueOrDefault(),
                    Source = new Source
                    {
                        ShippingDate = article.TheoreticalExpeditionDate.GetValueOrDefault(),
                        DeliveryDateMax = article.TheoreticalDeliveryDateMax.GetValueOrDefault(),
                        DeliveryDateMin = article.TheoreticalDeliveryDate.GetValueOrDefault(),
                        ShippingLocation = new ShippingLocation(),
                        StockLocation = new StockLocation()
                    }
                };

                parcelId = GetNextParcelId(choice);

                choice.Parcels.Add(parcelId, parcel);
                item.ParcelId = parcelId;
            }

            var items = group.Items.ToList();
            items.Add(item);
            group.Items = items;

            return parcelId;
        }

        private int GetNextParcelId(ShippingChoice choice)
        {
            var maxValue = choice.Parcels.Count > 0 ? choice.Parcels.Max(p => p.Key) : 0;
            return maxValue + 1;
        }
    }
}
