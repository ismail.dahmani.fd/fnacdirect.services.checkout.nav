﻿using FnacDirect.OrderPipe.Base.BaseModel;
using FnacDirect.OrderPipe.Base.LineGroups;

namespace FnacDirect.OrderPipe.Base.Business.LogisticLineGroups.Internals.Initializers
{
    public interface ILogisticLineGroupInitializerHelper
    {
        T Clone<T>(T item);
        
        void CopyShippingMethodEvaluation(ILineGroupHeader source, LogisticLineGroup destination, ShippingMethodEvaluation shippingMethodEvaluation);
    }
}
