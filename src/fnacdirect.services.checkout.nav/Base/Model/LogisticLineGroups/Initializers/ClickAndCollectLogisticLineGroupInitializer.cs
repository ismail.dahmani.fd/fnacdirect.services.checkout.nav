using FnacDirect.OrderPipe.Base.Business.LogisticLineGroups.Internals.Initializers;
using FnacDirect.OrderPipe.Base.Business.Shipping;
using FnacDirect.OrderPipe.Base.LineGroups;
using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.OrderPipe.Core.Services;
using FnacDirect.OrderPipe.Shipping;
using FnacDirect.Technical.Framework.Web.Mvc;
using System;
using System.Collections.Generic;

namespace FnacDirect.OrderPipe.Base.Business.LogisticLineGroups.Initializers
{
    public class ClickAndCollectLogisticLineGroupInitializer : BaseLogisticLineGroupInitializer
    {
        private readonly IApplicationContext _applicationContext;
        private readonly ILogisticLineGroupGenerator _logisticLineGroupGenerator;
        private readonly ShippingMethodBusiness _shippingMethodBusiness;
        private readonly IShippingMethodBasketServiceProvider _shippingMethodBasketServiceProvider;
        private readonly IPipeParametersProvider _pipeParametersProvider;

        public ClickAndCollectLogisticLineGroupInitializer(IApplicationContext applicationContext,
                                                           ILogisticLineGroupGenerator logisticLineGroupGenerator,
                                                           IShippingMethodBasketServiceProvider shippingMethodBasketServiceProvider,
                                                           IPipeParametersProvider pipeParametersProvider)
        {
            _applicationContext = applicationContext;
            _logisticLineGroupGenerator = logisticLineGroupGenerator;
            _shippingMethodBusiness = new ShippingMethodBusiness();
            _shippingMethodBasketServiceProvider = shippingMethodBasketServiceProvider;
            _pipeParametersProvider = pipeParametersProvider;
        }

        public override IEnumerable<ILogisticLineGroup> Initialize(IDictionary<ILineGroupHeader, IEnumerable<Article>> selectedLineGroups)
        {
            var shippingMethodEvaluation = Context.IGotShippingMethodEvaluation.ShippingMethodEvaluation;

            var output = new List<ILogisticLineGroup>();
            
            foreach (var lineGroup in selectedLineGroups.Keys)
            {
                foreach(var article in selectedLineGroups[lineGroup])
                {
                    article.SellerID = SellerIds.GetId(SellerType.ClickAndCollect);

                    var logisticLineGroup = _logisticLineGroupGenerator.Generate(lineGroup);

                    logisticLineGroup.UniqueId = Guid.NewGuid().ToString();
                    logisticLineGroup.OrderType = OrderTypeEnum.CollectInStore;
                    logisticLineGroup.Informations.OrderType = (int)OrderInfoOrderTypeEnum.RetraitMagasin;
                    logisticLineGroup.Informations.OrderStatusId = _pipeParametersProvider.Get("clickandcollect.orderstatusid", 30);
                    logisticLineGroup.Seller.SellerId =
                        lineGroup.Seller.Type == SellerType.ClickAndCollectOnly ?
                        lineGroup.Seller.SellerId : SellerIds.GetId(SellerType.ClickAndCollect);

                    logisticLineGroup.Articles = new List<Article>
                    {
                        article
                    };

                    Helper.CopyShippingMethodEvaluation(lineGroup, logisticLineGroup, shippingMethodEvaluation);

                    ResetShippingMethodChoices(logisticLineGroup);

                    output.Add(logisticLineGroup);
                }
            }

            return output;
        }

        public override IEnumerable<ILogisticLineGroup> SimulateInitialize(IDictionary<ILineGroupHeader, IEnumerable<Article>> selectedLineGroups)
        {
            var output = new List<ILogisticLineGroup>();

            foreach (var lineGroup in selectedLineGroups.Keys)
            {
                foreach (var article in selectedLineGroups[lineGroup])
                {
                    output.Add(new LogisticLineGroup());
                }
            }

            return output;
        }

        /// <summary>
        /// Reset the ShippingMethodChoice property of the logisticLineGroup, by calling _shippingMethodBusiness.GetShippingChoices()
        /// 
        /// Warning: _shippingMethodBusiness.GetShippingChoices() returns ShippingCost with MethodDetailUserCost and MethodGlobalUserCost set to 0.
        /// </summary>
        /// <param name="logisticLineGroup"></param>
        public virtual void ResetShippingMethodChoices(LogisticLineGroup logisticLineGroup)
        {
            var shippingMethodBasketService = _shippingMethodBasketServiceProvider.Get(_pipeParametersProvider);

            var shippingMethodSelection = _pipeParametersProvider.Get("config.shippingMethodSelection", string.Empty);

            var basket = shippingMethodBasketService.GetShippingMethodBasketForOrderAndAddress(Context.IGotShippingMethodEvaluation,
                                                                                               new Dictionary<ILineGroupHeader, IEnumerable<Article>>()
                                                                                               {
                                                                                                           { logisticLineGroup, logisticLineGroup.Articles }
                                                                                               },
                                                                                               logisticLineGroup.ShippingAddress);

            logisticLineGroup.ShippingMethodChoice = _shippingMethodBusiness.GetShippingChoices(basket,
                                                                                                shippingMethodSelection,
                                                                                                _applicationContext.GetLocale());

        }
    }
}
