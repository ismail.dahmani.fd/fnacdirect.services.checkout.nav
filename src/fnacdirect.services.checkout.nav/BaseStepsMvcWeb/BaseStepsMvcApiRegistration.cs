using FnacDirect.Technical.Framework.Web.Mvc;
using System.Web.Http;

namespace FnacDirect.OrderPipe.BaseMvc.Web
{
    public class BaseStepsMvcApiRegistration : WebApiRegistration
    {
        public BaseStepsMvcApiRegistration()
        {
            Configure((httpConfiguration, httpServer) =>
            {
                httpConfiguration.Routes.MapHttpBatchRoute("OrderPipe_Pop_BatchHandler",
                    "orderpipe/api/v1/pop/$batch",
                    new PopHttpBatchHandler(httpServer));

                httpConfiguration.Routes.MapHttpBatchRoute("OrderPipe_Popfdv_BatchHandler",
                    "orderpipe/api/v1/popfdv/$batch",
                    new PopFdvHttpBatchHandler(httpServer));

                httpConfiguration.Routes.MapHttpBatchRoute("OrderPipe_Payment_BatchHandler",
                    "orderpipe/api/v1/payment/$batch",
                    new PaymentHttpBatchHandler(httpServer));

                httpConfiguration.Formatters.JsonFormatter.SerializerSettings.NullValueHandling = Newtonsoft.Json.NullValueHandling.Ignore;
            });
        }
    }
}
