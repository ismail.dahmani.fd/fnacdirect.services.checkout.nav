using FnacDirect.Contracts.Online.Model;
using FnacDirect.OrderPipe.Base.LineGroups;
using FnacDirect.OrderPipe.Base.Model;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace FnacDirect.OrderPipe.Base.BaseModel
{
    /***
     *
     * TODO: Class à nettoyer:
     *      - Les méthodes et propriétés publiques et privées se baladent trop.
     *      - Certaines méthodes (dont ExcludeLogisticType()) peuvent être mieux repensés/utilisés.
     */
    public class ShippingMethodEvaluation : IXmlSerializable
    {
        public List<FreeShipping> FreeShipping { get; set; }

        private readonly IDictionary<int, ShippingMethodEvaluationGroup> _shippingMethodEvaluationGroups
            = new Dictionary<int, ShippingMethodEvaluationGroup>();


        public void Remove(int sellerId)
        {
            if (_shippingMethodEvaluationGroups.ContainsKey(sellerId))
            {
                _shippingMethodEvaluationGroups.Remove(sellerId);
            }
        }

        public Maybe<ShippingMethodEvaluationGroup> FnacCom
        {
            get
            {
                return Seller(SellerIds.GetId(LineGroups.SellerType.Fnac));
            }
        }

        public ShippingMethodEvaluation WithSeller(int sellerId, params ShippingMethodEvaluationType[] allowedItemTypes)
        {
            if (!Seller(sellerId).HasValue)
            {
                _shippingMethodEvaluationGroups.Add(sellerId, new ShippingMethodEvaluationGroup(sellerId, allowedItemTypes));
            }

            return this;
        }

        public Maybe<List<ShippingMethodEvaluationGroup>> MarketPlaces
        {
            get
            {
                var mpGroups = ShippingMethodEvaluationGroups.Where(g => SellerIds.IsMarketPlaceType(g.SellerId));
                if (mpGroups.Any())
                    return mpGroups.ToList();

                return Maybe.Empty<List<ShippingMethodEvaluationGroup>>();
            }
        }

        public Maybe<ShippingMethodEvaluationGroup> Seller(int sellerId)
        {
            if (!_shippingMethodEvaluationGroups.TryGetValue(sellerId, out var value))
                return Maybe.Empty<ShippingMethodEvaluationGroup>();

            return value;
        }

        public void Clear()
        {
            _shippingMethodEvaluationGroups.Clear();
        }

        private ShippingMethodEvaluationGroup _mainShippingMethodEvaluationGroup;

        public void InitMainShippingMethodEvaluationGroup(ShippingMethodEvaluationGroup value)
        {
            _mainShippingMethodEvaluationGroup = value;
        }

        public ShippingMethodEvaluationGroup GetMainShippingMethodEvaluationGroup()
        {
            if (_mainShippingMethodEvaluationGroup == null)
            {
                return new ShippingMethodEvaluationGroup();
            }

            return _mainShippingMethodEvaluationGroup;
        }

        public IEnumerable<ShippingMethodEvaluationGroup> ShippingMethodEvaluationGroups
        {
            get
            {
                return _shippingMethodEvaluationGroups.Values;
            }
        }
        public IEnumerable<ShippingMethodEvaluationGroup> GetGroupsOfType(ShippingMethodEvaluationType type)
        {
            return ShippingMethodEvaluationGroups.Where(s => s.SelectedShippingMethodEvaluationType == type);
        }


        public Maybe<ShippingMethodEvaluationGroup> GetShippingMethodEvaluationGroup(int? sellerId)
        {
            return Seller(sellerId.GetValueOrDefault());
        }

        /// <summary>
        /// True si tous les SgippingMethodEvaluationGroup ont une SelectedShippingMethodEvaluation spécifiée
        /// </summary>
        public bool IsAllSelectedShippingMethodEvaluationKnown
        {
            get
            {
                return _shippingMethodEvaluationGroups.All(g => g.Value.SelectedShippingMethodEvaluationType != ShippingMethodEvaluationType.Unknown);
            }
        }

        public object GetShippingMethodEvaluationGroup(object getIds)
        {
            throw new NotImplementedException();
        }

        public bool? ContainsArticlesForCollectInStore { get; set; }

        public string Uid { get; set; }

        public bool Validated { get; set; }

        private readonly IList<int> _excludedLogisticTypes
            = new List<int>();

        public IEnumerable<int> ExcludedLogisticTypes
        {
            get
            {
                return _excludedLogisticTypes;
            }
        }

        public System.Xml.Schema.XmlSchema GetSchema()
        {
            return null;
        }

        public void ReadXml(XmlReader reader)
        {
            var validated = reader["v"];

            if (validated != null && validated == "1")
            {
                Validated = true;
            }
            else
            {
                Validated = false;
            }

            var containsArticlesForCollectInStore = reader["c"];

            if (containsArticlesForCollectInStore != null)
            {
                ContainsArticlesForCollectInStore = bool.Parse(containsArticlesForCollectInStore);
            }

            var uid = reader["u"];

            if (uid != null)
            {
                Uid = uid;
            }

            var isEmpty = reader.IsEmptyElement;

            reader.Read();

            if (isEmpty)
                return;

            while (reader.NodeType == XmlNodeType.Whitespace)
                reader.Read();

            while (reader.NodeType == XmlNodeType.Element && reader.Name == "g")
            {
                var type = reader["t"];
                var allowedTypesRaw = reader["a"];

                var allowedTypes = new List<ShippingMethodEvaluationType>();

                if (!string.IsNullOrEmpty(allowedTypesRaw))
                {
                    foreach (var allowedTypeRaw in allowedTypesRaw.Split('|'))
                    {
                        if (Enum.TryParse(allowedTypeRaw, out ShippingMethodEvaluationType allowedType))
                        {
                            allowedTypes.Add(allowedType);
                        }
                    }
                }

                if (int.TryParse(type, out var shippingMethodEvaluationGroupId))
                {
                    var shippingMethodEvaluationGroup = new ShippingMethodEvaluationGroup(shippingMethodEvaluationGroupId, allowedTypes.ToArray());

                    shippingMethodEvaluationGroup.ReadXml(reader);

                    _shippingMethodEvaluationGroups.Add(shippingMethodEvaluationGroupId, shippingMethodEvaluationGroup);
                }

                reader.ReadToNextElementOrEndElement();
            }

            if (reader.NodeType == XmlNodeType.EndElement)
            {
                reader.ReadEndElement();
            }
        }

        public void WriteXml(XmlWriter writer)
        {
            writer.WriteAttributeString("v", Validated ? "1" : "0");

            if (ContainsArticlesForCollectInStore.HasValue)
            {
                writer.WriteAttributeString("c", ContainsArticlesForCollectInStore.Value.ToString(CultureInfo.InvariantCulture));
            }

            if (!string.IsNullOrEmpty(Uid))
            {
                writer.WriteAttributeString("u", Uid);
            }

            foreach (var shippingMethodEvaluationGroup in _shippingMethodEvaluationGroups)
            {
                writer.WriteStartElement("g");

                writer.WriteAttributeString("t", shippingMethodEvaluationGroup.Key.ToString());

                writer.WriteAttributeString("a", string.Join("|", shippingMethodEvaluationGroup.Value.AllowedTypes.Select(t => t.ToString())));

                shippingMethodEvaluationGroup.Value.WriteXml(writer);

                writer.WriteEndElement();
            }
        }

        public void Reset()
        {
            Validated = false;

            foreach (var shippingMethodEvaluationGroup in ShippingMethodEvaluationGroups)
            {
                shippingMethodEvaluationGroup.Reset();
            }
        }

        /// <summary>
        /// Permet de faire en plus du Reset, une réinitialisation des propriétés GlobalShippingMethodChoice et GlobalShippingMethodChoiceIsSetManually 
        /// des FnacCom ligne group.
        /// On a besoin de réinitialiser ces propriétés avant de faire l'appel à la ShippingBusiness, pour forcer ou pas l'application de la shipping rule DefaultShippingMode
        /// </summary>
        /// <param name="orderForm"></param>
        public void ResetFull(BaseOF orderForm)
        {
            Reset();

            foreach (var lg in orderForm.LineGroups.Where(lineGroup => lineGroup.HasFnacComArticle))
            {
                lg.GlobalShippingMethodChoice = null;
                lg.GlobalShippingMethodChoiceIsSetManually = false;
            }
        }

        public int GetGlobalShippingMethodChoiceFor(string lineGroupIdentifier)
        {
            foreach (var shippingMethodEvaluationGroup in ShippingMethodEvaluationGroups)
            {
                var shippingMethods = shippingMethodEvaluationGroup.GetShippingMethodFor(lineGroupIdentifier).ToList();

                if (shippingMethods.Any())
                {
                    return shippingMethods.First();
                }
            }

            return 0;
        }

        /// <summary>
        /// Permet de flagger un type logisitic comme étant exclu.
        /// Il pourra être évalué, mais il ne sera pas retourné comme élémént à part entière via Single ou un ShippingMethodEvaluationItem
        /// </summary>
        /// <param name="logisticType"></param>
        public void ExcludeLogisticType(int logisticType)
        {
            if (_excludedLogisticTypes.Contains(logisticType))
            {
                return;
            }

            _excludedLogisticTypes.Add(logisticType);
        }

        public override string ToString()
        {
            var shippingMethodEvaluationGroupsBuilder = new StringBuilder();

            foreach (var shippingMethodEvaluationGroup in ShippingMethodEvaluationGroups)
            {
                shippingMethodEvaluationGroupsBuilder.AppendLine(shippingMethodEvaluationGroup.ToString());
            }
            return shippingMethodEvaluationGroupsBuilder.ToString();
        }
    }
}
