using FnacDirect.Contracts.Online.Model;
using FnacDirect.OrderPipe.FDV.Model;


namespace FnacDirect.OrderPipe.Base.Model.FDV
{
    public class ShopInfosData : GroupData
    {
        public SalesmanCM Shop { get; set; }
        public FnacStore.Model.FnacStore Store { get; set; }
        public SellerAuthenticationResult RebateValidator { get; set; }
        public ShopAddress ShopAddress { get; set; }
        public bool IsVendorAuthenticationNotRequired => Store?.HasBlockRegisterControl ?? false;
        public int LegalEntityId => Store?.LegalEntityId ?? -1;
        public string RefUG => Store?.RefUG.ToString("D4");
    }
}
