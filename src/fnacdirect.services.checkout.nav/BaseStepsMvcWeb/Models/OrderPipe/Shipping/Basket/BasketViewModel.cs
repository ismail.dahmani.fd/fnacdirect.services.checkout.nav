using System.Collections.Generic;
using FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Common.Basket;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Shipping
{
    public class BasketViewModel
    {
        private readonly List<BasketItemViewModel> _basketItemViewModels
         = new List<BasketItemViewModel>();

        public IReadOnlyCollection<BasketItemViewModel> Items
        {
            get
            {
                return _basketItemViewModels.AsReadOnly();
            }
        }

        public BasketViewModel With(params BasketItemViewModel[] basketItemViewModels)
        {
            _basketItemViewModels.AddRange(basketItemViewModels);

            return this;
        }

        public BasketViewModel()
        {

        }
        public OrderExternalReferenceViewModel OrderExternalReference { get; set; }

        public bool DisplayBirthDatePopin { get; set; }
    }
}
