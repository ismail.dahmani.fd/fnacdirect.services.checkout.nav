﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using FnacDirect.Contracts.Online.Model;
using FnacDirect.Customer.Model;
using FnacDirect.OrderPipe.Base.Model.Facets;
using FnacDirect.OrderPipe.Config;

namespace FnacDirect.OrderPipe.Base.Business.Payment
{
    public interface IPaymentSelectionBusinessProvider
    {
        IPaymentSelectionBusiness GetPaymentSelectionBusinessInstance();
    }
}
