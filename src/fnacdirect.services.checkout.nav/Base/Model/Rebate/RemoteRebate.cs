using System;

namespace FnacDirect.OrderPipe.Base.Model.FDV.Rebate
{
    public class RemoteRebate
    {
        public int Id { get; set; }

        public DateTime CreationDate { get; set; }

        public int ShopId { get; set; }

        public RemoteRebateStatus Status { get; set; }

        public string Content { get; set; }

        public string RespComment { get; set; }

        public string RespMatricule { get; set; }

        public string SellerComment { get; set; }

        public string SellerMatricule { get; set; }

        public string SellerPoste { get; set; }
    }
}
