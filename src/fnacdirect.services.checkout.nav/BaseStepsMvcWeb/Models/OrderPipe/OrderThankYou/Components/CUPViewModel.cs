﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.OrderThankYou
{
    public class CupViewModel
    {
        public string FirstName { get; set; }

        public string CupUrl { get; set; }

        public bool HasCupAccount { get; set; }

        public bool HasFinarefAccount { get; set; }

        public bool IsValidAdherent { get; set; }
    }
}
