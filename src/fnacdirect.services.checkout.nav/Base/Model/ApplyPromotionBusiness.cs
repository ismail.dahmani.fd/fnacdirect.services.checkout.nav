using FnacDirect.Contracts.Online.Model;
using FnacDirect.OrderPipe.Base.BaseModel.Model;
using FnacDirect.OrderPipe.Base.Business;
using FnacDirect.OrderPipe.Base.Business.Helpers;
using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.OrderPipe.Base.Model.Facets;
using System;
using System.Collections.Generic;
using System.Linq;
using Promotion = FnacDirect.OrderPipe.Base.Model.Promotion;

namespace FnacDirect.OrderPipe.Base.Steps.PriceDispo
{
    public class ApplyPromotionBusiness
    {
        private readonly ShoppingCart _shoppingCart;
        private readonly bool _useMaxQuantityAllowedField;

        public ApplyPromotionBusiness(ShoppingCart shoppingCart, ISwitchProvider switchProvider)
        {
            _shoppingCart = shoppingCart;
            _useMaxQuantityAllowedField = switchProvider.IsEnabled("orderpipe.pop.pomcore.usemaxquantityallowedfield");
        }


        public bool ComputeArticleAndGetRewindStatus(StandardArticle standardArticle, IGotLineGroups iContainsLineGroups = null)
        {
            // Extract line item from shopping cart
            var currentline = _shoppingCart.LineItems.FirstOrDefault(i => i.Reference.PRID.Value == standardArticle.ProductID.Value);

            if (currentline == null)
            {
                return false;
            }

            /*
             * Note sur le pricer:
             *      - AvailableStock = Stock disponible du produit.
             *      - LimitedQuantity = Quantité maximum imposé.
             *              * C'est souvent utilisé dans le cas d'une promotion,
             *                  lorsque nous souhaitons qu'un utilisateur ne commande qu'une certain quantité au maximum.
             *              * Cette quantité est, en général, inférieur au stock du produit.
            */
            int? limitedQuantityAllowed = null;

            if (_useMaxQuantityAllowedField)
            {
                limitedQuantityAllowed = currentline.SalesInfo.HasLimitedQuantity ? currentline.SalesInfo.LimitedQuantity.MaxQuantityAllowed : (int?)null;

                if (limitedQuantityAllowed < 0)
                {
                    limitedQuantityAllowed = null;
                }

                standardArticle.AvailableStockToDisplay = currentline.AvailableStock.GetValueOrDefault();
                standardArticle.HasMaxQuantityInBasket = currentline.Quantity >= standardArticle.AvailableStockToDisplay;

                if (currentline.SalesInfo.HasLimitedQuantity && limitedQuantityAllowed.HasValue)
                {
                    if (currentline.AvailableStock > limitedQuantityAllowed)
                    {
                        currentline.AvailableStock = limitedQuantityAllowed;
                    }

                    if (standardArticle.Quantity > limitedQuantityAllowed)
                    {
                        standardArticle.Quantity = limitedQuantityAllowed;
                    }

                    standardArticle.HasLimitedQuantity = true;
                    standardArticle.MaxQuantityAllowed = limitedQuantityAllowed.GetValueOrDefault();

                    standardArticle.HasMaxQuantityInBasket = limitedQuantityAllowed.HasValue
                        && (currentline.Quantity >= limitedQuantityAllowed.Value || standardArticle.Quantity >= limitedQuantityAllowed.Value);
                }
            }
            else
            {
                int? limitedQty = null;
                int? limitedQtyInBasket = null;
                standardArticle.AvailableStockToDisplay = currentline.AvailableStock.GetValueOrDefault();
                if (currentline.SalesInfo.HasLimitedQuantity && currentline.SalesInfo.LimitedQuantity != null)
                {
                    limitedQty = currentline.SalesInfo.LimitedQuantity.StockCount;
                    if (currentline.AvailableStock > limitedQty)
                    {
                        currentline.AvailableStock = limitedQty;
                    }
                }

                if (currentline.SalesInfo.LimitedQuantityInBasket != null)
                {
                    limitedQtyInBasket = currentline.SalesInfo.LimitedQuantityInBasket.ArticleQuantityMax;
                    if (limitedQty.HasValue)
                    {
                        // Cumul de limitedQuantity
                        currentline.AvailableStock = Math.Min(limitedQty.Value, limitedQtyInBasket.Value);
                        if (limitedQtyInBasket.Value < limitedQty.Value)
                        {
                            standardArticle.HasMaxQuantityInBasket = true;
                            standardArticle.HasOPS = false;
                        }
                    }
                    else
                    {
                        // LimitedQuantityInBasket
                        currentline.AvailableStock = limitedQtyInBasket;
                        standardArticle.HasMaxQuantityInBasket = true;
                        standardArticle.Quantity = standardArticle.Quantity > limitedQtyInBasket.Value ? limitedQtyInBasket : standardArticle.Quantity;

                        if (iContainsLineGroups != null)
                        {
                            if (standardArticle.Quantity == 0 || standardArticle.Quantity == null)
                            {
                                var lineGroup = iContainsLineGroups.LineGroups.FirstOrDefault(l => l.Articles.Contains(standardArticle));
                                if (lineGroup != null)
                                {
                                    lineGroup.Articles.Remove(standardArticle);
                                }
                                iContainsLineGroups.DeletedArticles.Add(standardArticle);
                                iContainsLineGroups.ArticlesModified = true;
                                iContainsLineGroups.AddErrorMessage("MaxCompte", "orderpipe.pop.basket.maxAccount.quantityexceeded", standardArticle.Title);
                                return false;
                            }
                        }
                    }
                }

                limitedQuantityAllowed = limitedQtyInBasket;
            }

            if (iContainsLineGroups != null && ModifyExceededQuantity(iContainsLineGroups, limitedQuantityAllowed, standardArticle, currentline))
            {
                return true;
            }

            InnerComputeArticle(currentline, standardArticle);

            standardArticle.Promotions.Clear();

            if (currentline.SalesInfo.EcoTaxAmount.TTC != null)
            {
                if (currentline.SalesInfo.EcoTaxAmount.HT == null)
                {
                    throw new Exception($"Incohérence au niveau des données du PRID {currentline.Reference.PRID} : SalesInfo.EcoTaxAmount.TTC renseigné mais HT null (TVA non paramétrée ?)");
                }

                standardArticle.EcoTaxEur = currentline.SalesInfo.EcoTaxAmount.TTC;
                standardArticle.EcoTaxEurNoVAT = currentline.SalesInfo.EcoTaxAmount.HT;
                standardArticle.EcoTaxCode = currentline.SalesInfo.EcoTaxCode;
            }

            if (standardArticle.HasOPS.GetValueOrDefault() && currentline.SalesInfo.LimitedQuantity != null)
            {
                standardArticle.OPSName = currentline.SalesInfo.LimitedQuantity.Code;
                standardArticle.OPShasFnacOffer = currentline.SalesInfo.LimitedQuantity.IsFnacOffer;
                standardArticle.OPSIsValid = currentline.SalesInfo.LimitedQuantity.IsValid;
            }

            standardArticle.HasOPS = currentline.SalesInfo.HasLimitedQuantity;
            standardArticle.TypePromoBO = currentline.SalesInfo.ReducedPrice.TypePromoBO;
            standardArticle.CodePromoBO = currentline.SalesInfo.ReducedPrice.CodePromoBO;

            if (currentline.SalesInfo.ReducedPrice.PromotionList != null)
            {
                foreach (var promo in currentline.SalesInfo.ReducedPrice.PromotionList)
                {
                    if (!standardArticle.Promotions.Any(p => p.Code == promo.Code))
                        AddPromotion(standardArticle, promo, currentline);
                }
            }

            if (currentline.SalesInfo.ReducedDifferedPrice?.PromotionList != null)
            {
                foreach (var promo in currentline.SalesInfo.ReducedDifferedPrice.PromotionList)
                {
                    if (standardArticle.Promotions.All(p => p.Code != promo.Code))
                        AddPromotion(standardArticle, promo, currentline);
                }
            }

            foreach (var serviceItem in standardArticle.Services)
            {
                ComputeService(_shoppingCart, serviceItem);
            }

            if (currentline.OfferId.HasValue)
            {
                standardArticle.SupplySource = new SupplySource
                {
                    OfferId = currentline.OfferId,
                    OfferReference = currentline.OfferReference,
                    PreparationTimeInDays = currentline.Availability.PreparationTimeInDays,
                };
            }



            return false;
        }

        private bool ModifyExceededQuantity(IGotLineGroups iContainsLineGroups, int? maxQuantity, StandardArticle standardArticle, ShoppingCartLineItem currentline)
        {
            maxQuantity = maxQuantity ?? currentline.AvailableStock ?? 0 ;
            var lg = iContainsLineGroups?.LineGroups.SingleOrDefault(s => s.Articles.Any(a => a is StandardArticle && a.ID == standardArticle.ID));
            var seller = lg?.Seller;

            if (lg != null && !lg.IsTemporary &&
                (seller == null || seller.SellerId >= 0) // Vérification article non-FnacShop                    
                && currentline.AvailableStock >= 1 && !iContainsLineGroups.PrivateData.Contains(OPConstantes.DP_FloatingBasketExceedQty))
            {
                int? newQuantity = null;
                if (!currentline.Availability.IsAvailable && (!currentline.AvailableStock.HasValue || currentline.AvailableStock == 0) )
                {
                    newQuantity = 0;
                }
                else if (maxQuantity.HasValue && standardArticle.Quantity > maxQuantity.Value)
                {
                    newQuantity = currentline.AvailableStock.Value;
                }

                if (newQuantity.HasValue && BasketQuantity.ModifyExceededQuantityArticleAndGetRewindStatus(iContainsLineGroups, standardArticle, newQuantity.Value, true))
                {
                    return true;
                }
            }

            return false;
        }

        public void ComputeArticle(MarketPlaceArticle marketPlaceArticle)
        {
            // Extract line item from shopping cart
            var currentline = _shoppingCart.LineItems.FirstOrDefault(i => i.OfferReference == marketPlaceArticle.Offer.Reference);

            if (currentline == null)
            {
                return;
            }

            InnerComputeArticle(currentline, marketPlaceArticle);

            if (marketPlaceArticle.ShippingPromotionRules != null)
            {
                foreach (var shippingPromotionRule in marketPlaceArticle.ShippingPromotionRules)
                {
                    shippingPromotionRule.LogisticFamily = new List<int>() { marketPlaceArticle.LogType.GetValueOrDefault() };
                }
            }

            marketPlaceArticle.Promotions.Clear();

            if (currentline.SalesInfo.ReducedPrice.Promotion != null)
                marketPlaceArticle.Promotions.Add(
                    SetPromotion(currentline.SalesInfo.ReducedPrice.Promotion));

            if (currentline.SalesInfo.ReducedDifferedPrice?.Promotion != null)
                marketPlaceArticle.Promotions.Add(
                    SetPromotion(currentline.SalesInfo.ReducedDifferedPrice.Promotion));
 
        }

        private void InnerComputeArticle(ShoppingCartLineItem currentline, BaseArticle article)
        {
            article.ShippingPromotionRules.Clear();
            if (currentline.ListShippingPromotionRules != null)
            {
                article.ShippingPromotionRules.AddRange(currentline.ListShippingPromotionRules);
            }


            ApplyShoppingCartLineItem(article, currentline);
        }

        protected internal void ComputeService(ShoppingCart shoppingCart, StandardArticle standardArticle)
        {
            var currentservice = shoppingCart.LineItems.FirstOrDefault(i => i.Reference.PRID.Value == standardArticle.ProductID.Value);
            if (currentservice == null)
                return;

            ApplyShoppingCartLineItem(standardArticle, currentservice);

            if (currentservice.SalesInfo.ReducedPrice.PromotionList != null)
            {
                foreach (var promo in currentservice.SalesInfo.ReducedPrice.PromotionList)
                {
                    AddPromotion(standardArticle, promo, currentservice);
                }
            }
        }

        public virtual void ApplyShoppingCartLineItem(BaseArticle article, ShoppingCartLineItem shoppingCartLineItem)
        {
            article.AvailabilityId = shoppingCartLineItem.Availability.ID;
            article.AvailabilityLabel = shoppingCartLineItem.Availability.Label;
            article.IsAvailable = shoppingCartLineItem.Availability.IsAvailable;
            article.ArticleDelay = shoppingCartLineItem.Availability.Delay;

            if (article is StandardArticle standardArticle)
            {
                standardArticle.Availability = shoppingCartLineItem.Availability.IsAvailable ? StockAvailability.Central : StockAvailability.None;
                standardArticle.AvailableStock = shoppingCartLineItem.AvailableStock.GetValueOrDefault(0);

                if (standardArticle.AvailabilityId == (int)AvailabilityEnum.PreOrderRestockingPE
                    || standardArticle.AvailabilityId == (int)AvailabilityEnum.PreOrderRestockingPT)
                {
                    if (standardArticle.ArticleDTO.ExtendedProperties.RestockingDate.HasValue)
                        standardArticle.AnnouncementDate = article.ArticleDTO.ExtendedProperties.RestockingDate.Value;
                }
            }

            ApplySalesInfo(article, shoppingCartLineItem.SalesInfo);
        }

        protected internal void ApplySalesInfo(BaseArticle standardArticle, SalesInfo salesInfo)
        {
            standardArticle.SalesInfo = salesInfo;
            standardArticle.VATId = salesInfo.VATId;
            standardArticle.PriceNoVATEur = PriceHelpers.GetRoundedPrice(salesInfo.ReducedPrice.HT);
            standardArticle.PriceUserEur = PriceHelpers.GetRoundedPrice(salesInfo.ReducedPrice.TTC);
            standardArticle.PriceDBEur = PriceHelpers.GetRoundedPrice(salesInfo.StandardPrice.TTC);
            standardArticle.PriceDBEurNoVAT = PriceHelpers.GetRoundedPrice(salesInfo.StandardPrice.HT);
            standardArticle.PriceRealEur = PriceHelpers.GetRoundedPrice(salesInfo.PublicPrice.TTC);
        }

        public virtual void AddPromotion(StandardArticle pArticle, FnacDirect.Contracts.Online.Model.Promotion pPricerPromo, ShoppingCartLineItem pCurrentLine)
        {
            pArticle.Promotions.Add(SetPromotion(pPricerPromo));
        }

        /// <summary>
        /// Retourne une FnacDirect.OrderPipe.Base.Model.Promotion à partir d'une FnacDirect.Contracts.Online.Model.Promotion
        /// </summary>
        /// <param name="pricerPromo"></param>
        /// <param name="freeArticleId"></param>
        /// <returns></returns>
        public Promotion SetPromotion(FnacDirect.Contracts.Online.Model.Promotion pricerPromo, int freeArticleId = -1)
        {
            var promotion = new Promotion
            {
                Code = pricerPromo.Code,
                AdvantageCode = pricerPromo.AdvantageCode,
                DisplayImage = pricerPromo.ShoppingCartBanner,
                ConditionsPage = pricerPromo.ConditionsUrl,
                DisplayText = pricerPromo.ShoppingCartText,
                DescriptionUser = pricerPromo.ArticleText,
                Banner = pricerPromo.ArticleBanner,
                Type = (int)pricerPromo.Type,
                Cumulable = pricerPromo.ExtendedProperties.IsCumulable,
                DiscountType = (int)pricerPromo.ExtendedProperties.DiscountType,
                IsNotSameCriteria = pricerPromo.ExtendedProperties.IsNotSameCriteria,
                HasThreshold = pricerPromo.ExtendedProperties.HasThreshold,
                Usage = (int)pricerPromo.Usage,
                DiscountEur = pricerPromo.ExtendedProperties.DiscountAmount,
                DisplayTemplate = (int)pricerPromo.DisplayTemplate,
                Rebate = pricerPromo.Rebate
            };

            if (freeArticleId != -1)
            {
                promotion.FreeArtId = freeArticleId;
            }

            return promotion;
        }

        public virtual bool CanApplyPromotion(StandardArticle article)
        {
            throw new NotImplementedException();
        }
    }
}
