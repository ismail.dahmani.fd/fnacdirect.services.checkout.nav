using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.OrderPipe.Base.Proxy.Membership;
using FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Basket.Children;
using FnacDirect.OrderPipe.BaseMvc.Web.Services.Pop;
using FnacDirect.Technical.Framework.Web;
using FnacDirect.Technical.Framework.Web.Configuration;
using FnacDirect.Technical.Framework.Web.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using FnacDirect.Contracts.Online.Model;
using FnacDirect.OrderPipe.Base.Model.LineGroups;
using FnacDirect.OrderPipe.Base.BaseModel;
using FnacDirect.OrderPipe.Base.Business.Shipping;
using FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Basket;
using FnacDirect.Technical.Framework.Utils;
using FnacDirect.OrderPipe.Base.Proxy.Appointment;
using FnacDirect.OrderPipe.Base.BaseModel.Model;

namespace FnacDirect.OrderPipe.BaseMvc.Web.CrossSellBasket
{
    public class CrossSellBasketViewModelBuilder : ICrossSellBasketViewModelBuilder
    {
        private readonly IArticleImagePathService _articleImagePathService;
        private readonly IApplicationContext _applicationContext;
        private readonly string _orderPipeUrl;
        private readonly ICompositeCrossSellRichBasketItemChildrenViewModelBuilder _compositeCrossSellRichBasketItemChildrenViewModelBuilder;
        private readonly IAdherentCardArticleService _adherentCardArticleService;
        private readonly ISwitchProvider _switchProvider;
        private readonly ISolexBusiness _solexBusiness;
        private readonly RangeSet<int> _appointmentArticleTypeIds;
        private readonly int _donationTypeId;
        private readonly int _forcedMaxQuantity;
        private const string DefaultForcedMaxQuantity = "999";

        public CrossSellBasketViewModelBuilder(IArticleImagePathService articleImagePathService,
                                               IUrlManager urlManager,
                                               IApplicationContext applicationContext,
                                               ICompositeCrossSellRichBasketItemChildrenViewModelBuilder compositeCrossSellRichBasketItemChildrenViewModelBuilder,
                                               IAdherentCardArticleService adherentCardArticleService,
                                               ISwitchProvider switchProvider,
                                               ISolexBusiness solexBusiness,
                                               IAppointmentService appointmentService)
        {
            _articleImagePathService = articleImagePathService;
            _orderPipeUrl = urlManager.Sites.OrderPipe.GetPage("Root").DefaultUrl.ToString();
            _applicationContext = applicationContext;
            _compositeCrossSellRichBasketItemChildrenViewModelBuilder = compositeCrossSellRichBasketItemChildrenViewModelBuilder;
            _adherentCardArticleService = adherentCardArticleService;
            _switchProvider = switchProvider;
            _solexBusiness = solexBusiness;
            _appointmentArticleTypeIds = appointmentService.GetAppointmentArticleTypeIds();
            _donationTypeId = int.Parse(_applicationContext.GetAppSetting("DonationTypeId"));
            _forcedMaxQuantity = int.Parse(_applicationContext.GetAppSetting("ForcedMaxQuantity") ?? DefaultForcedMaxQuantity);
        }

        public EmptyCrossSellBasketViewModel Build(IEnumerable<CrossSellBasketItemInput> basketItems, PopOrderForm orderForm)
        {
            var errorMessages = new List<string>();
            var aboIlliData = orderForm.GetPrivateData<AboIlliData>("AboIlliGroup");
            var isFnacPro = _applicationContext.GetSiteId() == (int)FnacSites.FNACPRO;

            //vérifier si on a un message d'erreur de type Max Articles dans le basket
            var errorMaxCompte = orderForm.PipeMessages?.Category("MaxCompte");
            if (errorMaxCompte.Any())
            {
                errorMaxCompte.ToList().ForEach(e => errorMessages.Add(_applicationContext.GetMessage(e.RessourceId, e.Parameters != null ? e.Parameters.FirstOrDefault() : "")));
            }            

            if (!basketItems.Any())
            {
                var viewModel = new EmptyCrossSellBasketViewModel(_orderPipeUrl)
                {
                    IsFnacPro = isFnacPro
                };
                viewModel.ErrorMessages.AddRange(errorMessages);

                return viewModel;
            }

            var crossSellBasketItemViewModels = new List<CrossSellBasketItemViewModel>();

            Func<BaseArticle, bool> hasMachingFnacComProduct = p => basketItems.Any(i => (i.ProductId == p.ProductID.GetValueOrDefault() || i.MasterProductId == p.ProductID.GetValueOrDefault()) && i.Offer == Guid.Empty);
            Func<MarketPlaceArticle, bool> hasMatchingMarketplaceProduct = p => basketItems.Any(i => i.ProductId == p.ProductID.GetValueOrDefault() && i.Offer == p.Offer.Reference);

            BasketItemReferenceViewModel mainProductViewModel = null;

            var mainBasketItem = basketItems.FirstOrDefault();

            if (mainBasketItem != null)
            {
                if (mainBasketItem.Offer == Guid.Empty)
                {
                    if (mainBasketItem.MasterProductId > 0)
                    {
                        var matching = orderForm.LineGroups.GetArticles()
                                                      .OfType<StandardArticle>()
                                                      .Where(a => a.ProductID.GetValueOrDefault(0) == mainBasketItem.MasterProductId)
                                                      .SelectMany(a => a.Services)
                                                      .FirstOrDefault(s => s.ProductID.GetValueOrDefault(0) == mainBasketItem.ProductId);

                        if (matching != null)
                        {
                            mainProductViewModel = new BasketItemReferenceViewModel(matching.Referentiel, matching.ProductID.GetValueOrDefault(0));
                        }
                    }
                    else
                    {
                        var matching = orderForm.LineGroups.GetArticles().OfType<StandardArticle>().FirstOrDefault(a => a.ProductID.GetValueOrDefault(0) == mainBasketItem.ProductId);

                        if (matching != null)
                        {
                            mainProductViewModel = new BasketItemReferenceViewModel(matching.Referentiel, matching.ProductID.GetValueOrDefault(0));
                        }
                    }
                }
                else
                {
                    var matching = orderForm.LineGroups.GetArticles().OfType<MarketPlaceArticle>().FirstOrDefault(a => a.ProductID.GetValueOrDefault(0) == mainBasketItem.ProductId
                                                                                        && a.Offer.Reference.ToString().Equals(mainBasketItem.Offer.ToString(), StringComparison.InvariantCultureIgnoreCase));

                    if (matching != null)
                    {
                        mainProductViewModel = new BasketItemReferenceViewModel(matching.Referentiel, matching.ProductID.GetValueOrDefault(0));
                    }
                }

                if(mainProductViewModel != null)
                {
                    mainProductViewModel.MasterProductId = mainBasketItem.MasterProductId;
                }
                
            }
            
            var crossSellBasketItemViewModelTemp = new List<CrossSellBasketItemViewModel>();
            foreach (var article in orderForm.LineGroups.GetArticles().OfType<BaseArticle>())
            {
                var hasMatchingArticleInInput = false;

                if (article is MarketPlaceArticle)
                {
                    hasMatchingArticleInInput = hasMatchingMarketplaceProduct(article as MarketPlaceArticle);
                }
                else
                {
                    hasMatchingArticleInInput = hasMachingFnacComProduct(article);
                }

                if (hasMatchingArticleInInput)
                {
                    var crossSellBasketItemViewModel = new CrossSellBasketItemViewModel
                    {
                        Quantity = new QuantityViewModel(),
                        ProductId = article.ProductID.GetValueOrDefault(),
                        ImagePath = _articleImagePathService.BuildPath(article),
                        Title = article.Title
                    };
                    
                    if (article is MarketPlaceArticle)
                    {
                        var marketPlaceArticle = article as MarketPlaceArticle;
                        crossSellBasketItemViewModels.Add(crossSellBasketItemViewModel);
                        crossSellBasketItemViewModel.OfferId = marketPlaceArticle.Offer.Reference;

                        crossSellBasketItemViewModel.Quantity.HasMax = true;
                        crossSellBasketItemViewModel.Quantity.Max = marketPlaceArticle.AvailableQuantity;
                        crossSellBasketItemViewModel.Quantity.Count = mainBasketItem.Quantity;
                    }

                    if (article is StandardArticle)
                    {
                        var standardArticle = article as StandardArticle;
                                                
                        if (!standardArticle.IsAdhesionCard)
                        {
                            crossSellBasketItemViewModel.Quantity.Count = mainBasketItem.Quantity;
                            crossSellBasketItemViewModels.Add(crossSellBasketItemViewModel);
                        }
                        else
                        {
                            crossSellBasketItemViewModel.Quantity.Count = 1;
                            crossSellBasketItemViewModelTemp.Add(crossSellBasketItemViewModel);
                        }

                        if (standardArticle.Hidden)
                        {
                            continue;
                        }

                        // les articles qui n'ont pas de gestion stock ou qui sont en précommandes ont une quantité max de 999
                        crossSellBasketItemViewModel.Quantity.Max = 999;

                        if (!(article.TypeId.HasValue && aboIlliData.TypeArtIds.Contains(article.TypeId.Value))
                           && !standardArticle.IsAdhesionCard && article.TypeId != _donationTypeId)
                        {
                            crossSellBasketItemViewModel.Availability = GetAvailability(standardArticle);

                            if (crossSellBasketItemViewModel.Availability.HasQuantityMax && standardArticle.AvailableStock > 0)
                            {
                                crossSellBasketItemViewModel.Quantity.HasMax = true;
                                crossSellBasketItemViewModel.Quantity.Max = standardArticle.AvailableStock;
                            }
                            else if (standardArticle.Availability == StockAvailability.ClickAndCollectOnly && orderForm is PopOrderForm)
                            {
                                var popOrderForm = orderForm as PopOrderForm;
                                crossSellBasketItemViewModel.Quantity.Max = GetStomQuantity(popOrderForm, standardArticle);
                            }
                        }



                        if (basketItems.Count() == 1)
                        {
                            crossSellBasketItemViewModel.CrossSellRichItemChildrenViewModels = _compositeCrossSellRichBasketItemChildrenViewModelBuilder.Build(standardArticle);
                        }

                        foreach (var service in standardArticle.Services)
                        {
                            var serviceBasketItem = basketItems.FirstOrDefault(s => s.ProductId == service.ProductID.GetValueOrDefault());

                            if (serviceBasketItem != null)
                            {
                                var serviceCrossSellBasketItemViewModel = new CrossSellBasketItemViewModel
                                {
                                    ImagePath = _articleImagePathService.BuildPath(service),
                                    Title = service.Title
                                };

                                crossSellBasketItemViewModels.Add(serviceCrossSellBasketItemViewModel);
                            }
                        }
                    }

                    crossSellBasketItemViewModel.Quantity.Max = Math.Min(crossSellBasketItemViewModel.Quantity.Max, _forcedMaxQuantity);

                    //Construire l'ItemViewModel de l'OPC Deezer
                    if (_switchProvider.IsEnabled("orderpipe.pop.promo.activatedeezer") && !crossSellBasketItemViewModelTemp.Any(c => c.ProductId == 0))
                    {
                        if (article.SalesInfo?.Promotions != null && article.SalesInfo.Promotions.Count > 0)
                        {
                            if (article.SalesInfo.Promotions.Any(p => p.ShoppingCartText != null && p.ShoppingCartText.Contains(Constants.ShoppingCardDeezer)))
                            {
                                var promotion = article.SalesInfo.Promotions.FirstOrDefault(p => p.DisplayTemplate == PromotionTemplate.RefundOfferPartner && p.ShoppingCartText != null && p.ShoppingCartText.Contains(Constants.ShoppingCardDeezer));
                                if (promotion != null)
                                {
                                    var crossSellBasketItemViewModelDeezer = BuildDeezerModel(promotion);
                                    crossSellBasketItemViewModelTemp.Add(crossSellBasketItemViewModelDeezer);
                                }
                            }

                        }
                    }
                }
            }

            //ajouter la carte fnac plus et les OPCs au viewModel 
            if (crossSellBasketItemViewModelTemp.Count() > 0)
            {
                crossSellBasketItemViewModels.AddRange(crossSellBasketItemViewModelTemp);
            }

            var allProducts = new List<BasketItemReferenceViewModel>();

            foreach (var product in orderForm.LineGroups.GetArticles().OfType<BaseArticle>())
            {
                var services = new List<ReferenceViewModel>();

                if (product is StandardArticle)
                {
                    var standardArticle = product as StandardArticle;

                    if (standardArticle.Services != null
                        && standardArticle.Services.Any())
                    {
                        services = standardArticle.Services.Select(s => new ReferenceViewModel(s.Referentiel, s.ProductID.GetValueOrDefault(0))).ToList();
                    }
                }

                allProducts.Add(new BasketItemReferenceViewModel(product.Referentiel, product.ProductID.GetValueOrDefault(0), services.ToArray()));
            }

            var deletedProducts = new List<CrossSellBasketItemViewModel>();
            foreach (var article in orderForm.DeletedArticles.OfType<BaseArticle>())
            {
                var hasMatchingArticleInInput = false;

                if (article is MarketPlaceArticle)
                {
                    hasMatchingArticleInInput = hasMatchingMarketplaceProduct(article as MarketPlaceArticle);
                }
                else
                {
                    hasMatchingArticleInInput = hasMachingFnacComProduct(article);
                }

                if (hasMatchingArticleInInput)
                {
                    var crossSellBasketItemViewModel = new CrossSellBasketItemViewModel
                    {
                        ImagePath = _articleImagePathService.BuildPath(article),
                        Title = article.Title
                    };

                    deletedProducts.Add(crossSellBasketItemViewModel);
                }
            }

            var crossSellBasketViewModel = new CrossSellBasketViewModel(_orderPipeUrl, mainProductViewModel, crossSellBasketItemViewModels, allProducts, deletedProducts)
            {
                IsFnacPro = isFnacPro
            };

            var errorMaxArticlesInBasket = orderForm.PipeMessages?.Category("MaxArticlesInBasket");
            if (errorMaxArticlesInBasket.Any())
            {
                errorMaxArticlesInBasket.ToList().ForEach(e => errorMessages.Add(_applicationContext.GetMessage(e.RessourceId, e.Parameters != null ? e.Parameters.FirstOrDefault() : "")));
                crossSellBasketViewModel.HasMaxItemsInBasketMessage = true;
            }

            crossSellBasketViewModel.ErrorMessages.AddRange(errorMessages);

            return crossSellBasketViewModel;
        }


        private CrossSellBasketItemViewModel BuildDeezerModel(FnacDirect.Contracts.Online.Model.Promotion promotion)
        {
            var opcPush = _applicationContext.ConfigurationOpcPush;
            var opcPushs = opcPush.Select(x => x.Value);
            var push = opcPushs.FirstOrDefault(x => x.DisplayTemplate == promotion.DisplayTemplate);

            if (push == null || !push.Visibility.Contains(VisibilityPlace.PIPE))
            {
                return null;
            }
            var model = new CrossSellBasketItemViewModel
            {
                ProductId = 0,
                Content = promotion.LongWording
            };
            return model;
        }

        private int GetStomQuantity(PopOrderForm popOrderForm, StandardArticle standardArticle)
        {
            ILineGroup parentLineGroup = null;

            foreach (var lineGroup in popOrderForm.LineGroups)
            {
                if (lineGroup.Articles.Any(a => a == standardArticle))
                {
                    parentLineGroup = lineGroup;
                    break;
                }
            }

            if (parentLineGroup != null)
            {
                var maybeShippingMethodEvaluationGroup = popOrderForm.ShippingMethodEvaluation.Seller(parentLineGroup.Seller.SellerId);

                if (maybeShippingMethodEvaluationGroup.HasValue)
                {
                    var shippingMethodEvaluationGroup = maybeShippingMethodEvaluationGroup.Value;

                    if (shippingMethodEvaluationGroup.SelectedShippingMethodEvaluationType == ShippingMethodEvaluationType.Shop)
                    {
                        var shopAddress = shippingMethodEvaluationGroup.ShopShippingMethodEvaluationItem.Value.ShippingAddress;

                        if (int.TryParse(shopAddress.RefUG, out var refUG))
                        {
                            var storeStockInformations = _solexBusiness.GetStoreStockInformations(refUG.ToString(), standardArticle);

                            if (storeStockInformations != null && storeStockInformations.TotalAvailableClickAndCollect > 0)
                            {
                                return storeStockInformations.TotalAvailableClickAndCollect;
                            }
                        }
                    }
                }
            }
            return 999;
        }

        public AvailabilityViewModel GetAvailability(StandardArticle standardArticle)
        {
            var availabilityViewModel = new AvailabilityViewModel();

            if ((standardArticle.AvailabilityId == 99 || standardArticle.AvailabilityId == 199) && standardArticle.AvailableStockToDisplay <= 5 && standardArticle.AvailableStockToDisplay > 0)
                {
                    availabilityViewModel.HasLimitedQuantity = true;
                    availabilityViewModel.Availability = _applicationContext.GetMessage("orderpipe.pop.basket.availability.stock.limited", standardArticle.AvailableStockToDisplay);
                    availabilityViewModel.HasQuantityMax = true;
                }
                else
                {
                    availabilityViewModel.Availability = standardArticle.AvailabilityLabel;
                    availabilityViewModel.HasQuantityMax = standardArticle.EnableUpdateQuantity;
                }
            

            return availabilityViewModel;
        }


    }
}
