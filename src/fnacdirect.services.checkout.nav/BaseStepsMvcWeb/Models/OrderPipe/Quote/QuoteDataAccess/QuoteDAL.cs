using FnacDirect.Customer.Model;
using FnacDirect.Quote.Model;
using FnacDirect.Quote.Model.Enums;
using FnacDirect.Technical.Framework.CoreServices;
using System;
using System.Data;
using System.Data.SqlClient;

namespace FnacDirect.Quote.DAL
{
    public class QuoteDAL
    {
        private const string DATABASE_SqlQuote = "SqlQuote";
        private const int CodeError = 11001;

        private const string Add_Quote = "spQuote_Add_Quote";
        private const string Update_Quote = "spQuote_Update_Quote";
        private const string Write_Quote = "spQuote_Write_Quote"; // recriture du devis 
        private const string Get_Quote_BySID = "spQuote_Get_Quote_BySID";
        private const string Get_Quote_SequenceReference = "spQuote_getQuoteSequenceRef";
        private const string Get_BinaryPdf_ByQuoteSID = "spQuote_Get_BinaryPdf_ByQuoteSID";

        /// <summary>
        /// 
        /// </summary>
        /// <param name="prmType"></param>
        /// <param name="Quote_Id"></param>
        /// <param name="withDetail"></param>
        /// <param name="prmszSID"></param>
        /// <returns></returns>
        public static QuoteEntity Read(string quote_SID, bool withDetail)
        {
            try
            {
                using (var database = Database.CreateInstance(DATABASE_SqlQuote))
                {
                    using (var command = database.GetStoredProcCommand(Get_Quote_BySID))
                    {
                        database.AddInParameter(command, "fld_Quote_SID", SqlDbType.UniqueIdentifier, new Guid(quote_SID));
                        using (var reader = database.ExecuteReader(command))
                        {
                            while (reader.Read())
                            {
                                return FillQuote<QuoteEntity>(reader, withDetail);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logging.Current.WriteError(ex, CodeError);
                throw;
            }
            return null;
        }

        /// <summary>
        /// FillQuote
        /// </summary>
        /// <typeparam name="T">a QuoteForm type</typeparam>
        /// <param name="reader"></param>
        /// <param name="withDetail"></param>
        /// <returns></returns>
        private static T FillQuote<T>(SqlDataReader reader, bool withDetail) where T : new()
        {
            var t = new T();

            if (typeof(T) == typeof(QuoteForm))
            {
                var qf = t as QuoteForm;

                qf.QuoteId = DataReaderHelper.GetField<int>(reader, "fld_Quote_Id_Pk");
                qf.QuoteReference = DataReaderHelper.GetField<string>(reader, "fld_Quote_Reference");
                qf.AccountID = DataReaderHelper.GetField<int>(reader, "fld_Quote_Account_Id_Fk");
                qf.Status = DataReaderHelper.GetField<short>(reader, "fld_Quote_Status_Fk");
                qf.OrderReference = DataReaderHelper.GetField<int>(reader, "fld_Quote_Order_Fk");
                qf.QuoteMaster = DataReaderHelper.GetField<int>(reader, "fld_Quote_Master_Fk");
                qf.ContactID = DataReaderHelper.GetField<int>(reader, "fld_Quote_Contact_Fk");

                if (withDetail)
                {
                    qf.QuoteXmlContent = DataReaderHelper.GetField<string>(reader, "fld_Quote_Content");
                }

                qf.TotalHT = DataReaderHelper.GetField<decimal>(reader, "fld_Quote_Total_HT");
                qf.Total_TTC = DataReaderHelper.GetField<decimal>(reader, "fld_Quote_Total_TTC");
                qf.ArticlesCount = DataReaderHelper.GetField<int>(reader, "fld_Quote_Articles_Count");
                qf.PrivateComment = DataReaderHelper.GetField<string>(reader, "fld_Quote_Private_Comment");
                qf.CreationDate = DataReaderHelper.GetField<DateTime>(reader, "fld_Quote_CreationDate");
                qf.ExpirationDate = DataReaderHelper.GetField<DateTime>(reader, "fld_Quote_ExpirationDate");
                qf.CreatedBy = DataReaderHelper.GetField<string>(reader, "fld_Quote_CreatedBy");
                qf.LastModifiedBy = DataReaderHelper.GetField<string>(reader, "fld_Quote_LastUpdatedBy");
                qf.LastModificationDate = DataReaderHelper.GetField<DateTime>(reader, "fld_Quote_LastUpdated");
                qf.SID = DataReaderHelper.GetField<string>(reader, "fld_Quote_SID");
                var l_quoteRequestReasonValue = DataReaderHelper.GetField<short>(reader, "fld_Quote_Request_Reason");
                qf.QuoteDemandRaison = (QuoteDemandRaisonEnum)Enum.Parse(typeof(QuoteDemandRaisonEnum), l_quoteRequestReasonValue.ToString());

                var companyInfo = new CompanyInfo
                {
                    ID = DataReaderHelper.GetField<int>(reader, "fld_Quote_CompanyId_FK")
                };

                qf.Company = companyInfo;
            }
            else if (typeof(T) == typeof(QuoteEntity))
            {
                var qe = t as QuoteEntity;

                qe.QuoteId = DataReaderHelper.GetField<int>(reader, "QuoteId");
                qe.QuoteReference = DataReaderHelper.GetField<string>(reader, "QuoteReference");
                qe.AccountID = DataReaderHelper.GetField<int>(reader, "AccountId");
                qe.Status = DataReaderHelper.GetField<short>(reader, "StatusId");
                qe.OrderReference = DataReaderHelper.GetField<int>(reader, "OrderReference");
                qe.QuoteMaster = DataReaderHelper.GetField<int>(reader, "QuoteMaster");
                qe.ContactID = DataReaderHelper.GetField<int>(reader, "ContactId");

                if (withDetail)
                {

                    qe.QuoteXmlContent = DataReaderHelper.GetField<string>(reader, "QuoteXmlContent");
                }

                qe.TotalHT = DataReaderHelper.GetField<decimal>(reader, "QuoteTotalHT");
                qe.Total_TTC = DataReaderHelper.GetField<decimal>(reader, "QuoteTotalTTC");
                qe.ArticlesCount = DataReaderHelper.GetField<int>(reader, "ArticlesCount");
                qe.PrivateComment = DataReaderHelper.GetField<string>(reader, "PrivateComment");
                qe.CreationDate = DataReaderHelper.GetField<DateTime>(reader, "CreationDate");
                qe.ExpirationDate = DataReaderHelper.GetField<DateTime>(reader, "ExpirationDate");
                qe.CreatedBy = DataReaderHelper.GetField<string>(reader, "CreatedBy");
                qe.LastModifiedBy = DataReaderHelper.GetField<string>(reader, "LastModifiedBy");
                qe.LastModificationDate = DataReaderHelper.GetField<DateTime>(reader, "LastModificationDate");
                qe.SID = DataReaderHelper.GetField<string>(reader, "SID");
                var l_quoteRequestReasonValue = DataReaderHelper.GetField<short>(reader, "RequestReason");
                qe.QuoteDemandRaison = (QuoteDemandRaisonEnum)Enum.Parse(typeof(QuoteDemandRaisonEnum), l_quoteRequestReasonValue.ToString());

                var companyInfo = new CompanyInfo
                {
                    ID = DataReaderHelper.GetField<int>(reader, "CompanyId")
                };
                qe.Company = companyInfo;
            }

            return t;
        }

        //RG-DE-0163
        public static int GetQuoteSequenceReference()
        {
            var _returnvalue = -1;
            try
            {
                using (var database = Database.CreateInstance(DATABASE_SqlQuote))
                {
                    using (var command = database.GetStoredProcCommand(Get_Quote_SequenceReference))
                    {
                        database.AddOutParameter(command, "NewQuoteReference", SqlDbType.VarChar, 20);
                        using (var reader = database.ExecuteReader(command))
                        {
                            var l_result = database.GetParameterValue(command, "NewQuoteReference", string.Empty);
                            if (!string.IsNullOrEmpty(l_result))
                                _returnvalue = int.Parse(l_result);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logging.Current.WriteError(ex, CodeError);
                throw;
            }
            return _returnvalue;
        }

        public static bool Write(QuoteForm quoteForm)
        {
            var _returnvalue = false;
            try
            {
                using (var database = Database.CreateInstance(DATABASE_SqlQuote))
                {
                    // switch type to store 
                    using (var command = database.GetStoredProcCommand(Write_Quote))
                    {
                        database.AddInParameter(command, "Quote_Id", SqlDbType.Int, quoteForm.QuoteId);
                        database.AddInParameter(command, "Quote_Reference", SqlDbType.VarChar, quoteForm.QuoteReference);
                        database.AddInParameter(command, "Quote_Account_Id", SqlDbType.Int, quoteForm.UserInfo.AccountId);
                        database.AddInParameter(command, "Quote_Status", SqlDbType.SmallInt, quoteForm.Status);
                        database.AddInParameter(command, "Quote_Order", SqlDbType.Int, quoteForm.OrderReference);
                        database.AddInParameter(command, "Quote_Master", SqlDbType.Int, quoteForm.QuoteMaster);
                        database.AddInParameter(command, "Quote_Contact", SqlDbType.Int, quoteForm.ContactID);
                        database.AddInParameter(command, "Quote_Content", SqlDbType.NVarChar, quoteForm.QuoteXmlContent);
                        database.AddInParameter(command, "Quote_Total_HT", SqlDbType.Decimal, quoteForm.TotalHT);
                        database.AddInParameter(command, "Quote_Total_TTC", SqlDbType.Decimal, quoteForm.Total_TTC);
                        database.AddInParameter(command, "Quote_Articles_Count", SqlDbType.Int, quoteForm.ArticlesCount);
                        database.AddInParameter(command, "Quote_Private_Comment", SqlDbType.NVarChar, quoteForm.PrivateComment);
                        database.AddInParameter(command, "Quote_CreationDate", SqlDbType.DateTime, quoteForm.CreationDate);
                        database.AddInParameter(command, "Quote_ExpirationDate", SqlDbType.DateTime, quoteForm.ExpirationDate);
                        if (quoteForm.CreatedBy != null)
                            database.AddInParameter(command, "Quote_CreatedBy", SqlDbType.VarChar, quoteForm.CreatedBy);
                        else
                            database.AddInParameter(command, "Quote_CreatedBy", SqlDbType.VarChar, DBNull.Value);
                        database.AddInParameter(command, "Quote_LastUpdatedBy", SqlDbType.VarChar, quoteForm.LastModifiedBy);
                        database.AddInParameter(command, "Quote_LastUpdated", SqlDbType.DateTime, quoteForm.LastModificationDate);
                        database.AddInParameter(command, "Quote_SID", SqlDbType.UniqueIdentifier, new Guid(quoteForm.SID));
                        if (null != quoteForm.Company)
                            database.AddInNullableParameter(command, "Quote_Company_Id", SqlDbType.Int, quoteForm.Company.ID);
                        else
                            database.AddInNullableParameter(command, "Quote_Company_Id", SqlDbType.Int, DBNull.Value);
                        database.AddInNullableParameter(command, "Quote_Resquest_Reason", SqlDbType.SmallInt, quoteForm.QuoteDemandRaison);

                        database.ExecuteNonQuery(command);
                        _returnvalue = true;
                    }
                }
            }
            catch (Exception ex)
            {
                Logging.Current.WriteError(ex, CodeError);
                throw;
            }
            return _returnvalue;
        }

        /// <summary>
        /// Mise à jour des champs hors XML du devis
        /// </summary>
        /// <param name="prmiType"></param>
        /// <param name="quoteForm"></param>
        /// <returns></returns>
        public static bool Update(QuoteForm quoteForm)
        {
            var _returnvalue = false;
            try
            {
                using (var database = Database.CreateInstance(DATABASE_SqlQuote))
                {
                    // switch type to store 
                    using (var command = database.GetStoredProcCommand(Update_Quote))
                    {

                        database.AddInOutParameter(command, "Quote_Id", SqlDbType.Int, quoteForm.QuoteId);
                        database.AddInParameter(command, "Quote_Reference", SqlDbType.VarChar, quoteForm.QuoteReference);
                        database.AddInParameter(command, "Quote_Account_Id", SqlDbType.Int, quoteForm.AccountID);
                        database.AddInParameter(command, "Quote_Status", SqlDbType.SmallInt, quoteForm.Status);
                        database.AddInParameter(command, "Quote_Order", SqlDbType.Int, quoteForm.OrderReference);
                        database.AddInParameter(command, "Quote_Master", SqlDbType.Int, quoteForm.QuoteMaster);
                        database.AddInParameter(command, "Quote_Contact", SqlDbType.Int, quoteForm.ContactID);
                        database.AddInParameter(command, "Quote_Total_HT", SqlDbType.Decimal, quoteForm.TotalHT);
                        database.AddInParameter(command, "Quote_Total_TTC", SqlDbType.Decimal, quoteForm.Total_TTC);
                        database.AddInParameter(command, "Quote_Articles_Count", SqlDbType.Int, quoteForm.ArticlesCount);
                        database.AddInParameter(command, "Quote_Private_Comment", SqlDbType.Text, quoteForm.PrivateComment);
                        database.AddInParameter(command, "Quote_CreationDate", SqlDbType.DateTime, quoteForm.CreationDate);
                        database.AddInParameter(command, "Quote_ExpirationDate", SqlDbType.DateTime, quoteForm.ExpirationDate);
                        if (quoteForm.CreatedBy != null)
                            database.AddInParameter(command, "Quote_CreatedBy", SqlDbType.VarChar, quoteForm.CreatedBy);
                        else
                            database.AddInParameter(command, "Quote_CreatedBy", SqlDbType.VarChar, DBNull.Value);
                        database.AddInParameter(command, "Quote_LastUpdatedBy", SqlDbType.VarChar, quoteForm.LastModifiedBy);
                        database.AddInParameter(command, "Quote_LastUpdated", SqlDbType.DateTime, quoteForm.LastModificationDate);
                        database.AddInParameter(command, "Quote_SID", SqlDbType.UniqueIdentifier, new Guid(quoteForm.SID));
                        database.AddInParameter(command, "Quote_Company_Id", SqlDbType.Int, quoteForm.Company.ID);
                        database.AddInNullableParameter(command, "Quote_Request_Reason", SqlDbType.SmallInt, quoteForm.QuoteDemandRaison);

                        database.ExecuteNonQuery(command);

                        _returnvalue = true;
                    }
                }
            }
            catch (Exception ex)
            {
                Logging.Current.WriteError(ex, CodeError);
                throw;
            }
            return _returnvalue;
        }

        /// <summary>
        /// Insertion d'un nouveau devis
        /// </summary>
        /// <param name="prmiType"></param>
        /// <param name="quoteForm"></param>
        /// <returns>retourne 0 en cas d'échec sinon elle renvoie le numéro technique du devis</returns>
        public static int Add(QuoteForm quoteForm)
        {
            var _returnvalue = 0;
            try
            {
                using (var database = Database.CreateInstance(DATABASE_SqlQuote))
                {
                    using (var command = database.GetStoredProcCommand(Add_Quote))
                    {
                        database.AddInParameter(command, "fld_Quote_Reference", SqlDbType.VarChar, quoteForm.QuoteReference);
                        database.AddInParameter(command, "fld_Quote_SID", SqlDbType.UniqueIdentifier, new Guid(quoteForm.SID));
                        database.AddInParameter(command, "fld_Quote_Account_Id", SqlDbType.Int, quoteForm.AccountID);
                        database.AddInParameter(command, "fld_Quote_Status", SqlDbType.SmallInt, quoteForm.Status);
                        database.AddInParameter(command, "fld_Quote_Order", SqlDbType.Int, quoteForm.OrderReference);
                        database.AddInParameter(command, "fld_Quote_Master", SqlDbType.Int, quoteForm.QuoteMaster);
                        database.AddInParameter(command, "fld_Quote_Contact", SqlDbType.Int, quoteForm.ContactID);
                        database.AddInParameter(command, "fld_Quote_Content", SqlDbType.NVarChar, quoteForm.QuoteXmlContent);
                        database.AddInNullableParameter(command, "fld_Quote_RequestReason", SqlDbType.SmallInt, quoteForm.QuoteDemandRaison);
                        database.AddInParameter(command, "fld_Quote_Total_HT", SqlDbType.Decimal, quoteForm.TotalHT);
                        database.AddInParameter(command, "fld_Quote_Total_TTC", SqlDbType.Decimal, quoteForm.Total_TTC);
                        database.AddInParameter(command, "fld_Quote_Articles_Count", SqlDbType.Int, quoteForm.ArticlesCount);
                        database.AddInParameter(command, "fld_Quote_Private_Comment", SqlDbType.Text, quoteForm.PrivateComment);
                        database.AddInParameter(command, "fld_Quote_CreationDate", SqlDbType.DateTime, quoteForm.CreationDate);
                        database.AddInParameter(command, "fld_Quote_ExpirationDate", SqlDbType.DateTime, quoteForm.ExpirationDate);
                        if (quoteForm.CreatedBy != null)
                            database.AddInParameter(command, "fld_Quote_CreatedBy", SqlDbType.VarChar, quoteForm.CreatedBy);
                        else
                            database.AddInParameter(command, "fld_Quote_CreatedBy", SqlDbType.VarChar, DBNull.Value);
                        database.AddInParameter(command, "fld_Quote_LastUpdatedBy", SqlDbType.VarChar, quoteForm.LastModifiedBy);
                        database.AddInParameter(command, "fld_Quote_LastUpdated", SqlDbType.DateTime, quoteForm.LastModificationDate);
                        if (null != quoteForm.Company)
                            database.AddInParameter(command, "fld_Quote_Company", SqlDbType.Int, quoteForm.Company.ID);
                        database.AddOutParameter(command, "fld_Quote_Id_Pk", SqlDbType.Int);

                        database.ExecuteNonQuery(command);

                        _returnvalue = database.GetParameterValue(command, "fld_Quote_Id_Pk", 0);
                    }
                }
            }
            catch (Exception ex)
            {
                Logging.Current.WriteError(ex, CodeError);
                throw;
            }
            return _returnvalue;
        }

        public static QuoteBinaryPdf GetQuoteBinaryPdfByQuoteSID(string quoteSID)
        {
            QuoteBinaryPdf binaryPdf = null;

            try
            {
                using (var db = Database.CreateInstance(DATABASE_SqlQuote))
                {
                    using (var command = db.GetStoredProcCommand(Get_BinaryPdf_ByQuoteSID))
                    {
                        db.AddInParameter(command, "Quote_SID", SqlDbType.VarChar, quoteSID);
                        using (var reader = db.ExecuteReader(command))
                        {
                            while (reader.Read())
                            {
                                binaryPdf = FillQuoteBinaryPdf(reader, binaryPdf);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logging.Current.WriteError(ex, CodeError);
                throw;
            }
            return binaryPdf;
        }

        private static QuoteBinaryPdf FillQuoteBinaryPdf(SqlDataReader reader, QuoteBinaryPdf binaryPdf)
        {
            if (binaryPdf == null)
                binaryPdf = new QuoteBinaryPdf();
            binaryPdf.QuoteID = DataReaderHelper.GetField<int>(reader, "quoteId");
            binaryPdf.BinaryPdf = DataReaderHelper.GetField<byte[]>(reader, "binaryPdf");
            binaryPdf.CreationDate = DataReaderHelper.GetField<DateTime>(reader, "creationDate");
            binaryPdf.QuoteCreationDate = DataReaderHelper.GetField<DateTime>(reader, "quoteCreationDate");
            return binaryPdf;
        }

        public static QuoteEntity GetQuoteDetailBySID(string quoteSid)
        {
            return Read(quoteSid, true);
        }
    }
}
