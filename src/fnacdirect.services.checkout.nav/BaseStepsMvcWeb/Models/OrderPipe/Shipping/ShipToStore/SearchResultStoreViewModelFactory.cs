﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using FnacDirect.OrderPipe.Base.Business.Shipping.Shop;
using FnacDirect.OrderPipe.Base.Model;
using System.Web.Mvc;
using FnacDirect.Technical.Framework.Web.Mvc;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Shipping.ShipToStore
{
    public class SearchResultStoreViewModelFactory
    {
        private static readonly Lazy<SearchResultStoreViewModelFactory> Singleton
            = new Lazy<SearchResultStoreViewModelFactory>(() => new SearchResultStoreViewModelFactory());

        public static SearchResultStoreViewModelFactory Current
        {
            get { return Singleton.Value; }
        }

        private SearchResultStoreViewModelFactory()
        {

        }

        public T CreateSearchResultStoreViewModel<T>(FnacStore.Model.FnacStore fnacStore, IEnumerable<ShipToStoreAvailability> availabilities, IEnumerable<StandardArticle> standardArticles, Action<T> additionalInitializer = null)
            where T : SearchResultStoreViewModel, new()
        { 
            var geoLocalizationAddress = fnacStore.GeoLocalizationAddress.IndexOf(",", StringComparison.Ordinal);

            var latitude = double.Parse(fnacStore.GeoLocalizationAddress.Substring(1, geoLocalizationAddress - 1), CultureInfo.InvariantCulture);
            var longitude = double.Parse(fnacStore.GeoLocalizationAddress.Substring(geoLocalizationAddress + 1, fnacStore.GeoLocalizationAddress.Length - geoLocalizationAddress - 2), CultureInfo.InvariantCulture);


            var instance = new T
            {
                ClickAndCollectAvailabilities = new List<StoreArticleAvailabilityViewModel>(),
                DefferedAvailabilities = new List<StoreArticleAvailabilityViewModel>(),

                Id = fnacStore.Id,
                Name = fnacStore.Name,
                City = fnacStore.City != null ? fnacStore.City.Name : string.Empty,
                ZipCode = fnacStore.ZipCode,
                Address = fnacStore.Address,
                Latitude = latitude,
                Longitude = longitude,
                OpeningHours = fnacStore.OpeningHours,
                Organisation = fnacStore.Organization
            };

            if (availabilities == null || standardArticles == null) 
                return instance;

            var articles = standardArticles as IList<StandardArticle> ?? standardArticles.ToList();

            instance.ClickAndCollectAvailabilities =
                (from standardArticle in articles
                    let availability = availabilities.FirstOrDefault(a => a.ReferenceGu == standardArticle.ReferenceGU)
                    where availability is ClickAndCollectShipToStoreAvailability
                    select new StoreArticleAvailabilityViewModel
                    {
                        Quantity = standardArticle.Quantity.HasValue ? standardArticle.Quantity.Value : 1,
                        Title = standardArticle.Title
                    }).ToList();

            instance.DefferedAvailabilities =
                (from standardArticle in articles
                    let availability = availabilities.FirstOrDefault(a => a.ReferenceGu == standardArticle.ReferenceGU)
                    where availability is ShippingShipToStoreAvailability
                    select new StoreArticleAvailabilityViewModel
                    {
                        Quantity = standardArticle.Quantity.HasValue ? standardArticle.Quantity.Value : 1,
                        Title = standardArticle.Title
                    }).ToList();

            var applicationContext = DependencyResolver.Current.GetService<IApplicationContext>();

            if (applicationContext != null)
            {
                instance.ClickAndCollectAvailability = applicationContext.GetMessage("orderpipe.index.shiptostore.onehour", null);
            }

            instance.DefferedAvailability = "Retrait";

            if (additionalInitializer != null)
                additionalInitializer(instance);

            return instance;
        }
    }
}