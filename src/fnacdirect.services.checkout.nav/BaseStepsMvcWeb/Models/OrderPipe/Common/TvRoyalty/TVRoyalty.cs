using System.Xml.Serialization;

namespace FnacDirect.OrderPipe.Base.Model
{
    public class TVRoyalty
    {
        [XmlIgnore]
        private string _Company;

        public string Company
        {
            get
            {
                return _Company;
            }
            set
            {
                _Company = value;
            }
        }

        [XmlIgnore]
        private string _FirstName;

        public string FirstName
        {
            get
            {
                return _FirstName;
            }
            set
            {
                _FirstName = value;
            }
        }

        [XmlIgnore]
        private string _LastName;

        public string LastName
        {
            get
            {
                return _LastName;
            }
            set
            {
                _LastName = value;
            }
        }

        [XmlIgnore]
        private string _BirthState;

        public string BirthState
        {
            get
            {
                return _BirthState;
            }
            set
            {
                _BirthState = value;
            }
        }
        [XmlIgnore]
        private string _BirthDate;

        public string BirthDate
        {
            get
            {
                return _BirthDate;
            }
            set
            {
                _BirthDate = value;
            }
        }
        [XmlIgnore]
        private string _TVUse2;

        public string TVUse2
        {
            get
            {
                return _TVUse2;
            }
            set
            {
                _TVUse2 = value;
            }
        }
        [XmlIgnore]
        private string _TVUse;

        public string TVUse
        {
            get
            {
                return _TVUse;
            }
            set
            {
                _TVUse = value;
            }
        }
        [XmlIgnore]
        private string _StreetType;

        public string StreetType
        {
            get
            {
                return _StreetType;
            }
            set
            {
                _StreetType = value;
            }
        }
        [XmlIgnore]
        private string _StreetRange;

        public string StreetRange
        {
            get
            {
                return _StreetRange;
            }
            set
            {
                _StreetRange = value;
            }
        }
        [XmlIgnore]
        private string _StreetNo;

        public string StreetNo
        {
            get
            {
                return _StreetNo;
            }
            set
            {
                _StreetNo = value;
            }
        }
        [XmlIgnore]
        private string _Gender;

        public string Gender
        {
            get
            {
                return _Gender;
            }
            set
            {
                _Gender = value;
            }
        }
        [XmlIgnore]
        private string _BirthLocation;

        public string BirthLocation
        {
            get
            {
                return _BirthLocation;
            }
            set
            {
                _BirthLocation = value;
            }
        }
        [XmlIgnore]
        private string _DOBYear;

        public string DOBYear
        {
            get
            {
                return _DOBYear;
            }
            set
            {
                _DOBYear = value;
            }
        }
        [XmlIgnore]
        private string _DOBMonth;

        public string DOBMonth
        {
            get
            {
                return _DOBMonth;
            }
            set
            {
                _DOBMonth = value;
            }
        }
        [XmlIgnore]
        private string _DOBDay;

        public string DOBDay
        {
            get
            {
                return _DOBDay;
            }
            set
            {
                _DOBDay = value;
            }
        }
        [XmlIgnore]
        private string _EMail;

        public string EMail
        {
            get
            {
                return _EMail;
            }
            set
            {
                _EMail = value;
            }
        }
        [XmlIgnore]
        private string _CellPhone;

        public string CellPhone
        {
            get
            {
                return _CellPhone;
            }
            set
            {
                _CellPhone = value;
            }
        }
        [XmlIgnore]
        private string _Fax;

        public string Fax
        {
            get
            {
                return _Fax;
            }
            set
            {
                _Fax = value;
            }
        }
        [XmlIgnore]
        private string _Tel;

        public string Tel
        {
            get
            {
                return _Tel;
            }
            set
            {
                _Tel = value;
            }
        }
        [XmlIgnore]
        private string _CountryId;

        public string CountryId
        {
            get
            {
                return _CountryId;
            }
            set
            {
                _CountryId = value;
            }
        }
        [XmlIgnore]
        private string _State;

        public string State
        {
            get
            {
                return _State;
            }
            set
            {
                _State = value;
            }
        }
        [XmlIgnore]
        private string _City;

        public string City
        {
            get
            {
                return _City;
            }
            set
            {
                _City = value;
            }
        }

        [XmlIgnore]
        private string _ZipCode;

        public string ZipCode
        {
            get
            {
                return _ZipCode;
            }
            set
            {
                _ZipCode = value;
            }
        }
        [XmlIgnore]
        private string _AddressLine3;

        public string AddressLine3
        {
            get
            {
                return _AddressLine3;
            }
            set
            {
                _AddressLine3 = value;
            }
        }
        [XmlIgnore]
        private string _AddressLine2;

        public string AddressLine2
        {
            get
            {
                return _AddressLine2;
            }
            set
            {
                _AddressLine2 = value;
            }
        }
        [XmlIgnore]
        private string _AddressLine1;

        public string AddressLine1
        {
            get
            {
                return _AddressLine1;
            }
            set
            {
                _AddressLine1 = value;
            }
        }
    }
}
