using FnacDirect.Contracts.Online.Model;
using FnacDirect.Membership.Model.Constants;
using FnacDirect.OrderPipe.Base.BaseModel;
using FnacDirect.OrderPipe.Base.BaseModel.Model;
using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.OrderPipe.Base.Business.Operations;
using FnacDirect.OrderPipe.Base.Model.Pop;
using FnacDirect.OrderPipe.Base.OneClick;
using FnacDirect.OrderPipe.Base.Proxy.Membership;
using FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Common.Basket;
using FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Common.Shipping.Postal;
using FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Common.Shipping.Relay;
using FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Common.Shipping.Shop;
using FnacDirect.Technical.Framework.Web.Mvc;
using System;
using System.Linq;
using FnacDirect.OrderPipe.Base.Business;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Common.Shipping
{
    public class ShippingViewModelBuilder : IShippingViewModelBuilder
    {
        private readonly IBillingOnlyViewModelBuilder _billingOnlyViewModelBuilder;
        private readonly IPostalShippingNetworkViewModelBuilder _postalNetworkViewModelBuilder;
        private readonly IShopShippingNetworkViewModelBuilder _shopShippingNetworkViewModelBuilder;
        private readonly IRelayShippingNetworkViewModelBuilder _relayShippingNetworkViewModelBuilder;
        private readonly IExpressPlusViewModelBuilder _expressPlusViewModelBuilder;
        private readonly IApplicationContext _applicationContext;
        private readonly IMembershipConfigurationService _membershipConfigurationService;
        private readonly IUserExtendedContextProvider _userExtendedContextProvider;
        private readonly IFnacPlusPushBuilder _fnacPlusPushBuilder;
        private readonly IDeliveredForChristmasService _deliveredForChristmasService;
        private readonly IAddressValidationBusiness _addressValidationBusiness;

        public ShippingViewModelBuilder(IBillingOnlyViewModelBuilder billingOnlyViewModelBuilder,
                                        IPostalShippingNetworkViewModelBuilder postalNetworkViewModelBuilder,
                                        IShopShippingNetworkViewModelBuilder shopShippingNetworkViewModelBuilder,
                                        IRelayShippingNetworkViewModelBuilder relayShippingNetworkViewModelBuilder,
                                        IExpressPlusViewModelBuilder expressPlusViewModelBuilder,
                                        IApplicationContext applicationContext,
                                        IMembershipConfigurationService membershipConfigurationService,
                                        IUserExtendedContextProvider userExtendedContextProvider,
                                        IFnacPlusPushBuilder fnacPlusPushBuilder,
                                        IDeliveredForChristmasService deliveredForChristmasService,
                                        IAddressValidationBusiness addressValidationBusiness)
        {
            _billingOnlyViewModelBuilder = billingOnlyViewModelBuilder;
            _postalNetworkViewModelBuilder = postalNetworkViewModelBuilder;
            _shopShippingNetworkViewModelBuilder = shopShippingNetworkViewModelBuilder;
            _relayShippingNetworkViewModelBuilder = relayShippingNetworkViewModelBuilder;
            _applicationContext = applicationContext;
            _expressPlusViewModelBuilder = expressPlusViewModelBuilder;
            _membershipConfigurationService = membershipConfigurationService;
            _userExtendedContextProvider = userExtendedContextProvider;
            _fnacPlusPushBuilder = fnacPlusPushBuilder;
            _deliveredForChristmasService = deliveredForChristmasService;
            _addressValidationBusiness = addressValidationBusiness;
        }

        public ShippingViewModel Build(PopOrderForm popOrderForm, int sellerId)
        {
            var shippingMode = ComputeShippingViewModelMode(popOrderForm, sellerId);

            var shippingViewModel = new ShippingViewModel(sellerId, shippingMode);

            var oneClickData = popOrderForm.GetPrivateData<OneClickData>(create: false);
            if (oneClickData != null && oneClickData.IsAvailable.HasValue)
            {
                shippingViewModel.IsOneClick = oneClickData.IsAvailable.Value;
            }
            var aboIlliData = popOrderForm.GetPrivateData<AboIlliData>("AboIlliGroup");
            var fnacLg = popOrderForm.LineGroups.Where(lg => lg.HasFnacComArticle);
            var hasOnlyExpressPlusInFnacCom = fnacLg.GetArticles()
                .Where(a => a.TypeId.HasValue && aboIlliData.TypeArtIds.Contains(a.TypeId.Value)).Any()
                && fnacLg.GetArticles().Count() == 1;
            if (popOrderForm.HasFnacComArticle() || popOrderForm.HasClickAndCollectArticle())
            {
                shippingViewModel.ExpressPlusInfo = _expressPlusViewModelBuilder.Build(aboIlliData);
                if (shippingViewModel.ExpressPlusInfo != null)
                {
                    shippingViewModel.ExpressPlusInfo.FromShipping = true;
                    shippingViewModel.ExpressPlusInfo.ShowInShipping = !aboIlliData.AddedFromShippingBackFromBasket && aboIlliData.IsAddedFromShippingStep || !aboIlliData.HasAboIlliInBasket && !aboIlliData.IsAbonne && aboIlliData.IsBasketElligibleForAboIlli;
                }
            }
            shippingViewModel.HasExpressPlus = (aboIlliData.IsAbonne || aboIlliData.HasAboIlliInBasket) && !popOrderForm.HasOnlyMarketPlaceArticle() && !hasOnlyExpressPlusInFnacCom;
            var siteCountryId = _applicationContext.GetSiteContext().CountryId;

            shippingViewModel.FnacPlusInfos = _fnacPlusPushBuilder.Build(popOrderForm);

            shippingViewModel.BillingAddressModel = new BillingAddressModel()
            {
                HasEligibleBillingAdress = false,
                DisplayMembershipMessage = popOrderForm.LineGroups.GetArticles().OfType<StandardArticle>().Any(art => art.TypeId.GetValueOrDefault() == (int)CategoryCardType.Recruit)
            };
            var wrapData = popOrderForm.GetPrivateData<WrapData>(create: false);
            if (wrapData != null)
            {
                shippingViewModel.HasWrap = popOrderForm.LineGroups.Fnac().Any(lg => lg.Informations.ChosenWrapMethod != 0) || wrapData.HasBeenRemovedForShopShipping;
            }

            shippingViewModel.IsValid = IsValid(popOrderForm, sellerId);

            if (_membershipConfigurationService != null)
            {
                shippingViewModel.HasFnacPlusCardInBasket = popOrderForm.LineGroups.GetArticles().OfType<StandardArticle>().Any(art =>
                    _membershipConfigurationService.FnacPlusCardTypes.Contains(art.TypeId.GetValueOrDefault()));
            }

            if (_userExtendedContextProvider != null)
            {
                var userExtendedContext = _userExtendedContextProvider.GetCurrentUserExtendedContext();
                var segments = userExtendedContext.Customer.ContractDTO.Segments;

                shippingViewModel.IsFnacPlusUser = segments.Contains(CustomerSegment.EssaiPlus.ToString())
                    || segments.Contains(CustomerSegment.ExpressPlus.ToString());
            }

            if (popOrderForm.BillingAddress.Identity != -1)
            {
                var hasMissingInfo = !_addressValidationBusiness.IsAddressValid(popOrderForm.BillingAddress.Convert());

                shippingViewModel.BillingAddressModel.BillingAddress = popOrderForm.BillingAddress.ConvertToAddressViewModel(siteCountryId, hasMissingInfo);
            }
            else
            {
                shippingViewModel.BillingAddressModel.BillingAddress = Maybe.Empty<AddressViewModel>();
            }

            var maybeShippingMehodEvaluationGroup = popOrderForm.ShippingMethodEvaluation.GetShippingMethodEvaluationGroup(sellerId);

            if (!maybeShippingMehodEvaluationGroup.HasValue)
            {
                return shippingViewModel;
            }

            var shippingMehodEvaluationGroup = maybeShippingMehodEvaluationGroup.Value;

            if (shippingViewModel.Mode != ShippingViewModelMode.BillingOnly)
            {
                if (shippingMehodEvaluationGroup.PostalShippingMethodEvaluationItem.HasValue
                    && shippingMehodEvaluationGroup.PostalShippingMethodEvaluationItem.Value.HasChoices)
                {
                    shippingViewModel.Postal = _postalNetworkViewModelBuilder.Build(popOrderForm, sellerId);

                    if (shippingMode == ShippingViewModelMode.Postal && shippingViewModel.BillingAddressModel.BillingAddress.HasValue
                        && shippingViewModel.SelectedShippingItem.HasValue
                        && shippingViewModel.SelectedShippingItem.Value is PostalDetailsViewModel
                        && shippingViewModel.Postal.Value.Selected.Value.Reference == shippingViewModel.BillingAddressModel.BillingAddress.Value.Reference)
                    {
                        (shippingViewModel.SelectedShippingItem.Value as PostalDetailsViewModel).IsShippingAndBillingAddresseAreEqual = true;
                        shippingViewModel.BillingAddressModel.BillingAddress = Maybe<AddressViewModel>.Some(shippingViewModel.SelectedShippingItem.Value as AddressViewModel);
                    }
                }

                if (shippingMehodEvaluationGroup.ShopShippingMethodEvaluationItem.HasValue
                    && shippingMehodEvaluationGroup.ShopShippingMethodEvaluationItem.Value.HasChoices)
                {
                    shippingViewModel.Shop = _shopShippingNetworkViewModelBuilder.Build(popOrderForm, sellerId);
                }

                if (shippingMehodEvaluationGroup.RelayShippingMethodEvaluationItem.HasValue
                    && shippingMehodEvaluationGroup.RelayShippingMethodEvaluationItem.Value.HasChoices)
                {
                    shippingViewModel.Relay = _relayShippingNetworkViewModelBuilder.Build(popOrderForm, sellerId);
                }

                shippingViewModel.BillingAddressModel.HasShippingAddress = shippingMehodEvaluationGroup.SelectedShippingMethodEvaluationType != ShippingMethodEvaluationType.Unknown && !shippingMehodEvaluationGroup.SelectedShippingMethodEvaluation.IsDummyAddress;
                shippingViewModel.BillingAddressModel.HasEligibleBillingAdress = true;

                shippingViewModel.BillingAddressModel.HasPostalShippingAddress = shippingViewModel.BillingAddressModel.HasShippingAddress && shippingMehodEvaluationGroup.SelectedShippingMethodEvaluationType == ShippingMethodEvaluationType.Postal;
            }
            else
            {
                shippingViewModel.BillingOnly = _billingOnlyViewModelBuilder.Build(popOrderForm, sellerId);
                shippingViewModel.BillingAddressModel.HasEligibleBillingAdress = true;

                shippingViewModel.BillingAddressModel.HasShippingAddress = true;
                shippingViewModel.BillingAddressModel.HasPostalShippingAddress = false;
            }

            if (shippingViewModel.SellerId <= 0)
            {
                shippingViewModel.ChristmasWording = _applicationContext.GetMessage(_deliveredForChristmasService.GetShippingWording());
            }

            return shippingViewModel;
        }

        protected internal ShippingViewModelMode ComputeShippingViewModelMode(PopOrderForm popOrderForm, int sellerId)
        {
            if (popOrderForm.IsOnlyNumerical())
            {
                return ShippingViewModelMode.BillingOnly;
            }

            if (!popOrderForm.HasMarketPlaceArticle())
            {
                var nonBillingOnlyLineGroups = (from lineGroup in popOrderForm.LineGroups
                                                where !lineGroup.Articles.OfType<BaseArticle>().Where(a => a.LogType.HasValue).All(a => popOrderForm.ShippingMethodEvaluation.ExcludedLogisticTypes.Contains(a.LogType.Value))
                                                where !lineGroup.HasDematSoftArticles
                                                where !lineGroup.HasNumericalArticles
                                                select lineGroup).ToList();

                if (!nonBillingOnlyLineGroups.Any())
                {
                    return ShippingViewModelMode.BillingOnly;
                }
            }

            var maybeShippingMethodEvaluationGroup = popOrderForm.ShippingMethodEvaluation.GetShippingMethodEvaluationGroup(sellerId);

            if (maybeShippingMethodEvaluationGroup.HasValue)
            {
                var shippingMethodEvaluationGroup = maybeShippingMethodEvaluationGroup.Value;

                if (shippingMethodEvaluationGroup.PostalShippingMethodEvaluationItem.HasValue
                    && shippingMethodEvaluationGroup.SelectedShippingMethodEvaluationType == ShippingMethodEvaluationType.Postal)
                {
                    return ShippingViewModelMode.Postal;
                }

                if (shippingMethodEvaluationGroup.ShopShippingMethodEvaluationItem.HasValue
                               && shippingMethodEvaluationGroup.SelectedShippingMethodEvaluationType == ShippingMethodEvaluationType.Shop)
                {
                    return ShippingViewModelMode.Shop;
                }

                if (shippingMethodEvaluationGroup.RelayShippingMethodEvaluationItem.HasValue
                               && shippingMethodEvaluationGroup.SelectedShippingMethodEvaluationType == ShippingMethodEvaluationType.Relay)
                {
                    return ShippingViewModelMode.Relay;
                }
            }

            return ShippingViewModelMode.None;
        }

        public bool IsValid(PopOrderForm popOrderForm, int sellerId)
        {
            if (popOrderForm.BillingAddress == null
                || popOrderForm.BillingAddress.IsDummy())
            {
                return false;
            }

            var shippingViewModelMode = ComputeShippingViewModelMode(popOrderForm, sellerId);

            if (shippingViewModelMode == ShippingViewModelMode.BillingOnly)
            {
                return true;
            }

            var shippingMethodEvaluationGroup = popOrderForm.ShippingMethodEvaluation.GetShippingMethodEvaluationGroup(sellerId).Value;

            if (shippingMethodEvaluationGroup.SelectedShippingMethodEvaluationType == ShippingMethodEvaluationType.Unknown)
            {
                return false;
            }

            if (shippingMethodEvaluationGroup.SelectedShippingMethodEvaluation.IsDummyAddress)
            {
                return false;
            }

            return true;
        }
    }
}
