using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;

namespace FnacDirect.OrderPipe.Base.Business
{
    public class GuptPromotionCodeConfiguration
    {
        public string CodeMotif
        {
            get;
            set;
        }

        public string CodeType
        {
            get;
            set;
        }
    }
}
