using System;
using System.Xml.Serialization;

namespace FnacDirect.OrderPipe.Base.Model.Adherent
{
    [XmlRoot("SUBSCRIBER")]
    public class AdherentDataForm
    {
        private const int MAX_LENGTH_ADDR1 = 95;
        
        private string _address1;

        [XmlElement("SBC_ID", Order = 1)]
        public int ID { get; set; }

        [XmlElement("SBC_FIRSTNAME", Order = 2, IsNullable = false)]
        public string Firstname { get; set; }

        [XmlElement("SBC_LASTNAME", Order = 3, IsNullable = false)]
        public string Lastname { get; set; }

        [XmlElement("SBC_LASTNAME2", Order = 4)]
        public string Lastname2 { get; set; }

        [XmlElement("SBC_CIVILITY", Order = 5, IsNullable = false)]
        public int Civility { get; set; }

        [XmlElement("SBC_BIRTHDATE", Order = 6, IsNullable = false)]
        public DateTime Birthdate { get; set; }

        [XmlElement("SBC_FISCAL_TYPE", Order = 7)]
        public string FiscalType { get; set; }

        [XmlElement("SBC_FISCAL_CODE", Order = 8)]
        public string FiscalCode { get; set; }

        [XmlElement("SBC_STREET_TYPE", Order = 9)]
        public string StreetType { get; set; }

        [XmlElement("SBC_STREET_NUMBER", Order = 10)]
        public string StreetNumber { get; set; }

        [XmlElement("SBC_ADDR1", Order = 11, IsNullable = false)]
        public string Address1
        {
            get { return _address1; }
            set
            {
                if (value != null && value.Length > MAX_LENGTH_ADDR1)
                    _address1 = value.Substring(0, MAX_LENGTH_ADDR1);
                else
                    _address1 = value;
            }
        }

        [XmlElement("SBC_ADDR2", Order = 12)]
        public string Address2 { get; set; }

        [XmlElement("SBC_ZIP", Order = 13, IsNullable = false)]
        public string Zip { get; set; }

        [XmlElement("SBC_CITY", Order = 14, IsNullable = false)]
        public string City { get; set; }

        [XmlElement("SBC_STATE", Order = 15, IsNullable = false)]
        public string State { get; set; }

        [XmlElement("SBC_COUNTRY", Order = 16, IsNullable = false)]
        public int Country { get; set; }

        [XmlElement("SBC_DIALECT", Order = 17)]
        public int Dialect { get; set; }

        [XmlElement("SBC_EMAIL", Order = 18, IsNullable = false)]
        public string Email { get; set; }

        [XmlElement("SBC_NATIONALITY", Order = 19)]
        public int Nationality { get; set; }

        [XmlElement("SBC_PHONE", Order = 20)]
        public string Phone { get; set; }

        [XmlElement("SBC_CELLPHONE", Order = 21)]
        public string CellPhone { get; set; }

        [XmlElement("SBC_STOPPUB", Order = 22, IsNullable = false)]
        public bool StopPub { get; set; }

        [XmlElement("SBC_NAMETOENGRAVE", Order = 23)]
        public string NameToEngrave { get; set; }

        [XmlElement("SBC_STREET_Name", Order = 24)]
        public string StreetName { get; set; }
    }
}
