using FnacDirect.Relay.Model;
using System;
using System.Collections.Generic;

namespace FnacDirect.OrderPipe.Base.Proxy.Relay
{
    public interface IRelayService
    {
        IEnumerable<DayEntity> GetOpenningHours(int relayId);
        RelayAddressEntity GetRelay(int relayId);
        IEnumerable<RelayAddressEntity> GetRelaysByGeoLocation(double latitude, double longitude);
        IEnumerable<RelayAddressEntity> GetRelaysByGeoLocation(double latitude, double longitude, int maxSearchRadius);
        IEnumerable<RelayAddressEntity> GetRelaysByGeoLocation(double latitude, double longitude, DateTime dateMin, DateTime dateMax);
        IEnumerable<RelayAddressEntity> GetRelaysGetByZipCode(string zipCode);
        IEnumerable<RelayAddressEntity> GetRelaysGetByZipCode(string zipCode, DateTime dateMin, DateTime dateMax);
        IEnumerable<RelayAddressEntity> GetRelaysNearRelay(int relayId);
        IEnumerable<RelayAddressEntity> GetRelaysNearRelay(int relayId, int maxSearchRadius);
        IEnumerable<RelayAddressEntity> GetRelaysNearRelay(int relayId, DateTime dateMin, DateTime dateMax);
    }
}
