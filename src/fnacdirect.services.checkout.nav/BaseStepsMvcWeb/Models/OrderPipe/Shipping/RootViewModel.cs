using FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Basket;
using FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Common;
using FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Common.Basket;
using FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Common.Payment;
using FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Common.Shipping;
using FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Membership;
using FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Omniture;
using System;
using System.Collections.Generic;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Shipping
{
    public class RootViewModel : BaseRootViewModel, IRootViewModel
    {
        public RootViewModel()
        {
            MemberBirthDate = Maybe.Empty<MemberBirthDateViewModel>();
        }

        public bool IsValid { get; set; }

        public SellerInformationsViewModel Seller { get; set; }

        public BasketViewModel Basket { get; set; }

        public MembershipViewModel Membership { get; set; }

        public PaymentSummaryViewModel PaymentSummary { get; set; }

        public ShippingViewModel Shipping { get; set; }
   
        public Maybe<MemberBirthDateViewModel> MemberBirthDate { get; set; }

        public Maybe<MembershipCardViewModel> AdjustedMembershipCard { get; set; }

        public string PageName { get; set; }

        public TrackingViewModel Tracking { get; set; }

        public RemindersViewModel Reminders { get; set; }
    }
}
