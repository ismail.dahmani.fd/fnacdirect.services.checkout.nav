﻿
using System.Xml.Serialization;

namespace FnacDirect.OrderPipe.Base.Business.OrderDetail
{
    [XmlInclude(typeof(PostalAddress))]
    [XmlInclude(typeof(RelayAddress))]
    [XmlInclude(typeof(ShopAddress))]
    public abstract class BaseAddress
    {
        [XmlIgnore]
        public string Id { get; set; }

        public string Address { get; set; }

        public string City { get; set; }

        public string Country { get; set; }

        public string ZipCode { get; set; }
    }
}
