using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Http.Controllers;
using System.Web.Http.ModelBinding;
using System.Web.Routing;

namespace FnacDirect.OrderPipe.CoreMvcWeb.Http
{
    public class FromOpContextAttribute : ParameterBindingAttribute
    {
        public override HttpParameterBinding GetBinding(HttpParameterDescriptor parameter)
        {
            return parameter.BindWithModelBinding(new OpContextModelBinder());
        }
    }
}
