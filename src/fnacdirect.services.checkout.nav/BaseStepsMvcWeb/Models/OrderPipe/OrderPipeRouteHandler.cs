using Common.Logging;
using FnacDirect.OrderPipe.Config;
using FnacDirect.OrderPipe.CoreRouting.Rules;
using FnacDirect.Technical.Framework.ServiceLocation;
using FnacDirect.Technical.Framework.Web;
using System;
using System.Web;
using System.Web.Routing;
using System.Web.UI;

namespace FnacDirect.OrderPipe.CoreRouting
{
    public class OrderPipeRouteHandler : BaseRouteHandler
    {
        private readonly Func<IOrderPipeInteractiveStepHandlerStrategyLocator> _getOrderPipeInteractiveStepHandlerStrategyLocator;
        private readonly Func<IOrderPipeStepRedirectionStrategyLocator> _getOrderPipeStepRedirectionStrategyLocator;
        private readonly Func<IOrderPipeForceStepStrategyLocator> _getOrderPipeForceStepStrategyLocator;

        private readonly RoutingRulesApplier _routingRulesApplier;
        private readonly IExecutePipeService _executePipe;
        private readonly ILog _log;

        public OrderPipeRouteHandler()
        {
            _getOrderPipeInteractiveStepHandlerStrategyLocator = ServiceLocator.Current.GetInstance<IOrderPipeInteractiveStepHandlerStrategyLocator>;
            _getOrderPipeStepRedirectionStrategyLocator = ServiceLocator.Current.GetInstance<IOrderPipeStepRedirectionStrategyLocator>;
            _getOrderPipeForceStepStrategyLocator = ServiceLocator.Current.GetInstance<IOrderPipeForceStepStrategyLocator>;
            _executePipe = ServiceLocator.Current.GetInstance<IExecutePipeService>();
            _routingRulesApplier = ServiceLocator.Current.GetInstance<RoutingRulesApplier>();
            _log = ServiceLocator.Current.GetInstance<ILog>();
        }

        protected override IHttpHandler InnerGetHttpHandler(RequestContext requestContext)
        {
            var routingRuleHandler = _routingRulesApplier.Apply(requestContext);

            if (requestContext.HttpContext.Request.HttpMethod == "HEAD")
            {
                return new HttpOKHandler();
            }

            if (routingRuleHandler != null)
            {
                return routingRuleHandler;
            }

            var pipeName = GetPipeName(requestContext);
            var resetPipe = ShouldResetPipe(requestContext);

            if (!_executePipe.IsKnownOrderPipe(pipeName))
            {
                return new HttpNotFoundHandler();
            }

            var pipeExecutionResult = _executePipe.ExecuteOrderPipe(pipeName, resetPipe, pipe =>
            {
                var orderPipeForceStepStrategyLocator = _getOrderPipeForceStepStrategyLocator();

                var orderPipeForceStepStrategy = orderPipeForceStepStrategyLocator.GetOrderPipeForceStepStrategyFor(pipe.UIDescriptor);

                return orderPipeForceStepStrategy.GetForceStepFor(pipe, requestContext);
            });

            try
            {

                var httpHandler = GetHttpHandlerFor(pipeExecutionResult, requestContext);

                httpHandler = ApplyTransferifRequired(httpHandler, pipeExecutionResult, requestContext);

                return httpHandler;
            }
            catch (Exception ex)
            {
                ex.Data.Add("requestContext.Url", requestContext.HttpContext.Request.Url.ToString());
                ex.Data.Add("requestContext.QueryString", requestContext.HttpContext.Request.QueryString.ToString());
                ex.Data.Add("pipeExecutionResult.OpContext.CurrentStep", pipeExecutionResult?.OpContext?.CurrentStep?.Name);
                ex.Data.Add("pipeExecutionResult.OpContext.Group", pipeExecutionResult?.OpContext?.CurrentStep?.Group);
                ex.Data.Add("pipeExecutionResult.OpContext.CurrentStep.UIDescriptor", pipeExecutionResult?.OpContext?.CurrentStep?.UIDescriptor);
                ex.Data.Add("pipeExecutionResult.OpContext.CurrentStep.OF.CurrentStep", pipeExecutionResult?.OpContext?.OF?.CurrentStep);
                ex.Data.Add("pipeExecutionResult.OpContext.PipeName", pipeExecutionResult?.OpContext?.OF?.PipeName);
                _log.Warn("Exception:", ex);

                return new HttpNotFoundHandler();
            }
        }

        protected virtual IHttpHandler GetHttpHandlerFor(PipeExecutionResult pipeExecutionResult, RequestContext requestContext)
        {
            var orderPipeInteractiveStepHandlerStrategyLocator =
              _getOrderPipeInteractiveStepHandlerStrategyLocator();

            var orderPipeInteractiveStepHandlerStrategy =
                orderPipeInteractiveStepHandlerStrategyLocator.GetOrderPipeInteractiveStepHandlerStrategyFor(pipeExecutionResult.CurrentUIDescriptor);

            var orderPipeInteractiveStepHandler =
                orderPipeInteractiveStepHandlerStrategy.GetHttpHandlerFor(pipeExecutionResult.CurrentUIDescriptor, pipeExecutionResult.OpContext, requestContext);

            return orderPipeInteractiveStepHandler;
        }

        protected virtual IHttpHandler ApplyTransferifRequired(IHttpHandler httpHandler, PipeExecutionResult pipeExecutionResult, RequestContext requestContext)
        {
            var currentUIDescriptor = pipeExecutionResult.CurrentUIDescriptor;
            var opContext = pipeExecutionResult.OpContext;

            if (httpHandler is Page
              // ReSharper disable UseIsOperator.1
              && !typeof(IAmAnInteractivePage).IsAssignableFrom(httpHandler.GetType()))
            // ReSharper restore UseIsOperator.1
            {
                var redirectTo = DispatchForContextAndResult(opContext, currentUIDescriptor);

                if (currentUIDescriptor.RedirectMode == RedirectMode.AlwaysTransfert
                  || (currentUIDescriptor.RedirectMode != RedirectMode.Redirect
                      && redirectTo.StartsWith(FnacContext.Current.UrlManagerNew.Sites.CurrentSite.BaseRootUrl.ToString())))
                {
                    httpHandler = GetHttpHandlerFor(pipeExecutionResult, requestContext);
                }
            }

            return httpHandler;
        }

        private string DispatchForContextAndResult(OPContext opContext, UIDescriptor currentUIDescriptor)
        {
            var orderPipeStepRedirectionStrategyLocator = _getOrderPipeStepRedirectionStrategyLocator();

            var orderPipeStepRedirectionStrategy = orderPipeStepRedirectionStrategyLocator.GetOrderPipeStepRedirectionStrategyFor(currentUIDescriptor);

            return orderPipeStepRedirectionStrategy.DoRedirectionFor(currentUIDescriptor, opContext);
        }

        private static string GetPipeName(RequestContext requestContext)
        {
            var pipeName = RouteTable.Routes.GetPipeName();

            return pipeName;
        }

        private static bool ShouldResetPipe(RequestContext requestContext)
        {
            var resetString = requestContext.RouteData.Values["reset"].ToStringOrDefault();

            if (string.IsNullOrEmpty(resetString))
                resetString = requestContext.HttpContext.Request.QueryString["reset"];

            if (string.IsNullOrEmpty(resetString))
                return false;

            return (bool.TryParse(resetString, out var reset) && reset) || resetString.Equals("1");
        }
    }
}
