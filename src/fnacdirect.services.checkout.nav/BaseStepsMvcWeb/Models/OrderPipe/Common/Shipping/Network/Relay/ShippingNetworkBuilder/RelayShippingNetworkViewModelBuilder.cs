using FnacDirect.OrderPipe.Base.BaseModel;
using FnacDirect.OrderPipe.Base.BaseModel.Model;
using FnacDirect.OrderPipe.Base.Business.Availability;
using FnacDirect.OrderPipe.Base.Business.Operations;
using FnacDirect.OrderPipe.Base.Business.Shipping;
using FnacDirect.OrderPipe.Base.Business.Shipping.Relay;
using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.OrderPipe.Base.Model.LineGroups;
using FnacDirect.OrderPipe.Base.Model.Pop;
using FnacDirect.OrderPipe.Base.Model.Solex;
using FnacDirect.OrderPipe.Base.Proxy.Customer;
using FnacDirect.OrderPipe.Base.Proxy.Relay;
using FnacDirect.OrderPipe.Core.Services;
using FnacDirect.Relay.Model;
using FnacDirect.Technical.Framework;
using FnacDirect.Technical.Framework.Web.Mvc;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using SolexContract = FnacDirect.Solex.Shipping.Client.Contract;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Common.Shipping.Relay
{
    public class RelayShippingNetworkViewModelBuilder : IRelayShippingNetworkViewModelBuilder
    {
        private readonly IRelayService _relayService;
        private readonly ICustomerRelayService _customerRelayService;
        private readonly IApplicationContext _applicationContext;
        private readonly IRelayValidityService _relayValidityService;
        private readonly IPopAvailabilityProcessor _popAvailabilityProcessor;
        private readonly IPipeParametersProvider _pipeParametersProvider;
        private readonly IShippingStateService _shippingStateService;
        private readonly ISolexBusiness _solexBusiness;
        private readonly ISolexObjectConverterService _solexObjectConverterService;
        private readonly IShippingChoiceCostService _shippingChoiceCostService;
        private readonly ISwitchProvider _switchProvider;

        private readonly bool _hideLongEstimatedDle;
        // TODO : brancher maxRadius changeset 234498
        private readonly int _maximumSearchRadius;

        public RelayShippingNetworkViewModelBuilder(IRelayService relayService,
                                                    ICustomerRelayService customerRelayService,
                                                    IApplicationContext applicationContext,
                                                    IRelayValidityService relayValidityService,
                                                    IPopAvailabilityProcessor popAvailabilityProcessor,
                                                    IPipeParametersProvider pipeParametersProvider,
                                                    IShippingStateService shippingStateService,
                                                    ISolexBusiness solexBusiness,
                                                    ISwitchProvider switchProvider,
                                                    ISolexObjectConverterService solexObjectConverterService,
                                                    IShippingChoiceCostService shippingChoiceCostService)
        {
            _relayService = relayService;
            _customerRelayService = customerRelayService;
            _applicationContext = applicationContext;
            _relayValidityService = relayValidityService;
            _popAvailabilityProcessor = popAvailabilityProcessor;
            _pipeParametersProvider = pipeParametersProvider;
            _shippingStateService = shippingStateService;
            _solexBusiness = solexBusiness;
            _solexObjectConverterService = solexObjectConverterService;
            _shippingChoiceCostService = shippingChoiceCostService;
            _switchProvider = switchProvider;

            _hideLongEstimatedDle = switchProvider.IsEnabled("front.pop.shipping.hidelongestimateddle");
            _maximumSearchRadius = int.Parse(_applicationContext.GetAppSetting("relay.NearRelaySearch.MaximumSearchRadius"));
        }

        public string ComputeShippingCost(decimal shippingCost)
        {
            return _applicationContext.GetPriceHelper().Format(shippingCost, PriceFormat);
        }

        public decimal SimulateShippingCost(RelayDummyData dummyRelayData, RelayAddressEntity relay)
        {
            var nearestStoreDistance = relay.GetDistanceToNearestStore();
            if (!nearestStoreDistance.HasValue)
                return dummyRelayData.MaxPriceTotal;

            return nearestStoreDistance > dummyRelayData.DistanceToNearestStore ? dummyRelayData.MinPriceTotal : dummyRelayData.MaxPriceTotal;
        }

        public RelayShippingNetworkViewModel Build(PopOrderForm popOrderForm, int sellerId)
        {
            var defaultRelayAddress = _customerRelayService.GetDefaultRelay();

            var popData = popOrderForm.GetPrivateData<PopData>();

            var christmasData = popOrderForm.GetPrivateData<ChristmasData>(false);

            var lineGroups = popOrderForm.LineGroups.Seller(sellerId);

            var relayAddressEntity = defaultRelayAddress != null ? _relayService.GetRelay(defaultRelayAddress.Id) : null;

            var isDefaultRelayAddressValid = (defaultRelayAddress != null && relayAddressEntity != null && CheckRelaysValidity(lineGroups, relayAddressEntity)) || (defaultRelayAddress == null);

            var maybeShippingMethodEvaluationGroup = popOrderForm.ShippingMethodEvaluation.GetShippingMethodEvaluationGroup(sellerId);

            var shippingMethodEvaluationGroup = maybeShippingMethodEvaluationGroup.Value;

            var isCurrentMode = shippingMethodEvaluationGroup.SelectedShippingMethodEvaluationType == ShippingMethodEvaluationType.Relay;

            var relayShippingNetworkViewModel = new RelayShippingNetworkViewModel
            {
                CurrentMode = isCurrentMode
            };

            var relayShippingMethodEvaluationGroup = shippingMethodEvaluationGroup.RelayShippingMethodEvaluationItem.Value;

            var relayChoices = relayShippingMethodEvaluationGroup.Choices;

            var shippingCost = _shippingChoiceCostService.CalculateShippingCost(relayChoices, relayShippingMethodEvaluationGroup.SelectedShippingMethodId);

            var shippingCostFormat = ComputeShippingCost(shippingCost);

            var defaultShippingMethodId = relayShippingMethodEvaluationGroup.GetDefaultShippingMethodId().FirstOrDefault();

            var popAvailabilityProcessorInputs = ComputeAvailabilityProcessorInput(shippingMethodEvaluationGroup, lineGroups, popOrderForm.ShippingMethodEvaluation.ExcludedLogisticTypes);

            var shortEstimatedDeliveryDate = _popAvailabilityProcessor.GetShortGlobalEstimatedDeliveryDate(defaultShippingMethodId, popAvailabilityProcessorInputs);

            relayShippingNetworkViewModel.DefaultShortEstimatedDeliveryDate = shortEstimatedDeliveryDate;

            var deliveryDateMin = popAvailabilityProcessorInputs.Min(sa => sa?.DateMin);

            if (deliveryDateMin.HasValue && _hideLongEstimatedDle)
            {
                var diff = deliveryDateMin.Value - DateTime.Now;
                if (diff.TotalHours > 360)
                {
                    shortEstimatedDeliveryDate = string.Empty;
                }
            }

            var longEstimatedDeliveryDate = ComputeLongEstimatedDelveryDateSolex(popOrderForm, relayShippingMethodEvaluationGroup);

            var displayLongEstimationDeliveryDates = longEstimatedDeliveryDate.Count > 1 && !popData.HasExcludedAvailabilitiesForDeliveryDates;

            var shippingChoice = popOrderForm.GetMainShippingChoice();

            if (!relayShippingMethodEvaluationGroup.IsDummyAddress
                && relayShippingMethodEvaluationGroup.ShippingAddress != null
                && relayShippingMethodEvaluationGroup.ShippingAddress.Identity.HasValue)
            {
                var relay = _relayService.GetRelay(relayShippingMethodEvaluationGroup.ShippingAddress.Identity.Value);

                relayShippingNetworkViewModel.Selected = BuildDetails(popOrderForm, relay, shippingCost, shortEstimatedDeliveryDate, longEstimatedDeliveryDate, defaultRelayAddress != null ? defaultRelayAddress.Id : new int?(), popOrderForm.RelayCellphone);

                relayShippingNetworkViewModel.With(new RelayViewModel(relay, shortEstimatedDeliveryDate, longEstimatedDeliveryDate, displayLongEstimationDeliveryDates)
                {
                    ShippingCost = shippingCostFormat,
                    IsFreeShipping = shippingCost == 0M,
                    IsSelected = relayShippingNetworkViewModel.CurrentMode,
                    IsFavorite = defaultRelayAddress != null && defaultRelayAddress.Id == relay.Id,
                    ShortEstimatedDeliveryDate = CheckDleVisibility(relay.ZipCode) ? shortEstimatedDeliveryDate : string.Empty,
                    CarbonFootprint = shippingChoice?.CarbonFootprint,
                });

                if (christmasData != null && christmasData.HasCurrentPhase)
                {
                    relayShippingNetworkViewModel.HasMarketingEvent = (relayShippingNetworkViewModel.Selected.GetValueOrDefault()?.BeforeXmas).GetValueOrDefault();

                    relayShippingNetworkViewModel.MarketingEventPicto = christmasData.PictoUiresourceKey != null ? _applicationContext.GetMessage(christmasData.PictoUiresourceKey) : string.Empty;
                }

            }
            else
            {
                if (christmasData != null && christmasData.HasCurrentPhase)
                {
                    //Check if we have at least an elligible article (catalog, allowed availabilities, IsTechnicalOnly...)
                    var hasElligibleArticles = lineGroups.GetArticles().All(a => christmasData.GetShippingBeforChristmasEligibility(a.ProductID.GetValueOrDefault()));

                    relayShippingNetworkViewModel.HasMarketingEvent = hasElligibleArticles;

                    relayShippingNetworkViewModel.MarketingEventPicto = christmasData.PictoUiresourceKey != null ? _applicationContext.GetMessage(christmasData.PictoUiresourceKey) : string.Empty;
                }
            }

            if (!relayShippingNetworkViewModel.Relays.Any())
            {
                var relays = ComputeRelaysValidity(lineGroups, _customerRelayService.GetRelayAddresses()).OrderByDescending(r => r.AddressBookId);

                if (!relays.Any() && defaultRelayAddress != null)
                {
                    //Si le client n'a aucun RC valide dans son carnet et qu'il a un RC par defaut, on cherche le plus proche dans un rayon de 10km
                    relays = ComputeRelaysValidity(lineGroups, _relayService.GetRelaysNearRelay(defaultRelayAddress.Id)).OrderBy(r => r.Distance);
                }

                if (relays.Any())
                {
                    var relay = relays.First();

                    relayShippingNetworkViewModel.With(new RelayViewModel(relay, shortEstimatedDeliveryDate, longEstimatedDeliveryDate, displayLongEstimationDeliveryDates)
                    {
                        ShippingCost = shippingCostFormat,
                        IsFreeShipping = shippingCost == 0M,
                        IsFavorite = defaultRelayAddress != null && defaultRelayAddress.Id == relay.Id,
                        ShortEstimatedDeliveryDate = CheckDleVisibility(relay.ZipCode) ? shortEstimatedDeliveryDate : string.Empty,
                        CarbonFootprint = shippingChoice?.CarbonFootprint,

                    })
                     .WarningMessage = !isDefaultRelayAddressValid && isCurrentMode ? _applicationContext.GetMessage("orderpipe.pop.relay.defaultrelay.noteligible") : string.Empty;
                }

                if (relayShippingNetworkViewModel.Selected.HasValue && relayShippingNetworkViewModel.CurrentMode)
                {
                    var matchingRelayViewModel = relayShippingNetworkViewModel.Relays.FirstOrDefault(s => s.Id == relayShippingNetworkViewModel.Selected.Value.Id);

                    if (matchingRelayViewModel != null)
                    {
                        matchingRelayViewModel.IsSelected = true;
                    }
                }
            }

            foreach (var relay in relayShippingNetworkViewModel.Relays)
            {
                relay.WorstDeliveryDate = ComputeWorstDeliveryDate(popAvailabilityProcessorInputs);

                if (relayShippingNetworkViewModel.Selected.HasValue)
                {
                    relayShippingNetworkViewModel.Selected.Value.WorstDeliveryDate = ComputeWorstDeliveryDate(popAvailabilityProcessorInputs);
                }
            }

            if (relayShippingNetworkViewModel.Selected.HasValue)
            {
                var relayShippingViewModel = relayShippingNetworkViewModel.Selected.Value;

                var summary = new SelectedShippingItemSummary
                {
                    Label = _applicationContext.GetMessage("orderpipe.pop.oneclick.shipping.shop.prefix"),
                    WorstDeliveryDate = relayShippingViewModel.WorstDeliveryDate
                };

                if (relayShippingViewModel.DisplayLongEstimatedDeliveryDates)
                {
                    summary.DisplayLongEstimatedDeliveryDates = true;
                    summary.Label += _applicationContext.GetMessage("orderpipe.pop.oneclick.shipping.postal.complex");
                    summary.LongEstimatedDeliveryDate = relayShippingViewModel.LongEstimatedDeliveryDate;
                }

                summary.ShortEstimatedDeliveryDate = relayShippingViewModel.ShortEstimatedDeliveryDate;

                summary.ShippingCost = relayShippingViewModel.ShippingCost;

                relayShippingViewModel.Summary = summary;
            }

            return relayShippingNetworkViewModel;
        }

        public IEnumerable<PopAvailabilityProcessorInput> ComputeAvailabilityProcessorInput(ShippingMethodEvaluationGroup shippingMethodEvaluationGroup, IEnumerable<ILineGroup> lineGroups, IEnumerable<int> excludedLogisticTypes)
        {
            var relayShippingMethodEvaluationItem = shippingMethodEvaluationGroup.RelayShippingMethodEvaluationItem.Value;

            var defaultShippingMethodId = 0;

            var bestChoice = relayShippingMethodEvaluationItem.Choices.GetBestChoice();
            defaultShippingMethodId = bestChoice?.MainShippingMethodId ?? 0;

            if (defaultShippingMethodId == 0)
            {
                return Enumerable.Empty<PopAvailabilityProcessorInput>();
            }

            var articles = lineGroups.GetArticles();
            if (!_switchProvider.IsEnabled("orderpipe.pop.shipping.dlemp.enable"))
            {
                articles = articles.Where(a => a is StandardArticle);
            }
            return new PopAvailabilityProcessorInputListBuilder(articles, defaultShippingMethodId)
                .ExcludeLogisticTypes(excludedLogisticTypes)
                .FilterArticles((article) => (!(article is StandardArticle standardArticle) || !standardArticle.IsEbook))
                .BuildList();
        }

        public DateTime? ComputeWorstDeliveryDate(IEnumerable<PopAvailabilityProcessorInput> availabilities)
        {
            if (availabilities.Any() && availabilities.Any(a => a.DateMax.HasValue))
            {
                return availabilities.Where(a => a.DateMax.HasValue).Max(a => a.DateMax);
            }

            if (availabilities.Any() && availabilities.Any(a => a.DateMin.HasValue))
            {
                return availabilities.Where(a => a.DateMin.HasValue).Max(a => a.DateMin);
            }

            return null;
        }

        public RelayDetailsViewModel BuildDetails(PopOrderForm popOrderForm, RelayAddressEntity relayAddressEntity, decimal shippingCost, string shortEstimatedDeliveryDate, IEnumerable<PopAvailabilityProcessorOutput> longEstimatedDeliveryDate, int? defaultRelayId, string relayCellphone)
        {
            return BuildDetails(popOrderForm, relayAddressEntity, shippingCost, shortEstimatedDeliveryDate, longEstimatedDeliveryDate, defaultRelayId, relayCellphone, null);
        }

        public RelayDetailsViewModel BuildDetails(PopOrderForm popOrderForm, RelayAddressEntity relay, decimal shippingCost, string shortEstimatedDeliveryDate, IEnumerable<PopAvailabilityProcessorOutput> longEstimatedDeliveryDate, int? defaultRelayId, string relayCellphone, Position referenceGeoPosition)
        {
            var popData = popOrderForm.GetPrivateData<PopData>();

            var displayLongEstimatedDeliveryDates = longEstimatedDeliveryDate.Count() > 1 && !popData.HasExcludedAvailabilitiesForDeliveryDates;

            var shippingChoice = popOrderForm.GetMainShippingChoice();

            var relayDetailsDetailViewModel = new RelayDetailsViewModel(relay, shortEstimatedDeliveryDate, longEstimatedDeliveryDate, displayLongEstimatedDeliveryDates)
            {
                ShippingCost = ComputeShippingCost(shippingCost),
                IsFreeShipping = shippingCost == 0M,
                IsFavorite = defaultRelayId.HasValue && defaultRelayId.Value == relay.Id,
                Latitude = relay.Latitude,
                Longitude = relay.Longitude,
                CarbonFootprint = shippingChoice?.CarbonFootprint,
            };

            relayDetailsDetailViewModel.Timetables = GetTimeTables(relay.Id);

            if (!string.IsNullOrEmpty(relayCellphone))
            {
                relayDetailsDetailViewModel.CellPhoneNumber = relayCellphone;
            }
            else
            {
                relayDetailsDetailViewModel.CellPhoneNumber = popOrderForm.UserContextInformations.CellPhone;
            }

            if (!CheckDleVisibility(relay.ZipCode))
            {
                relayDetailsDetailViewModel.ShortEstimatedDeliveryDate = string.Empty;
            }

            relayDetailsDetailViewModel.RegexPhoneNumber = _pipeParametersProvider.Get("orderpipe.relaydetails.cellphone.regexformat", "^(06|07)([0-9]){8}$");
            //Calcul de la distance entre la position de référence et le relais
            if (referenceGeoPosition != null)
            {
                var relayGeoPoint = new Position(relay.Latitude, relay.Longitude);
                relayDetailsDetailViewModel.Distance = FormatDistance((decimal)referenceGeoPosition.GetDistanceFrom(relayGeoPoint));
            }
            return relayDetailsDetailViewModel;
        }

        private bool CheckDleVisibility(string zipCode)
        {
            return string.IsNullOrEmpty(_shippingStateService.GetStateByZipCode(zipCode));
        }

        public IEnumerable<TimetableViewModel> GetTimeTables(int relayId)
        {
            var openingHours = _relayService.GetOpenningHours(relayId);
            return openingHours.ConvertToTimetableViewModel(_applicationContext).ToList();
        }

        private IEnumerable<RelayAddressEntity> ComputeRelaysValidity(IEnumerable<ILineGroup> lineGroups, IEnumerable<RelayAddressEntity> relayEntities)
        {
            return relayEntities.Where(relayAddressEntity => _relayValidityService.CheckRelayValidity(relayAddressEntity, lineGroups)).ToList();
        }

        private bool CheckRelaysValidity(IEnumerable<ILineGroup> lineGroups, RelayAddressEntity relayEntity)
        {
            return _relayValidityService.CheckRelayValidity(relayEntity, lineGroups);
        }

        private string FormatDistance(decimal distance)
        {
            if ((int)(distance * 1000) == 0)
            {
                return null;
            }

            return distance < 1
                   ? (string.Concat("{distance} ", _applicationContext.GetMessage("orderpipe.index.shiptostore.meter", null))).Replace("{distance}", ((int)(distance * 1000)).ToString(CultureInfo.InvariantCulture))
                   : (string.Concat("{distance} ", _applicationContext.GetMessage("orderpipe.index.shiptostore.km", null))).Replace("{distance}", distance.ToString("##.##"));

        }

        public IEnumerable<RelayDetailsViewModel> BuildWithSolex(IEnumerable<RelayAddressEntity> relays, PopOrderForm popOrderForm, Maybe<RelayShippingMethodEvaluationItem> relayShippingMethodEvaluationItem, IEnumerable<ILineGroup> lineGroups, Position referenceGeoPoint, int selectedRelayId)
        {
            var items = new List<SolexContract.Item>();

            var freeShippings = popOrderForm.ShippingMethodEvaluation.FreeShipping;

            int.TryParse(_applicationContext.GetAppSetting("relay.id"), out var toRelayShippingMethodId);

            var articles = lineGroups.GetArticles();

            foreach (var article in articles)
            {
                items.Add(new SolexContract.Item
                {
                    Expedition = new SolexContract.Expedition(),
                    Identifier = new SolexContract.Identifier { Prid = article.ProductID },
                    Quantity = (int)article.Quantity
                });
            }

            var shippingNetworks = new SolexContract.ShippingNetworks();

            foreach (var relay in relays)
            {
                shippingNetworks.Relay.Addresses.Add(new SolexContract.RelayAddress
                {
                    Id = relay.Id
                });
            }

            var evaluateShoppingCartResult = _solexBusiness.EvaluateShoppingCart(articles, freeShippings, shippingNetworks, "orderpipe.relayshippingnetworkviewmodelbuilder");

            var solexRelayAddresses = evaluateShoppingCartResult.ShippingNetworks.Relay.Addresses;

            var relayDetailsViewModels = new List<RelayDetailsViewModel>();

            foreach (var solexRelayAddress in solexRelayAddresses)
            {
                decimal shippingCost = 0;

                var choice = _solexObjectConverterService.ConvertSolexChoiceToShippingChoice(solexRelayAddress.Choices.FirstOrDefault().FirstOrDefault());
                var relay = relays.FirstOrDefault(s => s.Id == solexRelayAddress.Id);

                var availabilityProcessorInputs = new List<PopAvailabilityProcessorInput>();

                if (choice == null)
                {
                    continue;
                }

                shippingCost = choice.TotalPrice.Cost;

                foreach (var item in items)
                {
                    if (!(articles.FirstOrDefault(a => a.ProductID == item.Identifier.Prid) is BaseArticle article))
                    {
                        continue;
                    }

                    if (!_switchProvider.IsEnabled("orderpipe.pop.shipping.dlemp.enable")
                        && !(article is StandardArticle))
                    {
                        continue;
                    }

                    var parcel = choice.GetParcel(article);

                    if (parcel != null)
                    {
                        var availability = new PopAvailabilityProcessorInput(article, parcel.Source.DeliveryDateMin, parcel.Source.DeliveryDateMin);

                        availabilityProcessorInputs.Add(availability);
                    }
                }

                var shortEstimatedDeliveryDate = _popAvailabilityProcessor.GetShortGlobalEstimatedDeliveryDate(toRelayShippingMethodId, availabilityProcessorInputs);

                var deliveryDateMin = availabilityProcessorInputs.Min(sa => sa?.DateMin);
                if (deliveryDateMin.HasValue && _hideLongEstimatedDle)
                {
                    var diff = deliveryDateMin.Value - DateTime.Now;
                    if (diff.TotalHours > 360)
                    {
                        shortEstimatedDeliveryDate = string.Empty;
                    }
                }
                IEnumerable<PopAvailabilityProcessorOutput> longEstimatedDeliveryDate = new List<PopAvailabilityProcessorOutput>();

                longEstimatedDeliveryDate = ComputeLongEstimatedDelveryDateSolex(popOrderForm, relayShippingMethodEvaluationItem);

                var defaultRelay = _customerRelayService.GetDefaultRelay();

                var detail = BuildDetails(popOrderForm, relay, shippingCost, shortEstimatedDeliveryDate, longEstimatedDeliveryDate, defaultRelay != null ? defaultRelay.Id : new int?(), popOrderForm.RelayCellphone, referenceGeoPoint);

                detail.IsSelected = (selectedRelayId == detail.Id);

                detail.WorstDeliveryDate = ComputeWorstDeliveryDate(availabilityProcessorInputs);

                relayDetailsViewModels.Add(detail);
            }

            return relayDetailsViewModels;
        }

        private List<PopAvailabilityProcessorOutput> ComputeLongEstimatedDelveryDateSolex(PopOrderForm popOrderForm, Maybe<RelayShippingMethodEvaluationItem> relayShippingMethodEvaluationItem)
        {
            var longEstimatedDeliveryDate = new List<PopAvailabilityProcessorOutput>();

            var choices = new List<ShippingChoice>();
            if (relayShippingMethodEvaluationItem.HasValue && relayShippingMethodEvaluationItem.Value.Choices.Any())
            {
                choices = relayShippingMethodEvaluationItem.Value.Choices.ToList();
            }

            var excludedLogisticTypes = popOrderForm.ShippingMethodEvaluation.ExcludedLogisticTypes;
            foreach (var relayChoice in choices)
            {
                var orderedParcels = relayChoice.Parcels.OrderBy(p => p.Value.Source.DeliveryDateMax).ToList();

                // cas fourchette commune mais d'entrepots différents
                /**
                 * TODO: (hors NR) Une meilleure solution peut être trouvée pour ce cas, en utilisant la méthode GetLongGlobalEstimatedDeliveryDate()
                 * avec la liste "availabilities" (au lieu de faire l'appel pour chaque parcel, ligne 520-521).
                 * Cependant, un effet de bord est possible en utilisant cette solution. Il vaut mieux donc bien tout tester (et pas en période de NR).
                 */
                var dateMin = orderedParcels.MinOrDefault(p => p.Value.Source.DeliveryDateMin);
                var dateMax = orderedParcels.MaxOrDefault(p => p.Value.Source.DeliveryDateMax);

                if (orderedParcels.All(p => p.Value.Source.DeliveryDateMin == dateMin) &&
                    orderedParcels.All(p => p.Value.Source.DeliveryDateMax == dateMax))
                {
                    orderedParcels.RemoveRange(1, orderedParcels.Count - 1);
                }

                foreach (var parcel in orderedParcels)
                {
                    var parcelId = parcel.Key;
                    var parcelIsExcluded = relayChoice.Groups
                        .Where(g => excludedLogisticTypes.Contains(g.Id))
                        .SelectMany(x => x.Items)
                        .Any(i => i.ParcelId == parcelId);

                    if (parcelIsExcluded)
                    {
                        continue;
                    }

                    var shippingMethodId = parcel.Value.ShippingMethodId;

                    var articlesForParcel = relayChoice.Groups.SelectMany(x => x.Items).Where(i => i.ParcelId == parcelId).Select(i => i.Identifier).ToList();

                    var lineGroups = popOrderForm.LineGroups;
                    var articles = lineGroups.GetArticles();
                    if (!_switchProvider.IsEnabled("orderpipe.pop.shipping.dlemp.enable"))
                    {
                        articles = articles.Where(a => a is StandardArticle);
                    }
                    var availabilityList = new PopAvailabilityProcessorInputListBuilder(articles, shippingMethodId)
                        .FilterArticles((article) => (articlesForParcel.Any(a => a.Prid == article.ProductID)))
                        .WithShippingMethodId()
                        .BuildList();

                    longEstimatedDeliveryDate.AddRange(
                        _popAvailabilityProcessor.GetLongGlobalEstimatedDeliveryDate(availabilityList, false));
                }
            }
            return longEstimatedDeliveryDate;
        }

        protected virtual PriceFormat PriceFormat { get { return PriceFormat.CurrencyAsDecimalSeparator; } }

    }
}
