using FnacDirect.OrderPipe.BaseMvc.Web.Models.OrderPipe.Common.Payment;
using System.Collections.Generic;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Common.Payment
{
    public class PaymentViewModel
    {
        public bool IsOneClick { get; set; }
        public PaymentSummaryViewModel Summary { get; set; }

        public MessageViewModel Messages { get; set; }        

        public IEnumerable<TabPaymentMethodsModel> Tabs { get; set; }

        public IEnumerable<TabPaymentMethodsModel> SecondaryBillingMethodsTabs { get; set; }

        public IEnumerable<UnavailablePaymentViewModel> UnavailableTabs { get; set; }

        public bool DisplayCodePromo { get; set; }

        public TermsAndConditionsTypeViewModel TermsAndConditions { get; internal set; }
    }
}
