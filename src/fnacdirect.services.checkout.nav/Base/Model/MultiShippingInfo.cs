using System;

namespace FnacDirect.OrderPipe.Base.Model
{
    [Serializable]
    public class MultiShippingInfo
    {
        /// <summary>
        /// LineGroup en cours d'édition dans les écrans de livraisons multi-adresses
        /// </summary>
        public string SelectedLineGroupUniqueId { get; set; }

        /// <summary>
        /// L'article en cours pendant le choix d'une autre adresse de livraison
        /// </summary>
        public string SelectedArticlePRID { get; set; }

        /// <summary>
        /// Adresse de livraison en cours d'édition
        /// (son utilisation est à vérifier)
        /// </summary>
        public int? SelectedAdresseID { get; set; }

        /// <summary>
        /// Référence de l'adresse sélectionnée sur l'écran du choix d'adresse de livraison
        /// </summary>
        public string SelectedAddressReference { get; set; } = string.Empty;
    }
}
