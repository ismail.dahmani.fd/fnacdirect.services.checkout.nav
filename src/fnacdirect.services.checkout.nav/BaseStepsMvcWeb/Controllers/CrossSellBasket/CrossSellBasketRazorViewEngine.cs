﻿using FnacDirect.Technical.Framework.Web.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace FnacDirect.OrderPipe.BaseMvc.Web.CrossSellBasket
{
    public class CrossSellBasketRazorViewEngine : LocalizationRazorViewEngine
    {
        public CrossSellBasketRazorViewEngine()
            : base("OrderPipe/CrossSellBasket",
                   "OrderPipe/Shared")
        {
        }
    }
}
