﻿namespace FnacDirect.OrderPipe.Base.Business.LogisticLineGroups.Internals.Initializers
{
    public interface ILogisticLineGroupInitializerFactory
    {
        ILogisticLineGroupInitializer Get(ILogisticLineGroupGenerationProcessContext context);
    }

    public interface ILogisticLineGroupInitializerFactory<out T> : ILogisticLineGroupInitializerFactory
            where T : BaseLogisticLineGroupInitializer
    {
    }
}
