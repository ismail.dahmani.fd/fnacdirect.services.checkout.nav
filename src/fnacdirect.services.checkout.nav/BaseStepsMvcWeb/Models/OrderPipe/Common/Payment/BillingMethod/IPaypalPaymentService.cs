using FnacDirect.OrderPipe.Base.Model;

namespace FnacDirect.OrderPipe.Base.Business.Payment
{
    public interface IPaypalPaymentService
    {
        bool IsAllowed(PopOrderForm popOrderForm);
    }
}
