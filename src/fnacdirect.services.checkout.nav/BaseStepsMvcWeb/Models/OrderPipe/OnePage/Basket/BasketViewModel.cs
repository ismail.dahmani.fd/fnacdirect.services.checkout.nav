
using FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Common.Basket;
using FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Common.Payment;
using System.Collections.Generic;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.OnePage.Basket
{
    public class BasketViewModel
    {
        public string BackLinkUrl { get; set; }

        private readonly List<Pop.Basket.BasketSellerViewModel> _basketSellerViewModels
            = new List<Pop.Basket.BasketSellerViewModel>();

        public IReadOnlyCollection<Pop.Basket.BasketSellerViewModel> Sellers
        {
            get
            {
                return _basketSellerViewModels.AsReadOnly();
            }
        }

        public PaymentSummaryViewModel PaymentSummaryViewModel { get; set; }

        public BasketViewModel With(params Pop.Basket.BasketSellerViewModel[] basketSellerViewModels)
        {
            _basketSellerViewModels.AddRange(basketSellerViewModels);

            return this;
        }
        public OrderExternalReferenceViewModel OrderExternalReference { get; set; }

        public int ProductCount { get; set; }

        public bool DisplayBirthDatePopin { get; set; }
    }
}
