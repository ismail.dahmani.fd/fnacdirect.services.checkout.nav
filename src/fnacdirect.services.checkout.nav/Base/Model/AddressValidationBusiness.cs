using FnacDirect.Customer.BLL.FiscalNumber;
using FnacDirect.Customer.BLL.FiscalNumber.Validators;
using FnacDirect.Customer.Model;
using FnacDirect.OrderPipe;
using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.Technical.Framework.Web.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;

namespace FnacDirect.OrderPipe.Base.Business
{
    public interface IAddressValidationBusiness
    {
        IEnumerable<string> GetMissingFieldsName(IAddressViewModelFieldsValidation address);
        bool IsAddressValid(IAddressViewModelFieldsValidation address);
        bool IsAddressValid(AddressEntity addressEntity);
    }

    public class AddressValidationBusiness : IAddressValidationBusiness
    {
        private IApplicationContext _applicationContext;
        private IFiscalNumberFactory _fiscalNumberFactory;
        private const string ESPAGNE = Constants.LocalCountry.ESPAGNE;
        private const string PORTUGAL = Constants.LocalCountry.PORTUGAL;

        public AddressValidationBusiness(IApplicationContext applicationContext, IFiscalNumberFactory fiscalNumberFactory)
        {
            _applicationContext = applicationContext;
            _fiscalNumberFactory = fiscalNumberFactory;
        }

        public IEnumerable<string> GetMissingFieldsName(IAddressViewModelFieldsValidation address)
        {
            var FieldsStrategy = GetFieldsStrategy();

            return FieldsStrategy.GetFields(address).Select(f => f.Name);
        }

        public bool IsAddressValid(AddressEntity addressEntity)
        {
            var model = new AddressViewModelFieldsValidationDto(addressEntity);
            return IsAddressValid(model);
        }

        public bool IsAddressValid(IAddressViewModelFieldsValidation address)
        {
            var FieldsStrategy = GetFieldsStrategy();
            bool isValid = !FieldsStrategy.GetFields(address).Any();

            return isValid;
        }

        internal IManageFieldsStrategy GetFieldsStrategy()
        {
            var localCountry = _applicationContext?.GetLocalCountry();

            switch (localCountry)
            {
                case ESPAGNE:
                    return new SpainFieldsChecker(_fiscalNumberFactory);
                case PORTUGAL:
                    return new PortugalFieldsChecker(_fiscalNumberFactory);
                default:
                    return new DefaultFieldsChecker();
            }
        }

        internal interface IManageFieldsStrategy
        {
            IEnumerable<FieldInfo> GetFields(IAddressViewModelFieldsValidation addressViewModel);

        }
        internal class DefaultFieldsChecker : IManageFieldsStrategy
        {
            private readonly IEnumerable<FieldInfo> emptyList = new List<FieldInfo>();
            protected string CountryCode { get; private set; }
            public DefaultFieldsChecker() { }
            public DefaultFieldsChecker(string countryCode)
            {
                CountryCode = countryCode;
            }
            public IEnumerable<FieldInfo> GetFields(IAddressViewModelFieldsValidation addressViewModel)
            {
                var defaultMissingFields = GetDefaultMissingFields(addressViewModel);
                var specificMissingFields = GetSpecificMissingFields(addressViewModel);
                var result = defaultMissingFields.Concat(specificMissingFields);
                return result;
            }

            protected IEnumerable<FieldInfo> GetDefaultMissingFields(IAddressViewModelFieldsValidation addressViewModel)
            {
                if (string.IsNullOrEmpty(addressViewModel.GenderId) || addressViewModel.GenderId == "0") { yield return CreateField(nameof(addressViewModel.GenderId)); }
                if (string.IsNullOrEmpty(addressViewModel.FirstName)) { yield return CreateField(nameof(addressViewModel.FirstName)); }
                if (string.IsNullOrEmpty(addressViewModel.LastName)) { yield return CreateField(nameof(addressViewModel.LastName)); }
                if (string.IsNullOrEmpty(addressViewModel.ZipCode)) { yield return CreateField(nameof(addressViewModel.ZipCode)); }
                if (string.IsNullOrEmpty(addressViewModel.City)) { yield return CreateField(nameof(addressViewModel.City)); }
                if (string.IsNullOrEmpty(addressViewModel.Phone)) { yield return CreateField(nameof(addressViewModel.Phone)); }
                if (string.IsNullOrEmpty(addressViewModel.Address)) { yield return CreateField(nameof(addressViewModel.Address)); }
            }
            protected virtual IEnumerable<FieldInfo> GetSpecificMissingFields(IAddressViewModelFieldsValidation addressViewModel) { return emptyList; }
        }
        internal class SpainFieldsChecker : DefaultFieldsChecker
        {
            private readonly IFiscalNumberFactory _fiscalNumberFactory;
            public SpainFieldsChecker(IFiscalNumberFactory fiscalNumberFactory) : base(Constants.LocalCountry.ESPAGNE)
            {
                _fiscalNumberFactory = fiscalNumberFactory;
            }

            protected override IEnumerable<FieldInfo> GetSpecificMissingFields(IAddressViewModelFieldsValidation addressViewModel)
            {
                const int PARTICULIER = 1;
                const int ENTREPRISE = 2;

                var legalStatusValid = addressViewModel.LegalStatus != PARTICULIER && addressViewModel.LegalStatus != ENTREPRISE;
                if (legalStatusValid) { yield return CreateField(nameof(addressViewModel.LegalStatus)); }

                var legalstatus = addressViewModel.LegalStatus;
                if (legalstatus == ENTREPRISE && string.IsNullOrEmpty(addressViewModel.CompanyName))
                {
                    yield return CreateField(nameof(addressViewModel.CompanyName));
                }

                if (addressViewModel.CountryId == Constants.Countries.Spain
                    && string.IsNullOrEmpty(addressViewModel.State))
                {
                    yield return CreateField(nameof(addressViewModel.State));
                    yield return CreateField(nameof(addressViewModel.DocumentCountryId));
                }

                if (string.IsNullOrEmpty(addressViewModel.TaxIdNumber)) { yield return CreateField(nameof(addressViewModel.TaxIdNumber)); }
                else
                {
                    Enum.TryParse(addressViewModel.TaxNumberType, out FiscalNumberType fiscalNumberType);
                    if (fiscalNumberType == FiscalNumberType.Cif
                        || fiscalNumberType == FiscalNumberType.Nie
                        || fiscalNumberType == FiscalNumberType.Nif
                        || fiscalNumberType == FiscalNumberType.Passport)
                    {
                        var validator = GetValidator(fiscalNumberType);
                        if (!validator.IsValid(addressViewModel.TaxIdNumber))
                        {
                            yield return CreateField(nameof(addressViewModel.TaxNumberType));
                            yield return CreateField(nameof(addressViewModel.TaxIdNumber));
                            yield return CreateField(nameof(addressViewModel.DocumentCountryId));
                        }
                    }
                }

                const string TaxNumberTypeMissing = "-1";
                if (string.IsNullOrEmpty(addressViewModel.TaxNumberType) || addressViewModel.TaxNumberType == TaxNumberTypeMissing)
                {
                    yield return CreateField(nameof(addressViewModel.TaxNumberType));
                    yield return CreateField(nameof(addressViewModel.TaxIdNumber));
                    yield return CreateField(nameof(addressViewModel.DocumentCountryId));
                }          
            }

            private IFiscalNumberValidator GetValidator(FiscalNumberType fiscalNumberType)
            {
                return _fiscalNumberFactory.GetValidator(fiscalNumberType, CountryCode);
            }
        }
        internal class PortugalFieldsChecker : DefaultFieldsChecker
        {
            private readonly IFiscalNumberFactory _fiscalNumberFactory;

            public PortugalFieldsChecker(IFiscalNumberFactory fiscalNumberFactory) : base(Constants.LocalCountry.PORTUGAL)
            {
                _fiscalNumberFactory = fiscalNumberFactory;
            }

            protected override IEnumerable<FieldInfo> GetSpecificMissingFields(IAddressViewModelFieldsValidation addressViewModel)
            {
                if (string.IsNullOrEmpty(addressViewModel.TaxIdNumber))
                {
                    yield return CreateField(nameof(addressViewModel.TaxNumberType));
                    yield return CreateField(nameof(addressViewModel.TaxIdNumber));
                    yield return CreateField(nameof(addressViewModel.DocumentCountryId));
                }
                else
                {
                    Enum.TryParse(addressViewModel.TaxNumberType, out FiscalNumberType fiscalNumberType);
                    if (fiscalNumberType == FiscalNumberType.Nif)
                    {
                        var validator = GetValidator(fiscalNumberType);
                        if (!validator.IsValid(addressViewModel.TaxIdNumber))
                        {
                            yield return CreateField(nameof(addressViewModel.TaxNumberType));
                            yield return CreateField(nameof(addressViewModel.TaxIdNumber));
                            yield return CreateField(nameof(addressViewModel.DocumentCountryId));
                        }
                    }
                }
            }

            private IFiscalNumberValidator GetValidator(FiscalNumberType fiscalNumberType)
            {
                return _fiscalNumberFactory.GetValidator(fiscalNumberType, CountryCode);
            }
        }
        private static FieldInfo CreateField(string name)
        {
            return new FieldInfo
            {
                Name = name
            };
        }

        internal class FieldInfo
        {
            public string Name { get; set; }
        }
    }
}
