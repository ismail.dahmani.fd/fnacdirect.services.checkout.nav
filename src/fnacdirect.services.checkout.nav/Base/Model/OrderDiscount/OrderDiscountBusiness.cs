using FnacDirect.OrderPipe.Base.DAL;

namespace FnacDirect.OrderPipe.Base.Business
{
    public class OrderDiscountBusiness : IOrderDiscountBusiness
    {
        private readonly IPromotionDal _promotionDal;


        public OrderDiscountBusiness(IPromotionDal promotionDal)
        {
            _promotionDal = promotionDal;
        }


        public void AddOrderDiscount(
            int orderId,
            int orderDetailId,
            int discountTypeId,
            int discountReasonId,
            decimal discountAmount,
            decimal? s18DiscountAmount = null,
            string s18DiscountCode = null,
            string s18DiscountType = null,
            int? typeId = null,
            string code = null,
            int? templateId = null,
            string respComment = null,
            string sellerComment = null)
        {
            _promotionDal.AddOrderDiscount(orderId, orderDetailId, discountTypeId, discountReasonId, discountAmount, s18DiscountAmount, s18DiscountCode, s18DiscountType, typeId, code, templateId, respComment, sellerComment);
        }
    }
}
