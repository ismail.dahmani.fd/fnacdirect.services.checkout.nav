using System;
using System.Collections.Generic;
using System.Text;
using System.ServiceModel;

namespace FnacDirect.Stock.IModel
{
    [ServiceContract(Namespace="uri:fnac/stock/reservation")]
    public interface IReservationService
    {
        [OperationContract(IsOneWay=true)]
        void Reserve(string orderUserReference);
    }
}
