﻿using System;

namespace FnacDirect.OrderPipe.Base.Business.Stock
{
    public class StockReservationServiceFactory : IStockReservationServiceFactory
    {
        private readonly Func<Stock2DatabaseReservationService> _stock2DatabaseReservationServiceFactory;
        private readonly Func<WebUpReservationService> _webUpReservationServiceFactory;

        public StockReservationServiceFactory(Func<Stock2DatabaseReservationService> stock2DatabaseReservationServiceFactory,
                                              Func<WebUpReservationService> webUpReservationServiceFactory)
        {
            _stock2DatabaseReservationServiceFactory = stock2DatabaseReservationServiceFactory;
            _webUpReservationServiceFactory = webUpReservationServiceFactory;
        }

        public IStockReservationService Get(StockReservationMode stockReservationMode)
        {
            switch(stockReservationMode)
            {
                case StockReservationMode.WebUpService:
                    return _webUpReservationServiceFactory();
                case StockReservationMode.Stock2Database:
                default:
                    return _stock2DatabaseReservationServiceFactory();
            }
        }
    }
}
