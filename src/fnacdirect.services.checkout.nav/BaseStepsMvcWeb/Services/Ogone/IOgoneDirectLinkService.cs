using FnacDirect.Ogone.Model;
using FnacDirect.Technical.Framework.CoreServices;
using System.Collections.Generic;

namespace FnacDirect.OrderPipe.Base.Proxy.Ogone
{
    public interface IOgoneDirectLinkService
    {
        SerializableDictionary<string, string> CallDirectLink(string url, Dictionary<string, string> dataDictionary, OrderTransactionOgone otDirectLinkData, bool saveAlias, OPContext opContext);

        bool IsUncertainStatus(string status);

        bool PaymentUncertainIsAllowed(OPContext opContext);
    }
}
