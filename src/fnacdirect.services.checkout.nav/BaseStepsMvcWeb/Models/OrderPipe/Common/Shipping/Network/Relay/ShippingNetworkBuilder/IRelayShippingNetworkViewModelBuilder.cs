using FnacDirect.OrderPipe.Base.BaseModel;
using FnacDirect.OrderPipe.Base.BaseModel.Model;
using FnacDirect.OrderPipe.Base.Business.Availability;
using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.OrderPipe.Base.Model.LineGroups;
using FnacDirect.Relay.Model;
using FnacDirect.Technical.Framework;
using System;
using System.Collections.Generic;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Common.Shipping.Relay
{
    public interface IRelayShippingNetworkViewModelBuilder : IShippingNetworkViewModelBuilder<RelayShippingNetworkViewModel>
    {
        DateTime? ComputeWorstDeliveryDate(IEnumerable<PopAvailabilityProcessorInput> availabilities);
        RelayDetailsViewModel BuildDetails(PopOrderForm popOrderForm, RelayAddressEntity relayAddressEntity, decimal shippingCost, string shortEstimatedDeliveryDate, IEnumerable<PopAvailabilityProcessorOutput> longEstimatedDeliveryDate, int? defaultRelayId, string relayCellphone);
        RelayDetailsViewModel BuildDetails(PopOrderForm popOrderForm, RelayAddressEntity relay, decimal shippingCost, string shortEstimatedDeliveryDate, IEnumerable<PopAvailabilityProcessorOutput> longEstimatedDeliveryDate, int? defaultRelayId, string relayCellphone, Position referenceGeoPosition);
        decimal SimulateShippingCost(RelayDummyData dummyRelayData, RelayAddressEntity relay);
        string ComputeShippingCost(decimal shippingCost);
        IEnumerable<PopAvailabilityProcessorInput> ComputeAvailabilityProcessorInput(ShippingMethodEvaluationGroup shippingMethodEvaluationGroup, IEnumerable<ILineGroup> lineGroups, IEnumerable<int> excludedLogisticTypes);
        IEnumerable<TimetableViewModel> GetTimeTables(int relayId);

        IEnumerable<RelayDetailsViewModel> BuildWithSolex(IEnumerable<RelayAddressEntity> relays, PopOrderForm popOrderForm, Maybe<RelayShippingMethodEvaluationItem> relayShippingMethodEvaluationItem, IEnumerable<ILineGroup> lineGroups, Position referenceGeoPoint,int selectedRelayId);
    }
}
