﻿using FnacDirect.OrderPipe.Base.Model;
using System.Collections.Generic;

namespace FnacDirect.OrderPipe.Base.LineGroups
{
    public interface ILineGroupHeader
    {
        WrapList AvailableWraps { get; set; }
        ILineGroupInformations Informations { get; }
        SellerInformation Seller { get; set; }
        OrderTypeEnum OrderType { get; set; }
        int OrderOrigin { get; set; }
        LogisticTypeList LogisticTypes { get; }
        string UniqueId { get; set; }
        VatCountry VatCountry { get; set; }
        GlobalPrices GlobalPrices { get; set; }
        IDictionary<int, decimal> BillingMethodConsolidation { get; }
        bool IsVoluminous { get; set; }
        decimal RemainingTotal { get; set; }
    }
}
