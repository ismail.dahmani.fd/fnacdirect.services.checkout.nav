﻿using FnacDirect.OrderPipe.Base.Model;

namespace FnacDirect.OrderPipe.Base.Business.Payment
{
    public interface IPromotionCodeService
    {
        VoucherBusiness.CheckCode CheckVoucher(string cuu, PopOrderForm popOrderForm);
        void AddCode(string code,OPContext opContext);
        CodeType CodeType(string code);
        void RemoveCode(string code,PopOrderForm popOrderForm);        
    }
}