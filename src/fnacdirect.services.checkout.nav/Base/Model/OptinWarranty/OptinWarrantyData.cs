using System.Collections.Generic;

namespace FnacDirect.OrderPipe.Base.Model.OptinWarranty
{
    public class OptinWarrantyData : GroupData
    {
        public List<int> ArticlesWithOptinWarrantyChecked { get; set; }
    }
}
