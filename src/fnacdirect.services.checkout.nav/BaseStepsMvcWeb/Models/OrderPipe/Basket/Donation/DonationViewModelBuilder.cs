﻿using FnacDirect.Contracts.Online.IModel;
using FnacDirect.Contracts.Online.Model;
using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.Technical.Framework.CoreServices;
using FnacDirect.Technical.Framework.Web;
using FnacDirect.Technical.Framework.Web.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using Vanilla.Middle.Arborescence;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Basket
{
    /// <summary>
    /// Push Donation Builder
    /// </summary>
    public class DonationViewModelBuilder : IDonationViewModelBuilder
    {
        private readonly ISwitchProvider _switchProvider;
        private readonly ITreeLeafServices _treeLeafServices;
        private readonly IArticleBusiness3 _articleBusiness3;
        private readonly IApplicationContext _applicationContext;
        private readonly IUrlManager _urlManager;

        public DonationViewModelBuilder(ISwitchProvider switchProvider,
                                        ITreeLeafServices treeLeafServices,
                                        IArticleBusiness3 articleBusiness3,
                                        IApplicationContext applicationContext,
                                        IUrlManager urlManager)
        {
            _switchProvider = switchProvider;
            _treeLeafServices = treeLeafServices;
            _articleBusiness3 = articleBusiness3;
            _applicationContext = applicationContext;
            _urlManager = urlManager;
        }

        public DonationsViewModel Build(PopOrderForm popOrderForm, OPContext opContext)
        {
            var model = new DonationsViewModel();

            if (!_switchProvider.IsEnabled("orderpipe.pop.solidarydonation"))
            {
                model.DisplayDonationPush = false;
                return model;
            }

            try
            {
                model.Donations = GetDonationArticleViewModel(opContext);
                model.DisplayDonationPush = model.Donations.Any() && !HasDonationInBasket(popOrderForm, model.Donations);
            }
            catch (Exception ex)
            {
                Logging.Current.WriteError("Error while retrieving donation articles", ex);

                model.Donations = Enumerable.Empty<DonationViewModel>();
                model.DisplayDonationPush = false;
            }

            return model;
        }

        private bool HasDonationInBasket(PopOrderForm popOrderForm, IEnumerable<DonationViewModel> donations)
        {
            return popOrderForm.LineGroups.GetArticles().Any(a => donations.Any(d => d.ProductId == a.ProductID));
        }

        private IEnumerable<DonationViewModel> GetDonationArticleViewModel(OPContext opContext)
        {
            var siteContext = _applicationContext.GetSiteContext();

            var donations = new List<DonationViewModel>();

            var nodeId = opContext.Pipe.GlobalParameters.GetAsInt("donation.nodeId");

            var leaves = _treeLeafServices.GetLeaves(nodeId, Vanilla.Middle.Arborescence.Enums.LeafType.Article);

            foreach (var leaf in leaves.OrderBy(l => l.SortIndex))
            {
                var articleReference = new ArticleReference(leaf.Data.Prid, null, null, null, (ArticleCatalog)leaf.Data.ReferentialId);
                var article = _articleBusiness3.GetArticle(siteContext, articleReference);

                if (article == null)
                {
                    continue;
                }

                var donationModel = new DonationViewModel()
                {
                    ProductId = article.Reference.PRID.GetValueOrDefault(),
                    Information = article.Summaries.FirstOrDefault(x => string.Equals(x.Origin, "DESCRIPTION", StringComparison.InvariantCultureIgnoreCase))?.Content,
                    Title = article.Summaries.FirstOrDefault(x => string.Equals(x.Origin, "SUMMARY", StringComparison.InvariantCultureIgnoreCase))?.Content,
                    Subtitle = article.Summaries.FirstOrDefault(x => string.Equals(x.Origin, "PARTNER", StringComparison.InvariantCultureIgnoreCase))?.Content
                };
                var multimediaPath = _urlManager.Sites.Multimedia.BaseRootUrl;
                var path = article.Core.Images.Pictures.FirstOrDefault(p => p.Type == PictureType.Big_110x110)?.Location;
                if (string.IsNullOrEmpty(path))
                {
                    path = article.Core.Images.Pictures.FirstOrDefault(p => p.Type == PictureType.SmallScan)?.Location;
                }
                donationModel.PictureUrl = string.IsNullOrEmpty(path) ? string.Empty : multimediaPath.WithPage(path).ToString();
                donations.Add(donationModel);
            }

            return donations;
        }
    }
}
