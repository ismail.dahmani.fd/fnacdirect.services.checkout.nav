using FnacDirect.Customer.BLL;
using FnacDirect.Customer.IModel;
using FnacDirect.Customer.Model;
using FnacDirect.OrderPipe.Base.DAL;
using FnacDirect.OrderPipe.Base.Model;
using System;
using System.Linq;

namespace FnacDirect.OrderPipe.Base.Business
{
    public class AddressOPBusiness : IAddressOPBusiness
    {
        private readonly IAddressBusiness _addressBusiness;
        private readonly IShopCommerceDAL _shopCommerceDal;

        public AddressOPBusiness(IAddressBusiness addressBusiness, IShopCommerceDAL shopCommerceDal)
        {
            _addressBusiness = addressBusiness;
            _shopCommerceDal = shopCommerceDal;
        }

        /// <summary>
        /// Récupérer la référence de l'adresse postal de livraison correspondante dans le carnet d'adresse du client
        /// </summary>
        /// <param name="orderAccountId">l'account id de la commande</param>
        /// <returns></returns>
        public string FindAddressReferenceInCustomerAddressBook(PostalAddress orderShippingAddress, string accountReference, string culture)
        {
            var aeReference = string.Empty;
            var postalAddresses = _addressBusiness.PopulateAdresses(accountReference, CustomerEmums.FDAddressesSortType.stAdsShippingAndBillingDefault, culture);
            var ae = (from AddressEntity currentAdressEntity in postalAddresses
                      where
                       currentAdressEntity.FirstName.Equals(orderShippingAddress.Firstname, StringComparison.CurrentCultureIgnoreCase) &&
                       currentAdressEntity.LastName.Equals(orderShippingAddress.LastName, StringComparison.CurrentCultureIgnoreCase) &&
                       currentAdressEntity.AddressLine1.Equals(orderShippingAddress.AddressLine1, StringComparison.CurrentCultureIgnoreCase) &&
                       currentAdressEntity.AddressLine2.Equals(orderShippingAddress.AddressLine2, StringComparison.CurrentCultureIgnoreCase) &&
                       currentAdressEntity.AddressLine3.Equals(orderShippingAddress.AddressLine3, StringComparison.CurrentCultureIgnoreCase) &&
                       currentAdressEntity.AddressLine4.Equals(orderShippingAddress.AddressLine4, StringComparison.CurrentCultureIgnoreCase) &&
                       currentAdressEntity.City.Equals(orderShippingAddress.City, StringComparison.CurrentCultureIgnoreCase) &&
                       currentAdressEntity.Zipcode.Equals(orderShippingAddress.ZipCode, StringComparison.CurrentCultureIgnoreCase)
                      select currentAdressEntity).FirstOrDefault();
            if (ae != null)
            {
                aeReference = ae.AddressBookReference;
            }
            return aeReference;
        }

        public AddressEntity GetBillingAddressInCustomerAddressBook(BillingAddress orderBillingAddress, string accountReference, string culture)
        {
            var postalAddresses = _addressBusiness.PopulateAdresses(accountReference, CustomerEmums.FDAddressesSortType.stAdsShippingAndBillingDefault, culture);
            var ae = (from AddressEntity currentAdressEntity in postalAddresses
                      where
                       currentAdressEntity.FirstName.Equals(orderBillingAddress.Firstname, StringComparison.CurrentCultureIgnoreCase) &&
                       currentAdressEntity.LastName.Equals(orderBillingAddress.Lastname, StringComparison.CurrentCultureIgnoreCase) &&
                       currentAdressEntity.AddressLine1.Equals(orderBillingAddress.AddressLine1, StringComparison.CurrentCultureIgnoreCase) &&
                       currentAdressEntity.AddressLine2.Equals(orderBillingAddress.AddressLine2, StringComparison.CurrentCultureIgnoreCase) &&
                       currentAdressEntity.AddressLine3.Equals(orderBillingAddress.AddressLine3, StringComparison.CurrentCultureIgnoreCase) &&
                       currentAdressEntity.AddressLine4.Equals(orderBillingAddress.AddressLine4, StringComparison.CurrentCultureIgnoreCase) &&
                       currentAdressEntity.City.Equals(orderBillingAddress.City, StringComparison.CurrentCultureIgnoreCase) &&
                       currentAdressEntity.Zipcode.Equals(orderBillingAddress.ZipCode, StringComparison.CurrentCultureIgnoreCase)
                      select currentAdressEntity).FirstOrDefault();
            return ae;
        }

        /// <summary>
        /// Récupérer l'id du relais dans la table tbl_orderdetailsogep
        /// </summary>
        /// <param name="orderId">l'order id</param>
        /// <returns></returns>
        public int GetRelayId(int orderId)
        {
            return _shopCommerceDal.GetRelayId(orderId);
        }

        /// <summary>
        /// Récupérer l'id du relais dans la table tbl_ordershop
        /// </summary>
        /// <param name="orderId"></param>
        /// <returns></returns>
        public int GetShopId(int orderId)
        {
            return _shopCommerceDal.GetShopId(orderId);
        }
    }
}
