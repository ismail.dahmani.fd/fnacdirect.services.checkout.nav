﻿using FnacDirect.OrderPipe.Base.Model;
using System.Collections.Generic;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Basket.Children
{
    public interface IRichBasketItemChildrenViewModelBuilder
    {
        IEnumerable<ChildBasketItemViewModel> Build(StandardArticle standardArticle, IEnumerable<Promotion> globalPromotions, bool applyAdherentPromotion);
    }
}
