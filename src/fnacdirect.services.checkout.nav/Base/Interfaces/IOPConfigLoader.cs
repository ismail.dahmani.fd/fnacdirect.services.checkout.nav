using FnacDirect.OrderPipe.Config;

namespace FnacDirect.OrderPipe
{
    /// <summary>
    /// Interface du chargeur de configuration du Pipe
    /// </summary>
    public interface IOPConfigLoader
    {
        PipeConfig ReadConfig();
    }
}
