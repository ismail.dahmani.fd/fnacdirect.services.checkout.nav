using System;
using System.Xml.Serialization;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace FnacDirect.OrderPipe.Base.Model
{
    public class Wrap
    {
        [XmlIgnore]
        private Dictionary<LogisticType,object> _LogisticTypes=new Dictionary<LogisticType,object>();

        [XmlIgnore]
        public Dictionary<LogisticType,object> LogisticTypes
        {
            get
            {
                return _LogisticTypes;
            }
            set
            {
                _LogisticTypes = value;
            }
        }

        [XmlIgnore]
        private int? _WrapId;

        public int? WrapId
        {
            get
            {
                return _WrapId;
            }
            set
            {
                _WrapId = value;
            }
        }
        [XmlIgnore]
        private WrapMethod wrapMethod;

        [XmlIgnore]
        public WrapMethod Method
        {
            get
            {
                return wrapMethod;
            }
            set
            {
                wrapMethod = value;
            }
        }

        [XmlIgnore]
        private decimal totalDBPrice;

        [XmlIgnore]
        public decimal TotalDBPrice
        {
            get { return totalDBPrice; }
            set { totalDBPrice = value; }
        }

        [XmlIgnore]
        private decimal totalUserPrice;

        [XmlIgnore]
        public decimal TotalUserPrice
        {
            get { return totalUserPrice; }
            set { totalUserPrice = value; }
        }

        [XmlIgnore]
        private decimal totalNoVATPrice;

        [XmlIgnore]
        public decimal TotalNoVATPrice
        {
            get { return totalNoVATPrice; }
            set { totalNoVATPrice = value; }
        }
    }

    public class WrapList : KeyedCollection<int,Wrap>
    {
        protected override int GetKeyForItem(Wrap item)
        {
            return item.WrapId.Value;
        }
    }
}

