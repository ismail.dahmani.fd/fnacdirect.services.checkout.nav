﻿
using FnacDirect.Contracts.Online.Model;

namespace FnacDirect.OrderPipe.FDV.Model
{
    public class SellerData : Data
    {
        /// <summary>
        /// Ensemble des informations du vendeur
        /// </summary>
        public Seller Seller { get; set; }

        /// <summary>
        /// Matricule du vendeur saisi sur l'interface
        /// </summary>
        public int? Matricule { get; set; }


    }
}
