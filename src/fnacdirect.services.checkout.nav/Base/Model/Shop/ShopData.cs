using System;
using FnacDirect.OrderPipe.Base.Model;
using FM = FnacDirect.FnacStore.Model;

namespace FnacDirect.OrderPipe.Base.BaseModel.Model
{
    public class ShopData : GroupData
    {
        public ShopAddress ShopAddress { get; set; }

        public bool IsDefault { get; set; }

        public bool SetByUser { get; set; }

        private FM.FnacStore store = new FM.FnacStore();

        public FM.FnacStore Store
        {
            get { return store; }
            set { store = value; }
        }

        private DateTime dateRetrait;

        public DateTime DateRetrait
        {
            get { return dateRetrait; }
            set { dateRetrait = value; }
        }

        private string _mailHtml;

        public string MailHtml
        {
            get { return _mailHtml; }
            set { _mailHtml = value; }
        }

        public string MailText { get; set; }

        public DateTime? CancelDate { get; set; }

        public int Availability { get; set; }

        public bool IsAvailable
        {
            get
            {
                return Availability != 0;
            }
        }

        public bool IsInStock
        {
            get
            {
                return Availability == 1;
            }
        }

        private ModePrevenanceShop modePrevenance = ModePrevenanceShop.Mail;

        public ModePrevenanceShop ModePrevenance
        {
            get { return modePrevenance; }
            set { modePrevenance = value; }
        }

        /// <summary>
        /// Obtient ou définit une valeur déterminant si les données relative au magasin sont activées.
        /// </summary>
        public bool IsEnabled { get; set; }
        private bool _IsInitialized = false;

        public ShopData()
        {
            ShopAddress = null;
        }


        /// <summary>
        /// Initialisation des données de l'adresse du magasin de retrait.
        /// </summary>
        public void Initialize()
        {
            if (!_IsInitialized)
            {
                _IsInitialized = IsEnabled = true;
            }
        }
    }
}
