using FnacDirect.OrderPipe.Base.Model;
using System;
using System.Collections.Generic;

namespace FnacDirect.OrderPipe.Base.BaseModel.Model
{
    [Serializable]
    public class TrackingOrder
    {
        public List<TrackingProduct> Products { get; set; }

        public int OrderId { get; set; }

        public string OrderReference { get; set; }

        public string OrderUserReference { get; set; }

        public string LgOrderUserReferences { get; set; }

        public int TotalQuantity { get; set; }

        public decimal TotalPrice { get; set; }

        public decimal TotalPriceWithoutTVA { get; set; }

        public string PromotionCode { get; set; }

        public string OrderShippingMethods { get; set; }

        public decimal OrderShippingCost { get; set; }
        public string OrderBillingLine1 { get; set; }
        public string OrderBillingLine2 { get; set; }
        public string OrderBillingCity { get; set; }
        public string OrderBillingPostalCode { get; set; }
        public string OrderBillingCountry { get; set; }

        public List<TrackingBillingMethod> BillingMethods { get; set; } = new List<TrackingBillingMethod>();

        public TrackingBillingMethod MainBillingMethod { get; set; }

        public BillingMethod BillingMethod { get; set; }
        public List<BillingMethod> OrderBillingMethods { get; set; }

        public decimal TotalBasketPrice { get; set; }

        public decimal TotalBasketPriceNoVAT { get; set; }

        public decimal TotalShippingPrice { get; set; }

        public decimal TotalShippingPriceNoVAT { get; set; }

        public decimal TotalVAT { get; set; }

        public string Currency { get; set; }

        public string OrderBillingState { get; set; }

        public string OrderBillingZip { get; set; }

        public string GlobalShippingMethod { get; set; }

        public string ShippingAddressLine1 { get; set; }
        public string ShippingAddressLine2 { get; set; }
        public string ShippingAddressCity { get; set; }
        public string ShippingAddressPostalCode { get; set; }
        public string ShippingAddressCountry { get; set; }
        public string Name { get; set; }

        public string FirstShippingMethod { get { return OrderShippingMethods?.Split('|')[0] ?? string.Empty; } }

    }
}
