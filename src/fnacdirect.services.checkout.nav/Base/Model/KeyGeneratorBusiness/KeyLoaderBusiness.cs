using FnacDirect.KeyGenerator;
using FnacDirect.OrderPipe.Base.Business.OrderTransaction;
using FnacDirect.OrderPipe.Base.LineGroups;
using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.OrderPipe.Base.Model.OrderInformations;
using FnacDirect.OrderPipe.FDV.Model;
using System;
using System.Collections.Generic;
using System.Linq;

namespace FnacDirect.OrderPipe.Base.Business.MainOrderTransactionInfo
{
    public class KeyLoaderBusiness : IKeyLoaderBusiness
    {
        private readonly ISwitchProvider _switchProvider;
        private readonly IOrderTransactionBusiness _orderTransactionBusiness;

        private int _indexOrderTransaction;
        private int _indexOrderTransactionOperation;

        public KeyLoaderBusiness(IOrderTransactionBusiness orderTransactionBusiness, ISwitchProvider switchProvider)
        {
            _switchProvider = switchProvider;
            _orderTransactionBusiness = orderTransactionBusiness;
        }

        public void LoadKeyInOrderForm(MultiKey mkey, IOF of, IOrderGlobalInformations orderGlobalInformations, IEnumerable<ILogisticLineGroup> logisticLineGroups, IEnumerable<BillingMethod> orderBillingMethods)
        {
            _indexOrderTransaction = 0;
            _indexOrderTransactionOperation = 0;

            orderGlobalInformations.MainOrderPk = mkey["MainOrder"][0].Id;
            orderGlobalInformations.MainOrderUserReference = "C" + mkey["MainOrder"][0].UserReference;
            orderGlobalInformations.MainOrderReference = Guid.NewGuid();

            //Pour les paiements ogone nous sommes obligé de conserver cette gestion des transactions
            //afin d'etre compatible avec les steps fnac.com
            if (!_switchProvider.IsEnabled("front.bmp.enabled", false)
                || orderBillingMethods.Any(b => b.BillingMethodId == OgoneCreditCardBillingMethod.IdBillingMethod))
            {
                if (_orderTransactionBusiness.CanSetMainOrderTransaction(logisticLineGroups, orderBillingMethods))
                {
                    var mainOrderTransactionInfos = new OrderTransactionInfoList
                    {
                        new OrderTransactionInfo
                        {
                            OrderTransaction = CreateOrderTransaction(mkey),
                            IsMainBillingMethod = true
                        }
                    };

                    orderGlobalInformations.MainOrderTransactionInfos = mainOrderTransactionInfos;

                    if (orderBillingMethods.OfType<EurosFnacBillingMethod>().Any())
                    {
                        mainOrderTransactionInfos.Add(new OrderTransactionInfo
                        {
                            OrderTransaction = CreateOrderTransaction(mkey)
                        });
                    }
                }
            }

            var indexOrder = 0;
            var indexOrderDetail = 0;

            foreach (var logisticLineGroup in logisticLineGroups)
            {
                logisticLineGroup.Informations.OrderTransactionInfos.Clear();

                //Pour les paiements ogone nous sommes obligé de conserver cette gestion des transactions
                //afin d'etre compatible avec les steps fnac.com
                if (!_switchProvider.IsEnabled("front.bmp.enabled", false)
                    || orderBillingMethods.Any(b => b.BillingMethodId == OgoneCreditCardBillingMethod.IdBillingMethod))
                {
                    if (_orderTransactionBusiness.CanSetOrderTransaction(orderBillingMethods))
                    {
                        foreach (var billingMethod in CollectionUtils.FilterOnType<BillingMethod>(orderBillingMethods))
                        {
                            var orderTransactionInfo = new OrderTransactionInfo
                            {
                                OrderTransaction = CreateOrderTransaction(mkey, billingMethod.BillingMethodId)
                            };

                            logisticLineGroup.Informations.OrderTransactionInfos.Add(orderTransactionInfo);
                        }
                    }

                    if (orderBillingMethods.Any(o => o is TPEBillingMethod) && logisticLineGroups.Any(lg => lg.IsIntraMag))
                    {
                        logisticLineGroup.Informations.OrderTransactionInfos.First().DebitCancellationOrderTransactionOperation = new FnacDirect.Ogone.Model.OrderTransactionOperation(mkey["TransactionOperation"][_indexOrderTransactionOperation].Id);
                        ++_indexOrderTransactionOperation;
                    }
                }

                logisticLineGroup.OrderPk = mkey["Order"][indexOrder].Id;
                logisticLineGroup.OrderUserReference = mkey["Order"][indexOrder].UserReference;
                logisticLineGroup.OrderDate = orderGlobalInformations.OrderDate;
                logisticLineGroup.OrderReference = Guid.NewGuid();
                ++indexOrder;

                foreach (var article in logisticLineGroup.Articles)
                {
                    article.OrderDetailPk = mkey["OrderDetail"][indexOrderDetail].Id;
                    indexOrderDetail += 1;
                }
            }
        }         

        private FnacDirect.Ogone.Model.OrderTransaction CreateOrderTransaction(MultiKey mkey, int billingMethodId)
            => new FnacDirect.Ogone.Model.OrderTransaction(GetTransactionId(mkey), GetTransactionOperationId(mkey), billingMethodId);

        private FnacDirect.Ogone.Model.OrderTransaction CreateOrderTransaction(MultiKey mkey)
            => new FnacDirect.Ogone.Model.OrderTransaction(GetTransactionId(mkey), GetTransactionOperationId(mkey));

        private int GetTransactionId(MultiKey mkey) => mkey["Transaction"][_indexOrderTransaction++].Id;

        private int GetTransactionOperationId(MultiKey mkey) => mkey["TransactionOperation"][_indexOrderTransactionOperation++].Id;
    }
}
