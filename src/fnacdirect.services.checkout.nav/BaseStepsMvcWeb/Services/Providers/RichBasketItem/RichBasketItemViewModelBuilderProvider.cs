using FnacDirect.OrderPipe.Base.Business;
using FnacDirect.OrderPipe.Base.Business.Shipping;
using FnacDirect.OrderPipe.Base.Proxy.Membership;
using FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Basket.Children;
using FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Common.Basket;
using FnacDirect.OrderPipe.BaseMvc.Web.Services.Article.Availability;
using FnacDirect.Technical.Framework.Web.Mvc;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Basket
{
    public class RichBasketItemViewModelBuilderProvider : IRichBasketItemViewModelBuilderProvider
    {
        private readonly IBasketItemViewModelBuilder _basketItemViewModelBuilder;
        private readonly Base.Proxy.Articles.IW3ArticleService _w3ArticleService;
        private readonly IArticleSubTitleService _articleSubTitleService;
        private readonly IApplicationContext _applicationContext;
        private readonly IPriceViewModelBuilder _priceViewModelBuilder;
        private readonly IExpressPlusViewModelBuilder _expressPlusViewModelBuilder;
        private readonly IMembershipConfigurationService _membershipConfigurationService;
        private readonly ICompositeRichBasketItemChildrenViewModelBuilder _compositeRichBasketItemChildrenViewModelBuilder;
        private readonly IPromotionTemplateViewModelBuilder _promotionTemplateViewModelBuilder;
        private readonly ISwitchProvider _switchProvider;
        private readonly ISolexBusiness _solexBusiness;
        private readonly IArticleAvailabilityService _articleAvailabilityService;
        private readonly IAdherentCardArticleService _adherentCardArticleService;

        public RichBasketItemViewModelBuilderProvider(IBasketItemViewModelBuilder basketItemViewModelBuilder,
                                                      Base.Proxy.Articles.IW3ArticleService w3ArticleService,
                                                      IArticleSubTitleService articleSubTitleService,
                                                      IApplicationContext applicationContext,
                                                      IExpressPlusViewModelBuilder expressPlusViewModelBuilder,
                                                      IPriceViewModelBuilder priceViewModelBuilder,
                                                      ICompositeRichBasketItemChildrenViewModelBuilder compositeRichBasketItemChildrenViewModelBuilder,
                                                      IMembershipConfigurationService membershipConfigurationService,
                                                      IPromotionTemplateViewModelBuilder promotionTemplateViewModelBuilder,
                                                      ISwitchProvider switchProvider,
                                                      ISolexBusiness solexBusiness,
                                                      IArticleAvailabilityService articleAvailabilityService,
                                                      IAdherentCardArticleService adherentCardArticleService)
        {
            _basketItemViewModelBuilder = basketItemViewModelBuilder;
            _w3ArticleService = w3ArticleService;
            _articleSubTitleService = articleSubTitleService;
            _applicationContext = applicationContext;
            _priceViewModelBuilder = priceViewModelBuilder;
            _expressPlusViewModelBuilder = expressPlusViewModelBuilder;
            _membershipConfigurationService = membershipConfigurationService;
            _compositeRichBasketItemChildrenViewModelBuilder = compositeRichBasketItemChildrenViewModelBuilder;
            _promotionTemplateViewModelBuilder = promotionTemplateViewModelBuilder;
            _switchProvider = switchProvider;
            _solexBusiness = solexBusiness;
            _articleAvailabilityService = articleAvailabilityService;
            _adherentCardArticleService = adherentCardArticleService;
        }

        public virtual IRichBasketItemViewModelBuilder Get(RichBasketItemViewModelBuilderContext context)
        {
            return new RichBasketItemViewModelBuilder(_basketItemViewModelBuilder,
                                                      _w3ArticleService,
                                                      _articleSubTitleService,
                                                      _applicationContext,
                                                      _solexBusiness,
                                                      _expressPlusViewModelBuilder,
                                                      _priceViewModelBuilder,
                                                      context,
                                                      _membershipConfigurationService,
                                                      _compositeRichBasketItemChildrenViewModelBuilder,
                                                      _promotionTemplateViewModelBuilder,
                                                      _switchProvider,
                                                      _articleAvailabilityService,
                                                      _adherentCardArticleService);
        }
    }
}
