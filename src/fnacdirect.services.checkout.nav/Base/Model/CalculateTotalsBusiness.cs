using FnacDirect.OrderPipe.Base.LineGroups;
using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.OrderPipe.Base.Model.Facets;
using FnacDirect.OrderPipe.Base.Model.LineGroups;
using System.Collections.Generic;
using System.Linq;

namespace FnacDirect.OrderPipe.Base.Business
{
    public class CalculateTotalsBusiness : ICalculateTotalsBusiness
    {
        public CalculateTotalsBusiness()
        { }

        public void CalculateSubTotals(ILogisticLineGroup logisticLineGroup)
        {
            if (logisticLineGroup.IsMarketPlace)
            {
                CalculateMpArticlesSubTotals(logisticLineGroup, logisticLineGroup.Articles.OfType<MarketPlaceArticle>());
            }
            else
            {
                CalculateStandardArticlesSubTotals(logisticLineGroup, logisticLineGroup.Articles.OfType<StandardArticle>());
            }
        }

        public void CalculateStandardArticlesSubTotals(ILineGroupHeader lineGroupHeader, IEnumerable<StandardArticle> standardArticles)
        {
            var nTotalBasketPriceDbEUR = 0m;
            var nTotalBasketPriceUserEUR = 0m;
            var nTotalBasketPriceNoVatEUR = 0m;
            var nTotalQuantity = 0;
            var nTotalEcoPart = 0m;
            var l_nTotalEcoPartNoVAT = 0m;


            // Calculate basket prices
            foreach (var standardArticle in standardArticles)
            {
                if (standardArticle.Type.Value == ArticleType.Saleable || standardArticle.Type.Value == ArticleType.Free)
                {
                    // put no Vat price if needed
                    if (!standardArticle.UseVat)
                    {
                        standardArticle.PriceUserEur = standardArticle.PriceNoVATEur;
                        standardArticle.VATRate = 0;
                    }

                    nTotalBasketPriceDbEUR = (nTotalBasketPriceDbEUR
                                + (standardArticle.Quantity.Value * standardArticle.PriceDBEur.Value));
                    nTotalBasketPriceNoVatEUR = (nTotalBasketPriceNoVatEUR
                               + (standardArticle.Quantity.Value * standardArticle.PriceNoVATEur.Value));
                    nTotalBasketPriceUserEUR = (nTotalBasketPriceUserEUR
                               + (standardArticle.Quantity.Value * standardArticle.PriceUserEur.Value));

                    if (standardArticle.EcoTaxEur.HasValue)
                    {
                        nTotalEcoPart += (standardArticle.EcoTaxEur.Value * standardArticle.Quantity.Value);

                        if (standardArticle.EcoTaxEurNoVAT.HasValue) // simo fix bug null
                            l_nTotalEcoPartNoVAT += (standardArticle.EcoTaxEurNoVAT.Value * standardArticle.Quantity.Value);
                    }

                    nTotalQuantity += standardArticle.Quantity.GetValueOrDefault();

                    if (standardArticle.Services.Count > 0)
                    {
                        foreach (var service in standardArticle.Services)
                        {
                            nTotalBasketPriceDbEUR = (nTotalBasketPriceDbEUR
                                      + (service.Quantity.Value * service.PriceDBEur.Value));
                            nTotalBasketPriceUserEUR = (nTotalBasketPriceUserEUR
                                       + (service.Quantity.Value * service.PriceUserEur.Value));
                            nTotalBasketPriceNoVatEUR = (nTotalBasketPriceNoVatEUR
                                       + (service.Quantity.Value * service.PriceNoVATEur.Value));
                            nTotalQuantity += service.Quantity.GetValueOrDefault();

                        }
                    }
                }
            }

            var globalPrices = lineGroupHeader.GlobalPrices;

            globalPrices.TotalShoppingCartBusinessPriceDBEur = nTotalBasketPriceDbEUR;
            globalPrices.TotalShoppingCartBusinessPriceUserEur = nTotalBasketPriceUserEUR;
            globalPrices.TotalShoppingCartBusinessPriceNoVatEur = nTotalBasketPriceNoVatEUR;
            globalPrices.TotalQuantity = nTotalQuantity;

            // Reset de l'éco tax avant calcul (au cas où il est setté par le clone de LogisticLineGroup)
            globalPrices.TotalEcoTaxEur = 0;
            globalPrices.TotalEcoTaxEurNoVAT = 0;
            if (nTotalEcoPart > 0)
            {
                globalPrices.TotalEcoTaxEur = nTotalEcoPart;
                globalPrices.TotalEcoTaxEurNoVAT = l_nTotalEcoPartNoVAT;
            }

            //  Total price of the entire basket (price of the articles + shipping price + wrap price)
            globalPrices.TotalPriceEur =
                globalPrices.TotalShoppingCartBusinessPriceUserEur +
                globalPrices.TotalShoppingCartBusinessWrapPriceUserEur.GetValueOrDefault() +
                globalPrices.TotalShipPriceUserEur.GetValueOrDefault() +
                globalPrices.TotalEcoTaxEur.GetValueOrDefault();

            if (lineGroupHeader.OrderType == OrderTypeEnum.FnacCom || lineGroupHeader.OrderType == OrderTypeEnum.Pro)
            {
                globalPrices.TotalPriceEur += globalPrices.TotalSplitPriceUserEur.GetValueOrDefault();
            }

            // Total Fnac ship prices
            globalPrices.TotalFnacShipPriceUserEur = globalPrices.TotalShipPriceUserEur.GetValueOrDefault();
        }

        public void CalculateStandardArticlesSubTotals(IEnumerable<ILineGroup> facet)
        {
            foreach (var lineGroup in facet)
            {
                if (!lineGroup.HasMarketPlaceArticle)
                {
                    CalculateStandardArticlesSubTotals(lineGroup, lineGroup.Articles.OfType<StandardArticle>());
                }
            }
        }

        public void CalculateMpArticlesSubTotals(ILineGroupHeader lineGroupHeader, IEnumerable<MarketPlaceArticle> marketPlaceArticles)
        {
            var nTotalBasketPriceDbEUR = 0.0m;
            var nTotalBasketPriceUserEUR = 0.0m;
            var nTotalBasketPriceUserNoVatEur = 0.0m;
            var nTotalQuantity = 0;

            // calculate basket prices
            foreach (var article in marketPlaceArticles)
            {
                nTotalQuantity += article.Quantity.GetValueOrDefault();
                nTotalBasketPriceDbEUR += article.Quantity.Value * article.PriceDBEur.Value;
                nTotalBasketPriceUserEUR += article.Quantity.Value * article.PriceUserEur.Value;
                nTotalBasketPriceUserNoVatEur += article.Quantity.Value * article.PriceNoVATEur.Value;
            }

            var globalPrices = lineGroupHeader.GlobalPrices;

            globalPrices.TotalShoppingCartBusinessPriceDBEur = nTotalBasketPriceDbEUR;
            globalPrices.TotalShoppingCartBusinessPriceUserEur = nTotalBasketPriceUserEUR;
            globalPrices.TotalShoppingCartBusinessPriceNoVatEur = nTotalBasketPriceUserNoVatEur;
            globalPrices.TotalQuantity = nTotalQuantity;

            globalPrices.TotalPriceEur =
                globalPrices.TotalShoppingCartBusinessPriceUserEur +
                globalPrices.TotalShipPriceUserEur.GetValueOrDefault();
        }

        public void CalculateMpArticlesSubTotals(IEnumerable<ILineGroup> lineGroups)
        {
            foreach (var lineGroup in lineGroups)
            {
                if (lineGroup.OrderType == OrderTypeEnum.MarketPlace)
                {
                    CalculateMpArticlesSubTotals(lineGroup, lineGroup.Articles.OfType<MarketPlaceArticle>());
                }
            }
        }

        public void CalculateTotals(MultiFacet<IGotBillingInformations, IGotLineGroups> orderFormMultiFacets)
        {
            var mainGlobalPrices = orderFormMultiFacets.Facet1.GlobalPrices;

            mainGlobalPrices.TotalShoppingCartBusinessPriceDBEur = 0;
            mainGlobalPrices.TotalShoppingCartBusinessPriceUserEur = 0;
            mainGlobalPrices.TotalShoppingCartBusinessPriceNoVatEur = 0;
            mainGlobalPrices.TotalQuantity = 0;
            mainGlobalPrices.TotalShipPriceDBEur = 0;
            mainGlobalPrices.TotalShipPriceUserEur = 0;
            mainGlobalPrices.TotalShipPriceUserEurNoVAT = 0;
            mainGlobalPrices.TotalShoppingCartBusinessWrapPriceDBEur = 0;
            mainGlobalPrices.TotalShoppingCartBusinessWrapPriceUserEur = 0;
            mainGlobalPrices.TotalShoppingCartBusinessWrapPriceNoVatEur = 0;
            mainGlobalPrices.TotalSplitPriceDBEur = 0;
            mainGlobalPrices.TotalSplitPriceUserEur = 0;
            mainGlobalPrices.TotalEcoTaxEur = 0;
            mainGlobalPrices.TotalEcoTaxEurNoVAT = 0;
            mainGlobalPrices.TotalPriceEur = 0;
            mainGlobalPrices.TotalFnacShipPriceUserEur = 0;

            foreach (var lineGroup in orderFormMultiFacets.Facet2.LineGroups)
            {
                var globalPrices = lineGroup.GlobalPrices;

                mainGlobalPrices.TotalPriceEur += globalPrices.TotalPriceEur.GetValueOrDefault();
                mainGlobalPrices.TotalEcoTaxEur += globalPrices.TotalEcoTaxEur.GetValueOrDefault();
                mainGlobalPrices.TotalEcoTaxEurNoVAT += globalPrices.TotalEcoTaxEurNoVAT.GetValueOrDefault();
                mainGlobalPrices.TotalQuantity += globalPrices.TotalQuantity.GetValueOrDefault();
                mainGlobalPrices.TotalShipPriceDBEur += globalPrices.TotalShipPriceDBEur.GetValueOrDefault();
                mainGlobalPrices.TotalShipPriceUserEur += globalPrices.TotalShipPriceUserEur.GetValueOrDefault();
                mainGlobalPrices.TotalShipPriceUserEurNoVAT += globalPrices.TotalShipPriceUserEurNoVAT.GetValueOrDefault();
                mainGlobalPrices.TotalShoppingCartBusinessPriceDBEur += globalPrices.TotalShoppingCartBusinessPriceDBEur.GetValueOrDefault();
                mainGlobalPrices.TotalShoppingCartBusinessPriceUserEur += globalPrices.TotalShoppingCartBusinessPriceUserEur.GetValueOrDefault();
                mainGlobalPrices.TotalShoppingCartBusinessPriceNoVatEur += globalPrices.TotalShoppingCartBusinessPriceNoVatEur.GetValueOrDefault();
                mainGlobalPrices.TotalShoppingCartBusinessWrapPriceDBEur += globalPrices.TotalShoppingCartBusinessWrapPriceDBEur.GetValueOrDefault();
                mainGlobalPrices.TotalShoppingCartBusinessWrapPriceUserEur += globalPrices.TotalShoppingCartBusinessWrapPriceUserEur.GetValueOrDefault();
                mainGlobalPrices.TotalShoppingCartBusinessWrapPriceNoVatEur += globalPrices.TotalShoppingCartBusinessWrapPriceNoVatEur.GetValueOrDefault();
                mainGlobalPrices.TotalSplitPriceDBEur += globalPrices.TotalSplitPriceDBEur.GetValueOrDefault();
                mainGlobalPrices.TotalSplitPriceUserEur += globalPrices.TotalSplitPriceUserEur.GetValueOrDefault();
                mainGlobalPrices.TotalFnacShipPriceUserEur += globalPrices.TotalFnacShipPriceUserEur.GetValueOrDefault();
            }
        }

        public void CalculateTotals(IGotBillingInformations iGotBillingInformations, IEnumerable<ILogisticLineGroup> logisticLineGroups)
        {
            var mainGlobalPrices = iGotBillingInformations.GlobalPrices;

            mainGlobalPrices.TotalShoppingCartBusinessPriceDBEur = 0;
            mainGlobalPrices.TotalShoppingCartBusinessPriceUserEur = 0;
            mainGlobalPrices.TotalShoppingCartBusinessPriceNoVatEur = 0;
            mainGlobalPrices.TotalQuantity = 0;
            mainGlobalPrices.TotalShipPriceDBEur = 0;
            mainGlobalPrices.TotalShipPriceUserEur = 0;
            mainGlobalPrices.TotalShipPriceUserEurNoVAT = 0;
            mainGlobalPrices.TotalShoppingCartBusinessWrapPriceDBEur = 0;
            mainGlobalPrices.TotalShoppingCartBusinessWrapPriceUserEur = 0;
            mainGlobalPrices.TotalShoppingCartBusinessWrapPriceNoVatEur = 0;
            mainGlobalPrices.TotalSplitPriceDBEur = 0;
            mainGlobalPrices.TotalSplitPriceUserEur = 0;
            mainGlobalPrices.TotalEcoTaxEur = 0;
            mainGlobalPrices.TotalEcoTaxEurNoVAT = 0;
            mainGlobalPrices.TotalPriceEur = 0;
            mainGlobalPrices.TotalFnacShipPriceUserEur = 0;

            foreach (var logisticLineGroup in logisticLineGroups)
            {
                var globalPrices = logisticLineGroup.GlobalPrices;

                mainGlobalPrices.TotalPriceEur += globalPrices.TotalPriceEur.GetValueOrDefault();
                mainGlobalPrices.TotalEcoTaxEur += globalPrices.TotalEcoTaxEur.GetValueOrDefault();
                mainGlobalPrices.TotalEcoTaxEurNoVAT += globalPrices.TotalEcoTaxEurNoVAT.GetValueOrDefault();
                mainGlobalPrices.TotalQuantity += globalPrices.TotalQuantity.GetValueOrDefault();
                mainGlobalPrices.TotalShipPriceDBEur += globalPrices.TotalShipPriceDBEur.GetValueOrDefault();
                mainGlobalPrices.TotalShipPriceUserEur += globalPrices.TotalShipPriceUserEur.GetValueOrDefault();
                mainGlobalPrices.TotalShipPriceUserEurNoVAT += globalPrices.TotalShipPriceUserEurNoVAT.GetValueOrDefault();
                mainGlobalPrices.TotalShoppingCartBusinessPriceDBEur += globalPrices.TotalShoppingCartBusinessPriceDBEur.GetValueOrDefault();
                mainGlobalPrices.TotalShoppingCartBusinessPriceUserEur += globalPrices.TotalShoppingCartBusinessPriceUserEur.GetValueOrDefault();
                mainGlobalPrices.TotalShoppingCartBusinessPriceNoVatEur += globalPrices.TotalShoppingCartBusinessPriceNoVatEur.GetValueOrDefault();
                mainGlobalPrices.TotalShoppingCartBusinessWrapPriceDBEur += globalPrices.TotalShoppingCartBusinessWrapPriceDBEur.GetValueOrDefault();
                mainGlobalPrices.TotalShoppingCartBusinessWrapPriceUserEur += globalPrices.TotalShoppingCartBusinessWrapPriceUserEur.GetValueOrDefault();
                mainGlobalPrices.TotalShoppingCartBusinessWrapPriceNoVatEur += globalPrices.TotalShoppingCartBusinessWrapPriceNoVatEur.GetValueOrDefault();
                mainGlobalPrices.TotalSplitPriceDBEur += globalPrices.TotalSplitPriceDBEur.GetValueOrDefault();
                mainGlobalPrices.TotalSplitPriceUserEur += globalPrices.TotalSplitPriceUserEur.GetValueOrDefault();
                mainGlobalPrices.TotalFnacShipPriceUserEur += globalPrices.TotalFnacShipPriceUserEur.GetValueOrDefault();
            }
        }
    }
}
