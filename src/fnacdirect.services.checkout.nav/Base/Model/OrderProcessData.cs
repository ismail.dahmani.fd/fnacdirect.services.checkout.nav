using System.Collections.Generic;

namespace FnacDirect.OrderPipe.Base.Model.B2B
{
    public class OrderProcessData : StepData
    {
        public bool SkipBillingAddressCheck { get; set; }
        public int OrderType { get; set; }
        public int OrderOrigin { get; set; }
        public decimal? GlobalShippingCost { get; set; }
        public decimal? GlobalWrapCost { get; set; }
        public int SelectedShippingMethod { get; set; } 
        public List<ShippingInfos> ShippingInfos { get; set; }

    }
    public class ShippingInfos
    {

        public int? ProductId { get; set; }
        public decimal? ShippingCost { get; set; }
        public int? SelectedShippingMethod { get; set; }
    }
}
