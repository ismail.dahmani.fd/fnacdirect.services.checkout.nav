﻿namespace System.Web.Routing
{
    public static class RouteDatasExtensions
    {
        public static string GetRouteName(this RouteData routeData)
        {
            if (routeData == null)
            {
                return null;
            }

            if (routeData.DataTokens == null)
            {
                return null;
            }

            if (!routeData.DataTokens.ContainsKey("__RouteName"))
            {
                return null;
            }

            if (routeData.DataTokens["__RouteName"] == null)
            {
                return null;
            }

            return routeData.DataTokens["__RouteName"].ToString();
        }
    }
}
