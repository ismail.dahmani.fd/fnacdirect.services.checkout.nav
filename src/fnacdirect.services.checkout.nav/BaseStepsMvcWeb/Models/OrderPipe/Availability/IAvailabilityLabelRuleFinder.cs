﻿namespace FnacDirect.OrderPipe.Base.Business.Availability
{
    public interface IAvailabilityLabelRuleFinder
    {
        IAvailabilityLabelRule Find(int? shippingMethodId);
    }
}
