using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.OrderPipe.Base.Model.Solex;
using FnacDirect.OrderPipe.Shipping.Model;
using System.Collections.Generic;
using System.Linq;

namespace FnacDirect.OrderPipe.Base.BaseModel
{
    public abstract class ShippingMethodEvaluationItem
    {
        protected ShippingMethodEvaluationItem(ShippingMethodEvaluationType shippingMethodEvaluationType, ShippingAddress shippingAddress, bool isDummyAddress)
        {
            _shippingMethodEvaluationType = shippingMethodEvaluationType;
            ShippingAddress = shippingAddress;
            IsDummyAddress = isDummyAddress;
        }

        public ShippingAddress ShippingAddress { get; set; }

        private readonly ShippingMethodEvaluationType _shippingMethodEvaluationType;

        public ShippingMethodEvaluationType ShippingMethodEvaluationType
        {
            get { return _shippingMethodEvaluationType; }
        }
        public bool IsDummyAddress { get; set; }

        #region Legacy

        private readonly Dictionary<string, ShippingMethodResult> _shippingMethodResults
            = new Dictionary<string, ShippingMethodResult>();

        public bool HasShippingMethodResults
        {
            get { return _shippingMethodResults.Any(); }
        }

        public bool HasChoiceList
        {
            get
            {
                return (Choices != null && Choices.Any()) 
                    || (_shippingMethodResults.Any() && _shippingMethodResults.Any(r => r.Value.ChoiceList.Any()));
            }
        }

        public bool ContainsShippingMethodId(int shippingMethodId)
        {
            return (Choices != null && Choices.Any(choice => choice.ContainsShippingMethodId(shippingMethodId)))
                    || _shippingMethodResults.Select(r => r.Value)
                                             .SelectMany(r => r.ChoiceList)
                                             .SelectMany(c => c.Choices)
                                             .Any(c => c.ShippingMethodId == shippingMethodId);
        }

        public ShippingMethodEvaluationItem WithResultsForLineGroup(string lineGroupIdentifier, ShippingMethodResult shippingMethodResults)
        {
            if (_shippingMethodResults.ContainsKey(lineGroupIdentifier))
                _shippingMethodResults.Remove(lineGroupIdentifier);

            _shippingMethodResults.Add(lineGroupIdentifier, shippingMethodResults);

            return this;
        }

        public ShippingMethodResult GetShippingMethodResultsFor(string lineGroupIdentifier)
        {
            if (!_shippingMethodResults.ContainsKey(lineGroupIdentifier))
                return null;

            return _shippingMethodResults[lineGroupIdentifier];
        }

        public IEnumerable<ShippingMethodResult> GetShippingMethodResults()
        {
            return _shippingMethodResults.Values;
        }

        public IEnumerable<int> GetDefaultShippingMethodId()
        {
            if (!_shippingMethodResults.Any())
                yield break;

            var methodResult = _shippingMethodResults.First();

            if (methodResult.Value == null)
                yield break;

            foreach (var shippingMethod in methodResult.Value.GetShippingMethods())
            {
                yield return shippingMethod;
            }
        }

        #endregion

        #region SOLEX

        /// <summary>
        /// Choices available for shipping network (Solex Behaviour)
        /// </summary>
        public IEnumerable<ShippingChoice> Choices { get; set; }

        /// <summary>
        /// Property use to store the selected shipping method choice (Solex Behaviour) 
        /// </summary>
        public int? SelectedShippingMethodId { get; set; }

        /// <summary>
        /// Give information on the solex choices tables (Solex Behaviour)
        /// </summary>
        public bool HasChoices
        {
            get
            {
                if (Choices != null)
                {
                    return Choices.Any();
                }
                return false;
            }

        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public int? GetDefaultShippingMethodIdFromChoices()
        {
            if (Choices == null)
                return null;

            var bestChoice = Choices.GetBestChoice(Constants.ShippingMethods.HandDelivery);
            return bestChoice?.MainShippingMethodId;
        }

        public ShippingChoice GetSelectedChoice()
        {
            if (Choices == null)
                return null;

            return Choices.FirstOrDefault(choice => choice.MainShippingMethodId == SelectedShippingMethodId);
        }

        #endregion
    }

    public abstract class ShippingMethodEvaluationItem<T> : ShippingMethodEvaluationItem
        where T : ShippingAddress
    {
        public new T ShippingAddress
        {
            get { return base.ShippingAddress as T; }
        }

        protected ShippingMethodEvaluationItem(ShippingMethodEvaluationType shippingMethodEvaluationType, T shippingAddress, bool isDummyAddress)
            : base(shippingMethodEvaluationType, shippingAddress, isDummyAddress)
        {
        }
    }

    public class PostalShippingMethodEvaluationItem : ShippingMethodEvaluationItem<PostalAddress>
    {
        public int? DefaultShippingMethodId { get; set; }

        public PostalShippingMethodEvaluationItem(PostalAddress postalAddress, bool isDummyAddress)
            : base(ShippingMethodEvaluationType.Postal, postalAddress, isDummyAddress)
        {
        }
    }

    public class ShopShippingMethodEvaluationItem : ShippingMethodEvaluationItem<ShopAddress>
    {
        public ShopShippingMethodEvaluationItem(ShopAddress shopAddress, bool isDummyAddress)
            : base(ShippingMethodEvaluationType.Shop, shopAddress, isDummyAddress)
        {

        }
    }

    public class RelayShippingMethodEvaluationItem : ShippingMethodEvaluationItem<RelayAddress>
    {
        public RelayShippingMethodEvaluationItem(RelayAddress relayAddress, bool isDummyAddress)
            : base(ShippingMethodEvaluationType.Relay, relayAddress, isDummyAddress)
        {

        }
    }
}
