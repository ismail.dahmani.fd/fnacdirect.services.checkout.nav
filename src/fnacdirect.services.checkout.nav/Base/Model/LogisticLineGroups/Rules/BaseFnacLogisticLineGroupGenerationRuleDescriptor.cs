﻿using System;
using System.Collections.Generic;
using FnacDirect.OrderPipe.Base.Business.LogisticLineGroups.InitialSelectors;
using FnacDirect.OrderPipe.Base.Business.LogisticLineGroups.Internals.Initializers;
using FnacDirect.OrderPipe.Base.Business.LogisticLineGroups.Unfolders;
using FnacDirect.OrderPipe.Base.Business.LogisticLineGroups.Handlers;

namespace FnacDirect.OrderPipe.Base.Business.LogisticLineGroups.Rules
{
    public abstract class BaseFnacLogisticLineGroupGenerationRuleDescriptor<T> : BaseLogisticLineGroupGenerationRuleDescriptor<FnacLineGroupInitialSelector,
                                                              T,
                                                              ServiceLogisticLineGroupUnfolder>
        where T : BaseLogisticLineGroupInitializer
    {
        public override IEnumerable<Type> LogisticLineGroupGeneratedHandlers
        {
            get
            {
                yield return typeof(UpdateShippingMethodChoiceLogisticLineGroupHandler);
                yield return typeof(UpdateShippingUserCostProcessCompletedHandler);
                yield return typeof(CalculateSubTotalsLogisticLineGroupHandler);
            }
        }
    }
}
