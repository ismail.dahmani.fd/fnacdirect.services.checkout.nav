using FnacDirect.OrderPipe.Base.Business.Payment;
using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.OrderPipe.Base.Model.Pop;
using FnacDirect.OrderPipe.BaseMvc.Web.Controllers.Pop;
using FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Common.Payment;
using FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Common.Shipping;
using FnacDirect.OrderPipe.BaseMvc.Web.Pop.Models.OnePage;
using FnacDirect.Technical.Framework.Web;
using System;
using System.Linq;
using System.Web.Mvc;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Pop.Controllers.Payment
{
    public class PaymentOnePageController : PopPaymentBaseController
    {
        private readonly IOnePageRootViewModelBuilder _onePageRootViewModelBuilder;
        private readonly OPContext _opContext;
        private readonly IShippingViewModelBuilder _shippingViewModelBuilder;
        private const string OnePageIndexPath = "../PopOnePage/_Poc/Index";

        public PaymentOnePageController(IOnePageRootViewModelBuilder onePageRootViewModelBuilder,
            OPContext opContext,
            IShippingViewModelBuilder shippingViewModelBuilder,
            IPaymentViewModelBuilder paymentViewModelBuilder,
            IPaymentSelectionBusiness paymentSelectionBusiness,
            IUrlManager urlManager)
            : base(paymentSelectionBusiness, urlManager, paymentViewModelBuilder, opContext)
        {
            _opContext = opContext;
            _onePageRootViewModelBuilder = onePageRootViewModelBuilder;
            _shippingViewModelBuilder = shippingViewModelBuilder;
        }

        [HttpGet]
        public ActionResult Index()
        {
            var popOrderForm = _opContext.OF as PopOrderForm;

            var rootViewModel = _onePageRootViewModelBuilder.GetRootViewModel(_opContext, popOrderForm, (a, b) => Url.RouteUrl(a, b));

            rootViewModel.IsFixed = true;// one page figée

            rootViewModel.Shipping.IsOneClick = true;

            return View(OnePageIndexPath, rootViewModel);
        }

        /// <summary>
        /// Attention! La validation du paiment (form de la vue) avec Ogone AliasGateway ne passe par cette action, le post est directement fait chez Ogone, sans passer par le server Fnaccom
        /// </summary>
        /// <param name="paymentMethodId"></param>
        /// <param name="relayCellphone"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Index(int? paymentMethodId, string relayCellphone, string pinf)
        {
            var popOrderForm = _opContext.OF as PopOrderForm;

            var sellerId = ComputeSellerId(popOrderForm);

            var isValid = _shippingViewModelBuilder.IsValid(popOrderForm, sellerId);

            if (!isValid)
            {
                var rootViewModel = _onePageRootViewModelBuilder.GetRootViewModel(_opContext, popOrderForm, (a, b) => Url.RouteUrl(a, b));

                return View(OnePageIndexPath, rootViewModel);
            }

            var onePageData = popOrderForm.GetPrivateData<PopData>();

            if (PaymentValidation(paymentMethodId, _opContext))
            {
                onePageData.DisplayOnePageValidated = true;

                return Continue();
            }
            else
            {
                var rootViewModel = _onePageRootViewModelBuilder.GetRootViewModel(_opContext, popOrderForm, (a, b) => Url.RouteUrl(a, b));
                rootViewModel.IsFixed = true;// one page figée
                rootViewModel.Shipping.IsOneClick = true;

                if (rootViewModel.Payment.HasValue)
                {
                    var paymentViewModel = rootViewModel.Payment.Value;

                    var tabSelected = paymentViewModel.Tabs.FirstOrDefault(t => t.BillingMethods.Any(b => b.Id == paymentMethodId));

                    if (tabSelected != null)
                    {
                        foreach (var tab in paymentViewModel.Tabs)
                        {
                            tab.IsSelected = tab == tabSelected;
                        }
                    }
                }

                var popData = popOrderForm.GetPrivateData<PopData>(create: false);

                return View(OnePageIndexPath, rootViewModel);
            }
        }

        private int ComputeSellerId(PopOrderForm popOrderForm)
        {
            var shippingMethodEvaluation = popOrderForm.ShippingMethodEvaluation;
            var mainShippingMethodEvaluationGroup = shippingMethodEvaluation.GetMainShippingMethodEvaluationGroup();
            return mainShippingMethodEvaluationGroup.SellerId;
        }
    }
}
