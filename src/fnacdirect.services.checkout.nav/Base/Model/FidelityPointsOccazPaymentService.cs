using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.OrderPipe.Base.Model.Facets;
using FnacDirect.OrderPipe.Base.Model.UserInformations;
using FnacDirect.Technical.Framework.CoreServices;
using FnacDirect.Technical.Framework.Web.Mvc;
using System;
using System.Linq;

namespace FnacDirect.OrderPipe.Base.Business.Payment
{
    public class FidelityPointsOccazPaymentService : IFidelityPointsOccazPaymentService
    {
        private readonly IUserExtendedContextProvider _userExtendedContextProvider;
        private readonly ICheckArticleService _checkArticleService;
        private readonly ISwitchProvider _switchProvider;
        private readonly IPaymentEligibilityService _paymentEligibilityService;

        public FidelityPointsOccazPaymentService(IUserExtendedContextProvider userExtendedContextProvider,
            ICheckArticleService checkArticleService,
            ISwitchProvider switchProvider,
            IPaymentEligibilityService paymentEligibilityService)
        {
            _userExtendedContextProvider = userExtendedContextProvider;
            _checkArticleService = checkArticleService;
            _switchProvider = switchProvider;
            _paymentEligibilityService = paymentEligibilityService;
        }

        public bool IsAllowed(IGotBillingInformations iGotBillingInformations, IGotLineGroups iGotLineGroups, IUserContextInformations userContext, bool disableClickAndCollectCheck = false, bool disableEccvCheck = false)
        {
            var rules = _paymentEligibilityService.GetRules("orderpipe.pop.payment.fidelitypointsoccaz.eligibilityrules",
                disableClickAndCollectCheck: disableClickAndCollectCheck,
                disableEccvCheck: disableEccvCheck);

            if (!_paymentEligibilityService.IsAllowed(iGotLineGroups, rules))
            {
                return false;
            }

            var userExtendedContext = _userExtendedContextProvider.GetCurrentUserExtendedContext();
            var customer = userExtendedContext.Customer;

            var behavior = ConfigurationManager.Current.AppSettings.Settings["front.account.adherent.behavior"].Value;
            if (string.Compare(behavior, "siebel", StringComparison.OrdinalIgnoreCase) != 0)
            {
                return false;
            }

            if (userContext.IsAdherent || !userContext.IsGamer)
            {
                return false;
            }

            if (iGotLineGroups.LineGroups.GetArticlesOfType<MarketPlaceArticle>().Any()
                && _switchProvider.IsEnabled("orderpipe.pop.payment.filtersecondaryformarketplace"))
            {
                return false;
            }

            var fidelityPointsOccazBusiness = new FidelityBusiness<FidelityPointsOccazBillingMethod>();
            var maxUsableAmount = fidelityPointsOccazBusiness.GetMaxUsableAmount(customer, iGotBillingInformations, 10);
            if (maxUsableAmount == 0)
            {
                return false;
            }

            if (iGotBillingInformations.GlobalPrices.TotalPriceEur.GetValueOrDefault(0) + iGotBillingInformations.GlobalPrices.TotalShipPriceDBEur.GetValueOrDefault(0) < 10)
            {
                return false;
            }

            if (_checkArticleService.HasDonation(iGotLineGroups.Articles))
            {
                return false;
            }

            return true;
        }
    }
}
