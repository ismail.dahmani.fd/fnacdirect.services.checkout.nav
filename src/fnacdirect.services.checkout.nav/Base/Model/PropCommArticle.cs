using System;

namespace FnacDirect.OrderPipe.FDV.Model
{
    /// <summary>
    /// represente la ligne de l'article
    /// </summary>
    [Serializable]
    public class PropCommArticle
    {
        public string CurrentLiteralDateTime
        {
            get
            {
                return "dd/MM/yyyy";
            }
        }

        public string MerchantName
        {
            get;
            set;
        }

        /// <summary>
        /// Montant total EcoTaxe TTC
        /// </summary>
        public decimal TotalEcoTaxTTC
        {
            get { return Quantity * UnitEcoTaxTTC; }
        }

        /// <summary>
        ///  Montant total EcoTaxe HT
        /// </summary>
        public decimal TotalEcoTaxHT
        {
            get { return Quantity * UnitEcoTaxHT; }
        }

        public string Reference
        {
            get;
            set;
        } = string.Empty;

        public string ArticleLabel
        {
            get;
            set;
        } = string.Empty;

        public string ShippingType
        {
            get;
            set;
        } = string.Empty;

        public string ShippingAdress
        {
            get;
            set;
        } = string.Empty;

        public string DeliveryType
        {
            get { return $"{ShippingType} {ShippingAdress}"; }
        }

        /// <summary>
        /// Quantit� command�e
        /// </summary>
        public int Quantity
        {
            get;
            set;
        }

        /// <summary>
        /// Type de remise
        /// </summary>
        public string DiscountType
        {
            get;
            set;
        } = string.Empty;

        /// <summary>
        /// Montant TTC de la remise
        /// </summary>
        public decimal TotalDiscountTTC
        {
            get;
            set;
        }

        /// <summary>
        /// Montant HT de la remise
        /// </summary>
        public decimal TotalDiscountHT
        {
            get;
            set;
        }

        /// <summary>
        /// Prix unitaire TTC de l'article non remis�
        /// </summary>
        public decimal UnitPriceTTC
        {
            get;
            set;
        }

        /// <summary>
        /// Prix unitaire TTC de l'article non remis� avec D3E
        /// </summary>
        public decimal UnitPriceTTCWithEcoTax
        {
            get { return UnitPriceTTC + UnitEcoTaxTTC; }
        }

        /// <summary>
        /// Prix unitaire HT de l'article non remis�
        /// </summary>
        public decimal UnitPriceHT
        {
            get;
            set;
        }

        /// <summary>
        /// Prix unitaire HT de l'article non remis� avec D3E
        /// </summary>
        public decimal UnitPriceHTWithEcoTax
        {
            get { return UnitPriceHT + UnitEcoTaxHT; }
        }

        /// <summary>
        /// Ecotaxe TTC
        /// </summary>
        public decimal UnitEcoTaxTTC
        {
            get;
            set;
        }

        /// <summary>
        /// Ecotaxe HT
        /// </summary>
        public decimal UnitEcoTaxHT
        {
            get;
            set;
        }

        /// <summary>
        /// Montant � payer
        /// </summary>
        public decimal TotalPriceTTC
        {
           get {return (Quantity * UnitPriceTTC) + TotalEcoTaxTTC - TotalDiscountTTC;}

        }

        /// <summary>
        /// Montant des frais de livraison TTC
        /// </summary>
        public decimal ShippingCostTTC
        {
            get;
            set;
        }

        /// <summary>
        /// Montant des frais de livraison HT
        /// </summary>
        public decimal ShippingCostHT
        {
            get;
            set;
        }

        /// <summary>
        /// Montant total HT
        /// </summary>
        public decimal TotalPriceHT
        {
            get { return (Quantity * UnitPriceHT) + TotalEcoTaxHT - TotalDiscountHT; }

        }

    }
}
