using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.OrderPipe.Base.Model.Facets;
using FnacDirect.OrderPipe.Core.Services;
using FnacDirect.Technical.Framework.Web.Mvc;
using System.Collections.Specialized;
using System.Globalization;
using System.Net;
using System.Text;
using System.Web.Script.Serialization;

namespace FnacDirect.OrderPipe.Base.Business.Payment
{
    public class DeferredPaymentService : IDeferredPaymentService
    {
        private readonly IApplicationContext _applicationContext;
        private readonly ICheckOriginService _checkOriginService;
        private readonly IPaymentEligibilityService _paymentEligibilityService;

        private readonly decimal _minimumAmountNoVat;

        public DeferredPaymentService(IApplicationContext applicationContext,
                                        IPipeParametersProvider pipeParametersProvider,
                                        ICheckOriginService checkOriginService,
                                        IPaymentEligibilityService paymentEligibilityService)
        {
            _applicationContext = applicationContext;
            _checkOriginService = checkOriginService;
            _paymentEligibilityService = paymentEligibilityService;

            _minimumAmountNoVat = pipeParametersProvider.Get("basket.amountnovat.grather.than", 140m);
        }

        public DeferredPaymentEligibility GetDeferredPaymentEligibility(int accountId, decimal orderTotalPrice)
        {
            var baseUrl = _applicationContext.GetAppSetting("DeferredPaymentServiceUrl");
            var apiUrl = $"{baseUrl}/account/{accountId}/simulation/";

            using (var client = new WebClient())
            {
                var parameters = new NameValueCollection
                    {
                        { "orderTotalPrice", orderTotalPrice.ToString(CultureInfo.InvariantCulture) }
                    };
                var jsonDataBytes = client.UploadValues(apiUrl, "POST", parameters);
                var jsonData = Encoding.UTF8.GetString(jsonDataBytes);
                var serializer = new JavaScriptSerializer();
                return serializer.Deserialize<DeferredPaymentEligibility>(jsonData);
            }
        }

        public bool IsAllowed(PopOrderForm popOrderForm, IGotBillingInformations iGotBillingInformations, IGotUserContextInformations iGotUserContextInformations)
        {
            var rules = _paymentEligibilityService.GetRules("orderpipe.pop.payment.deferred.eligibilityrules");

            if (!_paymentEligibilityService.IsAllowed(popOrderForm, rules))
            {
                return false;
            }

            var disabledCheckMinimumAmount = _checkOriginService.IsFromFnacProAndCallCenter(iGotUserContextInformations);
            if (iGotBillingInformations.GlobalPrices.TotalPriceEurNoVat < _minimumAmountNoVat && !disabledCheckMinimumAmount)
            {
                return false;
            }

            var deferredPayment = iGotUserContextInformations?.GetPrivateData<DeferredPaymentEligibilityData>(false);
            return deferredPayment != null && deferredPayment.IsEligible;
        }
    }
}
