

using FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Common.Basket;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Membership
{
    public enum PushType
    {
        None,
        Confirmation,
        Recruit,
        Renew
    }

    public class MembershipPushViewModel : IGotFnacPlusData
    {      

        public PushType Type { get; set; }

        public bool IsAdherent { get; set; }

        public bool IsAdherentOrHasAdherentCardInBasket
        {
            get { return IsAdherent || HasAdherentCardInBasket; }
        }

        public bool IsConfirmation
        {
            get { return Type == PushType.Confirmation; }
        }

        public bool IsRecruit
        {
            get { return Type == PushType.Recruit; }
        }

        public bool IsRenew
        {
            get { return Type == PushType.Renew; }
        }

        public bool IsTryCard { get; set; }

        public bool CanAddCardFromPopin
        {
            get { return IsRecruit || IsRenew; }
        }
        public bool IsMembershipAboutToExpire { get; set; }

        public bool IsMembershipExpired { get; set; }

        public bool HasDiscount { get; set; }

        public bool HasImmediatDiscount { get; set; }

        public bool HasDifferedDiscount { get; set; }

        public bool HasImmediatOrDifferedDiscount
        {
            get { return HasDifferedDiscount || HasImmediatDiscount; }
        }

        public bool DisplayShippingDiscount { get; set; }

        public string ShippingDiscount { get; set; }

        public string ImmediateDiscount { get; set; }

        public string DifferedDiscount { get; set; }        

        public bool IsShippingCostOffered { get; set; }

        public bool IsFreeShippingForNonAdherent { get; set; }
        
        public bool IsMixed { get; set; }

        public bool IsProfitable { get; set; }        
        
        public string SouscriptionCardPrice { get; set; }
                     
        public string CardPrice { get; set; }

        public bool DisplayPriceBenefit { get; set; }

        public int CardId { get; set; }

        public bool IsIdentified { get; set; }

        public bool HasAdherentNumber { get; set; }

        public int PromotionKey { get; set; }

        public int ProfilKey { get; set; }

        public bool IsFnacPlusEnabled { get; set; }

        public bool IsFnacPlusCanalPlayEnabled { get; set; }

        public bool IsEligibleEssaiFnacPlus { get; set; }

        public bool IsEligiblePushRecruit { get; set; }

        public bool IsEligiblePushRenouvAdh { get; set; }

        public bool IsEligiblePushRenouvFnacPlus { get; set; }

        public bool IsEligiblePushConfirm { get; set; }

        public bool HasAdherentCardInBasket { get; set; }

        public bool HasFnacPlusCardInBasket { get; set; }

        public MembershipPushCardViewModel CardPlusInfos { get; set; }

        public string FnacPlusImmediateDiscount { get; set; }

        public string FnacPlusDifferedDiscount { get; set; }

        public bool HasFnacPlusDiscount { get; set; }

        public bool HasFnacPlusImmediateDiscount { get; set; }

        public bool HasFnacPlusDifferedDiscount { get; set; }
 
        public bool IsSteedFromStoreAvailable { get; set; }

        public bool IsFullMP { get; set; }

        public bool IsFnacSeller { get; set; }

        public bool ShowPopin { get; set; }

        public bool ShowPush { get; set; }
    }
}
