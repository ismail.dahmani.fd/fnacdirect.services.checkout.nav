﻿using FnacDirect.OrderPipe.BaseMvc.Web.Models.Common;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Pop.Models
{
    public class ContextViewModel
    {
        public bool IsPro { get; set; }

        public BreadCrumbViewModel BreadCrumb { get; set; }
    }
}
