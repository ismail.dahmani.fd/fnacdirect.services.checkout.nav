﻿namespace FnacDirect.OrderPipe.Base.Business.ArticleConversions
{
    public interface IArticleConverter<in TFrom, out TTo>
    {
        TTo Convert(TFrom from);
    }
}
