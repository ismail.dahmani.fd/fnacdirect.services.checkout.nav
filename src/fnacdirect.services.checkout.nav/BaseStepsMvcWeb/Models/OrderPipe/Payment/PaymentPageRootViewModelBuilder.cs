using FnacDirect.OrderPipe.Base.Business;
using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.OrderPipe.BaseMvc.Web.ApiControllers.Pop;
using FnacDirect.OrderPipe.BaseMvc.Web.Models;
using FnacDirect.OrderPipe.BaseMvc.Web.Models.OrderPipe.Common.Scoring;
using FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Basket;
using FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Common.Payment;
using FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Common.Shipping;
using FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Omniture;
using FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Payment;
using FnacDirect.OrderPipe.BaseMvc.Web.Pop.Models.Common;
using FnacDirect.Technical.Framework.Web.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Routing;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Pop.Models.Payment
{
    public class PaymentPageRootViewModelBuilder : IPaymentPageRootViewModelBuilder
    {

        private readonly OPContext _opContext;
        private readonly IApplicationContext _applicationContext;
        private readonly IPaymentViewModelBuilder _paymentViewModelBuilder;
        private readonly IOptionsViewModelBuilder _optionsViewModelBuilder;
        private readonly Web.Models.Pop.Payment.IBasketViewModelBuilder _basketViewModelBuilder;
        private readonly IPopBaseRootViewModelBuilder<Web.Models.Pop.Payment.RootViewModel> _popBaseRootViewModelBuilder;
        private readonly IOmnitureViewModelBuilder _omnitureViewModelBuilder;
        private readonly IShippingViewModelBuilder _shippingViewModelBuilder;
        private readonly INavigationViewModelBuilder _navigationViewModelBuilder;
        private readonly string navigationBackLinkUrlName = "DisplayShippingAddress";
        private readonly ITermsAndConditionsViewModelBuilder _termsAndConditionsViewModelBuilder;
        private readonly IAddressValidationBusiness _addressValidationBusiness;
        private readonly IScoringViewModelBuilder _scoringViewModelBuilder;
        public PaymentPageRootViewModelBuilder(OPContext opContext,
                                               IPaymentViewModelBuilder paymentViewModelBuilder,
                                               IOptionsViewModelBuilder optionsViewModelBuilder,
                                               Web.Models.Pop.Payment.IBasketViewModelBuilder basketViewModelBuilder,
                                               IApplicationContext applicationContext,
                                               IPopBaseRootViewModelBuilder<Web.Models.Pop.Payment.RootViewModel> popBaseRootViewModelBuilder,
                                               IOmnitureViewModelBuilder omnitureViewModelBuilder,
                                               IShippingViewModelBuilder shippingViewModelBuilder,
                                               INavigationViewModelBuilder navigationViewModelBuilder,
                                               ITermsAndConditionsViewModelBuilder termsAndConditionsViewModelBuilder,
                                               IAddressValidationBusiness addressValidationBusiness,
                                               IScoringViewModelBuilder scoringViewModelBuilder)
        {
            _opContext = opContext;
            _paymentViewModelBuilder = paymentViewModelBuilder;
            _optionsViewModelBuilder = optionsViewModelBuilder;
            _basketViewModelBuilder = basketViewModelBuilder;
            _applicationContext = applicationContext;
            _popBaseRootViewModelBuilder = popBaseRootViewModelBuilder;
            _omnitureViewModelBuilder = omnitureViewModelBuilder;
            _shippingViewModelBuilder = shippingViewModelBuilder;
            _navigationViewModelBuilder = navigationViewModelBuilder;
            _termsAndConditionsViewModelBuilder = termsAndConditionsViewModelBuilder;
            _addressValidationBusiness = addressValidationBusiness;
            _scoringViewModelBuilder = scoringViewModelBuilder;
        }

        public Web.Models.Pop.Payment.RootViewModel GetRootViewModel(PopOrderForm popOrderForm, Func<string, RouteValueDictionary, string> getUrl)
        {
            var lstSelectedShippings = new List<ISelectedShippingItem>();

            var countryId = _applicationContext.GetSiteContext().CountryId;
            var paymentViewModel = _paymentViewModelBuilder.Build(_opContext, popOrderForm);
            var optionsViewModel = _optionsViewModelBuilder.Build(popOrderForm);
            var basketViewModel = _basketViewModelBuilder.Build(popOrderForm);

            var hasMissingInfo = !_addressValidationBusiness.IsAddressValid(popOrderForm.BillingAddress.Convert());
            var billingAddress = popOrderForm.BillingAddress.ConvertToAddressViewModel(countryId, hasMissingInfo);
            var isShippingEqualToBilling = popOrderForm.ShippingMethodEvaluation.ShippingMethodEvaluationGroups
                .SelectMany(_ => _.ShippingMethodEvaluationItems)
                .Select(_ => _.ShippingAddress.Reference)
                .Contains(billingAddress.Reference);
            billingAddress.IsShippingAndBillingAddresseAreEqual = isShippingEqualToBilling;

            var sellers = popOrderForm.LineGroups.SellerInformations().ToList();
            foreach (var s in sellers)
            {
                var shippingViewModel = _shippingViewModelBuilder.Build(popOrderForm, s.SellerId);
                var selectedShipping = shippingViewModel.SelectedShippingItem.Value;
                if (selectedShipping is AddressViewModel addr)
                {
                    addr.IsShippingAndBillingAddresseAreEqual = isShippingEqualToBilling;
                }
                lstSelectedShippings.Add(selectedShipping);
            }

            var omniturePaymentViewModel = _omnitureViewModelBuilder.BuildOnePage(popOrderForm);

            var rootViewModel = _popBaseRootViewModelBuilder.Build(getUrl);

            rootViewModel.PageName = PopApiEnvironments.PaymentPage;
            rootViewModel.Payment = paymentViewModel;
            rootViewModel.Options = optionsViewModel;
            rootViewModel.Basket = basketViewModel;
            rootViewModel.BillingAddress = billingAddress;
            rootViewModel.SelectedShippings = lstSelectedShippings;

            rootViewModel.HasAdherentNumber = HasAdherentNumber(popOrderForm);
            rootViewModel.Omniture = omniturePaymentViewModel;

            rootViewModel.Navigation = _navigationViewModelBuilder.Build(popOrderForm, navigationBackLinkUrlName);

            rootViewModel.TermsAndConditions = _termsAndConditionsViewModelBuilder.Build(popOrderForm.LineGroups);

            rootViewModel.Scoring = _scoringViewModelBuilder.Build();

            return rootViewModel;
        }

        private bool HasAdherentNumber(PopOrderForm popOrderForm)
        {
            if (popOrderForm.UserContextInformations.IsGuest)
            {
                return false;
            }

            return !string.IsNullOrEmpty(popOrderForm.UserContextInformations.CustomerEntity.AdherentCardNumber) || popOrderForm.LineGroups.GetArticles().OfType<StandardArticle>().Any(a => a.IsAdhesionCard);
        }
    }
}
