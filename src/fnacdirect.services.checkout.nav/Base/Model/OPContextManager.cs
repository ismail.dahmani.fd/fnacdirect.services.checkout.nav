using System;
using System.Collections.Generic;
using FnacDirect.Contracts.Online.Model;
using System.Linq;

namespace FnacDirect.OrderPipe
{
    /// <summary>
    /// Implémentation de IOPContextManager utilisant une variable ThreadStatic
    /// </summary>
    public class ThreadStaticOPContextManager : IOPContextManager
    {
        [ThreadStatic]
        private static Dictionary<string, OPContext> _ctx = new Dictionary<string, OPContext>();

        public Dictionary<string, OPContext> OpContextList
        {
            get
            {
                return _ctx;
            }
            set
            {
                _ctx = value;
            }
        }
    }

    /// <summary>
    /// Classe de gestion du contexte
    /// </summary>
    public static class OPContextManager
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2211:NonConstantFieldsShouldNotBeVisible")]
        public static IOPContextManager manager;

        public static IOPContextManager Current
        {
            get { return manager; }
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1810:InitializeReferenceTypeStaticFieldsInline")]
        static OPContextManager()
        {
            manager = ObjectFactory.Get<IOPContextManager>();
        }

        /// <summary>
        /// Création d'un nouveau contexte courant
        /// </summary>
        /// <param name="sid"></param>
        /// <param name="siteContext"></param>
        /// <param name="pipeName"></param>
        /// <returns></returns>
        public static OPContext Create(string sid, SiteContext siteContext, string pipeName)
        {
            var opContext = new OPContext
            {
                SID = sid,
                SiteContext = siteContext
            };

            if (manager.OpContextList == null)
            {
                manager.OpContextList = new Dictionary<string, OPContext>();
            }

            manager.OpContextList[pipeName] = opContext;

            return opContext;
        }

        /// <summary>
        /// Création d'un nouveau contexte générique basé sur OPContext
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="sid"></param>
        /// <param name="siteContext"></param>
        /// <param name="pipeName"></param>
        /// <returns></returns>
        public static T Create<T>(string sid, SiteContext siteContext, string pipeName) where T : OPContext
        {
            var opContext = Activator.CreateInstance<T>();

            opContext.SID = sid;
            opContext.SiteContext = siteContext;
            manager.OpContextList[pipeName] = opContext;
            return opContext;
        }

        public static OPContext GetCurrentContext(string pipeName)
        {
            if (manager.OpContextList?.Keys == null)
            {
                return null;
            }

            var key = manager.OpContextList.Keys.FirstOrDefault(k => string.Equals(pipeName, k, StringComparison.InvariantCultureIgnoreCase));

            if (key == null)
            {
                return null;
            }

            return manager.OpContextList[key];
        }
    }
}
