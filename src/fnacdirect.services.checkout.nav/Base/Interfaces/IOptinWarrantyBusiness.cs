using FnacDirect.OrderPipe.Base.Model;
using System.Collections.Generic;

namespace FnacDirect.OrderPipe.Base.Business
{
    public interface IOptinWarrantyBusiness
    {
        bool HasWarrantyLinked(IEnumerable<StandardArticle> standardArticles);
        IEnumerable<int> ArticlesWithWarrantyLinked(IEnumerable<StandardArticle> standardArticles);
    }
}
