using Common.Logging;
using FnacDirect.Context;
using FnacDirect.Contracts.Online.Model;
using FnacDirect.Customer.BLL;
using FnacDirect.Customer.Model;
using FnacDirect.Front.WebBusiness.Configuration;
using FnacDirect.IdentityImpersonation;
using FnacDirect.Membership.Model.Constants;
using FnacDirect.OrderPipe.Base.BaseModel.Model;
using FnacDirect.OrderPipe.Base.Business.Limonetik;
using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.OrderPipe.Base.Model.BillingMethods;
using FnacDirect.OrderPipe.Base.Model.Facets;
using FnacDirect.OrderPipe.Base.Model.Pop;
using FnacDirect.OrderPipe.Base.Model.SelfCheckout;
using FnacDirect.OrderPipe.Base.Proxy.Customer;
using FnacDirect.OrderPipe.Config;
using FnacDirect.Technical.Framework.CoreServices.Localization;
using FnacDirect.Technical.Framework.Web.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text.RegularExpressions;

namespace FnacDirect.OrderPipe.Base.Business.Payment
{
    public class PaymentSelectionBusiness : IPaymentSelectionBusiness
    {
        private readonly OPContext _opContext;
        private readonly IExecutionContextProvider _executionContextProvider;
        private readonly IUserContextProvider _userContextProvider;
        private readonly ICreditCardDescriptorBusiness _creditCardDescriptorBusiness;
        private readonly IApplicationContext _applicationContext;
        private readonly ICustomerCreditCardService _customerCreditCardService;
        private readonly IMaintenanceConfigurationService _maintenanceConfigurationService;
        private readonly IUIResourceService _uiResourceService;
        private readonly ILimonetikBusiness _limonetikBusiness;
        private readonly string _creditCardConfig;
        private readonly ParameterList _globalParameters;
        private readonly IGotLineGroups _gotLineGroups;
        private readonly IBankAccountNumberService _bancBankAccountNumberService;
        private readonly ICreditCardUsageBusiness _creditCardUsageBusiness;
        private readonly bool _saveCreditCardOnTacitAgreement;
        private readonly ISwitchProvider _switchProvider;
        private readonly IAccountAuditService _accountAuditService;
        private readonly ILog _logger;
        private readonly IBillingMethodService _billingMethodService;
        private readonly ICountryService _countryService;

        public PaymentSelectionBusiness(OPContext opContext,
            IExecutionContextProvider executionContextProvider,
            IUserContextProvider userContextProvider,
            ICreditCardDescriptorBusiness creditCardDescriptorBusiness,
            IApplicationContext applicationContext,
            IMaintenanceConfigurationService maintenanceConfigurationService,
            IUIResourceService uiResourceService,
            ICustomerCreditCardService customerCreditCardService,
            ILimonetikBusiness limonetikBusiness,
            IBankAccountNumberService bancBankAccountNumberService,
            ICreditCardUsageBusiness creditCardUsageBusiness,
            ISwitchProvider switchProvider,
            IAccountAuditService accountAuditService,
            ILog logger,
            IBillingMethodService billingMethodService,
            ICountryService countryService)
        {
            _opContext = opContext;
            _executionContextProvider = executionContextProvider;
            _userContextProvider = userContextProvider;
            _creditCardDescriptorBusiness = creditCardDescriptorBusiness;
            _applicationContext = applicationContext;
            _maintenanceConfigurationService = maintenanceConfigurationService;
            _uiResourceService = uiResourceService;
            _creditCardConfig = System.Configuration.ConfigurationManager.AppSettings["config.shippingMethodSelection"];
            _customerCreditCardService = customerCreditCardService;
            _globalParameters = opContext.Pipe.GlobalParameters;
            _gotLineGroups = opContext.OF as IGotLineGroups;
            _limonetikBusiness = limonetikBusiness;
            _bancBankAccountNumberService = bancBankAccountNumberService;
            _accountAuditService = accountAuditService;
            _saveCreditCardOnTacitAgreement = _globalParameters.Get("save.creditcard.on.tacit.agreement", true);
            _creditCardUsageBusiness = creditCardUsageBusiness;
            _switchProvider = switchProvider;
            _logger = logger;
            _billingMethodService = billingMethodService;
            _countryService = countryService;
        }

        public Tuple<bool, UserMessage> OgoneSelectionAndHandleCreditCard(string selectedCreditCardReference, int selectedCreditCardId, bool registerCreditCard,
                                                    IGotBillingInformations gotBillingInformations, IGotUserContextInformations gotUserContextInformations,
                                                    IGotLogisticLineGroups gotLogisticLineGroups, ParameterList _globalParameters)
        {
            //Save Ogone Billing in popOrderForm
            var result = OgoneSelection(selectedCreditCardReference, selectedCreditCardId, registerCreditCard,
                                        gotBillingInformations, gotUserContextInformations, _globalParameters);

            //Get the Usage of CB to update Account Credit Card
            var ogoneBM = (OgoneCreditCardBillingMethod)gotBillingInformations.MainBillingMethod;
            if (ogoneBM != null && !string.IsNullOrEmpty(ogoneBM.Reference))
            {
                if (!string.IsNullOrEmpty(ogoneBM.Reference))
                {
                    var cce = _customerCreditCardService.Load(ogoneBM.Reference);
                    var newFlag = _creditCardUsageBusiness.GetUsageFlags(ogoneBM);
                    if (newFlag.HasValue)
                    {
                        cce.CBUsage = (CBUsageFlags)newFlag;
                        _customerCreditCardService.SaveCustomerCreditCard(cce, gotUserContextInformations.UserContextInformations.CustomerEntity.Reference);
                    }
                }
                var executionContext = _executionContextProvider.GetCurrentExecutionContext();
                var cardLegalNotice = executionContext.GetValue("CardLegalNotice");
                if (!string.IsNullOrWhiteSpace(cardLegalNotice))
                {
                    var userContext = _userContextProvider.GetCurrentUserContext();
                    _accountAuditService.AddEntry(userContext.GetUserId(), AuditEntryType.TermsConditionAgreement, "Carte : " + ogoneBM.CardNumber + " Message : " + cardLegalNotice);
                }
            }
            return result;
        }

        public (bool IsSuccess, UserMessage ErrorMessage) OgoneSelection(int selectedCreditCardId, PopOrderForm popOrderForm, ParameterList globalParameters)
        {
            var result = OgoneSelection(null, selectedCreditCardId, false, popOrderForm, popOrderForm, _globalParameters);
            return (IsSuccess: result.Item1, ErrorMessage: result.Item2);
        }

        public Tuple<bool, UserMessage> OgoneSelection(string selectedCreditCardReference, int selectedCreditCardId, bool registerCreditCard,
                                                    IGotBillingInformations gotBillingInformations, IGotUserContextInformations gotUserContextInformations,
                                                    ParameterList _globalParameters)
        {
            var popOrderForm = _opContext.OF as PopOrderForm;

            var isGuest = popOrderForm.UserContextInformations.IsGuest;

            var hadSouscriptionCardInBasket = popOrderForm.LineGroups != null && popOrderForm.LineGroups.GetArticles().OfType<StandardArticle>()
                .Any(art => art.TypeId.GetValueOrDefault() == (int)CategoryCardType.Recruit || art.TypeId.GetValueOrDefault() == (int)CategoryCardType.Renew);

            var aboIlliData = gotBillingInformations.GetPrivateData<AboIlliData>();
            var fnacPlusData = gotBillingInformations.GetPrivateData<PushFnacPlusAdherentData>(create: false);
            int? cgvVersion = null;
            var registerCreditCardOnTR = _saveCreditCardOnTacitAgreement && ((fnacPlusData != null && fnacPlusData.HasFnacPlusCardInBasket) || aboIlliData.HasAboIlliInBasket || hadSouscriptionCardInBasket);

            if (registerCreditCard)
            {
                cgvVersion = _opContext.Pipe.GlobalParameters.GetAsInt("CGVVersion");
            }
            else if (registerCreditCardOnTR)
            {
                cgvVersion = 0;
            }

            var saveWalet = ((_saveCreditCardOnTacitAgreement && ((fnacPlusData != null && fnacPlusData.HasFnacPlusCardInBasket) || aboIlliData.HasAboIlliInBasket || hadSouscriptionCardInBasket)) || registerCreditCard) && !isGuest;

            return OgoneSelection(selectedCreditCardReference, selectedCreditCardId, saveWalet, registerCreditCardOnTR, registerCreditCard, cgvVersion,
                                                                gotBillingInformations, gotUserContextInformations,
                                                                _globalParameters);
        }

        public Tuple<bool, UserMessage> OgoneSelection(string selectedCreditCardReference, int selectedCreditCardId, bool registerCreditCard, bool registerCreditCardOnTR, bool registerCreditCardOnCheck, int? cgvVersion,
                                                    IGotBillingInformations gotBillingInformations, IGotUserContextInformations gotUserContextInformations,
                                                    ParameterList _globalParameters)
        {
            if (!VerifyBillingMethodEligibility(gotBillingInformations, OgoneCreditCardBillingMethod.IdBillingMethod))
            {
                return Tuple.Create<bool, UserMessage>(false, null);
            }

            var creditCardId = 0;
            var brand = string.Empty;
            var paymentMethod = string.Empty;
            var ogoneAlias = string.Empty;
            var virtualCard = string.Empty;
            var creditCardReference = string.Empty;
            var debitType = string.Empty;
            var savingWallet = false;

            var popOrderForm = _opContext.OF as PopOrderForm;
            var isGuest = popOrderForm.UserContextInformations.IsGuest;

            CustCreditCardDescriptor cardDescriptor = null;

            if (gotBillingInformations == null || gotUserContextInformations == null)
            {
                return Tuple.Create<bool, UserMessage>(false, null);
            }

            if (!string.IsNullOrEmpty(selectedCreditCardReference) && !isGuest)
            {
                var selectedCreditCard = _customerCreditCardService.GetCustomerCreditCards(gotUserContextInformations.UserContextInformations.UID).FirstOrDefault(x => x.Reference == selectedCreditCardReference);

                if (selectedCreditCard != null)
                {
                    cardDescriptor = _creditCardDescriptorBusiness.GetCreditCardDescriptor(selectedCreditCard.TypeId, _creditCardConfig, _applicationContext.GetSiteContext().Culture);

                    if (cardDescriptor != null)
                    {
                        creditCardId = selectedCreditCard.Id;

                        ogoneAlias = selectedCreditCard.OgoneCreditCardAlias;

                        brand = selectedCreditCard.Brand;

                        paymentMethod = cardDescriptor.PaymentMethod;

                        debitType = selectedCreditCard.DebitType.ToString();

                        creditCardReference = selectedCreditCard.Reference;

                        virtualCard = selectedCreditCard.VirtualCard.ToString();
                        //Update Mode
                        savingWallet = true;

                        cgvVersion = selectedCreditCard.CGVVersion ?? _opContext.Pipe.GlobalParameters.GetAsInt("CGVVersion");
                        var aliasGatewayOutputData = gotBillingInformations.GetPrivateData<OgoneAliasGatewayOutputData>(create: false);
                        if (aliasGatewayOutputData != null)
                        {
                            aliasGatewayOutputData.SaveWalet = true;
                        }
                    }
                }
            }
            else
            {
                cardDescriptor = _creditCardDescriptorBusiness.GetCreditCardDescriptor(selectedCreditCardId, _creditCardConfig, _applicationContext.GetSiteContext().Culture);

                if (cardDescriptor != null)
                {
                    brand = cardDescriptor.Brand;

                    paymentMethod = cardDescriptor.PaymentMethod;

                    debitType = cardDescriptor.DebitType.ToString();

                    if (_switchProvider.IsEnabled("orderpipe.pop.payment.option.clicinstore") && IsClicInStore(gotUserContextInformations.UserContextInformations.ContactEmail) || isGuest)
                    {
                        savingWallet = false;
                    }
                    else
                    {
                        savingWallet = registerCreditCard && cardDescriptor.Id != 11 /* ECB */ && cardDescriptor.Id != 30 /*Paypal*/;
                    }
                }
            }
            if (cardDescriptor != null)
            {
                if (!VerifyCreditCardEligibility(gotBillingInformations, cardDescriptor.Id, cardDescriptor.Label))
                {
                    return Tuple.Create<bool, UserMessage>(false, null);
                }

                var oChosenBillingMethod = new OgoneCreditCardBillingMethod
                {
                    Identity = creditCardId,
                    TypeId = cardDescriptor.Id,
                    TypeLabel = cardDescriptor.Label,
                    NumByPhone = false,
                    CryptoFlag = false,
                    Reference = creditCardReference,
                    Brand = brand,
                    Pm = paymentMethod,
                    OgoneAlias = ogoneAlias,
                    DebitType = debitType,
                    VirtualCard = virtualCard,
                    SavingCardAutorisation = savingWallet,
                    SavingCardOnTacitAgreement = registerCreditCardOnTR,
                    SavingCardOnCheck = registerCreditCardOnCheck,
                    CGVVersion = cgvVersion,
                    UserGUID = gotUserContextInformations.UserContextInformations.UID,
                    // Define Ogone Process: AliasGateway or ECommerce
                    OgoneTransactionProcess = cardDescriptor.TransactionProcess
                };


                gotBillingInformations.MainBillingMethod = oChosenBillingMethod;

                gotBillingInformations.RemoveBillingMethod(OgoneCreditCardBillingMethod.IdBillingMethod);
                gotBillingInformations.AddBillingMethod(oChosenBillingMethod);


                return Tuple.Create<bool, UserMessage>(true, null);
            }

            var msg = UserMessage.CreateMessage("creditcard", UserMessage.MessageLevel.Error, "OP.Preview.SelectCreditCard");
            gotBillingInformations.UserMessages.Add(msg);
            return Tuple.Create(false, msg);
        }

        private bool IsClicInStore(string contactEmail)
        {
            IdentityImpersonator identity = null;
            if (System.Web.HttpContext.Current != null)
            {
                identity = IdentityImpersonator.GetIdentityImpersonatorFromCookie(System.Web.HttpContext.Current.Request.Cookies[WebSiteConfiguration.Value.IdentityImpersonator.CookieName], WebSiteConfiguration.Value.IdentityImpersonator.Base64Key);
            }
            if (identity != null && identity.UserMail == contactEmail && !string.IsNullOrEmpty(identity.StoreCodeGU) && !string.IsNullOrEmpty(identity.VendorCode))
                return true;
            else
                return false;
        }

        public bool PayByRefSelection(string bankAccountNumber, IGotBillingInformations gotBillingInformations, IGotUserContextInformations gotUserContextInformations)
        {
            if (!VerifyBillingMethodEligibility(gotBillingInformations, PayByRefBillingMethod.IdBillingMethod))
            {
                return false;
            }

            if (!BankAccountNumberBusiness.IsIbanValid(bankAccountNumber, _applicationContext.GetCulture()))
            {
                gotUserContextInformations.AddErrorMessage("paybyref", "OP.PF.ErrMsg.NIB.Invalid");
                return false;
            }

            var customerBankAccountNumber = _bancBankAccountNumberService.GetBankAccountNumber();
            if (bankAccountNumber != customerBankAccountNumber && string.IsNullOrEmpty(customerBankAccountNumber))
            {
                customerBankAccountNumber = bankAccountNumber;
                _bancBankAccountNumberService.AddBankAccountNumber(customerBankAccountNumber);
            }
            else
            {
                customerBankAccountNumber = bankAccountNumber;
            }

            var pselData = gotBillingInformations.GetPrivateData<PaymentSelectionData>();
            var oChosenBillingMethod = new PayByRefBillingMethod
            {
                Amount = pselData.RemainingAmount,
                BankAccountNumber = customerBankAccountNumber,
                UserGUID = gotUserContextInformations.UserContextInformations.UID
            };

            gotBillingInformations.MainBillingMethod = oChosenBillingMethod;
            gotBillingInformations.RemoveBillingMethod(PayByRefBillingMethod.IdBillingMethod);
            gotBillingInformations.AddBillingMethod(oChosenBillingMethod);

            return true;
        }

        public bool MBWaySelection(string phoneNumber, IGotBillingInformations gotBillingInformations, IGotUserContextInformations gotUserContextInformations)
        {
            if (!VerifyBillingMethodEligibility(gotBillingInformations, MBWayBillingMethod.IdBillingMethod))
            {
                return false;
            }

            var mbwayData = _opContext.OF.GetPrivateData<MBWayData>("MBWayData");

            var countryId = _applicationContext?.GetCountryId();

            var selectedCountryWithValidation = _countryService.GetCountriesWithValidations().FirstOrDefault(c => c.Id == countryId);

            if (!Regex.IsMatch(phoneNumber, selectedCountryWithValidation.CellPhoneValidation))
            {
                gotUserContextInformations.AddErrorMessage("mbway", "OP.PF.ErrMsg.Phone.Invalid");
                return false;
            }

            var pselData = gotBillingInformations.GetPrivateData<PaymentSelectionData>();
            var oChosenBillingMethod = new MBWayBillingMethod
            {
                Amount = pselData.RemainingAmount,
                PhoneNumber = phoneNumber,
                UserGUID = gotUserContextInformations.UserContextInformations.UID
            };

            gotBillingInformations.MainBillingMethod = oChosenBillingMethod;
            gotBillingInformations.RemoveBillingMethod(MBWayBillingMethod.IdBillingMethod);
            gotBillingInformations.AddBillingMethod(oChosenBillingMethod);

            mbwayData.MobilePhone = phoneNumber;

            return true;
        }


        public bool FibSelection(IGotBillingInformations gotBillingInformations, IGotUserContextInformations gotUserContextInformations)
        {
            if (!VerifyBillingMethodEligibility(gotBillingInformations, FibBillingMethod.IdBillingMethod))
            {
                return false;
            }

            var pselData = gotBillingInformations.GetPrivateData<PaymentSelectionData>();

            var oChosenBillingMethod = new FibBillingMethod
            {
                Amount = pselData.RemainingAmount,
                UserGUID = gotUserContextInformations.UserContextInformations.UID
            };

            gotBillingInformations.MainBillingMethod = oChosenBillingMethod;
            gotBillingInformations.RemoveBillingMethod(PayByRefBillingMethod.IdBillingMethod);
            gotBillingInformations.AddBillingMethod(oChosenBillingMethod);

            return true;
        }

        public bool CheckSelection(string checkNumber, IGotBillingInformations gotBillingInformations, IGotUserContextInformations gotUserContextInformations)
        {
            if (!VerifyBillingMethodEligibility(gotBillingInformations, CheckBillingMethod.IdBillingMethod))
            {
                return false;
            }

            var pselData = gotBillingInformations.GetPrivateData<PaymentSelectionData>();

            var oChosenBillingMethod = new CheckBillingMethod
            {
                Amount = pselData.RemainingAmount,
                ExpectedAmountEur = pselData.RemainingAmount,
                UserGUID = gotUserContextInformations.UserContextInformations.UID,
                CheckNumber = checkNumber,
            };

            gotBillingInformations.MainBillingMethod = oChosenBillingMethod;
            gotBillingInformations.RemoveBillingMethod(CheckBillingMethod.IdBillingMethod);
            gotBillingInformations.AddBillingMethod(oChosenBillingMethod);

            return true;
        }

        public bool UnicreSelection(int selectedCreditCardId, IGotBillingInformations gotBillingInformations)
        {
            if (!VerifyBillingMethodEligibility(gotBillingInformations, UnicreCreditCardBillingMethod.IdBillingMethod))
            {
                return false;
            }

            var cardDescriptor = _creditCardDescriptorBusiness.GetCreditCardDescriptor(selectedCreditCardId, _creditCardConfig, _applicationContext.GetSiteContext().Culture);
            if (cardDescriptor != null)
            {
                var oChosenBillingMethod = new UnicreCreditCardBillingMethod
                {
                    PaymentMean = cardDescriptor.Brand
                };
                gotBillingInformations.MainBillingMethod = oChosenBillingMethod;

                gotBillingInformations.RemoveBillingMethod(UnicreCreditCardBillingMethod.IdBillingMethod);
                gotBillingInformations.AddBillingMethod(oChosenBillingMethod);
                return true;
            }
            return false;
        }

        public bool SipsSelection(CreditCardEntity oCcd, string creditId, IGotBillingInformations gotBillingInformations, IGotUserContextInformations gotUserContextInformations)
        {
            if (!VerifyBillingMethodEligibility(gotBillingInformations, CreditCardBillingMethod.IdBillingMethod))
            {
                return false;
            }

            CustCreditCardDescriptor desc = null;
            var id = 0;

            oCcd.CardNumber = oCcd.CardNumber.Replace("-", "");

            desc = _creditCardDescriptorBusiness.GetCreditCardDescriptor(oCcd.TypeId, _creditCardConfig, _applicationContext.GetSiteContext().Culture);

            if (!VerifyCreditCardEligibility(gotBillingInformations, desc.Id, desc.Label))
            {
                return false;
            }

            if (!desc.HasExpDate)
            {
                oCcd.ExpireMonth = 0;
                oCcd.ExpireYear = 0;
            }

            var oChosenBillingMethod = new CreditCardBillingMethod
            {
                Identity = id,
                NumByPhone = false,
                VerificationCode = (!string.IsNullOrEmpty(oCcd.VerificationCode) ? oCcd.VerificationCode.Substring(0, desc.CryptoLength) : string.Empty)
            };
            oChosenBillingMethod.TypeId = oCcd.TypeId;
            oChosenBillingMethod.CardNumber = oCcd.CardNumber;
            var sExpDate = (oCcd.ExpireMonth > 9 || oCcd.ExpireMonth == 0 ? "" : "0") + oCcd.ExpireMonth + "/" + oCcd.ExpireYear;
            oChosenBillingMethod.CardExpirationDate = sExpDate;
            oChosenBillingMethod.CardHolder = oCcd.Holder;

            if (string.IsNullOrEmpty(oChosenBillingMethod.VerificationCode))
                oChosenBillingMethod.CryptoFlag = true;
            else
                oChosenBillingMethod.CryptoFlag = false;

            if (desc.Id == 4)
            {
                oChosenBillingMethod.CreditBareme = (creditId ?? "");
            }
            else
            {
                oChosenBillingMethod.CreditBareme = "";
            }


            oChosenBillingMethod.UserGUID = gotUserContextInformations.UserContextInformations.UID;

            gotBillingInformations.MainBillingMethod = oChosenBillingMethod;

            gotBillingInformations.RemoveBillingMethod(CreditCardBillingMethod.IdBillingMethod);
            gotBillingInformations.AddBillingMethod(oChosenBillingMethod);

            return true;
        }

        public bool SipsByPhoneSelection(string userId, IGotBillingInformations gotBillingInformations)
        {
            if (!_billingMethodService.IsPhonePaymentAllowed(_opContext, _opContext.OF as PopOrderForm))
            {
                return false;
            }

            var oChosenBillingMethod = new CreditCardBillingMethod
            {
                Identity = 0,
                NumByPhone = true,
                Reference = "",
                CardExpirationDate = "",
                UserGUID = userId
            };

            gotBillingInformations.MainBillingMethod = oChosenBillingMethod;

            gotBillingInformations.RemoveBillingMethod(CreditCardBillingMethod.IdBillingMethod);
            gotBillingInformations.AddBillingMethod(oChosenBillingMethod);

            return true;
        }

        public bool LimonetikSelection(int giftCardType, IGotBillingInformations gotBillingInformations, IGotUserContextInformations gotUserContextInformations)
        {
            if (!VerifyBillingMethodEligibility(gotBillingInformations, LimonetikBillingMethod.IdBillingMethod))
            {
                return false;
            }

            int cardTypeId;
            string creditCardConfig;
            string culture;

            creditCardConfig = System.Configuration.ConfigurationManager.AppSettings["config.shippingMethodSelection"];

            cardTypeId = giftCardType;

            var giftCardTypes = GetAvailableLimonetikGiftCardTypeValues(_limonetikBusiness, _opContext);
            var foundCardType = giftCardTypes.Find((c) => int.Parse(c.Value) == cardTypeId);

            if (foundCardType == null)
            {
                throw new Exception(string.Format("No limonetik gift card type associated to the id {0}", cardTypeId));
            }

            culture = _applicationContext.GetCulture();
            var desc = _creditCardDescriptorBusiness.GetCreditCardDescriptorUnfiltered(cardTypeId, creditCardConfig, culture);
            if (desc != null)
            {
                var oChosenBillingMethod = new LimonetikOgoneBillingMethod
                {
                    Identity = 0,
                    TypeId = desc.Id,
                    TypeLabel = desc.Label,
                    NumByPhone = false,
                    CryptoFlag = false,
                    Reference = string.Empty,
                    Brand = desc.Brand,
                    Pm = desc.PaymentMethod,
                    OgoneAlias = string.Empty,
                    DebitType = desc.DebitType.ToString(),
                    VirtualCard = string.Empty,
                    SavingCardAutorisation = false,
                    CreditBareme = string.Empty,
                    UserGUID = gotUserContextInformations.UserContextInformations.UID
                };

                // FIX temporaire Limonetik avec CVD
                // Forcer les valeurs Brand et PM à CB et CREDITCARD
                if (!_switchProvider.IsEnabled("op.limonetik.byogone.enabled"))
                {
                    oChosenBillingMethod.Pm = "CREDITCARD";
                    oChosenBillingMethod.Brand = "CB";
                }

                gotBillingInformations.MainBillingMethod = oChosenBillingMethod;

                gotBillingInformations.RemoveBillingMethod(OgoneCreditCardBillingMethod.IdBillingMethod);
                gotBillingInformations.AddBillingMethod(oChosenBillingMethod);

                return true;
            }
            return false;
        }

        public bool BankTransfertSelection(IGotBillingInformations gotBillingInformations)
        {
            if (!VerifyBillingMethodEligibility(gotBillingInformations, BankTransferBillingMethod.IdBillingMethod))
            {
                return false;
            }

            gotBillingInformations.RemoveBillingMethod(BankTransferBillingMethod.IdBillingMethod);

            var pselData = gotBillingInformations.GetPrivateData<PaymentSelectionData>(false);
            var bm = new BankTransferBillingMethod
            {
                Amount = pselData.RemainingAmount
            };

            gotBillingInformations.MainBillingMethod = bm;
            gotBillingInformations.AddBillingMethod(bm);

            return true;
        }

        public bool FnacCardSelection(CreditCardEntity oCcd, string creditId, IGotBillingInformations gotBillingInformations, IGotUserContextInformations gotUserContextInformations)
        {
            if (!VerifyBillingMethodEligibility(gotBillingInformations, FnacCardBillingMethod.IdBillingMethod))
            {
                return false;
            }

            oCcd.CardNumber = oCcd.CardNumber.Replace("-", "");

            var oChosenBillingMethod = new FnacCardBillingMethod
            {
                CardExpirationDate = oCcd.ExpireMonth.ToString("00") + "/" + oCcd.ExpireYear.ToString("0000"),
                CardNumber = oCcd.CardNumber,
                VerificationCode = oCcd.VerificationCode,
                CardHolder = oCcd.Holder,
                CreditType = creditId,
                UserGUID = gotUserContextInformations.UserContextInformations.UID
            };

            gotBillingInformations.MainBillingMethod = oChosenBillingMethod;
            gotBillingInformations.RemoveBillingMethod(FnacCardBillingMethod.IdBillingMethod);
            gotBillingInformations.AddBillingMethod(oChosenBillingMethod);

            return true;
        }

        public bool FnacCardCetelemSelection(IGotBillingInformations gotBillingInformations, IGotUserContextInformations gotcontextinformation, string creditId)
        {
            if (!VerifyBillingMethodEligibility(gotBillingInformations, FnacCardCetelemBillingMethod.IdBillingMethod))
            {
                return false;
            }

            gotBillingInformations.RemoveBillingMethod(FnacCardCetelemBillingMethod.IdBillingMethod);

            var bm = new FnacCardCetelemBillingMethod
            {
                UserGUID = gotcontextinformation.UserContextInformations.UID,
                CreditType = creditId
            };

            gotBillingInformations.MainBillingMethod = bm;

            gotBillingInformations.AddBillingMethod(bm);

            return true;
        }

        public bool PayOnDeliverySelection(IGotBillingInformations gotBillingInformations)
        {
            if (!VerifyBillingMethodEligibility(gotBillingInformations, PayOnDeliveryBillingMethod.IdBillingMethod))
            {
                return false;
            }

            gotBillingInformations.RemoveBillingMethod(PayOnDeliveryBillingMethod.IdBillingMethod);

            var pselData = gotBillingInformations.PrivateData.GetByType<PaymentSelectionData>();
            var bm = new PayOnDeliveryBillingMethod
            {
                Amount = pselData.RemainingAmount
            };

            gotBillingInformations.MainBillingMethod = bm;
            gotBillingInformations.AddBillingMethod(bm);
            return true;
        }

        public bool PayDeferred(IGotBillingInformations gotBillingInformations, IGotUserContextInformations gotcontextinformation)
        {
            if (!VerifyBillingMethodEligibility(gotBillingInformations, DefferedPaymentBillingMethod.IdBillingMethod))
            {
                return false;
            }

            gotBillingInformations.RemoveBillingMethod(DefferedPaymentBillingMethod.IdBillingMethod);

            var pselData = gotBillingInformations.PrivateData.GetByType<PaymentSelectionData>();
            var bm = new DefferedPaymentBillingMethod()
            {
                Amount = pselData.RemainingAmount,
                UserGUID = gotcontextinformation.UserContextInformations.UID
            };
            // @TODO : Insérer une ligne dans tbl_DeferredPaymentDocuments

            gotBillingInformations.MainBillingMethod = bm;
            gotBillingInformations.AddBillingMethod(bm);
            return true;
        }

        public bool OrangeSelection(IGotBillingInformations gotBillingInformations)
        {
            if (!VerifyBillingMethodEligibility(gotBillingInformations, OrangeBillingMethod.IdBillingMethod))
            {
                return false;
            }

            gotBillingInformations.RemoveBillingMethod(OrangeBillingMethod.IdBillingMethod);

            var pselData = gotBillingInformations.GetPrivateData<PaymentSelectionData>(false);

            var bm = new OrangeBillingMethod
            {
                Amount = pselData.RemainingAmount
            };

            gotBillingInformations.MainBillingMethod = bm;
            gotBillingInformations.AddBillingMethod(bm);

            return true;
        }

        private List<GiftCardTypeValue> GetAvailableLimonetikGiftCardTypeValues(ILimonetikBusiness limonetikBusiness, OPContext context)
        {
            var list = new List<GiftCardTypeValue>();
            var ids = limonetikBusiness.GetValidLimonetikCardTypes(context);
            if (ids != null && ids.Count > 0)
            {
                foreach (var id in ids)
                {
                    list.Add(new GiftCardTypeValue() { Label = MessageHelper.Get(_applicationContext.GetSiteContext(), string.Format("orderpipe.Preview.CustomAdvantages.GiftCard.limonetik.{0}", id)), Value = id.ToString() });
                }
            }
            return list;
        }

        public string GetErrorMessage(string messageId)
        {
            var siteContext = _applicationContext.GetSiteContext();

            if (siteContext != null)
                return _uiResourceService.GetUIResourceValue(messageId, siteContext.Culture);

            return string.Empty;
        }

        public ValidationResult CheckSipsCreditCardVerificationCode(string verificationCodeModel, int cryptoLength, bool isFnacCard = false)
        {
            if (isFnacCard)
            {
                cryptoLength = _opContext.Pipe.GlobalParameters.Get("fnaccard.cvv.length", 3);
            }

            if (verificationCodeModel.Length != cryptoLength)
            {
                return new ValidationResult(GetErrorMessage("OP.CARD.ErrMsg.crypto"));
            }
            else
            {
                if (cryptoLength > 0)
                {
                    if (!int.TryParse(verificationCodeModel, out var tmp))
                    {
                        return new ValidationResult(GetErrorMessage("OP.CARD.ErrMsg.crypto.digit"));
                    }
                }
            }
            return ValidationResult.Success;
        }

        public ValidationResult CheckSipsCreditCardHolderName(string name)
        {
            if (name.Trim().Length == 0)
            {
                return new ValidationResult(GetErrorMessage("OP.CARD.ErrMsg.holder"));
            }
            else
            {
                var pattern = @"[\w\-\. \']{1,}$";
                var regex = new Regex(pattern);
                if (!regex.IsMatch(name))
                {
                    return new ValidationResult(GetErrorMessage("OP.CARD.ErrMsg.holder"));
                }
            }

            return ValidationResult.Success;
        }

        public ValidationResult CheckSipsCreditCardExpireDate(int expireMonth, int expireYear, bool hasExpDate)
        {
            if (hasExpDate)
            {
                if (expireYear == 0 || expireMonth == 0)
                {
                    return new ValidationResult(GetErrorMessage("OP.CARD.ErrMsg.needexpdate"));
                }
                else
                {
                    var date = new DateTime(expireYear, expireMonth, 1).AddMonths(1).AddHours(-1);

                    if (DateTime.Today > date)
                    {
                        return new ValidationResult(GetErrorMessage("OP.CARD.ErrMsg.expired"));
                    }
                }
            }

            return ValidationResult.Success;
        }

        public ValidationResult CheckSipsCreditCardNumber(string cardNumber, int numberLength, int typeId, string prefix)
        {
            if (cardNumber.Length != numberLength)
            {
                return new ValidationResult(GetErrorMessage("OP.CARD.ErrMsg.number.length"));
            }
            else
            {
                var notdigit = false;
                foreach (var c in cardNumber)
                {
                    if (!char.IsDigit(c))
                    {
                        notdigit = true;
                        break;
                    }
                }

                if (notdigit)
                {
                    return new ValidationResult(GetErrorMessage("OP.CARD.ErrMsg.number.digit"));
                }
                else
                {
                    if (!_creditCardDescriptorBusiness.CheckLuhn(cardNumber))
                    {
                        return new ValidationResult(GetErrorMessage("OP.CARD.ErrMsg.number.luhn"));
                    }
                    else if (cardNumber.StartsWith("000000") || cardNumber.StartsWith("100000"))
                    {
                        return new ValidationResult(GetErrorMessage("OP.CARD.ErrMsg.number.luhn"));
                    }
                    else if (!string.IsNullOrEmpty(prefix) && !cardNumber.StartsWith(prefix))
                    {
                        return new ValidationResult(GetErrorMessage("OP.CARD.ErrMsg.number.luhn"));
                    }
                    else
                    {
                        var isEcard = _creditCardDescriptorBusiness.CheckECard(cardNumber);

                        if (typeId != 11 && isEcard)
                        {
                            return new ValidationResult(GetErrorMessage("OP.CARD.ErrMsg.number.ecard"));
                        }
                        else
                        {
                            if (typeId == 11 && !isEcard)
                            {
                                return new ValidationResult(GetErrorMessage("OP.CARD.ErrMsg.number.notecard"));
                            }
                        }


                        if (typeId == 11 && isEcard && _gotLineGroups.LineGroups.Count() > 1)
                        {
                            return new ValidationResult(GetErrorMessage("OP.CARD.ErrMsg.ecard.multiplelinegroup"));
                        }

                        if (typeId == 11 && isEcard && _globalParameters.Get("mode.shopretrieval", false))
                        {
                            return new ValidationResult(GetErrorMessage("OP.CARD.ErrMsg.ecard.shopretrieval"));
                        }
                    }
                }
            }

            return ValidationResult.Success;
        }

        public ValidationResult CheckFnacCardNumber(string cardNumber, bool checkLuhn)
        {
            if (cardNumber.Length != _opContext.Pipe.GlobalParameters.Get("fnaccard.number.length", 16))
            {
                return new ValidationResult(GetErrorMessage("OP.CARD.ErrMsg.number.length"));
            }
            else
            {
                var notdigit = false;
                foreach (var c in cardNumber)
                {
                    if (!char.IsDigit(c))
                    {
                        notdigit = true;
                        break;
                    }
                }

                if (notdigit)
                {
                    return new ValidationResult(GetErrorMessage("OP.CARD.ErrMsg.number.digit"));
                }
                else
                {
                    if (checkLuhn && !_creditCardDescriptorBusiness.CheckLuhn(cardNumber))
                    {
                        return new ValidationResult(GetErrorMessage("OP.CARD.ErrMsg.number.luhn"));
                    }
                    //else if (cardNumber.StartsWith("000000") || cardNumber.StartsWith("100000"))
                    //{
                    //    return new ValidationResult(GetErrorMessage("OP.CARD.ErrMsg.number.luhn"));
                    //}
                }
            }
            return ValidationResult.Success;
        }

        private bool VerifyCreditCardEligibility(IGotBillingInformations popOrderForm, int cardId, string cardLabel = null)
        {
            if (popOrderForm.HiddenCreditCards.Contains(cardId))
            {
                //Reset des moyens de paiement
                popOrderForm.MainBillingMethod = null;
                popOrderForm.ClearBillingMethods();

                //Reset des booleen de validation de commande
                popOrderForm.OrderGlobalInformations.OrderValidated = false;
                var onePageData = popOrderForm.GetPrivateData<PopData>(false);
                if (onePageData != null)
                {
                    onePageData.DisplayOnePageValidated = false;
                }
                var selfCheckoutData = popOrderForm.GetPrivateData<SelfCheckoutData>(false);
                if (selfCheckoutData != null)
                {
                    selfCheckoutData.DisplaySelfCheckoutValidated = false;
                }

                //Récupération du label de la carte si non fourni.
                if (string.IsNullOrEmpty(cardLabel))
                {
                    var cardDescriptor = _creditCardDescriptorBusiness.GetCreditCardDescriptor(cardId, _creditCardConfig, _applicationContext.GetSiteContext().Culture);

                    if (cardDescriptor != null)
                    {
                        cardLabel = cardDescriptor.Label;
                    }
                }

                //Ecriture d'un log.
                var text = $"Utilisation d'une carte qui a été préalablement filtrée. ";
                if (string.IsNullOrEmpty(cardLabel))
                {
                    text += $"Id de la carte: {cardId}";
                }

                else
                {
                    text += $"Type de la carte: {cardLabel}";
                }
                _logger.ErrorFormat(text);

                return false;
            }

            return true;
        }

        private bool VerifyBillingMethodEligibility(IOF of, int billingMethodId)
        {
            var paymentSelectionData = of.GetPrivateData<PaymentSelectionData>();
            if (paymentSelectionData != null)
            {
                if (!paymentSelectionData.RemainingMethods.Any(rm => rm.Id == billingMethodId))
                {
                    return false;
                }
            }
            return true;
        }

    }
}
