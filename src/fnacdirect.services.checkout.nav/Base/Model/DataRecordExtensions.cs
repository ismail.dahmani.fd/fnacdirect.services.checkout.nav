﻿using System;
using System.Data;

namespace FnacDirect.OrderPipe.Base.BaseDAL
{
    /// <summary>
    /// Extension pour vérifier si une colonne est présente dans un SqlDataReader
    /// </summary>
    internal static class DataRecordExtensions
    {
        internal static bool HasColumn(this IDataRecord dr, string columnName)
        {
            for (var i = 0; i < dr.FieldCount; i++)
            {
                if (dr.GetName(i).Equals(columnName, StringComparison.InvariantCultureIgnoreCase))
                    return true;
            }
            return false;
        }
    }
}
