using FnacDirect.OrderPipe.Base.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FnacDirect.OrderPipe.Base.Business
{
    public interface IMembershipDateBusiness
    {
        bool IsLowerThanAdherenAgeMin(DateTime pBirthDate);
        int GetAge(DateTime pBirthDate);

        bool DisplayBirthDatePopin(PopOrderForm popOrderForm);
        
    }
}
