using FnacDirect.Membership.Model.Constants;
using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.OrderPipe.Base.Model.Pop;
using FnacDirect.Technical.Framework.Web.Mvc;
using FnacDirect.OrderPipe.Base.Proxy.Membership;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FnacDirect.Context;

namespace FnacDirect.OrderPipe.Base.Business
{
    public class MembershipDateBusiness : IMembershipDateBusiness
    {
        private readonly IFnacPlusCardService _fnacPlusCardService;
        private readonly IMembershipConfigurationService _membershipConfigurationService;
        private readonly ISwitchProvider _switchProvider;
        private readonly IUserContextProvider _userContextProvider;
        private readonly int _minAge;

        public MembershipDateBusiness(IApplicationContext applicationContext,
                                        IMembershipConfigurationService membershipConfigurationService,
                                        ISwitchProvider switchProvider,
                                        IFnacPlusCardService fnacPlusCardService,
                                        IUserContextProvider userContextProvider)
        {
            var setting = applicationContext.GetAppSetting("adherent.age.min");

            if (!int.TryParse(setting, out _minAge))
            {
                _minAge = 14;
            }

            _userContextProvider = userContextProvider;
            _membershipConfigurationService = membershipConfigurationService;
            _switchProvider = switchProvider;
            _fnacPlusCardService = fnacPlusCardService;
        }

        public bool IsLowerThanAdherenAgeMin(DateTime pBirthDate)
        {
            var age = GetAge(pBirthDate);
            return age < _minAge;
        }

        public int GetAge(DateTime pBirthDate)
        {
            // Age théorique
            var age = DateTime.Now.Year - pBirthDate.Year;

            var jourAnniverssaire = pBirthDate.Day;
            if (!DateTime.IsLeapYear(DateTime.Now.Year) && pBirthDate.Day == 29 && pBirthDate.Month == 2) // le 29 n'existe pas forcement tout les ans
                jourAnniverssaire = 28;

            // Date de l'anniversaire de cette année
            var DateAnniv = new DateTime(DateTime.Now.Year, pBirthDate.Month, jourAnniverssaire);

            // Si pas encore passé, retirer 1 an
            if (DateAnniv > DateTime.Now)
                age--;

            return age;
        }

        public bool DisplayBirthDatePopin(PopOrderForm popOrderForm)
        {
            if (!IsUserAuthenticated())
            {
                return false;
            }

            var hasCards = HasValidCardInBasket(popOrderForm);
            var hasFnacPlusDecouverteInBasket = HasFnacPlusDecouverteInBasket(popOrderForm);
            if (!hasCards && !hasFnacPlusDecouverteInBasket)
            {
                return false;
            }

            var popData = popOrderForm.GetPrivateData<PopData>(create: false);

            // Si validation CGV: La popin s'affiche jusqu'à qu'elle soit valide.
            if (_switchProvider.IsEnabled("orderpipe.pop.adhesion.enable.cgvrule"))
            {
                return popData != null && !popData.MembershipCGVHasBeenConfirmed;
            }

            // Sinon: Validation sur la date de naissance et le NIF
            if (popData == null || popData.MembershipBirthDateHasBeenConfirmed)
            {
                return false;
            }

            // Fnac+ Decouverte: Forcer l'affichage (iso prod)
            if (hasFnacPlusDecouverteInBasket)
            {
                return true;
            }

            // Pour les autres cartes: Vérifier si c'est nécessaire (check date de naissance et NIF)
            var shouldShowNifPopin = _switchProvider.IsEnabled("orderpipe.pop.adhesion.enable.birthday.nif.popin");
            if (IsBirthDateValid(popOrderForm) && !shouldShowNifPopin)
            {
                return false;
            }

            return true;
        }

        #region privates

        private bool IsBirthDateValid(PopOrderForm popOrderForm)
        {
            if (popOrderForm.UserContextInformations.CustomerEntity == null)
            {
                // Can't test => default true
                return true;
            }

            // Null birthdate
            if (popOrderForm.UserContextInformations.CustomerEntity.Birthdate.Equals(new DateTime(1900, 1, 1)))
            {
                return false;
            }

            // Too young
            if (_membershipConfigurationService.IsFnacPlusEnabled
                && IsLowerThanAdherenAgeMin(popOrderForm.UserContextInformations.CustomerEntity.Birthdate))
            {
                return false;
            }

            return true;
        }

        private bool IsUserAuthenticated()
        {
            var userContext = _userContextProvider.GetCurrentUserContext();
            return userContext != null && userContext.IsAuthenticated;
        }

        private bool HasFnacPlusDecouverteInBasket(PopOrderForm popOrderForm)
        {
            return popOrderForm.LineGroups.Fnac().GetArticles().Any(a => a.TypeId.GetValueOrDefault() == (int)CategoryCardType.SouscriptionFnacPlusTrialDecouverte);
        }

        private bool HasValidCardInBasket(PopOrderForm popOrderForm)
        {
            var articles = popOrderForm.LineGroups.Fnac().GetArticles();

            // Try card
            if(articles.Any(a => a.ProductID == _membershipConfigurationService.GetTryCardPrid()))
            {
                return true;
            }

            // Adherent card
            if (articles.OfType<StandardArticle>().Any(a => a.IsAdhesionCard))
            {
                return true;
            }

            // Fnac+
            if (articles.Any(a => _membershipConfigurationService.FnacPlusCardTypes.Contains(a.TypeId.GetValueOrDefault())))
            {
                return true;
            }

            // Fnac+ Try Card
            if (_fnacPlusCardService.GetFnacPlusTryCardFromBasket(popOrderForm.LineGroups) != null)
            {
                return true;
            }

            return false;
        }

        #endregion 
    }
}
