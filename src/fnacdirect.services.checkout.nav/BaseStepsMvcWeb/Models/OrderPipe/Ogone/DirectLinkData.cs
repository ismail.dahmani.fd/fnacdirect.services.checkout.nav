using FnacDirect.Ogone.Model;
using FnacDirect.Technical.Framework.CoreServices;
using System.Collections.Generic;

namespace FnacDirect.OrderPipe.Base.Model
{
    public class DirectLinkData : GroupData
    {
        public SerializableDictionary<string, string> ResponseDirectLink { get; set; }
        
        public OrderTransaction OrderTransaction { get; set; }

        public string OgoneResult { get; set; }

        public FallBackOption? FallbackResult { get; set; } = null;

        public string OrderTemplateTemplateRef { get; set; }
        public string AliasSend { get; set; }
        public string CreditCardReference { get; set; }

        public bool IsEmpty()
        {
            return string.IsNullOrEmpty(OgoneResult);
        }

        /// <summary>
        /// Instantiate a new SerializableDictionary and copy the KeyValues from the dictionary in parameter
        /// </summary>
        /// <param name="dicoSource"></param>
        /// <returns></returns>
        public SerializableDictionary<string, string> ConvertTo(Dictionary<string,string> dicoSource)
        {
            var newDico = new SerializableDictionary<string, string>();
            foreach (var key in dicoSource.Keys)
                newDico.Add(key, (string)dicoSource[key]);

            return newDico;
        }
    }
}

