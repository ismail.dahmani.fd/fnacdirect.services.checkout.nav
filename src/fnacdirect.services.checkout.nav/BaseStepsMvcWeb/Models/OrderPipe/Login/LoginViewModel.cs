﻿using FnacDirect.Technical.Framework.Web.Mvc.DataAnnotations;
namespace FnacDirect.OrderPipe.BaseMvc.Web.Pop.Models.Login
{
    public class LoginViewModel
    {
        public string FirstName { get; set; }

        [DisplayName("orderpipe.pop.login.email")]
        [Required(ErrorMessage = "api.login.rest.v2.email.required")]
        [EmailAddress(ErrorMessage = "api.login.rest.v2.email.valid")]
        public string Email { get; set; }

        [DisplayName("orderpipe.pop.login.password")]
        [Required(ErrorMessage = "api.login.rest.v2.password.required")]
        public string Password { get; set; }

        public bool RememberMe { get; set; }
    }
}
