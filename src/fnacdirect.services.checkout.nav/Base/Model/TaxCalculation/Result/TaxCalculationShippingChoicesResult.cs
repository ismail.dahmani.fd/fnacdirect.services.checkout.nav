using System.Collections.Generic;

namespace FnacDirect.OrderPipe.Base.Model.TaxCalculation
{
    public class TaxCalculationShippingChoicesResult
    {
        public List<TaxCalcultationShippingChoiceResult> Choices { get; set; }
        public TaxInfos TaxInfos { get; set; }
    }


    public class TaxCalcultationShippingChoiceResult
    {
        public int MainShippingMethodId { get; set; }
        public GenericPrice TotalPrice { get; set; }
        public IEnumerable<TaxCalculationGroupResult> Groups { get; set; }
    }

    public class TaxCalculationGroupResult
    {
        public int Id { get; set; }
        public GenericPrice GlobalPrice { get; set; }
        public IEnumerable<TaxCalcuationItemInGroupResult> Items { get; set; }
    }

    public class TaxCalcuationItemInGroupResult
    {
        public Identifier Identifier { get; set; }
        public GenericPrice UnitPrice { get; set; }
    }
}
