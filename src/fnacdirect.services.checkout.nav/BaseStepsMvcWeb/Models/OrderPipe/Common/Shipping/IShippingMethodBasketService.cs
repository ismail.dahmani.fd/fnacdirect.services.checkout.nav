﻿using System.Collections.Generic;
using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.OrderPipe.Base.Model.Facets;
using FnacDirect.OrderPipe.Base.Model.LineGroups;
using FnacDirect.OrderPipe.Shipping.Model;
using FnacDirect.OrderPipe.Base.LineGroups;

namespace FnacDirect.OrderPipe.Base.Business.Shipping
{
    public interface IShippingMethodBasketService
    {
        ShippingMethodBasket GetShippingMethodBasketForOrderAndAddress(IGotShippingMethodEvaluation iContainsShippingMethodEvaluation, ILineGroup lineGroup, ShippingAddress shippingAddress);
        ShippingMethodBasket GetShippingMethodBasketForOrderAndAddress(IGotShippingMethodEvaluation iContainsShippingMethodEvaluation, IEnumerable<ILineGroup> lineGroups, ShippingAddress shippingAddress);
        ShippingMethodBasket GetShippingMethodBasketForOrderAndAddress(IGotShippingMethodEvaluation iContainsShippingMethodEvaluation, IDictionary<ILineGroupHeader, IEnumerable<Article>> orderInput, ShippingAddress shippingAddress);
    }
}
