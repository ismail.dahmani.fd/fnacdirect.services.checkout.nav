using System.Xml.Serialization;

namespace FnacDirect.OrderPipe.Base.Model
{
    public class SogepPushInfo
    {

        [XmlIgnore]
        private int? _NoPushSogep;

        public int? NoPushSogep
        {
            get
            {
                return _NoPushSogep;
            }
            set
            {
                _NoPushSogep = value;
            }
        }

        private int _totalQuantityPushSogep;
        [XmlIgnore]
        public int TotalQuantityPushSogep
        {
            get
            {
                return _totalQuantityPushSogep;
            }
            set
            {
                _totalQuantityPushSogep = value;
            }
        }
        
        private decimal _totalPricePushSogep;
        [XmlIgnore]
        public decimal TotalPricePushSogep
        {
            get
            {
                return _totalPricePushSogep;
            }
            set
            {
                _totalPricePushSogep = value;
            }
        }

        private bool _pushOnBasket;

        [XmlIgnore]
        public bool PushOnBasket
        {
            get { return _pushOnBasket; }
            set { _pushOnBasket = value; }
        }
        private bool _pushOnPreview;

        [XmlIgnore]
        public bool PushOnPreview
        {
            get { return _pushOnPreview; }
            set { _pushOnPreview = value; }
        }

    }
}
