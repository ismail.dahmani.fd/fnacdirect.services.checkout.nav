﻿using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.OrderPipe.Base.Model.LineGroups;
using FnacDirect.OrderPipe.Config;
using FnacDirect.OrderPipe.Core.Services;
using FnacDirect.OrderPipe.Shipping.Model;
using System;
using System.Collections.Generic;

namespace FnacDirect.OrderPipe.Base.Business.Shipping.Rules
{
    public abstract class BaseShippingMethodBasketBuildingRule : IShippingMethodBasketBuildingRule
    {
        protected BaseShippingMethodBasketBuildingRule()
        {
        }

        public abstract string SwitchId { get; }

        public IPipeParametersProvider PipeParametersProvider { get; set; }

        public abstract Func<ShippingMethodBasket, ShippingMethodBasket> GetModifier(IPrivateDataContainer privateDataContainer, IEnumerable<Article> articles, ShippingAddress shippingAddress);
    }
}
