using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using FnacDirect.Contracts.Online.Model;
using FnacDirect.OrderPipe.Base.DAL;
using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.OrderPipe.Base.Model.Facets;
using FnacDirect.Technical.Framework.CoreServices;
using FnacDirect.VirtualGiftCheck.BLL;
using FnacDirect.VirtualGiftCheck.Model;
using Article = FnacDirect.OrderPipe.Base.Model.Article;

namespace FnacDirect.OrderPipe.Base.Business
{
    public enum AddVoucherStatus
    {
        OK,
        KOWithErase,
        KOWithoutErase,
        KOKeep
    }

    public class VoucherBusiness : IVoucherBusiness
    {
        private readonly CatalogDAL _catalogDAL = new CatalogDAL();
        private readonly CustomerDAL _customerDAL = new CustomerDAL();
        private readonly BlacklistBusiness _blacklistBusiness = new BlacklistBusiness();

        public enum CheckCode
        {
            Valid,
            DoesNotExist,
            Expired,
            NotAppliable,
            NotCumulable
                // NLA -- 20100520
            ,
            DifferentAccount
          , ContinueLogIn
            // ---------------
        }

        public void CheckExistingVouchers(MultiFacet<IGotBillingInformations, IGotLineGroups, IGotUserContextInformations> multiFacet, SiteContext context)
        {
            var shop = 1;
            var vbmCount = 0;
            var toRemove = new List<BillingMethod>();
            foreach (var billingMethod
                in CollectionUtils.FilterOnType<VoucherBillingMethod>(multiFacet.Facet1.OrderBillingMethods))
            {
                if (ConfigurationManager.Current.Maintenance.CCV)
                {
                    toRemove.Add(billingMethod);
                    continue;
                }

                var vi = GetVoucherInfo(billingMethod.CheckNumber, shop);
                if (vi == null || !vi.Valid)
                {
                    toRemove.Add(billingMethod);
                    continue;
                }

                // NLA -- 20100520
                if (vi.VerifyAccount)
                {
                    var LoggedAccountId = 0;
                    if (multiFacet.Facet3.UserContextInformations.CustomerEntity == null)
                    {
                        var accountId = _customerDAL.GetAccountId(multiFacet.Facet3.UserContextInformations.UID);
                        if (accountId.AccountPk.HasValue && accountId.AccountPk.Value != -1)
                            LoggedAccountId = accountId.AccountPk.Value;
                    }
                    else
                    {
                        LoggedAccountId = multiFacet.Facet3.UserContextInformations.AccountId;
                    }

                    if ((LoggedAccountId == 0)
                            || (LoggedAccountId != 0 && vi.ReceiverAccountId != LoggedAccountId))
                    {
                        toRemove.Add(billingMethod);
                        continue;
                    }
                }
                // ---------------


                if (!CheckUsageConditions(multiFacet.Facet1, multiFacet.Facet2, context, vi, out var basketAmount))
                {
                    toRemove.Add(billingMethod);
                    continue;
                }

                vbmCount++;
                if (vbmCount > 1)
                {
                    //    Si le coupon qu'on ajoute n'est pas cumulable
                    // ou si le coupon déjà entré n'est pas cumulable
                    // ou si il est d'un autre type de génération
                    if (!vi.Cumulative || !billingMethod.IsCumulable.Value
                        || billingMethod.GenId != vi.GenType
                        || (vi.MaxCount != 0 && vbmCount > vi.MaxCount))
                    {
                        toRemove.Add(billingMethod);
                        continue;
                    }
                }

                if (vi.AmountSlice > 0 && ((basketAmount - (vi.AmountSlice * (vbmCount - 1)) < vi.AmountSlice)))
                {
                    toRemove.Add(billingMethod);
                    continue;
                }

                billingMethod.CheckId = vi.Id;
                billingMethod.TypeId = vi.Type;
                billingMethod.AmountEur = vi.Amount;
                billingMethod.Amount = vi.Amount;
                billingMethod.GenId = vi.GenType;
                billingMethod.IsCumulable = vi.Cumulative;
                billingMethod.ShopId = shop;
                billingMethod.DematCondition = (VoucherBillingMethod.DematConditionEnum)((int)vi.DematCondition);
                billingMethod.Valid = true;

            }

            foreach (var item in toRemove)
            {
                multiFacet.Facet1.RemoveBillingMethod(item.BillingMethodId);
            }
        }

        public CheckCode CheckVoucher(string code, int shop, BaseOF of, out VoucherInfo info, SiteContext context)
        {
            return CheckVoucher(code, shop, of, of, of, out info, context);
        }

        public CheckCode CheckVoucher(string code, int shop, IGotUserContextInformations userContext, IGotLineGroups lineGroups, IGotBillingInformations igGotBillingInformations, out VoucherInfo info, SiteContext context)
        {
            info = null;

            if (!CheckAlreadyAdded(igGotBillingInformations, code))
                return CheckCode.NotAppliable;

            var type = VGCNumber.GetCheckType(code);

            if (type == -1)
                return CheckCode.DoesNotExist;
            if (type == -2)
                return CheckCode.Expired;

            var vi = GetVoucherInfo(code, shop);

            if (vi == null || !vi.Valid)
                return CheckCode.DoesNotExist;

            // NLA -- 20100520
            if (vi.VerifyAccount)
            {
                var LoggedAccountId = 0;
                if (userContext.UserContextInformations.CustomerEntity == null)
                {
                    var accountId = _customerDAL.GetAccountId(userContext.UserContextInformations.UID);
                    if (accountId.AccountPk.HasValue && accountId.AccountPk.Value != -1)
                        LoggedAccountId = accountId.AccountPk.Value;
                }
                else
                {
                    LoggedAccountId = userContext.UserContextInformations.AccountId;
                }

                if (LoggedAccountId == 0)
                {
                    return CheckCode.ContinueLogIn;
                }
                else if (LoggedAccountId != 0 && vi.ReceiverAccountId != LoggedAccountId)
                {
                    return CheckCode.DifferentAccount;
                }
            }
            // ---------------

            if (IsVoucherDemat(vi) && !lineGroups.HasNumericalArticle())
                return CheckCode.NotAppliable;


            if (!CheckUsageConditions(igGotBillingInformations, lineGroups, context, vi, out var basketAmount))
                return CheckCode.NotAppliable;

            var ret = CheckCumulativeAddition(igGotBillingInformations, vi, basketAmount);

            if (ret == CheckCode.Valid)
                info = vi;

            return ret;
        }

        private bool CheckAlreadyAdded(IGotBillingInformations igotBillingInformations, string code)
        {
            foreach (var vbm in CollectionUtils.FilterOnType<VoucherBillingMethod>(igotBillingInformations.OrderBillingMethods))
            {
                if (vbm.CheckNumber == code)
                    return false;
            }
            return true;
        }

        private CheckCode CheckCumulativeAddition(IGotBillingInformations of, VoucherInfo vi, decimal basketAmount)
        {
            var vbmCount = 0;
            var montantPanier = (decimal)of.GlobalPrices.TotalPriceEur;
            foreach (var vbm in CollectionUtils.FilterOnType<VoucherBillingMethod>(of.OrderBillingMethods))
            {
                //on calcule le montant restant à payer
                montantPanier -= (decimal)vbm.AmountEur;
                //Pour le amountSlice on s'interesse qu'aux coupons de méme type que celui ci
                if (vbm.TypeId == vi.Type)
                    vbmCount++;
                //    Si le coupon qu'on ajoute n'est pas cumulable
                // ou si le coupon déjà entré n'est pas cumulable
                // ou si il est d'un autre type de génération
                if (!vi.Cumulative || (vbm.IsCumulable.HasValue && !vbm.IsCumulable.Value) || vbm.GenId != vi.GenType || (vi.MaxCount != 0 && vbmCount >= vi.MaxCount))
                    return CheckCode.NotCumulable;

            }

            if (vi.Cumulative && vi.AmountSlice > 0)
            {
                if (basketAmount - (vi.AmountSlice * vbmCount) < vi.AmountSlice)
                    return CheckCode.NotAppliable;
            }
            //On vérifie si on a dépassé le montant total du panier
            if (montantPanier < vi.Amount)
                return CheckCode.NotAppliable;

            return CheckCode.Valid;
        }

        private VoucherInfo GetVoucherInfo(string code, int shop)
        {
            var vi = VGCNumber.CheckCUUValidity(code, shop);

            //if (vi != null)
            //{
            //    switch (vi.GenType)
            //    {
            //        case 20: //cGenerationTypeDVD2006
            //        case 24: //cGenerationTypeCDDVD2008
            //        case 25: //cGenerationTypeCDDVD2009
            //            vi.AmountSlice = 10.0m;
            //            break;
            //        case 22: //cGenerationTypePT2006
            //            vi.AmountSlice = 50.0m;
            //            vi.MaxCount = 4;
            //            break;
            //    }
            //}
            return vi;
        }

        private bool CheckUsageConditions(IGotBillingInformations igotbGotBillingInformations, IGotLineGroups igoGotLineGroups, SiteContext context, VoucherInfo vi, out decimal basketAmount)
        {
            var vgcBus = new VGCBusiness();
            var conditions = vgcBus.GetCUUUsingCondition(vi.Id);
            basketAmount = 0;
            if (conditions == null || conditions.Count == 0)
                return false;

            var callCatalog = false;

            vi.DematCondition = DematConditionEnum.Mixed;
            foreach (var cuu in conditions)
            {
                switch (cuu.ConditionId)
                {
                    // ' Valable pour le rayon, Valable pour le type article,Valable hors rayon, Valable hors type article
                    case 1:
                    case 3:
                    case 4:
                    case 5:
                    case 6:
                    case 8:
                    case 9:
                    case 14:
                    case 15:
                    case 17:
                        callCatalog = true;
                        break;
                    //case 23:
                    //    if (!string.IsNullOrEmpty(OF.OrderInfo.AdvantageCode))
                    //        return ErrorCode.NotAppliable;
                    //    break;
                    case 32: // numérique only
                        vi.DematCondition = DematConditionEnum.DematOnly;
                        break;
                    case 33: // physique only
                        vi.DematCondition = DematConditionEnum.PhysicalOnly;
                        break;
                }
            }

            if (callCatalog)
            {
                var conditionLst = BuildConditionList(conditions);

                var articleList = BuildArticleList(igoGotLineGroups.LineGroups.SelectMany(lg => lg.Articles));

                var valid = _catalogDAL.ValidateCUUConditions(
                    articleList,
                    conditionLst,
                    igotbGotBillingInformations.GlobalPrices.TotalPriceEur.GetValueOrDefault(),
                    igotbGotBillingInformations.GlobalPrices.TotalFnacShipPriceUserEur.GetValueOrDefault(),
                    out basketAmount,
                    context.MarketId);

                if (!valid)
                    return false;
                vi.Amount = Math.Min(vi.Amount, basketAmount);
            }
            else
            {
                return false;
            }
            return true;
        }

        private static string BuildConditionList(IEnumerable<CUUCondition> conditions)
        {            
            var szConditionLst = new StringBuilder();

            foreach (var cuu in conditions)
            {
                szConditionLst.Append(cuu.ToString());
                szConditionLst.Append("#");
            }

            return szConditionLst.ToString().TrimEnd(new char[] { '#' });            
        }

        private static string BuildArticleList(IEnumerable<Article> articles)
        {
            var artLst = new StringBuilder();
            foreach (var item in CollectionUtils.FilterOnType<StandardArticle>(articles))
            {
                if (artLst.Length > 0)
                    artLst.Append("#");

                artLst.AppendFormat("{0},{1},{2},{3},{4},{5}",
                    item.ID.Value.ToString(),
                    (item.Quantity.Value * item.PriceRealEur.Value).ToString(CultureInfo.InvariantCulture),
                    (item.Quantity.Value * item.PriceUserEur.Value).ToString(CultureInfo.InvariantCulture),
                    item.TypeId.Value.ToString(),
                    string.Format("'{0}'", item.Promotions.Count > 0 ? item.Promotions[0].Code : string.Empty),
                    item.IsNumerical ? 1 : 0
                );

                // Add services
                if (item.Services.Count > 0)
                {
                    foreach (var ServiceItem in item.Services)
                    {
                        artLst.AppendFormat("#{0},{1},{2},{3},{4},{5}",
                            ServiceItem.ID.Value.ToString(),
                            (ServiceItem.Quantity.Value * ServiceItem.PriceRealEur.Value).ToString(CultureInfo.InvariantCulture),
                            (ServiceItem.Quantity.Value * ServiceItem.PriceUserEur.Value).ToString(CultureInfo.InvariantCulture),
                            ServiceItem.TypeId.Value.ToString(),
                            string.Format("'{0}'", ServiceItem.Promotions.Count > 0 ? ServiceItem.Promotions[0].Code : string.Empty),
                            "NULL"
                        );
                    }
                }
            }

            return artLst.ToString();
        }


        public void RemoveVoucher(IGotBillingInformations orderForm, string code)
        {
            var vbm = orderForm.OrderBillingMethods.OfType<VoucherBillingMethod>().FirstOrDefault(v => v.CheckNumber == code);
            if (vbm != null)
                orderForm.RemoveBillingMethod(vbm.BillingMethodId);
        }

        public AddVoucherStatus AddVoucher(OPContext context, string code)
        {
            var orderForm = context.OF as BaseOF;
            return AddVoucher(context, orderForm, orderForm, orderForm, orderForm, code);
        }

        public AddVoucherStatus AddVoucher(OPContext context, IGotLineGroups igGotLineGroups, IGotBillingInformations billingInformations, IGotUserContextInformations userContext, IGotContextInformations contextInformations, string code)
        {
            if (string.IsNullOrEmpty(code))
                return AddVoucherStatus.KOWithoutErase;


            if (userContext.UserContextInformations.CustomerEntity != null)
            {
                var acc = new Account(userContext.UserContextInformations.CustomerEntity);
                if (acc.IsBlackListed)
                {
                    contextInformations.UserMessages.Add("voucher", UserMessage.MessageLevel.Error, "OP.CUU.ErrMsg.Invalid");
                    return AddVoucherStatus.KOWithoutErase;
                }
            }
            else
            {
                var limit = context.Pipe.GlobalParameters.Get("vgc.blacklist.limit", 50);
                if (userContext.UserContextInformations.BlackListCounter >= limit)
                {
                    contextInformations.UserMessages.Add("voucher", UserMessage.MessageLevel.Error, "OP.CUU.ErrMsg.Invalid");
                    return AddVoucherStatus.KOWithoutErase;
                }
            }

            const int shopId = 1;

            var chkcode = CheckVoucher(code, shopId, userContext, igGotLineGroups, billingInformations, out var info, context.SiteContext);

            var priceBusiness = new PriceBusiness(context.SiteContext.MarketId);

            var vatRateId = context.Pipe.GlobalParameters.Get("wrapping.vat.rate.id", -2);
            var localCountry = context.Pipe.GlobalParameters.GetAsInt("culture.localcountry.id");

            switch (chkcode)
            {
                case CheckCode.Valid:
                    {
                        var meth = new VoucherBillingMethod
                        {
                            CheckNumber = code,
                            CheckId = info.Id,
                            Amount = info.Amount,
                            AmountEur = info.Amount,
                            AmountEurNoVAT = priceBusiness.GetPriceHT(info.Amount, vatRateId, localCountry),
                            GenId = info.GenType,
                            IsCumulable = info.Cumulative,
                            ShopId = shopId,
                            Valid = true,
                            TypeId = info.Type,
                            DematCondition = (VoucherBillingMethod.DematConditionEnum) ((int) info.DematCondition)
                        };
                        billingInformations.AddBillingMethod(meth);
                    }
                    return AddVoucherStatus.OK;
                case CheckCode.DoesNotExist:
                    TestAndBlackList(userContext, context);
                    contextInformations.UserMessages.Add("voucher", UserMessage.MessageLevel.Error, "OP.CUU.ErrMsg.Invalid");
                    break;
                case CheckCode.Expired:
                    contextInformations.UserMessages.Add("voucher", UserMessage.MessageLevel.Error, "OP.CUU.ErrMsg.Expired");
                    break;
                case CheckCode.NotAppliable:
                    contextInformations.UserMessages.Add("voucher", UserMessage.MessageLevel.Error, "OP.CUU.ErrMsg.NotAppliable");
                    break;
                case CheckCode.NotCumulable:
                    contextInformations.UserMessages.Add("voucher", UserMessage.MessageLevel.Error, "OP.CUU.ErrMsg.NotCumulable");
                    break;
                case CheckCode.DifferentAccount:
                    contextInformations.UserMessages.Add("voucher", UserMessage.MessageLevel.Error, "OP.CUU.ErrMsg.DifferentAccount");
                    return AddVoucherStatus.KOWithErase;
                case CheckCode.ContinueLogIn:
                    contextInformations.UserMessages.Add("voucher", UserMessage.MessageLevel.Error, "OP.CUU.ErrMsg.ContinueLogIn");
                    return AddVoucherStatus.KOKeep;
            }

            return AddVoucherStatus.KOWithoutErase;
        }

        private void TestAndBlackList(IGotUserContextInformations orderForm, OPContext context)
        {
            if (orderForm.UserContextInformations.CustomerEntity != null)
            {
                var acc = new Account(orderForm.UserContextInformations.CustomerEntity);
                acc.RegisterFailure();
            }
            else
            {
                var limit = context.Pipe.GlobalParameters.Get("vgc.blacklist.limit", 50);
                orderForm.UserContextInformations.BlackListCounter++;
                if (orderForm.UserContextInformations.BlackListCounter >= limit)
                    _blacklistBusiness.BlacklistCustomer(orderForm.UserContextInformations.AccountId);
            }
        }

        public enum Usability
        {
            Inactive,
            Unavailable,
            Used,
            Addable,
            Usable,
        }

        public Usability GetVoucherUsability(IGotBillingInformations igotbiBillingInformations, List<CUUItem> codes)
        {
            codes.Clear();
            if (!string.IsNullOrEmpty(igotbiBillingInformations.OrderGlobalInformations.AdvantageCode))
                return Usability.Unavailable;

            var i = 0;
            var cumul = false;
            foreach (var billingmeth
                in CollectionUtils.FilterOnType<VoucherBillingMethod>(igotbiBillingInformations.OrderBillingMethods))
            {
                codes.Add(new CUUItem(billingmeth.CheckNumber, billingmeth.Amount.GetValueOrDefault(), billingmeth.Valid.GetValueOrDefault()));
                cumul |= billingmeth.IsCumulable.GetValueOrDefault();
                i++;
            }
            if (i != 0)
            {
                return cumul ? Usability.Addable : Usability.Used;
            }
            return Usability.Usable;
        }

        private bool IsVoucherDemat(VoucherInfo vi)
        {
            return vi.GenType == 35;
        }
    }
}

