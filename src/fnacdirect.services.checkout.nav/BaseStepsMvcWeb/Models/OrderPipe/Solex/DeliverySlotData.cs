using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace FnacDirect.OrderPipe.Base.Model.Solex
{
    public class DeliverySlotData : CustomerData
    {
        public DeliverySlot DeliverySlotSelected { get; set; }
        public List<DeliverySlot> DeliverySlotChoices { get; set; }

        public int ChoiceShippingMethodId { get; set; }
        public int SlotShippingMethodId { get; set; }

        [XmlIgnore]
        public List<int> ParcelIdPks { get; set; } = new List<int>();
        public bool HasSelectedSlot
        {
            get
            {
                return DeliverySlotSelected != null;
            }
        }

        public DateTime? StartDateReference { get; set; }

        public bool HasStartDateReference
        {
            get
            {
                return StartDateReference != null;
            }
        }

        public bool IsImmediateDelivery { get; set; }
    }
}
