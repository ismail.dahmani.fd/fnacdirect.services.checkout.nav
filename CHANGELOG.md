# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## Unreleased

### Added

- Something is being added

## 1.0.1 - 2021-04-15

### Added

- Something was added

### Changed

- Something was changed

### Fixed

- Some bug was fixed

## 1.0.0 - 2021-03-01

Initial version

