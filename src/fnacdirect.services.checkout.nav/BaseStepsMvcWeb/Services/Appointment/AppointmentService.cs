﻿using System.Collections.Generic;
using System.Linq;
using FnacDirect.Appointment.Business;
using FnacDirect.Appointment.Model;
using FnacDirect.Technical.Framework.Utils;

namespace FnacDirect.OrderPipe.Base.Proxy.Appointment
{
    public class AppointmentService : IAppointmentService
    {
        private readonly AppointmentManager _appointmentManager;


        public AppointmentService()
        {
            _appointmentManager = new AppointmentManager();
        }

        public RangeSet<int> GetAppointmentArticleTypeIds()
        {
            return AppointmentConfigurationManager.Instance.GetAppointmentArticleTypeIds();
        }

        public void SaveAppointments(IEnumerable<FnacDirect.Appointment.Model.Appointment> appointments, string user, string comment)
        {
            _appointmentManager.SaveAppointments(appointments.ToList(), new ApplicativeUser { FnacUserId = AppointmentConfigurationManager.Instance.GetApplicationUserId(user) }, comment);
        }
    }
}
