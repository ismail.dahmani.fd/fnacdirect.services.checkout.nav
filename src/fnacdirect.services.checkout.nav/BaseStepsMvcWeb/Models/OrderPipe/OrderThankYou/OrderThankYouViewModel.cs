using FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Common.Payment;
using System;
using System.Collections.Generic;
using System.Linq;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.OrderThankYou
{
    public class OrderThankYouViewModel
    {
        [Flags]
        private enum OrderThankYouViewModelContent
        {
            None = 0,
            DigiCopy = 1,
            Ebook = 2,
            DematSoft = 4,
            PayByPhone = 8,
            FormAtHome = 16,
            BillingOnly = 32,
            TvRoyalty = 64,
            OtyDifferedAmount = 256,
            ErrorScoring = 512,
            PayByRef = 1024,
            SteedFromStore = 2048,
            BankTransfer = 4096,
            Cup = 8192,
            Donation = 16384,
            Deezer = 32768,
            Fib = 65536,
            DeliverySlotInfo = 131072,
            CheckPayment = 262144,
            DeferredPayment = 524288, //2 exp 18
            KoboOrange = 1048576,
            CanalPlay = 2097152,
            MBWay = 4194304
        }

        private readonly List<OrderReferenceViewModel> _references = new List<OrderReferenceViewModel>();
        private readonly List<OrderDetailViewModel> _orderDetails = new List<OrderDetailViewModel>();
        private OrderThankYouViewModelContent _content;

        public OrderThankYouViewModel(string email, string mainOrderUserReference)
        {
            Email = email;
            MainOrderUserReference = mainOrderUserReference;
        }

        public string Email { get; }

        public CupViewModel Cup { get; set; }

        #region Flags

        public void WithEbook()
        {
            _content = _content | OrderThankYouViewModelContent.Ebook;
        }
        public bool HasEbook => _content.HasFlag(OrderThankYouViewModelContent.Ebook);

        public void WithDematSoft()
        {
            _content = _content | OrderThankYouViewModelContent.DematSoft;
        }
        public bool HasDematSoft => _content.HasFlag(OrderThankYouViewModelContent.DematSoft);

        public void WithPaybyPhone()
        {
            _content = _content | OrderThankYouViewModelContent.PayByPhone;
        }
        public bool HasPayByPhone => _content.HasFlag(OrderThankYouViewModelContent.PayByPhone);

        public void WithFormAtHome()
        {
            _content = _content | OrderThankYouViewModelContent.FormAtHome;
        }
        public bool HasFormAtHome => _content.HasFlag(OrderThankYouViewModelContent.FormAtHome);

        public void IsBillingOnly()
        {
            _content = _content | OrderThankYouViewModelContent.BillingOnly;
        }
        public bool BillingOnly => _content.HasFlag(OrderThankYouViewModelContent.BillingOnly);

        public void WithTvRoyalty()
        {
            _content = _content | OrderThankYouViewModelContent.TvRoyalty;
        }
        public bool HasTvRoyalty => _content.HasFlag(OrderThankYouViewModelContent.TvRoyalty);

        public void WithOtyDifferedAmount()
        {
            _content = _content | OrderThankYouViewModelContent.OtyDifferedAmount;
        }
        public bool HasOtyDifferedAmount => _content.HasFlag(OrderThankYouViewModelContent.OtyDifferedAmount);

        public void WithErrorScoring()
        {
            _content = _content | OrderThankYouViewModelContent.ErrorScoring;
        }
        public bool HasErrorScoring => _content.HasFlag(OrderThankYouViewModelContent.ErrorScoring);

        public void WithCup()
        {
            _content = _content | OrderThankYouViewModelContent.Cup;
        }
        public bool HasCup => _content.HasFlag(OrderThankYouViewModelContent.Cup);

        public void WithCheckPayment()
        {
            _content = _content | OrderThankYouViewModelContent.CheckPayment;
        }
        public bool HasPayByCheck => _content.HasFlag(OrderThankYouViewModelContent.CheckPayment);

        public void WithDeferredPayment()
        {
            _content = _content | OrderThankYouViewModelContent.DeferredPayment;
        }
        public bool HasDeferredPayment => _content.HasFlag(OrderThankYouViewModelContent.DeferredPayment);

        public void WithKoboOrange()
        {
            _content = _content | OrderThankYouViewModelContent.KoboOrange;
        }
        public bool HasKoboOrange => _content.HasFlag(OrderThankYouViewModelContent.KoboOrange);

        public void WithCanalPlay()
        {
            _content = _content | OrderThankYouViewModelContent.CanalPlay;
        }
        public bool HasCanalPlay => _content.HasFlag(OrderThankYouViewModelContent.CanalPlay);

        #endregion

        public string MainOrderUserReference { get; }
        public string OrderExternalReference { get; set; }
        public AdvertisingViewModel Advertising { get; set; }

        public void WithReference(OrderReferenceViewModel orderReference)
        {
            _references.Add(orderReference);

            orderReference.Index = _references.Count;

            foreach (var reference in _references)
            {
                reference.Total = _references.Count;
            }
        }

        public IReadOnlyCollection<OrderReferenceViewModel> References => _references.AsReadOnly();

        public void WithPayByRef(string merchantId, string reference, string amountEur)
        {
            _content = _content | OrderThankYouViewModelContent.PayByRef;
            PayByRef = new PayByRefViewModel()
            {
                MerchantId = merchantId,
                Reference = reference,
                AmountEur = amountEur
            };
        }
        public bool HasPayByRef => _content.HasFlag(OrderThankYouViewModelContent.PayByRef);


        public void WithMBWay(string phoneNumber)
        {
            _content |= OrderThankYouViewModelContent.MBWay;
            MBWay = new MBWayViewModel()
            {
                PhoneNumber = phoneNumber
            };
        }

        public bool HasMBWay => _content.HasFlag(OrderThankYouViewModelContent.MBWay);
        public bool HasCaixaUncertain { get; set; }

        public void WithBankTransfer(BankTransfertMethodViewModel banktransferviewmodel)
        {
            _content = _content | OrderThankYouViewModelContent.BankTransfer;
            BankTransfer = banktransferviewmodel;
        }
        public bool HasBankTransfer => _content.HasFlag(OrderThankYouViewModelContent.BankTransfer);

        public string DifferedAmount { get; set; }
        public bool DisplayMembershipMessage { get; set; }

        private readonly List<ShipToStoreInformation> _shipToStoreInformation = new List<ShipToStoreInformation>();

        public void WithShipToStoreInformation(IEnumerable<ShipToStoreInformation> info)
        {
            _shipToStoreInformation.AddRange(info);
        }
        public IReadOnlyCollection<ShipToStoreInformation> ShipToStoreInformation => _shipToStoreInformation.AsReadOnly();

        public void WithSteedFromStore(string deliveryDate, string phoneNumber)
        {
            _content |= OrderThankYouViewModelContent.SteedFromStore;

            SteedFromStore = new SteedFromStoreViewModel
            {
                DeliveryDate = deliveryDate,
                PhoneNumber = phoneNumber
            };
        }
        public bool HasSteedFromStore => _content.HasFlag(OrderThankYouViewModelContent.SteedFromStore);

        public PayByRefViewModel PayByRef { get; set; }
        public MBWayViewModel MBWay { get; set; }

        public SteedFromStoreViewModel SteedFromStore { get; set; }

        public TvRoyaltyFormViewModel TvRoyaltyFormViewModel { get; set; }

        public BankTransfertMethodViewModel BankTransfer { get; set; }

        public bool HasOnlyFnacCom
        {
            get
            {
                return _references.ToList().All(r => r.IsFnac);
            }
        }

        public bool IsUncertainOrder { get; set; }

        public bool DisplayDeliveryDates { get; set; }

        public void WithDetail(OrderDetailViewModel detail, bool isFnac)
        {
            if (!isFnac || (!_orderDetails.Any(od => od.IsFnac)))
            {
                _orderDetails.Add(detail);

                detail.Index = _orderDetails.Count;

                foreach (var orderDetails in _orderDetails)
                {
                    orderDetails.Total = _orderDetails.Count;
                }
            }
            else
            {
                var orderDetailFnac = _orderDetails.FirstOrDefault(od => od.IsFnac);
                if (orderDetailFnac != null)
                {
                    orderDetailFnac.References.AddRange(detail.References);
                    orderDetailFnac.AddRangeArticle(detail.Articles.ToList());
                    if (orderDetailFnac.Address == null)
                    {
                        orderDetailFnac.Address = detail.Address;
                        orderDetailFnac.ShippingMode = detail.ShippingMode;
                    }
                }
            }
        }
        public IReadOnlyCollection<OrderDetailViewModel> OrderDetails => _orderDetails.AsReadOnly();

        public decimal? TotalPrice { get; set; }

        public DonationViewModel Donation { get; set; }

        public void WithDonation(DonationViewModel donation)
        {
            _content |= OrderThankYouViewModelContent.Donation;
            Donation = donation;
        }
        public bool HasDonation => _content.HasFlag(OrderThankYouViewModelContent.Donation);

        public bool HasAdvertising { get; set; }


        public DeezerViewModel DeezerViewModel { get; set; }
        public void WithDeezer(bool withFnacPlus)
        {
            _content = _content | OrderThankYouViewModelContent.Deezer;
            DeezerViewModel = new DeezerViewModel()
            {
                WithFnacPlus = withFnacPlus
            };
        }
        public bool HasDeezer => _content.HasFlag(OrderThankYouViewModelContent.Deezer);

        //FIB Billing Method
        public FibBillingMethodViewModel FibBillingMethodViewModel { get; set; }

        public void WithFib(string priceFormat, string userReference, IEnumerable<string> logisticNumbers)
        {
            _content = _content | OrderThankYouViewModelContent.Fib;
            FibBillingMethodViewModel = new FibBillingMethodViewModel()
            {
                PriceFormat = priceFormat,
                OrderUserReference = userReference,
                LogisticNumbers = string.Join("|", logisticNumbers)

            };
        }
        public bool HasFib => _content.HasFlag(OrderThankYouViewModelContent.Fib);

        public bool HasIdentityClash { get; set; }

        //En cas de Kobo + Compte Social Connect pur
        public bool NeedPassword { get; set; }

        public GuestInfosViewModel GuestInfosViewModel { get; set; }
    }
}
