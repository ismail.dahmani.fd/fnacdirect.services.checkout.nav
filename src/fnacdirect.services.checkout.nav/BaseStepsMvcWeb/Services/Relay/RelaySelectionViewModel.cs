﻿namespace FnacDirect.OrderPipe.BaseMvc.Web.Pop.ApiControllers.Models
{
    public class RelaySelectionViewModel
    {
        public int RelayId { get; set; }
    }
}
