﻿
using System.Collections.Generic;
using System.Xml.Serialization;

namespace FnacDirect.OrderPipe.Base.Business.OrderDetail
{
    public class Article : BaseArticle
    {           
        public string SerieTitle { get; set; }        

        public bool IsPreOrder { get; set; }

        public bool DigitalLockerMatched { get; set; }                                         

        public string FormatLabel { get; set; }

        public bool IsNumerical { get; set; }             

        public List<Service> Services { get; set; }

        public List<Participant> Participants { get; set; }

        public int? TypeId { get; set; }

        public string TypeLabel { get; set; }

        public int? ShippingMethodId { get; set; }

        public bool HasUpcDeezerReference { get; set; }
    }
}
