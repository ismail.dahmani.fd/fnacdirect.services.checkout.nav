using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Common.Payment
{
    public class VirtualGiftCheckViewModel : BillingMethodViewModel
    {
        public bool Active { get; set; }
        public bool HasAccountBalance { get; set; }
        public string AccountBalance { get; set; }
        public string UsableAmount { get; set; }
        public bool HasRemainingBalance { get; set; }
        public string RemainingBalance { get; set; }

        public bool HasMaximumAllowedAmountForClickAndCollect { get; set; } = false;
        public decimal? MaximumAllowedAmountForClickAndCollect { get; set; }
    }
}
