﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.OnePage.Omniture
{
    public class OmnitureCommonViewModel
    {
        public string Prefix { get; set; }

        public string Suffix { get; set; }

        public string Products { get; set; }

        public OmnitureFnacPlusViewModel FnacPlus { get; set; }
    }
}
