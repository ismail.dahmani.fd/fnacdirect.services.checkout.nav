using FnacDirect.Membership.Model.Constants;
using System;
using System.Xml.Serialization;

namespace FnacDirect.OrderPipe.Base.Model
{
    public class FidelityBillingMethod : BillingMethod
    {
        [XmlIgnore]
        private int? _TransactionId;

        public int? TransactionId
        {
            get { return _TransactionId; }
            set { _TransactionId = value; }
        }
        
        [XmlIgnore] 
        public AdvantageCodeType AdvantageCodeType { get; set; }

        [XmlIgnore]
        private long _PointsQuantity;

        public long PointsQuantity
        {
            get { return _PointsQuantity; }
            set { _PointsQuantity = value; }
        }

        [XmlIgnore]
        private bool? _Valid;

        public bool? Valid
        {
            get { return _Valid; }
            set { _Valid = value; }
        }

        [XmlIgnore]
        private string _ContractId;

        public string ContractId
        {
            get { return _ContractId; }
            set { _ContractId = value; }
        }

        [XmlIgnore]
        private DateTime _BirthDate;

        public DateTime BirthDate
        {
            get { return _BirthDate; }
            set { _BirthDate = value; }
        }

        [XmlIgnore]
        private int? _ErrorCode;

        public int? ErrorCode
        {
            get { return _ErrorCode; }
            set { _ErrorCode = value; }
        }
    }
}