
namespace FnacDirect.OrderPipe.Config
{
	public class EndTransactionStep : Core.Model.Config.BaseStep
    {
        public EndTransactionStep()
        {
            Class = "FnacDirect.OrderPipe.EndTransactionStep";
        }
    }
}
