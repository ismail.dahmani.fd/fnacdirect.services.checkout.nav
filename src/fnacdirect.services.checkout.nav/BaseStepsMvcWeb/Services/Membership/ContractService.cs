﻿using System;
using System.Collections.Generic;
using System.Linq;
using FnacDirect.Membership.Business;
using FnacDirect.Membership.Model.Entities;

namespace FnacDirect.OrderPipe.Base.Proxy.Membership
{
    public class ContractService : IContractService
    {
        private readonly ContractBusiness _contractBusiness;

        public ContractService(ContractBusiness contractBusiness)
        {
            _contractBusiness = contractBusiness;
        }

        public IEnumerable<Contract> GetContractsByAdherentNumber(string adherentNumber, DateTime birthDate)
        {
            return _contractBusiness.GetContractsByAdherentNumber(adherentNumber, birthDate);
        }

        public Contract GetGamingContract(IEnumerable<Contract> contracts)
        {
            return _contractBusiness.getGamingContract(contracts.ToList());
        }

        public Contract GetMembershipContract(IEnumerable<Contract> contracts)
        {
            return _contractBusiness.getMembershipContract(contracts.ToList());
        }
    }
}
