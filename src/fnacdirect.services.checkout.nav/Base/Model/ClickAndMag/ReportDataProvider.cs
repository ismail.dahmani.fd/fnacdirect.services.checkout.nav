﻿using FnacDirect.OrderPipe.FDV.Model;
using System.Collections.Generic;
namespace FnacDirect.OrderPipe.CM.Business
{
   public class ReportDataProvider
    {


        public static PropCommForm GetReportForm(PropCommReportData reportData)
        {
            return reportData.ReportForm;
        }

        public static List<PropCommArticle> GetReportLines(PropCommReportData reportData)
        {
            return reportData.PropCommArticles;
        }

    }
}
