﻿using FnacDirect.OrderPipe.Base.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FnacDirect.OrderPipe.Base.Business.ArticleConversions.ShoppingCartToOrderForm
{
    public interface IShoppingCartToOrderFormConverter : IArticleConverter<ShoppingCartEntities.Article, Article>
    {
        ArticleList ConvertArticles(ShoppingCartEntities.Articles list, bool pWithStockCheck = false, string cartId = null);
        Article Convert(ShoppingCartEntities.Article a, bool withStockCheck);
    }
}
