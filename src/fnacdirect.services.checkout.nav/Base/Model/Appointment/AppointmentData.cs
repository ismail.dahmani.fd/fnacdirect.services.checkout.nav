using System.Collections.Generic;
using AM = FnacDirect.Appointment.Model;
namespace FnacDirect.OrderPipe.Base.BaseModel.Model
{
    public class AppointmentData : GroupData
    {
        public AppointmentData()
        {
            Appointments = new AppointmentList();
        }

        public AppointmentList Appointments { get; set; }

        public bool? HasAppointmentArticleType { get; set;}
    }

    public class AppointmentList : List<AM.Appointment>
    {        
    }
}
