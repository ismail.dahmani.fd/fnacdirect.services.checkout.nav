﻿using System;
using System.Text;
using System.Reflection;
using System.Xml;

namespace FnacDirect.OrderPipe.Core.Utils
{
    [AttributeUsage(AttributeTargets.All, Inherited = true, AllowMultiple = false)]
    public sealed class DontDumpAttribute : Attribute
    {
        // This is a positional argument.
        public DontDumpAttribute()
        {
        }
    }


	/// <summary>
	/// Summary description for Dumper.
	/// </summary>
	public class Dumper
	{
		protected Dumper()
		{
		}

		public static string DumpObject(object obj) 
		{
			return DumpObject(obj, 15);
		}

		public static string DumpObject(object obj, int maxLevel) 
		{
			StringBuilder sb;

			sb = new StringBuilder(10000);
			if (obj == null)
				return "null";
			else 
			{
                PrivDump(sb, obj, "[ObjectToDump]", 0, maxLevel);
				return sb.ToString();
			}
		}

		public static object GetFieldValue(object obj, string fieldName) 
		{
            if (obj == null)
                return null;

			FieldInfo fi;
			Type t;

			t = obj.GetType();
			fi = t.GetField(fieldName, BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance);
			if (fi == null)
				return null;
			else
				return fi.GetValue(obj);
		}

		protected static void PrivDump(StringBuilder sb, object obj, string objName, int level, int maxLevel) 
		{
			if (sb == null || obj == null)
				return;

			if (maxLevel >= 0 && level >= maxLevel)
				return;

			var padstr = new StringBuilder();

            for (var i=0; i < level; i++)
				if (i<level-1)
					padstr.Append(" | ");
				else
					padstr.Append(" + ");

            string str;
			string[] strarr;
			Type t;
			t = obj.GetType();
			strarr = new string[7];
			strarr[0] = padstr.ToString();
			strarr[1] = objName;
			strarr[2] = " AS ";
			strarr[3] = t.FullName;
			strarr[4] = " = ";
			strarr[5] = obj.ToString();
			strarr[6] = "\r\n";
			sb.Append(string.Concat(strarr));
			if (obj.GetType().BaseType == typeof(ValueType))
				return;
            if (obj is string)
                return;
            if (obj is XmlNode)
                return;
            if (obj.GetType().GetCustomAttributes(typeof(DontDumpAttribute),true).Length!=0)
                return;
            DumpType(sb, obj, level, t, maxLevel);
			Type bt;
			bt = t.BaseType;
			if (bt != null) 
			{
				while (!(bt == typeof(object)))
				{
                    level++;
                    padstr.Insert(0, " | ");
					str = bt.FullName;
                    if (str != "System.Array")
                    {
                        sb.AppendFormat("{0}({1})\r\n", padstr, str);
                        DumpType(sb, obj, level, bt, maxLevel);
                    }
                    bt = bt.BaseType;
                    if (bt != null)
                        continue;
					break;
				}
			}
		}

		protected static void DumpType(StringBuilder sb, object obj , int level, Type t, int maxlevel)
		{
            if (sb == null || t == null || level == int.MaxValue)
                return;

			FieldInfo[] fi;
			fi = t.GetFields(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance);
			if (t == typeof(Delegate)) return;
            foreach (var f in fi)
            {
                if (f.GetCustomAttributes(typeof(DontDumpAttribute), true).Length == 0)
                    PrivDump(sb, f.GetValue(obj), f.Name, level + 1, maxlevel);
            }
			object[] arl;
			int i;
			if (obj is Array)
			{
				try
				{
					arl = (object[])obj;
					for (i = 0;i<arl.GetLength(0);i++)
						PrivDump(sb, arl[i], "[" + i + "]", level + 1, maxlevel);
				}
				catch (Exception)
                {
                    // we don't care we want what we can have
                }
			}
		}
	}
}
