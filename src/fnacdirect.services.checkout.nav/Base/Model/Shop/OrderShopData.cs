﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FnacDirect.OrderPipe.Base.BaseModel.Model
{
	public class OrderShopData : GroupData
	{
		public bool HasData
		{
			get
			{
				return OrderShop != null;
			}
		}
		public OrderShop OrderShop { get; set; }
	}
}
