using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.OnePage;
using System;
using System.Web.Routing;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Pop.Models.OnePage
{
    public interface IOnePageRootViewModelBuilder
    {
        RootViewModel GetRootViewModel(OPContext opContext, PopOrderForm popOrderForm, Func<string, RouteValueDictionary, string> getUrl);
    }
}
