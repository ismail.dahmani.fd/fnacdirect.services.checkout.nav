using System.Collections.Generic;

namespace FnacDirect.OrderPipe
{
    using System.IO;
    using System.Runtime.Serialization.Formatters.Binary;

    public static class ObjectUtils
    {
        public static T DeepClone<T>(T obj) where T : class
        {
            T objResult;
            using (var ms = new MemoryStream())
            {
                var bf = new BinaryFormatter();
                bf.Serialize(ms, obj);
                ms.Position = 0;
                objResult = bf.Deserialize(ms) as T;
            }
            return objResult;
        }

        public static IEnumerable<T> Yield<T>(this T obj)
        {
            yield return obj;
        }
    }
}
