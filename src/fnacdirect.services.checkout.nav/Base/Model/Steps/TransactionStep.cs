using X=System.Xml;
using System.Transactions;

namespace FnacDirect.OrderPipe
{

    public class DummyTransactionManager : ITransactionManager
    {
        public IsolationLevel IsolationLevel { get; set; }

        public DummyTransactionManager()
        {
            IsolationLevel = IsolationLevel.Serializable;
        }
        public void Begin(OPContext context)
        {
            context.Log("BeginTransaction");
            if (context.IsInTransaction)
            {
                throw OPException.BuildException("Transaction already started", context, null, OPRange.Transaction, 1);
            }
            context.IsInTransaction = true;
        }

        public void Commit(OPContext context)
        {
            context.Log("CommitTransaction");
            if (!context.IsInTransaction)
            {
                throw OPException.BuildException("Transaction is not started", context, null, OPRange.Transaction, 3);
            }
            context.IsInTransaction = false;
        }

        public bool Dispose(OPContext context)
        {
            var ret = false;
            if (context.IsInTransaction)
            {
                context.IsInTransaction = false;
            }
            return ret;
        }
    }

    public class TransactionManager : ITransactionManager
    {
        public IsolationLevel IsolationLevel { get; set; }

        public TransactionManager() 
        {
            IsolationLevel = IsolationLevel.Serializable;
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope")]
        public void Begin(OPContext context)
        {
            context.Log("BeginTransaction");
            if (context.IsInTransaction)
            {
                throw OPException.BuildException("Transaction already started",context,null,OPRange.Transaction, 1);
            }

            if (Transaction.Current != null)
            {
                throw OPException.BuildException("Ambient transaction already started", context, null, OPRange.Transaction, 2);
            }

            var transaction = new CommittableTransaction(new TransactionOptions() { IsolationLevel = IsolationLevel });

            transaction.TransactionCompleted += new TransactionCompletedEventHandler(delegate(object sender, TransactionEventArgs e)
            {
                context.Log("Transaction completed : " + e.Transaction.TransactionInformation.Status);
            });

            Transaction.Current = transaction;
            context.IsInTransaction = true;
        }

        public void Commit(OPContext context)
        {
            context.Log("CommitTransaction");
            if (!context.IsInTransaction)
            {
                throw OPException.BuildException("Transaction is not started", context, null, OPRange.Transaction, 3);
            }

            if (Transaction.Current == null)
            {
                throw OPException.BuildException("Ambient transaction is not started", context, null, OPRange.Transaction, 4);
            }

            var transaction = Transaction.Current as CommittableTransaction;

            if (transaction == null)
            {
                throw OPException.BuildException("Ambient transaction is not committable", context, null, OPRange.Transaction, 5);
            }

            transaction.Commit();

            Transaction.Current = null;
            context.IsInTransaction = false;
        }

        public bool Dispose(OPContext context)
        {
            var ret = false;
            if (context.IsInTransaction)
            {
                context.Log("RollbackTransaction");
                if (Transaction.Current == null)
                {
                    throw OPException.BuildException("Ambient transaction is not started", context, null, OPRange.Transaction, 6);
                }

                var transaction = Transaction.Current as CommittableTransaction;

                if (transaction == null)
                {
                    throw OPException.BuildException("Ambient transaction is not committable", context, null, OPRange.Transaction, 7);
                }

                if (transaction.TransactionInformation.Status == TransactionStatus.Active)
                {
                    transaction.Rollback();
                    ret = true;
                }

                Transaction.Current = null;
                context.IsInTransaction = false;
            }
            return ret;
        }
    }

    public abstract class TransactionStep : BaseStep
    {
        public ITransactionManager TransactionManager { get; set; }
    }

    public class BeginTransactionStep : TransactionStep
    {
        public override bool Run(OPContext context, PipeResult res, bool force)
        {
            TransactionManager.Begin(context);
            res.LastStepReturn = StepReturn.Next;
            return false;
        }
    }

    public class EndTransactionStep : TransactionStep
    {
        public override bool Run(OPContext context, PipeResult res, bool force)
        {
            TransactionManager.Commit(context);
            res.LastStepReturn = StepReturn.Next;
            return false;
        }
    }

}
