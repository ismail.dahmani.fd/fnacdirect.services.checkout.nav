﻿using FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Common.Basket;
using System;
using System.Collections.Generic;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Payment
{
    public class BasketSellerViewModel
    {
        public BasketSellerViewModel()
        {
            Shipping = Maybe.Empty<IShippingViewModel>();
        }

        public SellerInformationsViewModel Informations { get; set; }

        public Maybe<IShippingViewModel> Shipping { get; set; }

        private readonly List<BasketItemViewModel> _items
            = new List<BasketItemViewModel>();

        public IReadOnlyCollection<BasketItemViewModel> Items
        {
            get
            {
                return _items;
            }
        }

        public BasketSellerViewModel WithItem(params BasketItemViewModel[] basketItemViewModels)
        {
            _items.AddRange(basketItemViewModels);

            return this;
        }
    }
}
