using FnacDirect.IdentityImpersonation;
using FnacDirect.OrderPipe.Base.BaseModel;
using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.OrderPipe.Base.Model.LineGroups;
using FnacDirect.Technical.Framework;
using System;
using System.Collections.Generic;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Common.Shipping.Shop
{
    public interface IShopShippingNetworkViewModelBuilder : IShippingNetworkViewModelBuilder<ShopShippingNetworkViewModel>
    {
        string ComputeShippingCost(decimal shippingCost);

        ShopDetailsViewModel BuildDetails(FnacStore.Model.FnacStore fnacStore, decimal shippingCost, int? favoriteStoreId);

        ShopDetailsViewModel BuildDetails(FnacStore.Model.FnacStore fnacStore, decimal shippingCost, int? favoriteStoreId, Position referenceGeoPoint);

        List<TimetableViewModel> GetTimeTables(int storeId);

        IEnumerable<ExceptionnalClosingViewModel> GetExceptionnalTimetables(int storeId);

        void ApplyAvailabilities(PopOrderForm popOrderForm,ShopViewModel shopViewModel, ShopShippingMethodEvaluationItem shopShippingMethodEvaluationItem, IEnumerable<ILineGroup> lineGroups, IEnumerable<int> excludedLogisticTypes);

        DateTime? ComputeWorstDeliveryDate(int shopId, ShopShippingMethodEvaluationItem shopShippingMethodEvaluationItem, IEnumerable<ILineGroup> lineGroups, IEnumerable<int> excludedLogisticTypes, IdentityImpersonator identityImpersonator);

        ShopShippingNetworkViewModel Build(PopOrderForm popOrderForm, int sellerId, Position referenceGeoPoint);
    }
}
