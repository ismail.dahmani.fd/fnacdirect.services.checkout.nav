using FnacDirect.OrderPipe.Base.BaseModel;
using FnacDirect.OrderPipe.Base.BaseModel.Model;
using FnacDirect.OrderPipe.Base.Business.OnePage;
using FnacDirect.OrderPipe.Base.Business.Payment;
using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.OrderPipe.Base.Model.Adherent;
using FnacDirect.OrderPipe.Base.Model.Facets;
using FnacDirect.OrderPipe.Base.Model.Pop;
using FnacDirect.OrderPipe.Base.OneClick;
using FnacDirect.OrderPipe.Base.Proxy.Membership;
using FnacDirect.OrderPipe.Base.Proxy.Nav;
using FnacDirect.OrderPipe.Base.Proxy.Wishlist;
using FnacDirect.OrderPipe.BaseMvc.Web.Models;
using FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Basket;
using FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Common.Basket;
using FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Common.Payment;
using FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.DataLayer;
using FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Membership;
using FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Omniture;
using FnacDirect.OrderPipe.BaseMvc.Web.Pop.Models.Common;
using FnacDirect.OrderPipe.BaseMvc.Web.Services.Pop.DataLayer;
using FnacDirect.Technical.Framework.Utils;
using FnacDirect.Technical.Framework.Web;
using FnacDirect.Technical.Framework.Web.Mvc;
using System;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Controllers.Pop
{
    public class PopBasketController : PopBaseController
    {
        private readonly OPContext _opContext;
        private readonly IApplicationContext _applicationContext;
        private readonly IBasketViewModelBuilder _basketViewModelBuilder;
        private readonly ISummaryViewModelBuilder _paymentSummaryViewModelBuilder;
        private readonly IMembershipViewModelBuilder _membershipViewModelBuilder;
        private readonly IUserExtendedContextProvider _userExtendedContextProvider;
        private readonly IOptionsViewModelBuilder _optionsViewModelBuilder;
        private readonly IExpressPlusViewModelBuilder _expressPlusViewModelBuilder;
        private readonly ISwitchProvider _switchProvider;
        private readonly IDonationViewModelBuilder _donationViewModelBuilder;
        private readonly IOmnitureViewModelBuilder _omnitureViewModelBuilder;
        private readonly IPipeMessageViewModelBuilder _pipeMessageViewModelBuilder;
        private readonly IKoboOrangeService _koboOrangeService;
        private readonly IPopBaseRootViewModelBuilder<RootViewModel> _popBaseRootViewModelBuilder;
        private readonly IBillingMethodService _billingMethodService;
        private readonly INavigationViewModelBuilder _navigationViewModelBuilder;
        private readonly CultureInfo _currentCulture;

        private readonly PopOrderForm _popOrderForm;
        private readonly AboIlliData _aboIlliData;
        private readonly IWishlistService _wishlistService;
        private readonly IOnePageService _onepageService;
        private readonly IDataLayerService _dataLayerService;

        public PopBasketController(OPContext opContext,
                                   IBasketViewModelBuilder basketViewModelBuilder,
                                   ISummaryViewModelBuilder summaryViewModelBuilder,
                                   IMembershipViewModelBuilder membershipViewModelBuilder,
                                   IUserExtendedContextProvider userExtendedContextProvider,
                                   IOptionsViewModelBuilder optionsViewModelBuilder,
                                   IExpressPlusViewModelBuilder expressPlusViewModelBuilder,
                                   IMembershipConfigurationService membershipConfigurationService,
                                   ISwitchProvider switchProvider,
                                   IApplicationContext applicationContext,
                                   IDonationViewModelBuilder donationViewModelBuilder,
                                   IOmnitureViewModelBuilder omnitureViewModelBuilder,
                                   IPipeMessageViewModelBuilder pipeMessageViewModelBuilder,
                                   IKoboOrangeService koboOrangeService,
                                   IPopBaseRootViewModelBuilder<RootViewModel> popBaseRootViewModelBuilder,
                                   IBillingMethodService billingMethodService,
                                   IOnePageService onePageService,
                                   IWishlistService wishlistService,
                                   INavigationViewModelBuilder navigationViewModelBuilder,
                                   IDataLayerService dataLayerService
                                   )
        {
            _opContext = opContext;
            _applicationContext = applicationContext;
            _basketViewModelBuilder = basketViewModelBuilder;
            _paymentSummaryViewModelBuilder = summaryViewModelBuilder;
            _membershipViewModelBuilder = membershipViewModelBuilder;
            _userExtendedContextProvider = userExtendedContextProvider;
            _optionsViewModelBuilder = optionsViewModelBuilder;
            _expressPlusViewModelBuilder = expressPlusViewModelBuilder;
            _switchProvider = switchProvider;
            _donationViewModelBuilder = donationViewModelBuilder;
            _omnitureViewModelBuilder = omnitureViewModelBuilder;
            _pipeMessageViewModelBuilder = pipeMessageViewModelBuilder;
            _koboOrangeService = koboOrangeService;
            _popBaseRootViewModelBuilder = popBaseRootViewModelBuilder;
            _currentCulture = CultureInfo.GetCultureInfo(applicationContext.GetLocale());
            _billingMethodService = billingMethodService;
            _popOrderForm = _opContext.OF as PopOrderForm;
            _aboIlliData = _popOrderForm.GetPrivateData<AboIlliData>("AboIlliGroup", true);
            _wishlistService = wishlistService;
            _onepageService = onePageService;
            _navigationViewModelBuilder = navigationViewModelBuilder;
            _dataLayerService = dataLayerService;
        }

        [HttpGet]
        public async Task<ViewResult> Index()
        {
            var basketViewModel = GetBasketViewModel();

            var paymentSummaryViewModel = GetPaymentViewModel();

            var loginPopinViewModel = GetLoginPopinViewModel();

            _popOrderForm.UserContextInformations.IsGuest = false;

            SetPaymentIsExpressPlus(basketViewModel, paymentSummaryViewModel);

            // Si le nombre d'articles indispos a augmenté,
            // afficher la popin expliquant au client que certains de ces articles ne sont plus disponibles
            var popData = _popOrderForm.GetPrivateData<PopData>();
            var showUnavailabilityPopin = _popOrderForm.DeletedArticles.Count() > popData.OldDeletedQuantity;

            var showOrangeBanner = IsOrangeBannerEligible();

            var rootViewModel = _popBaseRootViewModelBuilder.Build((a, b) => Url.RouteUrl(a, b));

            rootViewModel.PageName = ApiControllers.Pop.PopApiEnvironments.BasketPage;
            rootViewModel.Basket = basketViewModel;
            rootViewModel.PaymentSummary = paymentSummaryViewModel;
            rootViewModel.Options = _optionsViewModelBuilder.Build(_popOrderForm);
            rootViewModel.Membership = _membershipViewModelBuilder.Build(_popOrderForm);
            rootViewModel.AdjustedMembershipCard = _membershipViewModelBuilder.BuildAdjustedCard(_popOrderForm);
            rootViewModel.Login = loginPopinViewModel;
            rootViewModel.Donations = _donationViewModelBuilder.Build(_popOrderForm, GetOpContext());
            rootViewModel.ShowUnavailabilityPopin = showUnavailabilityPopin;
            rootViewModel.PipeMessages = _pipeMessageViewModelBuilder.Build(_popOrderForm);
            rootViewModel.ShowOrangeBanner = showOrangeBanner;
            //surcharge de la navigation pour mettre la home en page de retour
            rootViewModel.Navigation = _navigationViewModelBuilder.BackToHome();

            var popMembershipData = _popOrderForm.GetPrivateData<PopMembershipData>(false);
            if (popMembershipData != null && popMembershipData.DisplayNotification && !popMembershipData.NotificationHasBeenDisplayed)
            {
                popMembershipData.NotificationHasBeenDisplayed = true;

                Rewind();
            }

            if (IsReactJSMode())
            {
                var basketAmount = rootViewModel.PaymentSummary.TotalGlobal;

                // TODO remove it after DataLayer is definitively used:
                rootViewModel.Omniture = _omnitureViewModelBuilder.BuildBasket(_popOrderForm);
                rootViewModel.Omniture.BasketAmount = basketAmount;
                rootViewModel.Tracking = _omnitureViewModelBuilder.BuildTracking(_popOrderForm, TrackedPage.popBasket, basketAmount);
                // TODO end

                var trackingModel = new DatalayerViewModel()
                {
                    PageType = PageTypeEnum.PopBasket,
                    PopOrderForm = _popOrderForm,
                };

                rootViewModel.WishlistAside = await _wishlistService.GetWishlistAside(Request.Headers["Cookie"]);

                var viewResult = View("_Poc/Index", rootViewModel);
                //Datalayer and Tracking switchabled
                _dataLayerService.ComputeShoppingCart(trackingModel, viewResult as ViewResultBase, ControllerContext);

                return viewResult;
            }

            return View(rootViewModel);
        }

        private bool IsOrangeBannerEligible()
        {
            var showOrangeBanner = false;
            var iGotBillingInformations = (IGotBillingInformations)_opContext.OF;
            var iGotLineGroups = (IGotLineGroups)_opContext.OF;
            var iGotShippingMethodEvaluation = (IGotShippingMethodEvaluation)_opContext.OF;

            if (_billingMethodService.IsOrangeAllowed(iGotBillingInformations, iGotLineGroups, iGotShippingMethodEvaluation))
            {
                showOrangeBanner = _koboOrangeService.IsValidOrangeRequest();
            }
            return showOrangeBanner;
        }

        private Maybe<BasketViewModel> GetBasketViewModel()
        {
            var basketViewModel = _basketViewModelBuilder.Build(_popOrderForm);

            if (basketViewModel.HasValue)
            {
                var showExpressPlus = basketViewModel.HasValue
                    && !_aboIlliData.HasAboIlliInBasket
                    && !_aboIlliData.IsAbonne
                    && _aboIlliData.IsBasketElligibleForAboIlli;

                basketViewModel.Value.ExpressPlusInfo = showExpressPlus ? _expressPlusViewModelBuilder.Build(_aboIlliData) : null;
            }

            return basketViewModel;
        }

        private PaymentSummaryViewModel GetPaymentViewModel()
        {
            var paymentSummaryViewModel = _paymentSummaryViewModelBuilder.Build(_opContext, _popOrderForm);

            paymentSummaryViewModel.IsRelais = _popOrderForm.ShippingMethodEvaluation.ShippingMethodEvaluationGroups
                .All(smeg => smeg.SelectedShippingMethodEvaluationType == ShippingMethodEvaluationType.Relay);
            paymentSummaryViewModel.IsShop = _popOrderForm.ShippingMethodEvaluation.ShippingMethodEvaluationGroups
                .All(smeg => smeg.SelectedShippingMethodEvaluationType == ShippingMethodEvaluationType.Shop);

            return paymentSummaryViewModel;
        }

        private LoginPopinViewModel GetLoginPopinViewModel()
        {
            var userExtendedContext = _userExtendedContextProvider.GetCurrentUserExtendedContext();
            var loginPopinViewModel = new LoginPopinViewModel
            {
                ContainsRegistrationForm = true,
                ShowPopin = !_switchProvider.IsEnabled("identityserver") && !userExtendedContext.Visitor.IsAuthenticated,
                IsLogged = userExtendedContext.Visitor.IsAuthenticated
            };

            // This specific level is the minimal level used for mobile session when "remember me" optin was unchecked
            // Non oneclick user should be prompted with the login popin
            if (userExtendedContext.Visitor.IsAuthenticated && userExtendedContext.Visitor.Session.IsPreciseLevel(Customer.Model.Constants.SessionLevel.OneClickMobileOptinUnchecked))
            {
                var oneClickData = _popOrderForm.GetPrivateData<OneClickData>(create: false);

                if (oneClickData == null
                    || !oneClickData.IsAvailable.HasValue
                    || !oneClickData.IsAvailable.Value)
                {
                    loginPopinViewModel.ShowPopin = !_switchProvider.IsEnabled("identityserver");
                }
            }

            return loginPopinViewModel;
        }

        private void SetPaymentIsExpressPlus(Maybe<BasketViewModel> basketViewModel, PaymentSummaryViewModel paymentSummaryViewModel)
        {
            if ((_aboIlliData.HasAboIlliInBasket || _aboIlliData.IsAbonne)
                            && basketViewModel.HasValue && basketViewModel.Value.Sellers
                            .Where(s => s.Informations.IsFnac)
                            .Any(s => s.Items.Any(i => i.Availability is ShopAvailabilityViewModel))
                            && paymentSummaryViewModel.IsExpressPlus)
            {
                paymentSummaryViewModel.IsExpressPlus = false;
            }
        }

        private bool IsReactJSMode()
        {
            return _switchProvider.IsEnabled("orderpipe.pop.reactjs.basket");
        }

        [HttpPost]
        [ActionName("Index")]
        public ActionResult Validate(string paymentABTest = "", bool continueWithoutAccount = false)
        {
            var userExtendedContext = _userExtendedContextProvider.GetCurrentUserExtendedContext();

            if (continueWithoutAccount && (userExtendedContext.Visitor != null && !userExtendedContext.Visitor.IsAuthenticated))
            {
                _popOrderForm.UserContextInformations.IsGuest = true;
                _popOrderForm.ShippingMethodEvaluation.Reset();
                _popOrderForm.BillingAddress = null;

                Helper.SetCookie(ParametersName.UID,
                                string.Empty,
                                DateTime.Now.AddDays(-1),
                                true,
                                forceSet: true);
            }

            var popData = _popOrderForm.GetPrivateData<PopData>();
            popData.PaymentABTest = paymentABTest;
            popData.DisplayBasketValidated = true;
            popData.HasLimitedQuantity = false;

            var root = new RootViewModel();
            popData.AccountHasJusteBenCreated = TryUpdateModel(root) && root.AccountHasBeenCreated;

            var oneClickData = _popOrderForm.GetPrivateData<OneClickData>(create: false);
            if (oneClickData?.IsAvailable != null && oneClickData.IsAvailable.Value)
            {
                oneClickData.IsAvailable = false;
            }

            var siteId = _applicationContext.GetSiteId();
            _popOrderForm.Path = _onepageService.GetPath(_popOrderForm, popData, siteId);

            var canalPlayNodeId = int.Parse(_applicationContext.GetAppSetting("CanalPlayNodeId"));
            var standardArticles = _popOrderForm.LineGroups.SelectMany(l => l.Articles).OfType<StandardArticle>().ToList();
            var typesId = RangeSet.Parse<int>(_applicationContext.GetAppSetting("CanalPlayTypeIds"));

            if (standardArticles.Any(a => a.TreeNodes.Any(t => t.Id == canalPlayNodeId) && a.TypeId.HasValue && typesId.Contains(a.TypeId.Value)) && _popOrderForm.GlobalPrices.TotalPriceEur == 0)
            {
                _popOrderForm.AddInfoMessage("canalplay", "orderpipe.basket.canalplay.blockfree");

                return Index().Result;
            }

            return Continue();
        }
    }
}
