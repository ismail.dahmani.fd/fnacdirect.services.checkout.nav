using FnacDirect.OrderPipe.Base.Business.Shipping.Postal;
using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.Technical.Framework.Web.Mvc;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Common.Shipping
{
    public static class AddressElegibilityExtensions
    {
        public static EligibleAddressViewModel ConvertToElegibleAddressViewModel(this AddressElegibility addressEligibility, int countryId, IApplicationContext applicationContext = null)
        {
            var addressViewModel = new EligibleAddressViewModel()
            {
                IsEligible = addressEligibility.IsElegible,
                HasMissingInformations = addressEligibility.HasMissingInformations,

                Id = addressEligibility.AddressEntity.Id,
                Reference = addressEligibility.AddressEntity.AddressBookReference
            };
            if (string.IsNullOrEmpty(addressEligibility.AddressEntity.AliasName))
            {
                addressViewModel.Alias = $"{addressEligibility.AddressEntity.FirstName} {addressEligibility.AddressEntity.LastName} {addressEligibility.AddressEntity.City}";
            }
            else
            {
                addressViewModel.Alias = addressEligibility.AddressEntity.AliasName;
            }

            addressViewModel.ShouldDisplayCompany = !string.IsNullOrEmpty(addressEligibility.AddressEntity.Company);
            addressViewModel.CompanyName = addressEligibility.AddressEntity.Company;

            addressViewModel.AddressLine1 = addressEligibility.AddressEntity.AddressLine1;

            addressViewModel.ShouldDisplayAddressLine2 = !string.IsNullOrEmpty(addressEligibility.AddressEntity.AddressLine2);
            addressViewModel.AddressLine2 = addressEligibility.AddressEntity.AddressLine2;

            addressViewModel.ShouldDisplayAddressLine3 = !string.IsNullOrEmpty(addressEligibility.AddressEntity.AddressLine3);
            addressViewModel.AddressLine3 = addressEligibility.AddressEntity.AddressLine3;

            addressViewModel.City = addressEligibility.AddressEntity.City;
            addressViewModel.ZipCode = addressEligibility.AddressEntity.Zipcode;
            addressViewModel.GenderId = addressEligibility.AddressEntity.GenderId;
            addressViewModel.Lastname = addressEligibility.AddressEntity.LastName;
            addressViewModel.Firstname = addressEligibility.AddressEntity.FirstName;
            addressViewModel.ShouldDisplayCountry = countryId != addressEligibility.AddressEntity.CountryId;
            addressViewModel.Phone = addressEligibility.AddressEntity.Tel;
            addressViewModel.CellPhone = addressEligibility.AddressEntity.CellPhone;

            addressViewModel.IsPostal = addressEligibility.AddressEntity.Network == (int)Customer.Model.Constants.ShippingNetwork.Postal;
            addressViewModel.IsRelay = addressEligibility.AddressEntity.Network == (int)Customer.Model.Constants.ShippingNetwork.Relay;
            addressViewModel.IsStore = addressEligibility.AddressEntity.Network == (int)Customer.Model.Constants.ShippingNetwork.Store;

            addressViewModel.Country = addressEligibility.AddressEntity.CountryLabel;
            addressViewModel.CountryId = addressEligibility.AddressEntity.CountryId;

            addressViewModel.State = addressEligibility.AddressEntity.State;
            addressViewModel.TaxIdNumber = addressEligibility.AddressEntity.TaxIdNumber;
            addressViewModel.TaxNumberType = addressEligibility.AddressEntity.TaxIdNumberType;
            addressViewModel.DocumentCountryId = addressEligibility.AddressEntity.DocumentCountryId;
            addressViewModel.LegalStatus = addressEligibility.AddressEntity.LegalStatusId;


            if (addressViewModel.IsRelay)
            {
                if (addressEligibility.AddressEntity is RelayAddressEntity relayAddressEntity)
                {
                    addressViewModel.Timetables = relayAddressEntity.DaysOpen.ConvertToTimetableViewModel(applicationContext);
                }
            }
            else if (addressViewModel.IsStore)
            {
                if (addressEligibility.AddressEntity is StoreAddressEntity storeAddressEntity)
                {
                    addressViewModel.Timetables = storeAddressEntity.Timetables.ConvertToTimetableViewModel(applicationContext);
                    addressViewModel.ExceptionnalTimetables = storeAddressEntity.ExceptionnalTimetables.ConvertToTimetableExceptionalViewModel(applicationContext);
                }
            }

            return addressViewModel;
        }

        public static RelayShopEligibleAddressViewModel ConvertToShopAddressViewModel(this AddressElegibility addressEligibility, int countryId, IApplicationContext applicationContext = null)
        {
            var elegibleAddressViewModel = ConvertToElegibleAddressViewModel(addressEligibility, countryId, applicationContext);
            return new RelayShopEligibleAddressViewModel(elegibleAddressViewModel);
        }
    }
}
