using FnacDirect.Technical.Framework.Utilities;

namespace FnacDirect.OrderPipe
{
    public enum OFRoute
    {
        [StringValue("")]
        Unknown = -1,
        [StringValue("Parcours long")]
        Full = 0,
        [StringValue("One-page")]
        OnePage = 1,
        [StringValue("One-click")]
        OneClick = 2
    }

    public static class OFRouteExtensions
    {
        public static string GetName(this OFRoute oFRoute)
        {
            return StringEnum.GetStringValue(oFRoute);
        }
    }
}
