﻿namespace FnacDirect.OrderPipe.CM.Business
{
    public class GuptHelper
    {
        public static string FormatGUPTData(string prmData)
        {
            var szReturnValue = "";
            if(!string.IsNullOrEmpty(prmData))
                szReturnValue = prmData.ToLowerInvariant();
            szReturnValue = szReturnValue.Replace("â", "a");
            szReturnValue = szReturnValue.Replace("à", "a");
            szReturnValue = szReturnValue.Replace("ä", "a");
            szReturnValue = szReturnValue.Replace("ê", "e");
            szReturnValue = szReturnValue.Replace("é", "e");
            szReturnValue = szReturnValue.Replace("è", "e");
            szReturnValue = szReturnValue.Replace("ë", "e");
            szReturnValue = szReturnValue.Replace("î", "i");
            szReturnValue = szReturnValue.Replace("ï", "i");
            szReturnValue = szReturnValue.Replace("ô", "o");
            szReturnValue = szReturnValue.Replace("ö", "o");
            szReturnValue = szReturnValue.Replace("û", "u");
            szReturnValue = szReturnValue.Replace("ü", "u");
            szReturnValue = szReturnValue.Replace("ù", "u");
            szReturnValue = szReturnValue.Replace("ç", "c");
            szReturnValue = szReturnValue.Replace("ñ", "n");
            return szReturnValue.ToUpperInvariant();
        }
    }
}
