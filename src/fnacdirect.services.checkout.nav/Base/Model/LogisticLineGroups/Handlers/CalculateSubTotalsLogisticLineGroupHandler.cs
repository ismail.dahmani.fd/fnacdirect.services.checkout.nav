﻿using System;
using System.Collections.Generic;
using FnacDirect.OrderPipe.Base.LineGroups;
using FnacDirect.OrderPipe.Base.Model.Facets;
using FnacDirect.OrderPipe.Base.Business.LogisticLineGroups.Internals.Handlers;

namespace FnacDirect.OrderPipe.Base.Business.LogisticLineGroups.Handlers
{
    public class CalculateSubTotalsLogisticLineGroupHandler : ILogisticLineGroupGeneratedHandler, ILogisticLineGroupProcessCompletedHandler
    {
        private readonly ICalculateTotalsBusiness _calculateTotalsBusiness;

        public CalculateSubTotalsLogisticLineGroupHandler(ICalculateTotalsBusiness calculateTotalsBusiness)
        {
            _calculateTotalsBusiness = calculateTotalsBusiness;
        }
        
        public void OnGenerated(IEnumerable<ILineGroupHeader> source, IEnumerable<ILogisticLineGroup> logisticLineGroups)
        {
            foreach (var logisticLineGroup in logisticLineGroups)
            {
                _calculateTotalsBusiness.CalculateSubTotals(logisticLineGroup);
            }
        }

        public void OnGenerated(ILogisticLineGroupGenerationProcessContext logisticLineGroupGenerationProcessContext, IEnumerable<ILogisticLineGroup> logisticLineGroups)
        {
            foreach (var logisticLineGroup in logisticLineGroups)
            {
                _calculateTotalsBusiness.CalculateSubTotals(logisticLineGroup);
            }
        }
    }
}
