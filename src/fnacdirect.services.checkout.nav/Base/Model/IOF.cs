using FnacDirect.Contracts.Online.Model;
using System.Xml.Serialization;

namespace FnacDirect.OrderPipe
{
    public interface IOF : IPrivateDataContainer
    {
        /// <summary>
        /// Nom de l'étape courante
        /// </summary>
        string CurrentStep { get; set; }

        /// <summary>
        /// Identifiant de l'order form
        /// </summary>
        [XmlAttribute]
        string SID { get; set; }

        /// <summary>
        /// Liste de contraintes
        /// </summary>
        ConstraintList Constraints { get; set; }

        [XmlAttribute]
        string PipeName { get; set; }

        bool IsPro { get; set; }

        [XmlIgnore]
        UserMessages UserMessages { get; }

        /// <summary>
        /// Données privées (d'étape ou de groupe)
        /// </summary>
        [XmlArrayItem(typeof(Data))]
        DataList PrivateData { get; set; }
        
        /// <summary>
        /// Nettoyage de la structure avant sérialisation
        /// </summary>
        void CleanUp();

        IOF DeepCopy();

        PipeMessage AddErrorMessage(string categoryId, string ressourceId, params object[] parameters);
        PipeMessage AddWarningMessage(string categoryId, string ressourceId, params object[] parameters);
        PipeMessage AddInfoMessage(string categoryId, string ressourceId, params object[] parameters);
        PipeMessages PipeMessages { get; }
        ExecutionContext ExecutionContext { get; }

        bool IsCurrentlyRunningCriticalSection { get; set; }

        OFRoute GetRoute();
    }
}
