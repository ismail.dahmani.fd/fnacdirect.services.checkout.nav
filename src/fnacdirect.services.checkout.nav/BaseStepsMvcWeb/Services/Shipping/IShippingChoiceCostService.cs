using FnacDirect.OrderPipe.Base.Model.Solex;
using System.Collections.Generic;

namespace FnacDirect.OrderPipe.Base.Business.Shipping
{
    public interface IShippingChoiceCostService
    {
        decimal CalculateShippingCost(IEnumerable<ShippingChoice> shippingChoices, int? selectedShippingMethodId);
    }
}
