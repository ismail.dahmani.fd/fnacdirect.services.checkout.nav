using System;
using System.Collections.Generic;
using System.Xml.Serialization;
using FnacDirect.OrderPipe.Base.BaseModel.Model;
using FnacDirect.OrderPipe.Base.Model.OrderInformations;

namespace FnacDirect.OrderPipe.Base.Model
{
    public class OrderInfo : IOrderContextInformations, IOrderMobileInformations, IOrderGlobalInformations
    {
        [XmlIgnore]
        public string Affiliate { get; set; }

        [XmlIgnore]
        public bool OrderInSession { get; set; }

        [XmlIgnore]
        public bool IsFromCallCenter { get; set; }

        public bool HasOgoneBeenCalled { get; set; }
        

        // Order identification

        public int MainOrderPk { get; set; }

        public Guid MainOrderReference { get; set; }

        public string MainOrderUserReference { get; set; }

        public string OrderReference { get; set; }

        public OrderTransactionInfoList MainOrderTransactionInfos { get; set; }
        
        public DateTime OrderDate { get; set; }

        public string AdvantageCode { get; set; }

        /// <summary>
        /// Codes retour du panier issu du pricer (codes avantages par exemple)
        /// </summary>
        [XmlIgnore]
        public int MessageCodes { get; set; }

        public string OrderMessage { get; set; }

        public string Culture { get; set; }

        [XmlIgnore]
        public string Site { get; set; }

        [XmlIgnore]
        public string Catalogue { get; set; }

        // Technical information

        public string WebFarm { get; set; }

        public string IPAddress { get; set; }

        public bool OrderInserted { get; set; }

        public bool OrderCancelled { get; set; }

        public bool OgoneDirectLinkUnfinished { get; set; }

        [XmlIgnore]
        public bool OrderValidated { get; set; }

        public bool IsOrderWithAuthentication { get; set; }

        [XmlIgnore]
        public int? OrderType { get; set; }

        [XmlIgnore]
        public int? Origin { get; set; }

        [XmlIgnore]
        public OriginType OriginType
        {
            get
            {
                if (!Origin.HasValue)
                {
                    return OriginType.Unknown;
                }

                return (OriginType) Origin.Value;
            }
        }

        [XmlIgnore]
        public int? MasterOrder { get; set; }

        [XmlIgnore]
        public bool ModeBatch { get; set; }

        public string PaymentReference { get; set; }

        public string MerchantId { get; set; }

        public string BankAccountNumber { get; set; }

        public bool IsCryptoOptional { get; set; }

        public bool IsTransfertToShoppingCartEnabled { get; set; }

        /// <summary>
		/// Obtient ou définit la plateforme qui est à l'origine de la commande (iphone, ipad, classique...)
		/// </summary>
		public string CallingPlatform { get; set; }

        public bool IsMobile { get; set; }

        public string OrderExternalReference { get; set; }

        public string SenderEmail { get; set; }
    }
}
