﻿using System;
using System.Collections.Generic;
using FnacDirect.OrderPipe.Base.Business.LogisticLineGroups.Initializers;
using FnacDirect.OrderPipe.Base.Business.LogisticLineGroups.InitialSelectors;
using FnacDirect.OrderPipe.Base.Business.LogisticLineGroups.Unfolders;
using FnacDirect.OrderPipe.Base.Business.LogisticLineGroups.Handlers;

namespace FnacDirect.OrderPipe.Base.Business.LogisticLineGroups.Rules
{
    public class SupplySourceLogisticLineGroupGenerationRuleDescriptor
                : BaseLogisticLineGroupGenerationRuleDescriptor<SupplySourceLineGroupInitialSelector,
                                                              SupplySourceLogisticLineGroupInitializer,
                                                              DummyLogisticLineGroupUnfolder>
    {
        public override string SwitchId
        {
            get
            {
                return "orderpipe.logisticlinegroupgenerationrule.supplysource";
            }
        }

        public override IEnumerable<Type> LogisticLineGroupGeneratedHandlers
        {
            get
            {
                yield return typeof(UpdateShippingMethodChoiceLogisticLineGroupHandler);
                yield return typeof(CalculateSubTotalsLogisticLineGroupHandler);
            }
        }
    }
}
