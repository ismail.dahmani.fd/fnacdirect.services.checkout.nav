using System;
using System.Collections.Generic;
using System.Reflection;
using System.Xml.Serialization;
using FnacDirect.Customer.Model;
using FnacDirect.OrderPipe;
using FnacDirect.OrderPipe.Base.BaseModel.Model;
using FnacDirect.OrderPipe.Base.BaseModel.Model.Push;
using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.OrderPipe.Base.Model.AddOn;
using FnacDirect.Quote.Model.Enums;
using FnacDirect.Technical.Framework.CoreServices;
using FnacDirect.Technical.Framework.CoreServices.Localization;
using CheckBillingMethod = FnacDirect.OrderPipe.Base.Model.CheckBillingMethod;
using CreditCardBillingMethod = FnacDirect.OrderPipe.Base.Model.CreditCardBillingMethod;
using GiftCardBillingMethod = FnacDirect.OrderPipe.Base.Model.GiftCardBillingMethod;
using PayPalBillingMethod = FnacDirect.OrderPipe.Base.Model.PayPalBillingMethod;
using VirtualGiftCheckBillingMethod = FnacDirect.OrderPipe.Base.Model.VirtualGiftCheckBillingMethod;
using VoucherBillingMethod = FnacDirect.OrderPipe.Base.Model.VoucherBillingMethod;

namespace FnacDirect.Quote.Model
{
    [Serializable]
    public class QuoteEntity : QuoteForm
    {
        private QuoteForm _quoteForm;
        public QuoteForm QuoteForm
        {
            get
            {
                if (null == _quoteForm)
                {
                    _quoteForm = new QuoteForm();
                }

                return _quoteForm;
            }
            set { _quoteForm = value; }
        }

        private string _userCode;
        public string UserCode
        {
            get { return _userCode; }
            set { _userCode = value; }
        }

        public QuoteEntity(OF orderForm)
        {
            QuoteForm = (QuoteForm)orderForm;
            QuoteForm.SID = orderForm.SID;
        }

        #region Constructors

        public QuoteEntity()
        {
            QuoteForm = new QuoteForm();
        }

        public QuoteEntity(QuoteForm quoteForm)
        {
            //QuoteEntity l_result = new QuoteEntity();
            ModelHelper.Copy(quoteForm, this);
            QuoteForm = quoteForm;
        }

        #endregion Constructors

        private decimal _totalQuoteAmountBeforeDiscount;
        public decimal TotalQuoteAmountBeforeDiscount
        {
            get
            {
                _totalQuoteAmountBeforeDiscount = QuoteForm.TotalTtcBeforeDiscount;

                return _totalQuoteAmountBeforeDiscount;
            }
            set { _totalQuoteAmountBeforeDiscount = value; }
        }

        private string _customerReference;
        public string CustomerReference
        {
            get { return _customerReference; }
            set { _customerReference = value; }
        }

        [XmlIgnore]
        new public string QuoteXmlContent
        {
            get { return QuoteForm.QuoteXmlContent; }
            set { QuoteForm.QuoteXmlContent = value; }
        }

        private string _cultureString = "fr-FR";
        public string CultureString
        {
            get { return _cultureString; }
            set { _cultureString = value; }
        }

        private QuoteMotif _motifQuote = QuoteMotif.NewCustomer;
        public QuoteMotif MotifQuote
        {
            get { return _motifQuote; }
            set { _motifQuote = value; }
        }

        private string _motifLabel;
        public string MotifLabel
        {
            get
            {
                _motifLabel = GetLiteralMotif(MotifQuote);

                return _statusLabel;
            }
            set { _motifLabel = value; }
        }

        private string GetLiteralMotif(QuoteMotif quoteMotif)
        {
            return Service<IUIResourceService>.Instance.GetUIResourceValue("B2B.QUOTE.MOTIF." + quoteMotif, CultureString);
        }

        private string _quoteDemandRaisonLabel;
        public string QuoteDemandRaisonLabel
        {
            get
            {
                _quoteDemandRaisonLabel = GetDemandRaison(QuoteForm.QuoteDemandRaison);

                return _quoteDemandRaisonLabel;
            }
            set { _quoteDemandRaisonLabel = value; }
        }

        private string GetDemandRaison(QuoteDemandRaisonEnum quoteDemandRaisonEnum)
        {
            return Service<IUIResourceService>.Instance.GetUIResourceValue("B2B.QUOTE.DemandRaison." + quoteDemandRaisonEnum, CultureString);
        }

        private string _statusLabel;
        public string StatusLabel
        {
            get
            {
                _statusLabel = GetLiteralStatus(QuoteForm.Status);

                return _statusLabel;
            }
            set { _statusLabel = value; }
        }

        private string _validityLabel;
        public string ValidityLabel
        {
            get
            {
                var valid = Service<IUIResourceService>.Instance.GetUIResourceValue("B2B.QUOTE.VALIDITY." + "Valide", CultureString);
                var invalid = Service<IUIResourceService>.Instance.GetUIResourceValue("B2B.QUOTE.VALIDITY." + "Invalide", CultureString);

                _validityLabel = QuoteForm.ExpirationDate > DateTime.Now ? valid : invalid;

                return _validityLabel;
            }
            set { _validityLabel = value; }
        }

        private string GetLiteralStatus(int status)
        {
            return Service<IUIResourceService>.Instance.GetUIResourceValue("B2B.QUOTE.STATUS." + ((QuoteStatusEnum)status), CultureString);
        }

        private int _actualPagerIndexer;
        public int ActualPagerIndexer
        {
            get { return _actualPagerIndexer; }
            set { _actualPagerIndexer = value; }
        }

        private decimal _totalAmountPort;
        public decimal TotalAmountPort
        {
            get { return _totalAmountPort; }
            set { _totalAmountPort = value; }
        }

        public QuoteLineDetail QuoteLineDetailMemoryUpdateLineDetail(QuoteLineDetail quoteLineDetail)
        {
            var indexResult =
            QuoteLineDetailCollection.FindIndex(delegate(QuoteLineDetail t)
                                                   {
                                                       return
                                                           t.QuoteDetailID.Equals(quoteLineDetail.QuoteDetailID);
                                                   });
            QuoteLineDetailCollection[indexResult] = quoteLineDetail;

            return QuoteLineDetailCollection[indexResult];
        }

        public QuoteLineDetail GetLineDetailById(Guid id)
        {
            QuoteLineDetail result = null;

            foreach (var quoteLineDetail in QuoteLineDetailCollection)
            {
                if (quoteLineDetail.QuoteDetailID == id)
                {
                    result = quoteLineDetail;
                    break;
                }
            }

            return result;
        }
    }

    [Serializable]
    [XmlInclude(typeof(QuoteEntity))]
    [XmlInclude(typeof(SogepPushData))]
    [XmlInclude(typeof(AdherentData))]
    [XmlInclude(typeof(QasShippingData))]
    [XmlInclude(typeof(QasBillingData))]
    [XmlInclude(typeof(PaymentSelectionData))]
    [XmlInclude(typeof(NullStepData))]
    [XmlInclude(typeof(LastCUUMemoryData))]
    [XmlInclude(typeof(AddressesValidityData))]
    [XmlInclude(typeof(PageRedirectData))]
    [XmlInclude(typeof(NeedValidation))]
    [XmlInclude(typeof(QuoteDetailButtonsGroupData))]
    [XmlInclude(typeof(DefferedPaymentData))]
    [XmlInclude(typeof(GiftCardBillingMethod))]
    [XmlInclude(typeof(CheckBillingMethod))]
    [XmlInclude(typeof(CreditCardBillingMethod))]
    [XmlInclude(typeof(VoucherBillingMethod))]
    [XmlInclude(typeof(VirtualGiftCheckBillingMethod))]
    [XmlInclude(typeof(PayPalBillingMethod))]
    [XmlInclude(typeof(TrackingData))]
    [XmlInclude(typeof(QuoteLineService))]
    [XmlInclude(typeof(AddressOrderInfoCollection))]
	[XmlInclude(typeof(OgoneCreditCardBillingMethod))]
	[XmlInclude(typeof(LimonetikOgoneBillingMethod))]

    public class QuoteForm : BaseOF
    {
        #region Constructors

        public QuoteForm()
        { }

        public QuoteForm(BaseOF of)
        {
            ModelHelper.Copy(of, this);
        }

        #endregion Constructors

        private CompanyInfo _company;
        public CompanyInfo Company
        {
            get { return _company; }
            set { _company = value; }
        }

        private string _quoteCompanyName;
        public string QuoteCompanyName
        {
            get { return _quoteCompanyName; }
            set { _quoteCompanyName = value; }
        }

        private List<ShippingModeValueCollection> _shippingModeValuesCollection;
        public List<ShippingModeValueCollection> ShippingModeValuesCollection
        {
            get { return _shippingModeValuesCollection; }
            set { _shippingModeValuesCollection = value; }
        }
       
        private ModeViewEnum _modeView;
        public ModeViewEnum ModeView
        {
            get
            {
                return _modeView;
            }
            set
            {
                _modeView = value;
            }
        }

        protected QuoteLineDetailCollection _QuoteLineDetailCollection;
        public QuoteLineDetailCollection QuoteLineDetailCollection
        {
            get
            {
                if (_QuoteLineDetailCollection == null)
                {
                    _QuoteLineDetailCollection = new QuoteLineDetailCollection();
                }

                return _QuoteLineDetailCollection;
            }
            set { _QuoteLineDetailCollection = value; }
        }

        private decimal _total_HT_beforeDiscount;
        public decimal TotalHtBeforeDiscount
        {
            get { return _total_HT_beforeDiscount; }
            set { _total_HT_beforeDiscount = value; }
        }

        private decimal _total_TTC_beforeDiscount;
        public decimal TotalTtcBeforeDiscount
        {
            get { return _total_TTC_beforeDiscount; }
            set { _total_TTC_beforeDiscount = value; }
        }

        protected string _quoteXmlContent;
        public string QuoteXmlContent
        {
            get { return _quoteXmlContent; }
            set { _quoteXmlContent = value; }
        }

        private DateTime _expirationDate;
        public DateTime ExpirationDate
        {
            get { return _expirationDate; }
            set { _expirationDate = value; }
        }

        private int _accountId;
        public int AccountID
        {
            get { return _accountId; }
            set { _accountId = value; }
        }

        private string _quoteRefusalComment;
        public string QuoteRefusalComment
        {
            get { return _quoteRefusalComment; }
            set { _quoteRefusalComment = value; }
        }

        private QuoteDemandRaisonEnum _quoteDemandRaison;
        public QuoteDemandRaisonEnum QuoteDemandRaison
        {
            get { return _quoteDemandRaison; }
            set { _quoteDemandRaison = value; }
        }

        private int _status;
        /// <summary>
        /// Gets or sets quote status <see cref="QuoteStatusEnum"/>
        /// </summary>
        public int Status
        {
            get { return _status; }
            set { _status = value; }
        }

        private string _createdBy;
        public string CreatedBy
        {
            get { return _createdBy; }
            set { _createdBy = value; }
        }

        private string _lastModifiedBy;
        public string LastModifiedBy
        {
            get { return _lastModifiedBy; }
            set { _lastModifiedBy = value; }
        }

        private int _articlesCount;
        public int ArticlesCount
        {
            get { return _articlesCount; }
            set { _articlesCount = value; }
        }

        private decimal _totalHt;
        public decimal TotalHT
        {
            get { return _totalHt; }
            set { _totalHt = value; }
        }

        private decimal _wrapCost;
        /// <summary>
        /// total frai d'mballage pour le devis 
        /// </summary>
        public decimal WrapCost
        {
            get { return _wrapCost; }
            set { _wrapCost = value; }
        }

        private decimal _portCost;
        /// <summary>
        /// total frai de livraison pour le devis 
        /// </summary>
        public decimal PortCost
        {
            get { return _portCost; }
            set { _portCost = value; }
        }

        private VATAppliedList _vat;
        public VATAppliedList VAT
        {
            get { return _vat; }
            set { _vat = value; }
        }

        private decimal _totalTtc;
        public decimal Total_TTC
        {
            get { return _totalTtc; }
            set { _totalTtc = value; }
        }

        private int _orderReference;
        public int OrderReference
        {
            get { return _orderReference; }
            set { _orderReference = value; }
        }

        private int _quoteMaster;
        public int QuoteMaster
        {
            get { return _quoteMaster; }
            set { _quoteMaster = value; }
        }

        private DateTime _lastModificationDate;
        public DateTime LastModificationDate
        {
            get { return _lastModificationDate; }
            set { _lastModificationDate = value; }
        }

        private string _privateComment;
        public string PrivateComment
        {
            get { return _privateComment; }
            set { _privateComment = value; }
        }

        private int _contactId;
        public int ContactID
        {
            get { return _contactId; }
            set { _contactId = value; }
        }

        private int _quoteId;
        public int QuoteId
        {
            get { return _quoteId; }
            set { _quoteId = value; }
        }

        private string _quoteReference;
        public string QuoteReference
        {
            get { return _quoteReference; }
            set { _quoteReference = value; }
        }

        private DateTime _creationDate;
        public DateTime CreationDate
        {
            get { return _creationDate; }
            set { _creationDate = value; }
        }

        private string _actualOwner;
        public string ActualOwner
        {
            get { return _actualOwner; }
            set { _actualOwner = value; }
        }

        private string _historyComment;
        public string HistoryComment
        {
            get { return _historyComment; }
            set { _historyComment = value; }
        }

        private QuoteLineRemiseCollection _globalRemises;
        public QuoteLineRemiseCollection GlobalRemises
        {
            get { return _globalRemises; }
            set { _globalRemises = value; }
        }

        private QuoteAddressEntityCollection _addressRepository;
        /// <summary>
        /// Gets or sets addresses repository
        /// </summary>        
        public QuoteAddressEntityCollection AddressRepository
        {
            get
            {
                if (null == _addressRepository)
                {
                    _addressRepository = new QuoteAddressEntityCollection();
                }

                return _addressRepository;
            }
            set { _addressRepository = value; }
        }

        private int _actualIndexer;
        public int ActualIndexer
        {
            get { return _actualIndexer; }
            set { _actualIndexer = value; }
        }

        public ShippingModeValue GetShippingModeValue(string datakey)
        {
            ShippingModeValue result = null;

            foreach (var lg in LineGroups)
            {
                var found = false;

                foreach (var shippingModeValue in lg.ShippingModeValueCollection)
                {
                    if (shippingModeValue.DataKey.ToString() == datakey)
                    {
                        result = shippingModeValue;
                        found = true;
                        break;
                    }
                }

                if (found)
                {
                    break;
                }
            }

            return result;
        }

        private string _shoppingCartSID;
        /// <summary>
        /// Gets or sets shopping cart SID
        /// </summary>
        public string ShoppingCartSID
        {
            get { return _shoppingCartSID; }
            set { _shoppingCartSID = value; }
        }

        private bool _completeAvailability;
        public bool CompleteAvailability
        {
            get
            {
                _completeAvailability = QuoteLineDetailCollection.StandardQuoteLinesByAdress.CompleteDisponibility;

                return _completeAvailability;
            }
            set { _completeAvailability = value; }
        }

        private AddressOrderInfoCollection _addressOrderInfoCollection;
        public AddressOrderInfoCollection AddressOrderInfoCollection
        {
            get
            {
                if (_addressOrderInfoCollection == null)
                {
                    _addressOrderInfoCollection = new AddressOrderInfoCollection();
                }

                return _addressOrderInfoCollection;
            }
            set { _addressOrderInfoCollection = value; }
        }

        private string _quoteReferenceMaster;
        public string QuoteReferenceMaster
        {
            get { return _quoteReferenceMaster; }
            set { _quoteReferenceMaster = value; }
        }

		/// <summary>
		/// Référence de l'adresse de facturation
		/// </summary>
		public string BillingAddressReference { get; set; }
    }

    public class ModelHelper
    {
        public static void Copy(object baseClass, object inheritedClass)
        {
            var type = baseClass.GetType();

            while (type != null)
            {
                var isSubClass = inheritedClass.GetType().IsSubclassOf(type);

                if (isSubClass || type.AssemblyQualifiedName.Equals(inheritedClass.GetType().AssemblyQualifiedName))
                {
                    GetValues(type, baseClass, inheritedClass);
                }

                type = type.BaseType;
            }

        }

        private static void GetValues(Type type, object source, object destination)
        {
            var flags = BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance;
            var fields = type.GetFields(flags);

            foreach (var field in fields)
            {
                field.SetValue(destination, field.GetValue(source));
            }
        }
    }
}
