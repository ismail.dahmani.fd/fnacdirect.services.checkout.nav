﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.OnePage.Omniture
{
    public class OmnitureOrderThankYouViewModel : OmnitureCommonViewModel
    {
        public string PipeRoute { get; set; }
        public string OrderNumbers { get; set; }
        public string PaymentMethod { get; set; }
        public string OrderType { get; set; }
        public string DeliveryMode { get; set; }
        public bool OrderedExpressPlus { get; set; }
        public bool OrderedExpressPlusTrial { get; set; }

        public decimal OrderShippingCost { get; set; }
    }
}
