﻿using FnacDirect.Customer.Model;
using FnacDirect.FnacStore.Model;
using System.Collections.Generic;

namespace FnacDirect.OrderPipe.Base.Business.Shipping.Postal
{
    public class StoreAddressEntity : AddressEntity
    {
        public IEnumerable<FnacTimetable> Timetables { get; set; }

        public IEnumerable<FnacTimetableExceptionnal> ExceptionnalTimetables { get; set; }
    }
}
