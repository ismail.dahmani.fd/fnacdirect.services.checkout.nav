using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.OrderPipe.Base.Model.Facets;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FnacDirect.OrderPipe.Base.Business.Ogone
{
    public interface IOgoneBusiness3DSecure
    {
        string Get3DSecureInfos(
            decimal transactionAmount,
            int creditCardTypeId,
            IGotUserContextInformations iGotUserContextInformations,
            IEnumerable<LineGroups.ILogisticLineGroup> logisticLineGroups,
            BaseModel.ShippingMethodEvaluation shippingMethodEvaluation,
            bool isGuest);

        string Get3DSecureInfos(
           decimal transactionAmount,
           int creditCardTypeId,
           IGotUserContextInformations iGotUserContextInformations,
           IEnumerable<Model.LineGroups.ILineGroup> lineGroups,
           BaseModel.ShippingMethodEvaluation shippingMethodEvaluation,
           bool isGuest);

    }
}
