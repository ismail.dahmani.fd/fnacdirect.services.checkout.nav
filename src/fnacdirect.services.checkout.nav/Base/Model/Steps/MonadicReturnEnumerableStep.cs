﻿using System;
using System.Collections.Generic;

namespace FnacDirect.OrderPipe.Core.Steps
{
    public abstract class MonadicReturnEnumerableStep<TOrderForm, TInput, TOutput> : Step
        where TOrderForm : OF
    {
        protected internal abstract IEnumerable<TInput> Bind(TOrderForm orderForm);

        protected internal abstract TOrderForm Returns(TOrderForm orderForm, IEnumerable<TOutput> output);

        public abstract ProcessResult Process(TInput input);

        public sealed override void PreCalc(OPContext opContext)
        {
            base.PreCalc(opContext);
        }

        public sealed override StepReturn Check(OPContext opContext)
        {
            var orderForm = opContext.OF as TOrderForm;

            var input = Bind(orderForm);

            var checking = Check(input);

            if (checking.HasValue)
                return checking.Value;

            return StepReturn.Process;
        }

        protected internal virtual Maybe<StepReturn> Check(IEnumerable<TInput> input)
        {
            return Maybe<StepReturn>.Empty();
        }

        public sealed override StepReturn Process(OPContext opContext)
        {
            var orderForm = opContext.OF as TOrderForm;

            var inputs = Bind(orderForm);

            var outputs = new List<TOutput>();

            var stepReturn = StepReturn.Next;

            foreach (var input in inputs)
            {
                var result = Process(input);
                if (result.Output.HasValue)
                {
                    outputs.Add(result.Output.Value);
                }
                if(result.StepReturn.HasValue)
                {
                    stepReturn = result.StepReturn.Value;
                    break;
                }
            }

            opContext.OF = Returns(orderForm, outputs);

            return stepReturn;
        }

        public class ProcessResult
        {
            private readonly Maybe<StepReturn> _stepReturn;
            private readonly Maybe<TOutput> _output;

            public ProcessResult()
            {
                _stepReturn = Maybe<StepReturn>.Empty();
                _output = Maybe<TOutput>.Empty();
            }
            
            public ProcessResult(StepReturn stepReturn)
            {
                _stepReturn = stepReturn;
                _output = Maybe<TOutput>.Empty();
            }

            public ProcessResult(StepReturn stepReturn, TOutput output)
            {
                _stepReturn = stepReturn;
                _output = output;
            }

            public Maybe<StepReturn> StepReturn
            {
                get { return _stepReturn; }
            }

            public Maybe<TOutput> Output
            {
                get { return _output; }
            }
        }
    }
}
