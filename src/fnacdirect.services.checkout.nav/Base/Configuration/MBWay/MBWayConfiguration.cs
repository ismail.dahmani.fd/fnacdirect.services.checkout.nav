using System.Collections.Generic;


namespace FnacDirect.OrderPipe.Base.Business.Configuration
{
    public class MBWayConfiguration
    {
        public string MBWayApiUrl { get; set; }
        public string SqlCommerce { get; set; }
        public string EntityId { get; set; }
        public string Currency { get; set; }
        public string PaymentType { get; set; }
        public string PaymentBrand { get; set; }
        public string AccountId { get; set; }
        public string ShopperResultUrl { get; set; }
        public string Authorization { get; set; }

    }
}
