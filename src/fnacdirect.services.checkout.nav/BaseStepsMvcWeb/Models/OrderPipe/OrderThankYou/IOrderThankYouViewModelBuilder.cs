﻿using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.Technical.Framework.Utils;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.OrderThankYou
{
    public interface IOrderThankYouViewModelBuilder
    {
        OrderThankYouViewModel Build(OPContext opContext, PopOrderForm popOrderForm, RangeSet<int> tvArticleTypesIds);
    }
}
