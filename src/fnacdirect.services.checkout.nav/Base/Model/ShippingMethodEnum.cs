﻿namespace FnacDirect.OrderPipe.Base.Model
{
    public enum ShippingMethodEnum
    {
        LivraisonRapidePostal = 1,
        ColissimoSuiviEurope = 2,
        Chronopost = 3,
        LivraisonRapideChronopost = 4,
        ServiceEnLigneNonExpedie = 5,
        RelaisColis = 6,
        LivraisonRapideCalberson = 7,
        FedEx = 8,
        RetraitMagasin = 9,
        LivraisonRapideLLT = 10,
        LivraisonExpressChronopost = 11,
        Coursier1012 = 12,
        Coursier1618 = 13,
        Coursier1921 = 14,
        Coursier1921Bis = 15,
        LivraisonExpressDHL = 16,
        Adrexo = 17,
        LivraisonNormal = 20,
        LivraisonSuivie = 21,
        LivraisonRecommandee = 22,
        Telechargement = 23,
        RetraitChezLeVendeur = 28,
        LivraisonMarketPlaceExpress=50,
        LivraisonMarketPlaceGenerique1=51,
        LivraisonMarketPlaceGenerique2=52,
        LivraisonMarketPlaceGenerique3 = 53,
        LivraisonMarketPlaceGenerique4 = 54,
        LivraisonMarketPlaceRelaisColisSOGEP = 55,
        LivraisonNextDayDelivery = 56,
        Livraison3heuresChrono = 57,
        LivraisonNextDayDeliveryIDF = 66,
    }
}
