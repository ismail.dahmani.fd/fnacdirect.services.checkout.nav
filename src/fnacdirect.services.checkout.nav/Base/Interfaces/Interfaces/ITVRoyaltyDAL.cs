﻿using FnacDirect.OrderPipe.Base.Model;
using System.Collections.Generic;
namespace FnacDirect.OrderPipe.Base.DAL
{
    public interface ITVRoyaltyDAL
    {
        Dictionary<int, string> GetTelevisualRoyaltyWays(string defaultLabel);
        BillingAddress GetOrderBillingAddressTvRoyalty(string orderRef, out int errorCode, out int orderId, out int pridTv, out int quantity, out int orderDetailPk, out int accountId);
    }
}
