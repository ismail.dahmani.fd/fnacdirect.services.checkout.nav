﻿using FnacDirect.OrderPipe.Base.Model;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Common.Basket
{
    public class OrderExternalReferenceViewModelBuilder : IOrderExternalReferenceViewModelBuilder
    {
        public OrderExternalReferenceViewModel Build(PopOrderForm popOrderForm)
        {
            return new OrderExternalReferenceViewModel()
            {
                Value = popOrderForm.OrderGlobalInformations.OrderExternalReference
            };            
        }
    }
}
