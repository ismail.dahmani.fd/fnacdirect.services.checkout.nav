﻿using System.Web.Mvc;

namespace FnacDirect.OrderPipe.CoreMvcWeb
{
    public class OrderPipeRazorViewEngine : RazorViewEngine
    {
        public OrderPipeRazorViewEngine()
        {
            var viewLocations = new[]
            {
                "~/Legacy/OrderPipe/{1}/{0}.cshtml",
                "~/Legacy/OrderPipe/Shared/{0}.cshtml",
            };

            AreaMasterLocationFormats = null;
            AreaPartialViewLocationFormats = null;
            AreaViewLocationFormats = null;

            PartialViewLocationFormats = viewLocations;
            ViewLocationFormats = viewLocations;
        }
    }
}