using FnacDirect.Contracts.Online.Model;
using FnacDirect.OrderPipe.Base.BaseModel;
using FnacDirect.OrderPipe.Base.BaseModel.Model;
using FnacDirect.OrderPipe.Base.BaseModel.Model.Push;
using FnacDirect.OrderPipe.Base.LineGroups;
using FnacDirect.OrderPipe.Base.Model.B2B;
using FnacDirect.OrderPipe.Base.Model.Facets;
using FnacDirect.OrderPipe.Base.Model.LineGroups;
using FnacDirect.OrderPipe.Base.Model.OrderInformations;
using FnacDirect.OrderPipe.Base.Model.UserInformations;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Xml.Serialization;
using DTO = FnacDirect.Contracts.Online.Model;
using FM = FnacDirect.FnacStore.Model;

namespace FnacDirect.OrderPipe.Base.Model
{
    [XmlInclude(typeof(AboIlliData))]
    [XmlInclude(typeof(QasBillingData))]
    [XmlInclude(typeof(QasShippingData))]
    [XmlInclude(typeof(SubscriptionsData))]
    [XmlInclude(typeof(SogepPushData))]
    [XmlInclude(typeof(OrderProcessData))]
    [XmlInclude(typeof(DirectLinkData))]
    public class BaseOF : BasketOF,
                         IGotContextInformations,
                         IGotMobileInformations,
                         IGotUserContextInformations,
                         IGotShippingMethodEvaluation,
                         IGotLineGroups,
                         IGotBillingInformations,
                         IGotLogisticLineGroups,
                         IGotQuoteInformations
    {
        /// <summary>
        /// Représente l'identifiant en base de la méthode de livraison "Retrait chez le vendeur".
        /// </summary>
        const int HAND_DELIVER_SHIPPING_METHOD_ID = 28;

        public BillingAddress BillingAddress { get; set; }

        public string RelayCellphone { get; set; }

        private readonly BillingMethodList _orderBillingMethods = new BillingMethodList();

        public BillingMethodList OrderBillingMethods
        {
            get { return _orderBillingMethods; }
        }

        public void AddBillingMethod(BillingMethod billingMethod)
        {
            _orderBillingMethods.Add(billingMethod);
        }

        public void RemoveBillingMethod(int billingMethodId)
        {
            _orderBillingMethods.RemoveAll(m => m.BillingMethodId == billingMethodId);
        }

        public void RemoveBillingMethod(Predicate<BillingMethod> predicate)
        {
            _orderBillingMethods.RemoveAll(predicate);
        }

        public void SortOrderBillingMethods()
        {
            _orderBillingMethods.Sort();
        }

        public void ClearBillingMethods()
        {
            _orderBillingMethods.Clear();
        }

        private readonly List<int> _hiddenCreditCards = new List<int>();

        public List<int> HiddenCreditCards
        {
            get { return _hiddenCreditCards; }
        }

        private OrderInfo _orderInfo = new OrderInfo();

        public OrderInfo OrderInfo
        {
            get { return _orderInfo; }
            set { _orderInfo = value; }
        }

        private readonly ArticleList _unshippableArticles = new ArticleList();

        [XmlIgnore]
        public ArticleList UnshippableArticles
        {
            get { return _unshippableArticles; }
        }

        private readonly ArticleList _unfoldedArticles = new ArticleList();

        [XmlIgnore]
        public ArticleList UnfoldedArticles
        {
            get { return _unfoldedArticles; }
        }

        private readonly LogisticTypeList _logisticTypes = new LogisticTypeList();

        public LogisticTypeList LogisticTypes
        {
            get { return _logisticTypes; }
        }

        [XmlIgnore]
        ArticleList IGotLineGroups.DeletedArticles
        {
            get
            {
                return DeletedArticles;
            }
        }

        [XmlIgnore]
        ArticleList IGotLineGroups.RemovedArticles
        {
            get
            {
                return RemovedArticles;
            }
        }

        [XmlIgnore]
        public LineGroup NumericalArticles { get; set; }

        [XmlIgnore]
        public LineGroup DematSoftArticles { get; set; }

        [XmlIgnore]
        public ShippingAddress CalcShippingAddress { get; set; }

        [DefaultValue(0)]
        public int ChosenWrapMethod { get; set; }

        public BillingMethod MainBillingMethod { get; set; }

        [XmlIgnore]
        public int BillingMethodMask { get; set; }

        [XmlIgnore]
        public Dictionary<int, decimal> BillingMethodConsolidation { get; set; }

        [XmlIgnore]
        public decimal RemainingTotal { get; set; }

        [XmlIgnore]
        public bool DisableSplit { get; set; }

        [XmlIgnore]
        public List<Credit> AvailableCredits { get; set; }

        IList<Credit> IGotBillingInformations.AvailableCredits
        {
            get
            {
                return AvailableCredits;
            }
        }

        public string PaymentResponseUrl { get; set; }

        [XmlIgnore]
        public bool IsOnlyPEArticles
        {
            get
            {
                var result = true;
                foreach (var art in Articles)
                {
                    var group = (ArticleGroup)art.Family.GetValueOrDefault(0);

                    if (!(group == ArticleGroup.Book
                        || group == ArticleGroup.Video
                        || group == ArticleGroup.Audio
                        || group == ArticleGroup.Software))
                    {
                        result = false;
                        break;
                    }
                }
                return result;
            }
        }

        [XmlIgnore]
        public bool EnableOgoneCreditCard { get; set; }

        public BasketType BasketType { get; set; }

        [XmlIgnore]
        public List<ShippingAddress> AllShippingAddresses
        {
            get
            {
                var lst = new List<ShippingAddress>();
                if (CalcShippingAddress != null && CalcShippingAddress.Identity != -1) lst.Add(CalcShippingAddress);
                foreach (var lg in LineGroups)
                {
                    if (lg.ShippingAddress != null && !lst.Contains(lg.ShippingAddress))
                    {
                        lst.Add(lg.ShippingAddress);
                    }
                }
                return lst;
            }
        }

        private FM.FnacStore _store;
        public FM.FnacStore Store
        {
            get { return _store ?? new FM.FnacStore(); }
            set { _store = value; }
        }

        public void ClearShippingAddresses()
        {
            foreach (var lineGroup in LineGroups)
            {
                lineGroup.ShippingAddress = null;
            }
        }

        public void SetShippingAddresses(ShippingAddress shippingAddress)
        {
            CalcShippingAddress = shippingAddress;

            // On ne modifie que les linegroups non Clic & Collect
            foreach (var lineGroup in LineGroups.FindAll(lineGroup => lineGroup.Articles.Any(article =>
                article.SellerID != SellerIds.GetId(Base.LineGroups.SellerType.ClickAndCollect) &&
                article.SellerID != SellerIds.GetId(Base.LineGroups.SellerType.Shop))))
            {
                lineGroup.ShippingAddress = shippingAddress;
            }
        }

        /// <summary>
        /// Détermine si la livraison concerne exclusivement le retrait chez le vendeur.
        /// </summary>
        public bool IsHandDeliveryOnly
        {
            get
            {
                var l_Result = false;

                if (LineGroups != null && LineGroups.Count > 0)
                {
                    var l_Index = 0;
                    l_Result = true;

                    while (l_Result && l_Index < LineGroups.Count)
                    {
                        if (LineGroups[l_Index].ShippingMethodChoice != null &&
                            LineGroups[l_Index].ShippingMethodChoice.SelectedChoice != null &&
                            !LineGroups[l_Index].ShippingMethodChoice.SelectedChoice.Choice.GetValueOrDefault().Equals(HAND_DELIVER_SHIPPING_METHOD_ID))
                        {
                            l_Result = false;
                        }

                        l_Index++;
                    }
                }

                return l_Result;
            }
        }

        private ShippingMethodEvaluation _shippingMethodEvaluation;

        public BaseOF()
        {
            ChosenWrapMethod = 0;
            EnableOgoneCreditCard = false;
            DisableSplit = false;
            BillingAddress = null;
            AvailableCredits = new List<Credit>();
            LogisticLineGroups = new List<LogisticLineGroup>();
        }

        public ShippingMethodEvaluation ShippingMethodEvaluation
        {
            get { return _shippingMethodEvaluation ?? (_shippingMethodEvaluation = new ShippingMethodEvaluation()); }
            set { _shippingMethodEvaluation = value; }
        }

        [XmlIgnore]
        public bool ContainsBook
        {
            get
            {
                return LineGroups.SelectMany(lg => lg.Articles).OfType<StandardArticle>().Any(
                    a => (ArticleGroup)a.Family.GetValueOrDefault(0) == ArticleGroup.Book && a.ItemPhysicalType == PhysicalType.Physical);
            }
        }
        public IOrderGlobalInformations OrderGlobalInformations
        {
            get
            {
                return OrderInfo;
            }
        }
        public IOrderContextInformations OrderContextInformations
        {
            get
            {
                return OrderInfo;
            }
        }

        public IOrderMobileInformations OrderMobileInformations
        {
            get
            {
                return OrderInfo;
            }
        }

        public IUserContextInformations UserContextInformations
        {
            get
            {
                return UserInfo;
            }
        }

        [XmlIgnore]
        IEnumerable<ILineGroup> IGotLineGroups.LineGroups
        {
            get
            {
                return LineGroups;
            }
        }

        public void RemoveLineGroup(ILineGroup lineGroup)
        {
            if (lineGroup is LineGroup)
            {
                LineGroups.Remove(lineGroup as LineGroup);
            }
        }

        public void ClearLineGroups()
        {
            LineGroups.Clear();
        }

        public void AddLineGroup(ILineGroup lineGroup)
        {
            if (lineGroup is LineGroup)
            {
                LineGroups.Add(lineGroup as LineGroup);
            }
        }

        public void InsertLineGroup(ILineGroup lineGroup,int index)
        {
            if (lineGroup is LineGroup)
            {
                LineGroups.Insert(index, lineGroup as LineGroup);
            }
        }

        public ILineGroup BuildLineGroup(ArticleList articleList)
        {
            return new LineGroup()
            {
                Articles = articleList
            };
        }

        public ILineGroup BuildTemporaryLineGroup(ArticleList articleList)
        {
            return new TemporaryLineGroup
            {
                Articles = articleList
            };
        }

        IEnumerable<Article> IGotLineGroups.Articles
        {
            get
            {
                return Articles;
            }
        }

        bool IGotLineGroups.ArticlesModified
        {
            get
            {
                return Articles.Modified;
            }
            set
            {
                Articles.Modified = value;
            }
        }

        IEnumerable<BillingMethod> IGotBillingInformations.OrderBillingMethods
        {
            get
            {
                return OrderBillingMethods;
            }
        }

        IOrderGlobalInformations IGotBillingInformations.OrderGlobalInformations
        {
            get
            {
                return OrderInfo;
            }
        }

        IEnumerable<Promotion> IGotBillingInformations.GlobalPromotions
        {
            get
            {
                return GlobalPromotions;
            }
        }

        public void AddGlobalPromotion(Promotion promotion)
        {
            GlobalPromotions.Add(promotion);
        }

        public new bool HasFnacComArticle
        {
            get
            {
                return this.HasFnacComArticle();
            }
        }

        public new bool HasMarketPlaceArticle
        {
            get
            {
                return this.HasMarketPlaceArticle();
            }
        }

        public new bool HasClickAndCollectArticle
        {
            get
            {
                return this.HasClickAndCollectArticle();
            }
        }

        public bool IsOnlyNumerical
        {
            get
            {
                return this.IsOnlyNumerical();
            }
        }

        public bool HasNumericalArticle
        {
            get
            {
                return this.HasNumericalArticle();
            }
        }

        public List<LogisticLineGroup> LogisticLineGroups { get; set; }

        IEnumerable<ILogisticLineGroup> IGotLogisticLineGroups.LogisticLineGroups
        {
            get
            {
                return LogisticLineGroups as IEnumerable<ILogisticLineGroup>;
            }
        }

        public void AddLogisticLineGroup(ILogisticLineGroup logisticLineGroup)
        {
            if (logisticLineGroup is LogisticLineGroup)
            {
                LogisticLineGroups.Add(logisticLineGroup as LogisticLineGroup);
            }
        }

        public void RemoveLogisticLineGroup(ILogisticLineGroup logisticLineGroup)
        {
            LogisticLineGroups.Remove(logisticLineGroup as LogisticLineGroup);
        }

        public void RemoveArticles(ArticleList articlesToRemove)
        {
            AddToRemovedArticles(articlesToRemove);

            RemoveOfOrderForm(articlesToRemove);
        }

        private void AddToRemovedArticles(ArticleList articlesToRemove)
        {
            foreach (var articleToRemove in articlesToRemove)
            {
                var notExist = !RemovedArticles.Select(ra => ra.ProductID).Contains(articleToRemove.ProductID);
                if (notExist)
                    RemovedArticles.Add(articleToRemove);
            }
        }

        private void RemoveOfOrderForm(ArticleList articlesToRemove)
        {
            var articlesIds = articlesToRemove.Select(a => a.ProductID);
            foreach (var lg in LineGroups)
            {
                lg.RemoveArticlesById(articlesIds);
            }
        }

        public bool HasProfessionalAddressForSeller(int sellerId)
        {
            var shippingMethodEvaluationGroupForSeller = ShippingMethodEvaluation.GetShippingMethodEvaluationGroup(sellerId);

            if (shippingMethodEvaluationGroupForSeller.HasValue &&
                shippingMethodEvaluationGroupForSeller.Value.SelectedShippingMethodEvaluationType == ShippingMethodEvaluationType.Postal)
            {
                var selectedShippingMethodEvaluation = shippingMethodEvaluationGroupForSeller.Value.SelectedShippingMethodEvaluation;
                var shippingAddressLegalStatus = selectedShippingMethodEvaluation.ShippingAddress?.LegalStatus;

                if (IsProfessionalLegalStatus(shippingAddressLegalStatus))
                {
                    return true;
                }
            }

            var billingAddressLegalStatus = BillingAddress?.LegalStatus;

            if (IsProfessionalLegalStatus(billingAddressLegalStatus))
            {
                return true;
            }

            return false;
        }

        private static bool IsProfessionalLegalStatus(int? legalStatus)
        {
            return legalStatus.HasValue && legalStatus.Value == (int)LegalStatus.Business;
        }

        public void AddArticle(Article article)
        {
            //Chelou à quoi sert base OF ?
            // Nous tentons de récupérer le groupe d'articles correspondant au vendeur de l'article en cours de lecture.
            var lineGroup = LineGroups.FirstOrDefault(l => l.Seller.SubSellerId == article.SellerID.GetValueOrDefault());

            if (lineGroup == null)
            {
                // Initialisation du nouveau groupe d'article à partir des informations de L'OrderForm et de l'article en cours de lecture.
                lineGroup = new LineGroup
                {
                    Seller =
                        {
                            SellerId = article.SellerID.GetValueOrDefault()
                        }
                };

                if (article is MarketPlaceArticle)
                {
                    var marketPlaceArticle = article as MarketPlaceArticle;
                    var marketPlaceSeller = marketPlaceArticle.Offer.Seller;
                    lineGroup.Seller.DisplayName = marketPlaceSeller.Company ?? string.Format("{0} {1}", marketPlaceSeller.FirstName, marketPlaceSeller.LastName);
                }
                else if (article is Model.VirtualGiftCheck)
                {
                    lineGroup.Seller.SellerId = SellerIds.GetId(Base.LineGroups.SellerType.VirtualGiftCheck); // CCV
                }

                LineGroups.Add(lineGroup);
            }

            lineGroup.Articles.Add(article);
        }

        public decimal? RemainingGlobalPricesForPrimaryMethods()
        {
            var globalPrice = GlobalPrices?.TotalPriceEur.GetValueOrDefault();
            foreach (var billingMethod in OrderBillingMethods.Where(m => !m.IsPrimary && !(m is FreeBillingMethod)))
            {
                globalPrice -= billingMethod.Amount.GetValueOrDefault(0);
            }
            return globalPrice;
        }

        [XmlIgnore]
        public QuoteInfos QuoteInfos { get; set; }
    }
}

