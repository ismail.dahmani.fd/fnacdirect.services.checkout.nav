using FnacDirect.OrderPipe.Base.Model;
using System.Collections.Generic;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Basket
{
    public interface IBasketSellerViewModelBuilder
    {
        BasketSellerViewModel Build(PopOrderForm popOrderForm, int seller, List<BaseArticle> articles);
    }
}
