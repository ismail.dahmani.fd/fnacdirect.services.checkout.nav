using FnacDirect.Contracts.Online.Model;
using System;
using System.Collections.Generic;

namespace FnacDirect.Quote.Model
{
    [Serializable]
    public class QuoteLinePromotions : List<QuoteLinePromotion>
    {
        public QuoteLinePromotions(List<Promotion> promotions)
        {
            foreach (var promotion in promotions)
            {
                Add(new QuoteLinePromotion(promotion));
            }
        }

        public QuoteLinePromotions() { }
    }

    [Serializable]
    public class QuoteLinePromotion
    {
        public string Code { get; set; }
        public string ShoppingCartText { get; set; }
        public int? Id { get; set; }
        public string ArticleText { get; set; }
        public string ShoppingCartBanner { get; set; }
        public string ConditionsUrl { get; set; }
        public PromotionType Type { get; set; }
        public SegmentCollection CustomerSegments { get; set; }
        public string AdvantageCode { get; set; }
        public PromotionUsage Usage { get; set; }
        public string ArticleBanner { get; set; }
        public PromotionExtendedProperties ExtendedProperties { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public string SHBannerCode { get; set; }
        public string SHText { get; set; }
        public string SHPictoUrl { get; set; }
        public string TemplateCode { get; set; }
        public string ArticleStimulisPriceText { get; set; }
        public string ArticleStimulisThematiqueText { get; set; }
        public bool IsDiscountBlock { get; set; }
        public string LongWording { get; set; }
        public string ShortWording { get; set; }
        public PromotionTemplate DisplayTemplate { get; set; }
        public int? PromotionPushPictoId { get; set; }
        public bool? Display { get; set; }
        public decimal? Rebate { get; set; }

        public QuoteLinePromotion(Promotion promotion)
        {
            Code = promotion.Code;
            ShoppingCartText = promotion.ShoppingCartText;
            Id = promotion.Id;
            ArticleText = promotion.ArticleText;
            ShoppingCartBanner = promotion.ShoppingCartBanner;
            ConditionsUrl = promotion.ConditionsUrl;
            Type = promotion.Type;
            CustomerSegments = promotion.CustomerSegments;
            AdvantageCode = promotion.AdvantageCode;
            Usage = promotion.Usage;
            ArticleBanner = promotion.ArticleBanner;
            ExtendedProperties = promotion.ExtendedProperties;
            StartDate = promotion.StartDate;
            EndDate = promotion.EndDate;
            SHBannerCode = promotion.SHBannerCode;
            SHText = promotion.SHText;
            SHPictoUrl = promotion.SHPictoUrl;
            TemplateCode = promotion.TemplateCode;
            ArticleStimulisPriceText = promotion.ArticleStimulisPriceText;
            ArticleStimulisThematiqueText = promotion.ArticleStimulisThematiqueText;
            IsDiscountBlock = promotion.IsDiscountBlock;
            LongWording = promotion.LongWording;
            ShortWording = promotion.ShortWording;
            DisplayTemplate = promotion.DisplayTemplate;
            PromotionPushPictoId = promotion.PromotionPushPictoId;
            Display = promotion.Display;
            Rebate = promotion.Rebate;
        }

        public QuoteLinePromotion() { }
    }
}
