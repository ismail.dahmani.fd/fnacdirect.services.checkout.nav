using FnacDirect.Membership.Model.Constants;
using FnacDirect.OrderPipe.Base.BaseModel.Model;
using FnacDirect.OrderPipe.Base.Business;
using FnacDirect.OrderPipe.Base.Business.InstallmentPayment;
using FnacDirect.OrderPipe.Base.Business.Payment;
using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.OrderPipe.Base.Model.Facets;
using FnacDirect.OrderPipe.Base.Model.Pop;
using FnacDirect.OrderPipe.Base.Proxy.Customer;
using FnacDirect.OrderPipe.Base.Proxy.Ogone;
using FnacDirect.OrderPipe.BaseMvc.Web.Services.Pop.Payment;
using FnacDirect.OrderPipe.Core.Services;
using FnacDirect.Technical.Framework.Web.Mvc;
using System.Collections.Generic;
using System.Linq;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Common.Payment.BillingMethodFactories
{
    public class OgoneViewModelFactory : BaseCreditCardViewModelFactory<OgoneCreditCardBillingMethod>
    {
        private readonly ISwitchProvider _switchProvider;
        private readonly IPipeParametersProvider _pipeParametersProvider;
        private readonly IPaymentMethodService _paymentMethodService;
        private readonly ICreditCardDescriptorBusiness _creditCardDescriptorBusiness;
        private readonly IOgoneConfigurationService _ogoneConfigurationService;
        private readonly IBillingMethodService _billingMethodService;
        private readonly IApplicationContext _applicationContext;
        private readonly IInstallmentService _installmentService;
        private readonly IUserExtendedContextProvider _userExtendedContextProvider;

        public OgoneViewModelFactory(IApplicationContext applicationContext,
                                     IPipeParametersProvider pipeParametersProvider,
                                     IPaymentMethodService paymentMethodService,
                                     ICustomerCreditCardService customerCreditCardService,
                                     ICreditCardDescriptorBusiness creditCardDescriptorBusiness,
                                     IInstallmentService installmentService,
                                     ISwitchProvider switchProvider,
                                     IOgoneConfigurationService ogoneConfigurationService,
                                     IBillingMethodService billingMethodService,
                                     IUserExtendedContextProvider userExtendedContextProvider)

            : base(applicationContext, creditCardDescriptorBusiness, customerCreditCardService, paymentMethodService)
        {
            _applicationContext = applicationContext;
            _pipeParametersProvider = pipeParametersProvider;
            _switchProvider = switchProvider;
            _paymentMethodService = paymentMethodService;
            _installmentService = installmentService;
            _creditCardDescriptorBusiness = creditCardDescriptorBusiness;
            _ogoneConfigurationService = ogoneConfigurationService;
            _billingMethodService = billingMethodService;
            _userExtendedContextProvider = userExtendedContextProvider;
        }

        public override BillingMethodViewModel Get(string tabName, PaymentMethod paymentMethod, OPContext opContext, PopOrderForm popOrderForm)
        {
            if (paymentMethod.SpecificRules.Contains(SpecificRules.InstallmentPayment))
            {
                var orderFormMultiFacets = MultiFacet<IGotLineGroups, IGotBillingInformations, IGotUserContextInformations, IGotShippingMethodEvaluation>.From(popOrderForm);

                var parameters =
                    new InstallmentServiceParameters(orderFormMultiFacets, _userExtendedContextProvider, _applicationContext.GetLocalCountry());

                var partnerOffer = 
                    _installmentService.GetEligibleCreditCardIds(parameters);
                return new InstallmentPaymentBillingMethodViewModel(partnerOffer, _switchProvider);
            }

            // Version light du modèle Ogone: n'envoyer que le minimum nécessaire (dont la liste des cartes et la propriété Display)
            if (paymentMethod.SpecificRules.Contains(SpecificRules.Light))
            {
                var lightViewModel = new OgoneBillingMethodViewModel();
                CreditCardBillingMethod(lightViewModel, popOrderForm, tabName, paymentMethod);
                return lightViewModel;
            }

            var hasMembershipSouscriptionCardInBasket = popOrderForm.LineGroups.GetArticles()
                .OfType<StandardArticle>()
                .Any(
                    art => art.TypeId.GetValueOrDefault() == (int)CategoryCardType.Recruit || art.TypeId.GetValueOrDefault() == (int)CategoryCardType.Renew
                );

            // Ogone Alias Gateway Parameters Constants
            var viewModel = new OgoneBillingMethodViewModel();
            viewModel.OgoneViewModel.OgoneAliasGatewayNewCardViewModel.Holder = string.Format("{0} {1}", popOrderForm.BillingAddress.Firstname, popOrderForm.BillingAddress.Lastname);
            viewModel.OgoneViewModel.OgoneAliasGatewayNewCardViewModel.ECardPrefixes = _creditCardDescriptorBusiness.GetECardPrefixesRange();

            CreditCardBillingMethod(viewModel, popOrderForm, tabName, paymentMethod);

            // Filtrer les cartes de credit, selon les regles specifiques
            // React seulement
            var popData = opContext.OF.GetPrivateData<PopData>(create: false);
            if (popData != null && popData.UseReact)
            {

                var creditCartTypeList = paymentMethod.GetCreditCardTypes(opContext);
                if (creditCartTypeList.Count > 0)
                {

                    // Definir les cartes à filtrer, selon la SpecificRules
                    var cardtoRemove = new List<CreditCardViewModel>();

                    if (paymentMethod.SpecificRules.Contains(SpecificRules.OnlyCreditCardType))
                    {
                        var request = viewModel.CreditCards.Where((cc) => !creditCartTypeList.Contains(cc.Id));
                        cardtoRemove.AddRange(request);
                    }

                    if (paymentMethod.SpecificRules.Contains(SpecificRules.ExcludeCreditCardType))
                    {
                        var request = viewModel.CreditCards.Where((cc) => creditCartTypeList.Contains(cc.Id));
                        cardtoRemove.AddRange(request);
                    }

                    // Filtrage
                    cardtoRemove = cardtoRemove.Distinct().ToList();
                    foreach (var creditCard in cardtoRemove)
                        viewModel.RemoveCreditCard(creditCard);
                }
            }


            var aboIlliData = popOrderForm.GetPrivateData<AboIlliData>("AboIlliGroup");
            var fnacplusData = popOrderForm.GetPrivateData<PushFnacPlusAdherentData>("PushFnacPlusAdherentGroup", create: false);
            var saveCreditCardOnTacitAgreement = _pipeParametersProvider.Get("save.creditcard.on.tacit.agreement", true);

            viewModel.DisplayTacitAgreement = (saveCreditCardOnTacitAgreement && (hasMembershipSouscriptionCardInBasket || aboIlliData.HasAboIlliInBasket || (fnacplusData != null && fnacplusData.HasFnacPlusCardInBasket)));

            if (_switchProvider.IsEnabled("op.ogone.enablealiasgateway"))
            {
                // Remaining amount utilisé pour déterminé si le paiement est éligible au 3DSecure
                viewModel.RealAmount = popOrderForm.PrivateData.GetByType<PaymentSelectionData>().RemainingAmount * 100;

                var creditCards = _paymentMethodService.GetCreditCardsDescriptor();
                viewModel.IsECardAutorised = !popOrderForm.IsMultiPspCommand();

                foreach (var creditCardViewModel in viewModel.CreditCards)
                {
                    var creditCard = creditCards.FirstOrDefault(x => x.Id == creditCardViewModel.Id);
                    // Get éligibilité de la CB/panier au 3DSecure
                    var flag3DsEnable = _paymentMethodService.Is3DSecureEnable(creditCard.Id, popOrderForm, viewModel.RealAmount);

                    // Check le switch de DirectLink3DS (D3D)
                    var d3dEnable = _paymentMethodService.IsDirectLink3DSecureEnable(creditCard.Id, flag3DsEnable);
                    creditCardViewModel.TransactionProcess = (!d3dEnable ? null : creditCard.TransactionProcess) ?? "Ecommerce"; // Ecommerce = default process for ogone
                    creditCardViewModel.Is3DSecureTransaction = flag3DsEnable;
                    creditCardViewModel.IsCryptoRequired = true;
                }
            }

            viewModel.IsClicInStore = IsClicInStore(popOrderForm.UserContextInformations.ContactEmail);

            var isGuest = popOrderForm.UserContextInformations.IsGuest;
            var localCountryId = _applicationContext?.GetCountryId();

            var ogoneConfigurationData = popOrderForm.GetPrivateData<OgoneConfigurationData>(create:false);

            // Si clicinstore activé et utilisé => On retire la liste des cartes enregistrées et la possibilité d'enregistrer de nouvelles cartes.
            // Meme chose avec les commandes du callcenter si le switch est activé
            viewModel.DisableCustomerCards = (!paymentMethod.DisplayRecordedPayments)
                                         || (_switchProvider.IsEnabled("orderpipe.pop.payment.option.clicinstore") && viewModel.IsClicInStore)
                                         || (_switchProvider.IsEnabled("orderpipe.pop.payment.disablecustomercardsforcallcenter") && popOrderForm.UserContextInformations.IdentityImpersonator != null)
                                         || (!_switchProvider.IsEnabled("op.ogone.enablealiasgateway") && localCountryId == (int)CountryEnum.Espagne)
                                         || (ogoneConfigurationData != null && !ogoneConfigurationData.ShouldUseCustomerCreditCards);

            viewModel.DisplaySaveCreditCard = ogoneConfigurationData == null || ogoneConfigurationData.ShouldUseCustomerCreditCards;

            IEnumerable<CustomerCreditCardViewModel> customerCreditCards = new List<CustomerCreditCardViewModel>();
            if (!viewModel.DisableCustomerCards && !isGuest)
            {
                // Recup les CB sauvegardées du client.
                customerCreditCards = GetCustomerCreditCards(viewModel, popOrderForm.UserContextInformations.UID);
            }

            viewModel.AddCustomerCreditCards(customerCreditCards);

            if (_switchProvider.IsEnabled("op.ogone.enablealiasgateway"))
            {
                // Renseigne les paramètres d'appel à Ogone Alias Gateway dans le ViewModel
                _paymentMethodService.SetAliasGatewayViewModel(viewModel, opContext, popOrderForm);

                foreach (var customerCreditCardViewModel in customerCreditCards)
                {
                    var flagIsCryptoRequired = true;

                    if (!customerCreditCardViewModel.IsDumy)
                    {
                        var cryptoPaymentRequirement = popOrderForm.GetPrivateData<CryptoPaymentRequirement>(create: false);

                        if (cryptoPaymentRequirement != null && cryptoPaymentRequirement.RequirementsByCardTypeId.ContainsKey(customerCreditCardViewModel.TypeId))
                        {
                            flagIsCryptoRequired = cryptoPaymentRequirement.RequirementsByCardTypeId[customerCreditCardViewModel.TypeId];
                        }
                    }

                    customerCreditCardViewModel.IsCryptoRequired = flagIsCryptoRequired;
                }
            }

            if(ShouldProcessPaypalExpressCheckout(popOrderForm, paymentMethod))
            {
                ProcessPaypalExpressCheckout(viewModel);
            }

            if (_billingMethodService.HasSensibleSupportId(popOrderForm.LineGroups.GetArticles()))
            {
                viewModel.DisplayAlone = true;
            }
            return viewModel;
        }

        private bool ShouldProcessPaypalExpressCheckout(PopOrderForm popOrderForm, PaymentMethod paymentMethod)
        {
            var paypalExpressCheckoutData = popOrderForm.GetPrivateData<PaypalExpressCheckoutData>(create: false);
            return paypalExpressCheckoutData != null &&
                paypalExpressCheckoutData.IsInitialized &&
                paymentMethod.SpecificRules.Contains(SpecificRules.Paypal);
        }

        private void ProcessPaypalExpressCheckout(OgoneBillingMethodViewModel viewModel)
        {
            viewModel.DisplayFirst = true;
            viewModel.IsSelected = true;
        }
    }
}
