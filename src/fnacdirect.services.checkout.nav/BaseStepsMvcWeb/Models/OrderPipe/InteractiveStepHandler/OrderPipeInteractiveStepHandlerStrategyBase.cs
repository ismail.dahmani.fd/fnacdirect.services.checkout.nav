﻿using System.Web;
using System.Web.Routing;
using FnacDirect.OrderPipe.Config;

namespace FnacDirect.OrderPipe.CoreRouting
{
    public abstract class OrderPipeInteractiveStepHandlerStrategyBase<TDescriptor> : IOrderPipeInteractiveStepHandlerStrategy<TDescriptor>
        where TDescriptor : class, IUiDescriptor
    {
        public abstract IHttpHandler GetHttpHandlerFor(TDescriptor mvcUiDescriptor, OPContext opContext, RequestContext requestContext);

        public IHttpHandler GetHttpHandlerFor(IUiDescriptor descriptor, OPContext opContext, RequestContext requestContext)
        {
            return GetHttpHandlerFor(descriptor as TDescriptor, opContext, requestContext);
        }
    }
}
