using System.Xml.Serialization;

namespace FnacDirect.OrderPipe.Base.Model
{
    public class EurosFnacBillingMethod : FidelityBillingMethod
    {
        public static int IdBillingMethod
        {
            get { return 268435456; }
        }

        public EurosFnacBillingMethod()
        {
            BillingMethodId = IdBillingMethod;
            Priority = 10;
            CanModifyAmountDown = false;
            CanModifyAmountUp = false;
            AdvantageCodeType = Membership.Model.Constants.AdvantageCodeType.SIEBEL_EUROS;
        }

        [XmlIgnore]
        public int? OrderSiebelId { get; set; }
        [XmlIgnore]
        public string AdherentNumber { get; set; }
        [XmlIgnore]
        public string ContractNumber { get; set; }
        [XmlIgnore]
        public string CardNumber { get; set; }
    }
}
