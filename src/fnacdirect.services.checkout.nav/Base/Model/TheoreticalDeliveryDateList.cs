using System;
using System.Collections.Generic;
using System.Linq;

namespace FnacDirect.OrderPipe.Base.Model
{
    public class TheoreticalDeliveryDateList : List<TheoreticalDeliveryDate>
    {
        public TheoreticalDeliveryDate? Get(Article article)
        {
            Guid? offerRef = null;
            if(article is MarketPlaceArticle mp)
            {
                offerRef = mp.Offer.Reference;
            }
            return Get(article.ProductID.GetValueOrDefault(), offerRef);
        }

        public TheoreticalDeliveryDate? Get(int prid, Guid? offerRef)
        {
            var query = this.Where(e => e.Prid == prid);

            if(offerRef != null)
            {
                query = query.Where(e => offerRef.Equals(e.OfferRef));
            }
            else
            {
                query = query.Where(e => e.OfferRef == null);
            }

            if (!query.Any())
            {
                return null;
            }

            return query.First();
        }

        public void Add(int prid, Guid? offerRef, string deliveryDate, DateTime? startDate, DateTime? endDate)
        {
            Add(new TheoreticalDeliveryDate(prid, offerRef, deliveryDate, startDate, endDate));
        }
    }

    public struct TheoreticalDeliveryDate
    {
        public int Prid { get; private set; }
        public Guid? OfferRef { get; private set; }
        public DateTime? DeliveryStartDate { get; private set; }
        public DateTime? DeliveryEndDate { get; private set; }
        public string DeliveryDate { get; private set; }

        public TheoreticalDeliveryDate(int prid, Guid? offerRef, string deliveryDate, DateTime? startDate, DateTime? endDate)
        {
            Prid = prid;
            OfferRef = offerRef;
            DeliveryDate = deliveryDate;
            DeliveryStartDate = startDate;
            DeliveryEndDate = endDate;
        }
    }
}
