using FnacDirect.OrderPipe.Base.Business.Payment;
using FnacDirect.OrderPipe.Base.LineGroups;
using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.OrderPipe.Base.Proxy.Ogone;
using System.Collections.Generic;
using System.Linq;

namespace FnacDirect.OrderPipe.Base.Business.OrderTransaction
{
    internal class OrderTransactionBusiness : IOrderTransactionBusiness
    {
        private readonly IOgoneConfigurationService _ogoneConfigurationService;
        private readonly ICreditCardTransactionBusiness _creditCardBusiness;

        public OrderTransactionBusiness(IOgoneConfigurationService ogoneConfigurationService, ICreditCardTransactionBusiness creditCardBusiness)
        {
            _ogoneConfigurationService = ogoneConfigurationService;
            _creditCardBusiness = creditCardBusiness;
        }

        public bool CanSetMainOrderTransaction(IEnumerable<ILogisticLineGroup> logisticLineGroups, IEnumerable<BillingMethod> orderBillingMethods, BillingMethod mainBillingMethod = null)
        {
            if (logisticLineGroups.Count() > 1
                && _ogoneConfigurationService.GetPspMode() == PspMode.MonoPsp)
            {
                return true;
            }

            if (mainBillingMethod != null)
            {
                if (mainBillingMethod.IsGlobalPayment)
                {
                    return true;
                }

                if (mainBillingMethod is OgoneCreditCardBillingMethod ogoneCreditCardBillingMethod
                    && _creditCardBusiness.MustAddMainOrderTransaction(ogoneCreditCardBillingMethod)
                    && logisticLineGroups.Count() > 1)
                {
                    return true;
                }
            }

            return false;
        }

        public bool CanSetOrderTransaction(IEnumerable<BillingMethod> orderBillingMethods)
        {
            return true;
        }
    }
}
