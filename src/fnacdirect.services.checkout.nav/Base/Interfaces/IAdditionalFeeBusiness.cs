using FnacDirect.OrderPipe.Base.Model;
using static FnacDirect.OrderPipe.Base.Business.AdditionalFeeBusiness;

namespace FnacDirect.OrderPipe.Base.Business
{
    public interface IAdditionalFeeBusiness
    {
        void CalculateGlobalFeePrices(LogisticTypeList logTypes, FeeCalculationRule calcRule, GetAvailableFeeListDelegate delegAvailable, GetChosenFeeDelegate delegChosen, out decimal nTotalshippingPriceDbEUR, out decimal nTotalshippingPriceUserEUR, out decimal nTotalshippingPriceUserNoVatEUR);
        void CalculateFeePrices(ArticleList articles, LogisticTypeList logTypes, GetAvailableFeeListDelegate deleg);
    }
}
