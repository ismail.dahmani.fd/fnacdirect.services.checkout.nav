using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.OrderPipe.Base.Model.SelfCheckout;

namespace FnacDirect.OrderPipe.Base.Business
{
    public interface ISelfCheckoutService
    {
        void ResetSelfCheckoutOrderForm(PopOrderForm orderForm, bool resetPrivateData);

        ShopTicket CreateShopTicketFromSelfCheckoutOrderForm(PopOrderForm popOrderForm);
    }
}
