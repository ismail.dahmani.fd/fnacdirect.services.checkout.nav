using FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.OnePage.Address;
using System.Globalization;
using System.Linq;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.OnePage.Address.Extensions
{
    public static class AddressViewModelExtension
    {
        public static Pop.Common.Shipping.AddressViewModel ConvertToShippingAddressViewModel(
            this AddressViewModel address, int siteCountryId)
        {
            var result = new Pop.Common.Shipping.AddressViewModel
            {
                Alias = address.AddressName,
                Reference = address.AddressReference,
                Firstname = address.FirstName,
                Lastname = address.LastName,
                ShouldDisplayCompany = !string.IsNullOrEmpty(address.CompanyName),
                CompanyName = address.CompanyName,
                AddressLine1 = address.Address,
                ShouldDisplayAddressLine2 = !string.IsNullOrEmpty(address.Floor),
                AddressLine2 = address.Floor,
                ShouldDisplayAddressLine3 = !string.IsNullOrEmpty(address.AddressComplimentary),
                AddressLine3 = address.AddressComplimentary,
                ZipCode = address.ZipCode,
                City = address.City,
                TaxIdNumber = address.TaxIdNumber,
                ShouldDisplayCountry = siteCountryId != address.CountryId,
                IsShippingAndBillingAddresseAreEqual = address.UseAsDefaultBillingAddress
            };

            var countryViewModel = address.Countries.FirstOrDefault(c => c.Id == address.CountryId.ToString(CultureInfo.InvariantCulture));
            if (countryViewModel != null)
            {
                result.Country = countryViewModel.Label;
            }

            return result;
        }
    }
}
