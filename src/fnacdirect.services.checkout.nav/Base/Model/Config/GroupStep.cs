using System.Collections.Generic;
using System.Xml.Serialization;

namespace FnacDirect.OrderPipe.Core.Model.Config
{
	public class GroupStep : Step
	{
		public GroupStep()
		{
			Class = "FnacDirect.OrderPipe.GroupStep";
		}

		[XmlAttribute("usetransaction")]
		public bool UseTransaction { get; set; }

		[XmlElement("Step", typeof(Step)), XmlElement("GroupStep", typeof(GroupStep))]
		public List<BaseStep> Steps { get; set; }
	}
}