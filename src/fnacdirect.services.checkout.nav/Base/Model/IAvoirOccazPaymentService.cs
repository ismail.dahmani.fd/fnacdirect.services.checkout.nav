using FnacDirect.OrderPipe.Base.Model.Facets;

namespace FnacDirect.OrderPipe.Base.Business.Payment
{
    public interface IAvoirOccazPaymentService
    {
        bool IsAllowed(IGotLineGroups iGotLineGroups, int delayAfterExpiration, bool disableClickAndCollectCheck = false, bool disableECCVCheck = false);
    }
}
