using System.Collections.Generic;
using static FnacDirect.OrderPipe.Constants;
using System.Linq;
using FnacDirect.Technical.Framework.ServiceLocation;
using FnacDirect.Technical.Framework.Web.Mvc;
using FnacDirect.OrderPipe.Base.Business.Operations;
using System;

namespace FnacDirect.OrderPipe.Base.Business.Availability
{
    public class AvailabilityItemInfo
    {
        public int Id { get; set; }
        public Guid? OfferRef { get; set; }
        public string Title { get; set; }
        public int MethodId { get; set; }
        public DateTime? EstimatedDeliveryEndDate { get; set; }
        public DateTime? EstimatedDeliveryStartDate { get; set; }
    }
    public class PopAvailabilityProcessorOutput
    {
        private readonly string _key;
        private readonly IEnumerable<AvailabilityItemInfo> _items;
        private readonly bool _isAppointment;
        private int _methodId { get; set; }
        public string Key
        {
            get { return _key; }
        }

        public IEnumerable<AvailabilityItemInfo> Items
        {
            get
            {
                return _items;
            }
        }

        public bool IsAppointment
        {
            get
            {
                return _methodId == ShippingMethods.Fly5Hours;
            }
        }

        public PopAvailabilityProcessorOutput(string key, IEnumerable<AvailabilityItemInfo> items, bool beforeXmas, bool clickAndCollect = false)
        {
            _key = key;
            _items = items;
            _methodId = items.Select(i => i.MethodId).FirstOrDefault();
            _isAppointment = IsAppointment;
            BeforeXmas = beforeXmas;
            ClickAndCollect = clickAndCollect;
        }

        public bool BeforeXmas { get; set; }

        public bool ClickAndCollect { get; set; }

        public string PictoLabel
        {
            get
            {
                if (BeforeXmas)
                {
                    var deliveredForChristmasService = ServiceLocator.Current.GetInstance<IDeliveredForChristmasService>();
                    var applicationContext = ServiceLocator.Current.GetInstance<IApplicationContext>();
                    return applicationContext.GetMessage(deliveredForChristmasService.GetPictoUiresourceKey());
                }
                return string.Empty;
            }
        }
    }

    public class PopAvailabilityProcessorOutput<T> where T : class
    {
        private readonly string _key;
        private readonly IEnumerable<T> _items;

        public string Key
        {
            get { return _key; }
        }

        public IEnumerable<T> Items
        {
            get
            {
                return _items;
            }
        }

        public PopAvailabilityProcessorOutput(string key, IEnumerable<T> items)
        {
            _key = key;
            _items = items;
        }
    }
}
