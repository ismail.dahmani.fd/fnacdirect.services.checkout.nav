using FnacDirect.OrderPipe.Base.Model;
using System.Collections.Generic;

namespace FnacDirect.OrderPipe.Base.Business.Payment
{
    public interface ICheckArticleService
    {
        bool HasSensibleSupportId(IEnumerable<Article> articles);
        bool HasECCV(IEnumerable<Article> articles);
        IEnumerable<Article> GetECCVs(IEnumerable<Article> articles);
        bool HasDonation(IEnumerable<Article> articles);
        bool IsECCV(Article article);
    }
}
