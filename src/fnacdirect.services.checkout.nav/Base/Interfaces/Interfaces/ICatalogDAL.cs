using FnacDirect.Contracts.Online.Model;
using FnacDirect.OrderPipe.Base.Model;
using System.Collections.Generic;

namespace FnacDirect.OrderPipe.Base.DAL
{
    public interface ICatalogDAL
    {
        bool GetRetailerMarketCountry(int countryId, int marketId, int retailerId);
        string GetFDV_WEEETaxBySKU(string sku);
        VatInfoList GetVATRates(int marketId);
        bool ValidateCUUConditions(string artlist, string conditionList, decimal totalPrice, decimal shippingPrice, out decimal basketAmount, int marketId);
        void GetNumericalAddOn(StandardArticle art, SiteContext siteContext);
        ItemInfosResult GetItemInfos(int productId);        
        IDictionary<int, Dictionary<int, decimal>> GetVolumesData();
    }
}
