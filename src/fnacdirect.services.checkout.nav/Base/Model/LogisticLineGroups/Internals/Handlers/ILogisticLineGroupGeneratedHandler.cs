﻿using FnacDirect.OrderPipe.Base.LineGroups;
using System.Collections.Generic;

namespace FnacDirect.OrderPipe.Base.Business.LogisticLineGroups.Internals.Handlers
{
    public interface ILogisticLineGroupGeneratedHandler
    {
        void OnGenerated(IEnumerable<ILineGroupHeader> source, IEnumerable<ILogisticLineGroup> logisticLineGroups);
    }
}
