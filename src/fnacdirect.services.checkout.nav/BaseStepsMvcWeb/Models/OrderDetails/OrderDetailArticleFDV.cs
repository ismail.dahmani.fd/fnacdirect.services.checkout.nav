using FnacDirect.OrderPipe.Base.BaseModel.Model.OrderDetails;
using System.Collections.Generic;

namespace FnacDirect.OrderPipe.Base.Model.OrderDetails
{
    public class OrderDetailArticleLightFDV : OrderDetailArticleLight
    {
        public int Flux { get; set; }

        public string Price { get; set; }

        public decimal ShippingPrice { get; set; }

        public string Subtitle { get; set; }

        public string AdditionalInfos { get; set; }

        public string LldContractNumber { get; set; }

        public string SubscriptionNumber { get; set; }

        public List<string> GPContractNumbers { get; set; }

        public string ReferenceGu { get; set; }

        public int? TypeId { get; set; }

        public int? OrderDetailPk { get; set; }
    }
}
