using Common.Logging;
using FnacDirect.OrderPipe.Base.Model;
using System;

namespace FnacDirect.OrderPipe.Base.Business.Payment
{
    public class DeferredPaymentBusiness : IDeferredPaymentBusiness
    {
        private readonly IDeferredPaymentService _service;
        private readonly ISwitchProvider _switchPovider;
        private readonly ILog _log;

        public DeferredPaymentBusiness(IDeferredPaymentService service, ISwitchProvider switchPovider, ILog log)
        {
            _service = service;
            _switchPovider = switchPovider;
            _log = log;
        }

        public DeferredPaymentEligibility GetDeferredPaymentEligibility(int accountId, decimal orderTotalPrice, bool fromCallCenter)
        {
            var eligibility = GetDeferredPaymentEligibility(accountId, orderTotalPrice);
            OverrideEligibilityForCallCenter(eligibility, fromCallCenter);
            return eligibility;
        }

        /// <summary>
        /// Appelle le web service pour récupérer l'éligibilité du client.
        /// </summary>
        private DeferredPaymentEligibility GetDeferredPaymentEligibility(int accountId, decimal orderTotalPrice)
        {
            var eligibility = new DeferredPaymentEligibility();
            if (_switchPovider.IsEnabled("orderpipe.pop.payment.deferred"))
            {
                try
                {
                    eligibility = _service.GetDeferredPaymentEligibility(accountId, orderTotalPrice);
                }
                catch (Exception ex)
                {
                    _log.Error("Error while calling DeferredPaymentWS", ex);
                }
            }
            return eligibility;

        }

        /// <summary>
        /// Gère l'éligibilité du call center.
        /// </summary>
        private void OverrideEligibilityForCallCenter(DeferredPaymentEligibility eligibility, bool fromCallCenter)
        {
            if (!eligibility.IsEligible &&
                eligibility.AuthorizationType == DeferredPaymentAuthorizationType.AcceptCallCenter &&
                fromCallCenter)
            {
                // Le client n'est pas éligible au paiement différé,
                // mais le call center peut accéder à ce moyen de paiement à sa place
                eligibility.IsEligible = true;

                // TODO: Gérer l'application Devis / Quote comme le faisait l'ancienne step (GetDeferredPaymentData) ?
                // À confirmer lors de la refonte de l'application.
            }
        }
    }
}
