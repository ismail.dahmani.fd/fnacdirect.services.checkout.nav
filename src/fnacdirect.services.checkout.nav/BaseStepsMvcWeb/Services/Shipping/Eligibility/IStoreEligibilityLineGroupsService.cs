﻿using FnacDirect.OrderPipe.Base.BaseModel;
using FnacDirect.OrderPipe.Base.Model.LineGroups;
using System;
using System.Collections.Generic;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Services.ShipToStore
{
    public interface IStoreEligibilityLineGroupsService
    {
        IEnumerable<FnacStore.Model.FnacStore> GetStoresEligibleForLineGroups(IEnumerable<FnacStore.Model.FnacStore> fnacStores,
                                                                              IEnumerable<ILineGroup> lineGroups,
                                                                              Maybe<ShopShippingMethodEvaluationItem> shippingMethodEvaluationItem);

    }
}
