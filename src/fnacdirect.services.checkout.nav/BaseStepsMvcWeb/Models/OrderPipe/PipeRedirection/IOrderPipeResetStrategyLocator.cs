﻿namespace FnacDirect.OrderPipe.CoreRouting
{
    public interface IOrderPipeResetStrategyLocator
    {
        IOrderPipeResetStrategy GetOrderPipePipeRedirectionStrategyFor(OPContext opContext);
    }
}
