using FnacDirect.OrderPipe.Base.Model;

namespace FnacDirect.OrderPipe.Base.Business.Payment
{
    public interface ICreditCardUsageBusiness
    {
        int? GetUsageFlags(OgoneCreditCardBillingMethod ogoneBM);
    }
}
