using Common.Logging;
using FnacDirect.OrderPipe.Base.Business.Helpers;
using FnacDirect.OrderPipe.Base.Business.LogisticLineGroups.Internals.Handlers;
using FnacDirect.OrderPipe.Base.LineGroups;
using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.OrderPipe.Base.Model.Solex;
using FnacDirect.Technical.Framework.ServiceLocation;
using StructureMap.Pipeline;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FnacDirect.OrderPipe.Base.Business.LogisticLineGroups.Handlers
{
    public class DispatchFnacShippingFeesEquallyProcessCompletedHandlerDescriptor : CompletedHandlerDescriptor<DispatchFnacShippingFeesEquallyProcessCompletedHandler>
    {
        private readonly IEnumerable<string> _sourceIds;
        private readonly IEnumerable<string> _destinationIds;

        public DispatchFnacShippingFeesEquallyProcessCompletedHandlerDescriptor(IEnumerable<string> sourceIds,
                                                                                IEnumerable<string> destinationIds)
        {
            _sourceIds = sourceIds;
            _destinationIds = destinationIds;
        }

        public override ExplicitArguments GetExplicitArguments()
        {
            var explicitArguments = new ExplicitArguments();

            explicitArguments.Set(new DispatchFnacShippingFeesEquallyProcessCompletedHandlerArguments(_sourceIds, _destinationIds));

            return explicitArguments;
        }

        public override int Priority
        {
            get
            {
                return 100;
            }
        }
    }

    public class DispatchFnacShippingFeesEquallyProcessCompletedHandlerArguments
    {
        private readonly IEnumerable<string> _sourceUniqueIds;
        private readonly IEnumerable<string> _destinationUniqueIds;

        public IEnumerable<string> SourceUniqueIds
        {
            get
            {
                return _sourceUniqueIds;
            }
        }

        public IEnumerable<string> DestinationUniqueIds
        {
            get
            {
                return _destinationUniqueIds;
            }
        }

        public DispatchFnacShippingFeesEquallyProcessCompletedHandlerArguments(IEnumerable<string> sourceUniqueIds,
                                                                                   IEnumerable<string> destinationUniqueIds)
        {
            _sourceUniqueIds = sourceUniqueIds;
            _destinationUniqueIds = destinationUniqueIds;
        }
    }

    public class DispatchFnacShippingFeesEquallyProcessCompletedHandler : ILogisticLineGroupProcessCompletedHandler
    {
        protected readonly IEnumerable<string> _sourceIds;
        protected readonly IEnumerable<string> _destinationIds;
        protected readonly IEnumerable<string> _allIds;

        protected readonly ISwitchProvider _switchProvider;

        private readonly ILog _logger;


        public DispatchFnacShippingFeesEquallyProcessCompletedHandler(DispatchFnacShippingFeesEquallyProcessCompletedHandlerArguments arguments)
        {
            _sourceIds = arguments.SourceUniqueIds;
            _destinationIds = arguments.DestinationUniqueIds;
            _allIds = _sourceIds.Union(_destinationIds).Distinct();
            _switchProvider = ServiceLocator.Current.GetInstance<ISwitchProvider>();
            _logger = ServiceLocator.Current.GetInstance<ILog>();
        }

        public void OnGenerated(ILogisticLineGroupGenerationProcessContext logisticLineGroupGenerationProcessContext, IEnumerable<ILogisticLineGroup> logisticLineGroups)
        {
            var shippingMethodEvaluation = logisticLineGroupGenerationProcessContext.IGotShippingMethodEvaluation.ShippingMethodEvaluation;

            var logisticTypesCount = new Dictionary<int, int>();

            var initialTotalGlobalShippingFees = 0m;
            var initialAmountFnacLineGroups = logisticLineGroupGenerationProcessContext.LineGroups
                                                                                       .Where(l => _sourceIds.Contains(l.UniqueId))
                                                                                       .Sum(l => l.GlobalPrices.TotalShoppingCartBusinessPriceUserEur.GetValueOrDefault());

            var fnacLineGroupLogTypesRemaining = new List<int>();

            foreach (var lineGroup in logisticLineGroupGenerationProcessContext.LineGroups.Where(l => _allIds.Contains(l.UniqueId)))
            {
                var maybeShippingMethodEvaluationGroup = shippingMethodEvaluation.GetShippingMethodEvaluationGroup(lineGroup.Seller.SellerId);

                if (maybeShippingMethodEvaluationGroup.HasValue)
                {
                    var shippingMethodEvaluationGroup = maybeShippingMethodEvaluationGroup.Value;

                    if (shippingMethodEvaluationGroup.SelectedShippingMethodEvaluationType != BaseModel.ShippingMethodEvaluationType.Unknown)
                    {
                        initialTotalGlobalShippingFees += shippingMethodEvaluationGroup.GetSelectedChoice().Groups.Sum(g => g.GlobalPrice.Cost);
                    }
                }

                var matchingLogisticLineGroup = logisticLineGroups.FirstOrDefault(l => l.UniqueId == lineGroup.UniqueId);

                if (matchingLogisticLineGroup != null)
                {
                    foreach (var article in matchingLogisticLineGroup.Articles)
                    {
                        if (!fnacLineGroupLogTypesRemaining.Contains(article.LogType.GetValueOrDefault()))
                        {
                            fnacLineGroupLogTypesRemaining.Add(article.LogType.GetValueOrDefault());
                        }
                    }
                }
            }

            var newLinegroupLogTypes = new List<int>();

            foreach (var logisticLineGroup in logisticLineGroups.Where(l => _destinationIds.Contains(l.UniqueId)))
            {
                foreach (var article in logisticLineGroup.Articles)
                {
                    if (!newLinegroupLogTypes.Contains(article.LogType.GetValueOrDefault()))
                    {
                        newLinegroupLogTypes.Add(article.LogType.GetValueOrDefault());
                    }
                    if (logisticLineGroup.IsClickAndCollect && !fnacLineGroupLogTypesRemaining.Contains(article.LogType.GetValueOrDefault()))
                    {
                        fnacLineGroupLogTypesRemaining.Add(article.LogType.GetValueOrDefault());
                    }
                }
            }

            if (initialTotalGlobalShippingFees > 0)
            {
                //on compte le nombre d'occurence d'un type logistique
                foreach (var type in newLinegroupLogTypes)
                {
                    if (fnacLineGroupLogTypesRemaining.Contains(type))
                        logisticTypesCount.Add(type, 2);
                    else
                        logisticTypesCount.Add(type, 1);
                }
                foreach (var type in fnacLineGroupLogTypesRemaining)
                {
                    if (!logisticTypesCount.ContainsKey(type))
                        logisticTypesCount.Add(type, 1);
                }

                if (_switchProvider.IsEnabled("patch.2hc.dispatch.shipping"))
                    DispatchShippingFees(logisticLineGroups, logisticTypesCount);
                else
                    DispatchShippingFees(logisticLineGroups, logisticTypesCount, initialAmountFnacLineGroups);
            }
        }

        protected virtual void DispatchShippingFees(IEnumerable<ILogisticLineGroup> logisticLineGroups, Dictionary<int, int> logisticTypesCount, decimal initialAmountFnacLineGroups)
        {
            //On fait la répartition des frais
            for (var i = 0; i < _allIds.Count(); i++)
            {
                var logisticLineGroup = logisticLineGroups.FirstOrDefault(l => l.UniqueId == _allIds.ElementAt(i));

                if (logisticLineGroup != null)
                {
                    //remise a zero du Total price pour le recalculer le bons apres le scindage par la suite.
                    logisticLineGroup.SelectedShippingChoice.TotalPrice.ResetCost();

                    var groupsToKeep = new List<Model.Solex.Group>();

                    //Parcours des groups pour lister les LogTypes.
                    foreach (var group in logisticLineGroup.SelectedShippingChoice.Groups)
                    {
                        //Si on a eu le scindage des Commandes Logistiques (Fnac+Offre locale) avec les memes LogType dans les deux commandes log.
                        if (logisticTypesCount.ContainsKey(group.Id) && logisticTypesCount[group.Id] > 1)
                        {
                            //Scindage des fais de port globaux.
                            var cost = group.GlobalPrice.Cost;

                            var totalShoppingCart = logisticLineGroup.GlobalPrices.TotalShoppingCartBusinessPriceUserEur.GetValueOrDefault();
                            var newMethodTotalUserCost = PriceHelpers.GetRoundedPrice(totalShoppingCart / initialAmountFnacLineGroups * cost);
                            group.GlobalPrice.Cost = newMethodTotalUserCost;

                            var costToApply = group.GlobalPrice.CostToApply ?? 0m;
                            var newCostToApply = PriceHelpers.GetRoundedPrice(totalShoppingCart / initialAmountFnacLineGroups * costToApply);
                            group.GlobalPrice.CostToApply = newCostToApply;

                            var noTaxCost = group.GlobalPrice.NoTaxCost ?? 0m;
                            var newMethodTotalUserNoTaxCost = PriceHelpers.GetRoundedPrice(totalShoppingCart / initialAmountFnacLineGroups * noTaxCost);
                            group.GlobalPrice.NoTaxCost = newMethodTotalUserNoTaxCost;
                        }
                        //Calcule des prix unitaires.
                        var totalUnitPrice = 0m;
                        decimal? totalToApplyUnitPrice = 0m;
                        decimal? totalNoTaxCostUnitPrice = 0m;

                        var anyItem = false;
                        foreach (var item in group.Items.Where(item => logisticLineGroup.Articles.Any(a => a.ProductID == item.Identifier.Prid)))
                        {
                            totalUnitPrice += item.UnitPrice.Cost * item.Quantity;
                            totalToApplyUnitPrice += item.UnitPrice.CostToApply * item.Quantity;
                            totalNoTaxCostUnitPrice += item.UnitPrice.NoTaxCost * item.Quantity;
                            anyItem = true;
                        }

                        if (anyItem)
                        {
                            //Recalcule du total.
                            logisticLineGroup.SelectedShippingChoice.TotalPrice.Cost += group.GlobalPrice.Cost + totalUnitPrice;
                            logisticLineGroup.SelectedShippingChoice.TotalPrice.CostToApply += group.GlobalPrice.CostToApply + totalToApplyUnitPrice;
                            logisticLineGroup.SelectedShippingChoice.TotalPrice.NoTaxCost += group.GlobalPrice.NoTaxCost + totalNoTaxCostUnitPrice;
                            groupsToKeep.Add(group);
                        }
                    }

                    logisticLineGroup.SelectedShippingChoice.Groups = groupsToKeep;

                    logisticLineGroup.GlobalPrices.TotalShipPriceUserEur = logisticLineGroup.SelectedShippingChoice.TotalPrice.Cost;
                }
            }
        }

        protected virtual void DispatchShippingFees(IEnumerable<ILogisticLineGroup> logisticLineGroups, Dictionary<int, int> logisticTypesCount)
        {
            var logisticLineGroupsOrdered = logisticLineGroups.Where(llg => _allIds.Contains(llg.UniqueId)).OrderByDescending(l => l.IsSteedFromStore == true);

            var lastSteedFromStoreLogisticLinegroup = logisticLineGroupsOrdered.Last(l => l.IsSteedFromStore);

            var steedFromStoreBasketTotal = logisticLineGroupsOrdered.Where(llg => llg.IsSteedFromStore == true).Sum(llg => llg.GlobalPrices.TotalShoppingCartBusinessPriceUserEur.GetValueOrDefault());


            //Find MAX globalPrice in every LLG of type steedFromStore
            var Max2HCShipping = logisticLineGroupsOrdered
                .Where(llg => llg.IsSteedFromStore)
                .SelectMany(llg => llg.SelectedShippingChoice.Groups.Select(g => g.GlobalPrice))
                .OrderByDescending(g => g.Cost)
                .First();

            var total2HCShippingPrice = new GenericPrice
            {
                Cost = Max2HCShipping.Cost,
                CostToApply = Max2HCShipping.CostToApply,
                NoTaxCost = Max2HCShipping.NoTaxCost
            };

            var Remaining2HCShippingPrice = new GenericPrice
            {
                Cost = Max2HCShipping.Cost,
                CostToApply = Max2HCShipping.CostToApply,
                NoTaxCost = Max2HCShipping.NoTaxCost
            };

            foreach (var logisticLineGroup in logisticLineGroupsOrdered)
            {
                if (logisticLineGroup == null)
                    continue;

                var groupsToKeep = new List<Group>();

                //remise a zero du Total price pour le recalculer le bons apres le scindage par la suite.
                logisticLineGroup.SelectedShippingChoice.TotalPrice.ResetCost();

                //Parcours des groups pour lister les LogTypes.
                foreach (var group in logisticLineGroup.SelectedShippingChoice.Groups)
                {

                    if (logisticTypesCount.ContainsKey(group.Id) && logisticTypesCount[group.Id] > 1)
                    {
                        //Si on a eu le scindage des Commandes Logistiques (Fnac+Offre locale) avec les memes LogType dans les deux commandes log.
                        //SteedFromStore logisticLineGroup, requires splitting
                        if (logisticLineGroup.IsSteedFromStore)
                        {
                            //Last steedFromStore linegroup : apply remaining amount to split instead of computing prorata
                            if (logisticLineGroup == lastSteedFromStoreLogisticLinegroup)
                            {
                                group.GlobalPrice.Cost = Remaining2HCShippingPrice.Cost;
                                group.GlobalPrice.CostToApply = Remaining2HCShippingPrice.CostToApply;
                                group.GlobalPrice.NoTaxCost = Remaining2HCShippingPrice.NoTaxCost;
                            }
                            //Compute prorata and reduce remainingAmount
                            else
                            {
                                var totalLogisticLineGroup = logisticLineGroup.GlobalPrices.TotalShoppingCartBusinessPriceUserEur.GetValueOrDefault();

                                //Scindage des fais de port globaux.
                                var newMethodTotalUserCost = PriceHelpers.GetRoundedPrice(totalLogisticLineGroup / steedFromStoreBasketTotal * total2HCShippingPrice.Cost);
                                group.GlobalPrice.Cost = newMethodTotalUserCost;
                                Remaining2HCShippingPrice.Cost -= newMethodTotalUserCost;

                                var newCostToApply = PriceHelpers.GetRoundedPrice(totalLogisticLineGroup / steedFromStoreBasketTotal * (total2HCShippingPrice.CostToApply ?? 0M));
                                group.GlobalPrice.CostToApply = newCostToApply;
                                Remaining2HCShippingPrice.CostToApply -= newCostToApply;

                                var newMethodTotalUserNoTaxCost = PriceHelpers.GetRoundedPrice(totalLogisticLineGroup / steedFromStoreBasketTotal * (total2HCShippingPrice.NoTaxCost ?? 0M));
                                group.GlobalPrice.NoTaxCost = newMethodTotalUserNoTaxCost;
                                Remaining2HCShippingPrice.NoTaxCost -= newMethodTotalUserNoTaxCost;
                            }
                        }
                        //Necessary for non-2HC lineGroups containing articles with a 2HC group. Happens with some articles not in shop stock anymore.
                        else
                        {
                            group.GlobalPrice.ResetCost();
                        }
                    }
                    try
                    {
                        //Log for trace PFNAC-556
                        if (Remaining2HCShippingPrice.Cost < 0 || Remaining2HCShippingPrice.CostToApply < 0 || Remaining2HCShippingPrice.NoTaxCost < 0 ||
                            group.GlobalPrice.Cost < 0 || group.GlobalPrice.CostToApply < 0 || group.GlobalPrice.NoTaxCost < 0)
                        {
                            var ex = new Exception();
                            ex.Data.Add("Shipping Select Method Id ", logisticLineGroup.SelectedShippingChoice?.MainShippingMethodId);
                            ex.Data.Add("Shipping Select Label", logisticLineGroup.SelectedShippingChoice.Label);
                            ex.Data.Add("Shipping is Steed From Store", logisticLineGroup.IsSteedFromStore);

                            ex.Data.Add("Max2HCShipping.Cost", Max2HCShipping?.Cost);
                            ex.Data.Add("Max2HCShipping.CostToApply", Max2HCShipping?.CostToApply);
                            ex.Data.Add("Max2HCShipping.NoTaxCost", Max2HCShipping?.NoTaxCost);

                            ex.Data.Add("Remaining2HCShippingPrice.Cost", Remaining2HCShippingPrice?.Cost);
                            ex.Data.Add("Remaining2HCShippingPrice.CostToApply", Remaining2HCShippingPrice?.CostToApply);
                            ex.Data.Add("Remaining2HCShippingPrice.NoTaxCost", Remaining2HCShippingPrice?.NoTaxCost);

                            ex.Data.Add("logisticLineGroup.isClickAndCollect", logisticLineGroup.IsClickAndCollect);
                            ex.Data.Add("logisticLineGroup.IsVoluminous", logisticLineGroup.IsVoluminous);
                            ex.Data.Add("Seller.DisplayName", logisticLineGroup.Seller?.DisplayName);

                            ex.Data.Add("ShippingMethodChoice.AppliedShippingPromotion", string.Join(",", logisticLineGroup.SelectedShippingChoice?.AppliedPromotions?.Select(p=>p.PromotionCode).ToList()));
                            ex.Data.Add("ShippingMethodChoice.AppliedShippingPromotion.TotalPrice.Cost", logisticLineGroup.SelectedShippingChoice?.TotalPrice?.Cost);
                            ex.Data.Add("ShippingMethodChoice.AppliedShippingPromotion.TotalPrice.CostToApply", logisticLineGroup.SelectedShippingChoice?.TotalPrice?.CostToApply);
                            ex.Data.Add("ShippingMethodChoice.AppliedShippingPromotion.TotalPrice.NoTaxCost", logisticLineGroup.SelectedShippingChoice?.TotalPrice?.NoTaxCost);

                            _logger.Warn($"DispatchFnacShippingFeesEquallyProcessCompletedHandler - Remaining2HCShippingPrice)", ex);
                        }
                    }
                    catch (Exception ex)
                    {
                        _logger.Warn($"DispatchFnacShippingFeesEquallyProcessCompletedHandler - Remaining2HCShippingPrice", ex);
                    }

                    //Calcule des prix unitaires.
                    var totalUnitPrice = 0m;
                    decimal? totalToApplyUnitPrice = 0m;
                    decimal? totalNoTaxCostUnitPrice = 0m;

                    var anyItem = false;
                    foreach (var item in group.Items.Where(item => logisticLineGroup.Articles.Any(a => a.ProductID == item.Identifier.Prid)))
                    {
                        totalUnitPrice += item.UnitPrice.Cost * item.Quantity;
                        totalToApplyUnitPrice += item.UnitPrice.CostToApply * item.Quantity;
                        totalNoTaxCostUnitPrice += item.UnitPrice.NoTaxCost * item.Quantity;
                        anyItem = true;
                    }

                    if (anyItem)
                    {
                        //Recalcule du total.
                        logisticLineGroup.SelectedShippingChoice.TotalPrice.Cost += group.GlobalPrice.Cost + totalUnitPrice;
                        logisticLineGroup.SelectedShippingChoice.TotalPrice.CostToApply += group.GlobalPrice.CostToApply + totalToApplyUnitPrice;
                        logisticLineGroup.SelectedShippingChoice.TotalPrice.NoTaxCost += group.GlobalPrice.NoTaxCost + totalNoTaxCostUnitPrice;
                        groupsToKeep.Add(group);

                        //Log for trace PFNAC-556
                        try
                        {
                            if (totalUnitPrice < 0 || totalToApplyUnitPrice < 0 || totalNoTaxCostUnitPrice < 0)
                            {
                                var ex = new Exception();
                                ex.Data.Add("totalUnitPrice", totalUnitPrice);
                                ex.Data.Add("totalToApplyUnitPrice", totalUnitPrice);
                                ex.Data.Add("totalNoTaxCostUnitPrice", totalNoTaxCostUnitPrice);

                                _logger.Warn($"DispatchFnacShippingFeesEquallyProcessCompletedHandler - Recalcule Total", ex);
                            }
                        }
                        catch (Exception ex)
                        {
                            _logger.Warn($"DispatchFnacShippingFeesEquallyProcessCompletedHandler - Recalcule Total", ex);
                        }
                    }
                }

                logisticLineGroup.SelectedShippingChoice.Groups = groupsToKeep;

                logisticLineGroup.GlobalPrices.TotalShipPriceUserEur = logisticLineGroup.SelectedShippingChoice.TotalPrice.Cost;
            }

        }
    }
}
