﻿using FnacDirect.Appointment.Business;
using FnacDirect.Appointment.Model;
using FnacDirect.Technical.Framework.Utils;

namespace FnacDirect.OrderPipe.Base.Proxy.Appointment
{
    public class FormAtHomeService :IFormAtHomeService
    {
        public RangeSet<int> GetRelatedArticleTypeIds()
        {
            return FormAtHomeManager.Instance.RelatedArticleTypeIds;
        }

        public void ConfirmSales(AppointmentRequest<FormAtHomeAppointment> appointmentRequest, string user)
        {
            FormAtHomeManager.Instance.ConfirmSales(appointmentRequest, new ApplicativeUser { FnacUserId = AppointmentConfigurationManager.Instance.GetApplicationUserId(user) });
        }
    }
}
