using FnacDirect.OrderPipe.Config;
using FnacDirect.Context;
using StructureMap;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FnacDirect.OrderPipe.Context;

namespace FnacDirect.OrderPipe.CoreRouting.UiDescriptorRenderer
{
    public class StructureMapUiDescriptorRendererLocator : IUiDescriptorRendererLocator
    {
        private readonly IContainer _container;

        public StructureMapUiDescriptorRendererLocator(IContainer container)
        {
            _container = container;
        }

        public IUiDescriptorRenderer GetUiDescriptorRendererLocatorFor<TDescriptor>(TDescriptor descriptor) where TDescriptor : IUiDescriptor
        {
            var instanceType = descriptor.GetType();
            var serviceType = typeof(IUiDescriptorRenderer<>);
            var concreteServiceType = serviceType.MakeGenericType(instanceType);

            return _container.GetInstance(concreteServiceType) as IUiDescriptorRenderer;
        }
    }
}
