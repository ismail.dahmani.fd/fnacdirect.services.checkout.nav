using System.Globalization;
using System.Linq;
using System.Web.Mvc;
using FnacDirect.Context;
using FnacDirect.OrderPipe.Base.Business.Helpers;
using FnacDirect.OrderPipe.Base.Business.Payment;
using FnacDirect.OrderPipe.Base.Business.Shipping.Relay;
using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.OrderPipe.Base.Model.Pop;
using FnacDirect.OrderPipe.Base.Steps.Pop;
using FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Common.Payment;
using FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Common.Shipping;
using FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.DataLayer;
using FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Omniture;
using FnacDirect.OrderPipe.BaseMvc.Web.Pop.Models.OnePage;
using FnacDirect.OrderPipe.BaseMvc.Web.Services.Pop.DataLayer;
using FnacDirect.Technical.Framework.Web;
using FnacDirect.Technical.Framework.Web.Mvc;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Controllers.Pop
{
    public class PopOnePageController : PopPaymentBaseController
    {
        private readonly IApplicationContext _applicationContext;
        private readonly OPContext _opContext;
        private readonly IShippingViewModelBuilder _shippingViewModelBuilder;
        private readonly IRelayCellphoneService _relayCellphoneService;
        private readonly IExecutionContextProvider _executionContextProvider;
        private readonly ISwitchProvider _switchProvider;
        private readonly IOnePageRootViewModelBuilder _onePageRootViewModelBuilder;
        private readonly IOmnitureViewModelBuilder _omnitureViewModelBuilder;
        private readonly IDataLayerService _dataLayerService;

        public PopOnePageController(OPContext opContext,
            IShippingViewModelBuilder shippingViewModelBuilder,
            IPaymentViewModelBuilder paymentViewModelBuilder,
            IPaymentSelectionBusiness paymentSelectionBusiness,
            IRelayCellphoneService relayCellphoneService,
            IUrlManager urlManager,
            IExecutionContextProvider executionContextProvider,
            ISwitchProvider switchProvider,
            IOnePageRootViewModelBuilder onePageRootViewModelBuilder,
            IOmnitureViewModelBuilder omnitureViewModelBuilder,
            IApplicationContext applicationContext,
            IDataLayerService dataLayerService
            )
            : base(paymentSelectionBusiness, urlManager, paymentViewModelBuilder, opContext)
        {
            _opContext = opContext;
            _shippingViewModelBuilder = shippingViewModelBuilder;
            _relayCellphoneService = relayCellphoneService;
            _executionContextProvider = executionContextProvider;
            _switchProvider = switchProvider;
            _onePageRootViewModelBuilder = onePageRootViewModelBuilder;
            _omnitureViewModelBuilder = omnitureViewModelBuilder;
            _applicationContext = applicationContext;
            _dataLayerService = dataLayerService;
        }

        [HttpGet]
        public ActionResult Index()
        {
            var popOrderForm = _opContext.OF as PopOrderForm;

            var rootViewModel = _onePageRootViewModelBuilder.GetRootViewModel(_opContext, popOrderForm, (a, b) => Url.RouteUrl(a, b));

            var popData = popOrderForm.GetPrivateData<PopData>(create: false);

            if (IsReacJSMode(popData))
            {
                var basketAmount = string.Empty;
                if (rootViewModel.Payment.HasValue)
                    basketAmount = rootViewModel.Payment.Value.Summary.TotalGlobal;

                // TODO remove it after DataLayer is definitively used:
                var omnitureBasketViewModel = _omnitureViewModelBuilder.BuildOnePage(popOrderForm);
                rootViewModel.Omniture = omnitureBasketViewModel;
                rootViewModel.Tracking = _omnitureViewModelBuilder.BuildTracking(popOrderForm, TrackedPage.popOnePage, basketAmount);
                // TODO end

                var trackingModel = new DatalayerViewModel()
                {
                    PageType = popOrderForm.IsOneClick() ? PageTypeEnum.PopOneClick : PageTypeEnum.PopOnePage,
                    PopOrderForm = popOrderForm,
                };
                var viewResult = View("_Poc/Index", rootViewModel);
                //Datalayer and Tracking switchabled
                _dataLayerService.ComputeShoppingCart(trackingModel, viewResult as ViewResultBase, ControllerContext);

                return viewResult;
            }

            return View(rootViewModel);
        }

        private bool IsReacJSMode(PopData popData)
        {
            return popData != null && popData.UseReact;
        }

        private int ComputeSellerId(PopOrderForm popOrderForm)
        {
            var shippingMethodEvaluation = popOrderForm.ShippingMethodEvaluation;
            var mainShippingMethodEvaluationGroup = shippingMethodEvaluation.GetMainShippingMethodEvaluationGroup();
            return mainShippingMethodEvaluationGroup.SellerId;
        }

        /// <summary>
        /// Attention! La validation du paiment (form de la vue) avec Ogone AliasGateway ne passe par cette action, le post est directement fait chez Ogone, sans passer par le server Fnaccom
        /// </summary>
        /// <param name="paymentMethodId"></param>
        /// <param name="relayCellphone"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Index(int? paymentMethodId, string relayCellphone, string pinf)
        {
            var popOrderForm = _opContext.OF as PopOrderForm;

            if (!string.IsNullOrEmpty(relayCellphone) && !_relayCellphoneService.SetRelayCellPhone(relayCellphone, popOrderForm))
            {
                var rootViewModel = _onePageRootViewModelBuilder.GetRootViewModel(_opContext, popOrderForm, (a, b) => Url.RouteUrl(a, b));

                if (rootViewModel.Shipping.Relay.HasValue && rootViewModel.Shipping.Relay.Value.Selected.HasValue)
                {
                    rootViewModel.Shipping.Relay.Value.Selected.Value.CellPhoneNumber = relayCellphone;
                }

                return View("Index", rootViewModel);
            }

            var sellerId = ComputeSellerId(popOrderForm);

            var isValid = _shippingViewModelBuilder.IsValid(popOrderForm, sellerId);

            if (!isValid)
            {
                var rootViewModel = _onePageRootViewModelBuilder.GetRootViewModel(_opContext, popOrderForm, (a, b) => Url.RouteUrl(a, b));

                return View("Index", rootViewModel);
            }

            var onePageData = popOrderForm.GetPrivateData<PopData>();

            if (PaymentValidation(paymentMethodId, _opContext))
            {
                onePageData.DisplayOnePageValidated = true;
                // Récupération des informations pour le scoring BATS
                var headers = _executionContextProvider.GetCurrentExecutionContext().GetHeaders();
                ExperianHelper.SetExperianData(_switchProvider, popOrderForm, headers, pinf);

                return Continue();
            }
            else
            {
                var rootViewModel = _onePageRootViewModelBuilder.GetRootViewModel(_opContext, popOrderForm, (a, b) => Url.RouteUrl(a, b));

                if (rootViewModel.Payment.HasValue)
                {
                    var paymentViewModel = rootViewModel.Payment.Value;

                    var tabSelected = paymentViewModel.Tabs.FirstOrDefault(t => t.BillingMethods.Any(b => b.Id == paymentMethodId));

                    if (tabSelected != null)
                    {
                        foreach (var tab in paymentViewModel.Tabs)
                        {
                            tab.IsSelected = tab == tabSelected;
                        }
                    }
                }

                var popData = popOrderForm.GetPrivateData<PopData>(create: false);

                if (IsReacJSMode(popData))
                {
                    var omnitureBasketViewModel = _omnitureViewModelBuilder.BuildOnePage(popOrderForm);
                    rootViewModel.Omniture = omnitureBasketViewModel;

                    return View("_Poc/Index", rootViewModel);
                }

                return View("Index", rootViewModel);
            }
        }
    }
}
