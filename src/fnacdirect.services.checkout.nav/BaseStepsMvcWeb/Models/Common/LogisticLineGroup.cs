using System.Collections.Generic;
using FnacDirect.OrderPipe.Base.Model;
using System;
using FnacDirect.OrderPipe.Shipping.Model;
using System.Xml.Serialization;
using FnacDirect.OrderPipe.Base.Model.Solex;

namespace FnacDirect.OrderPipe.Base.LineGroups
{
    public class LogisticLineGroup : ILogisticLineGroup
    {
        private readonly IDictionary<int, decimal> _billingMethodConsolidation;

        public LogisticLineGroup()
        {
            Articles = new List<Article>();
            Informations = new PopLineGroupInformations();
            UniqueId = Guid.NewGuid().ToString();
            Seller = new SellerInformation();
            VatCountry = new VatCountry();
            GlobalPrices = new GlobalPrices();
            LogisticTypes = new LogisticTypeList();
            _billingMethodConsolidation = new Dictionary<int, decimal>();
        }

        [XmlIgnore]
        public ILineGroupInformations Informations { get; set; }

        public List<Article> Articles { get; set; }

        public OrderTypeEnum OrderType { get; set; }

        [XmlIgnore]
        public int OrderOrigin { get; set; }

        public string UniqueId { get; set; }

        [XmlIgnore]
        public SellerInformation Seller { get; set; }

        [XmlIgnore]
        public VatCountry VatCountry { get; set; }

        [XmlIgnore]
        public GlobalPrices GlobalPrices { get; set; }

        [XmlIgnore]
        public LogisticTypeList LogisticTypes { get; set; }

        [XmlIgnore]
        public bool IsVoluminous { get; set; }

        [XmlIgnore]
        public decimal RemainingTotal { get; set; }

        [XmlIgnore]
        public ShippingAddress ShippingAddress { get; set; }

        [XmlIgnore]
        public ShippingMethodResult ShippingMethodChoice { get; set; }

        [XmlIgnore]
        public ShippingChoice SelectedShippingChoice { get; set; }
        
        [XmlIgnore]
        public WrapList AvailableWraps { get; set; }
        
        public IDictionary<int, decimal> BillingMethodConsolidation
        {
            get
            {
                return _billingMethodConsolidation;
            }
        }

        public bool IsFnacCom
        {
            get
            {
                return OrderType == OrderTypeEnum.FnacCom || OrderType == OrderTypeEnum.Pro;
            }
        }

        public bool IsMarketPlace
        {
            get
            {
                return OrderType == OrderTypeEnum.MarketPlace;
            }
        }

        public bool IsClickAndCollect
        {
            get
            {
                return OrderType == OrderTypeEnum.CollectInStore;
            }
        }

        public bool IsIntraMag
        {
            get
            {
                return OrderType == OrderTypeEnum.IntraMag;
            }
        }

        public bool IsIntramagPE
        {
            get
            {
                return OrderType == OrderTypeEnum.IntraMagPE;
            }
        }

        public bool IsNumerical
        {
            get
            {
                return OrderType == OrderTypeEnum.Numerical;
            }
        }

        public bool IsSteedFromStore { get; set; }
        
        public int? OrderPk { get; set; }
        public string OrderUserReference { get; set; }
        public Guid OrderReference { get; set; }
        public string GUPTReference { get; set; }
        public int GUPTTransactionId { get; set; }
        public DateTime OrderDate { get; set; }
    }
}
