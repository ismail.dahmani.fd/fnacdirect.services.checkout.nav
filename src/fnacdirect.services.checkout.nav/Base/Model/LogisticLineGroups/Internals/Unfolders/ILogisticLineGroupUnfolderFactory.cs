﻿namespace FnacDirect.OrderPipe.Base.Business.LogisticLineGroups.Internals.Unfolders
{
    public interface ILogisticLineGroupUnfolderFactory
    {
        ILogisticLineGroupUnfolder Get(ILogisticLineGroupGenerationProcessContext context);
    }

    public interface ILogisticLineGroupUnfolderFactory<out T> : ILogisticLineGroupUnfolderFactory
            where T : BaseLogisticLineGroupUnfolder
    {
    }
}
