using FnacDirect.Customer.IModel;
using FnacDirect.Customer.Model;
using FnacDirect.OrderPipe.Base.DAL;
using System;

namespace FnacDirect.OrderPipe.Base.Business
{
    public class PersonnelNumberBusiness : IPersonnelNumberBusiness
    {
        private readonly IShopCommerceDAL _commerceDal;
        private readonly IAttribut _attributBusiness;
        public PersonnelNumberBusiness(IShopCommerceDAL commerceDal,
                                        IAttribut attributBusiness)
        {
            _commerceDal = commerceDal;
            _attributBusiness = attributBusiness;
        }

        public void AddPersonnelNumber(int accountId, int orderId)
        {
            var attribute = _attributBusiness.Populate(accountId, (int)AttributeType.PersonnelNumber);

            var personnelNumber = attribute.AttributeValue;

            _commerceDal.AddOrderInfo(orderId, Constants.OrderInfoTypes.PersonnelNumber, personnelNumber);
        }
    }
}
