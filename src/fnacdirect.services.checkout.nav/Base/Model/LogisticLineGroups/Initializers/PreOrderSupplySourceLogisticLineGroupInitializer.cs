﻿using FnacDirect.OrderPipe.Base.Business.LogisticLineGroups.Handlers;
using FnacDirect.OrderPipe.Base.LineGroups;
using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.Technical.Framework.Web.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;

namespace FnacDirect.OrderPipe.Base.Business.LogisticLineGroups.Initializers
{
    public class PreOrderSupplySourceLogisticLineGroupInitializer : BaseSupplySourceLogisticLineGroupInitializer
    {
        public PreOrderSupplySourceLogisticLineGroupInitializer(IApplicationContext applicationContext, IMarketPlaceServiceProvider marketPlaceServiceProvider, ISwitchProvider switchProvider) 
            : base(applicationContext, marketPlaceServiceProvider, switchProvider)
        {
        }

        public override IEnumerable<ILogisticLineGroup> Initialize(IDictionary<ILineGroupHeader, IEnumerable<Article>> selectedLineGroups)
        {
            var output = new List<ILogisticLineGroup>();

            var sourceIds = new List<string>();
            var destinationIds = new List<string>();

            foreach (var lineGroup in selectedLineGroups.Keys)
            {
                sourceIds.Add(lineGroup.UniqueId);

                var marketPlaceArticles = new List<MarketPlaceArticle>();

                var articles = selectedLineGroups[lineGroup].OfType<StandardArticle>().ToList();

                foreach (var standardArticle in articles)
                {
                    var marketPlaceArticle = ConvertToMarketPlaceArticle(standardArticle);

                    if (marketPlaceArticle != null)
                    {
                        var seller = marketPlaceArticle.Offer.Seller;

                        var logisticLineGroup = new LogisticLineGroup()
                        {
                            OrderType = OrderTypeEnum.MarketPlace,
                            UniqueId = Guid.NewGuid().ToString(),
                            Seller = new SellerInformation()
                            {
                                SellerId = seller.SellerId,
                                DisplayName = seller.Company ?? string.Format("{0} {1}", seller.FirstName, seller.LastName)
                            },
                            VatCountry = lineGroup.VatCountry,
                            LogisticTypes = lineGroup.LogisticTypes,
                        };

                        logisticLineGroup.Informations.OrderType = (int)OrderInfoOrderTypeEnum.Marketplace;

                        logisticLineGroup.Articles.Add(marketPlaceArticle);

                        Helper.CopyShippingMethodEvaluation(lineGroup, logisticLineGroup, Context.IGotShippingMethodEvaluation.ShippingMethodEvaluation);

                        var newShippingMethodEvaluation = Context.IGotShippingMethodEvaluation.ShippingMethodEvaluation.WithSeller(seller.SellerId);
                        var marketplaceEvaluationGroup = newShippingMethodEvaluation.Seller(seller.SellerId).Value;
                        marketplaceEvaluationGroup.TransfertShippingMethodsFor(new Guid(lineGroup.UniqueId), new Guid(logisticLineGroup.UniqueId));
                        var shippingMethodType = Context.IGotShippingMethodEvaluation.ShippingMethodEvaluation.FnacCom.Value.SelectedShippingMethodEvaluationType;

                        if (shippingMethodType != BaseModel.ShippingMethodEvaluationType.Unknown)
                        {
                            var shippingMethodEvaluationItem = Context.IGotShippingMethodEvaluation.ShippingMethodEvaluation.FnacCom.Value.SelectedShippingMethodEvaluation;
                            var shippingAddress = shippingMethodEvaluationItem.ShippingAddress;
                            marketplaceEvaluationGroup.WithAddressIfNotSet(shippingAddress as dynamic);
                            marketplaceEvaluationGroup.SelectShippingMethodEvaluationType(shippingMethodType);
                            marketplaceEvaluationGroup.SelectedShippingMethodEvaluation.WithResultsForLineGroup(logisticLineGroup.UniqueId, shippingMethodEvaluationItem.GetShippingMethodResultsFor(lineGroup.UniqueId));
                        }

                        Context.IGotShippingMethodEvaluation.ShippingMethodEvaluation = newShippingMethodEvaluation;
                        destinationIds.Add(logisticLineGroup.UniqueId);
                        output.Add(logisticLineGroup);
                    }
                }
            }

            if (output.Any())
            {
                if (_switchProvider.IsEnabled("orderpipe.pop.supplysource.roundshippingfees"))
                {
                    Context.AddProcessCompletedHandler(new DispatchFnacShippingFeesAndRoundProcessCompletedHandlerDescriptor(sourceIds.Distinct(), destinationIds.Distinct()));
                }
                else
                {
                    Context.AddProcessCompletedHandler(new DispatchFnacShippingFeesEquallyProcessCompletedHandlerDescriptor(sourceIds.Distinct(), destinationIds.Distinct()));
                }
            }

            return output;
        }
    }
}
