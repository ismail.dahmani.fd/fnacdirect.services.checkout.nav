﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Common.Payment
{
    public class GiftCardNumber
    {
        public string Number { get; set; }

        public string Code { get; set; }

        public decimal NewAmount { get; set; }
    }
}
