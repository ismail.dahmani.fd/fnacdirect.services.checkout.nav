﻿namespace FnacDirect.OrderPipe.Base.BaseDAL
{
    public interface ISubscriptionCommerceDal
    {
        /// <summary>
        /// Ajout les informations sur toutes les souscriptions active du client.
        /// </summary>
        /// <param name="orderId"></param>
        /// <param name="subscriptionType"></param>
        /// <param name="isTrial"></param>
        void AddOrderAccountSubscription(int orderId, int subscriptionType, bool isTrial);
    }
}
