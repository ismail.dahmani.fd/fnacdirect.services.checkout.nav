using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Basket
{
    public class QuantityViewModel
    {
        public int Count { get; set; }
        public int Max { get; set; }
        public bool HasMax { get; set; }
        public int DefaultMax { get; set; }

        [XmlIgnore]
        public bool ClickAndCollectEnabled { private get; set; }

        public bool ClickAndCollect { get
            {
                return ClickAndCollectEnabled
                    && Count <= MaxClickAndCollect;
            }
        }

        public int MaxClickAndCollect { get; set; }
    }
}
