﻿namespace FnacDirect.OrderPipe.Core.Steps
{
    /// <summary>
    /// Step de base pour les étapes les plus simples.
    /// Utilisable par les steps n'ayant pas a manipuler l'order form par exemple.
    /// </summary>
    public abstract class BasicStep : Step<OF,NullStepData,NullGroupData>
    {
    }
}
