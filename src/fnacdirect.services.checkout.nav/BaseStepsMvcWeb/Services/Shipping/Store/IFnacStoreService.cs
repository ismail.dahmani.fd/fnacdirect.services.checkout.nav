using System.Collections.Generic;

namespace FnacDirect.OrderPipe.Base.Proxy.Store
{
    public interface IFnacStoreService
    {
        FnacStore.Model.FnacStore GetStore(int storeId);
        IEnumerable<FnacStore.Model.FnacStore> GetFnacStores();
        IEnumerable<FnacStore.Model.FnacStore> GetAllStores();
        IEnumerable<FnacStore.Model.FnacStore> GetAllStoresWithDeliveryOrClickAndCollect();
        IEnumerable<FnacStore.Model.FnacStore> GetStoresByDepartement(int departement);
        string GetLayoutUrlBySpaceId(int spaceId);
    }
}
