using System.Collections.Generic;

namespace FnacDirect.OrderPipe.Base.BaseModel.Model.Quote
{
    public class AppliedPromotion
    {
        public string TriggerKey { get; set; }
        public int Quantity { get; set; }
        public string PromotionCode { get; set; }
        public List<int> ApplyToLogisticTypes { get; set; } = new List<int>();
        public string AdditionalRuleParam1 { get; set; }
        public string AdditionalRuleParam2 { get; set; }

        public AppliedPromotion()
        {

        }

        public AppliedPromotion(Solex.Shipping.Client.Contract.Promotion promotion)
        {
            TriggerKey = promotion.TriggerKey;
            Quantity = promotion.Quantity;
            PromotionCode = promotion.PromotionCode;
            ApplyToLogisticTypes = promotion.ApplyToLogisticTypes;
            AdditionalRuleParam1 = promotion.AdditionalRuleParam1;
            AdditionalRuleParam2 = promotion.AdditionalRuleParam2;
        }
    }
}
