﻿using FnacDirect.OrderPipe.Base.Model.OrderInformations;

namespace FnacDirect.OrderPipe.Base.Model.Facets
{
    public interface IGotMobileInformations : IOF
    {
        IOrderMobileInformations OrderMobileInformations { get; }
    }
}
