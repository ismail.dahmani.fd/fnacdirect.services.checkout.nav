﻿using FnacDirect;
using System.Collections.Generic;

namespace FnacDirect.OrderPipe
{
    /// <summary>
    /// Implémentation de base d'une étape
    /// </summary>
    public class GroupStep : Step
    {
        public ITransactionManager TransactionManager { get; set; }

        public bool UseTransaction { get; set; }

        private readonly List<IStep> _steps = new List<IStep>();
        /// <summary>
        /// Liste des étapes
        /// </summary>
        public IList<IStep> Steps
        {
            get
            {
                return _steps;
            }
        }

        /// <summary>
        /// Exécution d'une étape du pipe
        /// </summary>
        /// <param name="context">Contexte d'exécution</param>
        /// <param name="res">Résultat du pipe</param>
        /// <param name="force">Vrai si il faut forcer l'affichage de cette étape</param>
        /// <returns></returns>
        public override bool Run(OPContext context, PipeResult res, bool force)
        {
            if (UseTransaction)
                TransactionManager.Begin(context);

            var result = RunTransaction(context, res, force);

            if (UseTransaction)
            {
                TransactionManager.Commit(context);
            }

            return result;
        }

        public bool RunTransaction(OPContext context, PipeResult res, bool force)
        {
            using (new Stopwatch("Running " + Name))
            {
                var exit = false;

                // Phase préalable
                PreCalc(context);
                Stopwatch.Current.Result += "precalc ";

                // Suppression des contraintes positionnées par l'étape courante
                CollectionUtils.RemoveItemsFromCollection(context.OF.Constraints, delegate (Constraint c)
                {
                    return c.ConstrainerStepName == Name;
                });

                // Phase de vérification
                var ret = Check(context);
                Stopwatch.Current.Result += "check ";
                res.LastStepReturn = ret;

                // Sortie de pipe : rembobinage
                if (ret == StepReturn.Rewind)
                    exit = true;

                // Sortie de pipe : goto step
                if (ret is StepReturnForward)
                    exit = true;

                // Continuation avec exécution
                if (ret == StepReturn.Process)
                {
                    BeforeProcess(context);
                    Stopwatch.Current.Result += "beforeProcess ";

                    foreach (Step s in Steps)
                    {
                        exit = s.Run(context, res, force);

                        Stopwatch.Current.Result += "runningChild ";

                        // Sortie de pipe
                        if (exit)
                            break;
                    }

                    AfterProcess(context);
                    Stopwatch.Current.Result += "afterProcess ";
                }

                return exit;
            }
        }

        public virtual void BeforeProcess(OPContext context) { }
        public virtual void AfterProcess(OPContext context) { }

        public override void Init(IOP pipe, ISwitchProvider switchProvider)
        {
            base.Init(pipe, switchProvider);

            foreach (var s in Steps)
            {
                s.Init(pipe, switchProvider);
            }
        }
    }
}
