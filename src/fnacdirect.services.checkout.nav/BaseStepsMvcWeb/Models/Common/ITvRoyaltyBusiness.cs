using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.Technical.Framework.Utils;
using FnacDirect.TVRoyaltiesAddressNormalization;
using System.Collections.Generic;

namespace FnacDirect.OrderPipe.Base.Business
{
    public interface ITvRoyaltyBusiness
    {
        Dictionary<int, string> GetTelevisualRoyaltyWays(string defaultLabel);
        void AddTVRoyalty(int orderFk, int accountFk, string company, string firstName, string lastName, string addressLine1, string addressLine2, string addressLine3, string zipCode, string city, string state, string country, string tel, string cel, string fax, string dOB, string bLocation, int genderFk, string streetNo, string streetRange, string streetType, int tVUseFk, int tVUse2Fk, string bState, string mark, int quantity, int orderDetailFk);
        BillingAddress GetOrderBillingAddressTvRoyalty(string orderRef, out int errorCode, out int orderId, out int pridTv, out int quantity, out int orderDetailPk, out int accountId);
        NormalizerResult NormalizeAddress(string address);
        bool DoesArticleNeedTVRoyaltyForm(Article article, RangeSet<int> tvArticleTypesId);
        bool DoesArticleNeedTVRoyaltyOTY(Article article, RangeSet<int> tvArticleTypesId);
        bool DoesArticleNeedTVRoyalty(Article article, RangeSet<int> tvArticleTypesId);
    }
}
