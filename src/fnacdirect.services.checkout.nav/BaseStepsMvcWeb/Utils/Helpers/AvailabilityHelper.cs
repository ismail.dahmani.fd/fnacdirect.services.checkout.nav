﻿using System;
using System.Globalization;
using FnacDirect.Technical.Framework.Web.Mvc;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Helpers
{
    public class AvailabilityHelper
    {
        private readonly IApplicationContext _applicationContext;

        public AvailabilityHelper(IApplicationContext applicationContext)
        {
            _applicationContext = applicationContext;
        }

        public string Format(DateTime? dateMin)
        {
            return Format(dateMin, null, false);
        }

        public string FormatPreOrder(DateTime? dateMin)
        {
            return Format(dateMin, null, true);
        }

        public string Format(DateTime? dateMin, DateTime? dateMax)
        {
            return Format(dateMin, dateMax, false);
        }

        public string Format(DateTime? dateMin, DateTime? dateMax, bool preOrder)
        {
            if (!dateMin.HasValue)
                return string.Empty;

            if (preOrder)
                return string.Format(_applicationContext.GetMessage("orderpipe.index.shiptoaddress.preOrderArticleBis", null), dateMin.Value.ToString("dd/MM"));

            if (!dateMax.HasValue)
                return _applicationContext.GetMessage("orderpipe.index.shiptoaddress.deliveryFrom", null) + dateMin.Value.ToString("dd/MM", CultureInfo.InvariantCulture);

            if (dateMin == dateMax)
                return string.Format(_applicationContext.GetMessage("orderpipe.shiptostore.the", null), dateMin.Value.ToString("dd/MM", CultureInfo.InvariantCulture));

            return string.Format(_applicationContext.GetMessage("orderpipe.shiptostore.between", null), dateMin.Value.ToString("dd/MM", CultureInfo.InvariantCulture), dateMax.Value.ToString("dd/MM", CultureInfo.InvariantCulture));
        }
    }
}