using FnacDirect.Context;
using FnacDirect.Contracts.Online.Model;
using FnacDirect.Membership.Model.Constants;
using FnacDirect.OrderPipe.Base.Business;
using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.OrderPipe.Base.Model.Adherent;
using FnacDirect.OrderPipe.Base.Model.Pop;
using FnacDirect.OrderPipe.Base.Proxy.Membership;
using FnacDirect.OrderPipe.Base.Steps.Pop;
using FnacDirect.OrderPipe.BaseMvc.Web.ApiControllers.Pop;
using FnacDirect.OrderPipe.BaseMvc.Web.Models;
using FnacDirect.OrderPipe.BaseMvc.Web.Models.OrderPipe.Common.Scoring;
using FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Basket;
using FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Common.Basket;
using FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Common.Payment;
using FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Common.Shipping;
using FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Membership;
using FnacDirect.OrderPipe.BaseMvc.Web.Pop.Models.Common;
using FnacDirect.Technical.Framework.Web.Mvc;
using System;
using System.Linq;
using System.Web.Routing;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Pop.Models.OnePage
{
    public class OnePageRootViewModelBuilder : IOnePageRootViewModelBuilder
    {

        private readonly OPContext _opContext;
        private readonly Web.Models.Pop.OnePage.Basket.IBasketViewModelBuilder _basketViewModelBuilder;
        private readonly IShippingViewModelBuilder _shippingViewModelBuilder;
        private readonly IMembershipViewModelBuilder _membershipViewModelBuilder;
        private readonly IPaymentViewModelBuilder _paymentViewModelBuilder;
        private readonly IOptionsViewModelBuilder _optionsViewModelBuilder;
        private readonly IUserContextProvider _userContextProvider;
        private readonly ISummaryViewModelBuilder _summaryViewModelBuilder;
        private readonly IApplicationContext _applicationContext;
        private readonly ISwitchProvider _switchProvider;
        private readonly IPopBaseRootViewModelBuilder<Web.Models.Pop.OnePage.RootViewModel> _popBaseRootViewModelBuilder;
        private readonly ISellerInformationsViewModelBuilder _sellerInformationsViewModelBuilder;
        private readonly INavigationViewModelBuilder _navigationViewModelBuilder;
        private readonly IRemindersViewModelBuilder _remindersViewModelBuilder;
        private readonly IScoringViewModelBuilder _scoringViewModelBuilder;
        private readonly ITermsAndConditionsViewModelBuilder _termsAndConditionsViewModelBuilder;

        private readonly int _tryCardPrid;
        private readonly string navigationBackLinkUrlName = "DisplayBasket";

        public OnePageRootViewModelBuilder(OPContext opContext,
           Web.Models.Pop.OnePage.Basket.IBasketViewModelBuilder basketViewModelBuilder,
           IShippingViewModelBuilder shippingViewModelBuilder,
           IPaymentViewModelBuilder paymentViewModelBuilder,
           IOptionsViewModelBuilder optionsViewModelBuilder,
           ISummaryViewModelBuilder summaryViewModelBuilder,
           IMembershipViewModelBuilder membershipViewModelBuilder,
           IUserContextProvider userContextProvider,
           IApplicationContext applicationContext,
           ISwitchProvider switchProvider,
           ISellerInformationsViewModelBuilder sellerInformationsViewModelBuilder,
           IPopBaseRootViewModelBuilder<Web.Models.Pop.OnePage.RootViewModel> popBaseRootViewModelBuilder,
           INavigationViewModelBuilder navigationViewModelBuilder,
           IRemindersViewModelBuilder remindersViewModelBuilder,
           ITermsAndConditionsViewModelBuilder termsAndConditionsViewModelBuilder,
           IScoringViewModelBuilder scoringViewModelBuilder)
        {
            _userContextProvider = userContextProvider;
            _opContext = opContext;
            _basketViewModelBuilder = basketViewModelBuilder;
            _shippingViewModelBuilder = shippingViewModelBuilder;
            _paymentViewModelBuilder = paymentViewModelBuilder;
            _optionsViewModelBuilder = optionsViewModelBuilder;
            _summaryViewModelBuilder = summaryViewModelBuilder;
            _membershipViewModelBuilder = membershipViewModelBuilder;
            _applicationContext = applicationContext;
            _switchProvider = switchProvider;
            _popBaseRootViewModelBuilder = popBaseRootViewModelBuilder;
            _sellerInformationsViewModelBuilder = sellerInformationsViewModelBuilder;
            _navigationViewModelBuilder = navigationViewModelBuilder;
            _remindersViewModelBuilder = remindersViewModelBuilder;
            _termsAndConditionsViewModelBuilder = termsAndConditionsViewModelBuilder;
            _scoringViewModelBuilder = scoringViewModelBuilder;
        }

        public Web.Models.Pop.OnePage.RootViewModel GetRootViewModel(OPContext opContext, PopOrderForm popOrderForm, Func<string, RouteValueDictionary, string> getUrl)
        {
            var sellerId = ComputeSellerId(popOrderForm);

            var popShippingData = popOrderForm.GetPrivateData<PopShippingData>();

            var isGuest = popOrderForm.UserContextInformations.IsGuest;
            var hasAdherentNumber = !isGuest
                                    && (!string.IsNullOrEmpty(popOrderForm.UserContextInformations.CustomerEntity?.AdherentCardNumber)
                                    || popOrderForm.LineGroups.GetArticles().OfType<StandardArticle>().Any(a => a.IsAdhesionCard));

            var userContext = _userContextProvider.GetCurrentUserContext();

            var rootViewModel = _popBaseRootViewModelBuilder.Build(getUrl);

            rootViewModel.Seller = _sellerInformationsViewModelBuilder.Build(popOrderForm, sellerId);
            rootViewModel.PageName = PopApiEnvironments.OnePage;
            rootViewModel.Basket = _basketViewModelBuilder.Build(popOrderForm);
            rootViewModel.Shipping = _shippingViewModelBuilder.Build(popOrderForm, sellerId);
            rootViewModel.Membership = _membershipViewModelBuilder.Build(popOrderForm);
            rootViewModel.AdjustedMembershipCard = _membershipViewModelBuilder.BuildAdjustedCard(popOrderForm);
            rootViewModel.HasAdherentNumber = hasAdherentNumber;
            rootViewModel.Login = new LoginPopinViewModel
            {
                ContainsRegistrationForm = false,
                IsLogged = userContext.IsAuthenticated
        };
            rootViewModel.IsGuest = isGuest;
            rootViewModel.DisplayRelogin = !userContext.IsAuthenticated;

            rootViewModel.Payment = _paymentViewModelBuilder.Build(_opContext, popOrderForm);
            rootViewModel.Options = _optionsViewModelBuilder.Build(popOrderForm);

            if (rootViewModel.Shipping != null && rootViewModel.Shipping.IsValid)
            {
                var paymentViewModel = rootViewModel.Payment.Value;
                rootViewModel.Basket.PaymentSummaryViewModel = paymentViewModel.Summary;

                var selectedTab = paymentViewModel.Tabs.FirstOrDefault(t => t.IsSelected);
                if (selectedTab != null && selectedTab.BillingMethods
                                .OfType<CreditCardBillingMethodsViewModel>()
                                .Any(c => c.CustomerCreditCards.Any(cc => !cc.IsDumy)))
                {
                    rootViewModel.Basket.PaymentSummaryViewModel.IsPayButtonActive = true;
                }
            }
            else
            {
                var summary = _summaryViewModelBuilder.Build(_opContext, popOrderForm);
                rootViewModel.Basket.PaymentSummaryViewModel = summary;
            }

            if (rootViewModel.Shipping != null)
            {
                rootViewModel.Shipping.HasManyEligibleBillingAddresses = popShippingData.HasManyEligibleBillingAddresses;
            }

            if (rootViewModel.Basket.DisplayBirthDatePopin)
            {
                rootViewModel.MemberBirthDate = _membershipViewModelBuilder.BuildMemberBirthDateViewModel(_opContext, popOrderForm);
            }

            var hasNoFnacComShippableArticle = popOrderForm.HasNoFnacComShippableArticle(_applicationContext);
            if (popOrderForm.HasMarketPlaceArticle() && hasNoFnacComShippableArticle && rootViewModel.Shipping.ExpressPlusInfo != null)
            {
                rootViewModel.Shipping.ExpressPlusInfo.ShowInShipping = false;
            }

            rootViewModel.Navigation = _navigationViewModelBuilder.Build(popOrderForm, navigationBackLinkUrlName);

            rootViewModel.Reminders = _remindersViewModelBuilder.Build(popOrderForm);

            rootViewModel.TermsAndConditions = _termsAndConditionsViewModelBuilder.Build(popOrderForm.LineGroups);

            rootViewModel.Scoring = _scoringViewModelBuilder.Build();
            
            return rootViewModel;
        }

        private int ComputeSellerId(PopOrderForm popOrderForm)
        {
            var shippingMethodEvaluation = popOrderForm.ShippingMethodEvaluation;
            var mainShippingMethodEvaluationGroup = shippingMethodEvaluation.GetMainShippingMethodEvaluationGroup();
            return mainShippingMethodEvaluationGroup.SellerId;
        }
    }
}
