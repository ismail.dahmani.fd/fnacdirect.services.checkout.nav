using FnacDirect.OrderPipe.Base.Model;
using System;
using System.Collections.Generic;

namespace FnacDirect.OrderPipe.Base.DAL
{
    public interface IOrderInsertionDal
    {
        void AddComponentForBundle(int orderFk, int orderDetailFk, int bundleNber, string bundleEan13, int compNber, string compEan13, decimal priceDbFRF, decimal priceDbEUR, decimal priceUserFRF, decimal priceUserEUR, decimal priceToPayFRF, decimal priceToPayEUR, int quantity, int availability, int? status, string refGU, decimal ecoTaxEUR, int? universeId);

        void AddContact(int contactType, int orderFk);

        void AddGlobalPrice(int orderFk, int articleTypeLogisticFk, decimal dbPriceFRF, decimal dbPriceEUR, decimal userPriceFRF, decimal userPriceEUR, decimal? priceToPayFRF, decimal? priceToPayEUR, int paymentStatusFk, decimal? vATRate, string vatCode);

        void AddMainOrder(int mainOrderId, DateTime orderDateId, int accountId, bool modeBatch, string market, string mainOrderUserReference, Guid mainOrderReference, string pipeName, string contactEmail, int? secondsLapseTime = null, string pipeRoute = null, string sid = null);

        void AddPromo(int orderId, string codePromo, int orderDetailId, int typePromo, int? discountTypeId, int hasTreshold, int homogeneousCriteria, string typeBO, bool isPromoDiffered, decimal? amount = null);

        void AddOrder(int orderId, string orderUserReferenceId, DateTime orderDateId, int accountId, int? orderStatusId, int? orderCoherenceStatusId, string orderMessage, int isItAGift, string contactEmail, string affiliate, int useVat, int? vatCountryId, decimal totalPriceEUR, int orderShowStatusId, int orderTypeId,int orderInSession, int orderPaymentMethod, string webFarm, string worflowFoundationFarm, string iPAddress, string culture, string site, string catalogue, string clientCard, int? mainOrderId, int? sellerId, string externalOrderReference, int? origin, bool modeBatch, bool isVoluminous, int? orderIndex, string paymentRef, string bankAccountNumber, int masterOrder, bool fldOrderOCB, bool fldOrderOCBAuthenticate, bool fldOrderOCBCreditCardCVC, int? quoteId, Guid orderReference, string quoteReference);

        void AddOrderDetail(int orderDetailId, DateTime orderDate, int orderFk, int orderDetailTypeFk, int orderDetailQuantity, int articleNumber, string articleLabel, int articleTypeLogisticFk, string articleEan13, int delayArticle, int delayShipping, int shippingMethodFk, int wrapMethodFk, int shippingAddressFk, int paymentMethodFk, decimal priceDbEUR, decimal priceUserEUR, decimal priceToPayEUR, decimal shippingPriceDbEUR, decimal shippingPriceUserEUR, decimal shippingPriceToPayEUR, decimal wrapPriceDbEUR, decimal wrapPriceUserEUR, decimal wrapPriceToPayEUR, decimal vatRate, string affiliate, int? orderDetailStatusFk, int orderDetailPaymentStatusFk, int orderDetailDeliveryStatusFk, int shippingSplit, int availability, int isBundle, int? refGU, int epsilonFlag, int? insuranceFlag, decimal ecoTaxEUR, decimal priceRealEUR, int itemId, string referentiel, string vatCode, int? offerId, int typeart, int? masterOdetFk, int? mPProductStatus, string externalOrderDetailReference, DateTime? dtTheoExpDate, DateTime? dtTheoDelDate, DateTime? dtTheoDelDateMax, int? universeId, int? vatCountryId, string sku = null, int? pShopStockSource = null, bool? isOptinWarranty = null);

        void UpdateOrderInformations(int orderId, int accountId, bool resetOrderStatus, int orderPaymentMethod);

        void AddOrderDetailAltSellerInfo(int orderDetailFk, int? sellerTypeId, decimal? fnacPrice, int? availabilityId, decimal? bestOfferPrice, int? totalOfferCount);
        void AddWrapDetail(int wrapType, int orderId, string wrapText, int? orderDetailId = null);
        void AddOrderDetailInfo(int orderDetailFk, OrderDetailInfoType type, string template, string xmldata);
        void AddOrderDetailInfo(int orderDetailFk, int type, string template, string xmldata);

        void AddOrderDetailLink(int orderDetailFkMaster, int orderDetailFkChild);

        void AddOrderDetailParaTax(int orderDetailFk, string code, decimal amount, int paraTaxType, decimal? vatRate);
        void AddOrderDetailSogep(int orderDetailFk, int fDRelay, int foreignRelay, int companySogep);

        void AddOrderPaymentMethod(int orderId, int paymentMethodId, decimal amountEur, bool increase);

        int AddShippingAddress(int orderId, int accountId, string alias, string company, string firstName, string lastName, string addressLine1, string addressLine2, string addressLine3, string addressLine4, string zipCode, string city, string state, string countryId, string tel, string cellPhone, string fax, int network, int orderShippingAddressStatusId, int gender, string shopCodeGu);
        int AddShippingAddress(int orderId, int accountId, string alias, string company, int documentCountry, int documentType, string taxIdNumber, string firstName, string lastName, string addressLine1, string addressLine2, string addressLine3, string addressLine4, string zipCode, string city, string state, string countryId, string tel, string cellPhone, string fax, int network, int orderShippingAddressStatusId, int gender, string shopCodeGu);

        void AddTVRoyalty(int orderFk, int accountFk, string company, string firstName, string lastName, string addressLine1, string addressLine2, string addressLine3, string zipCode, string city, string state, string country, string tel, string cel, string fax, string dOB, string bLocation, int genderFk, string streetNo, string streetRange, string streetType, int tVUseFk, int tVUse2Fk, string bState, string mark, int quantity, int orderDetailFk);

        void AddWrapPrice(int orderFk, int articleTypeLogisticFk, decimal dbPriceFRF, decimal dbPriceEUR, decimal userPriceFRF, decimal userPriceEUR, decimal? priceToPayFRF, decimal? priceToPayEUR, int paymentStatusFk, decimal? vATRate, string vatCode);

        void InsertIntoOrderPiggyBack(int? orderId, string codePromo, int codeAsilage, int priority);

        void ConsumeAdvantageCode(int orderId, string advantageCode, int accountId);

        void AddOrderComments(int orderId, int commentsType, int fnacUserId, string commentaire);
        void AddDeviceInfo(int orderId, string devicePrinting, string headers, DateTime timeStamp, DateTime creationDate);

        int AddOrderAppointment(int orderId, int parcelId, string partnerId, string partnerUniqueId, DateTime startDate, DateTime endDate, string slotType);
        void AddOrderAppointmentProperties(int orderAppointmentId, Dictionary<string, string> parameters);

        int AddOrderParcel(int orderId, int shippingMethodId, string expeditionLocationType, string expeditionLocationCode);

        void AddOrderParcelDetails(int orderParcelId, int? orderDetailId);

        void AddDeferredPaymentDocument(int orderId, int userReferenceID);
    }
}
