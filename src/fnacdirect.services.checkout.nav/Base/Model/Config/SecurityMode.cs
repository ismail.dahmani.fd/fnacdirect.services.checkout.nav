namespace FnacDirect.OrderPipe.Config
{
	public enum SecurityMode
	{
		None,
		Secured,
		Authenticated,
		DontModify
	}
}