using Newtonsoft.Json.Linq;
using System.Threading.Tasks;

namespace FnacDirect.OrderPipe.Base.Proxy.Wishlist
{
    public interface IWishlistService
    {
        Task<int> GetCountAsideItems(string cookies);
        Task<JObject> GetWishlistAside(string cookies);
    }
}
