﻿using System.Web;
using System.Web.Routing;
using FnacDirect.OrderPipe.Config;

namespace FnacDirect.OrderPipe.CoreRouting
{
    public interface IOrderPipeInteractiveStepHandlerStrategy
    {
        IHttpHandler GetHttpHandlerFor(IUiDescriptor descriptor, OPContext opContext, RequestContext requestContext);
    }

    public interface IOrderPipeInteractiveStepHandlerStrategy<in TDescriptor> : IOrderPipeInteractiveStepHandlerStrategy
        where TDescriptor : IUiDescriptor
    {
    }
}
