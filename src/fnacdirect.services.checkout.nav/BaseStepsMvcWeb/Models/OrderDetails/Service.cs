﻿
namespace FnacDirect.OrderPipe.Base.Business.OrderDetail
{
    public class Service : BaseArticle
    {
        public string TypeId { get; set; }

        public string Debut { get; set; }

        public string CgvUrl { get; set; }
    }
}
