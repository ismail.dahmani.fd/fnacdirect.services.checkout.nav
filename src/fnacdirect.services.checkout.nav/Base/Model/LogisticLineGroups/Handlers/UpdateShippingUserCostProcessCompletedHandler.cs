using FnacDirect.OrderPipe.Base.Business.LogisticLineGroups.Internals.Handlers;
using FnacDirect.OrderPipe.Base.LineGroups;
using FnacDirect.WSOrderProcess.Models;
using System.Collections.Generic;

namespace FnacDirect.OrderPipe.Base.Business.LogisticLineGroups.Handlers
{
    public class UpdateShippingUserCostProcessCompletedHandler : ILogisticLineGroupGeneratedHandler
    {
        public void OnGenerated(IEnumerable<ILineGroupHeader> source, IEnumerable<ILogisticLineGroup> logisticLineGroups)
        {
            foreach (var logisticLineGroup in logisticLineGroups)
            {
                if (logisticLineGroup.SelectedShippingChoice == null
                || logisticLineGroup.SelectedShippingChoice.Groups == null)
                {
                    continue;
                }

                var totalShipping = 0m;
                decimal? totalShippingToApply = null;
                decimal? totalShippingNoTax = null;
                if (logisticLineGroup.OrderOrigin == (int)OrderOrigin.Batch_B2B)
                {
                    logisticLineGroup.GlobalPrices.TotalShipPriceUserEur = logisticLineGroup.SelectedShippingChoice.TotalPrice.Cost;
                }
                else
                {
                    foreach (var group in logisticLineGroup.SelectedShippingChoice.Groups)
                    {
                        foreach (var item in group.Items)
                        {
                            totalShipping += item.UnitPrice.Cost * item.Quantity;
                            if (item.UnitPrice.CostToApply.HasValue)
                            {
                                totalShippingToApply = (totalShippingToApply ?? 0) + item.UnitPrice.CostToApply.Value * item.Quantity;
                            }
                            if (item.UnitPrice.NoTaxCost.HasValue)
                            {
                                totalShippingNoTax = (totalShippingNoTax ?? 0) + item.UnitPrice.NoTaxCost.Value * item.Quantity;
                            }
                        }

                        totalShipping += group.GlobalPrice.Cost;
                        if (group.GlobalPrice.CostToApply.HasValue)
                        {
                            totalShippingToApply = (totalShippingToApply ?? 0) + group.GlobalPrice.CostToApply.Value;
                        }
                        if (group.GlobalPrice.NoTaxCost.HasValue)
                        {
                            totalShippingNoTax = (totalShippingNoTax ?? 0) + group.GlobalPrice.NoTaxCost.Value;
                        }
                    }

                    logisticLineGroup.GlobalPrices.TotalShipPriceUserEur = totalShippingToApply ?? totalShipping;
                    if (totalShippingNoTax.HasValue)
                    {
                        logisticLineGroup.GlobalPrices.TotalShipPriceUserEurNoVAT = totalShippingNoTax.Value;
                    }
                }
            }
        }
    }
}
