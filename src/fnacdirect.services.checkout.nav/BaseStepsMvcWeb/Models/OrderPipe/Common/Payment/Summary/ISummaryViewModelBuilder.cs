﻿using FnacDirect.OrderPipe.Base.Model;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Common.Payment
{
    public interface ISummaryViewModelBuilder
    {
        PaymentSummaryViewModel Build(OPContext opContext, PopOrderForm popOrderForm);
    }
}
