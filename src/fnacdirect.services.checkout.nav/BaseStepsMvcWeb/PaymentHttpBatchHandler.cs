using System.Web.Http;

namespace FnacDirect.OrderPipe.BaseMvc.Web
{
    public class PaymentHttpBatchHandler : PopHttpBatchHandler
    {
        protected override string Pipe => "payment";

        public PaymentHttpBatchHandler(HttpServer httpServer)
            : base(httpServer)
        {

        }
    }
}
