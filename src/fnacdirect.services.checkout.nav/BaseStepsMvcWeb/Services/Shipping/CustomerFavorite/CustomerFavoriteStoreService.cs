﻿using FnacDirect.Context;
using FnacDirect.Technical.Framework.Web.Mvc;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Services.ShipToStore
{
    public class CustomerFavoriteStoreService : ICustomerFavoriteStoreService
    {
        private readonly IUserContextProvider _userContextProvider;
        private readonly IUserExtendedContextProvider _userExtendedContextProvider;

        public CustomerFavoriteStoreService(IUserContextProvider userContextProvider, IUserExtendedContextProvider userExtendedContextProvider)
        {
            _userContextProvider = userContextProvider;
            _userExtendedContextProvider = userExtendedContextProvider;
        }

        public int? GetFavoriteStoreId()
        {
            if (!_userContextProvider.GetCurrentUserContext().IsIdentified)
            {
                return null;
            }

            var userExtendedContext = _userExtendedContextProvider.GetCurrentUserExtendedContext();

            return userExtendedContext.Customer?.FavoriteStoreId;
        }
    }
}