using FnacDirect.Solex.Shipping.Client.Contract;
using System.Linq;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace FnacDirect.OrderPipe.Base.BaseModel.Model.Quote
{
    public class Group
    {
        public int Id { get; set; }
        public List<ItemInGroup> Items { get; set; } = new List<ItemInGroup>();
        [JsonProperty(DefaultValueHandling = DefaultValueHandling.Populate)]
        public decimal GlobalPrice { get; set; }

        public Group()
        {

        }

        public Group(Solex.Shipping.Client.Contract.Group group)
        {
            Id = group.Id;
            Items = group.Items.ToList();
            GlobalPrice = group.GlobalPrice;
        }
    }
}
