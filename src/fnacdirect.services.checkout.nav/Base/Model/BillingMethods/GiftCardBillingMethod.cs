using FnacDirect.Partners.Kadeos.Model;
using System;
using System.Xml.Serialization;

namespace FnacDirect.OrderPipe.Base.Model
{
    public class GiftCardBillingMethod : BillingMethod
    {
        public const int IdBillingMethod = 16384;

        [XmlIgnore]
        private DateTime? _ExpirationDate;

        public DateTime? ExpirationDate
        {
            get
            {
                return _ExpirationDate;
            }
            set
            {
                _ExpirationDate = value;
            }
        }
        [XmlIgnore]
        private int? _TransactionId;

        public int? TransactionId
        {
            get
            {
                return _TransactionId;
            }
            set
            {
                _TransactionId = value;
            }
        }

        [XmlIgnore]
        private string _CardNumber;

        public string CardNumber
        {
            get
            {
                return _CardNumber;
            }
            set
            {
                _CardNumber = value;
            }
        }

        [XmlIgnore]
        private string _PinCode;

        public string PinCode
        {
            get
            {
                return _PinCode;
            }
            set
            {
                _PinCode = value;
            }
        }




        [XmlIgnore]
        private string _CardType;

        public string CardType
        {
            get
            {
                return _CardType;
            }
            set
            {
                _CardType = value;
            }
        }

        [XmlIgnore]
        private decimal? _MaxAmount;

        public decimal? MaxAmount
        {
            get
            {
                return _MaxAmount;
            }
            set
            {
                _MaxAmount = value;
            }
        }

        [XmlIgnore]
        private decimal? _AmountUsed;

        public decimal? AmountUsed
        {
            get
            {
                return _AmountUsed;
            }
            set
            {
                _AmountUsed = value;
            }
        }

        [XmlIgnore]
        private int? _ErrorCode;

        public int? ErrorCode
        {
            get
            {
                return _ErrorCode;
            }
            set
            {
                _ErrorCode = value;
            }
        }



        [XmlIgnore]
        private decimal? _ChoosenAmount;

        public decimal? ChoosenAmount
        {
            get
            {
                return _ChoosenAmount;
            }
            set
            {
                _ChoosenAmount = value;
            }
        }

        [XmlIgnore]
        private bool? _Valid;

        public bool? Valid
        {
            get
            {
                return _Valid;
            }
            set
            {
                _Valid = value;
            }
        }

        [XmlIgnore]
        private bool? _PartiallyUsed;

        public bool? PartiallyUsed
        {
            get
            {
                return _PartiallyUsed;
            }
            set
            {
                _PartiallyUsed = value;
            }
        }

        [XmlIgnore]
        private string emiterName;

        public string EmiterName
        {
            get { return emiterName; }
            set { emiterName = value; }
        }

        [XmlIgnore]
        public string Details { get; set; }


        public GiftCardBillingMethod()
        {
            _PartiallyUsed = false;
            BillingMethodId = IdBillingMethod;
            Priority = 10;
            CanModifyAmountDown = false;
            CanModifyAmountUp = false;
            Name = "GiftCardBillingMethod";
        }

        public GiftCardBillingMethod(KadeosCard kadeosCard, decimal orderUsedAmount) : this()
        {
            ErrorCode = kadeosCard.ErrorCode;
            CardNumber = kadeosCard.Number;
            PinCode = kadeosCard.PinCode;
            CardType = kadeosCard.Type.ToString();
            ChoosenAmount = orderUsedAmount;
            MaxAmount = kadeosCard.MaxAmount;
            Amount = orderUsedAmount;
            emiterName = kadeosCard.EmitterName;
            ExpirationDate = kadeosCard.ExpirationDate;
            TransactionId = kadeosCard.TransactionId;
            AmountUsed = kadeosCard.AmountUsed;
        }
    }
}
