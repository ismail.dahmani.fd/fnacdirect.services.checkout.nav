using FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Common.Payment;
using System;
using FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Membership;
using FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Omniture;
using Newtonsoft.Json.Linq;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Basket
{
    public class RootViewModel : BaseRootViewModel, IRootViewModel
    {
        public Maybe<BasketViewModel> Basket { get; set; }

        public PaymentSummaryViewModel PaymentSummary { get; set; }

        public OptionsViewModel Options { get; set; }

        public MembershipViewModel Membership { get; set; }

        public Maybe<MembershipCardViewModel> AdjustedMembershipCard { get; set; }

        public LoginPopinViewModel Login { get; set; }

        public DonationsViewModel Donations { get; set; }

        public bool ShowUnavailabilityPopin { get; set; }

        public bool AccountHasBeenCreated { get; set; }

        public string PageName { get; set; }

        public OmnitureBasketViewModel Omniture { get; set; }
        public TrackingViewModel Tracking { get; set; }

        public bool ShowOrangeBanner { get; set; }

        public TermsAndConditionsViewModel TermsAndConditions { get; set; }

        public JObject WishlistAside { get; set; }
    }
}
