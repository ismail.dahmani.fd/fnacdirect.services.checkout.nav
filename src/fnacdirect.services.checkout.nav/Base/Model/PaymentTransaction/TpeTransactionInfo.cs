namespace FnacDirect.OrderPipe.Base.Model.FDV
{
    public class TpeTransactionInfo
    {
        /// <summary>
        /// Id de l'imprimante selectionnée si paiement par TPE
        /// </summary>
        public int? PrinterId { get; set; }

        /// <summary>
        /// ModelId de l'imprimante selectionnée si paiement par TPE
        /// </summary>
        public int? PrinterModelId { get; set; }
    }
}
