using FnacDirect.OrderPipe.Base.Business.LogisticLineGroups.Internals.Initializers;
using FnacDirect.OrderPipe.Base.LineGroups;
using FnacDirect.OrderPipe.Base.Model;
using System.Collections.Generic;
using System.Linq;

namespace FnacDirect.OrderPipe.Base.Business.LogisticLineGroups.Initializers
{
    public class SelfCheckoutLogisticLineGroupInitializer : BaseLogisticLineGroupInitializer
    {
        private readonly ILogisticLineGroupGenerator _logisticLineGroupGenerator;

        public SelfCheckoutLogisticLineGroupInitializer(ILogisticLineGroupGenerator logisticLineGroupGenerator)
        {
            _logisticLineGroupGenerator = logisticLineGroupGenerator;
        }

        public override IEnumerable<ILogisticLineGroup> Initialize(IDictionary<ILineGroupHeader, IEnumerable<Article>> selectedLineGroups)
        {
            var shippingMethodEvaluation = Context.IGotShippingMethodEvaluation.ShippingMethodEvaluation;
            var output = new List<ILogisticLineGroup>();

            foreach (var lineGroup in selectedLineGroups.Keys)
            {
                var logisticLineGroup = _logisticLineGroupGenerator.Generate(lineGroup);
                logisticLineGroup.OrderType = OrderTypeEnum.IntraMagPE;
                logisticLineGroup.Informations.OrderType = (int)OrderInfoOrderTypeEnum.IntramagPE;

                logisticLineGroup.Articles = new List<Article>(selectedLineGroups[lineGroup]);

                lineGroup.Informations.OrderType = logisticLineGroup.Informations.OrderType;

                Helper.CopyShippingMethodEvaluation(lineGroup, logisticLineGroup, shippingMethodEvaluation);

                // Ne devrait pas arriver
                if (logisticLineGroup.SelectedShippingChoice != null)
                {
                    foreach (var article in logisticLineGroup.Articles.OfType<StandardArticle>())
                    {
                        article.ShipMethod = logisticLineGroup.SelectedShippingChoice.MainShippingMethodId;
                    }
                }

                output.Add(logisticLineGroup);
            }

            return output;
        }
    }
}
