using System.Collections.Generic;
using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.OrderPipe.Base.Model.LineGroups;
using FnacDirect.Relay.Model;

namespace FnacDirect.OrderPipe.Base.Business.Shipping.Relay
{
    public interface IRelayValidityService
    {
        bool CheckRelayValidity(RelayAddress relayAddress, IEnumerable<ILineGroup> lineGroups);
        bool CheckRelayValidity(RelayAddressEntity relayAddressEntity, IEnumerable<ILineGroup> lineGroups);
        bool CheckRelayValidity(RelayAddress relayAddress, IEnumerable<ILineGroup> lineGroups, out string errorMessage);
        bool CheckRelayValidity(RelayAddress relayAddress, IEnumerable<ILineGroup> lineGroups,int deliveryMargin, out string errorMessage);
    }
}
