﻿namespace FnacDirect.OrderPipe.Config
{
    public interface IUiDescriptor
    {
        void Init(IInteractiveStep step, IOP pipe, InteractiveStep cs, Pipe cp);

        string GetRedirect();

        string Label { get; set; }
        
        string LabelId { get; set; }

        bool ForceRedirectOnContinue { get; set; }

        RedirectMode RedirectMode { get; set; }
    }
}
