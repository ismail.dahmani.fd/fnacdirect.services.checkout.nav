namespace FnacDirect.OrderPipe.CM.Model
{
    public enum AdherentSearchField
    {
        Undefined = 0,
        LastName,
        FirstName,
        EmailAddress,
        PhoneNumber,
        BirthDate
    }
}
