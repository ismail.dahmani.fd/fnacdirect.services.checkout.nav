using FnacDirect.OrderPipe.BaseMvc.Web.Pop.Models.Common;
using System.Collections.Generic;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Common.Shipping.Postal
{
    public class PostalShippingNetworkViewModel : ShippingNetworkViewModel<PostalDetailsViewModel>, IBaseApiRootViewModel
    {
        public List<PostalShippingChoiceViewModel> PostalShippingChoices { get; set; }

        public bool HasMarketingEvent { get; set; }

        public string MarketingEventPicto { get; set; }
        
        public int? FnacPlusCardIdFromVirtualExpressFnacPlus { get; set; }
        
        public List<PipeMessageViewModel> PipeMessages { get; } = new List<PipeMessageViewModel>();
    }
}
