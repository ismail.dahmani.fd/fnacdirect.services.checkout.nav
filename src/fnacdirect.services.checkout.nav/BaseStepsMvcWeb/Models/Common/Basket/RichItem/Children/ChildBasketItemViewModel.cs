﻿namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Basket.Children
{
    public abstract class ChildBasketItemViewModel
    {
        public virtual bool IsDummy { get; set; }
    }
}
