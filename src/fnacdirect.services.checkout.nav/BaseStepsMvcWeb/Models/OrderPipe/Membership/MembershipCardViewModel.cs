﻿
namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Membership
{
    public class MembershipCardViewModel
    {
        public string CardPrice { get; set; }

        public string ReducedCardPrice { get; set; }

        public bool IsRenewable { get; set; }

        public bool DisplayPopin { get; set; }        
    }
}
