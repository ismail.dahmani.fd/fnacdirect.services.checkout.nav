using FnacDirect.OrderPipe.Base.Business;
using FnacDirect.OrderPipe.Base.Business.ArticleConversions.ShoppingCartToOrderForm;
using FnacDirect.OrderPipe.Base.Business.Registries;
using FnacDirect.OrderPipe.Base.Business.Tickets.ServiceClient;
using FnacDirect.OrderPipe.Base.Proxy;

namespace StructureMap.Configuration.DSL
{
    public static class OrderPipeBusinessFrontSpecificsBuilderExtensions
    {
        public static void WithFrontSpecifics(this OrderPipeBusinessRegistryBuilder builder)
        {
            var registry = builder.Registry;
            
            registry.For<IShoppingCartToOrderFormConverter>().Use<ShoppingCartToOrderFormArticleConverter>();
            registry.For<IPricingServiceProvider>().Use<PricingServiceProvider>();

            registry.For<ICustomerContext>().Use<CustomerContext>();

            registry.For<ITicketGeneratorService>().Use(() => new TicketGeneratorServiceClient("TicketGeneratorService"));
        }
    }
}
