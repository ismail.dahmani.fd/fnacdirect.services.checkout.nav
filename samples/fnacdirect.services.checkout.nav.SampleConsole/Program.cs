﻿using System;
using Microsoft.Extensions.DependencyInjection;
using fnacdirect.services.checkout.nav;

namespace fnacdirect.services.checkout.navSampleConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            var serviceCollection=new ServiceCollection();
            serviceCollection.AddMyService(o=>o.InitialValue=1);
            serviceCollection.AddLogging();

            var container=serviceCollection.BuildServiceProvider();

            var service=container.GetService<IMyService>();

            var value=service.GetNextValue();
            
            Console.WriteLine($"Value = {value}");
        }
    }
}
