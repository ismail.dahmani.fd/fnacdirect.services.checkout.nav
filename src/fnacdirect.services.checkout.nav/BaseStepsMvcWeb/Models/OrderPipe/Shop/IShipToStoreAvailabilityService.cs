using FnacDirect.IdentityImpersonation;
using FnacDirect.OrderPipe.Base.BaseModel;
using FnacDirect.OrderPipe.Base.Model;
using System;
using System.Collections.Generic;

namespace FnacDirect.OrderPipe.Base.Business.Shipping.Shop
{
    public interface IShipToStoreAvailabilityService
    {
        IEnumerable<ClickAndCollectShipToStoreAvailability> GetClickAndCollectAvailabilities(FnacStore.Model.FnacStore fnacStore, IEnumerable<Article> articles);
        IEnumerable<ShipToStoreAvailability> GetAvailabilitiesForShop(IEnumerable<Article> articles, Maybe<ShopShippingMethodEvaluationItem> shopShippingMethodEvaluationItem, IEnumerable<int> excludedLogisticTypes);
        IEnumerable<ShipToStoreAvailability> GetAvailabilities(FnacStore.Model.FnacStore fnacStore, IEnumerable<Article> articles);
        IEnumerable<ShipToStoreAvailability> GetAvailabilities(FnacStore.Model.FnacStore fnacStore, IEnumerable<Article> articles, IEnumerable<int> excludedLogisticTypes, IdentityImpersonator identityImpersonator);
    }
}
