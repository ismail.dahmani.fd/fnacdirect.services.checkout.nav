﻿using FnacDirect.OrderPipe.BaseMvc.Web.Models.Common;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Membership
{
    public class ChoiceViewModel : BaseChoiceData
    {
        public bool IsSelected { get; set; }
    }
}
