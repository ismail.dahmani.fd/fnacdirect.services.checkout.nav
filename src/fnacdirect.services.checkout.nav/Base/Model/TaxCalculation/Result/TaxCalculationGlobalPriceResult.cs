﻿namespace FnacDirect.OrderPipe.Base.Model.TaxCalculation
{
    public class TaxCalculationGlobalPriceResult
    {
        public GlobalPriceType PriceType { get; set; }
        public decimal TotalDBPriceToApply { get; set; }
        public decimal TotalUserPriceToApply { get; set; }
        public decimal TotalNoTaxUserPriceToApply { get; set; }

        public TaxCalculationGlobalPriceResult(GlobalPriceType globalPriceType)
        {
            PriceType = globalPriceType;
        }
    }
}
