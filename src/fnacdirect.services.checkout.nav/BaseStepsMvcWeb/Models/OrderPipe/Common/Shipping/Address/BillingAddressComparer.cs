using System;
using System.Collections.Generic;

namespace FnacDirect.OrderPipe.Base.Model
{
    public class BillingAddressComparer : IEqualityComparer<BillingAddress>
    {

        public bool Equals(BillingAddress x, BillingAddress y)
        {
            //dans le cas d'une commande pass� � partir du batch, le Firstname peut �tre = null
            x.Firstname = x.Firstname ?? string.Empty;
            const StringComparison SC = StringComparison.OrdinalIgnoreCase;

            return
                x.Firstname.Equals(y.Firstname, SC) &&
                x.Lastname.Equals(y.Lastname, SC) &&
                x.AddressLine1.Equals(y.AddressLine1, SC) &&
                x.AddressLine2.Equals(y.AddressLine2, SC) &&
                x.AddressLine3.Equals(y.AddressLine3, SC) &&
                x.AddressLine4.Equals(y.AddressLine4, SC) &&
                x.ZipCode.Equals(y.ZipCode, SC) &&
                x.City.Equals(y.City, SC);
        }


        public int GetHashCode(BillingAddress ba)
        {
            var hCode = ba.BillingAddressPK.GetValueOrDefault();
            return hCode.GetHashCode();
        }
    }
}

