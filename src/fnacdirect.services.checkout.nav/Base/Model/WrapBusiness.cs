using FnacDirect.OrderPipe.Base.BaseModel;
using FnacDirect.OrderPipe.Base.Model;
using System.Linq;

namespace FnacDirect.OrderPipe.Base.Business.Wrap
{
    public class WrapBusiness : IWrapBusiness
    {
        private readonly int _maxArticleQuantity = 10;

        public bool IsEligible(PopOrderForm orderForm)
        {
            var hasAnyAvailableWrapMethod = orderForm.LineGroups.Any(
                x => x.LogisticTypes.Any(
                    y => y.AvailableWrapMethod != null
                        && y.AvailableWrapMethod.Any(z => z.Id == 1)));

            if (!hasAnyAvailableWrapMethod)
                return false;

            if (orderForm.GetTotalArticleQuantities() > _maxArticleQuantity)
                return false;

            //&& orderForm.LineGroups.Any(lineGroup => lineGroup.Articles.Any(article => ((StandardArticle)article).IsPE))))
            if (!orderForm.ShippingMethodEvaluation.FnacCom.HasValue)
                return false;

            var shippingMethodEvaluationGroup = orderForm.ShippingMethodEvaluation.FnacCom.Value;
            var selectedShippingMethod = shippingMethodEvaluationGroup.SelectedShippingMethodEvaluation;

            if  (selectedShippingMethod?.ShippingMethodEvaluationType == ShippingMethodEvaluationType.Shop
                || (selectedShippingMethod?.ShippingMethodEvaluationType == ShippingMethodEvaluationType.Postal
                && shippingMethodEvaluationGroup.ShippingExcludedWrap()))
                return false;

            return true;
        }
    }
}
