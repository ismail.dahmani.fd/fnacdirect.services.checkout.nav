﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FnacDirect.OrderPipe.FDV.Model
{
    public enum MoneticTicketType
    {
        ClientTicket = 1,
        SellerTicket = 2
    }
}
