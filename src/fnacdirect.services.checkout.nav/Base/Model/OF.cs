using System;
using System.Linq;
using System.Xml.Serialization;
using System.Collections.ObjectModel;
using FnacDirect.Technical.Framework.Velocity;
using FnacDirect.Contracts.Online.Model;
using System.Collections.Generic;
using System.Xml.Schema;
using System.Xml;

namespace FnacDirect.OrderPipe
{   
    /// <summary>
    /// Classe abstraite définissant l'OrderForm
    /// </summary>
    public abstract class OF : IOF
    {
        protected OF()
        {
            PrivateData = new DataList();
            UserMessages = new UserMessages();
            PipeMessages = new PipeMessages();
            ExecutionContext = new ExecutionContext();
        }

        [XmlAttribute]
        public string PipeName { get; set; }

        /// <summary>
        /// Nom de l'étape courante
        /// </summary>
        [XmlAttribute]
        public string CurrentStep { get; set; }

        /// <summary>
        /// Identifiant de l'order form
        /// </summary>
        [XmlAttribute]
        public string SID { get; set; }

        public bool IsPro { get; set; }

        /// <summary>
        /// Données privées (d'étape ou de groupe)
        /// </summary>
        [XmlArrayItem(typeof(Data))]
        public DataList PrivateData { get; set; }

        private ConstraintList _constraints;

        /// <summary>
        /// Liste de contraintes
        /// </summary>
        public ConstraintList Constraints
        {
            get
            {
                return _constraints;
            }
            set
            {
                _constraints = value;
            }
        }

        /// <summary>
        /// Nettoyage de la structure avant sérialisation
        /// </summary>
        public virtual void CleanUp()
        {
            // On enlève les listes vides, pour que ca prenne moins de place
            foreach (var constraint in CollectionUtils.CheckNullList<Constraint, ConstraintList>(ref _constraints))
            {
                CollectionUtils.CheckNullList<ConstraintParameter, ConstraintParameterList>(ref constraint.parameters);
            }

            if (PrivateData == null)
            {
                PrivateData = new DataList();
            }

            foreach (var data in PrivateData)
            {
                var groupData = data as GroupData;

                if (groupData == null)
                {
                    continue;
                }

                foreach (var constraint in CollectionUtils.CheckNullList<Constraint, ConstraintList>(ref groupData.constraints))
                {
                    CollectionUtils.CheckNullList<ConstraintParameter, ConstraintParameterList>(ref constraint.parameters);
                }
            }

            CollectionUtils.RemoveItemsFromCollection(PrivateData, delegate(Data d) { return d is TransientData; });
        }

        public IOF DeepCopy()
        {
            return CloneUtil.DeepCopy(this);
        }

        /// <summary>
        /// Retourne les données privées d'étape ou de groupe correspondant au nom indiqué
        /// </summary>
        /// <typeparam name="T">Type de la donnée</typeparam>
        /// <param name="dataName">Nom de la donnée</param>
        /// <param name="create">Vrai si la donnée est initialisée en cas de non-existance</param>
        /// <returns>La donnée. Si elle n'existe pas, elle est instanciée</returns>
        public T GetPrivateData<T>(string dataName, bool create) where T : Data, new()
        {
            dataName = typeof(T).FullName;

            T t = null;
            if (dataName == null)
            {
                return PrivateData.GetByType<T>();
            }

            if (PrivateData.Contains(dataName))
                t = PrivateData[dataName] as T;
            else
                if (create)
                {
                t = new T
                {
                    Name = dataName
                };
                PrivateData.Add(t);
                }
            return t;
        }

        /// <summary>
        /// Retourne les données privées d'étape ou de groupe correspondant au nom indiqué
        /// </summary>
        /// <typeparam name="T">Type de la donnée</typeparam>
        /// <param name="dataName">Nom de la donnée</param>
        /// <returns>La donnée. Si elle n'existe pas, elle est instanciée</returns>
        public T GetPrivateData<T>(string dataName) where T : Data, new()
        {
            return GetPrivateData<T>(dataName, true);
        }

        public T GetPrivateData<T>() where T : Data, new()
        {
            return GetPrivateData<T>(typeof(T).FullName, true);
        }

        public T GetPrivateData<T>(bool create) where T : Data, new()
        {
            return GetPrivateData<T>(typeof(T).FullName, create);
        }

        /// <summary>
        /// Supprime les données privées en fonction du type spécifié
        /// </summary>
        /// <typeparam name="T">Type de la donnée</typeparam>
        public void RemovePrivateData<T>() where T : Data, new()
        {
            RemovePrivateData<T>(name: null);
        }

        /// <summary>
        /// Supprime les données privées en fonction du type spécifié et du nom donné à la data
        /// </summary>
        /// <typeparam name="T">Type de la donnée</typeparam>
        /// <param name="name">Nom de la donnée</param>
        public void RemovePrivateData<T>(string name) where T : Data, new()
        {
            var data = GetPrivateData<T>(name, false);

            RemovePrivateData(data);
        }

        /// <summary>
        /// Supprime les données privées spécifiées en paramètre
        /// </summary>
        /// <typeparam name="T">Type de la donnée</typeparam>
        /// <param name="data">Donnée</param>
        public void RemovePrivateData<T>(T data) where T : Data
        {
            PrivateData.Remove(data);
        }

        public UserMessages UserMessages { get; set; }

        public PipeMessages PipeMessages { get; set; }

        public ExecutionContext ExecutionContext { get; set; }

        private PipeMessage AddMessage(PipeMessageLevel pipeMessageLevel, string categoryId, string ressourceId, params object[] parameters)
        {
            var pipeMessage = new PipeMessage(CurrentStep, categoryId, ressourceId, pipeMessageLevel, parameters);

            if (!PipeMessages.List.Any(m => m.Equals(pipeMessage)))
            {
                PipeMessages.List.Add(pipeMessage);
            }

            PipeMessages.CopyToUserMessages(UserMessages);

            return pipeMessage;
        }

        public PipeMessage AddErrorMessage(string categoryId, string ressourceId, params object[] parameters)
        {
            return AddMessage(PipeMessageLevel.Error, categoryId, ressourceId, parameters);
        }

        public PipeMessage AddWarningMessage(string categoryId, string ressourceId, params object[] parameters)
        {
            return AddMessage(PipeMessageLevel.Warning, categoryId, ressourceId, parameters);
        }

        public PipeMessage AddInfoMessage(string categoryId, string ressourceId, params object[] parameters)
        {
            return AddMessage(PipeMessageLevel.Info, categoryId, ressourceId, parameters);
        }

        public bool IsCurrentlyRunningCriticalSection { get; set; }

        public abstract OFRoute GetRoute();
    }
}
