﻿namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Solex.Appointment
{
    public class SelectedSlotViewModel
    {
        public string PartnerId { get; set; }
        public string PartnerUniqueIdentifier { get; set; }
    }
}
