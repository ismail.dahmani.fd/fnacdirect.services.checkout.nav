﻿using System.Text.RegularExpressions;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Common.Payment
{
    public class GiftCardTypeViewModel
    {
        public GiftCardTypeViewModel(string label, string value)
        {
            Label = label;
            Value = value;           
        }

        public GiftCardTypeViewModel(string label, string value, string mask)
        {
            Label = label;
            Value = value;
            Mask = mask;
            Prefix = ComputePrefix(mask);
            Sufix = ComputeSufix(mask, Prefix);
        }

        public string Label { get; set; }

        public string Value { get; set; }

        public string Mask { get; set; }

        public string Prefix { get; set; }

        public string Sufix { get; set; }

        private string ComputePrefix(string mask)
        {
            var regex = new Regex("[\\d+\\-]*");
            return regex.Match(mask).Value;
        }
        private string ComputeSufix(string mask,string prefix)
        {
            return mask.Substring(prefix.Length);
        }
    }
}
