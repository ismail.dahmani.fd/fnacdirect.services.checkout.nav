using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Xml.Serialization;
using FnacDirect.Quote.Model.Enums;

namespace FnacDirect.Quote.Model
{
    /// <summary>
    /// class definissant un article d'une ligne du devis
    /// </summary>
    [Serializable]
    public class QuoteArticle : OrderPipe.Base.Model.StandardArticle
    {
        #region Members

        private QuoteProductValidation m_quoteProductValidation;

        #endregion Members

        /// <summary>
        /// Gets or sets product validation state 
        /// </summary>
        public QuoteProductValidation ProductValidation
        {
            get { return m_quoteProductValidation; }
            set { m_quoteProductValidation = value; }
        }

        public List<QuoteLineRemise> Remises
        {
            get
            {
                if (null == _Remises)
                    _Remises = new List<QuoteLineRemise>();
                return _Remises;
            }
            set { _Remises = value; }
        }

        private List<QuoteLineRemise> _Remises;


        [XmlIgnore] private decimal? _ShipPriceNoVATEur;


        public new decimal? ShipPriceNoVATEur
        {
            get { return _ShipPriceNoVATEur; }
            set { _ShipPriceNoVATEur = value; }
        }

        

        [XmlIgnore]
        private decimal? _ShipPriceUserEur;

        public new decimal? ShipPriceUserEur
        {
            get { return _ShipPriceUserEur; }
            set { _ShipPriceUserEur = value; }
        }

        [XmlIgnore] private decimal? _ShipPriceDBEur;


        public new decimal? ShipPriceDBEur
        {
            get { return _ShipPriceDBEur; }
            set { _ShipPriceDBEur = value; }
        }

        [XmlIgnore] private decimal? _GlobalShipPriceDBEur;

        public decimal? GlobalShipPriceDBEur
        {
            get { return _GlobalShipPriceDBEur; }
            set { _GlobalShipPriceDBEur = value; }
        }

        private int? logType;

        public new int? LogType
        {
            get { return logType; }
            set
            {
                base.LogType = logType = value;
            }
        }


        [XmlIgnore] private bool? _IsAvailable;


        public new bool? IsAvailable
        {
            get { return _IsAvailable; }
            set { _IsAvailable = value; }
        }

        private decimal? priceDBEur;
        private decimal? priceNoVATEur;
        private decimal? priceRealEur;
        private decimal? priceUserEur;


        private decimal? vATRate;

        [XmlElement("VATRate")]
        public new decimal? VATRate
        {
            get
            {
                vATRate = base.VATRate;
                return vATRate;
            }
            set { vATRate = base.VATRate = value; }
        }

        private int? vATId;

        [XmlElement("VATId")]
        public new int? VATId
        {
            get
            {
                vATId = base.VATId;
                return vATId;
            }
            set { vATId = base.VATId = value; }
        }


        [XmlElement("PriceDBEur")]
        public new decimal? PriceDBEur
        {
            get
            {
                priceDBEur = base.PriceDBEur;
                return priceDBEur;
            }
            set { priceDBEur = base.PriceDBEur = value; }
        }

        [XmlElement("PriceNoVATEur")]
        public new decimal? PriceNoVATEur
        {
            get
            {
                priceNoVATEur = base.PriceNoVATEur;
                return priceNoVATEur;
            }
            set 
            { 
                priceNoVATEur = value;
                base.PriceNoVATEur = value;
            }
        }

        [XmlElement("PriceRealEur")]
        public new decimal? PriceRealEur
        {
            get
            {
                priceRealEur = base.PriceRealEur;
                return priceRealEur;
            }
            set { priceRealEur = base.PriceRealEur = value; }
        }

        [XmlElement("PriceUserEur")]
        public new decimal? PriceUserEur
        {
            get
            {
                priceUserEur = base.PriceUserEur;
                return priceUserEur;
            }
            set { priceUserEur = base.PriceUserEur = value; }
        }
    }

    #region "Collections"

    [XmlType(TypeName = "Articles"), Serializable]
    public class QuoteArticles : CollectionBase
    {

        public QuoteArticle this[int index]
        {
            get { return ((QuoteArticle) List[index]); }
            set { List[index] = value; }
        }

        public int Add(QuoteArticle value)
        {
            return (List.Add(value));
        }

        public int IndexOf(QuoteArticle value)
        {
            return (List.IndexOf(value));
        }

        public void Insert(int index, QuoteArticle value)
        {
            List.Insert(index, value);
        }

        public void Remove(QuoteArticle value)
        {
            List.Remove(value);
        }

        public bool Contains(QuoteArticle value)
        {
            // If value is not of type Article, this will return false.
            return (List.Contains(value));
        }

        protected override void OnInsert(int index, object value)
        {
            // Insert additional code to be run only when inserting values.
        }

        protected override void OnRemove(int index, object value)
        {
            // Insert additional code to be run only when removing values.
        }

        protected override void OnSet(int index, object oldValue, object newValue)
        {
            // Insert additional code to be run only when setting values.
        }

        protected override void OnValidate(object value)
        {
        }
    }

    #endregion
}