﻿namespace FnacDirect.OrderPipe.Base.Model.LineGroups
{
    public class TemporaryLineGroup : LineGroup
    {
        public override bool IsTemporary
        {
            get
            {
                return true;
            }
        }
    }
}
