using System;
using System.Web.Mvc;
using FnacDirect.Technical.Framework.Web;
using FnacDirect.Technical.Framework.Web.Mvc;
using FnacDirect.Context;
using FnacDirect.OrderPipe.Base.Proxy.Customer;
using FnacDirect.OrderPipe.BaseMvc.Web.Pop.Models.Login;
using FnacDirect.Customer.BLL.Authentication;
using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.OrderPipe.Base.Model.Pop;
using FnacDirect.OrderPipe.Base.OneClick;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Controllers.Pop
{
    public class PopLoginController : PopBaseController
    {
        private readonly IApplicationContext _applicationContext;
        private readonly IUserContextProvider _userContextProvider;
        private readonly ICustomerService _customerService;
        private readonly IAuthenticationService _authenticationService;

        public PopLoginController(IApplicationContext applicationContext,
                                  IAuthenticationService authenticationService,
                                  IUserContextProvider userContextProvider, 
                                  ICustomerService customerService)
        {
            _authenticationService = authenticationService;
            _applicationContext = applicationContext;
            _userContextProvider = userContextProvider;
            _customerService = customerService;
        }

        [HttpGet]
        public ActionResult Index()
        {
            var loginViewModel = new LoginViewModel();

            var userContext = _userContextProvider.GetCurrentUserContext();

            if(userContext.IsIdentified)
            {
                var customer = _customerService.Load(userContext.GetUserId(), userContext.GetUserGuid());

                loginViewModel.FirstName = customer.FirstName;
                loginViewModel.Email = customer.EMail;
            }

            return View("Page", loginViewModel);
        }

        [HttpPost]
        public ActionResult Index(LoginViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View("Page", model);
            }

            if (!_authenticationService.AuthenticateWithTrial(model.Email, model.Password, model.RememberMe))
            {
                ModelState.AddModelError(string.Empty, _applicationContext.GetMessage("orderpipe.LoginControl.Email.Wrong"));

                return View("Page", model);
            }

            return Continue();
        }

        //[HttpGet]
        //public ActionResult SpreadRegistration()
        //{
        //    var opContext = GetOpContext();
        //    var popOrderForm = opContext.OF as PopOrderForm;

        //    var popData = popOrderForm.GetPrivateData<PopData>(create: false);

        //    if(popData != null)
        //    {
        //        popData.DisplayOnePageValidated = false;
        //    }

        //    popOrderForm.BillingAddress = null;
        //    popOrderForm.ShippingMethodEvaluation.Reset();
        //    popOrderForm.UserContextInformations = new Base.Model.UserInformations.UserContextInformations
        //    {
        //        IsCreatingAccount = true
        //    };

        //    FnacContext.Current.Session.SetAuthenticationId(null);
        //    Helper.SetCookie(ParametersName.UID, "0" + Guid.NewGuid(), DateTime.Now.Add(-TimeSpan.FromDays(180)));

        //    return RewindAndRedirect();
        //}
    }
}
