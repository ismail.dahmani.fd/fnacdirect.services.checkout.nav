﻿using FnacDirect.OrderPipe.Base.Business.LogisticLineGroups.Internals.InitialSelectors;
using System.Collections.Generic;
using System.Linq;
using FnacDirect.OrderPipe.Base.LineGroups;
using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.OrderPipe.Base.Model.LineGroups;
using FnacDirect.OrderPipe.Base.Model.ClickAndCollect;

namespace FnacDirect.OrderPipe.Base.Business.LogisticLineGroups.InitialSelectors
{
    public class SteedFromStoreLineGroupInitialSelector : BaseLineGroupInitialSelector
    {
        public override IDictionary<ILineGroupHeader, IEnumerable<Article>> Select(IEnumerable<ILineGroup> lineGroups)
        {
            var fnacLineGroups = lineGroups.Fnac();

            var output = new Dictionary<ILineGroupHeader, IEnumerable<Article>>();

            var steedFromStoreData = Context.PrivateDataContainer.GetPrivateData<SteedFromStoreData>(false);

            if (steedFromStoreData == null || !steedFromStoreData.StoreId.HasValue)
            {
                return output;
            }

            foreach (var productAvailability in steedFromStoreData.PridsAvailable)
            {
                foreach (var lineGroup in fnacLineGroups)
                {
                    var matchingArticle = lineGroup.Articles.OfType<StandardArticle>().FirstOrDefault(a => a.ProductID.GetValueOrDefault(0) == productAvailability);

                    if (matchingArticle != null && matchingArticle.SteedFromStore)
                    {
                        if (!output.ContainsKey(lineGroup))
                        {
                            output.Add(lineGroup, new List<Article>());
                        }

                        var entry = output[lineGroup];

                        if (!entry.Any(a => a.ProductID.GetValueOrDefault(0) == productAvailability))
                        {
                            (entry as IList<Article>).Add(matchingArticle);
                        }
                    }
                }
            }

            return output;
        }
    }
}
