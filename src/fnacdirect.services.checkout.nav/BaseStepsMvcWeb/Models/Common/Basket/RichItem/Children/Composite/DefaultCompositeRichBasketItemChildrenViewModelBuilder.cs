﻿using FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Basket.Children;
using System.Collections.Generic;
using System.Linq;
using FnacDirect.OrderPipe.Base.Model;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Basket
{
    public class DefaultCompositeRichBasketItemChildrenViewModelBuilder : ICompositeRichBasketItemChildrenViewModelBuilder
    {
        private readonly IEnumerable<IRichBasketItemChildrenViewModelBuilder> _richBasketItemChildrenViewModelBuilders;

        public DefaultCompositeRichBasketItemChildrenViewModelBuilder(IEnumerable<IRichBasketItemChildrenViewModelBuilder> richBasketItemChildrenViewModelBuilders)
        {
            _richBasketItemChildrenViewModelBuilders = richBasketItemChildrenViewModelBuilders;
        }

        public IEnumerable<ChildBasketItemViewModel> Build(StandardArticle standardArticle, IEnumerable<Promotion> globalPromotions, bool applyAdherentPromotion)
        {
            foreach (var item in _richBasketItemChildrenViewModelBuilders)
            {
                var results = item.Build(standardArticle, globalPromotions, applyAdherentPromotion).ToList();

                if (results.Any())
                {
                    return results.Where(c => !c.IsDummy);
                }
            }
            return Enumerable.Empty<ChildBasketItemViewModel>();
        }
    }
}
