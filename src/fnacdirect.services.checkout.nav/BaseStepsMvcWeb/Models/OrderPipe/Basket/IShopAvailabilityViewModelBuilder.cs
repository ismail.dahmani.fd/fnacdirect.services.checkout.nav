﻿using FnacDirect.OrderPipe.Base.BaseModel;
using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.OrderPipe.Base.Model.LineGroups;
using FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Basket;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.OrderPipe.Basket
{
    public interface IShopAvailabilityViewModelBuilder
    {
        ShopAvailabilityViewModel Build(StandardArticle standardArticle, ILineGroup parentLineGroup, ShippingMethodEvaluationGroup shippingMethodEvaluationGroup);
    }
}
