﻿using System.Web.Routing;
using FnacDirect.OrderPipe.Config;

namespace FnacDirect.OrderPipe.CoreRouting
{
    public abstract class OrderPipeForceStepStrategyBase<TDescriptor>
         : IOrderPipeForceStepStrategy<TDescriptor>
         where TDescriptor : class, IUiPipeDescriptor
    {
        public abstract string GetForceStepFor(IOP pipe, RequestContext requestContext);
    }
}
