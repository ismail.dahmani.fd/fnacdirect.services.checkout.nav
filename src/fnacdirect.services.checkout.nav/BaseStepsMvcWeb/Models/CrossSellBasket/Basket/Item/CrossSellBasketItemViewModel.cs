using FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Basket;
using FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Basket.Children;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FnacDirect.OrderPipe.BaseMvc.Web.CrossSellBasket
{
    public class CrossSellBasketItemViewModel
    {
        public CrossSellBasketItemViewModel()
        {
            CrossSellRichItemChildrenViewModels = Enumerable.Empty<CrossSellRichItemChildrenViewModel>();
        }

        public int? ProductId { get; set; }
        public string Title { get; set; }
        public string ImagePath { get; set; }
        public string Content { get; set; }
        public Guid OfferId { get; set; } = Guid.Empty;
        public QuantityViewModel Quantity { get; set; }
        public AvailabilityViewModel Availability { get; set; }

        public IEnumerable<CrossSellRichItemChildrenViewModel> CrossSellRichItemChildrenViewModels { get; set; }
    }
}
