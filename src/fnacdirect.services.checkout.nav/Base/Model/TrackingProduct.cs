using System;

namespace FnacDirect.OrderPipe.Base.BaseModel.Model
{
    [Serializable]
    public class TrackingProduct
    {
        public string OrderReference { get; set; }

        public string Title { get; set; }

        public int ReferentialId { get; set; }

        public int ProductId { get; set; }

        public decimal PriceNoVATEur { get; set; }

        public int? ShipMethod { get; set; }

        public decimal? ShipPriceUserEur { get; set; }

        public decimal? ShipPriceNoVATEur { get; set; }

        public decimal PriceUserEur { get; set; }

        public int Quantity { get; set; }

        public int TypeId { get; set; }

        public string OriginCode { get; set; }

        public string TypeLabel { get; set; }

        public bool IsMarketPlace { get; set; }

        public bool IsDemat { get; set; }

        public bool IsSoldOnMarketPlace { get; set; }

        public int ProductStatus { get; set; }

        public string SellerId { get; set; }

        public string SellerAnonymizedId { get; set; }

        public bool CollectInShop { get; set; }

        public string ShippingStoreName { get; set; }

        public string TradeMark { get; set; }

        public string PictureUrl { get; set; }

        public string ProductPageUrl { get; set; }

        public string AvailabilityLabel { get; set; }

        public bool IsAvailable { get; set; }

        public string ShippingArticleMode { get; set; }

        public decimal? VATRate { get; set; }

        public string OfferId { get; set; }

        public string SellerName { get; set; }
        public string SubCategory1Id { get; set; }
        public string SubCategory1 { get; set; }
    }
}
