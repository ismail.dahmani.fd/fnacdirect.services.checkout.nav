﻿namespace FnacDirect.OrderPipe.Base.Model
{
    public class NumericalAddOn
    {
        public string ExternalRef { get; set; }
        public string ReferenceFeed4 { get; set; }
        public int? FormatId { get; set; }
        public string FormatLabel { get; set; }
        public int? EditorId { get; set; }
        public string EditorLabel { get; set; }
        public int? DistributorId { get; set; }
        public string DistributorLabel { get; set; }
        public int? Flag { get; set; }
        public decimal? SellingPrice { get; set; }
        public decimal? PurchasePrice { get; set; }
        public int? AccountingFamily { get; set; }
        public int? InvoicingMode { get; set; }
        public int? SubscriptionMode { get; set; }
        public int? RetailerId { get; set; }
        public string RetailerLabel { get; set; }
        public string RetailerCode { get; set; }
        public decimal? SellingPriceBeforeTax { get; set; }
    }
}
