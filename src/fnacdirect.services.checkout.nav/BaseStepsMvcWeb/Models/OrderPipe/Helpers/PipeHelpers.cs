﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FnacDirect.OrderPipe.Base.Business.Helpers
{
    public class PipeHelpers
    {
        public static bool IsBasket(IOF orderForm)
        {
            return orderForm.PipeName == "basket";
        }

        public static bool IsPop(IOF orderForm)
        {
            return orderForm.PipeName == "pop";
        }
    }
}
