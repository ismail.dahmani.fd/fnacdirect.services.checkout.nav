﻿using System.Collections.Generic;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Common.Basket
{
    public class BasketItemWithPictureViewModel : BasketItemViewModel
    {
        public BasketItemWithPictureViewModel(BasketItemViewModel basketItemViewModel)
            : base(basketItemViewModel)
        {

        }

        public BasketItemWithPictureViewModel(BasketItemWithPictureViewModel basketItemWithPictureViewModel)
            : base(basketItemWithPictureViewModel)
        {
            NormalImage = basketItemWithPictureViewModel.NormalImage;
        }

        public string NormalImage { get; set; }
    }
}
