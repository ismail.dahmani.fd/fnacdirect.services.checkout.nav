﻿using FnacDirect.OrderPipe.Config;
using FnacDirect.Context;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FnacDirect.OrderPipe.Context;

namespace FnacDirect.OrderPipe.CoreRouting.UiDescriptorRenderer
{
    public abstract class UiDescriptorRendererBase<TDescriptor> : IUiDescriptorRenderer<TDescriptor>
                where TDescriptor : class, IUiDescriptor
    {
        public abstract string DoRenderFor(TDescriptor descriptor);

        public string Render(IUiDescriptor uiDescriptor)
        {
            return DoRenderFor(uiDescriptor as TDescriptor);
        }
    }
}
