
using System.Collections.Generic;

namespace FnacDirect.OrderPipe.Base.Business.OrderDetail
{
    public class Cgv
    {
        public CgvItem HasFnacArticles { get; set; }

        public CgvItem HasMpArticles { get; set; }

        public CgvItem HasEbook { get; set; }

        public CgvItem HasDematSoft { get; set; }

        public CgvItem HasAdherentCard { get; set; }

        public CgvItem HasExpressplus { get; set; }

        public CgvItem HasFnacplus { get; set; }

        public CgvItem HasGuarantee { get; set; }

        public List<Service> Services { get; set; }
    }
}
