using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FnacDirect.OrderPipe.Base.Model.Facets
{
    public interface IGotQuoteInformations : IOF
    {
        QuoteInfos QuoteInfos { get; set; }
    }
}
