using FnacDirect.OrderPipe.FDV.Model;
using System;

namespace FnacDirect.OrderPipe.Base.BaseModel.Model
{
    [Serializable]
    public class TicketData : StepData
    {
        public Ticket Ticket { get; set; }
        public string ShopTicketXml { get; set; }
    }
}
