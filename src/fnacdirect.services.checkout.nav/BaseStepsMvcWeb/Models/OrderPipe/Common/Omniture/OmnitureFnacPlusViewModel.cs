﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.OnePage.Omniture
{
    public class OmnitureFnacPlusViewModel
    {
        public string AddedProduct { get; set; }

        public string CurrentCustomerSegment { get; set; }

        public string FutureCustomerSegment { get; set; }
    }
}
