using FnacDirect.OrderPipe.Base.Model;
namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Common.Payment
{
    /// <summary>
    /// Model of saved client's Credit Cards
    /// </summary>

    public class CustomerCreditCardBaseViewModel
    {
        public string Brand { get; set; }

        public int TypeId { get; set; }

        public string Url { get; set; }

        public string MaskedCardNumber { get; set; }

        public string Holder { get; set; }

        public string ExpirationDateFormat { get; set; }

    }

    public class CustomerCreditCardViewModel : CustomerCreditCardBaseViewModel
    {

        public string Reference { get; set; }

        public string Label { get; set; }

        public bool IsSelected { get; set; }

        public bool IsDumy { get; set; }

        public string CardNumber { get; set; }

        #region for Ogone url call
        public string CardNo { get; set; }
        public string ExpirationDate { get; set; }
        public string Alias { get; set; }
        public string SHASign { get; set; }
        public int CryptoLength { get; set; }
        #endregion for Ogone url call

        public bool Is3DSecureTransaction { get; set; }
        public bool IsCryptoRequired { get; set; }

        /// <summary>
        /// Process name of the Ogone transaction to use (Ecommerce or AliasGateway/DirectLink)
        /// </summary>
        public string OgoneTransactionProcess { get; set; }

        public bool IsOgoneAliasGatewayProcess { get { return OgoneCreditCardBillingMethod.IsAliasGatewayProcess(OgoneTransactionProcess); } }

        // Valorisé uniquement si op.ogone.enablealiasgateway est true
        public decimal TransactionAmount { get; set; }

        // Valorisé uniquement si op.ogone.enablealiasgateway est true
        public decimal TransactionRealAmount { get; set; }
    }
}
