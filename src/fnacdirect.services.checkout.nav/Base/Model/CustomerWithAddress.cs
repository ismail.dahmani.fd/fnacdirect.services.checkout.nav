using FnacDirect.OrderPipe.Base.Model;

namespace FnacDirect.OrderPipe.CM.Model
{
    public class CustomerWithAddress
    {
        public BillingAddress BillingAddress { get; set; }

        public FnacDirect.Contracts.Online.Model.Customer Customer { get; set; }
    }
}
