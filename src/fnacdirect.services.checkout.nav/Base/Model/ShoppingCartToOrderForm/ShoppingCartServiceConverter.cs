﻿using FnacDirect.OrderPipe.Base.Model;

namespace FnacDirect.OrderPipe.Base.Business.ArticleConversions.ShoppingCartToOrderForm
{
    public partial class ShoppingCartToOrderFormArticleConverter
    {
        protected Service Convert<T>(ShoppingCartEntities.Service @from) where T : Service, new()
        {
            return (Service)Convert((ShoppingCartEntities.StandardArticle)@from, new T());
        }
    }
}
