using FnacDirect.OrderPipe.Base.Business.LogisticLineGroups;
using FnacDirect.OrderPipe.Base.Proxy.Membership;
using FnacDirect.Technical.Framework.Web.Mvc;

namespace FnacDirect.OrderPipe.Base.Business.Payment
{
    public class CheckCreditCardsEligibilityBusinessProvider : ICheckCreditCardsEligibilityBusinessProvider
    {
        private readonly ISwitchProvider _switchProvider;
        private readonly IBillingMethodService _billingMethodService;
        private readonly IApplicationContext _applicationContext;
        private readonly ICheckOriginService _checkOriginService;
        private readonly ILogisticLineGroupGenerationProcess _logisticLineGroupGenerationProcess;
        private readonly IAdherentCardArticleService _adherentCardArticleService;

        public CheckCreditCardsEligibilityBusinessProvider( ISwitchProvider switchProvider,
                                                            IBillingMethodService billingMethodService, 
                                                            IApplicationContext applicationContext, 
                                                            ICheckOriginService checkOriginService, 
                                                            ILogisticLineGroupGenerationProcess logisticLineGroupGenerationProcess,
                                                            IAdherentCardArticleService adherentCardArticleService)
        {
            _switchProvider = switchProvider;
            _billingMethodService = billingMethodService;
            _applicationContext = applicationContext;
            _checkOriginService = checkOriginService;
            _logisticLineGroupGenerationProcess = logisticLineGroupGenerationProcess;
            _adherentCardArticleService = adherentCardArticleService;
        }

        public ICheckCreditCardsEligibilityBusiness GetCheckCreditCardsEligibilityBusinessInstance(OPContext opContext)
        {
            return new CheckCreditCardsEligibilityBusiness(opContext, _switchProvider, _billingMethodService, _applicationContext, _checkOriginService, _logisticLineGroupGenerationProcess, _adherentCardArticleService);
        }
   }
}
