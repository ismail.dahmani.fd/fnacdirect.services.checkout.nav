using FnacDirect.Contracts.Online.IModel;
using FnacDirect.Contracts.Online.Model;
using FnacDirect.OrderPipe.Base.Business;
using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.OrderPipe.Base.Steps.PriceDispo;
using FnacDirect.Technical.Framework.CoreServices.Localization;
using FnacDirect.Technical.Framework.Web.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using DTO = FnacDirect.Contracts.Online.Model;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Services.Article.Pipe
{
    public class PipeArticleService : IPipeArticleService
    {
        private readonly IArticleDetailService _articleDetailService;
        private readonly IMarketPlaceServiceProvider _marketPlaceServiceProvider;
        private readonly IApplicationContext _applicationContext;
        private readonly IUIResourceService _uiResourceService;
        private readonly IPricingService _pricingService;
        private readonly IOfferPricer _offerPricer;
        private readonly ISwitchProvider _switchProvider;
        private readonly ISiteManagerBusiness2 _siteManagerBusiness2;
        private readonly IArticleService _articleService;

        public PipeArticleService(
            IArticleDetailService articleDetailService,
            IMarketPlaceServiceProvider marketPlaceServiceProvider,
            IApplicationContext applicationContext,
            IUIResourceService uiResourceService,
            IPricingServiceProvider pricingServiceProvider,
            IOfferPricer offerPricer,
            ISwitchProvider switchProvider,
            ISiteManagerBusiness2 siteManagerBusiness2,
            IArticleService articleService
            )
        {
            _articleDetailService = articleDetailService;
            _marketPlaceServiceProvider = marketPlaceServiceProvider;
            _applicationContext = applicationContext;
            _uiResourceService = uiResourceService;
            _pricingService = pricingServiceProvider.GetPricingService(_applicationContext.GetSiteContext().MarketId);
            _offerPricer = offerPricer;
            _switchProvider = switchProvider;
            _siteManagerBusiness2 = siteManagerBusiness2;
            _articleService = articleService;
        }

        public BaseArticle GetArticle(int productId, string offerRef = "")
        {
            if (!offerRef.IsNullOrEmpty() && offerRef != Guid.Empty.ToString())
                return GetMarketplaceArticle(productId, offerRef);
            else
                return GetStandardArticle(productId);
        }

        public StandardArticle GetStandardArticle(int productId)
        {
            var standardArticle = new StandardArticle
            {
                ProductID = productId,
                Quantity = 1
            };

            if (!_articleDetailService.GetArticleDetailWithPrice(standardArticle, _applicationContext.GetSiteContext(), false))
            {
                return null;
            }

            if (!IsStandardArticleValid(standardArticle))
            {
                return null;
            }

            ApplyPriceAndPromotionToStandardArticle(standardArticle);
            CheckAvailabilityForStandardArticle(standardArticle);

            return standardArticle;
        }


        public IEnumerable<StandardArticle> GetStandardArticles(IEnumerable<int> productIds) 
        {
            return productIds
                .Select(prid => GetStandardArticle(prid))
                .Where(article => article != null);
        }


        public MarketPlaceArticle GetMarketplaceArticle(int productId, string offerRef)
        {
            var marketPlaceArticle = new MarketPlaceArticle
            {
                ProductID = productId,
                OfferRef = offerRef,
                Quantity = 1
            };

            var marketPlaceService = _marketPlaceServiceProvider.GetMarketPlaceService(_applicationContext.GetSiteContext());
            if (!marketPlaceService.GetArticleDetail(marketPlaceArticle))
            {
                return null;
            }

            ApplyPriceAndPromotionToMPArticle(marketPlaceArticle);
            SetAvailabilityToMPArticle(marketPlaceArticle);

            return marketPlaceArticle;
        }

        public IEnumerable<DTO.Article> GetServices(StandardArticle standardArticle, ArticleFilter filter = ArticleFilter.NONE)
        {
            if (standardArticle.Availability == StockAvailability.ClickAndCollectOnly)
            {
                return Enumerable.Empty<DTO.Article>();
            }

            var categoryLinkedArticleList = GetLinkedArticles(standardArticle);
            var articles = categoryLinkedArticleList.Categories
                .Where(c => c != null)
                .SelectMany(c => c.Articles)
                .Where(a => a != null);

            return _articleService.FilterArticles(articles, filter);
        }

        public CategoryLinkedArticleList GetLinkedArticles(StandardArticle standardArticle)
        {
            var context = new DTO.Context()
            {
                SiteContext = _applicationContext.GetSiteContext()
            };
            var articleReference = new ArticleReference(standardArticle.ProductID.GetValueOrDefault(), null, null, null, ArticleCatalog.FnacDirect);
            return _siteManagerBusiness2.GetLinkedArticles(context, articleReference, LinkedType.NodeServices, null, null, null);
        }


        #region privates

        private bool IsStandardArticleValid(StandardArticle standardArticle)
        {
            // cf step GetArticleDetailV2
            return !standardArticle.InitialProductId.HasValue
                || standardArticle.InitialProductId.Value == standardArticle.ProductID;
        }

        private void CheckAvailabilityForStandardArticle(StandardArticle standardArticle)
        {
            // cf step GetArticleDetailV2 / step RemoveUnavailableArticleV2 / class DefaultUnavailableArticleHandler
            if ((standardArticle.Type == Base.Model.ArticleType.Saleable && !standardArticle.IsForSale)
                || standardArticle.IsAvailable == false
                || standardArticle.Quantity <= 0)
            {
                standardArticle.Availability = StockAvailability.None;
            }
        }

        private void SetAvailabilityToMPArticle(MarketPlaceArticle mpArticle)
        {
            // cf step GetMPArticleDetailV2 / step RemoveUnavailableOffersV2
            if ((mpArticle.Reservation != null && mpArticle.Reservation.Quantity == 0)
                 || mpArticle.AvailableQuantity <= 0)
            {
                var unavailableArticle = _uiResourceService.GetUIResourceValue("orderpipe.pop.basket.unavailable.article", _applicationContext.GetCulture());
                mpArticle.AvailabilityLabel = unavailableArticle;
                mpArticle.AvailabilityId = (int)AvailabilityEnum.Indisponible;
                mpArticle.AvailableQuantity = 0;
                return;
            }

            // cf step GetMPArticleDetailV2
            var label = _uiResourceService.GetUIResourceValue("OP.MP.Availability", _applicationContext.GetCulture());
            mpArticle.AvailabilityId = (int)AvailabilityEnum.StockPE;
            mpArticle.AvailabilityLabel = label;
        }

        private void ApplyPriceAndPromotionToStandardArticle(StandardArticle standardArticle)
        {
            // cf step ApplyPromotionV2
            var shoppingCart = _pricingService.GetArticlesPrices(
                       _applicationContext.GetSiteContext(),
                       new List<Base.Model.Article> {
                           standardArticle
                       });

            var applyPromotionBusiness = new ApplyPromotionBusiness(shoppingCart, _switchProvider);
            applyPromotionBusiness.ComputeArticleAndGetRewindStatus(standardArticle);

            if (!_switchProvider.IsEnabled("orderpipe.pop.enable.ecotax"))
            {
                standardArticle.EcoTaxCode = string.Empty;
                standardArticle.EcoTaxEur = null;
                standardArticle.EcoTaxEurNoVAT = null;
            }
        }

        private void ApplyPriceAndPromotionToMPArticle(MarketPlaceArticle marketPlaceArticle)
        {
            // cf step ApplyMPPromotionsV2
            marketPlaceArticle.Promotions.Clear();
            marketPlaceArticle.PriceUserEur = marketPlaceArticle.PriceNoVATEur;
            marketPlaceArticle.PriceDBEur = marketPlaceArticle.Offer.Price;
            marketPlaceArticle.PriceRealEur = marketPlaceArticle.PriceNoVATEur;

            // cf step ApplyMPPromotionV3
            if (!_switchProvider.IsEnabled("orderpipe.pop.mpoffer.pricing"))
            {
                return;
            }

            var context = new FnacDirect.Contracts.Online.Model.Context()
            {
                SiteContext = _applicationContext.GetSiteContext(),
                ShoppingCart = new ShoppingCart()
                {
                    LineItems = new List<ShoppingCartLineItem>
                    {
                        new ShoppingCartLineItem()
                        {
                            OfferId = marketPlaceArticle.OfferId,
                            OfferReference = marketPlaceArticle.Offer.Reference,
                            Reference = marketPlaceArticle.ToArticleReference(),
                            Quantity = marketPlaceArticle.Quantity.GetValueOrDefault()
                        }
                    }
                }
            };

            var shoppingCart = _offerPricer.EvaluateShoppingCart(context);

            var applyPromotionBusiness = new ApplyPromotionBusiness(shoppingCart, _switchProvider);
            applyPromotionBusiness.ComputeArticle(marketPlaceArticle);

            marketPlaceArticle.AvailabilityId = (int)AvailabilityEnum.StockPE;
            marketPlaceArticle.AvailabilityLabel = _applicationContext.GetMessage("OP.MP.Availability");
        }
        #endregion

    }
}
