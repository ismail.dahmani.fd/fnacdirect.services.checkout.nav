using System.Web.Routing;
using FnacDirect.OrderPipe.Config;

namespace FnacDirect.OrderPipe.CoreRouting
{
    public interface IOrderPipeResetStrategy
    {
        string DoResetFor(OPContext opContext, bool applyRedirect);
    }

    public interface IOrderPipeResetStrategy<in TDescriptor> : IOrderPipeResetStrategy
        where TDescriptor : IUiPipeDescriptor
    {

    }
}
