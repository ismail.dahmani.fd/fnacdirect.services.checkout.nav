﻿using System.Reflection;
using System.Web.Mvc;

namespace FnacDirect.OrderPipe.BaseMvc.Web.CrossSellBasket
{
    public class HasProductsQueryStringActionMethodSelectorAttribute : ActionMethodSelectorAttribute
    {
        private readonly bool _inversion;

        public HasProductsQueryStringActionMethodSelectorAttribute(bool inversion = false)
        {
            _inversion = inversion;
        }

        public override bool IsValidForRequest(ControllerContext controllerContext, MethodInfo methodInfo)
        {
            foreach(var key in controllerContext.HttpContext.Request.QueryString.AllKeys)
            {
                if(key == "products")
                {
                    return !_inversion;
                }
            }

            return _inversion;
        }
    }
}
