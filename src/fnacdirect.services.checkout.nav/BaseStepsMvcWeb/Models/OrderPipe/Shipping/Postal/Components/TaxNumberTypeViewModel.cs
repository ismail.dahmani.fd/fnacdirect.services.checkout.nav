namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.OnePage.Address
{
    public class TaxNumberTypeViewModel : BaseDropDownViewModel
    {
        public string RegExValidationRule { get; set; }
    }
}
