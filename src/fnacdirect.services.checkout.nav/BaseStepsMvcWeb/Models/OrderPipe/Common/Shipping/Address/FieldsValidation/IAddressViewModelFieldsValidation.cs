namespace FnacDirect.OrderPipe.Base.Model
{
    public interface IAddressViewModelFieldsValidation
    {
        string FirstName { get; set; }

        string LastName { get; set; }

        string GenderId { get; set; }

        string Phone { get; set; }

        string Address { get; set; }

        string CompanyName { get; set; }

        string City { get; set; }

        string State { get; set; }

        string ZipCode { get; set; }

        int CountryId { get; set; }

        int DocumentCountryId { get; set; }

        string Email { get; set; }

        string TaxIdNumber { get; set; }

        string TaxNumberType { get; set; }

        int LegalStatus { get; set; }

    }
}
