﻿namespace FnacDirect.Technical.Framework.Web.Mvc.Implementation
{
    public class UserExtendedContext : IUserExtendedContext
    {
        public virtual IW3Customer Customer
        {
            get
            {
                return FnacContext.Current.Customer;
            }
        }

        public int? CustomerFavoriteStoreId
        {
            get
            {
                return FnacContext.Current.Customer.FavoriteStoreId;
            }
        }

        public Contracts.Online.Model.Customer ContractDto
        {
            get
            {
                return FnacContext.Current.Customer.ContractDTO;
            }
        }

        public IVisitor Visitor
        {
            get
            {
                return FnacContext.Current.Visitor;
            }
        }

        public void SetRegistrationMode(FnacContext.LogonType logonType)
        {
            FnacContext.Current.RegistrationMode = logonType;
        }


        public ISession Session
        {
            get
            {
                return FnacContext.Current.Session;                
            }
        }
    }
}
