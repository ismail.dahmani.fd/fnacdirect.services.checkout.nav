﻿using System.Collections.Generic;
using FnacDirect.Technical.Framework.Web.Mvc.CustomizedContainers;
using FnacDirect.Technical.Framework.Web.Mvc.DataAnnotations;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Membership
{
    public class MembershipBindCardViewModel : IUseACustomizedContainer
    {        
        [Required]
        public int? BirthYear { get; set; }
        
        [Required]
        public int? BirthMonth { get; set; }
        
        [Required]
        public int? BirthDay { get; set; }
        
        public string AdherentCardNumberPrefix { get; set; }
        
        public string AdherentCardNumber { get; set; }

        public int AdherentCardLenght { get; set; }

        public string Error { get; set; }

        public bool IsValid { get; set; }

        public IEnumerable<int> Days { get; set; }

        public IEnumerable<int> Months { get; set; }

        public IEnumerable<string> AdherentCardsPrefix { get; set; }

        public bool DisplayAdherentCardPrefix { get; set; }

        public string CustomizedContainer
        {
            get { return "PopBindMembershipCard"; }
        }
    }
}
