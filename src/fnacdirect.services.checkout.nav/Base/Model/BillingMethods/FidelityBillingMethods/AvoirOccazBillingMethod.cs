using System;
using System.Xml.Serialization;

namespace FnacDirect.OrderPipe.Base.Model
{
    public class AvoirOccazBillingMethod : FidelityBillingMethod
    {
        public static int IdBillingMethod
        {
            get { return 131072; }
        }

        [XmlIgnore] 
        private string _ContractNumber;

        public string ContractNumber
        {
            get { return _ContractNumber; }
            set { _ContractNumber = value; }
        }

        public AvoirOccazBillingMethod()
        {
            BillingMethodId = IdBillingMethod;
            Priority = 10;
            CanModifyAmountDown = false;
            CanModifyAmountUp = false;
            AdvantageCodeType = Membership.Model.Constants.AdvantageCodeType.SIEBEL_AVOIRS_GAMING;
        }
    }
}