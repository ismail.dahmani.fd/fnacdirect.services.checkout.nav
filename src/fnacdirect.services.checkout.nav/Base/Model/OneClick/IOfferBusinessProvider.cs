﻿using FnacDirect.OfferService.IBusiness;
using FnacDirect.Technical.Framework.Web.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FnacDirect.OrderPipe.Base.Business.OneClick
{
    public interface IOfferBusinessProvider
    {
        IOfferBusiness GetOfferBusiness(IApplicationContext applicationContext);
    }
}
