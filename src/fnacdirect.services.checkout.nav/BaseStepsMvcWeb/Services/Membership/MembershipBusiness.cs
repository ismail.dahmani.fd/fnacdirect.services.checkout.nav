using FnacDirect.Customer.Model;
using FnacDirect.Front.AccountMVC.Business.NewsLetter;
using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.OrderPipe.Base.Model.Pop;
using FnacDirect.OrderPipe.Base.Proxy.Customer;
using FnacDirect.Technical.Framework.Web.Mvc;
using System;

namespace FnacDirect.OrderPipe.Base.Proxy.Membership
{
    public class MembershipBusiness : IMembershipBusiness
    {
        private readonly IApplicationContext _applicationContext;
        private readonly ICustomerService _customerService;
        private readonly INewsLetterService _newsletterService;

        public const int NewsletterFnacID = 0;


        public MembershipBusiness(
            IApplicationContext applicationContext,
            INewsLetterService newsletterService,
            ICustomerService customerService)
        {
            _applicationContext = applicationContext;
            _newsletterService = newsletterService;
            _customerService = customerService;
        }

        public void RegisterAccountToNewsletter(PopOrderForm popOrderForm, int accountId)
        {
            var allowMailType = (int)AllowMailType.One;
            _newsletterService.AddNewsLetterSetting(accountId, NewsletterFnacID, allowMailType);

            popOrderForm.UserContextInformations.CustomerEntity.AllowMail = allowMailType;
            _customerService.Update(popOrderForm.UserContextInformations.CustomerEntity);
        }

        public void UpdateMembershipData(PopOrderForm popOrderForm,
            DateTime? membershipBirthdate = null,
            string adherentNieNifType = null,
            string adherentNieNifValue = null)
        {
            if (membershipBirthdate.HasValue)
            {
                popOrderForm.UserContextInformations.CustomerEntity.Birthdate = membershipBirthdate.Value;
            }

            if (adherentNieNifType != null)
            {
                var popAdherentData = popOrderForm.GetPrivateData<PopAdherentData>();
                popAdherentData.TypeId = adherentNieNifType;
            }

            if (adherentNieNifType != null)
            {
                var popAdherentData = popOrderForm.GetPrivateData<PopAdherentData>();
                popAdherentData.NIENIF = adherentNieNifValue;
            }

            _customerService.Update(popOrderForm.UserContextInformations.CustomerEntity);
        }
    }
}
