using System;

namespace FnacDirect.OrderPipe.Base.Model
{
    public class PayOnDeliveryBillingMethod : BillingMethod
    {
        public const int IdBillingMethod = 128;

        public override bool IsPrimary => true;
    
        static int? _idShippingMethod;

        public static void InitIdShippingMethod(int value)
        {
            _idShippingMethod = value;
        }

        public static int GetShippingMethodId()
        {
            if (_idShippingMethod.HasValue)
                return _idShippingMethod.Value;
            else
                throw new Exception("The PayOnDeliveryShipping method hasn't been set.");
        }

        public PayOnDeliveryBillingMethod()
        {
            BillingMethodId = IdBillingMethod;
            CanModifyAmountUp = true;
            CanModifyAmountDown = true;
            ImplyValidateOrder = true;
        }
    }
}
