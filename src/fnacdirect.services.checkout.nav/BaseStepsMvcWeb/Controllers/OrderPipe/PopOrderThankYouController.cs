using System.Web.Mvc;
using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.OrderThankYou;
using FnacDirect.Technical.Framework.Utils;
using FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Omniture;
using FnacDirect.OrderPipe.Base.BaseModel.Model;
using System.Globalization;
using System;
using System.Collections.Generic;
using FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.DataLayer;
using FnacDirect.OrderPipe.BaseMvc.Web.Services.Pop.DataLayer;
using FnacDirect.OrderPipe.BaseMvc.Web.Services.Tracking;
using FnacDirect.OrderPipe.Base.Model.Tracking;
using FnacDirect.OrderPipe.Base.Model.ThankYou;
using FnacDirect.OrderPipe.BaseMvc.Web.Models.Tracking;
using System.Linq;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Controllers.Pop
{
    public class PopOrderThankYouController : PopBaseController
    {
        private readonly OPContext _opContext;
        private readonly IOrderThankYouViewModelBuilder _orderThankYouViewModelBuilder;
        private readonly IOmnitureViewModelBuilder _omnitureViewModelBuilder;
        private readonly IDataLayerService _dataLayerService;
        private readonly ITrackingService _trackingService;

        public PopOrderThankYouController(OPContext opContext,
                                        IOrderThankYouViewModelBuilder orderThankYouViewModelBuilder,
                                        IOmnitureViewModelBuilder omnitureViewModelBuilder,
                                        IDataLayerService dataLayerService,
                                        ITrackingService trackingService)
        {
            _opContext = opContext;
            _orderThankYouViewModelBuilder = orderThankYouViewModelBuilder;
            _omnitureViewModelBuilder = omnitureViewModelBuilder;
            _dataLayerService = dataLayerService;
            _trackingService = trackingService;
        }

        [ChildActionOnly]
        public ActionResult ConversionFollow()
        {
            var popOrderForm = _opContext.OF as PopOrderForm;

            var trackingData = popOrderForm.GetPrivateData<TrackingData>();

            if (trackingData != null)
            {
                var trackingOrder = trackingData.Value;

                if (trackingOrder != null)
                {
                    var conversionFollowViewModel = new ConversionFollowViewModel
                    {
                        OrderUserReference = trackingOrder.OrderUserReference,
                        TotalPrice = trackingOrder.TotalPrice.ToString(CultureInfo.InvariantCulture)
                    };

                    return PartialView("_ConversionFollow", Maybe<ConversionFollowViewModel>.Some(conversionFollowViewModel));
                }
            }

            return PartialView("_ConversionFollow", Maybe<ConversionFollowViewModel>.Empty());
        }

        [HttpGet]
        public ViewResult Index()
        {
            var popOrderForm = _opContext.OF as PopOrderForm;

            var tvArticleTypeIds = RangeSet.Parse<int>(_opContext.Pipe.GlobalParameters["tv.article.typeid"]);
            var orderThankYouViewModel = _orderThankYouViewModelBuilder.Build(_opContext, popOrderForm, tvArticleTypeIds);

            var trackingModel = new DatalayerViewModel()
            {
                PageType = PageTypeEnum.OrderThankYou,
                PopOrderForm = popOrderForm,
                OpContext = _opContext
            };

            var viewResult = View(orderThankYouViewModel);
            //Datalayer and Tracking switchabled
            _dataLayerService.ComputeTransaction(trackingModel, viewResult as ViewResultBase, ControllerContext);

            var thankYouOrderDetailData = popOrderForm.GetPrivateData<ThankYouOrderDetailData>(false);
            if(!thankYouOrderDetailData.IsKafkaOrderTrackingSent)
            {
                var trackingModels = new List<TrackingArticleModel>();
                foreach (var orderDetail in thankYouOrderDetailData.ThankYouOrdersDetails)
                {
                    trackingModels.AddRange(orderDetail.Articles.Select(a => new TrackingArticleModel()
                    {
                        Offer = a.OfferRef.ToStringOrDefault(),
                        Prid = a.Prid,
                        Quantity = a.Quantity,
                    }).ToList());
                }

                _trackingService.TrackArticles(popOrderForm, trackingModels, EventTracking.Order);

                thankYouOrderDetailData.IsKafkaOrderTrackingSent = true;

                Rewind();
            }

            return viewResult;
        }
    }
}
