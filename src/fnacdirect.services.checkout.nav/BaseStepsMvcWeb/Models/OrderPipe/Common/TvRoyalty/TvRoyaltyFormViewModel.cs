﻿using System.Collections.Generic;
using FnacDirect.OrderPipe.BaseMvc.Web.Models.Common;
using FnacDirect.Technical.Framework.Web.Mvc.CustomizedContainers;
using System;
using FnacDirect.Technical.Framework.Web.Mvc.DataAnnotations;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop
{
    public class TvRoyaltyFormViewModel
    {
        /// <summary>
        /// Appellation (Mr, MMe, Société, S.A...)
        /// </summary>
        [DisplayName("orderpipe.pop.tvroyalty.civility")]
        public int Civility { get; set; }

        [DisplayName("orderpipe.pop.addresses.lastname")]
        [Required(ErrorMessage = "orderpipe.pop.addresses.lastnamerequired")]
        [RegularExpression("^[^0-9()]+$", ErrorMessage = "orderpipe.pop.format.onlyletters")]
        [System.ComponentModel.DataAnnotations.StringLength(50)]
        public string LastName { get; set; }

        [DisplayName("orderpipe.pop.addresses.firstname")]
        [Required(ErrorMessage = "orderpipe.pop.addresses.firstnamerequired")]
        [RegularExpression("^[^0-9()]+$", ErrorMessage = "orderpipe.pop.format.onlyletters")]
        [System.ComponentModel.DataAnnotations.StringLength(50)]
        public string FirstName { get; set; }

        [DisplayName("orderpipe.pop.tvroyalty.streetnumber")]
        [System.ComponentModel.DataAnnotations.StringLength(50)]
        public string StreetNumber { get; set; }

        [DisplayName("orderpipe.pop.tvroyalty.streetrank")]
        public string StreetRank { get; set; }

        [DisplayName("orderpipe.pop.tvroyalty.streettype")]
        public int StreetType { get; set; }

        [DisplayName("orderpipe.pop.tvroyalty.firstline")]
        [Required(ErrorMessage = "orderpipe.pop.tvroyalty.firstlinerequired")]
        [System.ComponentModel.DataAnnotations.StringLength(42)]
        public string StreetAndNumber { get; set; }

        [DisplayName("orderpipe.pop.tvroyalty.secondline")]
        [System.ComponentModel.DataAnnotations.StringLength(42)]
        public string OptionalAddress { get; set; }

        [DisplayName("orderpipe.pop.tvroyalty.postalcode")]
        [Required(ErrorMessage = "orderpipe.pop.addresses.zipcoderequired")]
        [System.ComponentModel.DataAnnotations.StringLength(5)]
        [RegularExpression(@"^\d{5}", ErrorMessage = "orderpipe.pop.format.onlynumbers")]
        public string PostalCode { get; set; }

        [DisplayName("orderpipe.pop.addresses.city")]
        [Required(ErrorMessage = "orderpipe.pop.addresses.cityrequired")]
        [System.ComponentModel.DataAnnotations.StringLength(30)]
        public string City { get; set; }

        public int TerritoryOfUse { get; set; }

        [DisplayName("orderpipe.pop.tvroyalty.territoryofuse")]
        public int TvUse { get; set; }

        [DisplayName("orderpipe.pop.tvroyalty.birthplace")]
        [Required(ErrorMessage = "orderpipe.pop.tvroyalty.birthplacerequired")]
        [System.ComponentModel.DataAnnotations.StringLength(30)]
        public string BirthPlace { get; set; }

        [DisplayName("orderpipe.pop.tvroyalty.birthdepartment")]
        [Required(ErrorMessage = "orderpipe.pop.tvroyalty.birthdepartmentrequired")]
        [RegularExpression("^[0-9]{1,2}$", ErrorMessage = "orderpipe.pop.format.onlynumbers")]
        [System.ComponentModel.DataAnnotations.StringLength(2)]
        public string BirthDepartment { get; set; }

        [DisplayName("orderpipe.pop.tvroyalty.birthdate")]
        [Required(ErrorMessage = "orderpipe.pop.tvroyalty.birthyearrequired")]
        public int? BirthYear { get; set; }

        [DisplayName("orderpipe.pop.tvroyalty.birthdate")]
        [Required(ErrorMessage = "orderpipe.pop.tvroyalty.birthmonthrequired")]
        public int? BirthMonth { get; set; }

        [DisplayName("orderpipe.pop.tvroyalty.birthdate")]
        [Required(ErrorMessage = "orderpipe.pop.tvroyalty.birthdayrequired")]
        public int? BirthDay { get; set; }

        public IEnumerable<CivilityInfo> Civilities { get; set; }

        public IEnumerable<StreetTypeInfo> StreetTypes { get; set; }

        public IEnumerable<StreetRankInfo> StreetRanks { get; set; }

        public IEnumerable<TerritoryOfUseInfo> TerritoriesOfUse { get; set; }

        public IEnumerable<TvUseInfo> TvUses { get; set; }

        public IEnumerable<int> Days { get; set; }

        public IEnumerable<int> Months { get; set; }

        public IEnumerable<int> Years { get; set; }
    }
}
