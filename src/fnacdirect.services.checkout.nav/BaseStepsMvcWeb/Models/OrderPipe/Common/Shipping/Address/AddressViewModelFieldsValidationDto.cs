using FnacDirect.Customer.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FnacDirect.OrderPipe.Base.Model
{
    public class AddressViewModelFieldsValidationDto : IAddressViewModelFieldsValidation
    {      
        public AddressViewModelFieldsValidationDto(PostalAddress postalAddress)
        {
            GenderId = postalAddress.GenderId.ToString();
            FirstName = postalAddress.Firstname;
            LastName = postalAddress.LastName;
            Phone = GetPhone(postalAddress.Phone, postalAddress.CellPhone);
            Address = postalAddress.AddressLine1;
            CompanyName = postalAddress.Company;
            City = postalAddress.City;
            State = postalAddress.State;
            ZipCode = postalAddress.ZipCode;
            CountryId = postalAddress.CountryId ?? 0;
            DocumentCountryId = postalAddress.DocumentCountryId;
            TaxIdNumber = postalAddress.TaxNumber;
            TaxNumberType = postalAddress.TaxNumberType;
            LegalStatus = postalAddress.LegalStatus;
        }

        private string GetPhone(string phone, string cellPhone)
        {
            return string.IsNullOrEmpty(phone) ? cellPhone : phone;
        }

        public AddressViewModelFieldsValidationDto(BillingAddress billingAddress)
        {
            GenderId = billingAddress.GenderId.ToString();
            FirstName = billingAddress.Firstname;
            LastName = billingAddress.Lastname;
            Phone = GetPhone(billingAddress.Phone, billingAddress.CellPhone);
            Address = billingAddress.AddressLine1;
            CompanyName = billingAddress.Company;
            City = billingAddress.City;
            State = billingAddress.State;
            ZipCode = billingAddress.ZipCode;
            CountryId = billingAddress.CountryId ?? 0;
            DocumentCountryId = billingAddress.DocumentCountryId;
            Email = billingAddress.Email;
            ConfirmEmail = billingAddress.ConfirmEmail;
            TaxIdNumber = billingAddress.TaxIdNumber;
            TaxNumberType = billingAddress.TaxIdNumberType;
            LegalStatus = billingAddress.LegalStatus;
        }

        public AddressViewModelFieldsValidationDto(AddressEntity addressEntity)
        {
            GenderId = addressEntity.GenderId.ToString();
            FirstName = addressEntity.FirstName;
            LastName = addressEntity.LastName;
            Phone = GetPhone(addressEntity.Tel, addressEntity.CellPhone);
            Address = addressEntity.AddressLine1;
            CompanyName = addressEntity.Company;
            City = addressEntity.City;
            State = addressEntity.State;
            ZipCode = addressEntity.Zipcode;
            CountryId = addressEntity.CountryId;
            DocumentCountryId = addressEntity.DocumentCountryId;
            Email = addressEntity.EMail;
            TaxIdNumber = addressEntity.TaxIdNumber;
            TaxNumberType = addressEntity.TaxIdNumberType;
            LegalStatus = addressEntity.LegalStatusId;
        }

        public string GenderId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Phone { get; set; }
        public string Address { get; set; }
        public string CompanyName { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string ZipCode { get; set; }
        public int CountryId { get; set; }
        public int DocumentCountryId { get; set; }
        public string Email { get; set; }
        public string ConfirmEmail { get; set; }
        public string TaxIdNumber { get; set; }
        public string TaxNumberType { get; set; }
        public int LegalStatus { get; set; }
    }
}

