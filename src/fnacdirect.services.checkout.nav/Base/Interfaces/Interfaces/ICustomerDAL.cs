using FnacDirect.OrderPipe.Base.BaseModel.Model;
using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.OrderPipe.CM.Model;
using System.Collections.Generic;

namespace FnacDirect.OrderPipe.Base.DAL
{
    public interface ICustomerDAL
    {
        ShippingAddress GetDefaultShippingAddress(string userGuid);
        AccountId GetAccountId(string userGuid);
        List<CustomerWithAddress> FindCustomers(Dictionary<CustomerSearchField, string> criterias);
        List<string> SearchAutogeneratedMailAccount(string firstname, string lastname, string domain);
        bool AddBankAccountNumber(int accountId, string bankAccountNumber);
        List<State> GetState();
        List<Partner> GetPartners();
        CreditCardBillingMethod GetInformationCreditCardBillingMethod(string userGuid, int? identity, bool? numByPhone);
    }
}
