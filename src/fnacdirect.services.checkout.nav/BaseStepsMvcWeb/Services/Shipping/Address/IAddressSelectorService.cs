﻿using System.Collections.Generic;
using FnacDirect.OrderPipe.Base.Model;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Services.Pop.Shipping
{
    public interface IAddressSelectorService
    {
        void SelectAddressForSellers(OPContext opContext, PopOrderForm popOrderForm, int currentSellerId, IEnumerable<int> sellers);
    }
}