﻿using FnacDirect.Technical.Framework;
using System;
using System.Collections.Generic;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Services.ShipToStore
{
    public interface IStoreSearchService
    {
        FnacStore.Model.FnacStore GetStore(int storeId);

        IEnumerable<StoreSearchGeoResult> GetStoreNearStore(int storeId);
        IEnumerable<StoreSearchGeoResult> GetStoreNearStore(FnacStore.Model.FnacStore fnacStore);
        IEnumerable<StoreSearchGeoResult> GetStoreByGeoLocation(double latitude, double longitude );
        IEnumerable<StoreSearchGeoResult> GetStoreByGeoLocationandName(double latitude, double longitude, string city);

  
    }
}