﻿using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Common.Payment;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Basket
{
    public class OptionsViewModelBuilder : IOptionsViewModelBuilder
    {
        private readonly IPaymentViewModelBuilder _paymentViewModelBuilder;
        private readonly ISplitViewModelBuilder _splitViewModelBuilder;
        private readonly IWrapViewModelBuilder _wrapViewModelBuilder;
        private readonly IHideBillingInfoViewModelBuilder _hideBillingInfoViewModelBuilder;

        public OptionsViewModelBuilder(IPaymentViewModelBuilder paymentViewModelBuilder, 
            ISplitViewModelBuilder splitViewModelBuilde, 
            IWrapViewModelBuilder wrapViewModelBuilder,
            IHideBillingInfoViewModelBuilder hideBillingInfoViewModelBuilder)
        {
            _paymentViewModelBuilder = paymentViewModelBuilder;
            _splitViewModelBuilder = splitViewModelBuilde;
            _wrapViewModelBuilder = wrapViewModelBuilder;
            _hideBillingInfoViewModelBuilder = hideBillingInfoViewModelBuilder;
        }
        public OptionsViewModel Build(PopOrderForm popOrderForm)
        {

            return new OptionsViewModel
            {
                Promotion = _paymentViewModelBuilder.BuildPromotionCode(popOrderForm),
                Split = _splitViewModelBuilder.Build(popOrderForm),
                Wrap = _wrapViewModelBuilder.Build(popOrderForm),
                HideBillingInfo = _hideBillingInfoViewModelBuilder.Build(popOrderForm)
            };
        }
    }
}
