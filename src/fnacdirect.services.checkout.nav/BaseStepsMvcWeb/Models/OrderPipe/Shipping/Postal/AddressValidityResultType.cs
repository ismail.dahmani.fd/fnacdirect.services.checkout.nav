namespace FnacDirect.OrderPipe.Base.Business.Shipping.Postal
{
    public enum AddressValidityResultType
    {
        Valid,
        Recheck,
        Invalid
    }
}