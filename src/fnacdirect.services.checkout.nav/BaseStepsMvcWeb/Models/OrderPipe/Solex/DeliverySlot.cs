using FnacDirect.Technical.Framework.CoreServices;
using System;

namespace FnacDirect.OrderPipe.Base.Model.Solex
{
    public class DeliverySlot
    {
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public bool IsAvailable { get; set; }
        public decimal Price { get; set; }
        public string SlotType { get; set; }
        public string PartnerId { get; set; }
        public string PartnerUniqueIdentifier { get; set; }
        public bool IsSelected { get; set; }
        public bool HasChanged { get; set; }
        public bool PriceHasChanged { get; set; }
        public SerializableDictionary<string, string> PartnerInformations { get; set; }
        public int ShippingMethodId { get; set; }
        public string HoursTitle { get { return $"{StartDate.Hour}h - {EndDate.Hour}h"; } }
    }
}
