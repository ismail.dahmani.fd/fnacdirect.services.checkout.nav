using System;
using System.Linq;
using System.Collections.Generic;
using FnacDirect.OrderPipe.Base.Model.LineGroups;
using DTO = FnacDirect.Contracts.Online.Model;
using FnacDirect.Technical.Framework.Utils;
using System.Text;

namespace FnacDirect.OrderPipe.Base.Model
{
    public class ArticleList : List<Article>
    {
        public ArticleList() : base() { }
        public ArticleList(IEnumerable<Article> articles) : base(articles) { }

        public Article GetByPrid(int prid)
        {
            return Find(art => (art.ProductID == prid));
        }

        public Article GetById(int id)
        {
            return Find(art => (art.ID == id));
        }

        public bool Modified { get; set; }

        public bool ContainsArticleType(List<int> types)
        {
            foreach (var art in this)
            {
                if (art.TypeId.HasValue)
                {
                    if (types.Contains(art.TypeId.Value))
                    {
                        return true;
                    }
                }
            }

            return false;
        }

        public bool ContainsArticleType(RangeSet<int> types)
        {
            foreach (var art in this)
            {
                if (!art.TypeId.HasValue)
                {
                    continue;
                }

                if (types.Contains(art.TypeId.Value))
                {
                    return true;
                }
            }

            return false;
        }

        public bool ContainsSupport(int supportId)
        {
            foreach (var art in this)
            {
                if (art is StandardArticle standardArticle && standardArticle.SupportId == supportId)
                {
                    return true;
                }
            }

            return false;
        }

        public static implicit operator ArticleList(StandardArticle v)
        {
            throw new NotImplementedException();
        }

        public override string ToString()
        {
            var articles = new StringBuilder();
            ForEach(a => articles.AppendLine(a.ToString()));
            return articles.ToString();
        }
    }

    public class LogisticTypeList : List<LogisticType>
    {
        public LogisticType GetByIndex(int index)
        {
            return Find(delegate (LogisticType typ) { return (typ.Index.GetValueOrDefault(0) == index); });
        }
    }

    public class ShippingAddressList : List<ShippingAddress>
    {
    }

    public class BillingAddressList : List<BillingAddress>
    {
    }

    public class BillingMethodList : List<BillingMethod>
    {
    }

    [Serializable]
    public class PromotionList : List<Promotion>
    {
    }

    [Serializable]
    public class ServiceList : List<Service>
    {
    }

    [Serializable]
    public class WrapMethodList : List<WrapMethod>, IAdditionalFeeMethodList
    {
        public IEnumerable<IAdditionalFeeMethod> List
        {
            get
            {
                foreach (var method in this)
                    yield return method;
            }
        }
    }

    public interface IAdditionalFeeMethodList
    {
        IEnumerable<IAdditionalFeeMethod> List
        {
            get;
        }
    }
    public class ShippingMethodList : List<ShippingMethod>, IAdditionalFeeMethodList
    {
        public IEnumerable<IAdditionalFeeMethod> List
        {
            get
            {
                foreach (var method in this)
                    yield return method;
            }
        }
    }

    public static class LineGroupsExtensions
    {
        /// <summary>
        /// Retourne la liste complète des articles des LineGroups.
        /// </summary>
        public static IEnumerable<Article> GetArticles(this IEnumerable<ILineGroup> lineGroups)
        {
            return lineGroups.SelectMany(l => l.Articles);
        }

        public static IEnumerable<T> GetArticlesOfType<T>(this IEnumerable<ILineGroup> lineGroups) where T : Article
        {
            return lineGroups.SelectMany(l => l.Articles.OfType<T>());
        }
        public static bool Modified(this IEnumerable<ILineGroup> lineGroups)
        {
            return lineGroups.Any(l => l.Modified());
        }

        public static IEnumerable<ILineGroup> Fnac(this IEnumerable<ILineGroup> lineGroups)
        {
            return lineGroups.Where(l => l.HasFnacComArticle);
        }

        public static IEnumerable<ILineGroup> MarketPlace(this IEnumerable<ILineGroup> lineGroups)
        {
            return lineGroups.Where(l => l.HasMarketPlaceArticle);
        }

        public static IEnumerable<ILineGroup> Pro(this IEnumerable<ILineGroup> lineGroups)
        {
            return lineGroups.Where(l => l.IsPro);
        }
    }

    public class LineGroupList : List<LineGroup>
    {
        public LineGroup GetBySeller(int? sellerId)
        {
            foreach (var lineGroup in this)
                if (lineGroup.Seller.SellerId == sellerId)
                    return lineGroup;
            return null;
        }

        public LineGroup GetByCcvAmount(decimal? amount)
        {
            foreach (var lineGroup in this)
                foreach (var article in CollectionUtils.FilterOnType<VirtualGiftCheck>(lineGroup.Articles))
                    if (article.Amount == amount)
                        return lineGroup;
            return null;
        }

        public LineGroup GetByShippingAddressAndSeller(int? addressId, int? sellerId)
        {
            foreach (var lineGroup in this)
                if ((lineGroup.ShippingAddress.Identity == addressId) && (lineGroup.Seller.SellerId == sellerId))
                    return lineGroup;
            return null;
        }

        /// <summary>
        /// Retourne le nombre total d'articles des LineGroups correspondant à l'expression passée en paramètre.
        /// </summary>
        /// <param name="expression">Expression conditionnelle.</param>
        /// <returns></returns>
        public int GetArticlesCountFromLineGroup(Func<LineGroup, bool> expression)
        {
            var articlesCount = 0;

            var lineGroups = this.OfType<LineGroup>().Where(expression).ToList();

            if (lineGroups != null && lineGroups.Count > 0)
                lineGroups.ForEach(l => articlesCount += l.Articles.Count);

            return articlesCount;
        }
        /// <summary>
        /// Retourne le premier article correspodant aux conditions spécifiées.
        /// </summary>
        /// <param name="lineGroupExpression">Condition de filtre sur les LineGroups.</param>
        /// <param name="articleExpression">Condition supplémentaire de filtre sur les articles.</param>
        /// <returns></returns>
        public Article GetFirstArticleFromExpression(Func<LineGroup, bool> lineGroupExpression, Func<Article, bool> articleExpression = null)
        {
            Article article = null;

            var lineGroups = this.OfType<LineGroup>().Where(lineGroupExpression).ToList();
            if (lineGroups != null && lineGroups.Count > 0)
            {
                var index = 0;
                while (article == null && index < lineGroups.Count)
                {
                    if (articleExpression != null)
                        article = lineGroups[index].Articles.Where(articleExpression).FirstOrDefault();
                    else
                        article = lineGroups[index].Articles.FirstOrDefault();

                    index++;
                }
            }

            return article;
        }
        /// <summary>
        /// Récupère la LineGroup contenant l'article spécifié.
        /// </summary>
        /// <param name="article"></param>
        /// <returns></returns>
        public LineGroup GetLineGroupByArticle(Article article)
        {
            return this.OfType<LineGroup>().Where(lg => lg.Articles != null && lg.Articles.Contains(article)).FirstOrDefault();
        }
    }

    /// <summary>
    /// Liste des TVA appliquées dans une commande / dans un panier
    /// </summary>
    [Serializable]
    public class VATAppliedList : List<VATApplied>
    {
        /// <summary>
        /// Retourne le total des TVA
        /// </summary>
        public decimal Total
        {
            get
            {
                decimal total = 0;

                foreach (var vat in this)
                    total += vat.Total;
                return total;
            }
        }

        public VATApplied GetByRate(decimal rate)
        {
            foreach (var vat in this)
                if (vat.Rate == rate)
                    return vat;
            return null;
        }

        /// <summary>
        /// Ajoute une TVA et calcule son total à partir de son taux
        /// </summary>
        /// <param name="vatRate">taux du TVA</param>
        /// <param name="priceValue">valeur du TVA</param>
        public void AddVAT(decimal vatRate, decimal priceValue)
        {
            var vat = GetByRate(vatRate);

            if (vat != null)
                vat.Total += priceValue;
            else
            {
                vat = new VATApplied(vatRate, priceValue);
                Add(vat);
            }
        }
    }

    /// <summary>
    /// Collection de destinataires de ccv rangée par e-mail
    /// </summary>
    public class CcvReceiverInfoCollection : List<FnacDirect.VirtualGiftCheck.Model.ReceiverInfo>
    {
        public void AddReceiver(FnacDirect.VirtualGiftCheck.Model.ReceiverInfo receiver)
        {
            var index = -1;

            index = FindIndex(delegate (FnacDirect.VirtualGiftCheck.Model.ReceiverInfo rc)
                {
                    return rc.Email == receiver.Email;
                });
            if (index >= 0)
            {
                RemoveAt(index);
                Insert(index, receiver);
            }
            else
                Add(receiver);
        }

        public bool ContainsEmail(string email)
        {
            return GetReceiver(email) != null;
        }

        public FnacDirect.VirtualGiftCheck.Model.ReceiverInfo GetReceiver(string email)
        {
            foreach (var receiver in this)
            {
                if (receiver.Email == email)
                    return receiver;
            }

            return null;
        }

        public int GetTotalQuantity() => this.Select(r => r.Quantity.GetValueOrDefault(0)).Sum();
    }

    [Serializable]
    public class OrderTransactionInfoList : List<OrderTransactionInfo>
    {
        public OrderTransactionInfo GetByBillingId(int billingId)
        {
            return this.FirstOrDefault(oti => oti.OrderTransaction != null && oti.OrderTransaction.BillingMethodId == billingId);
        }
    }
}
