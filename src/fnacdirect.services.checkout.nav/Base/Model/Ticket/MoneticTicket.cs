using FnacDirect.OrderPipe.FDV.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FnacDirect.OrderPipe.FDV.Model
{
    [Serializable]
    public class MoneticTicket
    {
        public MoneticTicketType TicketType { get; set; }
        public string Ticket { get; set; }
    }
}
