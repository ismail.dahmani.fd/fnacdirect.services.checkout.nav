using FnacDirect.Customer.Model;
using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.OrderPipe.Base.Model.Facets;

namespace FnacDirect.OrderPipe.Base.Business.Payment
{
    public class CreditCardUsageBusiness : ICreditCardUsageBusiness
    {
        public int? GetUsageFlags(OgoneCreditCardBillingMethod ogoneBM)
        {
            int? result = null;
            CBUsageFlags? flags = null;

            var registerCreditCardOnTR = ogoneBM.SavingCardOnTacitAgreement;
            var registerCreditCardOnCheck = ogoneBM.SavingCardOnCheck;

            if (registerCreditCardOnCheck)
            {
                flags = CBUsageFlags.SubsequentPurchases;
            }
            if (registerCreditCardOnTR)
            {
                flags = flags != null ? flags | CBUsageFlags.TacitRenewal : CBUsageFlags.TacitRenewal;
            }

            if (flags != null)
            {
                result = (int)flags;
            }

            return result;
        }
    }
}
