using FnacDirect.OrderPipe.Base.Business.LogisticLineGroups.Internals.InitialSelectors;
using FnacDirect.OrderPipe.Base.LineGroups;
using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.OrderPipe.Base.Model.LineGroups;
using FnacDirect.Technical.Framework.Switch;
using System.Collections.Generic;
using System.Linq;

namespace FnacDirect.OrderPipe.Base.Business.LogisticLineGroups.InitialSelectors
{
    public class IntramagPTLineGroupInitialSelector : BaseLineGroupInitialSelector
    {
        private readonly ISwitchProvider _switchProvider;


        public IntramagPTLineGroupInitialSelector(ISwitchProvider switchProvider)
        {
            _switchProvider = switchProvider;
        }


        public override IDictionary<ILineGroupHeader, IEnumerable<Article>> Select(IEnumerable<ILineGroup> lineGroups)
        {
            var output = new Dictionary<ILineGroupHeader, IEnumerable<Article>>();

            // Si le contournement de la GUPT n'est pas activé, on traîte les LineGroup
            // Sinon, c'est le ShopSaleLogisticLineGroupGenerationRuleDescriptor qui se charge des PE et PT
            if (!_switchProvider.IsEnabled("orderpipe.pop.basket.bypassGuptForSelfService", false))
            {
                if (!Switch.IsEnabled("orderpipe.pop.fdv.enable", false))
                {
                    return lineGroups.Where(l => l.HasShopArticle && !l.HasIntraMagPE).ToDictionary(l => l as ILineGroupHeader, l => l.Articles.OfType<Article>());
                }

                // On récupère les LineGroups IntraMag où il peut y avoir du PT
                foreach (var lineGroup in lineGroups.Where(l => l.HasShopArticle))
                {
                    var articles = lineGroup.Articles.OfType<StandardArticle>().Where(a => !a.IsShopPE);

                    if (articles.Any())
                    {
                        output.Add(lineGroup, articles);
                    }
                }
            }

            return output;
        }
    }
}
