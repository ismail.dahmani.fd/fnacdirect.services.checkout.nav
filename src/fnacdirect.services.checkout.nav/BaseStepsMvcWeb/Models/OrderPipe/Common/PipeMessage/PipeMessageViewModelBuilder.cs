using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.Technical.Framework.Web.Mvc;
using System.Collections.Generic;
using System.Linq;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Pop.Models.Common
{
    public class PipeMessageViewModelBuilder : IPipeMessageViewModelBuilder
    {
        private readonly IApplicationContext _applicationContext;

        public PipeMessageViewModelBuilder(IApplicationContext applicationContext)
        {
            _applicationContext = applicationContext;
        }

        public List<PipeMessageViewModel> Build(PopOrderForm popOrderForm)
        {
            var pipeMessageViewModelList = new List<PipeMessageViewModel>();

            var pipeMessages = popOrderForm.PipeMessages.GetAll();

            if (!pipeMessages.Any())
            {
                return pipeMessageViewModelList;
            }

            foreach (var pipeMessage in pipeMessages)
            {
                pipeMessageViewModelList.Add(GetPipeMessageViewModel(pipeMessage));

            }

            return pipeMessageViewModelList;
        }

        public List<PipeMessageViewModel> BuildPipeMessageForSpecificCategory(PopOrderForm popOrderForm, string category)
        {
            return popOrderForm.PipeMessages.GetAll().Where(pm => pm.Category == category)
                                                     .OrderBy(pm => pm.Level)
                                                     .Select(pm => GetPipeMessageViewModel(pm))
                                                     .ToList();
        }

        private PipeMessageViewModel GetPipeMessageViewModel(PipeMessage pipeMessage)
        {
            return new PipeMessageViewModel
            {
                Category = pipeMessage.Category,
                RessourceId = pipeMessage.RessourceId,
                Message = _applicationContext.TryGetMessage(pipeMessage.RessourceId, pipeMessage.Parameters.ToArray())
            };
        }

        public PipeMessageViewModel CreatePipeMessageViewModel(string category, string uiResourceId)
        {
            return new PipeMessageViewModel
            {
                Category = category,
                RessourceId = uiResourceId,
                Message = _applicationContext.TryGetMessage(uiResourceId)
            };
        }
    }
}
