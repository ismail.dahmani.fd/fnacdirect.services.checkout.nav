using FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Common.Shipping.Relay;
using FnacDirect.Technical.Framework.Web.Mvc;
using FnacDirect.OrderPipe.Base.Business.Shipping;
using FnacDirect.OrderPipe.Base.Business.Availability;
using FnacDirect.OrderPipe.Core.Services;
using FnacDirect.OrderPipe.Base.Proxy.Relay;
using FnacDirect.OrderPipe.Base.Proxy.Customer;
using FnacDirect.OrderPipe.Base.Business.Shipping.Relay;
using FnacDirect.Solex.Shipping.Client;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Common.Shipping.Relay
{
    public class FdvRelayShippingNetworkViewModelBuilder : RelayShippingNetworkViewModelBuilder
    {
        public FdvRelayShippingNetworkViewModelBuilder(IRelayService relayService,
                                                    ICustomerRelayService customerRelayService,
                                                    IApplicationContext applicationContext,
                                                    IRelayValidityService relayValidityService,
                                                    IPopAvailabilityProcessor popAvailabilityProcessor,
                                                    IPipeParametersProvider pipeParametersProvider,
                                                    IShippingStateService shippingStateService,
                                                    ISwitchProvider switchProvider,
                                                    ISolexBusiness solexBusiness,
                                                    ISolexObjectConverterService solexObjectConverterService,
                                                    IShippingChoiceCostService shippingChoiceCostService)
            : base(relayService,
                  customerRelayService,
                  applicationContext,
                  relayValidityService,
                  popAvailabilityProcessor,
                  pipeParametersProvider,
                  shippingStateService,
                  solexBusiness,
                  switchProvider,
                  solexObjectConverterService,
                  shippingChoiceCostService)
        {

        }

        protected override PriceFormat PriceFormat { get { return PriceFormat.Default; } }
    }
}
