﻿using FnacDirect.OrderPipe.Base.Model.Facets;
using FnacDirect.OrderPipe.Core.Services;
using System.Text.RegularExpressions;

namespace FnacDirect.OrderPipe.Base.Business.Shipping.Relay
{
    public class RelayCellphoneService : IRelayCellphoneService
    {
        private readonly IPipeParametersProvider _pipeParametersProvider;

        public RelayCellphoneService(IPipeParametersProvider pipeParametersProvider)
        {
            _pipeParametersProvider = pipeParametersProvider;
        }

        public bool SetRelayCellPhone(string cellphone, IGotShippingMethodEvaluation iGotShippingMethodEvaluation)
        {
            //Verifiy
            var format = _pipeParametersProvider.Get("orderpipe.relaydetails.cellphone.regexformat", "^(06|07)([0-9]){8}$");

            if (!string.IsNullOrEmpty(cellphone) && !Regex.IsMatch(cellphone, format))
            {
                iGotShippingMethodEvaluation.AddErrorMessage("Shipping.Relay.SMS", "orderpipe.pop.shipping.sms.number.error");

                return false;
            }

            //Set
            iGotShippingMethodEvaluation.RelayCellphone = cellphone;

            return true;
        }
    }
}
