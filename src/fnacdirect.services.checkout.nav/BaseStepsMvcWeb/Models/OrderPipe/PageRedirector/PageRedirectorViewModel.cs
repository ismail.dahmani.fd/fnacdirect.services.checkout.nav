﻿using System.Collections.Generic;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Pop.Models.PageRedirector
{
    public class PageRedirectorViewModel
    {
        public string Url { get; set; }
        public IDictionary<string, string> Parameters { get;set; }
    }
}
