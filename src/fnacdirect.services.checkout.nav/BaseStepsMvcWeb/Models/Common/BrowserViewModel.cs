using System.ComponentModel.DataAnnotations;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Pop.ApiControllers.Models
{
    public class BrowserViewModel
    {
        [Required(AllowEmptyStrings=false)]
        public string ColorDepth { get; set; }

        [Required]
        public bool IsJavaEnabled { get; set; }

        [Required(AllowEmptyStrings=false)]
        public string Language { get; set; }

        [Required]
        public int ScreenHeight { get; set; }

        [Required]
        public int ScreenWidth { get; set; }

        [Required]
        public int TimeZone { get; set; }
    }
}
