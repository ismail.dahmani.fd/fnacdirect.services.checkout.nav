namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.OrderPipe.Common.Scoring
{
    public interface IScoringViewModelBuilder
    {
        ScoringViewModel Build();
    }
}
