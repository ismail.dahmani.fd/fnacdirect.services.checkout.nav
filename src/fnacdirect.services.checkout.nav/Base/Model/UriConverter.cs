using System;
using FnacDirect.Technical.Framework.Web.UrlMgr;
using FnacDirect.Technical.Framework.Web;

namespace FnacDirect.OrderPipe
{
    public static class UriConverter
    {
        public static UrlWriter Convert(Config.Uri uri)
        {
            if (string.IsNullOrEmpty(uri.Site))
            {
                if (string.IsNullOrEmpty(uri.Page))
                    if (string.IsNullOrEmpty(uri.Folder))
                        return FnacContext.Current.UrlManagerNew.Sites.CurrentSite.BaseRootUrl.WithPage(uri.Path);
                    else
                        return FnacContext.Current.UrlManagerNew.GetFolder(uri.Folder).WithPage(uri.Path);
                else
                    return null;
                //return FnacContext.Current.UrlManagerNew.GetPage(uri.Page);

            } else
            {
                if (string.IsNullOrEmpty(uri.Page))
                    if (string.IsNullOrEmpty(uri.Folder))
                        return FnacContext.Current.UrlManagerNew.Sites[uri.Site].BaseRootUrl.WithPage(uri.Path);
                    else
                        return FnacContext.Current.UrlManagerNew.Sites[uri.Site].GetFolder(uri.Folder).WithPage(uri.Path);
                else
                    return FnacContext.Current.UrlManagerNew.Sites[uri.Site].GetPage(uri.Page).DefaultUrl;
            }
        }
    }
}
