using FnacDirect.OrderPipe.Base.BaseModel;
using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.OrderPipe.Base.Model.FDV;
using FnacDirect.OrderPipe.Base.Model.LineGroups;
using FnacDirect.OrderPipe.Base.Model.Solex;
using FnacDirect.Solex.Shipping.Client.Contract;
using System.Collections.Generic;

namespace FnacDirect.OrderPipe.Base.Business.Shipping
{
    public interface IShippingEvaluationBusiness
    {
        IEnumerable<ShippingChoice> GetFirstChoices<T>(ShippingNetwork<T> shippingNetwork) where T : Address;
        IEnumerable<ShippingChoice> GetPostalChoices(ShippingMethodEvaluationGroup shippingMethodEvaluationGroup, EvaluateShoppingCartResult evaluateShoppingCartResult);
        ShippingChoice GetBestChoiceForDefaultShippingMethod(ShippingMethodEvaluationItem shippingMethodEvaluationItem, DeliverySlotData deliverySlotData = null);
        ShippingChoice GetForcedChoiceForShopOrders(IEnumerable<ILineGroup> lineGroups, ShopInfosData shopInfoData);
        bool IsDummyAddress(int sellerId, ShippingMethodEvaluation shippingMethodEvaluation);
        void UpdateSelectedShippingMethodEvaluationItem(int shippingMethodId, int sellerId, ShippingMethodEvaluation shippingMethodEvaluation);
        bool IsArticleShippedInStore(Article article, ShippingMethodEvaluationGroup shippingMethodEvaluationGroup);
    }
}
