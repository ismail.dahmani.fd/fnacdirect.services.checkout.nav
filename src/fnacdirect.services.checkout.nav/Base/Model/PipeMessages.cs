using FnacDirect.Contracts.Online.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace FnacDirect.OrderPipe
{
    public class PipeMessages : IXmlSerializable
    {
        private readonly List<PipeMessage> _list
            = new List<PipeMessage>();

        internal IList<PipeMessage> List
        {
            get
            {
                return _list;
            }
        }

        public IEnumerable<PipeMessage> Category(string category)
        {
            return _list.Where(i => i.Category == category);
        }

        internal void AutoClearFor(string stepName)
        {
            _list.RemoveAll(i => i.Behavior == PipeMessageBehavior.AutoClear && i.StepName == stepName);
        }

        public void ClearFor(string stepName)
        {
            _list.RemoveAll(i => i.Behavior != PipeMessageBehavior.ForcePersist && i.StepName == stepName);
        }

        public void ClearForCategory(string category)
        {
            _list.RemoveAll(i => i.Behavior != PipeMessageBehavior.ForcePersist && i.Category == category);
        }

        public void Clear()
        {
            _list.RemoveAll(i => i.Behavior != PipeMessageBehavior.ForcePersist);
        }

        public void RemoveForcePersistedMessages()
        {
            _list.RemoveAll(i => i.Behavior == PipeMessageBehavior.ForcePersist);
        }

        public IEnumerable<PipeMessage> GetAll()
        {
            return _list;
        }

        internal void CopyToUserMessages(UserMessages userMessages)
        {
            foreach (var pipeMessage in _list)
            {
                var hasMatching = false;

                foreach (var userMessage in userMessages.GetMessages())
                {
                    if (userMessage.Category == pipeMessage.Category
                        && userMessage.MessageId == pipeMessage.RessourceId)
                    {
                        hasMatching = true;
                    }
                }

                var level = UserMessage.MessageLevel.Error;

                switch (pipeMessage.Level)
                {
                    case PipeMessageLevel.Error:
                        level = UserMessage.MessageLevel.Error;
                        break;
                    case PipeMessageLevel.Warning:
                        level = UserMessage.MessageLevel.Warning;
                        break;
                }

                if (!hasMatching)
                {
                    if (pipeMessage.Parameters.Any())
                        userMessages.Add(pipeMessage.Category, level, pipeMessage.RessourceId, pipeMessage.Parameters);
                    else
                        userMessages.Add(pipeMessage.Category, level, pipeMessage.RessourceId);
                }
            }
        }

        public XmlSchema GetSchema()
        {
            return null;
        }

        public void ReadXml(XmlReader reader)
        {
            _list.Clear();

            var isEmpty = reader.IsEmptyElement;

            reader.Read();

            if (isEmpty)
            {
                return;
            }

            while (reader.NodeType == XmlNodeType.Whitespace)
            {
                reader.Read();
            }

            while (reader.NodeType == XmlNodeType.Element && reader.Name == "message")
            {
                var stepName = reader["step"];
                var category = reader["category"];
                var ressourceId = reader["ressourceid"];
                var level = (PipeMessageLevel)Enum.Parse(typeof(PipeMessageLevel), reader["level"]);
                var behavior = (PipeMessageBehavior)Enum.Parse(typeof(PipeMessageBehavior), reader["behavior"]);
                var parameters = new List<string>();

                reader.ReadToNextElementOrEndElement();

                while (reader.NodeType == XmlNodeType.Element && reader.Name == "parameter")
                {
                    var value = reader["value"];

                    parameters.Add(value);

                    reader.ReadToNextElementOrEndElement();
                }

                _list.Add(new PipeMessage(stepName, category, ressourceId, level, parameters.ToArray()) { Behavior = behavior });

                if (parameters.Any())
                {
                    reader.ReadToNextElementOrEndElement();
                }
            }

            if (reader.NodeType == XmlNodeType.EndElement)
            {
                reader.ReadEndElement();
            }
        }

        public void WriteXml(XmlWriter writer)
        {
            foreach (var pipeMessage in _list)
            {
                writer.WriteStartElement("message");

                writer.WriteAttributeString("step", pipeMessage.StepName);
                writer.WriteAttributeString("category", pipeMessage.Category);
                writer.WriteAttributeString("ressourceid", pipeMessage.RessourceId);
                writer.WriteAttributeString("level", pipeMessage.Level.ToString());
                writer.WriteAttributeString("behavior", pipeMessage.Behavior.ToString());

                foreach (var parameter in pipeMessage.Parameters)
                {
                    writer.WriteStartElement("parameter");

                    writer.WriteAttributeString("value", parameter);

                    writer.WriteEndElement();
                }

                writer.WriteEndElement();
            }
        }
    }
}
