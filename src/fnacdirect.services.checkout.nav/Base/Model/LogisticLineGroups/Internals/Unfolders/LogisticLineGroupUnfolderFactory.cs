﻿using System;

namespace FnacDirect.OrderPipe.Base.Business.LogisticLineGroups.Internals.Unfolders
{
    public class LogisticLineGroupUnfolderFactory<T> : ILogisticLineGroupUnfolderFactory<T>
        where T : BaseLogisticLineGroupUnfolder
    {
        private readonly Func<T> _factory;

        public LogisticLineGroupUnfolderFactory(Func<T> factory)
        {
            _factory = factory;
        }

        public ILogisticLineGroupUnfolder Get(ILogisticLineGroupGenerationProcessContext context)
        {
            var instance = _factory();

            instance.Context = context;

            return instance;
        }
    }
}
