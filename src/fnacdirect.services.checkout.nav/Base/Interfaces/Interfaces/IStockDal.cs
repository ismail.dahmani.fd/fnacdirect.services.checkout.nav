using System;

namespace FnacDirect.OrderPipe.Base.DAL
{
    public interface IStockDal
    {
        int TryReserve(string orderUserReference, int odetId, int refgu, string ean, string eanBundle, int qty, string market, int userId, DateTime orderDate);
        void AddPromotionStockCount(int promotionId, int quantity, int orderId, int orderLineId, int itemId, int? accountId = null);
    }
}
