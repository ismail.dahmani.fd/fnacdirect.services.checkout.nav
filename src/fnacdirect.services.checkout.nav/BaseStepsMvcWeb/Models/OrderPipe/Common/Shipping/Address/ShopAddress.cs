using System;
using System.Xml.Serialization;
using FM = FnacDirect.FnacStore.Model;

namespace FnacDirect.OrderPipe.Base.Model
{
    public class ShopAddress : ShippingAddress
    {
        [XmlIgnore]
        public int? CityId { get; set; }

        [XmlIgnore]
        public int? PhotoID { get; set; }

        [XmlIgnore]
        public string LegalEntity { get; set; }

        [XmlIgnore]
        public string RefUG { get; set; }

        [XmlIgnore]
        public string Country { get; set; }

        [XmlIgnore]
        public string City { get; set; }

        [XmlIgnore]
        public string Country_Code2 { get; set; }

        [XmlIgnore]
        public string ZipCode { get; set; }

        [XmlIgnore]
        public string ExceptionnalOpening { get; set; }

        [XmlIgnore]
        public string Opening { get; set; }

        [XmlIgnore]
        public string Address { get; set; }

        [XmlIgnore]
        public string Name { get; set; }

        [XmlIgnore]
        public StoreType StoreTypeId { get; set; }

        public string FnacName
        {
            get
            {
                return string.Format("FNAC {0}", Name);
            }
        }

        public string GetFormatName()
        {
            return StoreTypeId == StoreType.Fnac ? string.Format("FNAC {0}", Name) : Name;
        }

        [XmlIgnore]
        public override string State
        {
            get
            {
                return null;
            }
            set
            {
            }
        }

        public override bool IsDummy()
        {
            return !Identity.HasValue || Identity.Value == -1;
        }

        public override int Type
        {
            get
            {
                return 3;
            }
        }

        public ShopAddress()
        {
        }


        public ShopAddress(FM.FnacStore s)
        {
            Copy(s, this);
        }

        public static void Copy(FM.FnacStore s, ShopAddress sa)
        {
            if (s == null || sa == null)
            {
                return;
            }

            sa.Name = s.Name;
            sa.StoreTypeId = (StoreType)s.StoreTypeId;
            sa.Address = s.Address;
            sa.City = s.City?.Name;
            sa.ExceptionnalOpening = s.ExceptionalOpeningHours;
            sa.Opening = s.OpeningHours;
            sa.ZipCode = s.ZipCode;
            sa.RefUG = s.RefUG.ToString();
            sa.Identity = s.Id;
            sa.IsCountryUseVAT = true; //Les magasins sont en France
            sa.ShippingZone = 1;
            sa.Phone = s.Phone;

            if (s.Country != null)
            {
                sa.CountryId = s.Country.Id;
                sa.CountryCode2 = s.Country.Code;
            }
        }
    }
}
