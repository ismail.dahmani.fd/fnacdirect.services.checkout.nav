using FnacDirect.OrderPipe.Base.Model;

namespace FnacDirect.OrderPipe.Base.LineGroups
{
    public interface ILineGroupInformations
    {
        int? OrderType { get; set; }

        OrderTransactionInfoList OrderTransactionInfos { get; set; }

        bool GiftOption { get; set; }
        string OrderMessage { get; set; }
        int ChosenWrapMethod { get; set; }

        int? OrderStatusId { get; set; }
        
        int ShippingAddressId { get; set; }
    }
}
