using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.Technical.Framework.Utils;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Sorry
{
    public interface IOrderSorryViewModelBuilder
    {
        OrderSorryViewModel Build(OPContext opContext, PopOrderForm popOrderForm);
    }
}
