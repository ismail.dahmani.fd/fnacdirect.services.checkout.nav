using FnacDirect.Membership.Model.Constants;
using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.OrderPipe.Base.Model.LineGroups;
using FnacDirect.OrderPipe.Base.Proxy.Appointment;
using FnacDirect.OrderPipe.Base.Proxy.Membership;
using FnacDirect.Technical.Framework.Utils;
using FnacDirect.Technical.Framework.Web;
using FnacDirect.Technical.Framework.Web.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Common.Payment
{
    public class TermsAndConditionsBuilder : ITermsAndConditionsBuilder
    {
        private enum TermsAndConditionType
        {
            FnacCom,
            MarketPlace,
            Numerical,
            DematSoft,
            Membership,
            ExpressPlus,
            FnacPlus
        }

        private class TermsAndConditions
        {
            private readonly IApplicationContext _applicationContext;
            private readonly IUrlManager _urlManager;
            private readonly IAppointmentService _appointmentService;

            private readonly StringBuilder _stringBuilder
                = new StringBuilder();

            private string GetContentFor(TermsAndConditionType termsAndConditionType)
            {
                switch (termsAndConditionType)
                {
                    case TermsAndConditionType.FnacCom:
                        var fnaccomurl = _urlManager.Sites.WWWDotNetSecure.GetPage("popcgvfnac").DefaultUrl.ToString();
                        return _applicationContext.GetMessage("orderpipe.pop.termsandconditions.fnaccom", fnaccomurl);
                    case TermsAndConditionType.MarketPlace:
                        var mpUrl = _urlManager.Sites.WWWDotNetSecure.GetPage("popcgvmarket").DefaultUrl.ToString();
                        return _applicationContext.GetMessage("orderpipe.pop.termsandconditions.marketplace", mpUrl);
                    case TermsAndConditionType.Numerical:
                        var numUrl = _urlManager.Sites.WWWDotNetSecure.GetPage("popcgvebook").DefaultUrl.ToString();
                        return _applicationContext.GetMessage("orderpipe.pop.termsandconditions.numerical", numUrl);
                    case TermsAndConditionType.DematSoft:
                        var dematUrl = _urlManager.Sites.WWWDotNetSecure.GetPage("popcgvdematsoft").DefaultUrl.ToString();
                        return _applicationContext.GetMessage("orderpipe.pop.termsandconditions.dematsoft", dematUrl);
                    case TermsAndConditionType.Membership:
                        var membershipUrl = _urlManager.Sites.WWWDotNetSecure.GetPage("popcgvAdhesion").DefaultUrl.ToString();
                        return _applicationContext.GetMessage("orderpipe.pop.termsandconditions.membership", membershipUrl);
                    case TermsAndConditionType.ExpressPlus:
                        var expressUrl = _urlManager.Sites.WWWDotNetSecure.GetPage("popcgvExpressPlus").DefaultUrl.ToString();
                        return _applicationContext.GetMessage("orderpipe.pop.termsandconditions.expressplus", expressUrl);
                    case TermsAndConditionType.FnacPlus:
                        var fnacplusUrl = _urlManager.Sites.WWWDotNetSecure.GetPage("popcgvFnacPlus").DefaultUrl.ToString();
                        return _applicationContext.GetMessage("orderpipe.pop.termsandconditions.fnacplus", fnacplusUrl);
                }

                return string.Empty;
            }

            public TermsAndConditions(IApplicationContext applicationContext, TermsAndConditionType termsAndConditionType, IUrlManager urlManager, IAppointmentService appointmentService)
                : this(applicationContext, urlManager, appointmentService)
            {
                _stringBuilder.Append(applicationContext.GetMessage("orderpipe.pop.termsandconditions.start"));

                _stringBuilder.Append(GetContentFor(termsAndConditionType));
            }

            public TermsAndConditions(IApplicationContext applicationContext, IUrlManager urlManager, IAppointmentService appointmentService, params Service[] warranties)
                : this(applicationContext, urlManager, appointmentService)
            {
                _stringBuilder.Append(applicationContext.GetMessage("orderpipe.pop.termsandconditions.start"));

                _stringBuilder.Append(Warranties(warranties));
            }

            public TermsAndConditions(IApplicationContext applicationContext, IUrlManager urlManager, IAppointmentService appointmentService, params StandardArticle[] services)
                : this(applicationContext, urlManager, appointmentService)
            {
                _stringBuilder.Append(applicationContext.GetMessage("orderpipe.pop.termsandconditions.start"));

                _stringBuilder.Append(Services(services));
            }

            public TermsAndConditions(IApplicationContext applicationContext, IUrlManager urlManager, IAppointmentService appointmentService)
            {
                _applicationContext = applicationContext;
                _urlManager = urlManager;
                _appointmentService = appointmentService;
            }

            private string And()
            {
                return ", ";
            }

            public TermsAndConditions And(TermsAndConditionType termsAndConditionType)
            {
                _stringBuilder.Append(And());

                _stringBuilder.Append(GetContentFor(termsAndConditionType));

                return this;
            }

            public TermsAndConditions WithdrawalNumerical()
            {
                _stringBuilder.Append("<br />");

                _stringBuilder.Append(_applicationContext.GetMessage("orderpipe.pop.termsandconditions.withdrawal.numerical"));

                return this;
            }

            public TermsAndConditions WithdrawalService()
            {
                _stringBuilder.Append("<br />");

                _stringBuilder.Append(_applicationContext.GetMessage("orderpipe.pop.termsandconditions.withdrawal.service"));

                return this;
            }

            public TermsAndConditions Warranties(params Service[] warranties)
            {
                var appointmentId = _appointmentService.GetAppointmentArticleTypeIds();
                var appointmentUrl = _urlManager.Sites.WWWDotNetSecure.GetPage("popcgvservice").DefaultUrl.ToString();
                var warr = warranties.GroupBy(s => s.TypeLabel);
                foreach (var warranty in warr)
                {
                    _stringBuilder.Append(And());

                    _stringBuilder.Append(_applicationContext.GetMessage("orderpipe.pop.termsandconditions.warranty", warranty.Key, appointmentUrl));
                }

                return this;
            }

            public TermsAndConditions Services(params StandardArticle[] services)
            {
                var appointmentId = _appointmentService.GetAppointmentArticleTypeIds();
                var appointmentUrl = _urlManager.Sites.WWWDotNetSecure.GetPage("popcgvservice").DefaultUrl.ToString();
                var svc = services.GroupBy(s => s.TypeLabel);
                foreach (var service in svc)
                {
                    _stringBuilder.Append(And());
                    _stringBuilder.Append(_applicationContext.GetMessage("orderpipe.pop.termsandconditions.warranty", service.Key, appointmentUrl));
                }

                return this;
            }

            public TermsAndConditions AndServices(params StandardArticle[] services)
            {
                var appointmentId = _appointmentService.GetAppointmentArticleTypeIds();
                var appointmentUrl = _urlManager.Sites.WWWDotNetSecure.GetPage("popcgvservice").DefaultUrl.ToString();
                var svc = services.GroupBy(s => s.TypeLabel);
                foreach (var service in svc)
                {
                    _stringBuilder.Append(And());
                    _stringBuilder.Append(_applicationContext.GetMessage("orderpipe.pop.termsandconditions.warranty", service.Key, appointmentUrl));
                }

                return this;
            }

            public TermsAndConditions AndWarranties(params Service[] warranties)
            {
                var appointmentId = _appointmentService.GetAppointmentArticleTypeIds();
                var appointmentUrl = _urlManager.Sites.WWWDotNetSecure.GetPage("popcgvservice").DefaultUrl.ToString();
                var warr = warranties.GroupBy(s => s.TypeLabel);
                foreach (var warranty in warr)
                {
                    _stringBuilder.Append(And());

                    _stringBuilder.Append(_applicationContext.GetMessage("orderpipe.pop.termsandconditions.warranty", warranty.Key, appointmentUrl));
                }

                return this;
            }

            public override string ToString()
            {
                return _stringBuilder.ToString();
            }
        }

        private readonly IApplicationContext _applicationContext;
        private readonly IAdherentCardArticleService _adherentCardArticleService;
        private readonly IUrlManager _urlManager;
        private readonly IAppointmentService _appointmentService;
        private readonly IMembershipConfigurationService _membershipConfigurationService;
        private readonly RangeSet<int> _service16000Ids;
        private readonly RangeSet<int> _service19000Ids;
        private readonly RangeSet<int> _serviceOther;
        private readonly int _aboIlliTrialPrid;
        private readonly int _aboIlliPrid;

        public TermsAndConditionsBuilder(IApplicationContext applicationContext, IAdherentCardArticleService adherentCardArticleService, IUrlManager urlManager, IAppointmentService appointmentService, IMembershipConfigurationService membershipConfigurationService)
        {
            _applicationContext = applicationContext;
            _adherentCardArticleService = adherentCardArticleService;
            _urlManager = urlManager;
            _appointmentService = appointmentService;
            _service16000Ids = RangeSet.Parse<int>(_applicationContext.GetAppSetting("Service16000Ids"));
            _service19000Ids = RangeSet.Parse<int>(_applicationContext.GetAppSetting("Service19000Ids"));
            _serviceOther = RangeSet.Parse<int>(_applicationContext.GetAppSetting("ServiceOtherIds"));
            _membershipConfigurationService = membershipConfigurationService;
            var aboIlliTrialPrid = _applicationContext.GetAppSetting("AboIlliTrial.Prid");
            var aboIlliPrid = _applicationContext.GetAppSetting("AboIlli.Prid");
            int.TryParse(aboIlliTrialPrid, out _aboIlliTrialPrid);
            int.TryParse(aboIlliPrid, out _aboIlliPrid);
        }

        public string GetTermsAndConditionsFor(IEnumerable<ILineGroup> lineGroups)
        {
            TermsAndConditions termsAndConditions = null;

            if (lineGroups.Any(l => l.HasFnacComArticle || l.HasCollectInStoreArticles || l.HasIntraMagPE))
            {
                termsAndConditions = termsAndConditions == null ?
                    new TermsAndConditions(_applicationContext, TermsAndConditionType.FnacCom, _urlManager, _appointmentService)
                    : termsAndConditions.And(TermsAndConditionType.FnacCom);
            }

            if (lineGroups.Any(l => l.HasMarketPlaceArticle))
            {
                termsAndConditions = termsAndConditions == null ?
                    new TermsAndConditions(_applicationContext, TermsAndConditionType.MarketPlace, _urlManager, _appointmentService)
                    : termsAndConditions.And(TermsAndConditionType.MarketPlace);
            }

            if (lineGroups.Any(l => l.HasNumericalArticles))
            {
                termsAndConditions = termsAndConditions == null ?
                    new TermsAndConditions(_applicationContext, TermsAndConditionType.Numerical, _urlManager, _appointmentService)
                    : termsAndConditions.And(TermsAndConditionType.Numerical);
            }

            if (lineGroups.Any(l => l.HasDematSoftArticles))
            {
                termsAndConditions = termsAndConditions == null ?
                    new TermsAndConditions(_applicationContext, TermsAndConditionType.DematSoft, _urlManager, _appointmentService)
                    : termsAndConditions.And(TermsAndConditionType.DematSoft);
            }

            var expressPlus = lineGroups.SelectMany(l => l.Articles).OfType<StandardArticle>().Where(art => art.ProductID.HasValue && (art.ProductID == _aboIlliTrialPrid || art.ProductID == _aboIlliPrid));
            if (expressPlus.Any())
            {
                termsAndConditions = termsAndConditions == null ?
                    new TermsAndConditions(_applicationContext, TermsAndConditionType.ExpressPlus, _urlManager, _appointmentService)
                    : termsAndConditions.And(TermsAndConditionType.ExpressPlus);
            }

            if (_membershipConfigurationService.IsFnacPlusEnabled)
            {
                var fnacplus = lineGroups.SelectMany(l => l.Articles).OfType<StandardArticle>().Where(art => _membershipConfigurationService.FnacPlusCardTypes.Contains(art.TypeId.Value));
                if (fnacplus.Any())
                {
                    termsAndConditions = termsAndConditions == null ?
                        new TermsAndConditions(_applicationContext, TermsAndConditionType.FnacPlus, _urlManager, _appointmentService)
                        : termsAndConditions.And(TermsAndConditionType.FnacPlus);
                }
            }

            var services = lineGroups.SelectMany(l => l.Articles).OfType<StandardArticle>().SelectMany(a => a.Services).ToList();
            var all16000Services = lineGroups.SelectMany(l => l.Articles).OfType<StandardArticle>().Where(art => (art.TypeId.HasValue && (_service16000Ids.Contains(art.TypeId.Value) || _serviceOther.Contains(art.TypeId.Value)))).ToArray();

            var waranties = services.Union(lineGroups.SelectMany(l => l.Articles).OfType<Service>())
                               .Where(s => s.TypeId.HasValue && _service19000Ids.Contains(s.TypeId.Value)).ToArray();
            if (all16000Services.Any())
            {
                termsAndConditions = termsAndConditions == null ? new TermsAndConditions(_applicationContext, _urlManager, _appointmentService, all16000Services) : termsAndConditions.AndServices(all16000Services);
            }

            if (waranties.Any())
            {
                termsAndConditions = termsAndConditions == null ? new TermsAndConditions(_applicationContext, _urlManager, _appointmentService, waranties) : termsAndConditions.AndWarranties(waranties);
            }

            var hasAdherentCard = lineGroups.SelectMany(l => l.Articles).Where(a => a.ProductID.HasValue).Any(a => _adherentCardArticleService.IsAdherentCard(a.ProductID.Value));

            if (hasAdherentCard)
            {
                var adherentCardArticle = lineGroups.SelectMany(l => l.Articles).Where(a => a.ProductID.HasValue).First(a => _adherentCardArticleService.IsAdherentCard(a.ProductID.Value));
                if (adherentCardArticle.TypeId == (int)CategoryCardType.Recruit || adherentCardArticle.TypeId == (int)CategoryCardType.Renew)
                {
                    termsAndConditions = termsAndConditions == null ? new TermsAndConditions(_applicationContext, TermsAndConditionType.Membership, _urlManager, _appointmentService) : termsAndConditions.And(TermsAndConditionType.Membership);
                }
            }

            if (termsAndConditions == null)
            {
                return string.Empty;
            }

            if (lineGroups.Any(l => l.HasNumericalArticles) || lineGroups.Any(l => l.HasDematSoftArticles))
            {
                termsAndConditions.WithdrawalNumerical();
            }

            if (waranties.Any() || expressPlus.Any() || hasAdherentCard || all16000Services.Any())
            {
                termsAndConditions.WithdrawalService();
            }
            return termsAndConditions.ToString();
        }

        public TermsAndConditionsTypeViewModel GetTermsAndConditionsTypeViewModel(IEnumerable<ILineGroup> lineGroups)
        {
            var termsAndConditionsTypeViewModel = new TermsAndConditionsTypeViewModel
            {
                PopinsName = new List<string> {
                    "CGVfnacom","PersonnalData"
                },
                PopinsWarrantiesName = new List<string>()
            };

            var eligibileTermsAndConditionType = GetEligibleTermsAndConditionType(lineGroups);

            foreach (var t in eligibileTermsAndConditionType)
            {
                var typeName = Enum.GetName(typeof(TermsAndConditionType), t);
                termsAndConditionsTypeViewModel.PopinsName.Add(typeName);
            }

            var warrantiesLabel = GetEligibleWarrantiesLabel(lineGroups);

            termsAndConditionsTypeViewModel.PopinsWarrantiesName.AddRange(warrantiesLabel);

            return termsAndConditionsTypeViewModel;
        }

        private IEnumerable<TermsAndConditionType> GetEligibleTermsAndConditionType(IEnumerable<ILineGroup> lineGroups)
        {
            var termsAndConditions = new List<TermsAndConditionType>();

            if (lineGroups.Any(l => l.HasFnacComArticle || l.HasCollectInStoreArticles))
            {
                termsAndConditions.Add(TermsAndConditionType.FnacCom);
            }

            if (lineGroups.Any(l => l.HasMarketPlaceArticle))
            {
                termsAndConditions.Add(TermsAndConditionType.MarketPlace);
            }

            if (lineGroups.Any(l => l.HasNumericalArticles))
            {
                termsAndConditions.Add(TermsAndConditionType.Numerical);
            }

            if (lineGroups.Any(l => l.HasDematSoftArticles))
            {
                termsAndConditions.Add(TermsAndConditionType.DematSoft);
            }

            var standardArticles = lineGroups.SelectMany(l => l.Articles).OfType<StandardArticle>();

            var expressPlus = standardArticles.Where(art => art.ProductID.HasValue && (art.ProductID == _aboIlliTrialPrid || art.ProductID == _aboIlliPrid));
            if (expressPlus.Any())
            {
                termsAndConditions.Add(TermsAndConditionType.ExpressPlus);
            }

            if (_membershipConfigurationService.IsFnacPlusEnabled)
            {
                var fnacplus = standardArticles.Where(art => _membershipConfigurationService.FnacPlusCardTypes.Contains(art.TypeId.Value));
                if (fnacplus.Any())
                {
                    termsAndConditions.Add(TermsAndConditionType.FnacPlus);
                }
            }

            var adherentCardArticle = lineGroups.SelectMany(l => l.Articles).Where(a => a.ProductID.HasValue).FirstOrDefault(a => _adherentCardArticleService.IsAdherentCard(a.ProductID.Value));
            if (adherentCardArticle != null)
            {
                switch (adherentCardArticle.TypeId)
                {
                    case (int)CategoryCardType.Recruit:
                    case (int)CategoryCardType.Renew:
                        termsAndConditions.Add(TermsAndConditionType.Membership);
                        break;
                }
            }

            return termsAndConditions;
        }

        private IEnumerable<string> GetEligibleWarrantiesLabel(IEnumerable<ILineGroup> lineGroups)
        {
            var result = new List<string>();

            var all16000Services = lineGroups.SelectMany(l => l.Articles).OfType<StandardArticle>().Where(art => (art.TypeId.HasValue && (_service16000Ids.Contains(art.TypeId.Value) || _serviceOther.Contains(art.TypeId.Value)))).ToArray();
            if (all16000Services.Any())
            {
                result.AddRange(all16000Services.Select(s => s.TypeLabel));
            }

            var services = lineGroups.SelectMany(l => l.Articles).OfType<StandardArticle>().SelectMany(a => a.Services).ToList();
            var waranties = services.Union(lineGroups.SelectMany(l => l.Articles).OfType<Service>())
                               .Where(s => s.TypeId.HasValue && _service19000Ids.Contains(s.TypeId.Value)).ToArray();
            if (waranties.Any())
            {
                result.AddRange(waranties.Select(s => s.TypeLabel));
            }

            return result;
        }
    }
}
