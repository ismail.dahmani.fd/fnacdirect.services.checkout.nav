using FnacDirect.OrderPipe.Base.DAL;
using FnacDirect.OrderPipe.Base.Model;
using System;

namespace FnacDirect.OrderPipe.Base.Business
{
    public class CommerceOrderDetailService : ICommerceOrderDetailService
    {
        private readonly IOrderDetailsDAL _commerceDal;
        private readonly IOrderInsertionDal _orderInsertionDal;

        public CommerceOrderDetailService(IOrderDetailsDAL commerceDal, IOrderInsertionDal orderInsertionDal)
        {
            _commerceDal = commerceDal;
            _orderInsertionDal = orderInsertionDal;
        }

        public void AddOrderDetail(int orderPk, DateTime orderDate, int shippingAddressId, StandardArticle article, PricesOrderDetail prices, string affiliate, int billingMethodId, int? master, int refGu, int splitLevel, DateTime? dtTheoExpDate, bool? isOptinWarranty, int shippingMethodId)
        {
            _orderInsertionDal.AddOrderDetail(
                    article.OrderDetailPk.Value,
                    orderDate,
                    orderPk,
                    article.PriceUserEur.Value == 0 ? 2 : 1,
                    article.Quantity.Value,
                    article.ProductID.Value,
                    article.Label,
                    article.LogType.Value,
                    article.Ean13 ?? "",
                    article.ArticleDelay.Value,
                    article.ShipDelay.GetValueOrDefault(0),
                    // CHECK shipping method 0=5
                    shippingMethodId,
                    article.WrapMethod.GetValueOrDefault(0),
                    shippingAddressId,
                    billingMethodId,
                    prices.PriceDBEur,
                    prices.PriceUserEur,
                    0,
                    prices.ShipPriceToPay,
                    prices.ShipPriceToPay,
                    0,
                    article.UseVat ? prices.WrapPriceDBEur : prices.WrapPriceNoVATEur,
                    article.UseVat ? prices.WrapPriceUserEur : prices.WrapPriceNoVATEur,
                    0, // WrapPriceToPay
                    article.VATRate.Value,
                    affiliate,
                    null, // OrderDetailStatusFk
                    0, // OrderDetailPaymentStatusFk
                    0, // OrderDetailDeliveryStatusFk
                    splitLevel,
                    article.AvailabilityId.Value,
                    article.IsBundle.GetValueOrDefault() ? 1 : 0,
                    refGu == 0 ? null : (int?)refGu,
                    1, // EpsilonFlag
                    article.InsuranceStart,
                    prices.EcoTaxEur,
                    prices.PriceRealEur,
                    article.ID.Value,
                    null,
                    article.VATCodeGu ?? "",
                    null,
                    article.TypeId.Value,
                    master,
                    null,
                    article.ExternalOrderDetailReference,
                    dtTheoExpDate,
                    article.TheoreticalDeliveryDate,
                    article.TheoreticalDeliveryDateMax,
                    article.UniverseInfo?.UniverseId,
                    article.VatCountryId,
                    article.ReferenceGU,
                    null,
                    isOptinWarranty
                );
        }

        public void AddOrderDetailLink(int orderDetailFkMaster, int orderDetailFkChild)
        {
            _orderInsertionDal.AddOrderDetailLink(orderDetailFkMaster, orderDetailFkChild);
        }

        public void AddOrderDetailAltSellerInfo(int orderDetailFk, decimal? bestOfferPrice, int? totalOfferCount)
        {
            _orderInsertionDal.AddOrderDetailAltSellerInfo(orderDetailFk, null, null, null, bestOfferPrice, totalOfferCount);
        }

        public void AddOrderDetailInfo(int orderDetailFk, string xmldata)
        {
            _orderInsertionDal.AddOrderDetailInfo(orderDetailFk, OrderDetailInfoType.SubscriptionCard, "", xmldata);
        }

        public void AddOrderDetailInfoForRecruitingCardNumber(int orderDetailFk, string cardNumber)
        {
            _orderInsertionDal.AddOrderDetailInfo(orderDetailFk, OrderDetailInfoType.RecruitingCardNumber, "", cardNumber);
        }

        public void AddOrderDetailInfoForPosa(int orderDetailFk, string posaCode)
        {
            _orderInsertionDal.AddOrderDetailInfo(orderDetailFk, OrderDetailInfoType.POSACode128, posaCode, posaCode);
        }

        public void AddOrderDetailInfoForFreeBasket(int orderDetailFk, int type, string contractNumber)
        {
            _orderInsertionDal.AddOrderDetailInfo(orderDetailFk, type, contractNumber, contractNumber);
        }

        public void AddOrderDetailInfoForLongTermRental(int orderDetailFk, string contractNumber)
        {
            _orderInsertionDal.AddOrderDetailInfo(orderDetailFk, OrderDetailInfoType.LongTermRentalContractNumber, contractNumber, contractNumber);
        }

        public void AddOrderDetailInfoForGPSubscription(int orderDetailFk, string contractNumbers)
        {
            _orderInsertionDal.AddOrderDetailInfo(orderDetailFk, OrderDetailInfoType.SouscriptionGP, contractNumbers, contractNumbers);
        }

        public void AddOrderDetailInfoAdh(int orderDetailFk, string xmldata)
        {
            _orderInsertionDal.AddOrderDetailInfo(orderDetailFk, OrderDetailInfoType.ServiceDomicile, "AdherentCard", xmldata);
        }

        public void AddOrderDetailAddOn(int orderDetailFk, int typeArticle, int itemGroup, bool flagVenteService, string partnerName, NumericalAddOn numAddOn, string koboUserId)
        {
            _commerceDal.AddOrderDetailAddOn(orderDetailFk, typeArticle, itemGroup, flagVenteService, partnerName, numAddOn, koboUserId);
        }

        public void AddOrderDetailInfoForOpenCellSubscription(int orderDetailFk, string subscriptionNumber)
        {
            _orderInsertionDal.AddOrderDetailInfo(orderDetailFk, OrderDetailInfoType.OpenCellSubscriptionNumber, subscriptionNumber, subscriptionNumber);
        }

        public void AddOrderDetailInfo(int orderDetailFk, OrderDetailInfoType type, string info)
        {
            _orderInsertionDal.AddOrderDetailInfo(orderDetailFk, type, info, info);
        }
    }
}
