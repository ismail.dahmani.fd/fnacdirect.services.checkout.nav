namespace FnacDirect.OrderPipe.Base.Business.Tickets.ServiceClient
{
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name = "GeneratedTicket", Namespace = "http://schemas.datacontract.org/2004/07/FnacDirect.Terminal.TerminalsManagement.Service")]
    [System.SerializableAttribute()]
    public partial class GeneratedTicket : object, System.Runtime.Serialization.IExtensibleDataObject, System.ComponentModel.INotifyPropertyChanged
    {

        [System.NonSerializedAttribute()]
        private System.Runtime.Serialization.ExtensionDataObject extensionDataField;

        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private System.DateTime DateLotField;

        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string TicketIDField;

        [global::System.ComponentModel.BrowsableAttribute(false)]
        public System.Runtime.Serialization.ExtensionDataObject ExtensionData
        {
            get
            {
                return this.extensionDataField;
            }
            set
            {
                this.extensionDataField = value;
            }
        }

        [System.Runtime.Serialization.DataMemberAttribute()]
        public System.DateTime DateLot
        {
            get
            {
                return this.DateLotField;
            }
            set
            {
                if ((this.DateLotField.Equals(value) != true))
                {
                    this.DateLotField = value;
                    this.RaisePropertyChanged("DateLot");
                }
            }
        }

        [System.Runtime.Serialization.DataMemberAttribute()]
        public string TicketID
        {
            get
            {
                return this.TicketIDField;
            }
            set
            {
                if ((object.ReferenceEquals(this.TicketIDField, value) != true))
                {
                    this.TicketIDField = value;
                    this.RaisePropertyChanged("TicketID");
                }
            }
        }

        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;

        protected void RaisePropertyChanged(string propertyName)
        {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null))
            {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }
}
