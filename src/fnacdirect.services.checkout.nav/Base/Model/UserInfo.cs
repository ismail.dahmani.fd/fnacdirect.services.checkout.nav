using System.Xml.Serialization;
using FnacDirect.OrderPipe.Base.Model.UserInformations;
using DTO = FnacDirect.Contracts.Online.Model;
using FnacDirect.Customer.Model;
using FnacDirect.Technical.Framework.Web;
using FnacDirect.IdentityImpersonation;

namespace FnacDirect.OrderPipe.Base.Model
{
    public enum AccountStatusEnum
    {
        OK = 1,
        Blacklisted = 2
    }

    public class UserInfo : IUserContextInformations
    {
        [XmlIgnore]
        public W3Customer W3Customer { get; set; }
        [XmlIgnore]
        public DTO.Customer Customer { get; set; }

        public string UID { get; set; }

        [XmlIgnore]
        public int AccountId { get; set; }
        public string Identity { get; set; }
        public int AccountStatus { get; set; }

        public bool IsCustomer { get; set; }

        public bool IsAdherent { get; set; }

        public bool IsRenewable { get; set; }

        public bool IsGamer { get; set; }

        public string ClientCard { get; set; }

        public string ContactEmail { get; set; }

        [XmlIgnore]
        public int CountryConfidence { get; set; }

        [XmlIgnore]
        public int UserConfidence { get; set; }

        [XmlIgnore]
        public bool Scored { get; set; }

        [XmlIgnore]
        public int MailMessageFlag { get; set; }

        [XmlIgnore]
        public int ThankYouMessageFlag { get; set; }

        public string Gender { get; set; }        

        public string LastName { get; set; }

        public string FirstName { get; set; }

        public int BlackListCounter { get; set; }

        [XmlIgnore]
        public CustomerEntity CustomerEntity { get; set; }

        public string CellPhone { get; set; }

        /// <summary>
        /// Email de contact destiné à recevoir les alertes
        /// de prevenance de la commande
        /// </summary>
        public string ContactEmailAlert { get; set; }

        /// <summary>
        /// Numéro de téléphone portable destiné
        /// à recevoir les infos de prevenance
        /// de la commande
        /// </summary>
        public string CellphoneAlert { get; set; }

        public bool IsNewCustomer { get; set; }

        /// <summary>
        /// Numéro de carte si le client est adhérent
        /// </summary>
        public string PlasticCardNumber { get; set; }
        
        [XmlIgnore]
        public IdentityImpersonator IdentityImpersonator { get; set; }

        public OneClickBuyInfo OneClickBuyInfo { get; set; }

        public string KoboUserId { get; set; }

        public string ZipCode { get; set; }

        /// <summary>
		/// Id technique du magasin calculé à partir du refGU dans l'IdentityImpersonator
		/// </summary>
		[XmlIgnore]
        public int StoreCodeId { get; set; }

        /// <summary>
        /// Booleen qui indique que le client a choisi le parcours Guest.
        /// </summary>
        [XmlIgnore]
        public bool IsGuest
        {
            get
            {
                return false;
            }
            set
            {

            }
        }
    }
}
