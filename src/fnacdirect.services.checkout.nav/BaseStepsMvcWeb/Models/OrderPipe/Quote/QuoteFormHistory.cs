using System;
using System.Collections.Generic;
using System.Text;
using FnacDirect.Technical.Framework.CoreServices;
using FnacDirect.Technical.Framework.CoreServices.Localization;

namespace FnacDirect.Quote.Model
{
    [Serializable]
    public class QuoteFormEventHistory : QuoteFormHistory
    {
        
    }

    [Serializable]
    public class QuoteFormHistory : List<QuoteFormHistoryLine>
    {
        IUIResourceService loc = Service<IUIResourceService>.Instance;
        
        private object _eventId;
        private int _historyId;
        private int _quoteId;
        private int _statusId;
        private DateTime _historyDate;
        private string _filledBy;
        private string _historyComment;
        private string _historyCommentResult;
        private QuoteForm m_quoteForm;


        #region Constructors

        public QuoteFormHistory() { }

        public QuoteFormHistory(QuoteForm quoteForm)
        {
            QuoteId = quoteForm.QuoteId;
            QuoteStatus = quoteForm.Status;
            HistoryDate = quoteForm.LastModificationDate;
            m_quoteForm = quoteForm;
        }
             
        #endregion Constructors
       
        public int HistoryId
        {
            get {
                return _historyId;
            }
            set {
                _historyId = value;
            }
        }

        public int QuoteId
        {
            get {
                return _quoteId;
            }
            set {
                _quoteId = value;
            }
        }

        public int QuoteStatus
        {
            get {
                return _statusId;
            }
            set {
                _statusId = value;
            }
        }

        public DateTime HistoryDate
        {
            get {
                return _historyDate;
            }
            set {
                _historyDate = value;
            }
        }

        public string FilledBy
        {
            get {
                return _filledBy;
            }
            set {
                _filledBy = value;
            }
        }

        public string HistoryComment
        {
            get {
                return _historyComment;
            }
            set {
                _historyComment = value;
            }
        }


        public string HistoryCommentResult
        {
            get
            {
                _historyCommentResult = BuildCommentResult();
                return _historyCommentResult;
            }
            set
            {
                _historyCommentResult = value;
            }
        }


       
       
        private string _actualOwner;

       

        public object EventId
        {
            get {
                return _eventId;
            }
            set {
                _eventId = value;
            }
        }

        public string ActualOwner
        {
            get {
                return _actualOwner;
            }
            set {
                _actualOwner = value;
            }
        }

        /// <summary>
        /// Retrieves current Quote Form
        /// </summary>
        public QuoteForm QuoteForm { get { return m_quoteForm; } }

        public string BuildCommentResult()
        {
            var sb = new StringBuilder();

            foreach (var quoteFormHistoryLine in this)
            {
                sb.Append(quoteFormHistoryLine);
            }

            return sb.ToString();
        }
    }

    [Serializable]
    public class QuoteFormHistoryLine
    {
        private string template;
        private int eventType;
        private List<string> commentParams;
        private string commentLineResult;


        public QuoteFormHistoryLine()
        {
            CommentParams = new List<string>();
        }
        public string Template
        {
            get { return template; }
            set { template = value; }
        }

        public int EventType
        {
            get { return eventType; }
            set { eventType = value; }
        }

        public List<string> CommentParams
        {
            get { return commentParams; }
            set { commentParams = value; }
        }

        public string CommentLineResult
        {
            get
            {
                commentLineResult = ToString();
                return commentLineResult;
            }
            set { commentLineResult = value; }
        }

        public override string ToString()
        {
            return string.Format(Template,EventType, commentParams.ToArray());
        }
    }
}
