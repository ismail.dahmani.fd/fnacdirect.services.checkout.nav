using FnacDirect.OrderPipe.Base.Model.Facets;

namespace FnacDirect.OrderPipe.Base.Business.Payment
{
    public interface IOrangePaymentService
    {
        bool IsAllowed(IGotBillingInformations iGotBillingInformations, IGotLineGroups iGotLineGroups, IGotShippingMethodEvaluation shippingMethodEvaluation);
        bool IsAllowedByEmail();
    }
}
