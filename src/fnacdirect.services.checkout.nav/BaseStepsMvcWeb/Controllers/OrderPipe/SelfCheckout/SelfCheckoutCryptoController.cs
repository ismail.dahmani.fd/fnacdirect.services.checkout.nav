using FnacDirect.OrderPipe.BaseMvc.Web.Controllers.Pop;
using FnacDirect.OrderPipe.BaseMvc.Web.Pop.Models.SelfCheckout;
using System.Web.Mvc;
using FnacDirect.OrderPipe.Base.Model;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Pop.Controllers
{
    public class SelfCheckoutCryptoController : PopBaseController
    {
        private readonly OPContext _opContext;
        private readonly ISelfCheckoutCryptoViewModelBuilder _checkCardValidationViewModelBuilder;

        public SelfCheckoutCryptoController(OPContext opContext, ISelfCheckoutCryptoViewModelBuilder checkCardValidationViewModelBuilder)
        {
            _opContext = opContext;
            _checkCardValidationViewModelBuilder = checkCardValidationViewModelBuilder;
        }

        public ActionResult Index()
        {
            var checkCardValidationViewModel = _checkCardValidationViewModelBuilder.Build(_opContext);

            var popOrderForm = _opContext.OF as PopOrderForm;
            
            return View(checkCardValidationViewModel);
        }
    }
}
