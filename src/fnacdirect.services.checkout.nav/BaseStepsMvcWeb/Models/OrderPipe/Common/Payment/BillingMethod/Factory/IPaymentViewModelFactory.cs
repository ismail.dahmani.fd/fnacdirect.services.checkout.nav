﻿using FnacDirect.OrderPipe.Base.Model;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Common.Payment.BillingMethodFactories
{
    public interface IPaymentViewModelFactory
    {
        BillingMethodViewModel Get(string tabName, PaymentMethod paymentMethod, OPContext opContext, PopOrderForm popOrderForm);
    }

    public interface IPaymentViewModelFactory<TBillingMethod> : IPaymentViewModelFactory
        where TBillingMethod : BillingMethod
    {
    }
}
