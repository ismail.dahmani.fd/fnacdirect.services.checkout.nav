using FnacDirect.ShoppingCartEntities;
using System;

namespace FnacDirect.Basket.Business
{
    public interface IBasketBusiness
    {
        void SetShopId(string sid, int shopId);
        bool AddAccessoryItemTo(string prmszSID, int prmiFatherPRID, int prmiPRID, short prmiQuantity);
        bool AddItemTo(string prmszSID, int prmiPRID, short prmiQuantity, int? shopId = null);
        bool AddMPItemTo(string sid, int prid, Guid offer, short quantity);
        bool AddMPItemToBasket(ShoppingCartType prmiType, string prmszSID, Guid prmiOfferReference, short prmiQuantity);
        bool AddServiceItemTo(string prmszSID, int prmiFatherPRID, int prmiPRID, short prmiQuantity);
        bool DeleteMPItemFromBasket(ShoppingCartType prmiType, string prmszSID, string prmiOffer);
        StandardArticle GetArticle(string prmszSID, int prmiPRID);
        MarketPlaceArticle GetMarketPlaceArticle(string prmszSID, int prmiPRID, string offerReference);
        Service GetService(string prmszSID, int masterProductId, int serviceProductId);
        MarketPlaceArticle GetMPArticle(string sid, int prid, Guid offer);
        bool Remove(string prmszSID, int prmiPRID, short quantity);
        bool RemoveMP(string sid, int prid, Guid offer, short quantity);
        bool TransferItemMPShoppingCart(ShoppingCartType prmiTypeInit, string prmszSIDInit, ShoppingCartType prmiTypeTarget, string prmszSIDTarget, string prmsOfferRef);
        Orderform GetShoppingCart(ShoppingCartType shoppingCartType, string sid);
    }
}
