using System;
using System.Collections.Generic;
using System.Linq;
using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.OrderPipe.Base.Model.LineGroups;
using FnacDirect.Relay.Model;
using FnacDirect.Technical.Framework.Web.Mvc;

namespace FnacDirect.OrderPipe.Base.Business.Shipping.Relay
{
    public class RelayValidityService : IRelayValidityService
    {
        private readonly IApplicationContext _applicationContext;
        private const int DefaultDeliveryMargin = 10;

        public RelayValidityService(IApplicationContext applicationContext)
        {
            _applicationContext = applicationContext;
        }
        
        public bool CheckRelayValidity(RelayAddressEntity relayAddressEntity, IEnumerable<ILineGroup> lineGroups)
        {
            return CheckRelayValidity(new RelayAddress(relayAddressEntity, _applicationContext.GetSiteContext().CountryId), lineGroups, out var errorMessage);
        }      

        public bool CheckRelayValidity(RelayAddress relayAddress, IEnumerable<ILineGroup> lineGroups)
        {
            return CheckRelayValidity(relayAddress, lineGroups, out var errorMessage);
        }

        public bool CheckRelayValidity(RelayAddress relayAddress, IEnumerable<ILineGroup> lineGroups, out string errorMessage)
        {
            return CheckRelayValidity(relayAddress, lineGroups, DefaultDeliveryMargin, out errorMessage);
        }

        public bool CheckRelayValidity(RelayAddress relayAddress, IEnumerable<ILineGroup> lineGroups, int deliveryMargin, out string errorMessage)
        {
            errorMessage = string.Empty;

            switch (relayAddress.Status)
            {
                case "A":
                    if (lineGroups.All(lg=>lg.Seller.Type == LineGroups.SellerType.ClickAndCollectOnly))
                    {
                        return false;
                    }

                    if (relayAddress.LastName.ToUpper().Contains("FNAC") || relayAddress.Alias.ToUpper().Contains("FNAC"))
                    {
                        return false;
                    }

                    var standardArticles = lineGroups.Where(l => !l.HasMarketPlaceArticle).SelectMany(l => l.Articles).OfType<StandardArticle>()
                          .Where(standardArticle => standardArticle.Type == ArticleType.Saleable && standardArticle.ArticleDelay.HasValue).ToList();

                    var preOrderdArticles = lineGroups.Where(l => !l.HasMarketPlaceArticle).SelectMany(l => l.Articles).OfType<StandardArticle>()
                                   .Where(standardArticle => standardArticle.Type == ArticleType.Saleable && standardArticle.AvailabilityId.GetValueOrDefault() == 10).ToList();


                    var delayMaxByOrder = standardArticles.Select(a => a.ArticleDelay.GetValueOrDefault(0)).DefaultIfEmpty().Max();

                    var announcementDate = preOrderdArticles.Select(a => a.AnnouncementDate as DateTime?).DefaultIfEmpty().Max();

                    var delayMaxShipping = delayMaxByOrder == 24 ? 17 : 25;

                    var hasLastParcelDate = relayAddress.LastDateParcel.Year > 1900;

                    if (preOrderdArticles.Any() && announcementDate.HasValue && delayMaxByOrder == 0)
                    {
                        if (announcementDate.Value.AddDays(deliveryMargin) <= relayAddress.FirstDateParcel && (hasLastParcelDate && announcementDate.Value.AddDays(deliveryMargin) >= relayAddress.LastDateParcel))
                        {
                            errorMessage = _applicationContext.GetMessage(relayAddress.IsDefault ? "orderpipe.pop.relay.futureinvaliddefault" : "orderpipe.pop.relay.futureinvalid");
                            return false;
                        }
                    }
                    else if ((DateTime.Now.AddDays(delayMaxShipping) < relayAddress.FirstDateParcel && (hasLastParcelDate && DateTime.Now.AddDays(delayMaxShipping) > relayAddress.LastDateParcel))
                            || (preOrderdArticles.Any() && announcementDate.HasValue && (announcementDate.Value.AddDays(deliveryMargin) < relayAddress.FirstDateParcel || (hasLastParcelDate && announcementDate.Value.AddDays(deliveryMargin) > relayAddress.LastDateParcel))))
                    {
                        errorMessage = _applicationContext.GetMessage(relayAddress.IsDefault ? "orderpipe.pop.relay.futureinvaliddefault" : "orderpipe.pop.relay.futureinvalid");
                        return false;
                    }
                    break;
                case "T":
                    errorMessage = _applicationContext.GetMessage("orderpipe.pop.relay.temporaryclosed", string.Format("{0}", relayAddress.FirstDateParcel.ToShortDateString()));
                    return false;
                case "S":
                    errorMessage = _applicationContext.GetMessage("orderpipe.pop.relay.closed");
                    return false;
                case "C":
                case "R":
                    errorMessage = _applicationContext.GetMessage(relayAddress.IsDefault ? "orderpipe.pop.relay.invaliddefault" : "orderpipe.pop.relay.invalid");
                    return false;
            }
            return true;
        }
    }
}

