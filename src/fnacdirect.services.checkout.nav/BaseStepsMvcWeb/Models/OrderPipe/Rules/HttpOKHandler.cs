﻿using System;
using System.Web;

namespace FnacDirect.OrderPipe.CoreRouting.Rules
{
    public class HttpOKHandler : IHttpHandler
    {
        public bool IsReusable
        {
            get
            {
                return true;
            }
        }

        public void ProcessRequest(HttpContext context)
        {
            context.Response.StatusCode = 200;
            context.Response.StatusDescription = "OK";
        }
    }
}
