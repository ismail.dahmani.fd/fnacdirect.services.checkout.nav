namespace FnacDirect.OrderPipe.Base.Model
{
    public class BankTransferBillingMethod : BillingMethod
    {
        public const int IdBillingMethod = 256;

        public override bool IsPrimary => true;

        public BankTransferBillingMethod()
        {
            BillingMethodId = IdBillingMethod;
            CanModifyAmountUp = true;
            CanModifyAmountDown = true;
            ImplyValidateOrder = true;
        }
    }
}
