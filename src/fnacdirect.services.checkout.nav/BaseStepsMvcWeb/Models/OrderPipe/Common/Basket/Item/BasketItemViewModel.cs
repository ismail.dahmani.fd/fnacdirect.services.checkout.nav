using System.Collections.Generic;
using System.Linq;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Common.Basket
{
    public class BasketItemViewModel
    {
        private readonly List<BasketItemViewModel> _services;

        public BasketItemViewModel()
        {
            _services = new List<BasketItemViewModel>();
        }

        public BasketItemViewModel(BasketItemViewModel basketItemViewModel)
            : this()
        {
            Title = basketItemViewModel.Title;
            SubTitle = basketItemViewModel.SubTitle;
            PriceOld = basketItemViewModel.PriceOld;
            Price = basketItemViewModel.Price;
            PriceOldNoVat = basketItemViewModel.PriceOldNoVat;
            PriceNoVat = basketItemViewModel.PriceNoVat;
            PriceToDisplay = basketItemViewModel.PriceToDisplay;
            IsNumerical = basketItemViewModel.IsNumerical;
            IsFree = basketItemViewModel.IsFree;
            AvailabilityId = basketItemViewModel.AvailabilityId;
            TotalOfferCount = basketItemViewModel.TotalOfferCount;
            AddServices(basketItemViewModel.Services.ToArray());
            IsEbook = basketItemViewModel.IsEbook;
            IsBook = basketItemViewModel.IsBook;
            HasD3E = basketItemViewModel.HasD3E;
            HasRCP = basketItemViewModel.HasRCP;
        }

        public string Title { get; set; }

        public string Availability { get; set; }

        /// <summary>
        /// Numero de Tome, "Format Epub" et "En Téléchargement"
        /// </summary>
        public string SubTitle { get; set; }

        public string PriceOld { get; set; }

        public string Price { get; set; }

        public string PriceOldNoVat { get; set; }

        public string PriceNoVat { get; set; }

        public string PriceToDisplay { get; set; }

        public string PriceOldToDisplay { get; set; }

        public bool ShouldDisplayOldPrice
        {
            get { return (Price != PriceOld); }
        }

        public bool IsNumerical { get; set; }

        public bool IsEbook { get; set; }

        public bool IsBook { get; set; }
        public bool HasD3E { get; set; }
        public bool HasRCP { get; set; }
        public bool IsFree { get; set; }

        public IReadOnlyCollection<BasketItemViewModel> Services
        {
            get
            {
                return _services.AsReadOnly();
            }
        }

        public BasketItemViewModel AddServices(params BasketItemViewModel[] services)
        {
            _services.AddRange(services);

            return this;
        }

        public int AvailabilityId { get; set; }

        public int TotalOfferCount { get; set; }

        public bool IsAppointment { get; set; }
    }
}
