﻿namespace FnacDirect.OrderPipe.Base.Business.ShoppingCart
{
    public interface IShoppingCartService
    {
        /// <summary>
        /// Retourne tous les orderforms (SID et UID et fusionné) du ShoppingCart
        /// (utilisé par le panier persistant pour les transferts entre Pipe et ShoppingCart)
        /// </summary>
        void RetrieveShoppingCartOrderforms(string uid, string sid, ShoppingCartEntities.ShoppingCartType typeOfCart, out ShoppingCartEntities.Orderform ofSID, out ShoppingCartEntities.Orderform ofUID, out ShoppingCartEntities.Orderform ofFusionned, out bool ofUIDExists, ShoppingCartEntities.FusionType fusionType = ShoppingCartEntities.FusionType.Default);
    }
}
