﻿using FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Common.Shipping;
using FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Common.Shipping.Shop;
using System.Collections.Generic;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.OrderThankYou
{
    public class ShipToStoreInformation
    {
        public string Name { get; set; }

        public IEnumerable<TimetableViewModel> Timetables { get; set; }

        public IEnumerable<ExceptionnalClosingViewModel> ExceptionnalTimetables { get; set; }

        public string UrlStorePage { get; set; }

        public bool IsShipToStore { get; set; }

        public bool IsClickAndCollect { get; set; }
    }
}
