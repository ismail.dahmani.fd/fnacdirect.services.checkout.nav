﻿using System.Collections.Generic;
using FnacDirect.Customer.BLL;
using FnacDirect.Customer.Model;
using FnacDirect.Technical.Framework.Web.Mvc;

namespace FnacDirect.OrderPipe.Base.Proxy.Application
{
    public class CountryWithVatService : ICountryWithVatService
    {
        private readonly IApplicationContext _applicationContext;

        public CountryWithVatService(IApplicationContext applicationContext)
        {
            _applicationContext = applicationContext;
        }

        public Dictionary<int, CountryVatIntraEntity> Get()
        {
            var culture = _applicationContext.GetLocale();

            return AddressBusiness.GetCountriesWithVatIntra(culture);
        }
    }
}
