using Models = FnacDirect.OrderPipe.TaxCalculation.Front.Models;

namespace FnacDirect.OrderPipe.Base.Model.TaxCalculation
{
    public class OrderPipeSalesInfo
    {
        public OrderPipeSalesInfoPrice ReducedPrice { get; set; }
        public OrderPipeSalesInfoPrice StandardPrice { get; set; }
        public OrderPipeSalesInfoPrice PublicPrice { get; set; }
        public OrderPipeSalesInfoPrice EcoTaxAmount { get; set; }
        public int TaxId { get; set; }
        public decimal TaxRate { get; set; }

        public Models.SalesInfo ToTaxCalculationSalesInfo()
        {
            var saleInfos = new Models.SalesInfo
            {
                TaxId = TaxId,
                TaxRate = TaxRate,
                ReducedPrice = new Models.SalesInfoPrice { HT = ReducedPrice.HT, TTC = ReducedPrice.TTC },
                StandardPrice = new Models.SalesInfoPrice { HT = StandardPrice.HT, TTC = StandardPrice.TTC},
                PublicPrice = new Models.SalesInfoPrice { HT = PublicPrice.HT, TTC = PublicPrice.TTC},
                EcoTaxAmount = new Models.SalesInfoPrice ()
            };

            if (EcoTaxAmount != null)
            {
                saleInfos.EcoTaxAmount.HT = EcoTaxAmount.HT;
                saleInfos.EcoTaxAmount.TTC = EcoTaxAmount.TTC;
            }

            return saleInfos;
        }
    }

    public class OrderPipeSalesInfoPrice
    {
        public decimal HT { get; set; }
        public decimal TTC { get; set; }
    }
}
