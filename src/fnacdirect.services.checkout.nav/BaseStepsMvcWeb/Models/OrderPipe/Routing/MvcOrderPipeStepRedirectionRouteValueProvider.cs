﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Routing;

namespace FnacDirect.OrderPipe.CoreMvcWeb.Routing
{
    public interface IMvcOrderPipeStepRedirectionRouteValueProvider
    {
        RouteValueDictionary GetRouteValueFor(IOF orderForm);
    }

    public abstract class MvcOrderPipeStepRedirectionRouteValueProvider<TOrderForm> : IMvcOrderPipeStepRedirectionRouteValueProvider
        where TOrderForm : class, IOF
    {
        protected abstract RouteValueDictionary GetRouteValueFor(TOrderForm orderForm);

        RouteValueDictionary IMvcOrderPipeStepRedirectionRouteValueProvider.GetRouteValueFor(IOF orderForm)
        {
            return this.GetRouteValueFor(orderForm as TOrderForm);
        }
    }
}