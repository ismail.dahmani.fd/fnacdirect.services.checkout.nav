using System;
using System.ComponentModel;
using System.Xml.Serialization;
using FnacDirect.OrderPipe.Base.LineGroups;

namespace FnacDirect.OrderPipe.Base.Model
{
    public class LineGroupInfo : ILineGroupInformations
    {
        public int? OrderPk { get; set; }

        public OrderTransactionInfoList OrderTransactionInfos { get; set; }

        public string OrderUserReference { get; set; }

        public Guid OrderReference { get; set; }

        public DateTime OrderDate { get; set; }

        [XmlIgnore]
        public int ShippingAddressId { get; set; }

        public int? OrderType { get; set; }

        public int? OrderStatusId { get; set; }

        public bool GiftOption { get; set; }

        [DefaultValue(0)]
        public int ChosenWrapMethod { get; set; }

        public string OrderMessage { get; set; }

        public string GUPTReference { get; set; }

        public LineGroupInfo()
        {
            OrderTransactionInfos = new OrderTransactionInfoList();
            GUPTTransactionId = 0;
            OrderStatusId = null;
        }

        public int GUPTTransactionId { get; set; }
    }
}
