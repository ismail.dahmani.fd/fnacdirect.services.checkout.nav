﻿namespace FnacDirect.OrderPipe.Base.BaseModel.Model.OrderDetails
{
    public class OrderDetailPostalAddress : OrderDetailBaseAddress
    {
        public OrderDetailPostalAddress()
        {
            Type = "PostalAddress";
        }

        public string Alias { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Company { get; set; }

        public string State { get; set; }

        public string Phone { get; set; }
    }
}
