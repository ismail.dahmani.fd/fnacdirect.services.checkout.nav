using FnacDirect.OrderPipe.Base.Model;
using System.Collections.Generic;
using DTO = FnacDirect.Contracts.Online.Model;

namespace FnacDirect.OrderPipe.Base.Business
{
    public interface IArticleService
    {
        IEnumerable<DTO.Article> FilterArticles(IEnumerable<DTO.Article> articles, ArticleFilter filter);
        IEnumerable<DTO.Article> GetFilteredArticles(IEnumerable<DTO.Article> articles, ArticleFilter filter);

        bool IsFlyService(DTO.Article service);
        bool IsFlyService(StandardArticle service);
    }
}
