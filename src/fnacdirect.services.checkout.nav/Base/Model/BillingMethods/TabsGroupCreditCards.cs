using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace FnacDirect.OrderPipe.Base.Model
{
    public class TabsGroupPaymentMethods
    {
        [XmlElement]
        public List<TabGroup> TabGroup { get; set; }

        public Dictionary<string, List<int>> ListGroupTypePerTabs(string billingMethodName)
        {
            var dico = new Dictionary<string, List<int>>();

            if(TabGroup == null)
            {
                return dico;
            }

            foreach (var tabGroup in TabGroup)
            {
                foreach (var paymentMethod in tabGroup.PaymentMethod.Where(pm => pm.Name == billingMethodName))
                {
                    if(dico.ContainsKey(tabGroup.Name))
                    {
                        dico[tabGroup.Name].AddRange(paymentMethod.GroupType);
                    }
                    else
                    {
                        dico.Add(tabGroup.Name, paymentMethod.GroupType);
                    }
                }   
            }

            return dico;
        }
    }

    public class TabGroup
    {
        [XmlAttribute]
        public string Name { get; set; }

        [XmlAttribute]
        public int Position { get; set; }

        [XmlAttribute]
        public string Type { get; set; }

        [XmlElement]
        public List<PaymentMethod> PaymentMethod { get; set; }
    }

    public class PaymentMethod
    {
        [XmlAttribute]
        public string Name { get; set; }

        [XmlElement]
        public List<int> GroupType { get; set; }

        /// <summary>
        /// Condition pour afficher ou non les méthodes de paiement enregistrés par l'utilisateur.
        /// Exemple: Afficher les cartes de crédits du client.
        /// </summary>
        [XmlElement]
        public bool DisplayRecordedPayments { get; set; } = true;

        /// <summary>
        /// Liste des cartes de crédits (données brutes).
        /// Merci d'utiliser GetCreditCardTypes() pour récupérer la liste des ID des cartes de crédits, plutôt que cette propriété.
        /// </summary>
        [XmlElement(ElementName = "CreditCardType")]
        public List<string> CreditCardTypeRawData { get; set; }

        [XmlElement]
        public List<SpecificRules> SpecificRules { get; set; }

        /// <summary>
        /// ID de la billing methode à utiliser.
        /// Elle surchage celle de la BillingMethod.
        /// </summary>
        [XmlAttribute]
        public int VirtualBillingMethodId { get; set; } = 0;


        /// <summary>
        /// Liste de carte de crédits.
        /// Important: Les cartes de crédits sont filtrés par cette liste, si et seulement si la règle SpecificRules.ExcludeCreditCardType ou SpecificRules.OnlyCreditCardType sont invoquées et si la feature flag "op.ogone.enablealiasgateway" est active.
        /// </summary>
        public List<int> GetCreditCardTypes(OPContext opContext) {
            var result = new List<int>();
            var globalParameters = opContext.Pipe.GlobalParameters;

            foreach (var rawData in CreditCardTypeRawData)
            {
                // Si de type entier, on considere que c'est un identifiant de carte
                if (int.TryParse(rawData, out var rawInt))
                {
                    result.Add(rawInt);
                    continue;
                }

                // Sinon, on considere que c'est une propriete
                if (globalParameters.Contains(rawData))
                {
                    result.Add(globalParameters.GetAsInt(rawData));
                    continue;
                }
            }

            return result;
        }

    }

    public enum SpecificRules
    {
        Phone,
        Credit,
        Private,
        InstallmentPayment,
        Light,
        Paypal,

        // Filtre les cartes de credits (selon la propriété XML CreditCardType)
        ExcludeCreditCardType,  // Exclusion
        OnlyCreditCardType,     // Seulement
    }
}
