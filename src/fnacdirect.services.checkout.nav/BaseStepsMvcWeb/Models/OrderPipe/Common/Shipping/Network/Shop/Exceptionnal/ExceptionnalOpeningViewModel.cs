using System.Collections.Generic;
using System.Linq;
using FnacDirect.FnacStore.Model;
using FnacDirect.Technical.Framework.CoreServices.Localization;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Common.Shipping.Shop
{
    public class ExceptionnalOpeningViewModel : ExceptionnalClosingViewModel
    {
        public override bool IsOpeningDay
        {
            get
            {
                return true;
            }
        }

        public string Openings { get; set; }

        public string Comment { get; set; }

        public static string GetOpenings(IEnumerable<FnacOpeningPeriod> openingPeriods, IUIResourceService uiResourceService, string culture, bool isDrive = false)
        {
            var fnacOpeningPeriods = openingPeriods as IList<FnacOpeningPeriod> ?? openingPeriods.ToList();

            var format = "%h\\hmm";

            if (fnacOpeningPeriods.Count() == 1)
            {
                var uiRessourceKey = "orderpipe.pop.shipping.store.timetable.fromto";
                if (isDrive)
                {
                    uiRessourceKey = "orderpipe.pop.shipping.store.timetable.fromto.drive";
                }
                var uiRessourceValue = uiResourceService.GetUIResourceValue(uiRessourceKey, culture);

                var openingPeriod = fnacOpeningPeriods.ElementAt(0);

                return string.Format(uiRessourceValue, openingPeriod.Opening.ToString(format), openingPeriod.Closing.ToString(format));
            }

            if(fnacOpeningPeriods.Count() == 2)
            {
                var uiRessourceKey = "orderpipe.pop.shipping.store.timetable.fromtoandfromto";
                if (isDrive)
                {
                    uiRessourceKey = "orderpipe.pop.shipping.store.timetable.fromtoandfromto.drive";
                }
                var uiRessourceValue = uiResourceService.GetUIResourceValue(uiRessourceKey, culture);

                var openingPeriod1 = fnacOpeningPeriods.ElementAt(0);
                var openingPeriod2 = fnacOpeningPeriods.ElementAt(1);

                return string.Format(uiRessourceValue, openingPeriod1.Opening.ToString(format), openingPeriod1.Closing.ToString(format), openingPeriod2.Opening.ToString(format), openingPeriod2.Closing.ToString(format));
            }

            return string.Empty;
        }
    }
}
