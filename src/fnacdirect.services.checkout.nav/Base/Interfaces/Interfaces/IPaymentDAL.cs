using FnacDirect.OrderPipe.Base.Model;

namespace FnacDirect.OrderPipe.Base.DAL
{
    public interface IPaymentDAL
    {
        PopOrderForm GetOrderByOrderReference(string orderReference, PopOrderForm popOrderForm);
        int? GetLatestShippedOrder(int accountId);
    }
}
