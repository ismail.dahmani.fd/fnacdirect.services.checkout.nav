﻿using FnacDirect.OrderPipe.Base.Model;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.OnePage.Basket
{
    public interface IBasketViewModelBuilder
    {
        BasketViewModel Build(PopOrderForm popOrderForm);
    }
}
