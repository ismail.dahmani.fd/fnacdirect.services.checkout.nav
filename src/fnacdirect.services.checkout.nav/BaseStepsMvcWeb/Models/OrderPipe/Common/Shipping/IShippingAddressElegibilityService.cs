﻿using System.Collections.Generic;
using FnacDirect.Customer.Model;
using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.OrderPipe.Base.Model.Facets;
using FnacDirect.OrderPipe.Base.Model.LineGroups;

namespace FnacDirect.OrderPipe.Base.Business.Shipping
{
    public interface IShippingAddressElegibilityService
    {
        bool IsElegibleForRelay(IGotShippingMethodEvaluation iContainsShippingMethodEvaluation, IEnumerable<ILineGroup> lineGroups);
        bool IsElegibleForShop(IGotShippingMethodEvaluation iContainsShippingMethodEvaluation, IEnumerable<ILineGroup> lineGroups);

        ShippingAddress DummyRelayAddress { get; }

        ShippingAddress GetDummyRelayAddress(IEnumerable<ILineGroup> lineGroups, OPContext opContext);


        ShopAddress DummyShopAddress { get; }
        
        IEnumerable<AddressEntity> GetEligibleAddresses(IGotShippingMethodEvaluation iContainsShippingMethodEvaluation, IEnumerable<ILineGroup> lineGroups, IEnumerable<AddressEntity> addresses);
        IEnumerable<ShippingAddress> GetEligibleAddresses(IGotShippingMethodEvaluation iContainsShippingMethodEvaluation, IEnumerable<ILineGroup> lineGroups, IEnumerable<ShippingAddress> addresses);
    }
}