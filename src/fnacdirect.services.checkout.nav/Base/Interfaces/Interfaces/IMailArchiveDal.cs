﻿using System;

namespace FnacDirect.OrderPipe.Base.DAL
{
    public interface IMailArchiveDal
    {
        void Archive(Guid mainOrderReference, int mailStatus, string mailContent, string neolaneStream);
    }
}
