using System.Collections.Generic;
using System.Linq;

namespace FnacDirect.OrderPipe.Base.Business.Availability
{
    public static class PopAvailabilityExtensions
    {
        public static bool ComputeBeforeXmas(this IEnumerable<PopAvailabilityProcessorOutput> availabilities)
        {
            return availabilities != null && availabilities.Any() && availabilities.All(a => a.BeforeXmas);
        }

        public static string GetPictoLabel(this IEnumerable<PopAvailabilityProcessorOutput> availabilities)
        {
            return availabilities?.FirstOrDefault(a => a.BeforeXmas)?.PictoLabel;
        }
    }
}
