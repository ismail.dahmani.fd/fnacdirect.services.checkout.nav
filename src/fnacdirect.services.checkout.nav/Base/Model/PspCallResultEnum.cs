﻿namespace FnacDirect.OrderPipe.Base.Model
{
    public enum PspCallResultEnum
    {
        Success,
        Cancelled,
        Declined,
        TechnicalError
    }
}
