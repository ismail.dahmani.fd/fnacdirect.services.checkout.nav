﻿using FnacDirect;
using System.Web.Mvc;

namespace FnacDirect.OrderPipe.BaseMvc.Web.ContextualizedOperations
{
    public class ContextualizedOperationsAreaRegistrationPart
    {
        private readonly ISwitchProvider _switchProvider;

        public ContextualizedOperationsAreaRegistrationPart(ISwitchProvider switchProvider)
        {
            _switchProvider = switchProvider;
        }

        public void RegisterArea(AreaRegistrationContext areaRegistrationContext)
        {
            if (_switchProvider.IsEnabled("orderpipe.pop.contextualizedoperation"))
            {
                areaRegistrationContext.MapRoute(
                    name: "ContextualizedOperation - Index",
                    url: "offre-partenaire/{publicId}/{token}",
                    defaults: new { controller = "ContextualizedOperation", action = "Index", token = UrlParameter.Optional }
                );
            }
        }
    }
}
