﻿using FnacDirect.Contracts.Online.Model;
using FnacDirect.SalesOffer.Catalog.Data.Core.Repositories;
using FnacDirect.Technical.Framework.Web;
using FnacDirect.Technical.Framework.Web.UrlMgr;
namespace FnacDirect.OrderPipe.Base.Business.Configuration
{
    public class OfferUrlProvider : IOfferUrlProvider
    {
        private readonly IUrlManager urlManager;
        public OfferUrlProvider(IUrlManager urlManager)
        {
            this.urlManager = urlManager;
        }
        public UrlWriter GetSellerOffersUrl(MarketPlaceSeller seller)
        {
            var defaultUrl = urlManager.GetPageInWWWDotNet("MarketPlaceSellerOffers")?.GetUrl(seller);
            var isFnacDarty = seller.SellerType == (int)LineGroups.SellerType.Fnac;
            if (!isFnacDarty)
            {
                return defaultUrl;
            }
            switch (FnacContext.Current.SiteContext.Market)
            {
                case Constants.LocalCountry.FRANCE:
                    return urlManager.Sites.CurrentSite.BaseRootUrl.WithPage("Reconditionne-et-occasion");
                default:
                    return defaultUrl;
            }
        }
        public string GetUrl(MarketPlaceSeller seller)
        {
            return GetSellerOffersUrl(seller)?.ToString() ?? string.Empty;
        }
    }
}
