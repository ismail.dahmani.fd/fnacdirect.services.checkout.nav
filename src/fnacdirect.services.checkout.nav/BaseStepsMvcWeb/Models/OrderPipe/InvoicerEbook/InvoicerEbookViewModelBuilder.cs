﻿using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.OrderPipe.Base.Model.ThankYou;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.InvoicerEbook
{
    public class InvoicerEbookViewModelBuilder : IInvoicerEbookViewModelBuilder
    {
        public InvoicerEbookViewModel Build(PopOrderForm popOrderForm)
        {
            var model = new InvoicerEbookViewModel();
            var thankYouOrderDetailData = popOrderForm.GetPrivateData<ThankYouOrderDetailData>();

            var logisticLineGroup = popOrderForm.LogisticLineGroups.FirstOrDefault(l => l.IsNumerical);

            if (logisticLineGroup != null)
            {
                model.OrderDate = logisticLineGroup.OrderDate.ToString("ddMMyyyy");
                model.OrderUserReference = logisticLineGroup.OrderUserReference;
                model.OrderReference = logisticLineGroup.OrderReference;
            }

            return model;
        }
    }
}
