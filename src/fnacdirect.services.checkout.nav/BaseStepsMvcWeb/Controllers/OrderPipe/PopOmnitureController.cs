using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.OrderPipe.Base.Steps.Pop;
using FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop;
using FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Omniture;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Controllers.Pop
{
    public class PopOmnitureController : PopBaseController
    {
        private readonly OPContext _opContext;
        private readonly IOmnitureViewModelBuilder _omnitureViewModelBuilder;

        public PopOmnitureController(OPContext opContext, IOmnitureViewModelBuilder omnitureViewModelBuild)
        {
            _opContext = opContext;
            _omnitureViewModelBuilder = omnitureViewModelBuild;
        }

        [ChildActionOnly]
        public ActionResult Basket()
        {
            var popOrderForm = _opContext.OF as PopOrderForm;

            var viewModel = _omnitureViewModelBuilder.BuildCommon(popOrderForm);

            return PartialView("_Basket", viewModel);
        }

        [ChildActionOnly]
        public ActionResult OnePage()
        {
            var popOrderForm = _opContext.OF as PopOrderForm;

            var viewModel = _omnitureViewModelBuilder.BuildCommon(popOrderForm);

            return PartialView("_OnePage", viewModel);
        }

        [ChildActionOnly]
        public ActionResult Shipping(int sellerIndex)
        {
            var popOrderForm = _opContext.OF as PopOrderForm;

            var popShippingData = popOrderForm.GetPrivateData<PopShippingData>();

            var popShippingSellerData = popShippingData.GetCurrentShippingSellerData();

            if (popShippingSellerData == null
                || sellerIndex != popShippingData.Sellers.IndexOf(popShippingSellerData))
            {
                return new EmptyResult();
            }

            var viewModel = _omnitureViewModelBuilder.BuildShipping(popOrderForm, popShippingSellerData.SellerId);

            return PartialView("_Shipping", viewModel);
        }

        [ChildActionOnly]
        public ActionResult Payment()
        {
            var popOrderForm = _opContext.OF as PopOrderForm;

            var viewModel = _omnitureViewModelBuilder.BuildCommon(popOrderForm);

            return PartialView("_Payment", viewModel);
        }

        [ChildActionOnly]
        public ActionResult OrderThankYou()
        {
            var popOrderForm = _opContext.OF as PopOrderForm;

            var viewModel = _omnitureViewModelBuilder.BuildOrderThankYou(popOrderForm);

            return PartialView("_OrderThankYou", viewModel);
        }
    }
}
