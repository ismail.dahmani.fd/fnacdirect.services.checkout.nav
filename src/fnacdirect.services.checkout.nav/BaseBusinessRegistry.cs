using FnacDirect;
using FnacDirect.MBWay.Configuration;
using FnacDirect.Orange.Configuration;
using FnacDirect.OrderPipe;
using FnacDirect.OrderPipe.Base.Business;
using FnacDirect.OrderPipe.Base.Business.Availability;
using FnacDirect.OrderPipe.Base.Business.Billing;
using FnacDirect.OrderPipe.Base.Business.Configuration;
using FnacDirect.OrderPipe.Base.Business.FDV;
using FnacDirect.OrderPipe.Base.Business.InstallmentPayment;
using FnacDirect.OrderPipe.Base.Business.Limonetik;
using FnacDirect.OrderPipe.Base.Business.LogisticLineGroups;
using FnacDirect.OrderPipe.Base.Business.LogisticLineGroups.Internals.Handlers;
using FnacDirect.OrderPipe.Base.Business.LogisticLineGroups.Internals.Rules;
using FnacDirect.OrderPipe.Base.Business.Mailing;
using FnacDirect.OrderPipe.Base.Business.MainOrderTransactionInfo;
using FnacDirect.OrderPipe.Base.Business.Membership;
using FnacDirect.OrderPipe.Base.Business.Neolane;
using FnacDirect.OrderPipe.Base.Business.Ogone;
using FnacDirect.OrderPipe.Base.Business.OneClick;
using FnacDirect.OrderPipe.Base.Business.OnePage;
using FnacDirect.OrderPipe.Base.Business.Operations;
using FnacDirect.OrderPipe.Base.Business.OrderDetail;
using FnacDirect.OrderPipe.Base.Business.OrderTransaction;
using FnacDirect.OrderPipe.Base.Business.Payment;
using FnacDirect.OrderPipe.Base.Business.PaymentConfiguration;
using FnacDirect.OrderPipe.Base.Business.Quote;
using FnacDirect.OrderPipe.Base.Business.Registries;
using FnacDirect.OrderPipe.Base.Business.Shipping;
using FnacDirect.OrderPipe.Base.Business.Shipping.Postal;
using FnacDirect.OrderPipe.Base.Business.Shipping.Postal.Appointment.Fly;
using FnacDirect.OrderPipe.Base.Business.Shipping.Postal.AppointmentDelivery.Fly;
using FnacDirect.OrderPipe.Base.Business.Shipping.Relay;
using FnacDirect.OrderPipe.Base.Business.Shipping.Rules;
using FnacDirect.OrderPipe.Base.Business.Shipping.Shop;
using FnacDirect.OrderPipe.Base.Business.ShoppingCart;
using FnacDirect.OrderPipe.Base.Business.Stock;
using FnacDirect.OrderPipe.Base.Business.TaxCalculation;
using FnacDirect.OrderPipe.Base.Business.Tickets;
using FnacDirect.OrderPipe.Base.Business.Wrap;
using FnacDirect.OrderPipe.Base.Proxy;
using FnacDirect.OrderPipe.CM.Business;
using FnacDirect.OrderPipe.Contracts.Services.OneClick;
using FnacDirect.OrderPipe.Contracts.Services.Tickets;
using FnacDirect.OrderPipe.FDV.Business;
using FnacDirect.Solex.AppointmentDelivery.Client;
using FnacDirect.Solex.Shipping.Client;
using FnacDirect.Solex.Shipping.Client.Contract;
using FnacDirect.Technical.Framework.Configuration;
using FnacDirect.TVRoyaltiesAddressNormalization;
using System;
using System.Linq;

namespace StructureMap.Configuration.DSL
{
    public static class OrderPipeBusinessRegistryExtensions
    {
        public static OrderPipeBusinessRegistryBuilder AddOrderPipeBusiness(this IRegistry registry, ISwitchProvider switchProvider, IConfigurationProvider configurationProvider)
        {
            registry.AddOfferBusiness();

            registry.For<FnacDirect.OrderPipe.Contracts.Services.ICreditCardDescriptorService>().Use<CreditCardDescriptorBusiness>();
            registry.For<IAdditionalFeeBusiness>().Use<AdditionalFeeBusiness>();
            registry.For<IAddressOPBusiness>().Use<AddressOPBusiness>();
            registry.For<IAddressService>().Use<AddressService>();
            registry.For<IAdherentBusiness>().Use<AdherentBusiness>();
            registry.For<IAppointmentDeliveryCalendarBusiness>().Use<AppointmentDeliveryCalendarBusiness>();
            registry.For<IAppointmentDeliveryCalendarService>().Use<AppointmentDeliveryCalendarService>();
            registry.For<IArticleService>().Singleton().Use<ArticleService>();
            registry.For<IArticleDetailService>().Use<ArticleDetailService>();
            registry.For<IArticleDetailServiceWithShopGetArticleBehavior>().Use<ArticleDetailServiceWithShopGetArticleBehavior>();
            registry.For<IArticleDetailsShopService>().Use<ArticleDetailsShopService>();
            registry.For<IArticleSubTitleService>().Use<ArticleSubTitleService>();
            registry.For<IAvailabilityLabelRuleFinder>().Use<AvailabilityLabelRuleFinder>();
            registry.For<IAvoirOccazPaymentService>().Use<AvoirOccazPaymentService>();
            registry.For<IBankPaymentService>().Use<BankPaymentService>();
            registry.For<IBillingAddressElegibilityServiceProvider>().Use<BillingAddressElegibilityServiceProvider>();
            registry.For<IBillingMethodBusiness>().Use<BillingMethodBusiness>();
            registry.For<IBillingMethodService>().Use<BillingMethodService>();
            registry.For<IBillingMethodsManager>().Use("IBillingMethodsManager factory", instance =>
            {
                var opContext = instance.GetInstance<OPContext>();

                if (opContext == null)
                {
                    throw new InvalidOperationException("IBillingMethodsManager could only be injected into pipe execution context.");
                }

                if (!(opContext.Pipe is BaseOP pipe))
                {
                    throw new InvalidOperationException("IBillingMethodsManager could only be injected for BaseOP family pipes.");
                }

                return pipe.BillingMethodsManager;
            });
            registry.For<ICalculateTotalsBusiness>().Use<CalculateTotalsBusiness>();
            registry.For<ICheckArticleService>().Use<CheckArticleService>();
            registry.For<ICheckCreditCardsEligibilityBusiness>().Use<CheckCreditCardsEligibilityBusiness>();
            registry.For<ICheckCreditCardsEligibilityBusinessProvider>().Use<CheckCreditCardsEligibilityBusinessProvider>();
            registry.For<ICheckOriginService>().Use<CheckOriginService>();
            registry.For<IClickAndMagBusiness>().Singleton().Use<ClickAndMagBusiness>();
            registry.For<ICommerceOrderDetailService>().Use<CommerceOrderDetailService>();
            registry.For<IComputeOneClickAvailability>().Use<OneClickAvailabilityService>();
            registry.For<ICreditCardDescriptorBusiness>().Use<CreditCardDescriptorBusiness>();
            registry.For<ICreditCardUsageBusiness>().Use<CreditCardUsageBusiness>();
            registry.For<ICreditCardTransactionBusiness>().Use<CreditCardTransactionBusiness>();
            registry.For<ICustomerContextProvider>().Use<CustomerContextProvider>();
            registry.For<ICUUPaymentService>().Use<CUUPaymentService>();
            registry.For<IDeferredPaymentBusiness>().Use<DeferredPaymentBusiness>();
            registry.For<IDeferredPaymentService>().Use<DeferredPaymentService>();
            registry.For<IDeliveredForChristmasService>().Use<DeliveredForChristmasService>();
            registry.For<IDematSoftBusiness>().Use<DematSoftBusiness>();
            registry.For<IFidelityEurosPaymentService>().Use<FidelityEurosPaymentService>();
            registry.For<IFidelityPointsOccazPaymentService>().Use<FidelityPointsOccazPaymentService>();
            registry.For<IFnacPlusCardService>().Use<FnacPlusCardService>();
            registry.For<IGiftCardBusiness>().Use<GiftCardBusiness>();
            registry.For<IGiftPaymentService>().Use<GiftPaymentService>();
            registry.For<IGuestService>().Use<GuestService>();
            registry.For<IGuptCMBusinessProvider>().Use<GuptCMBusinessProvider>();
            registry.For<IGuptFnacComBusinessProvider>().Use<GuptFnacComBusinessProvider>();
            registry.For<IItemService>().Use<ItemBusiness>();
            registry.For<IKeyGeneratorBusiness>().Use<KeyGeneratorBusiness>();
            registry.For<IKeyLoaderBusiness>().Use<KeyLoaderBusiness>();
            registry.For<ILimonetikBusiness>().Use<LimonetikBusiness>();
            registry.For<IWrapBusiness>().Singleton().Use<WrapBusiness>();
            registry.For<ILogisticLineGroupGenerationProcess>().Use<LogisticLineGroupGenerationProcess>();
            registry.For<ILogisticLineGroupGenerationRuleFactory>().Use<StructureMapLogisticLineGroupGenerationRuleFactory>();
            registry.For<ILogisticLineGroupGenerator>().Use<LogisticLineGroupGenerator>();
            registry.For<ILogisticLineGroupProcessCompletedHandlerFactory>().Use<StructureMapLogisticLineGroupProcessCompletedHandlerFactory>();
            registry.For<IMailingOrderModelBuilder>().Use<MailingOrderModelBuilder>();
            registry.For<IMarketPlaceServiceProvider>().Use<MarketPlaceServiceProvider>();
            registry.For<IMembershipDateBusiness>().Use<MembershipDateBusiness>();
            registry.For<IMergeLineGroupsArticlesBusiness>().Use<BaseBusiness>();
            registry.For<IMessageServiceSoap>().Use(context => new MessageServiceSoapClient());
            registry.For<INeolaneService>().Use<NeolaneService>();
            registry.For<INewAdhesionCMBusiness>().Use<NewAdhesionCMBusiness>();
            registry.For<INormalizer>().Use<Normalizer>();
            registry.For<INumericalBusiness>().Use<NumericalBusiness>();
            registry.For<IOfferBusinessProvider>().Use<OfferBusinessProvider>();
            registry.For<IOgoneBusiness>().Use<OgoneBusiness>();
            registry.For<IOgoneBusiness3DSecure>().Use<OgoneBusiness3DSecure>();
            registry.For<IOgoneCvcStatusService>().Use<OgoneCvcStatusService>();
            registry.For<IOgonePaymentService>().Use<OgonePaymentService>();
            registry.For<IOneClickAvailabilityService>().Use<OneClickAvailabilityService>();
            registry.For<IOnePageService>().Use<OnePageService>();
            registry.For<IOptinWarrantyBusiness>().Use<OptinWarrantyBusiness>();
            if (switchProvider.IsEnabled("orderpipe.pop.payment.orange"))
            {
                registry.For<IOrangeBusiness>().Use<OrangeBusiness>();
                registry.AddOrange(configurationProvider);
            }
            registry.For<IOrangeConfigurationProvider>().Singleton().Use<OrangeConfigurationProvider>();
            registry.For<IOrangePaymentService>().Use<OrangePaymentService>();
            registry.For<IMBWayPaymentService>().Use<MBWayPaymentService>();



            if (switchProvider.IsEnabled("orderpipe.pop.payment.mbway"))
            {
                registry.For<IMBWayBusiness>().Use<MBWayBusiness>();
                registry.AddMBWay(configurationProvider);
            }
            registry.For<IMBWayConfigurationProvider>().Singleton().Use<MBWayConfigurationProvider>();




            registry.For<IOrderDetailService>().Use<OrderDetailService>();
            registry.For<IOrderDiscountBusiness>().Use<OrderDiscountBusiness>();
            registry.For<IOrderTransactionBusiness>().Use<OrderTransactionBusiness>();
            registry.For<IOrderWithPendingPaymentBusiness>().Use<OrderWithPendingPaymentBusiness>();
            registry.For<IPaymentConfigurationBusiness>().Use<PaymentConfigurationBusiness>();
            registry.For<IPaymentMethodMatrixService>().Use<PaymentMethodMatrixService>();
            registry.For<IPaymentSelectionBusiness>().Use<PaymentSelectionBusiness>();
            registry.For<IPaymentSelectionBusinessProvider>().Use<PaymentSelectionBusinessProvider>();
            registry.For<IPaypalPaymentService>().Use<PaypalPaymentService>();
            registry.For<IPersonnelNumberBusiness>().Use<PersonnelNumberBusiness>();
            registry.For<IPhonePaymentService>().Use<PhonePaymentService>();
            registry.For<IPopAvailabilityProcessor>().Use<PopAvailabilityProcessor>();
            registry.For<IPricingServiceProvider>().Use<PricingServiceProvider>();
            registry.For<IPromotionCodeService>().Use<PromotionCodeService>();
            registry.For<IPromotionStoreService>().Use<PromotionStoreService>();
            registry.For<ICapencyService>().Singleton().Use<CapencyService>();
            registry.For<IQuoteDetailsBusiness>().Use<QuoteDetailsBusiness>();
            registry.For<IRelayAddressService>().Use<RelayAddressService>();
            registry.For<IRelayCellphoneService>().Use<RelayCellphoneService>();
            registry.For<IRelayValidityService>().Use<RelayValidityService>();
            registry.For<IRemoteRebateBusiness>().Use<RemoteRebateBusiness>();
            registry.For<IRemoveUnavailableOffersBusinessProvider>().Use<RemoveUnavailableOffersBusinessProvider>();
            registry.For<ISelfCheckoutService>().Singleton().Use<SelfCheckoutService>();
            registry.For<IShippingAddressElegibilityServiceProvider>().Use<ShippingAddressElegibilityServiceProvider>();
            registry.For<IShippingEvaluationBusiness>().Use<ShippingEvaluationBusiness>();
            registry.For<IShippingChoiceCostService>().Use<ShippingChoiceCostService>();
            registry.For<IShippingMethodBasketBuildingRuleFactory>().Use<ShippingMethodBasketBuildingRuleFactory>();
            registry.For<IShippingMethodBasketService>().Use<ShippingMethodBasketService>();
            registry.For<IShippingMethodBasketServiceProvider>().Use<ShippingMethodBasketServiceProvider>();
            registry.For<IShippingStateService>().Use<ShippingStateService>();
            registry.For<IShipToStoreAvailabilityService>().Singleton().Use<ShipToStoreAvailabilityService>();
            registry.For<IShoppingCartBaseBusiness>().Use<BaseBusiness>();
            registry.For<IShoppingCartPersistantConfigurationService>().Use<ShoppingCartPersistantConfigurationService>();
            registry.For<IShoppingCartService>().Use<ShoppingCartService>();
            registry.For<IShopTicketBuilderService>().Singleton().Use<ShopTicketBuilderService>();
            registry.For<ISolexBusiness>().Use<SolexBusiness>();
            registry.For<ISolexConfigurationProvider>().Singleton().Use<SolexConfigurationProvider>();
            registry.For<ISolexObjectConverterService>().Use<SolexObjectConverterService>();
            registry.For<ISplitBillingMethodBusiness>().Use<SplitBillingMethodBusiness>();
            registry.For<IStockReservationServiceFactory>().Use<StockReservationServiceFactory>();
            registry.For<ISubscriptionService>().Use<SubscriptionService>();
            registry.For<ITicketService>().Singleton().Use<TicketService>();
            registry.For<ITransfertShoppingCartService>().Use<TransfertShoppingCartService>();
            registry.For<ITvRoyaltyBusiness>().Use<TvRoyaltyBusiness>();
            registry.For<IUnavailableArticleHandlerFactory>().Singleton().Use<UnavailableArticleHandlerFactory>();
            registry.For<IUpdateArticlesBusiness>().Use<BaseBusiness>();
            registry.For<IVatCountryCalculationService>().Use<VatCountryCalculationService>();
            registry.For<IVoucherBusiness>().Use<VoucherBusiness>();
            registry.For<IW3ShoppingCart>().Use<W3ShoppingCartService>();
            registry.For<IShippingChoiceAggregationService>().Use<ShippingChoiceAggregationService>();
            registry.For<IAppointmentDeliveryService>().Use<AppointmentDeliveryService>();
            registry.For<IAggregatedMethodConfigurationProvider>().Use<AggregatedMethodsConfigurationProvider>();
            registry.For<ICustomerSegmentEvaluatorProvider>().Use<CustomerSegmentEvaluatorProvider>();
            registry.For<IGuptHelperService>().Use<GuptHelperService>();
            registry.For<IStomEcoTaxBusiness>().Use<StomEcoTaxBusiness>();
            registry.For<IFlyBusiness>().Singleton().Use<FlyBusiness>();
            registry.For<IInstallmentService>().Singleton().Use<InstallmentService>();



            registry.For<ITaxCalculationBusiness>().Use<TaxCalculationBusiness>();
            registry.For<IAddressValidationBusiness>().Use<AddressValidationBusiness>();

            registry.For<IPaymentEligibilityService>().Use<PaymentEligibilityService>();

            var guptConfiguration = configurationProvider.GetAs<GuptConfiguration>("guptConfiguration");
            registry.For<GuptConfiguration>().Singleton().Use(guptConfiguration);

            registry.AddPaymentConfigurationFront();

            registry.AddSolex(configurationProvider);

            return new OrderPipeBusinessRegistryBuilder(registry);
        }

        private static void AddSolex(this IRegistry registry, IConfigurationProvider configurationProvider)
        {
            var solexConfiguration = configurationProvider.GetAs<SolexConfiguration>("solex");

            var appointmentDeliveryConfiguration = new AppointmentDeliveryConfiguration
            {
                ApiUrl = solexConfiguration.AppointmentDelivery.EndpointAddress
            };

            var shippingConfiguration = new ShippingConfiguration
            {
                SqlOffer = solexConfiguration.Databases.SqlOffer,
                SqlCatalog = solexConfiguration.Databases.SqlCatalog,
                SqlMetaCatalog = solexConfiguration.Databases.SqlMetaCatalog,
                SqlStom = solexConfiguration.Databases.SqlStom,
                SiteId = solexConfiguration.SiteId,
                AvailabilityConfiguration = new AvailabilityConfiguration
                {
                    CommunityStoreAddress = solexConfiguration.StoresUri,
                    DefaultAddressLine = solexConfiguration.DefaultPostalAddress.AddressLine,
                    DefaultZipCode = solexConfiguration.DefaultPostalAddress.ZipCode,
                    DefaultCity = solexConfiguration.DefaultPostalAddress.City,
                    DefaultCountry = solexConfiguration.DefaultPostalAddress.Country,
                    SqlOffer = solexConfiguration.Databases.SqlOffer,
                    SqlAdminSolex = solexConfiguration.Databases.SqlAdminSolex,
                    DatesCalculatorConfiguration = new DatesCalculatorConfiguration
                    {
                        AvailabilityCodeToDelay = solexConfiguration.AvailabilityCodeToDelay.ToDictionary(x => int.Parse(x.Key), x => int.Parse(x.Value)),
                        ShippingMethodsExcluded = solexConfiguration.ShippingMethodNotToBeLogged
                    },
                    ShippingNetworksConfiguration = new ShippingNetworksConfiguration
                    {
                        PostalNetworkId = solexConfiguration.ShippingNetworks.PostalNetworkId,
                        RelayNetworkId = solexConfiguration.ShippingNetworks.RelayNetworkId,
                        ShopNetworkId = solexConfiguration.ShippingNetworks.ShopNetworkId
                    },
                    ShippingAndStockLocationsConfiguration = new ShippingAndStockLocationsConfiguration
                    {
                        ComputeLocationMethod = solexConfiguration.ShippingAndStockLocations.ComputeLocationMethod
                    }
                },
                EvaluateProductOptions = new EvaluateOptions()
                {
                    IncludeSourcesMode = IncludeSourcesMode.IncludeWhitelist,
                    IncludeSourcesWhitelist = solexConfiguration.EvaluateProductIncludeSourcesWhitelist
                },
                IsLoggingEvaluateProductQuery = solexConfiguration.IsLoggingEvaluateProductQuery,
                IsLoggingEvaluateProductsQuery = solexConfiguration.IsLoggingEvaluateProductsQuery,
                IsLoggingEvaluateShoppingCartQuery = solexConfiguration.IsLoggingEvaluateShoppingCartQuery
            };

            registry.AddAppointmentDeliveryClient(appointmentDeliveryConfiguration);
            registry.AddShippingClient(shippingConfiguration);
        }

        private static void AddOrange(this IRegistry registry, IConfigurationProvider configurationProvider)
        {
            var orangeConfiguration = configurationProvider.GetAs<OrangeConfiguration>("orange");

            var orangeDalConfiguration = new OrangeDalConfiguration
            {
                SqlCommerce = orangeConfiguration.SqlCommerce
            };
            registry.AddOrangeDal(orangeDalConfiguration);

            var orangeApiConfiguration = new OrangeApiConfiguration
            {
                OrangeApiUrl = orangeConfiguration.OrangeApiUrl,
                ProxyOrangeApi = orangeConfiguration.ProxyOrangeApi
            };
            registry.AddOrangeServices(orangeApiConfiguration);
        }

        private static void AddMBWay(this IRegistry registry, IConfigurationProvider configurationProvider)
        {
            var MBWayConfiguration = configurationProvider.GetAs<MBWayConfiguration>("MBWay");

            var MBWayDalConfiguration = new MBWayDalConfiguration
            {
                SqlCommerce = MBWayConfiguration.SqlCommerce
            };
            registry.AddMBWayDal(MBWayDalConfiguration);

            var MBWayApiConfiguration = new MBWayApiConfiguration
            {
                MBWayApiUrl = MBWayConfiguration.MBWayApiUrl,
                EntityId = MBWayConfiguration.EntityId,
                Currency = MBWayConfiguration.Currency,
                PaymentType = MBWayConfiguration.PaymentType,
                PaymentBrand = MBWayConfiguration.PaymentBrand,
                AccountId = MBWayConfiguration.AccountId,
                ShopperResultUrl = MBWayConfiguration.ShopperResultUrl,
                Authorization = MBWayConfiguration.Authorization
            };
            registry.AddMBWayServices(MBWayApiConfiguration);
        }
    }
}
