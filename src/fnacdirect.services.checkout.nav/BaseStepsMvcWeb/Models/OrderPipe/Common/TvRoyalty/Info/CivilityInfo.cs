﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop
{
    public class CivilityInfo
    {
        public int Id { get; set; }
        public string Label { get; set; }
    }
}
