

namespace FnacDirect.OrderPipe.Base.Model
{
    /// <summary>
    /// Status des ordershoptransaction dans commerce
    /// </summary>
    public enum OrderShopTransactionStatus
    {
        /// <summary>
        /// -2 Transaction orderpipe appel GUPT
        /// </summary>
        OrderPipeTransactionCallGupt = -2,
        /// <summary>
        /// -1 Transaction non trouvée en GUPT
        /// </summary>
        OrderPipeTransactionNotFound = -1,
        /// <summary>
        /// 0 Transaction Créée
        /// </summary>
        TransactionCreated = 0,
        /// <summary>
        /// 1 Transaction enregistrée en GUPT
        /// </summary>
        TransactionRecorded = 1,
        /// <summary>
        /// 2 Transaction confirmée
        /// </summary>
        TransactionConfirmed = 2,
        /// <summary>
        /// 3 Transaction Validée
        /// </summary>
        TransactionValidated = 3,
        /// <summary>
        /// 4 Transaction Annulée
        /// </summary>
        TransactionCanceled = 4,
        /// <summary>
        /// 5 Transaction Remboursée
        /// </summary>
        TransactionRefunded = 5,
        /// <summary>
        /// 6 Transaction Retirée
        /// </summary>
        TransactionWithdrawn = 6,
        /// <summary>
        /// 7 Transaction Refusé
        /// </summary>
        TransactionRefused = 7,
        /// <summary>
        /// 8 Transaction A Annuler
        /// </summary>
        TransactionToCancel = 8,
        /// <summary>
        /// 9 Transaction A Rembourser
        /// </summary>
        TransactionToRefund = 9,
        /// <summary>
        /// 10 Transaction et facture A Annuler
        /// </summary>
        TransactionAndInvoiceToCancel = 10,
        /// <summary>
        /// 11 Transaction confirmée sans réponse S98
        /// </summary>
        TransactionConfirmedWithoutS98Response = 11
    }
}
