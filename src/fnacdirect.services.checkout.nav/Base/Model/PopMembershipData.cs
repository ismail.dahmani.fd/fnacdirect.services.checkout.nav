﻿
namespace FnacDirect.OrderPipe.Base.Model.Adherent
{
    public class PopMembershipData : GroupData
    {
        public bool IsRenewable { get; set; }

        public decimal CardPrice { get; set; }

        public decimal AdjustedCardPrice { get; set; }

        public bool DisplayNotification { get; set; }

        public bool NotificationHasBeenDisplayed { get; set; }
        
        public bool MembershipCardHasBeenAdjusted { get; set; }       
    }
}
