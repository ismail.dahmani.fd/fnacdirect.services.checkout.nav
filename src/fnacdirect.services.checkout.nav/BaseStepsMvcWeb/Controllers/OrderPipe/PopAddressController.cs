using FnacDirect.Contracts.Online.Model;
using FnacDirect.Customer.Model;
using FnacDirect.OrderPipe.Base.Business.Shipping.Postal;
using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.OrderPipe.Base.Steps.Pop;
using FnacDirect.OrderPipe.BaseMvc.Web.Models.OnePage.Address.Extensions;
using FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.OnePage.Address;
using FnacDirect.OrderPipe.Core.Services;
using FnacDirect.Technical.Framework.Web.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Net;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Controllers.Pop
{
    public class PopAddressController : PopBaseController
    {
        private readonly IApplicationContext _applicationContext;
        private readonly IUserExtendedContextProvider _userExtendedContextProvider;
        private readonly IAddressViewModelBuilder _addressViewModelBuilder;
        private readonly IAddressService _addressService;
        private readonly ICountryService _countryService;
        private readonly IOrderPipeExecutionService _orderPipeExecutionService;
        private readonly OPContext _opContext;
        private readonly ISwitchProvider _switchProvider;

        public PopAddressController(OPContext opContext,
            IAddressViewModelBuilder addressViewModelBuilder,
            IAddressService addressService,
            ICountryService countryService,
            IApplicationContext applicationContext,
            IOrderPipeExecutionService orderPipeExecutionService,
            ISwitchProvider switchProvider,
            IUserExtendedContextProvider userExtendedContextProvider)
        {
            _applicationContext = applicationContext;
            _userExtendedContextProvider = userExtendedContextProvider;
            _addressViewModelBuilder = addressViewModelBuilder;
            _addressService = addressService;
            _countryService = countryService;
            _opContext = opContext;
            _orderPipeExecutionService = orderPipeExecutionService;
            _switchProvider = switchProvider;
        }

        [OutputCache(Duration = 0), HttpGet]
        public ActionResult Create(AddressFormViewModelMode addressFormViewModelMode)
        {
            return ChildCreate(addressFormViewModelMode);
        }

        [ChildActionOnly]
        public ActionResult ChildCreate(AddressFormViewModelMode addressFormViewModelMode)
        {
            var addressFormViewModel = _addressViewModelBuilder.BuildForm(addressFormViewModelMode);

            return PartialView("Index", addressFormViewModel);
        }

        [OutputCache(Duration = 0), HttpGet]
        public ActionResult Update(AddressFormViewModelMode addressFormViewModelMode, string addressReference, int sellerId)
        {
            return ChildUpdate(addressFormViewModelMode, addressReference, sellerId);
        }

        [ChildActionOnly]
        public ActionResult ChildUpdate(AddressFormViewModelMode addressFormViewModelMode, string addressReference, int sellerId)
        {
            var addressFormViewModel = _addressViewModelBuilder.GetAddressFormViewModelByAddressReference(addressReference, addressFormViewModelMode, sellerId);

            //When we open the popin those checkbox should be always checked
            addressFormViewModel.AddressViewModel.UseAsDefaultBillingAddress = true;
            addressFormViewModel.AddressViewModel.UseAsDefaultAddress = true;

            return PartialView("Index", addressFormViewModel);
        }

        private ActionResult CheckUserAuthenticationLevel()
        {
            var userExtendedContext = _userExtendedContextProvider.GetCurrentUserExtendedContext();

            var visitor = userExtendedContext.Visitor;

            if (!visitor.IsAuthenticated || !visitor.Session.IsGranted(Customer.Model.Constants.SessionLevel.Full))
            {
                var popOrderForm = _opContext.OF as PopOrderForm;
                var displayLoginData = popOrderForm.GetPrivateData<DisplayLoginData>(create: false);
                if (displayLoginData != null)
                {
                    if (displayLoginData.Parameters.Any())
                    {
                        //TODO : duplicate from popPageRedirectorController maybe we can redirect to the index of this controller

                        var queryString = string.Join("&", displayLoginData.Parameters.Select(p => string.Format("{0}={1}", p.Key, p.Value)));

                        Response.Headers.Add("Location", string.Format("{0}?{1}", displayLoginData.ChallengeUrl, queryString));
                    }
                    else
                    {
                        Response.Headers.Add("Location", displayLoginData.ChallengeUrl);
                    }
                }

                return new HttpUnauthorizedResult();
            }

            return null;
        }

        [HttpPost]
        public async Task<ActionResult> CreateAsync(AddressFormViewModelMode addressFormViewModelMode, AddressViewModel addressViewModel)
        {
            return CheckUserAuthenticationLevel() ?? await SaveOrUpdateAddressAsync(addressFormViewModelMode, addressViewModel, SaveMode.Create);
        }

        [HttpPost]
        public async Task<ActionResult> UpdateAsync(AddressFormViewModelMode addressFormViewModelMode, AddressViewModel addressViewModel)
        {
            return CheckUserAuthenticationLevel() ?? await SaveOrUpdateAddressAsync(addressFormViewModelMode, addressViewModel, SaveMode.Update);
        }

        private async Task<ActionResult> SaveOrUpdateAddressAsync(AddressFormViewModelMode addressFormViewModelMode, AddressViewModel addressViewModel, SaveMode savemode)
        {
            if (ModelState.IsValid)
            {
                var isAddressEligibleForDependingSellers = IsAddressEligibleForDependingSellers(addressViewModel);
                if (savemode == SaveMode.Update && !isAddressEligibleForDependingSellers)
                {
                    return PopulateAndRedirectToAddressForm(addressViewModel, addressFormViewModelMode, false);
                }

                var addresses = Enumerable.Empty<IAddress>();
                var isEligible = true;
                var errorElegibilityKey = string.Empty;
                var popOrderForm = _opContext.OF as PopOrderForm;

                var lineGroups = popOrderForm.LineGroups.Seller(addressViewModel.SellerId.GetValueOrDefault());

                switch (addressFormViewModelMode)
                {
                    case AddressFormViewModelMode.Shipping:
                        isEligible = _addressService.IsAddressEligible(_opContext, popOrderForm, lineGroups, addressViewModel.ToAddressEntity(_countryService));
                        errorElegibilityKey = "orderpipe.index.shiptoaddress.ineligibleaddress";
                        break;
                    case AddressFormViewModelMode.Billing:
                        isEligible = _addressService.IsBillingAddressEligible(_opContext, popOrderForm.UserContextInformations, popOrderForm.LineGroups, addressViewModel.ToAddressEntity(_countryService));
                        errorElegibilityKey = "orderpipe.index.billingaddress.ineligibleaddress";
                        break;
                }

                var isCorrectAddress = true;
                var addressEntity = BuildAddressEntity(addressViewModel);
                var addressValidityResult = await _addressService.GetAddressValidityResultAsync(addressEntity);
                if (addressValidityResult != null)
                {
                    isCorrectAddress = addressViewModel.ByPassQas || IsAddressValid(addressViewModel, addressValidityResult);
                    addresses = addressValidityResult.Addresses;
                }

                if (!isEligible)
                {
                    ModelState.AddModelError("AddressEligibility", _applicationContext.GetMessage(errorElegibilityKey, null));
                    return PopulateAndRedirectToAddressForm(addressViewModel, addressFormViewModelMode);
                }
                else if (!isCorrectAddress)
                {
                    return PopulateAndRedirectToAddressForm(addressViewModel, addressFormViewModelMode, addresses);
                }
                else
                {
                    if (!SaveAddress(addressViewModel, savemode, addressFormViewModelMode))
                    {
                        return PopulateAndRedirectToAddressForm(addressViewModel, addressFormViewModelMode);
                    }

                    if (addressViewModel.UseAsDefaultBillingAddress)
                    {
                        popOrderForm.BillingAddress = addressViewModel.ToBillingAddress(_countryService);
                    }
                }
            }
            else
            {
                return PopulateAndRedirectToAddressForm(addressViewModel, addressFormViewModelMode);
            }

            _orderPipeExecutionService.Rewind();

            Response.StatusCode = (int)HttpStatusCode.Created;

            return Json(addressViewModel.ConvertToShippingAddressViewModel(_applicationContext.GetSiteContext().CountryId));
        }

        private bool IsAddressEligibleForDependingSellers(AddressViewModel addressViewModel)
        {
            var popOrderForm = _opContext.OF as PopOrderForm;
            var sellers = addressViewModel.SellerId.HasValue ? SellersWithValidatedShippingChoice(addressViewModel.SellerId.Value) : new List<int>();

            foreach (var shippingMethodEvaluationGroup in popOrderForm.ShippingMethodEvaluation.ShippingMethodEvaluationGroups.Where(g => g.PostalShippingMethodEvaluationItem.HasValue))
            {
                var shippingAddress = shippingMethodEvaluationGroup.PostalShippingMethodEvaluationItem.Value.ShippingAddress;

                if (shippingAddress.Reference == addressViewModel.AddressReference && sellers.Contains(shippingMethodEvaluationGroup.SellerId))
                {
                    var lineGroups = popOrderForm.LineGroups.Seller(addressViewModel.SellerId.GetValueOrDefault());
                    if (!_addressService.IsAddressEligible(_opContext, popOrderForm, lineGroups, addressViewModel.ToAddressEntity(_countryService)))
                    {
                        return false;
                    }
                }
            }

            return true;
        }

        private IEnumerable<int> SellersWithValidatedShippingChoice(int sellerToExclude)
        {
            var popOrderForm = _opContext.OF as PopOrderForm;

            var popShippingData = popOrderForm.GetPrivateData<PopShippingData>();

            return popShippingData.Sellers.Where(seller => seller.ShippingChoiceValidated && seller.SellerId != sellerToExclude).Select(s => s.SellerId);
        }

        private bool IsAddressValid(AddressViewModel addressViewModel, AddressValidityResult addressVailidityResult)
        {
            if(addressVailidityResult == null)
            {
                return false;
            }
            if (addressVailidityResult.AddressValidityResultType == AddressValidityResultType.Valid)
            {
                return true;
            }
            if (addressVailidityResult.AddressValidityResultType != AddressValidityResultType.Recheck)
            {
                return false;
            }

            var verifiedAddress = addressVailidityResult.Addresses.First();
            if (!IsSameAddress(addressViewModel, verifiedAddress))
            {
                return false;
            }

            //Normalisation de l'adresse
            addressViewModel.Address = verifiedAddress.AddressLine1;
            addressViewModel.Floor = verifiedAddress.AddressLine2;
            addressViewModel.ZipCode = verifiedAddress.Zipcode;
            addressViewModel.City = verifiedAddress.City;

            return true;
        }

        private AddressEntity BuildAddressEntity(AddressViewModel addressViewModel)
        {
            return new AddressEntity
            {
                AddressLine1 = addressViewModel.Address,
                AddressLine2 = addressViewModel.Floor,
                AddressLine3 = addressViewModel.AddressComplimentary,
                Zipcode = addressViewModel.ZipCode,
                City = addressViewModel.City,
                CountryId = addressViewModel.CountryId,
                State = addressViewModel.State,
                Company = addressViewModel.CompanyName
            };
        }

        private static bool IsSameAddress(AddressViewModel addressViewModel, IAddress address)
        {
            return (
                string.Equals(address.AddressLine1.ToStringOrDefault(), addressViewModel.Address.ToStringOrDefault(), StringComparison.CurrentCultureIgnoreCase)
                && string.Equals(address.AddressLine2.ToStringOrDefault(), addressViewModel.Floor.ToStringOrDefault(), StringComparison.CurrentCultureIgnoreCase)
                && string.Equals(address.Zipcode.ToStringOrDefault(), addressViewModel.ZipCode.ToStringOrDefault(), StringComparison.CurrentCultureIgnoreCase)
                && string.Equals(address.City.ToStringOrDefault(), addressViewModel.City.ToStringOrDefault(), StringComparison.CurrentCultureIgnoreCase));
        }

        private bool SaveAddress(AddressViewModel addressViewModel, SaveMode mode, AddressFormViewModelMode addressFormViewModelMode)
        {
            var popOrderForm = _opContext.OF as PopOrderForm;
            var cellPhone = string.Empty;
            var homePhone = string.Empty;

            var selectedCountryWithValidation = _countryService.GetCountriesWithValidations().FirstOrDefault(c => c.Id == addressViewModel.CountryId);
            var selectedCountry = _countryService.GetCountries().FirstOrDefault(c => c.ID == addressViewModel.CountryId);

            if (selectedCountryWithValidation != null)
            {
                var isPhoneValid = Regex.IsMatch(addressViewModel.Phone, selectedCountryWithValidation.CellPhoneValidation);
                var isBelgiumAddress = addressViewModel.CountryId == Constants.Countries.Belgium;
                var isFranceAddress = addressViewModel.CountryId == Constants.Countries.France;
                var isFranceOrBelgiumAddress = (isFranceAddress || isBelgiumAddress);

                if (!isFranceOrBelgiumAddress || (isFranceOrBelgiumAddress && isPhoneValid))
                    cellPhone = addressViewModel.Phone;
                else
                    homePhone = addressViewModel.Phone;
            }

            var addressEntity = new AddressEntity
            {
                AddressBookReference = addressViewModel.AddressReference,
                AliasName = addressViewModel.AddressName ?? string.Format("{0} {1} {2}", addressViewModel.FirstName, addressViewModel.LastName, addressViewModel.City),
                FirstName = addressViewModel.FirstName,
                LastName = addressViewModel.LastName,
                GenderId = Convert.ToInt32(addressViewModel.GenderId),
                Company = addressViewModel.CompanyName,
                AddressLine1 = addressViewModel.Address,
                AddressLine2 = addressViewModel.Floor,
                AddressLine3 = addressViewModel.AddressComplimentary,
                Zipcode = addressViewModel.ZipCode,
                City = addressViewModel.City,
                CountryId = addressViewModel.CountryId,
                DocumentCountryId = addressViewModel.DocumentCountryId,
                State = addressViewModel.State,
                ShippingZone = selectedCountry != null ? selectedCountry.ShippingZone : 0,
                Tel = homePhone,
                CellPhone = cellPhone,
                IsDefaultBilling = addressViewModel.UseAsDefaultBillingAddress,
                IsDefaultShipping = addressViewModel.UseAsDefaultAddress,
                TaxIdNumber = addressViewModel.TaxIdNumber,
                TaxIdNumberType = addressViewModel.TaxNumberType,
                LegalStatusId = addressViewModel.LegalStatus
            };

            SetAutomaticallyTaxNumberTypeForEntreprise(selectedCountry, addressEntity);

            List<ValidationResult> validationResults;
            if (mode == SaveMode.Create)
            {
                if (addressFormViewModelMode == AddressFormViewModelMode.Shipping && addressViewModel.SellerId.HasValue)
                {
                    validationResults = _addressService.Add(_opContext, popOrderForm, addressEntity, popOrderForm.LineGroups.Seller(addressViewModel.SellerId.Value), true).ToList();
                }
                else
                {
                    validationResults = _addressService.AddWithBillingElegibility(_opContext, addressEntity, popOrderForm.LineGroups).ToList();
                }

                addressViewModel.AddressReference = addressEntity.AddressBookReference;
            }
            else
            {
                if (addressFormViewModelMode == AddressFormViewModelMode.Shipping && addressViewModel.SellerId.HasValue)
                {
                    validationResults = _addressService.Update(_opContext, popOrderForm, addressEntity, popOrderForm.LineGroups.Seller(addressViewModel.SellerId.Value), true).ToList();
                }
                else
                {
                    validationResults = _addressService.UpdateWithBillingElegibility(_opContext, addressEntity, popOrderForm.LineGroups).ToList();
                }
            }

            if (validationResults.Any())
            {
                foreach (var validationResult in validationResults)
                {
                    ModelState.AddModelError("AddressEligibility", validationResult.ErrorMessage);
                }
                return false;
            }
            return true;
        }

        /// <summary>
        /// Pour l'Espagne, on est obligé de remplir le TaxIdNumberType
        /// </summary>
        /// <param name="selectedCountry"></param>
        /// <param name="addressEntity"></param>
        private void SetAutomaticallyTaxNumberTypeForEntreprise(Country selectedCountry, AddressEntity addressEntity)
        {
            if (_switchProvider.IsEnabled("orderpipe.pop.address.settaxnumbertypeforentreprise")
                && addressEntity.TaxIdNumberType == null && addressEntity.LegalStatusId == (int)LegalStatus.Business)
            {
                var listWebSiteCountryIds = _opContext.Pipe.GlobalParameters.GetAsEnumerable<int>("country.extensions");
                if (listWebSiteCountryIds.Contains(selectedCountry.ID))
                {
                    addressEntity.TaxIdNumberType = TaxNumberType.CIF.ToString(); // CIF 
                }
                else if (selectedCountry.UseIntraCommunityVat.GetValueOrDefault())
                {
                    addressEntity.TaxIdNumberType = TaxNumberType.NifIntra.ToString(); // NIF intra
                }
                else
                {
                    addressEntity.TaxIdNumberType = TaxNumberType.FiscalNumber.ToString(); //N°Fiscal
                }
            }
        }

        private ActionResult PopulateAndRedirectToAddressForm(AddressViewModel addressViewModel, AddressFormViewModelMode addressFormViewModelMode, bool isEligible = true)
        {
            var viewModel = _addressViewModelBuilder.BuildForm(addressFormViewModelMode, addressViewModel);
            viewModel.IsEligibleForDependingSellers = isEligible;
            return PartialView("Index", viewModel);
        }

        private ActionResult PopulateAndRedirectToAddressForm(AddressViewModel addressViewModel, AddressFormViewModelMode addressFormViewModelMode, IEnumerable<IAddress> addresses)
        {
            return PartialView("Index", _addressViewModelBuilder.BuildForm(addressFormViewModelMode, addressViewModel, addresses));
        }
    }

    internal enum SaveMode
    {
        Create,
        Update
    }
}

