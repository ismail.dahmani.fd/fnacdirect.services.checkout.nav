using FnacDirect.OrderPipe.Base.Model;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Orderpipe.Common.Shipping.Network.Postal.Appointment.Fly
{
    public interface IFlyViewModelBuilder
    {
        FlyViewModel Build(PopOrderForm popOrderForm);
    }
}
