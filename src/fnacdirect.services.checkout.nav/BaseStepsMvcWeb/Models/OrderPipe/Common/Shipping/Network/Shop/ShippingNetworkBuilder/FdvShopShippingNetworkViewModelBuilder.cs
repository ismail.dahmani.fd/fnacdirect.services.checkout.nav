using FnacDirect.Technical.Framework.Web.Mvc;
using FnacDirect.OrderPipe.Base.Business.Shipping;
using FnacDirect.OrderPipe.Base.Business.Availability;
using FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Common.Shipping.Shop;
using FnacDirect.OrderPipe.BaseMvc.Web.Services.ShipToStore;
using FnacDirect.OrderPipe.Base.Business.Shipping.Shop;
using FnacDirect.OrderPipe.Base.Proxy.Customer;
using FnacDirect.Solex.Shipping.Client;
using FnacDirect.OrderPipe.Base.Business.Operations;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Common.Shipping.Shop
{
    public class FdvShopShippingNetworkViewModelBuilder : ShopShippingNetworkViewModelBuilder
    {
        public FdvShopShippingNetworkViewModelBuilder(IStoreSearchService storeSearchService,
                                                   IApplicationContext applicationContext,
                                                   ICustomerFavoriteStoreService customerFavoriteStoreService,
                                                   IShipToStoreAvailabilityService shipToStoreAvailabilityService,
                                                   IPopAvailabilityProcessor popAvailabilityProcessor,
                                                   IStoreAddressBookService storeAddressBookService,
                                                   ISwitchProvider switchProvider,
                                                   IShippingChoiceCostService shippingChoiceCostService)
            : base(storeSearchService,
                  applicationContext,
                  customerFavoriteStoreService,
                  shipToStoreAvailabilityService,
                  popAvailabilityProcessor,
                  storeAddressBookService,
                  switchProvider,
                  shippingChoiceCostService)                
        {

        }

        protected override PriceFormat PriceFormat { get { return PriceFormat.Default; } }
    }
}
