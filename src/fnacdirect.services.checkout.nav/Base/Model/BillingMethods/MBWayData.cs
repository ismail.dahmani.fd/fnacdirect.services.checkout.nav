using System;

namespace FnacDirect.OrderPipe.Base.Model.BillingMethods
{
    public class MBWayData : GroupData
    {
        public string MBWayUrl { get; set; }
        public int AccountId { get; set; }
        public string OrderId { get; set; }
        public string ShopperResultUrl { get; set; }
        public string JsonReponse { get; set; }
        public string MobilePhone { get; set; }
        public string Authorization { get; set; }
        
        public string Id { get; set; }
        public string PaymentType { get; set; }
        public string PaymentBrand { get; set; }
        public decimal Amount { get; set; }
        public string Currency { get; set; }
        public string Descriptor { get; set; }
        public MBWayResult Result { get; set; }
        public MBWayResultDetails ResultDetails { get; set; }
        public string BuildNumber { get; set; }
        public DateTime Timestamp { get; set; }
        public string Ndc { get; set; }

        public class MBWayResult
        {
            public string Code { get; set; }
            public string Description { get; set; }
        }

        public class MBWayResultDetails
        {
            public DateTime PreAuthorizationValidity { get; set; }
            public string ConnectorTxID1 { get; set; }
            public string ConnectorTxID2 { get; set; }
            public string ConnectorTxID3 { get; set; }
            public string AcquirerResponse { get; set; }
        }
    }
}
