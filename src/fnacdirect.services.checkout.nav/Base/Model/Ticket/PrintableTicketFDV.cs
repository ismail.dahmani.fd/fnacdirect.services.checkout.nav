﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FnacDirect.OrderPipe.FDV.Model
{
    [Serializable]
    public class PrintableTicketFDV
    {
        public char[] Ticket { get; set; }
        public byte[] TicketBuffer { get; set; }
        public string TicketXml { get; set; }

        public string TicketModel { get; set; }
    }
}
