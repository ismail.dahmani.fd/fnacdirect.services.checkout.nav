using System;
using System.Collections.Generic;
using System.Linq;
using FnacDirect.Context;
using FnacDirect.Contracts.Online.Model;
using FnacDirect.OrderPipe.Base.BaseModel;
using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.OrderPipe.Base.Model.Pop;
using FnacDirect.Technical.Framework.Utils;
using FnacDirect.Technical.Framework.Web.Mvc;

namespace FnacDirect.OrderPipe.Base.Business.OnePage
{
    public class OnePageService : IOnePageService
    {
        private readonly RangeSet<int> _excludedTypes;
        private readonly IAddressValidationBusiness _addressValidationBusiness;
        private readonly IUserContextProvider _userContextProvider;
        public OnePageService(IApplicationContext applicationContext,
                              IAddressValidationBusiness addressValidationBusiness,
                              IUserContextProvider userContextProvider)
        {
            _excludedTypes = RangeSet.Parse<int>(applicationContext.GetAppSetting("orderpipe.pop.onepage.excludedtypes"));
            _addressValidationBusiness = addressValidationBusiness;
            _userContextProvider = userContextProvider;
        }

        public bool IsEligible(PopOrderForm popOrderForm, IEnumerable<int> excludedLogisticTypes, bool reactMode)
        {
            if (!popOrderForm.EligiblePath.HasFlag(PipePath.OnePage))
            {
                return false;
            }
            var articles = popOrderForm.LineGroups.GetArticles();

            if (articles.Any(a => a.TypeId.HasValue && _excludedTypes.Contains(a.TypeId.Value)))
            {
                return false;
            }

            var hasOnlyOneSeller = HasOnlyOneSeller(popOrderForm, excludedLogisticTypes);

            if (!hasOnlyOneSeller)
            {
                return false;
            }
            var isIdentified = _userContextProvider.GetCurrentUserContext().IsIdentified;
            if (reactMode && isIdentified)
            {
                var popData = popOrderForm.GetPrivateData<PopData>();
                var mask = ~PipePath.OnePage;

                if (popOrderForm.BillingAddress == null
                    || popOrderForm.BillingAddress.IsDummy()
                    || !_addressValidationBusiness.IsAddressValid(popOrderForm.BillingAddress.Convert()))

                {
                    popData.HasBillingAddress = false;
                    popOrderForm.EligiblePath &= mask; // All previous values other than OnePage
                    return false;
                }

                var shippingMethodEvaluation = popOrderForm.ShippingMethodEvaluation;

                if (IsDemat(articles, shippingMethodEvaluation))
                {
                    return true;
                }

                foreach (var shippingMethodEvaluationGroup in shippingMethodEvaluation.ShippingMethodEvaluationGroups)
                {
                    if (!shippingMethodEvaluationGroup.PostalShippingMethodEvaluationItem.HasValue)
                    {
                        continue;
                    }

                    var postalShippingMethodEvaluationItem = shippingMethodEvaluationGroup.PostalShippingMethodEvaluationItem.Value;

                    var isAddressSelected = !postalShippingMethodEvaluationItem.IsDummyAddress;
                    var isAddressValid = _addressValidationBusiness.IsAddressValid(postalShippingMethodEvaluationItem.ShippingAddress.Convert());
                    var isMethodPostalSelected = shippingMethodEvaluationGroup.SelectedShippingMethodEvaluationType == BaseModel.ShippingMethodEvaluationType.Postal;

                    if ((isMethodPostalSelected && !isAddressSelected)
                        || (isAddressSelected && !isAddressValid))
                    {
                        popData.HasBillingAddress = false;
                        popOrderForm.EligiblePath &= mask; // All previous values other than OnePage
                        return false;
                    }
                }

                popData.HasBillingAddress = true;
            }

            return true;
        }

        public AppliablePath GetPath(PopOrderForm popOrderForm, PopData popData, int siteId)
        {
            AppliablePath path;
            //FnacPro only on long courses
            if (siteId == (int)FnacSites.FNACPRO)
            {
                path = AppliablePath.Full;

            }
            else
            {
                var isEligible = IsEligible(popOrderForm,
                                            popOrderForm.ShippingMethodEvaluation.ExcludedLogisticTypes,
                                            popData != null && popData.UseReact);

                if (isEligible)
                {
                    path = AppliablePath.OnePage;
                }
                else
                {
                    path = AppliablePath.Full;
                }
            }

            return path;
        }

        protected internal bool HasOnlyOneSeller(PopOrderForm popOrderForm, IEnumerable<int> excludedLogisticTypes)
        {
            var lineGroups = popOrderForm.LineGroups.Where(l => !l.Articles.All(a => a.LogType.HasValue && excludedLogisticTypes.Contains(a.LogType.Value))).ToList();

            return !lineGroups.Any() || lineGroups.Select(lg => lg.Seller.SellerId).Distinct().Count() == 1;
        }

        private bool IsDemat(IEnumerable<Model.Article> articles, ShippingMethodEvaluation shippingMethodEvaluation)
        {
            if (shippingMethodEvaluation.ShippingMethodEvaluationGroups.All(x => x.ShippingMethodEvaluationItems.All(y =>
                y.SelectedShippingMethodId == Constants.ShippingMethods.ServiceEnLigneNonExpedie ||
                y.SelectedShippingMethodId == Constants.ShippingMethods.Download)))
            {
                return true;
            }

            if (articles is IEnumerable<StandardArticle> standardArticles && standardArticles.All(x => x.IsNumerical && (x.IsEbook || x.IsDematSoft)))
            {
                return true;
            }

            return false;
        }
    }
}
