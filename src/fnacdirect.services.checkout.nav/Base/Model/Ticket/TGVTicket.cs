﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FnacDirect.OrderPipe.FDV.Model
{
    [Serializable]
    public class TGVTicket : Ticket
    {
        private string _DateTicket;
        private string _HeureTicket;
        private string _IdTicket;
        private string _NumCaisse;
        private string _NumCaissier;

        private DateTime? _DateLot;

        public TGVTicket() { }

        public TGVTicket(string dateTicket, string heureTicket, string idTicket, string numCaisse, string numCaissier, DateTime? dateLot)
        {
            try
            {
                if (dateTicket.Length != 8 || !int.TryParse(dateTicket, out int res))
                    throw new ArgumentException("dateTicket must contain 8 characters and must be integer");

                if (heureTicket.Length != 6 || !int.TryParse(heureTicket, out res))
                    throw new ArgumentException("heureTicket must contain 6 characters and must be integer");

                if (idTicket.Length != 6 || !int.TryParse(idTicket, out res))
                    throw new ArgumentException("idTicket must contain 6 characters and must be integer");

                if (numCaisse.Length != 3 || !int.TryParse(numCaisse, out res))
                    throw new ArgumentException("numCaisse must contain 3 characters and must be integer");

                if (numCaissier.Length != 6 || !int.TryParse(numCaissier, out res))
                    throw new ArgumentException("numCaissier must contain 6 characters and must be integer");


                DateTicket = dateTicket;
                HeureTicket = heureTicket;
                IdTicket = idTicket;
                NumCaisse = numCaisse;
                NumCaissier = numCaissier;
                DateLot = dateLot;

                TicketKey = string.Concat(DateTicket, HeureTicket, IdTicket, NumCaisse, NumCaissier);
            }
            catch (Exception ex)
            {
                if (!ex.Data.Contains("DateTicket"))
                    ex.Data.Add("DateTicket", dateTicket);
                if (!ex.Data.Contains("HeureTicket"))
                    ex.Data.Add("HeureTicket", heureTicket);
                if (!ex.Data.Contains("IdTicket"))
                    ex.Data.Add("IdTicket", idTicket);
                if (!ex.Data.Contains("NumCaisse"))
                    ex.Data.Add("NumCaisse", numCaisse);
                if (!ex.Data.Contains("NumCaissier"))
                    ex.Data.Add("NumCaissier", numCaissier);

                throw;
            }
        }

        public string DateTicket
        {
            get { return _DateTicket; }
            set { _DateTicket = value; }
        }

        public string HeureTicket
        {
            get { return _HeureTicket; }
            set { _HeureTicket = value; }
        }

        public string IdTicket
        {
            get { return _IdTicket; }
            set { _IdTicket = value; }
        }

        public string NumCaisse
        {
            get { return _NumCaisse; }
            set { _NumCaisse = value; }
        }

        public string NumCaissier
        {
            get { return _NumCaissier; }
            set { _NumCaissier = value; }
        }

        public DateTime? DateLot
        {
            get { return _DateLot; }
            set { _DateLot = value; }
        }
    }
}
