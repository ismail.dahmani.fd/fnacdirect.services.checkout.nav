using System;
using System.Collections.Generic;
using System.Text;
using FnacDirect.OrderPipe.Base.Model;

namespace FnacDirect.Quote.Model
{

    public enum DiscountRaison
    {
        OPC = 0,
        QuotePrice = 1,
        QuotePort = 2,
        QuoteGlobalPort = 3,
        QuoteGlobalPrice = 4,
        OPCGlobalPrice = 5,
        OPCGlobalPort = 6,
        OPCGlobalSplit = 7,
        OPCGlobalWrap = 8
    }

    [Serializable]
    public class QuoteLineRemiseCollection : List<QuoteLineRemise>
    {
        
    }


    [Serializable]
    public class QuoteLineRemise : Remise
    {

        private int quoteAddressId;
        private string quoteDetailArticleReference;
        private int quoteDetailQuantity;
        private decimal quoteDetailUnitPriceExclTax;
        private decimal quoteDetailTotalPriceExclTax;
        private decimal quoteDetailOpcDiscountAmount;
        private decimal quoteDetailPmpArticle;
        private decimal quoteDetailOpcDiscountPercentage;

        /// <summary>
        /// Cl� permettant de diff�rencier les remises appliqu�es sur diff�rentes adresses
        /// </summary>
        public int QuoteAddressId
        {
            get { return quoteAddressId; }
            set { quoteAddressId = value; }
        }

        public string QuoteDetailArticleReference
        {
            get { return quoteDetailArticleReference; }
            set { quoteDetailArticleReference = value; }
        }

        public int QuoteDetailQuantity
        {
            get { return quoteDetailQuantity; }
            set { quoteDetailQuantity = value; }
        }

        public decimal QuoteDetailUnitPriceExclTax
        {
            get { return quoteDetailUnitPriceExclTax; }
            set { quoteDetailUnitPriceExclTax = value; }
        }

        public decimal QuoteDetailTotalPriceExclTax
        {
            get { return quoteDetailTotalPriceExclTax; }
            set { quoteDetailTotalPriceExclTax = value; }
        }

        public decimal QuoteDetailOpcDiscountAmount
        {
            get { return quoteDetailOpcDiscountAmount; }
            set { quoteDetailOpcDiscountAmount = value; }
        }

        public decimal QuoteDetailPmpArticle
        {
            get { return quoteDetailPmpArticle; }
            set { quoteDetailPmpArticle = value; }
        }

        public decimal QuoteDetailOpcDiscountPercentage
        {
            get { return quoteDetailOpcDiscountPercentage; }
            set { quoteDetailOpcDiscountPercentage = value; }
        }

        public QuoteLineRemise(){}
    }
}
