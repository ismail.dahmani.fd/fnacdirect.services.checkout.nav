using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.OrderPipe.Base.Model.SelfCheckout;
using FnacDirect.OrderPipe.Base.Model.ThankYou;
using FnacDirect.Technical.Framework.Web;
using FnacDirect.Technical.Framework.Web.Http.Services;
using FnacDirect.Technical.Framework.Web.Mvc;
using System.Collections.Generic;
using System.Linq;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Pop.Models.SelfCheckout
{
    public class SelfCheckoutOrderThankYouModelBuilder : ISelfCheckoutOrderThankYouModelBuilder
    {
        private readonly IUrlManager _urlManager;
        private readonly IPriceHelper _priceHelper;
        private readonly ILogger _logger;

        public SelfCheckoutOrderThankYouModelBuilder(IUrlManager urlManager, ILogger logger, IApplicationContext applicationContext)
        {
            _urlManager = urlManager;
            _logger = logger;
            _priceHelper = applicationContext.GetPriceHelper();
        }

        public SelfCheckoutOrderThankYouViewModel Build(PopOrderForm popOrderForm)
        {
            //Internal Code
            var internalCode = string.Empty;
            var selfcheckoutdata = popOrderForm.GetPrivateData<SelfCheckoutData>(create: false);
            if (selfcheckoutdata == null || string.IsNullOrWhiteSpace(selfcheckoutdata.InternalCode))
                _logger.WriteError("SelfCheckout OrderThankYouViewModelBuilder: InternalCode not found");
            else
                internalCode = selfcheckoutdata.InternalCode;

            //BarCode
            var barCodeUrlManagerPage = _urlManager.Sites.MobileApplication.GetPage("Orderpipe.SelfCheckout.OrderThankYou.BarCode");

            //ViewModel Initialization
            var orderThankYouViewModel = new SelfCheckoutOrderThankYouViewModel
            {
                OrderExternalReference = string.Empty,
                InternalCode = selfcheckoutdata != null ? selfcheckoutdata.InternalCode : string.Empty,
                ShopFullName = string.Empty,
                BarCodeUrl = barCodeUrlManagerPage != null ? barCodeUrlManagerPage.DefaultUrl.ToString() : string.Empty
            };

            //thankYouOrderDetailData
            var thankYouOrderDetailData = popOrderForm.GetPrivateData<ThankYouOrderDetailData>(create: false);
            ThankYouOrderDetail thankYouOrderDetail;
            try
            {
                thankYouOrderDetail = thankYouOrderDetailData?.ThankYouOrdersDetails.SingleOrDefault(od => od.IsFnac);
            }
            catch (System.InvalidOperationException)
            {
                _logger.WriteError("SelfCheckout OrderThankYouViewModelBuilder: More than one thankYouOrderDetail found");
                thankYouOrderDetail = thankYouOrderDetailData?.ThankYouOrdersDetails
                    .Where(od => od.IsFnac).OrderByDescending(od => od.OrderDate).First();
            }

            if (thankYouOrderDetail == null)
            {
                _logger.WriteError("SelfCheckout OrderThankYouViewModelBuilder: thankYouOrderDetail not found");
            }
            else
            {
                orderThankYouViewModel.OrderDateAndTime = thankYouOrderDetail.OrderDate;
                orderThankYouViewModel.OrderExternalReference = thankYouOrderDetail.OrderUserReference;
                orderThankYouViewModel.ShopFullName = thankYouOrderDetail.ShopFullname;

                var totalPrice = 0m;
                foreach (var article in thankYouOrderDetail.Articles)
                {
                    orderThankYouViewModel.ArticleDetails.Add(new SelfCheckoutOrderThankYouArticleDetails
                    {
                        Name = article.Title,
                        Quantity = article.Quantity,
                        UnitPrice = _priceHelper.Format(article.Price.GetValueOrDefault(), PriceFormat.CurrencyAsDecimalSeparator),
                        Price = _priceHelper.Format(article.Price.GetValueOrDefault() * article.Quantity, PriceFormat.CurrencyAsDecimalSeparator),
                        PictureUrl = article.UrlPicture,
                        Ean = article.Ean
                    });
                    totalPrice += article.Price.GetValueOrDefault() * article.Quantity;
                }
                orderThankYouViewModel.Total = _priceHelper.Format(totalPrice, PriceFormat.CurrencyAsDecimalSeparator);
            }

            return orderThankYouViewModel;
        }
    }
}
