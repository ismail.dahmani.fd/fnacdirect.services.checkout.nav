﻿using System.Collections.Generic;
using FnacDirect.Customer.Model;
using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.OrderPipe.Base.Model.LineGroups;

namespace FnacDirect.OrderPipe.Base.Business.Billing
{
    public interface IBillingAddressElegibilityService
    {
        IEnumerable<AddressEntity> GetElegibleAddress(IEnumerable<ILineGroup> lineGroups, List<AddressEntity> addresses);
    }
}