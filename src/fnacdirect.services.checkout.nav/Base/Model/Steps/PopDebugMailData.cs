﻿using FnacDirect.OrderPipe.Base.Business.Mailing;

namespace FnacDirect.OrderPipe.Base.Steps.Mailing
{
    // PopDebugMailData is used to serialize the pop mail model only when the switch orderpipe.pop.mail.debug is active
    public class PopDebugMailData : StepData
    {
        public string Model { get; set; }
    }
}
