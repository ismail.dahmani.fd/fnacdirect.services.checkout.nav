﻿
using FnacDirect.Technical.Framework.Utils;

namespace FnacDirect.OrderPipe.Base.Business
{
    public interface IGuptFnacComBusinessProvider
    {
        IGuptFnacComBusiness GetGuptFnacComBusiness(RangeSet<int> tvArticleTypesId);
    }
}
