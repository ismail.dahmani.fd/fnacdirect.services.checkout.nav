﻿using FnacDirect.OrderPipe.Core.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FnacDirect.OrderPipe.Base.Business.Shipping
{
    public class ShippingStateService : IShippingStateService
    {
        private readonly IPipeParametersProvider _pipeParametersProvider;

        public ShippingStateService(IPipeParametersProvider pipeParametersProvider)
        {
            _pipeParametersProvider = pipeParametersProvider;
        }

        public string GetStateByZipCode(string zipCode)
        {
            if (int.TryParse(zipCode, out var intZipCode))
            {
                var islandsZipCode = _pipeParametersProvider.GetAsDictionary<string>("islands.zipcode");

                foreach (var iZipCode in islandsZipCode)
                {
                    var ranges = iZipCode.Value.Split('-').Select(int.Parse).ToList();
                    if (ranges.Count() > 1 && (ranges[0] <= intZipCode && intZipCode <= ranges[1]))
                    {
                        return iZipCode.Key.ToUpperInvariant();
                    }
                    else if (ranges[0] == intZipCode)
                        return iZipCode.Key.ToUpperInvariant();
                }
            }

            return string.Empty;
        }
    }
}
