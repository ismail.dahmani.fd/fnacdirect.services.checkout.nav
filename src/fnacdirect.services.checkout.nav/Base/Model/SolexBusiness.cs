using FnacDirect.OrderPipe.Base.BaseModel;
using FnacDirect.OrderPipe.Base.Business.Configuration;
using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.OrderPipe.Base.Model.Facets;
using FnacDirect.OrderPipe.Base.Model.FDV;
using FnacDirect.OrderPipe.Base.Model.LineGroups;
using FnacDirect.OrderPipe.Base.Proxy;
using FnacDirect.OrderPipe.Base.Proxy.Store;
using FnacDirect.OrderPipe.Core.Services;
using FnacDirect.Solex.Shipping.Client;
using FnacDirect.Solex.Shipping.Client.Contract;
using FnacDirect.Technical.Framework.Web.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using SolexModel = FnacDirect.OrderPipe.Base.Model.Solex;

namespace FnacDirect.OrderPipe.Base.Business.Shipping
{
    public class SolexBusiness : ISolexBusiness
    {
        private readonly ISolexConfigurationProvider _solexConfigurationProvider;
        private readonly IShippingServiceClient _shippingServiceClient;
        private readonly IStoreStockServiceClient _storeStockServiceClient;
        private readonly ICountryService _countryService;
        private readonly ICustomerContextProvider _customerContextProvider;
        private readonly IApplicationContext _applicationContext;
        private readonly IFnacStoreService _fnacStoreService;
        private readonly ISwitchProvider _switchProvider;
        private readonly IMarketPlaceServiceProvider _marketPlaceServiceProvider;

        private readonly List<int> _freeCountries;
        private readonly List<int> _freeCountriesException;
        private readonly string _freeOpeCode;

        private bool _excludeCCForFreeArticleFilter = false;
        private bool _excludeCCForFreeCCEligibleArticleFilter = false;
        private bool _exclude2HCForFreeCCEligibleArticleFilter = false;

        private readonly int _fnacLocalMpSellerId;


        public SolexBusiness(ISolexConfigurationProvider solexConfigurationProvider,
                             IShippingServiceClient shippingServiceClient,
                             IStoreStockServiceClient storeStockServiceClient,
                             ICountryService countryService,
                             ICustomerContextProvider customerContextProvider,
                             IPipeParametersProvider pipeParametersProvider,
                             IFnacStoreService fnacStoreService,
                             IApplicationContext applicationContext,
                             ISwitchProvider switchProvider,
                             IMarketPlaceServiceProvider marketPlaceServiceProvider)
        {
            _solexConfigurationProvider = solexConfigurationProvider;
            _shippingServiceClient = shippingServiceClient;
            _storeStockServiceClient = storeStockServiceClient;
            _countryService = countryService;
            _customerContextProvider = customerContextProvider;
            _applicationContext = applicationContext;
            _fnacStoreService = fnacStoreService;
            _switchProvider = switchProvider;
            _marketPlaceServiceProvider = marketPlaceServiceProvider;

            _freeCountries = StringUtils.ConvertParameterToList<int>(pipeParametersProvider.Get("shipping.free.countries", "")) ?? new List<int>();
            _freeCountriesException = StringUtils.ConvertParameterToList<int>(pipeParametersProvider.Get("shipping.free.ope.countries", "")) ?? new List<int>();
            _freeOpeCode = pipeParametersProvider.Get("shipping.free.ope", "EDITOFRAISPORT1207-EURO");

            _fnacLocalMpSellerId = pipeParametersProvider.Get("localoffer.sellerid", 4);
        }

        public SolexModel.ItemInGroup FindItemInChoice(SolexModel.ShippingChoice choice, Article article)
        {
            foreach (var group in choice.Groups)
            {
                foreach (var item in group.Items)
                {
                    if (article is StandardArticle standardArticle)
                    {
                        if (item.Identifier.Prid == standardArticle.ProductID)
                        {
                            return item;
                        }
                    }
                    else if (article is MarketPlaceArticle marketPlaceArticle && item.Identifier.OfferReference == marketPlaceArticle.Offer.Reference)
                    {
                        return item;
                    }
                }
            }

            return null;
        }

        public IEnumerable<SolexModel.Identifier> GetIdentifiers(ShippingMethodEvaluationItem item, int shippingMethodId)
        {
            var result = new List<SolexModel.Identifier>();

            if (item.HasChoices)
            {
                result = GetIdentifiers(item.Choices, shippingMethodId).ToList();
            }
            return result;
        }

        public IEnumerable<SolexModel.Identifier> GetIdentifiers(IEnumerable<SolexModel.ShippingChoice> choices, int shippingMethodId)
        {
            var result = new List<SolexModel.Identifier>();

            SolexModel.ShippingChoice currentChoice = null;

            if (!choices.Any())
            {
                return result;
            }
            else if (choices.Count() == 1)
            {
                currentChoice = choices.First().GetChoice();
            }
            else
            {
                currentChoice = choices.FirstOrDefault(choice => choice.ContainsShippingMethodId(shippingMethodId))?.GetChoice(shippingMethodId);

                if (currentChoice == null)
                {
                    return result;
                }
            }

            int? parcelId = null;
            foreach (var parcel in currentChoice.Parcels)
            {
                if (parcel.Value.ShippingMethodId == shippingMethodId)
                {
                    parcelId = parcel.Key;
                    break;
                }
            }

            if (parcelId.HasValue)
            {
                result = currentChoice.Groups.SelectMany(x => x.Items).Where(i => i.ParcelId == parcelId.Value).Select(i => i.Identifier).ToList();
            }

            return result;
        }

        public EvaluateShoppingCartResult EvaluateShoppingCart(IEnumerable<Article> articles, IEnumerable<FnacDirect.Contracts.Online.Model.FreeShipping> freeShippings, ShippingNetworks shippingNetworks, string usageKey)
        {
            var query = CreateEvaluateShoppingCartQuery(articles, freeShippings, shippingNetworks);
            query.Context.UsageKey = usageKey;
            return EvaluateShoppingCart(query, articles);
        }

        public decimal SimultateTotalsShippingCosts(IEnumerable<ILineGroup> lineGroups, IGotShippingMethodEvaluation iGotShippingMethodEvaluation, List<Solex.Shipping.Client.Contract.Promotion> shippingPromotionsTriggers, ShippingAddress address, bool excludeAdherentPromotion = true)
        {
            var total = 0m;
            var freeShippings = iGotShippingMethodEvaluation.ShippingMethodEvaluation.FreeShipping;

            foreach (var lineGroup in lineGroups)
            {
                var type = 0;

                var shippingNetworks = new ShippingNetworks();

                if (address is Model.PostalAddress)
                {
                    type = Constants.ShippingNetworks.Postal;
                    shippingNetworks.Postal = new ShippingNetwork<Solex.Shipping.Client.Contract.PostalAddress>();

                    var postalAddress = new Solex.Shipping.Client.Contract.PostalAddress();

                    if (address is Model.PostalAddress postalShippingAddress && postalShippingAddress.Identity != -1)
                    {
                        postalAddress.Country = _countryService.GetCountries().FirstOrDefault(c => c.ID == postalShippingAddress.CountryId).Code3;
                        postalAddress.ZipCode = postalShippingAddress.ZipCode;
                        postalAddress.City = postalShippingAddress.City;
                        postalAddress.AddressLine = postalShippingAddress.AddressLine1;
                    }
                    else
                    {
                        postalAddress.Default = true;
                    }

                    shippingNetworks.Postal.Addresses.Add(postalAddress);
                }
                else if (address is Model.ShopAddress shopShippingAddress)
                {
                    type = Constants.ShippingNetworks.Store;

                    var shopAddress = new Solex.Shipping.Client.Contract.ShopAddress();

                    if (!shopShippingAddress.IsDummy())
                    {
                        shopAddress.RefUG = _fnacStoreService.GetStore(shopShippingAddress.Identity.GetValueOrDefault()).RefUG.ToString();
                    }
                    else
                    {
                        shopAddress.Default = true;
                    }

                    var shopInfoData = iGotShippingMethodEvaluation.GetPrivateData<ShopInfosData>(create: false);
                    if (_switchProvider.IsEnabled("orderpipe.pop.fdv.enable", false) &&
                        shopInfoData?.Shop?.ShopRefUG != null &&
                        shopInfoData.Shop.ShopRefUG.Equals(shopAddress.RefUG, StringComparison.InvariantCultureIgnoreCase))
                    {
                        shopAddress.IsCurrentShop = true;
                    }

                    shippingNetworks.Shop.Addresses.Add(shopAddress);
                }
                else if (address is Model.RelayAddress)
                {
                    type = Constants.ShippingNetworks.Relay;

                    var relayAddress = new Solex.Shipping.Client.Contract.RelayAddress();

                    if (address is Model.RelayAddress relayShippingAddress && !relayShippingAddress.IsDummy())
                    {
                        relayAddress.Id = relayShippingAddress.Identity.GetValueOrDefault();
                    }
                    else
                    {
                        relayAddress.Default = true;
                    }

                    shippingNetworks.Relay.Addresses.Add(relayAddress);
                }

                if (type == 0)
                {
                    return total;
                }

                var evaluateShoppingCartQuery = CreateEvaluateShoppingCartQuery(lineGroup.Articles, freeShippings, shippingNetworks);
                var promotions = GetPromotions(lineGroup.Articles).ToList();

                if (shippingPromotionsTriggers != null && shippingPromotionsTriggers.Count > 0)
                {
                    if (!excludeAdherentPromotion)
                    {
                        promotions.AddRange(shippingPromotionsTriggers);
                    }
                    else
                    {
                        foreach (var promo in shippingPromotionsTriggers)
                        {
                            promotions.RemoveAll(p => p.TriggerKey == promo.TriggerKey);
                        }
                    }
                }

                evaluateShoppingCartQuery.Promotions = promotions;
                evaluateShoppingCartQuery.RuleTriggers = promotions.Select(p => p.TriggerKey)?.Distinct();
                
                var result = _shippingServiceClient.EvaluateShoppingCart(evaluateShoppingCartQuery);

                if (!iGotShippingMethodEvaluation.ShippingMethodEvaluation.FnacCom.HasValue
                    || !iGotShippingMethodEvaluation.ShippingMethodEvaluation.FnacCom.Value.SelectedShippingMethod.HasValue)
                {
                    return total;
                }

                var selectedShippingMethodId = iGotShippingMethodEvaluation.ShippingMethodEvaluation.FnacCom.Value.SelectedShippingMethod.Value;

                total += CalculateTotal(result, type, selectedShippingMethodId);
            }

            return total;
        }

        public StoreStockInformations GetStoreStockInformations(string refUg, Article article)
        {
            var solexConfiguration = _solexConfigurationProvider.Get();

            var context = new Solex.Shipping.Client.Contract.Context
            {
                Channel = solexConfiguration.Channel,
                Market = solexConfiguration.Market
            };

            return _storeStockServiceClient.Get(context, CreateItemFromArticle(article), refUg);
        }

        #region private methods

        private EvaluateShoppingCartQuery CreateEvaluateShoppingCartQuery(IEnumerable<Article> articles, IEnumerable<FnacDirect.Contracts.Online.Model.FreeShipping> freeShippings, ShippingNetworks shippingNetworks)
        {
            var solexConfiguration = _solexConfigurationProvider.Get();

            var saleableArticles = articles;
            // Sur FnacPro, on calcule avec les articles gratuits, la création du devis en a besoin
            if (_applicationContext.GetSiteId() != (int)FnacDirect.Contracts.Online.Model.FnacSites.FNACPRO)
            {
                saleableArticles = articles.Where(art => art.Type != ArticleType.Free);
            }

            var items = CreateItemsListFromArticlesList(saleableArticles, freeShippings);

            var evaluateShoppingCartQuery = new EvaluateShoppingCartQuery
            {
                Context = new Solex.Shipping.Client.Contract.Context
                {
                    Channel = solexConfiguration.Channel,
                    Market = solexConfiguration.Market
                },
                Items = items,
                ShippingNetworks = shippingNetworks
            };

            foreach (var postalAddress in shippingNetworks.Postal.Addresses)
            {
                var countryId = _applicationContext.GetCountryId();

                if (!postalAddress.Default)
                {
                    var country = _countryService.GetCountries()
                                               .FirstOrDefault(c => string.Equals(c.Code3, postalAddress.Country, StringComparison.InvariantCultureIgnoreCase));
                    if (country != null)
                    {
                        countryId = country.ID;
                    }
                }

                postalAddress.Options.ApplyDefaultFreeShipping = ProcessAddressFreeShipping(freeShippings, countryId);
            }

            evaluateShoppingCartQuery.Promotions = GetPromotions(articles);
            evaluateShoppingCartQuery.RuleTriggers = evaluateShoppingCartQuery.Promotions.Select(p => p.TriggerKey)?.Distinct();
            return evaluateShoppingCartQuery;
        }

        private EvaluateShoppingCartResult EvaluateShoppingCart(EvaluateShoppingCartQuery evaluateShoppingCartQuery, IEnumerable<Article> articles)
        {
            var evaluateShoppingCartResult = _shippingServiceClient.EvaluateShoppingCart(evaluateShoppingCartQuery);

            var customerContext = _customerContextProvider.GetCurrentCustomerContext();
            var customerEmail = customerContext.GetContractDTO()?.Info?.Email ?? string.Empty;

            var shippingMethodsAccountFilters = _solexConfigurationProvider.Get().ShippingMethodsAccountFiltersAsRegex;
            _excludeCCForFreeArticleFilter = _switchProvider.IsEnabled("orderpipe.pop.shipping.clickandcollect.excludeforfreearticle");
            _excludeCCForFreeCCEligibleArticleFilter = _switchProvider.IsEnabled("orderpipe.pop.shipping.clickandcollect.excludeforfreecceligiblearticle");
            _exclude2HCForFreeCCEligibleArticleFilter = _switchProvider.IsEnabled("orderpipe.pop.shipping.livraisondeuxheureschrono.excludeforfreecceligiblearticle");
            //Return if no filters exist
            if (!shippingMethodsAccountFilters.Any()
                && !_excludeCCForFreeArticleFilter
                && !_excludeCCForFreeCCEligibleArticleFilter
                && !_exclude2HCForFreeCCEligibleArticleFilter)
            {
                return evaluateShoppingCartResult;
            }

            IEnumerable<Choice>[] filterChoices(IEnumerable<Choice>[] input)
            {
                var output = new List<IEnumerable<Choice>>();

                foreach (var choiceList in input)
                {
                    var filteredChoices = new List<Choice>();

                    foreach (var choice in choiceList)
                    {
                        if (shippingMethodsAccountFilters.ContainsKey(choice.MainShippingMethodId))
                        {
                            if (shippingMethodsAccountFilters[choice.MainShippingMethodId].IsMatch(customerEmail))
                            {
                                filteredChoices.Add(choice);
                            }
                        }
                        else
                        {
                            filteredChoices.Add(choice);
                        }
                    }

                    output.Add(filteredChoices);
                }

                return output.ToArray();
            }

            if (evaluateShoppingCartResult.ShippingNetworks.Postal != null)
            {
                foreach (var address in evaluateShoppingCartResult.ShippingNetworks.Postal.Addresses)
                {
                    address.Choices = SetPostalShippingMethodItemChoices(filterChoices(address.Choices), articles);
                }
            }

            if (evaluateShoppingCartResult.ShippingNetworks.Shop != null)
            {
                foreach (var address in evaluateShoppingCartResult.ShippingNetworks.Shop.Addresses)
                {
                    address.Choices = SetShopShippingMethodItemChoices(filterChoices(address.Choices), articles);

                }
            }

            if (evaluateShoppingCartResult.ShippingNetworks.Relay != null)
            {
                foreach (var address in evaluateShoppingCartResult.ShippingNetworks.Relay.Addresses)
                {
                    address.Choices = filterChoices(address.Choices);
                }
            }

            return evaluateShoppingCartResult;
        }

        private Item CreateItemFromArticle(Article article)
        {
            var item = new Item();

            if (!article.LogType.HasValue)
            {
                return null;
            }

            item.ArticleTypeId = article.TypeId.GetValueOrDefault();
            item.LogisticTypeId = article.LogType.GetValueOrDefault();

            if (article is MarketPlaceArticle marketPlaceArticle)
            {
                item.IsPE = marketPlaceArticle.IsPE;

                item.Identifier = new Identifier
                {
                    Prid = marketPlaceArticle.ProductID.GetValueOrDefault(),
                    OfferReference = marketPlaceArticle.Offer.Reference
                };
                item.Expedition = new Expedition
                {
                    AvailabilityId = marketPlaceArticle.AvailabilityId.GetValueOrDefault(),
                    Stock = marketPlaceArticle.AvailableQuantity
                };

                item.Offer = new MarketPlaceOffer()
                {
                    LogisticTypeId = marketPlaceArticle.LogType.GetValueOrDefault(),
                    Prid = marketPlaceArticle.ProductID.GetValueOrDefault(),
                    SellerId = marketPlaceArticle.Offer.Seller.SellerId,
                    OfferReference = marketPlaceArticle.Offer.Reference,
                    HandDelivery = marketPlaceArticle.Offer.HandDelivery.GetValueOrDefault(),
                };
                
                var isDLEMPEnable = _switchProvider.IsEnabled("orderpipe.pop.shipping.dlemp.enable");
                if (isDLEMPEnable)
                {
                    item.Offer.PreparationTimeInDays = marketPlaceArticle.Offer.PreparationTimeInDays ?? 0;
                }
            }
            else
            {
                var standardArticle = (StandardArticle)article;

                item.LogistiqueAvailabilityDate = standardArticle.ArticleDTO?.ExtendedProperties?.RestockingDate ?? standardArticle.ArticleDTO?.RestockingDate;
                item.Sku = standardArticle.ReferenceGU;
                item.SupportId = standardArticle.SupportId;
                item.IsPE = standardArticle.IsPE;
                item.AnnouncementDate = standardArticle.AnnouncementDate;
                item.PurchaseId = standardArticle.PurchaseId;
                item.Identifier = new Identifier
                {
                    Prid = standardArticle.ProductID
                };
                item.Expedition = new Expedition
                {
                    AvailabilityId = standardArticle.AvailabilityId.GetValueOrDefault(),
                    Stock = standardArticle.AvailableStock,
                    IsLocalOffer = standardArticle.SupplySource?.OfferReference != null
                };

                if (item.Expedition.IsLocalOffer)
                {
                    var localMPlaceArticle = new MarketPlaceArticle
                    {
                        OfferId = standardArticle.SupplySource.OfferId.Value,
                        Quantity = standardArticle.Quantity
                    };

                    var siteContext = _applicationContext.GetSiteContext();
                    _marketPlaceServiceProvider.GetMarketPlaceService(siteContext)
                        .GetArticleDetail(localMPlaceArticle);
                    
                    item.Identifier.OfferReference = standardArticle.SupplySource.OfferReference.Value;

                    item.Offer = new MarketPlaceOffer
                    {
                        SellerId = localMPlaceArticle.SellerID ?? _fnacLocalMpSellerId,
                        Prid = standardArticle.ProductID ?? 0,
                        PreparationTimeInDays =  standardArticle.SupplySource?.PreparationTimeInDays,
                        OfferReference = standardArticle.SupplySource.OfferReference.Value   
                    };
                }

                item.SplitLevel = standardArticle.SplitLevel.GetValueOrDefault();
            }

            item.Quantity = (int)article.Quantity;

            if (article is BaseArticle baseArticle && baseArticle.PriceUserEur.HasValue)
            {
                item.TotalPrice = item.Quantity * baseArticle.PriceUserEur.Value;
            }

            return item;
        }

        private List<Item> CreateItemsListFromArticlesList(IEnumerable<Article> articles, IEnumerable<FnacDirect.Contracts.Online.Model.FreeShipping> freeShippings)
        {
            var items = new List<Item>();

            foreach (var article in articles)
            {
                var item = CreateItemFromArticle(article);

                if (item != null)
                {
                    item.Options = new Options()
                    {
                        ApplyDefaultFreeShipping = ProcessItemFreeShipping(freeShippings, article.LogType.GetValueOrDefault())
                    };

                    if (article is StandardArticle standardArticle)
                    {
                        if (standardArticle.Services.Any())
                        {
                            foreach (var service in standardArticle.Services)
                            {
                                var itemservice = new Item
                                {
                                    Identifier = new Identifier
                                    {
                                        Prid = service.ProductID
                                    },
                                    Expedition = new Expedition
                                    {
                                        AvailabilityId = service.AvailabilityId.GetValueOrDefault(),
                                        Stock = service.AvailableStock,
                                        IsLocalOffer = service.SupplySource?.OfferReference != null
                                    },
                                    SplitLevel = service.SplitLevel.GetValueOrDefault(),
                                    Quantity = (int)service.Quantity,
                                    ArticleTypeId = service.TypeId.GetValueOrDefault(),
                                    LogisticTypeId = service.LogType.GetValueOrDefault(),
                                    Sku = service.ReferenceGU,
                                    SupportId = service.SupportId,
                                    IsPE = service.IsPE,
                                    AnnouncementDate = service.AnnouncementDate
                                };
                                items.Add(itemservice);
                            }
                        }
                    }

                    items.Add(item);
                }
            }
            return items;
        }

        private bool ProcessAddressFreeShipping(IEnumerable<FnacDirect.Contracts.Online.Model.FreeShipping> freeShippings, int countryId)
        {
            if (freeShippings == null || !freeShippings.Any())
            {
                return false;
            }

            if (freeShippings.All(fs => fs.Promotion.Code != _freeOpeCode ||
                                        !_freeCountriesException.Contains(countryId)) &&
                                        !_freeCountries.Contains(countryId))
            {
                return false;
            }

            return true;
        }

        private bool ProcessItemFreeShipping(IEnumerable<FnacDirect.Contracts.Online.Model.FreeShipping> freeShippings, int logType)
        {
            if (freeShippings == null || !freeShippings.Any())
            {
                return false;
            }

            var freeShippingLogisticTypes = freeShippings.Select(f => (int)f.LogisticType).ToList();

            if (!freeShippingLogisticTypes.Any(f => f == logType))
            {
                return true;
            }

            return false;
        }

        private IEnumerable<Solex.Shipping.Client.Contract.Promotion> GetPromotions(IEnumerable<Article> articles)
        {
            var promotions = new List<Solex.Shipping.Client.Contract.Promotion>();

            var baseArticles = articles.OfType<BaseArticle>().Where(art => art.ShippingPromotionRules != null);
            if (baseArticles != null && baseArticles.Any())
            {
                foreach (var baseArticle in baseArticles)
                {
                    foreach (var shippingPromotionRule in baseArticle.ShippingPromotionRules.Where(x => x != null))
                    {
                        var shippingPromotionTrigger = new Solex.Shipping.Client.Contract.Promotion
                        {
                            TriggerKey = shippingPromotionRule.TriggerKey,
                            PromotionCode = shippingPromotionRule.Promotion != null ? shippingPromotionRule.Promotion.Code : string.Empty,
                            AdditionalRuleParam1 = shippingPromotionRule.Parameter1,
                            AdditionalRuleParam2 = shippingPromotionRule.Parameter2,
                            ApplyToLogisticTypes = shippingPromotionRule.LogisticFamily ?? new List<int>(),
                            Quantity = baseArticle.Quantity.GetValueOrDefault()
                        };

                        var existingTrigger = promotions.FirstOrDefault(x => x.TriggerKey == shippingPromotionTrigger.TriggerKey);

                        if (existingTrigger == null)
                        {
                            promotions.Add(shippingPromotionTrigger);
                        }
                        else
                        {
                            foreach (var logType in shippingPromotionTrigger.ApplyToLogisticTypes)
                            {
                                if (!existingTrigger.ApplyToLogisticTypes.Contains(logType))
                                {
                                    existingTrigger.ApplyToLogisticTypes.Add(logType);
                                }
                            }

                            existingTrigger.Quantity += baseArticle.Quantity.GetValueOrDefault();
                        }
                    }
                }
            }

            return promotions;
        }

        private decimal CalculateTotal(EvaluateShoppingCartResult result, int type, int selectedShippingMethodId)
        {
            switch (type)
            {
                case Constants.ShippingNetworks.Postal:
                    return CalculateTotalForPostal(result.ShippingNetworks.Postal, selectedShippingMethodId);
                case Constants.ShippingNetworks.Store:
                    return CalculateTotalForShop(result.ShippingNetworks.Shop, selectedShippingMethodId);
                case Constants.ShippingNetworks.Relay:
                    return CalculateTotalForRelay(result.ShippingNetworks.Relay, selectedShippingMethodId);
                default:
                    return 0;
            }
        }

        private decimal CalculateTotalForRelay(ShippingNetwork<Solex.Shipping.Client.Contract.RelayAddress> relay, int selectedShippingMethodId)
        {
            if (relay.Addresses.Any()
                && relay.Addresses.FirstOrDefault().Choices.Count() > 0
                && relay.Addresses.FirstOrDefault().Choices.FirstOrDefault().Count() > 0)
            {
                var choices = relay.Addresses.FirstOrDefault().Choices.FirstOrDefault();
                foreach (var choice in choices)
                {
                    if (choice.MainShippingMethodId == selectedShippingMethodId)
                    {
                        return choice.TotalPrice;
                    }
                }
            }
            return 0;
        }

        private decimal CalculateTotalForShop(ShippingNetwork<Solex.Shipping.Client.Contract.ShopAddress> shop, int selectedShippingMethodId)
        {
            if (shop.Addresses.Any()
                && shop.Addresses.FirstOrDefault().Choices.Count() > 0
                && shop.Addresses.FirstOrDefault().Choices.FirstOrDefault().Count() > 0)
            {
                var choices = shop.Addresses.FirstOrDefault().Choices.FirstOrDefault();
                foreach (var choice in choices)
                {
                    if (choice.MainShippingMethodId == selectedShippingMethodId)
                    {
                        return choice.TotalPrice;
                    }
                }
            }
            return 0;
        }

        private decimal CalculateTotalForPostal(ShippingNetwork<Solex.Shipping.Client.Contract.PostalAddress> postal, int selectedShippingMethodId)
        {
            if (postal.Addresses.Any()
                && postal.Addresses.FirstOrDefault().Choices.Count() > 0
                && postal.Addresses.FirstOrDefault().Choices.FirstOrDefault().Count() > 0)
            {
                var choices = postal.Addresses.FirstOrDefault().Choices.FirstOrDefault();
                foreach (var choice in choices)
                {
                    if (choice.MainShippingMethodId == selectedShippingMethodId)
                    {
                        return choice.TotalPrice;
                    }
                }
            }
            return 0;
        }

        /// <summary>
        /// Filter ClickAndCollect Choice
        /// </summary>
        /// <param name="evaluateShoppingCartResultShopChoices"></param>
        /// <param name="articles"></param>
        /// <returns></returns>
        protected internal IEnumerable<Choice>[] SetShopShippingMethodItemChoices(IEnumerable<Choice>[] evaluateShoppingCartResultShopChoices, IEnumerable<Article> articles)
        {
            var excludeClickAndCollect = false;
            var filteredChoices = new List<IEnumerable<Choice>>();
            var choices = evaluateShoppingCartResultShopChoices.FirstOrDefault() ?? Enumerable.Empty<Choice>();

            if (choices.Any())
            {
                var clickAndCollectChoice = choices.FirstOrDefault(c => c.MainShippingMethodId == Constants.ShippingMethods.ClickAndCollect);
                var articleWithNoPrice = articles.OfType<StandardArticle>().FirstOrDefault(article => article.PriceUserEur == 0 || !article.PriceUserEur.HasValue);

                if (_excludeCCForFreeArticleFilter
                    && clickAndCollectChoice != null
                    && articles.OfType<StandardArticle>().Any(article => article.Type == ArticleType.Free))
                {
                    excludeClickAndCollect = true;

                }
                else if (_excludeCCForFreeCCEligibleArticleFilter
                    && clickAndCollectChoice != null
                    && articleWithNoPrice != null)
                {
                    //Get article Parcel Id
                    var articleGroup = clickAndCollectChoice.Groups.FirstOrDefault(g => g.Items.Any(i => i.Identifier.Prid == articleWithNoPrice.ProductID));
                    var articleItem = articleGroup?.Items.FirstOrDefault(i => i.Identifier.Prid == articleWithNoPrice.ProductID);

                    //check if the article Parcel has ClickAndCollect, if so then exclude ClickAndCollect
                    excludeClickAndCollect = articleItem != null ?
                        clickAndCollectChoice.Parcels[articleItem.ParcelId].ShippingMethodId == Constants.ShippingMethods.ClickAndCollect : false;

                }

            }

            if (excludeClickAndCollect)
            {
                filteredChoices.Add(choices.Where(choice => choice.MainShippingMethodId != Constants.ShippingMethods.ClickAndCollect));
            }
            else
            {
                filteredChoices.Add(choices);
            }
            return filteredChoices.ToArray();
        }
        protected internal IEnumerable<Choice>[] SetPostalShippingMethodItemChoices(IEnumerable<Choice>[] evaluateShoppingCartResultShopChoices, IEnumerable<Article> articles)
        {
            var exclude2HChrono = false;
            var filteredChoices = new List<IEnumerable<Choice>>();
            var choices = evaluateShoppingCartResultShopChoices.FirstOrDefault() ?? Enumerable.Empty<Choice>();

            if (choices.Any())
            {
                var deuxHChronoChoice = choices.FirstOrDefault(c => c.MainShippingMethodId == Constants.ShippingMethods.LivraisonDeuxHeuresChrono);
                var articleWithNoPrice = articles.OfType<StandardArticle>().FirstOrDefault(article => article.PriceUserEur == 0 || !article.PriceUserEur.HasValue);

                if (_exclude2HCForFreeCCEligibleArticleFilter
                    && deuxHChronoChoice != null
                    && articleWithNoPrice != null)
                {
                    //Get article Parcel Id
                    var articleGroup = deuxHChronoChoice.Groups.FirstOrDefault(g => g.Items.Any(i => i.Identifier.Prid == articleWithNoPrice.ProductID));
                    var articleItem = articleGroup?.Items.FirstOrDefault(i => i.Identifier.Prid == articleWithNoPrice.ProductID);

                    //check if the article Parcel has LivraisonDeuxHeuresChrono, if so then exclude LivraisonDeuxHeuresChrono
                    exclude2HChrono = articleItem != null ? deuxHChronoChoice.Parcels[articleItem.ParcelId].ShippingMethodId == Constants.ShippingMethods.LivraisonDeuxHeuresChrono : false;
                }
            }
            if (exclude2HChrono)
            {
                filteredChoices.Add(choices.Where(choice => choice.MainShippingMethodId != Constants.ShippingMethods.LivraisonDeuxHeuresChrono));
            }
            else
            {
                filteredChoices.Add(choices);
            }
            return filteredChoices.ToArray();
        }

        #endregion
    }
}
