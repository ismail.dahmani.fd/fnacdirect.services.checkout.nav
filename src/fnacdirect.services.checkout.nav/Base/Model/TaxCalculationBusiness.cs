using System.Linq;
using System.Collections.Generic;
using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.OrderPipe.Base.Model.TaxCalculation;
using FnacDirect.OrderPipe.Base.Proxy.TaxCalculation;
using FnacDirect.Technical.Framework.Web.Mvc;
using FnacDirect.OrderPipe.Core.Services;
using FnacDirect.OrderPipe.Base.Model.LineGroups;
using FnacDirect.OrderPipe.Base.BaseModel;
using FnacDirect.OrderPipe.Base.LineGroups;
using FnacDirect.OrderPipe.Base.Model.Solex;
using Online = FnacDirect.Contracts.Online.Model;
using System;

namespace FnacDirect.OrderPipe.Base.Business.TaxCalculation
{
    public class TaxCalculationBusiness : ITaxCalculationBusiness
    {
        internal const string MaximumArticleTypeId = "localcountry.condition.articletypeid.maximum";
        internal const string MinimumArticleTypeId = "localcountry.condition.articletypeid.minimum";

        private readonly int _localCountryId;

        private readonly ITaxCalculationService _taxCalculationService;
        private readonly ISwitchProvider _switchProvider;

        private readonly int _maximumArticleTypeId;
        private readonly int _minimumArticleTypeId;

        private readonly int _shippingTaxId;
        private readonly int _wrapTaxId;
        private readonly int _splitTaxId;

        private readonly int _marketId;
        private readonly string _siteId;

        public TaxCalculationBusiness(ITaxCalculationService taxCalculationService, IApplicationContext applicationContext, ISwitchProvider switchProvider, IPipeParametersProvider pipeParametersProvider)
        {
            _taxCalculationService = taxCalculationService;
            _switchProvider = switchProvider;
            _marketId = applicationContext.GetSiteContext().MarketId;
            _siteId = applicationContext.GetSiteContext().SiteID.ToString();
            _localCountryId = _localCountryId = applicationContext.GetSiteContext().CountryId;

            _maximumArticleTypeId = pipeParametersProvider.Get<int>(MaximumArticleTypeId, 19000);
            _minimumArticleTypeId = pipeParametersProvider.Get<int>(MinimumArticleTypeId, 19999);

            _shippingTaxId = pipeParametersProvider.Get<int>("shipping.vat.rate.id", -1);
            _wrapTaxId = pipeParametersProvider.Get<int>("wrapping.vat.rate.id", -2);
            _splitTaxId = pipeParametersProvider.Get<int>("split.vat.rate.id", -3);
        }

        /// <summary>
        /// Expand shipping global price and article price for PostalShippingMethodEvaluationItem's linegroups
        /// </summary>
        public void SetGlobalPriceLineGroup(IEnumerable<ILineGroup> lineGroups, BillingAddress billingAddress, ShippingMethodEvaluationItem selectedShippingMethodEvaluationItem,
                                            ShippingMethodEvaluationGroup shippingMethodEvaluationGroup, string pipeName, string customerTaxNumber)
        {
            var isFnacCom = SellerIds.IsFnacType(shippingMethodEvaluationGroup.SellerId);
            var sellerId = shippingMethodEvaluationGroup.SellerId;

            foreach (var currentShippingMethodEvaluationItem in shippingMethodEvaluationGroup.ShippingMethodEvaluationItems)
            {
                var isShippingEvaluationSelectedOrPostal = IsShippingEvaluationIsSelectedOrPostal(selectedShippingMethodEvaluationItem, currentShippingMethodEvaluationItem);
                var selectedChoice = currentShippingMethodEvaluationItem.GetSelectedChoice();

                if (isShippingEvaluationSelectedOrPostal && selectedChoice != null)
                {
                    // Requêter le service TaxCalculation pour les produits dématérialisés et les autres, séparement.
                    SetGlobalPriceLineGroup(GetLineGroupsWithShippableItems(lineGroups), sellerId, billingAddress, currentShippingMethodEvaluationItem, selectedChoice, isFnacCom, pipeName, customerTaxNumber, true);
                    SetGlobalPriceLineGroup(GetLineGroupsWithoutShippableItems(lineGroups), sellerId, billingAddress, currentShippingMethodEvaluationItem, selectedChoice, isFnacCom, pipeName, customerTaxNumber, false);
                }
            }
        }

        public void SetShippingMethodEvaluationItemsShippingChoicesPrices(BillingAddress billingAddress, ShippingMethodEvaluationGroup shippingMethodEvaluationGroup, string pipeName, string customerTaxNumber)
        {
            //chelou should move in foreach ?
            var taxCountryId = GetTaxCountryId(billingAddress, shippingMethodEvaluationGroup);

            foreach (var shippingMethodEvaluationItem in shippingMethodEvaluationGroup.ShippingMethodEvaluationItems)
            {
                if (shippingMethodEvaluationItem.HasChoices)
                {
                    var shippingAddress = shippingMethodEvaluationItem.ShippingAddress;

                    var isExpeditionAddressLegalStatusEnterprise = IsExpeditionToAddressWithLegalStatusEnterprise(shippingAddress, billingAddress);

                    var shippingCountryId = shippingMethodEvaluationItem.ShippingAddress.CountryId;

                    if (_marketId != (int)Online.FnacMarket.Belgium &&
                        shippingMethodEvaluationItem.ShippingMethodEvaluationType != ShippingMethodEvaluationType.Postal)
                    {
                        taxCountryId = _localCountryId;
                        shippingCountryId = _localCountryId;
                    }

                    var choices = shippingMethodEvaluationItem.Choices.ToList();
                    var orderPipeShippingChoicesQuery = BuildOrderPipeShippingChoicesQuery(taxCountryId, shippingCountryId, choices, isExpeditionAddressLegalStatusEnterprise, pipeName, customerTaxNumber);

                    var shippingChoicesPricesToApply = _taxCalculationService.SetShippingChoicesPrices(orderPipeShippingChoicesQuery);

                    PopulateShippingChoicesTaxInformationsAndPrices(choices, shippingChoicesPricesToApply);
                    shippingMethodEvaluationItem.Choices = choices;
                }
            }
        }

        /// <summary>
        /// Set wrap's price of the available wraps for each linegroup base on the country of its tax infos.
        /// </summary>
        public void SetAvailabelWrapMethods(WrapMethodList availableWrapMethods, int? taxCountryId, bool isProfessionalAddress, string pipeName, string customerTaxNumber)
        {
            // Creates query from wrap list
            var orderPipeWrapQuery = BuildOrderPipeWrapQuery(availableWrapMethods, taxCountryId, isProfessionalAddress, pipeName, customerTaxNumber);

            // Call our service

            var wrapsInformationAndPricesToApply = _taxCalculationService.SetAvailableWrapPriceAndTaxInfos(orderPipeWrapQuery);

            // Update wraps
            PopulateWrapTaxInformationAndPrice(availableWrapMethods, wrapsInformationAndPricesToApply);
        }

        public void SetAvailableSplitPrice(SplitMethod splitMethod, int? taxCountryId, bool isProfessionalAddress, string pipeName, string customerTaxNumber)
        {
            //Creates query from split method
            var orderPipeSplitMethodQuery = BuildOrderPipeSplitMethodQuery(splitMethod, taxCountryId, isProfessionalAddress, pipeName, customerTaxNumber);

            var splitMethodPriceToApply = _taxCalculationService.SetSplitMethodPrices(orderPipeSplitMethodQuery);

            PopulateSplitMethodPrice(splitMethod, splitMethodPriceToApply);
        }

        public List<StandardArticle> SetExtendedBundleComponentPrices(List<Online.SalesArticle> bundleComponentPrices, int taxCountryId, bool isProfessionalAddress, string pipeName, string customerTaxNumber)
        {
            var orderPipeBundleComponentQuery = BuildOrderPipeBundleComponentQuery(bundleComponentPrices, taxCountryId, isProfessionalAddress, pipeName, customerTaxNumber);

            var bundleComponentPricesToApply = _taxCalculationService.GetExtendedPriceForArticles(orderPipeBundleComponentQuery);

            var orderPipeBundleComponentPriceToApply = ConvertBundleComponentPricesToApplyToOrderPipeBundleComponents(bundleComponentPricesToApply, bundleComponentPrices);

            return orderPipeBundleComponentPriceToApply;
        }

        protected List<StandardArticle> ConvertBundleComponentPricesToApplyToOrderPipeBundleComponents(List<TaxCalculationResultItem> bundleComponentPricesToApplyList, List<Online.SalesArticle> bundleComponentPricesList)
        {
            var orderPipeBundleComponentWithTaxesList = new List<StandardArticle>();

            foreach (var bundleComponentPricesToApply in bundleComponentPricesToApplyList)
            {
                var bundleComponentPrices = bundleComponentPricesList.FirstOrDefault(c => c.Reference != null && c.Reference.PRID == bundleComponentPricesToApply.Identifier.Prid);

                if (bundleComponentPrices != null)
                {
                    var orderPipeBundleComponentWithTaxes = new StandardArticle
                    {
                        // From bundle componenent
                        ProductID = bundleComponentPrices.Reference.PRID,
                        AvailabilityId = bundleComponentPrices.Availability?.ID,
                        VATId = bundleComponentPrices.SalesInfo?.VATId,
                        SalesInfo = bundleComponentPrices.SalesInfo,
                        // From prices return
                        VatCountryId = bundleComponentPricesToApply.ItemPrice.TaxToApplyInfos.CountryId,
                        VATRate = bundleComponentPricesToApply.ItemPrice.TaxToApplyInfos.CountryTaxRate,
                        UseVat = bundleComponentPricesToApply.ItemPrice.TaxToApplyInfos.UseTax,
                        PriceUserEur = Math.Round(bundleComponentPricesToApply.ItemPrice.ReducedPriceToApply.TTC, 2, MidpointRounding.ToEven),
                        PriceDBEur = Math.Round(bundleComponentPricesToApply.ItemPrice.StandardPriceToApply.TTC, 2, MidpointRounding.ToEven),
                        PriceRealEur = Math.Round(bundleComponentPricesToApply.ItemPrice.PublicPriceToApply.TTC, 2, MidpointRounding.ToEven),
                    };

                    orderPipeBundleComponentWithTaxesList.Add(orderPipeBundleComponentWithTaxes);
                }
            }

            return orderPipeBundleComponentWithTaxesList;
        }

        protected OrderPipeItemsQuery BuildOrderPipeBundleComponentQuery(List<Online.SalesArticle> bundleComponentPrices, int taxCountryId, bool isProfessionalAddress, string pipeName, string customerTaxNumber)
        {
            var orderPipeItemsQuery = new OrderPipeItemsQuery
            {
                OrderContext = new OrderContext
                {
                    MarketId = _marketId,
                    BillingCountryId = taxCountryId,
                    ReceptionCountryId = taxCountryId,
                    IsProAddress = isProfessionalAddress,
                    CustomerTaxNumber = customerTaxNumber,
                    PipeName = pipeName,
                    SiteId = _siteId
                },
                Items = bundleComponentPrices.Select(b => new TaxCalculationQueryItem
                {
                    Identifier = new Model.TaxCalculation.Identifier { Prid = b.Reference.PRID.GetValueOrDefault() },
                    SalesInfo = ConvertSalesInfoToOrderPipeSalesInfo(b.SalesInfo),
                    IsDematItem = false,
                    IsService = false
                }).ToList()
            };

            return orderPipeItemsQuery;
        }

        protected void PopulateSplitMethodPrice(SplitMethod splitMethod, TaxCalculationSplitMethodResult splitMethodPriceToApply)
        {
            splitMethod.UnitPriceGeneric = splitMethodPriceToApply.GenericUnitPrice ?? new GenericPrice();
            splitMethod.GlobalPriceGeneric = splitMethodPriceToApply.GenericGlobalPrice ?? new GenericPrice();
        }

        protected OrderPipeSplitMethodQuery BuildOrderPipeSplitMethodQuery(SplitMethod splitMethod, int? taxCountryId, bool isProfessionalAddress, string pipeName, string customerTaxNumber)
        {
            var orderPipeSplitMethodQuery = new OrderPipeSplitMethodQuery
            {
                OrderContext = new OrderContext
                {
                    MarketId = _marketId,
                    BillingCountryId = taxCountryId ?? _localCountryId,
                    ReceptionCountryId = taxCountryId,
                    IsProAddress = isProfessionalAddress,
                    CustomerTaxNumber = customerTaxNumber,
                    PipeName = pipeName,
                    SiteId = _siteId
                },
                TaxType = _splitTaxId,
                GlobalCost = splitMethod.GlobalPrice,
                UnitCost = splitMethod.UnitPrice
            };

            return orderPipeSplitMethodQuery;
        }

        protected void PopulateWrapTaxInformationAndPrice(WrapMethodList availableWrapMethods, List<TaxCalculationAvailableWrapResult> wrapsInformationAndPricesToApply)
        {
            foreach (var availableWrapMethod in availableWrapMethods)
            {
                var wrapInfoAndPriceToApply = wrapsInformationAndPricesToApply.FirstOrDefault(w => w.Id == availableWrapMethod.Id);

                if (wrapInfoAndPriceToApply != null)
                {
                    availableWrapMethod.PriceDetailDBEur = Math.Round(wrapInfoAndPriceToApply.PriceDetail.Cost, 2, MidpointRounding.ToEven);
                    availableWrapMethod.PriceDetailNoVatEUR = Math.Round(wrapInfoAndPriceToApply.PriceDetail.NoTaxCost.GetValueOrDefault(), 2, MidpointRounding.ToEven);
                    availableWrapMethod.PriceDetailEur = Math.Round(wrapInfoAndPriceToApply.PriceDetail.CostToApply.GetValueOrDefault(), 2, MidpointRounding.ToEven);

                    availableWrapMethod.PriceGlobalDBEur = Math.Round(wrapInfoAndPriceToApply.PriceGlobal.Cost, 2, MidpointRounding.ToEven);
                    availableWrapMethod.PriceGlobalNoVatEUR = Math.Round(wrapInfoAndPriceToApply.PriceGlobal.NoTaxCost.GetValueOrDefault(), 2, MidpointRounding.ToEven);
                    availableWrapMethod.PriceGlobalEur = Math.Round(wrapInfoAndPriceToApply.PriceGlobal.CostToApply.GetValueOrDefault(), 2, MidpointRounding.ToEven);

                    availableWrapMethod.VATCode = wrapInfoAndPriceToApply.TaxInfos.CodeGu;
                    availableWrapMethod.VATRate = wrapInfoAndPriceToApply.TaxInfos.CountryTaxRate;
                }
            }
        }

        protected OrderPipeWrapQuery BuildOrderPipeWrapQuery(WrapMethodList availableWrapMethods, int? taxCountryId, bool isProfessionalAddress, string pipeName, string customerTaxNumber)
        {
            var orderPipeWrapQuery = new OrderPipeWrapQuery
            {
                OrderContext = new OrderContext
                {
                    MarketId = _marketId,
                    BillingCountryId = taxCountryId ?? _localCountryId,
                    ReceptionCountryId = taxCountryId,
                    IsProAddress = isProfessionalAddress,
                    CustomerTaxNumber = customerTaxNumber,
                    PipeName = pipeName,
                    SiteId = _siteId
                },
                TaxType = _wrapTaxId,
                WrapPrices = availableWrapMethods.Select(w => new WrapPriceQuery(w.Id.Value, w.PriceGlobalEur.GetValueOrDefault(), w.PriceDetailEur.GetValueOrDefault())).ToList()
            };

            return orderPipeWrapQuery;
        }

        protected void PopulateShippingChoicesTaxInformationsAndPrices(IEnumerable<ShippingChoice> choices, TaxCalculationShippingChoicesResult shippingChoicesPricesToApply)
        {
            foreach (var choice in choices)
            {
                if (choice is AggregatedShippingChoice aggregatedShippingChoice)
                {
                    foreach (var aggregatedChoice in aggregatedShippingChoice.ShippingChoices)
                    {
                        SetChoicePrice(aggregatedChoice, shippingChoicesPricesToApply);
                    }
                }
                else
                {
                    SetChoicePrice(choice, shippingChoicesPricesToApply);
                }
            }
        }

        protected OrderPipeShippingChoicesQuery BuildOrderPipeShippingChoicesQuery(int taxCountryId, int? shippingCountryId, IEnumerable<ShippingChoice> choices, bool isProfessionalAddress,
                                                                                   string pipeName, string customerTaxNumber)
        {
            var orderPipeShippingChoicesQuery = new OrderPipeShippingChoicesQuery
            {
                OrderContext = new OrderContext
                {
                    MarketId = _marketId,
                    BillingCountryId = taxCountryId,
                    ReceptionCountryId = shippingCountryId,
                    IsProAddress = isProfessionalAddress,
                    CustomerTaxNumber = customerTaxNumber,
                    PipeName = pipeName,
                    SiteId = _siteId
                },
                TaxType = _shippingTaxId,
                Choices = FlattenShippingChoicesAndAggregatedShippingChoices(choices)
            };
            return orderPipeShippingChoicesQuery;
        }

        /// <summary>
        /// This method takes lineGroups and shipping information to create the request object to use with TaxCaculationService.
        /// </summary>
        /// <param name="lineGroups"></param>
        /// <param name="taxCountryId"></param>
        /// <param name="shippingMethodEvaluationItem"></param>
        /// <param name="selectedChoice"></param>
        /// <returns></returns>
        protected OrderPipeLineGroupQuery BuildOrderPipeLineGroupQuery(IEnumerable<ILineGroup> lineGroups, int sellerId, BillingAddress billingAddress, ShippingMethodEvaluationItem shippingMethodEvaluationItem,
                                                                       Model.Solex.ShippingChoice selectedChoice, bool isFnacCom, string pipeName, string customerTaxNumber, bool hasShippingItems)
        {
            var taxCountryId = GetTaxCountryId(billingAddress, isFnacCom);

            var shippingAddress = shippingMethodEvaluationItem.ShippingAddress;

            var isProfessionalAddress = IsExpeditionToAddressWithLegalStatusEnterprise(shippingAddress, billingAddress);

            var orderPipeLineGroupQuery = OrderPipeLineGroupQueryBuilder.BuildOrderPipeLineGroupsQuery(_shippingTaxId, selectedChoice.TotalPrice.Cost, hasShippingItems);
            orderPipeLineGroupQuery.OrderContext = OrderPipeLineGroupQueryBuilder.BuildOrderContext(_marketId, taxCountryId, shippingAddress.CountryId, isProfessionalAddress, _siteId, pipeName, customerTaxNumber);
            orderPipeLineGroupQuery.Items = OrderPipeLineGroupQueryBuilder.BuildStandardItems(lineGroups, sellerId);

            return orderPipeLineGroupQuery;
        }

        /// <summary>
        /// This method use return from TaxCalculation Nuget Package to populate linegroup with tax data (country infos, prices, tax rate) to apply.
        /// </summary>
        /// <param name="lineGroups"></param>
        /// <param name="lineGroupInformationAndPricesToApply"></param>
        protected void PopulateLineGroupsTaxInformationAndPrice(IEnumerable<ILineGroup> lineGroups, TaxCalcuationLineGroupResult lineGroupInformationAndPricesToApply, ShippingChoice selectedChoice)
        {
            var taxInfos = lineGroupInformationAndPricesToApply.TaxInfos;

            foreach (var lineGroup in lineGroups)
            {
                if (!lineGroup.HasNumericalArticles && !lineGroup.HasDematSoftArticles)
                {
                    lineGroup.GlobalPrices.TotalShipPriceDBEur = selectedChoice.TotalPrice.Cost;
                    lineGroup.GlobalPrices.TotalShipPriceUserEur = selectedChoice.TotalPrice.CostToApply;
                    lineGroup.GlobalPrices.TotalShipPriceUserEurNoVAT = selectedChoice.TotalPrice.NoTaxCost;
                }
                lineGroup.VatCountry = new VatCountry
                {
                    UseVat = taxInfos.UseTax,
                    VatCountryId = taxInfos.CountryId
                };

                if (!lineGroup.HasMarketPlaceArticle)
                {
                    var standardArticles = lineGroup.Articles.OfType<StandardArticle>();
                    PopulateArticlePrices(standardArticles, lineGroupInformationAndPricesToApply.GetPriceResultItemsBySeller(lineGroup.Seller.SellerId), _localCountryId);
                }

                if (lineGroup.HasMarketPlaceArticle)
                {
                    var marketPlaceArticles = lineGroup.Articles.OfType<MarketPlaceArticle>();
                    SetMarketPlaceArticlesPrice(marketPlaceArticles, lineGroup.VatCountry);
                }
            }
        }

        #region private methods
        private bool IsShippingEvaluationIsSelectedOrPostal(ShippingMethodEvaluationItem selectedShippingMethodEvaluationItem, ShippingMethodEvaluationItem currentShippingMethodEvaluationItem)
        {
            var isUnknow = selectedShippingMethodEvaluationItem == null ||
                           selectedShippingMethodEvaluationItem.ShippingMethodEvaluationType == ShippingMethodEvaluationType.Unknown;

            var ShippingEvaluationSelectedOrPostal = selectedShippingMethodEvaluationItem == currentShippingMethodEvaluationItem
                || (isUnknow && currentShippingMethodEvaluationItem.ShippingMethodEvaluationType == ShippingMethodEvaluationType.Postal);
            return ShippingEvaluationSelectedOrPostal;
        }

        private void PopulateArticlePrices(IEnumerable<StandardArticle> standardArticles, IEnumerable<TaxCalculationResultItem> articlesExtendedPrice, int localCountryId)
        {
            if (articlesExtendedPrice != null && articlesExtendedPrice.Any())
            {
                foreach (var standardArticle in standardArticles)
                {
                    var standardArticleExtendedPrice = articlesExtendedPrice.FirstOrDefault(art => art.Identifier.Prid == standardArticle.ProductID);

                    if (standardArticleExtendedPrice == null)
                    {
                        continue;
                    }

                    ApplyExtendedPriceToArticle(standardArticleExtendedPrice, standardArticle);

                    // potentiellement cette condition est mal gérée sur les prix d'article, on altère l'id du pays sans changer le taux appliquer...
                    // logique dans le nuget plutôt ?
                    if (standardArticle.TypeId.HasValue &&
                        _minimumArticleTypeId <= standardArticle.TypeId.Value &&
                        standardArticle.TypeId.Value <= _maximumArticleTypeId)
                    {
                        standardArticle.VatCountryId = localCountryId;
                        standardArticle.UseVat = true;
                    }

                    foreach (var service in standardArticle.Services)
                    {
                        var serviceExtendedPrice = articlesExtendedPrice.FirstOrDefault(art => art.Identifier.Prid == service.ProductID);

                        if (serviceExtendedPrice == null)
                        {
                            continue;
                        }

                        ApplyExtendedPriceToArticle(serviceExtendedPrice, service);
                    }

                    if (standardArticle.SalesInfo.EcoTaxAmount.TTC.HasValue
                            && standardArticle.Type != ArticleType.Free
                            && _switchProvider.IsEnabled("orderpipe.pop.enable.ecotax", false))
                    {
                        standardArticle.EcoTaxCode = standardArticle.SalesInfo.EcoTaxCode;
                        standardArticle.EcoTaxEur = standardArticleExtendedPrice.ItemPrice.EcoTaxAmountToApply.TTC;
                        standardArticle.EcoTaxEurNoVAT = standardArticleExtendedPrice.ItemPrice.EcoTaxAmountToApply.HT;
                    }

                }
            }
        }

        private static void ApplyExtendedPriceToArticle(TaxCalculationResultItem standardArticleExtendedPrice, StandardArticle article)
        {
            // TO DO : Refacto de l'object standardarticle pour encapsuler certains champs (prix, wrap, eco-tax...)
            article.PriceUserEur = Math.Round(standardArticleExtendedPrice.ItemPrice.ReducedPriceToApply.TTC, 2, MidpointRounding.ToEven);
            article.PriceUserEurNoVAT = Math.Round(standardArticleExtendedPrice.ItemPrice.ReducedPriceToApply.HT, 2, MidpointRounding.ToEven);
            article.PriceNoVATEur = Math.Round(standardArticleExtendedPrice.ItemPrice.ReducedPriceToApply.HT, 2, MidpointRounding.ToEven);

            article.PriceDBEur = Math.Round(standardArticleExtendedPrice.ItemPrice.StandardPriceToApply.TTC, 2, MidpointRounding.ToEven);
            article.PriceDBEurNoVAT = Math.Round(standardArticleExtendedPrice.ItemPrice.StandardPriceToApply.HT, 2, MidpointRounding.ToEven);

            article.PriceRealEur = Math.Round(standardArticleExtendedPrice.ItemPrice.PublicPriceToApply.TTC, 2, MidpointRounding.ToEven);
            article.PriceRealEurNoVAT = Math.Round(standardArticleExtendedPrice.ItemPrice.PublicPriceToApply.HT, 2, MidpointRounding.ToEven);

            article.VATRate = standardArticleExtendedPrice.ItemPrice.TaxToApplyInfos.CountryTaxRate;
            article.VATCodeGu = standardArticleExtendedPrice.ItemPrice.TaxToApplyInfos.CodeGu;
            article.VatCountryId = standardArticleExtendedPrice.ItemPrice.TaxToApplyInfos.CountryId;
            article.UseVat = standardArticleExtendedPrice.ItemPrice.TaxToApplyInfos.UseTax;
        }

        /// <summary>
        /// Map price from TaxCalculationService's shipping choice result with orderpipe shipping choice. 
        /// </summary>
        private static void SetChoicePriceFromPriceToApply(GenericPrice choicePrice, GenericPrice choicePriceToApply)
        {
            choicePrice.Cost = Math.Round(choicePriceToApply.Cost, 2, MidpointRounding.ToEven);
            choicePrice.CostToApply = Math.Round(choicePriceToApply.CostToApply.GetValueOrDefault(0), 2, MidpointRounding.ToEven);
            choicePrice.NoTaxCost = Math.Round(choicePriceToApply.NoTaxCost.GetValueOrDefault(0), 2, MidpointRounding.ToEven);
            choicePrice.TaxInfos = choicePriceToApply.TaxInfos;
        }

        /// <summary>
        /// This method expands total price of orderpipe choices, global price of choice's groups and unit price group's items
        /// with tax infos (tax's rate, tax's type, tax rate country's id) using result from TaxCalculationSevice
        /// </summary>
        private void SetChoicePrice(ShippingChoice choice, TaxCalculationShippingChoicesResult shippingChoicesPricesToApply)
        {
            var choicePriceToApply = shippingChoicesPricesToApply.Choices.FirstOrDefault(c => c.MainShippingMethodId == choice.MainShippingMethodId);

            if (choicePriceToApply == null || choice.Groups == null)
            {
                return;
            }

            var choiceGroups = choice.Groups.ToList();

            foreach (var group in choiceGroups)
            {
                var groupPriceToApply = choicePriceToApply.Groups.FirstOrDefault(c => c.Id == group.Id);

                if (groupPriceToApply == null)
                {
                    continue;
                }

                SetChoicePriceFromPriceToApply(group.GlobalPrice, groupPriceToApply.GlobalPrice);

                if (group.Items == null)
                {
                    return;
                }

                var groupItems = group.Items.ToList();

                foreach (var item in groupItems)
                {
                    TaxCalcuationItemInGroupResult itemPriceToApply;
                    if (item.Identifier.OfferReference.HasValue)
                    {
                        itemPriceToApply = groupPriceToApply.Items.FirstOrDefault(i => i.Identifier.Prid == item.Identifier.Prid &&
                                                                                       i.Identifier.OfferReference.Value == item.Identifier.OfferReference.Value);
                    }
                    else
                    {
                        itemPriceToApply = groupPriceToApply.Items.FirstOrDefault(i => i.Identifier.Prid == item.Identifier.Prid);
                    }

                    SetChoicePriceFromPriceToApply(item.UnitPrice, itemPriceToApply.UnitPrice);
                }

                group.Items = groupItems;
            }

            choice.TotalPrice = CalculateChoiceTotalPrice(choiceGroups);

            choice.Groups = choiceGroups;
        }

        /// <summary>
        /// This method computes totalcost for shipping choice from group global prices and item unit prices after
        /// their have been re-evaluated by the tax calculation module
        /// </summary>
        private GenericPrice CalculateChoiceTotalPrice(List<Group> choiceGroups)
        {
            var totalPriceCost = 0m;
            var totalPriceCostToApply = 0m;
            var totalPriceNoTaxCost = 0m;
            var choiceTaxInfos = new VatInfo();


            foreach (var group in choiceGroups)
            {
                totalPriceCost += group.GlobalPrice.Cost + group.Items.Sum(i => i.Quantity * i.UnitPrice.Cost);
                totalPriceNoTaxCost += group.GlobalPrice.NoTaxCost.GetValueOrDefault() + group.Items.Sum(i => i.Quantity * i.UnitPrice.NoTaxCost.GetValueOrDefault());
                totalPriceCostToApply += group.GlobalPrice.CostToApply.GetValueOrDefault() + group.Items.Sum(i => i.Quantity * i.UnitPrice.CostToApply.GetValueOrDefault());
                choiceTaxInfos = group.GlobalPrice.TaxInfos;
            }

            return new GenericPrice
            {
                Cost = totalPriceCost,
                NoTaxCost = totalPriceNoTaxCost,
                CostToApply = totalPriceCostToApply,
                TaxInfos = choiceTaxInfos
            };
        }

        /// <summary>
        /// This method is used to split aggregated shipping choice into a list of shipping choices and add them to the shipping choice list
        /// of the TaxCalculationService query object. 
        /// </summary>
        private List<TaxCalcultationQueryShippingChoice> FlattenShippingChoicesAndAggregatedShippingChoices(IEnumerable<ShippingChoice> choices)
        {
            var flattenShippingChoices = new List<TaxCalcultationQueryShippingChoice>();

            foreach (var choice in choices)
            {
                if (choice is AggregatedShippingChoice aggregatedShippingChoice)
                {
                    foreach (var aggregatedChoice in aggregatedShippingChoice.ShippingChoices)
                    {
                        if (!flattenShippingChoices.Any(c => c.MainShippingMethodId == aggregatedChoice.MainShippingMethodId))
                        {
                            var queryShippingChoice = new TaxCalcultationQueryShippingChoice(aggregatedChoice);
                            flattenShippingChoices.Add(queryShippingChoice);
                        }
                    }
                }
                else
                {
                    if (!flattenShippingChoices.Any(c => c.MainShippingMethodId == choice.MainShippingMethodId))
                    {
                        var queryShippingChoice = new TaxCalcultationQueryShippingChoice(choice);
                        flattenShippingChoices.Add(queryShippingChoice);
                    }
                }
            }

            return flattenShippingChoices;
        }

        private void SetMarketPlaceArticlesPrice(IEnumerable<MarketPlaceArticle> marketPlaceArticles, VatCountry billingCountryTaxInfo)
        {
            foreach (var marketPlaceArticle in marketPlaceArticles)
            {
                marketPlaceArticle.PriceUserEur = marketPlaceArticle.SalesInfo?.ReducedPrice?.TTC;
                marketPlaceArticle.PriceDBEur = marketPlaceArticle.SalesInfo?.StandardPrice?.TTC;
                marketPlaceArticle.PriceRealEur = marketPlaceArticle.SalesInfo?.PublicPrice?.TTC;

                marketPlaceArticle.VatCountryId = billingCountryTaxInfo.VatCountryId;
                marketPlaceArticle.UseVat = billingCountryTaxInfo.UseVat;
            }
        }

        private int GetTaxCountryId(BillingAddress billingAddress, ShippingMethodEvaluationGroup shippingMethodEvaluationGroup)
        {
            var isFnacCom = shippingMethodEvaluationGroup.SellerId == (int)SellerType.Fnac;
            return GetTaxCountryId(billingAddress, isFnacCom);
        }

        private int GetTaxCountryId(BillingAddress billingAddress, bool isFnacCom)
        {
            var taxCountryId = _localCountryId;
            if (isFnacCom && billingAddress != null && billingAddress.CountryId.HasValue)
            {
                taxCountryId = billingAddress.CountryId.Value;
            }

            return taxCountryId;
        }

        private bool IsExpeditionToAddressWithLegalStatusEnterprise(ShippingAddress shippingAddress, BillingAddress billingAddress)
        {
            if (shippingAddress != null)
            {
                return IsProfessionalLegalStatus(shippingAddress.LegalStatus);
            }

            return IsProfessionalLegalStatus(billingAddress?.LegalStatus);
        }

        private static bool IsProfessionalLegalStatus(int? legalStatus)
        {
            return legalStatus.HasValue && legalStatus.Value == (int)LegalStatus.Business;
        }

        private void SetGlobalPriceLineGroup(IEnumerable<ILineGroup> lineGroups, int sellerId, BillingAddress billingAddress, ShippingMethodEvaluationItem shippingMethodEvaluationItem,
                                             ShippingChoice selectedChoice, bool isFnacCom, string pipeName, string customerTaxNumber, bool hasShippingItems)
        {
            if (lineGroups == null || lineGroups.Empty())
            {
                return;
            }

            //CreateQuery From LineGroup
            var orderPipeLineGroupQuery = BuildOrderPipeLineGroupQuery(lineGroups, sellerId, billingAddress, shippingMethodEvaluationItem, selectedChoice, isFnacCom, pipeName, customerTaxNumber, hasShippingItems);

            //Call our service
            var lineGroupInformationAndPricesToApply = _taxCalculationService.SetLineGroupsAllPricesAndTaxInfos(orderPipeLineGroupQuery);

            //Update LineGroup
            PopulateLineGroupsTaxInformationAndPrice(lineGroups, lineGroupInformationAndPricesToApply, selectedChoice);
        }

        private static Func<ILineGroup, bool> _lineGroupWithoutShippableItemsCondition = l => l is PopLineGroup p
            && (p.Seller.Type == SellerType.Numerical || p.Seller.SubType == SellerType.Numerical
                || p.Seller.Type == SellerType.Software || p.Seller.SubType == SellerType.Software);

        private IEnumerable<ILineGroup> GetLineGroupsWithShippableItems(IEnumerable<ILineGroup> lineGroups)
        {
            return lineGroups.Where(l => !_lineGroupWithoutShippableItemsCondition(l));
        }

        private IEnumerable<ILineGroup> GetLineGroupsWithoutShippableItems(IEnumerable<ILineGroup> lineGroups)
        {
            return lineGroups.Where(l => _lineGroupWithoutShippableItemsCondition(l));
        }

        private static OrderPipeSalesInfo ConvertSalesInfoToOrderPipeSalesInfo(Online.SalesInfo salesInfo)
        {
            var orderPipeSalesInfo = new OrderPipeSalesInfo();

            if (salesInfo != null)
            {
                orderPipeSalesInfo.TaxId = salesInfo.VATId;
                orderPipeSalesInfo.TaxRate = salesInfo.VATRate;
                orderPipeSalesInfo.ReducedPrice = new OrderPipeSalesInfoPrice { TTC = salesInfo.ReducedPrice.TTC, HT = salesInfo.ReducedPrice.HT };
                orderPipeSalesInfo.StandardPrice = new OrderPipeSalesInfoPrice { TTC = salesInfo.StandardPrice.TTC, HT = salesInfo.StandardPrice.HT };
                orderPipeSalesInfo.PublicPrice = new OrderPipeSalesInfoPrice { TTC = salesInfo.PublicPrice.TTC, HT = salesInfo.PublicPrice.HT };
            }

            return orderPipeSalesInfo;
        }

        #endregion
    }
}
