using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.OrderPipe.Base.Model.Facets;
using FnacDirect.OrderPipe.Base.Model.LineGroups;
using System.Collections.Generic;

namespace FnacDirect.OrderPipe.Base.Business.Payment
{
    public interface IGiftPaymentService
    {
        bool IsCcvAllowed(PopOrderForm popOrderForm, IEnumerable<ILineGroup> lineGroups, decimal totalPriceEur, int anonymousBillingMethodsMaxAmount, bool isGuest, bool disableClickAndCollectCheck = false, bool disableMaxAmount = false, bool disableEccvCheck = false);
        bool IsGiftCardAllowed(IGotLineGroups iGotLineGroups, decimal totalPriceEur, int anonymousBillingMethodsMaxAmount, bool isGuest, bool disableClickAndCollectCheck = false, bool disableMaxAmount = false, bool disableECCVCheck = false);
    }
}
