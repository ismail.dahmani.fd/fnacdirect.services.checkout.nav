using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.Relay.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Common.Shipping.Relay
{
    public class RelayAddressViewModel
    {
        public RelayAddressViewModel(RelayAddress relayAddress)
        {
            Id = relayAddress.Identity.GetValueOrDefault();
            Name = TransformName(relayAddress.LastName);

            var addresses = (new[]{ relayAddress.AddressLine1,
                                    relayAddress.AddressLine2,
                                    relayAddress.AddressLine3,
                                    relayAddress.AddressLine4})
                                  .Where(s => !string.IsNullOrEmpty(s)).ToArray();

            Address = string.Join(" ", addresses);
            ZipCode = relayAddress.ZipCode;
            City = relayAddress.City;
        }

        private string TransformName(string name)
        {
            if (!string.IsNullOrEmpty(name) && name.Length > 1)
            {
                var tmp = name.ToLowerInvariant();

                return tmp[0].ToString().ToUpperInvariant() + tmp.Substring(1, tmp.Length - 1);
            }

            return name;
        }

        public RelayAddressViewModel(RelayAddressEntity relayAddressEntity)
        {
            Id = relayAddressEntity.Id;
            Name = TransformName(relayAddressEntity.BusinessName);

            var addresses = (new[]{ relayAddressEntity.AddressPart1,
                                    relayAddressEntity.AddressPart2,
                                    relayAddressEntity.AddressPart3,
                                    relayAddressEntity.AddressPart4})
                                  .Where(s => !string.IsNullOrEmpty(s)).ToArray();

            Address = string.Join(" ", addresses);
            ZipCode = relayAddressEntity.ZipCode;
            City = relayAddressEntity.City;
        }

        public int Id { get; set; }

        public string Name { get; set; }

        public string Address { get; set; }

        public string ZipCode { get; set; }

        public string City { get; set; }

        public decimal? CarbonFootprint { get; set; }
    }
}
