using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Common.Payment
{
    public class FibBillingMethodViewModel : BillingMethodViewModel
    {
        public string PriceFormat { get; set; }
        public string OrderUserReference { get; set; }
        public string LogisticNumbers { get; internal set; }
    }
}
