namespace FnacDirect.OrderPipe.Base.Model
{
    public class DefferedPaymentBillingMethod : BillingMethod
    {
        public const int IdBillingMethod = 512;

        public override bool IsPrimary => true;

        public DefferedPaymentBillingMethod()
        {
            BillingMethodId = IdBillingMethod;
            CanModifyAmountUp = true;
            CanModifyAmountDown = true;
            ImplyValidateOrder = true;
        }
    }
}
