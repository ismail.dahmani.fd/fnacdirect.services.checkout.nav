using FnacDirect.Customer.Model;
using FnacDirect.OrderPipe.Base.Business.Payment;
using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.OrderPipe.Base.Model.BillingMethods;
using FnacDirect.OrderPipe.Base.Model.Facets;
using FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Common.Payment;
using FnacDirect.Technical.Framework.ServiceLocation;
using FnacDirect.Technical.Framework.Web;
using FnacDirect.Technical.Framework.Web.Mvc;
using System;
using System.Linq;
using System.Web.Mvc;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Controllers.Pop
{
    public abstract class PopPaymentBaseController : PopBaseController
    {
        private readonly OPContext _opContext;
        private readonly IPaymentViewModelBuilder _paymentViewModelBuilder;
        private readonly IPaymentSelectionBusiness _paymentSelectionBusiness;       
        private readonly IUrlManager _urlManager;

        public PopPaymentBaseController(IPaymentSelectionBusiness paymentSelectionBusiness, 
                                        IUrlManager urlManager,
                                        IPaymentViewModelBuilder paymentViewModelBuilder,
                                        OPContext opContext)
        {
            _opContext = opContext;
            _paymentViewModelBuilder = paymentViewModelBuilder;
            _paymentSelectionBusiness = paymentSelectionBusiness;
            _urlManager = urlManager;
        }

        public bool PaymentValidation(int? paymentMethodId, OPContext opContext)
        {
            var gotBillingInformations = opContext.OF as IGotBillingInformations;
            var gotUserContextInformations = opContext.OF as IGotUserContextInformations;
            var gotLogisticLineGroups = opContext.OF as IGotLogisticLineGroups;
             
            var paymentData = gotBillingInformations.GetPrivateData<PaymentSelectionData>();

            if(gotBillingInformations.MainBillingMethod != null && paymentData.RemainingAmount == 0)
            {
                return true;
            }

            if (paymentMethodId == OgoneCreditCardBillingMethod.IdBillingMethod)
            {
                var model = new OgoneViewModel();

                if (TryUpdateModel(model))
                {
                    var result = _paymentSelectionBusiness.OgoneSelectionAndHandleCreditCard(model.SelectedCreditCardReference,
                                                                                model.SelectedCreditCardId,
                                                                                model.RegisterCreditCard,
                                                                                gotBillingInformations,
                                                                                gotUserContextInformations,
                                                                                gotLogisticLineGroups,
                                                                                opContext.Pipe.GlobalParameters);

                    if(!result.Item1)
                    {
                        return false;
                    }
                }
                else
                {
                    return false;
                }
            }
            else if (paymentMethodId == PayByRefBillingMethod.IdBillingMethod)
            {
                var model = new PayByRefBillingMethodViewModel();

                if (TryUpdateModel(model))
                {
                    return _paymentSelectionBusiness.PayByRefSelection(model.Nib, gotBillingInformations, gotUserContextInformations);
                }
                return false;
            }
            else if (paymentMethodId == UnicreCreditCardBillingMethod.IdBillingMethod && paymentData.RemainingMethods.Any(bm => bm.Type == typeof(UnicreCreditCardBillingMethod)))
            {
                var model = new UnicreBillingMethodViewModel();

                if (TryUpdateModel(model))
                {
                    return _paymentSelectionBusiness.UnicreSelection(model.SelectedCreditCardId, gotBillingInformations);
                }
                return false;
            }
            else if (paymentMethodId == CreditCardBillingMethod.IdBillingMethod && paymentData.RemainingMethods.Any(bm => bm.Type == typeof(CreditCardBillingMethod)))
            {
                var model = new SipsViewModel();

                var modelIsValid = TryUpdateModel(model);

                if (modelIsValid && !model.NumByPhone)
                {
                    var oCcd = new CreditCardEntity
                    {
                        TypeId = model.SelectedCreditCardId,
                        CardNumber = model.CardNo,

                        //si on a passé l'attribut de validation sur modèle que ValidityMonth ou ValidityYear est null c'est parce que nous sommes dans le cas d'une
                        //carte qui n'en a pas besoin donc on remplit les champs avec une date factice qui sera surchargé en amont.
                        ExpireMonth = int.Parse(model.ValidityMonth ?? "0"),
                        ExpireYear = int.Parse(model.ValidityYear ?? DateTime.Now.Year.ToString()),
                        Holder = model.Holder,
                        VerificationCode = model.Cvv
                    };

                    var result = _paymentSelectionBusiness.SipsSelection(oCcd, model.CreditId,
                                    gotBillingInformations, gotUserContextInformations);

                    if (!result)
                    {
                        return false;
                    }
                }
                else if (model.NumByPhone)
                {
                    _paymentSelectionBusiness.SipsByPhoneSelection(gotUserContextInformations.UserContextInformations.UID, gotBillingInformations);
                }
                else
                {
                    return false;
                }
            }
            else if (paymentMethodId == LimonetikBillingMethod.IdBillingMethod)
            {
                var model = new LimonetikBillingMethodViewModel();
                var modelIsValid = TryUpdateModel(model);

                var result = _paymentSelectionBusiness.LimonetikSelection(model.GiftCardId, gotBillingInformations, gotUserContextInformations);

                if (!result)
                {
                    return false;
                }
            }
            else if (paymentMethodId == BankTransferBillingMethod.IdBillingMethod)
            {
                _paymentSelectionBusiness.BankTransfertSelection(gotBillingInformations);
                return true;
            }
            else if (paymentMethodId == FnacCardBillingMethod.IdBillingMethod)
            {
                var model = new FnacCardMethodsViewModel();

                var modelIsValid = TryUpdateModel(model);

                if (modelIsValid)
                {
                    var oCcd = new CreditCardEntity
                    {
                        CardNumber = model.CardNo,

                        ExpireMonth = int.Parse(model.ValidityMonth),
                        ExpireYear = int.Parse(model.ValidityYear),
                        Holder = model.Holder,
                        VerificationCode = model.Cvv
                    };

                    var result = _paymentSelectionBusiness.FnacCardSelection(oCcd, model.CreditId,
                                    gotBillingInformations, gotUserContextInformations);

                    if (!result)
                    {
                        return false;
                    }
                }
            }
            else if (paymentMethodId == FnacCardCetelemBillingMethod.IdBillingMethod)
            {
                var model = new CreditCardCetelemMethodeViewModel();
                var modelIsValid = TryUpdateModel(model);
                if (modelIsValid)
                {
                    _paymentSelectionBusiness.FnacCardCetelemSelection(gotBillingInformations, gotUserContextInformations, model.SelectedCreditCardTypeId.ToString());
                }
                else
                {
                    return false;
                }

            }
            else if (paymentMethodId == PayOnDeliveryBillingMethod.IdBillingMethod)
            {
                var popOrderForm = opContext.OF as PopOrderForm;

                if (popOrderForm.PayOnDeliverySelected())
                {
                    return _paymentSelectionBusiness.PayOnDeliverySelection(gotBillingInformations);
                }
                else
                {
                    return false;
                }
            }
            else if (paymentMethodId == DefferedPaymentBillingMethod.IdBillingMethod)
            {
                return _paymentSelectionBusiness.PayDeferred(gotBillingInformations, gotUserContextInformations);
            }
            else if (paymentMethodId == FibBillingMethod.IdBillingMethod)
            {
                var model = new FibBillingMethodViewModel();
                if (TryUpdateModel(model))
                {
                    return _paymentSelectionBusiness.FibSelection(gotBillingInformations, gotUserContextInformations);
                }
                return false;
            }
            else if (paymentMethodId == CheckBillingMethod.IdBillingMethod)
            {
                var model = new CheckMethodViewModel();

                if (TryUpdateModel(model))
                {
                    return _paymentSelectionBusiness.CheckSelection(model.CheckNumber, gotBillingInformations, gotUserContextInformations);
                }
                return false;
            }
            else if (paymentMethodId == OrangeBillingMethod.IdBillingMethod)
            {
                var model = new OrangeBillingMethodsViewModel();
                _paymentSelectionBusiness.OrangeSelection(gotBillingInformations);
            }

            return true;
        }

        [HttpGet]
        public ActionResult Methods(string selectedTab, string selectedPaymentMethod)
        {
            var popOrderForm = _opContext.OF as PopOrderForm;

            PaymentViewModel paymentViewModel = null;

            if (string.IsNullOrEmpty(selectedPaymentMethod))
            {
                paymentViewModel = _paymentViewModelBuilder.Build(_opContext, popOrderForm, selectedTab);
            }
            else
            {
                paymentViewModel = _paymentViewModelBuilder.Build(_opContext, popOrderForm, selectedTab, selectedPaymentMethod);
            }


            var applicationContext = ServiceLocator.Current.GetInstance<IApplicationContext>();
            var country = applicationContext.GetLocalCountry();

            if (string.Equals(country, "fr", StringComparison.InvariantCultureIgnoreCase))
            {   
                return PartialView("Payment/_BillingMethods", Maybe.Some(paymentViewModel));
            }

            return PartialView("Payment/_BillingMethods", paymentViewModel.Tabs);
        }

        [HttpGet]
        public ActionResult SecondaryMethods(string selectedTab, string selectedPaymentMethod)
        {
            var popOrderForm = _opContext.OF as PopOrderForm;

            var applicationContext = ServiceLocator.Current.GetInstance<IApplicationContext>();
            var country = applicationContext.GetLocalCountry();

            if (string.Equals(country, "fr", StringComparison.InvariantCultureIgnoreCase))
            {
                PaymentViewModel paymentViewModel = null;

                if (string.IsNullOrEmpty(selectedPaymentMethod))
                {
                    paymentViewModel = _paymentViewModelBuilder.Build(_opContext, popOrderForm, selectedTab);
                }
                else
                {
                    paymentViewModel = _paymentViewModelBuilder.Build(_opContext, popOrderForm, selectedTab, selectedPaymentMethod);
                }

                return PartialView("Payment/_SecondaryBillingMethods", paymentViewModel);
            }

            var secondaryBillingMethodsViewModel = _paymentViewModelBuilder.BuildTabs(_opContext, popOrderForm, "tabs.group.secondary.payment.methods", string.Empty);

            return PartialView("Payment/_SecondaryBillingMethods", secondaryBillingMethodsViewModel);
        }
    }
}
