
using System.Collections.Generic;

namespace FnacDirect.OrderPipe.Base.Business.Mailing
{
    public class OrderGlobalInfo
    {
        public bool IsResocomRequired { get; set; }

        public string ResocomValidationUrl { get; set; }

        public bool HasManyShippingAddresses { get; set; }

        public bool IsOnlyNumerical { get; set; }

        public bool HasAppointment { get; set; }

        public bool HasSameDayDeliveryAdvantageCode { get; set; }

        public bool IsJukeBoxEnabled { get; set; }

        public AppointmentDeliveryInfo AppointmentDeliveryInfo { get; set; }

        public string TotalPrice { get; set; }

        public string TotalPriceNoVat { get; set; }

        public string WrapPrice { get; set; }

        public string TotalShippingPriceNoVat { get; set; }

        public List<VatRate> VatRates { get; set; }

        public bool IsPro { get; set; }
    }
}
