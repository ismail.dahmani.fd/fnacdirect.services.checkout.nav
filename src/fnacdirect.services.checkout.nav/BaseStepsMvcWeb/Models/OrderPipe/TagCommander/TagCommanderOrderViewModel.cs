﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.TagCommander
{
    public class TagCommanderOrderViewModel
    {
        public TagCommanderOrderViewModel()
        {
            Products = Enumerable.Empty<TagCommanderProductViewModel>().ToList();
        }

        public IList<TagCommanderProductViewModel> Products { get; set; }
        
        public string TemplateName { get; set; }
        public string PageName { get; set; }

        public string CartId { get; set; }
        public string OrderZip { get; set; }
        public string OrderState { get; set; }
        public string OrderTax { get; set; }
        public string OrderAmountAti { get; set; }
        public string OrderAmountWt { get; set; }
        public string OrderAmountAtiWithSf { get; set; }
        public string OrderAmountTfWithSf { get; set; }
        public string OrderCurrency { get; set; }
        public string OrderShipTf { get; set; }
        public string OrderShipAti { get; set; }
        public string OrderShippingMethod { get; set; }
        public string OrderNewClient { get; set; }
        public string OrderAmount { get; set; }
        public string OrderPromoCode { get; set; }
        public string OrderNbProducts { get; set; }
    }

    public class TagCommanderProductViewModel
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Ref { get; set; }
        public string RefId { get; set; }
        public string Quantity { get; set; }
        public string UnitPriceAti { get; set; }
        public string UnitPriceTf { get; set; }
        public string Trademark { get; set; }
        public string UrlPage { get; set; }
        public string UrlPicture { get; set; }
        public string Rating { get; set; }
        public string AvailabiltyLabel { get; set; }
        public string CategoryId { get; set; }
        public string CategoryLabel { get; set; }
        public string ProductItemType { get; set; }
        public string ProductEAN { get; set; }
        public string ProductPartNumber { get; set; }
        public Guid OfferId { get; set; } = Guid.Empty;
    }
}
