﻿using FnacDirect.OrderPipe.Config;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FnacDirect.OrderPipe.Context
{
    public interface IUiDescriptorRendererLocator
    {
        IUiDescriptorRenderer GetUiDescriptorRendererLocatorFor<TDescriptor>(TDescriptor descriptor) where TDescriptor : IUiDescriptor;
    }
}
