﻿using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.OrderPipe.Base.Model.LineGroups;

namespace FnacDirect.OrderPipe.Base.Business
{
    public interface IMergeLineGroupsArticlesBusiness
    {
        void MergeLineGroupsAndArticles(BaseOF of);
        bool DeleteArticle(LineGroup lg, Article art);
    }
}
