
using FnacDirect.Contracts.Online.Model;
using FnacDirect.OrderPipe.Base.Business.Shipping.Postal;
using System.Collections.Generic;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.OnePage.Address
{
    public interface IAddressViewModelBuilder
    {
        AddressViewModel Build(AddressFormViewModelMode addressFormViewModelMode);
        AddressViewModel Build(string addressReference, AddressFormViewModelMode addressFormViewModelMode);
        AddressFormViewModel BuildForm(AddressFormViewModelMode addressFormViewModelMode, AddressViewModel addressViewModel, IEnumerable<IAddress> addresses);
        AddressFormViewModel BuildForm(AddressFormViewModelMode addressFormViewModelMode, AddressViewModel addressViewModel);
        AddressFormViewModel BuildForm(AddressFormViewModelMode addressFormViewModelMode);
        AddressFormViewModel GetAddressFormViewModelByAddressReference(string addressReference, AddressFormViewModelMode addressFormViewModelMode, int sellerId);
    }
}
