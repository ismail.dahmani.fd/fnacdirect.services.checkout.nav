using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;
using FnacDirect.Contracts.Online.Model;
using FnacDirect.Customer.Model;
using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.OrderPipe.Base.Model.Facets;
using FnacDirect.OrderPipe.Base.Model.LineGroups;
using FnacDirect.OrderPipe.Base.Model.UserInformations;

namespace FnacDirect.OrderPipe.Base.Business.Shipping.Postal
{
    public interface IAddressService
    {
        AddressEntity GetAddressByAddressBookReference(string addressReference);
        AddressElegibilityList GetBillingAddresses(OPContext opContext,IUserContextInformations userContext, IEnumerable<ILineGroup> lineGroups, bool withKoboEligibility);
        AddressElegibilityList GetBillingAddresses(OPContext opContext,IUserContextInformations userContext, IEnumerable<ILineGroup> lineGroups, int? pageSize, int? pageIndex = null);
        AddressElegibilityList GetShippingAddresses(OPContext opContext, IGotShippingMethodEvaluation iContainsShippingMethodEvaluation, IEnumerable<ILineGroup> lineGroups);
        AddressElegibilityList GetShippingAddresses(OPContext opContext, IGotShippingMethodEvaluation iContainsShippingMethodEvaluation, IEnumerable<ILineGroup> lineGroups, int? pageSize = null, int? lastId = null);
        Task<AddressValidityResult> GetAddressValidityResultAsync(IAddress address);
        IEnumerable<ValidationResult> Add(OPContext opContext, IGotShippingMethodEvaluation orderForm, AddressEntity addressEntity,IEnumerable<ILineGroup> lineGroups, bool checkElegibility);
        IEnumerable<ValidationResult> Update(OPContext opContext, IGotShippingMethodEvaluation orderForm, AddressEntity addressEntity, IEnumerable<ILineGroup> lineGroups, bool checkElegibility);
        IEnumerable<ValidationResult> AddWithBillingElegibility(OPContext opContext, AddressEntity addressEntity, IEnumerable<ILineGroup> lineGroups);
        IEnumerable<ValidationResult> UpdateWithBillingElegibility(OPContext opContext, AddressEntity addressEntity, IEnumerable<ILineGroup> lineGroups);
        bool IsAddressEligible(OPContext opContext, IGotShippingMethodEvaluation iGotShippingMethodEvaluation, IEnumerable<ILineGroup> lineGroups, AddressEntity address);
        bool IsBillingAddressEligible(OPContext opContext,IUserContextInformations userContext, IEnumerable<ILineGroup> lineGroups, AddressEntity addressEntity);
    }
}
