using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FnacDirect.OrderPipe.Base.Business.Mailing
{
    public class GuestInfos
    {
        public bool IsGuest { get; set; }

        public string GuestOrderTrackingUrl { get; set; }

        public string GuestCGULabel { get; set; }
    }
}
