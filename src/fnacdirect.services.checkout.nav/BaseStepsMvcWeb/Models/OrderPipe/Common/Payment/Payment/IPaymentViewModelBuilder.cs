using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.OrderPipe.BaseMvc.Web.Models.OrderPipe.Common.Payment;
using System.Collections.Generic;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Common.Payment
{
    public interface IPaymentViewModelBuilder
    {
        PaymentViewModel Build(OPContext opContext, PopOrderForm popOrderForm);
        PaymentViewModel Build(OPContext opContext, PopOrderForm popOrderForm, string selectedTab);
        PaymentViewModel Build(OPContext opContext, PopOrderForm popOrderForm, string selectedTab, string selectedPaymentMethod);
        PromotionViewModel BuildPromotionCode(PopOrderForm popOrderForm);
        PromotionViewModel BuildPromotionCode(string code, string description, string errorMessage);
        IEnumerable<TabPaymentMethodsModel> BuildTabs(OPContext opContext, PopOrderForm popOrderForm, string parameterName, string totalGlobal);
        IEnumerable<UnavailablePaymentViewModel> BuildUnavailableTabs(OPContext opContext, PopOrderForm popOrderForm, IEnumerable<TabPaymentMethodsModel> visibleTabs);
        MessageViewModel BuildPaymentMessages(OPContext opContext, PopOrderForm popOrderForm);
    }
}
