﻿namespace FnacDirect.OrderPipe.Base.BaseModel
{
    public enum ShippingMethodEvaluationType
    {
        Unknown,
        Postal,
        Shop,
        Relay
    }
}
