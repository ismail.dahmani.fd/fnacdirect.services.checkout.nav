﻿using FnacDirect.Technical.Framework.Web;
using FnacDirect.Technical.Framework.Web.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Controllers.Pop
{
    public class PopRazorViewEngine : LocalizationRazorViewEngine
    {
        public PopRazorViewEngine()
            : base("OrderPipe/Pop",
                   "OrderPipe/Shared")
        {
        }
    }
}
