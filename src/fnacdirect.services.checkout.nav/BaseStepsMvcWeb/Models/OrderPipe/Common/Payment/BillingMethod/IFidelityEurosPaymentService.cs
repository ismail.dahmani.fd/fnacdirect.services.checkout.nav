using FnacDirect.OrderPipe.Base.Model.Facets;

namespace FnacDirect.OrderPipe.Base.Business.Payment
{
    public interface IFidelityEurosPaymentService
    {
        bool IsAllowed(IGotLineGroups iGotLineGroups, IGotBillingInformations iGotBillingInformations, int delayAfterExpiration, decimal multipleAmount, bool disableECCVCheck = false);
    }
}
