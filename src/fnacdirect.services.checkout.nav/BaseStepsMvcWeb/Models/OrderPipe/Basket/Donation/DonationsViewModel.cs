﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Basket
{
    public class DonationsViewModel
    {
        public bool DisplayDonationPush { get; set; }

        public IEnumerable<DonationViewModel> Donations { get; set; }
    }
}
