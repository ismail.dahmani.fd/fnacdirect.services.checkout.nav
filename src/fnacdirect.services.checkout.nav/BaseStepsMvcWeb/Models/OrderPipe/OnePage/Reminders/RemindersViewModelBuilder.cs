using FnacDirect.OrderPipe.Base.BaseModel.Model;
using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.OrderPipe.Base.Model.LineGroups;
using FnacDirect.OrderPipe.Base.Proxy.Appointment;
using FnacDirect.OrderPipe.Base.Proxy.Membership;
using FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Common;
using FnacDirect.OrderPipe.Core.Services;
using FnacDirect.Technical.Framework.Web.Mvc;
using System.Collections.Generic;
using System.Linq;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Pop.Models.Common
{
    public class RemindersViewModelBuilder : IRemindersViewModelBuilder
    {
        private readonly IAppointmentService _appointmentService;
        private readonly IPipeParametersProvider _pipeParametersProvider;
        private readonly IAdherentCardArticleService _adherentCardArticleService;
        private readonly IApplicationContext _applicationContext;

        public RemindersViewModelBuilder(IAppointmentService appointmentService,
            IPipeParametersProvider pipeParametersProvider, IAdherentCardArticleService adherentCardArticleService
            , IApplicationContext applicationContext)
        {
            _appointmentService = appointmentService;
            _pipeParametersProvider = pipeParametersProvider;
            _adherentCardArticleService = adherentCardArticleService;
            _applicationContext = applicationContext;
        }

        public RemindersViewModel Build(PopOrderForm popOrderForm)
        {
           var remindersViewModel = BuildReminders(popOrderForm);

            return remindersViewModel;
        }

        private RemindersViewModel BuildReminders(PopOrderForm popOrderForm)
        {
            var remindersViewModel = new RemindersViewModel();

            var articles = popOrderForm.LineGroups.GetArticles();

            var eccvTypeIds = _pipeParametersProvider.GetAsRangeSet<int>("eccv.typeid");
            var hasECCV = articles.Any(a => a.TypeId.HasValue && eccvTypeIds.Contains(a.TypeId.Value));
            remindersViewModel.ECCV = hasECCV;



            var hasDematSoft = popOrderForm.LineGroups.Any(l => l.HasDematSoftArticles);
            remindersViewModel.DematSoft = hasDematSoft;

            var hasAdherentCard = articles.Any(a => a.ProductID.HasValue && _adherentCardArticleService.IsAdherentCard(a.ProductID.Value));
            remindersViewModel.AdherentCard = hasAdherentCard;

            var appointmentTypeIds = _appointmentService.GetAppointmentArticleTypeIds();
            var hasAppointment = appointmentTypeIds != null && articles.Any(a => a.TypeId.HasValue && appointmentTypeIds.Contains(a.TypeId.Value));
            remindersViewModel.Appointment = hasAppointment;

            var hasService = articles.OfType<Service>().Any();
            remindersViewModel.Service = hasService;

            var aboIlliData = popOrderForm.GetPrivateData<AboIlliData>();
            var hasExpressPlus = aboIlliData != null && aboIlliData.HasAboIlliInBasket;
            remindersViewModel.ExpressPlus = hasExpressPlus;

            if (int.TryParse(_applicationContext.GetAppSetting("DonationTypeId"), out var donationTypeId))
            {
                var hasDonation = articles.Any(a => a.TypeId.HasValue && donationTypeId == a.TypeId.Value);
                remindersViewModel.Donation = hasDonation;
            }

            var standardArticles = popOrderForm.LineGroups.GetArticlesOfType<StandardArticle>();

            var hasEbook = standardArticles.Any(a => a.IsEbook);
            remindersViewModel.Ebook = hasEbook;

            //We should put this id in a config ? appSettings ?
            var supportIdGamingSubscription = 3848;
            var hasGamingSubscription = standardArticles.Any(a => a.SupportId == supportIdGamingSubscription);
            remindersViewModel.GamingSubscription = hasGamingSubscription;

            return remindersViewModel;
        }
    }
}
