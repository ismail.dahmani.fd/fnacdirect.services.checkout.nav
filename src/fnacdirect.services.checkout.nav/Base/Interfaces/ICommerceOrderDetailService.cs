using FnacDirect.OrderPipe.Base.Model;
using System;

namespace FnacDirect.OrderPipe.Base.Business
{
    public interface ICommerceOrderDetailService
    {
        void AddOrderDetail(int orderPk, DateTime orderDate, int shippingAddressId, StandardArticle article, PricesOrderDetail prices, string affiliate, int billingMethodId, int? master, int refGu, int splitLevel, DateTime? dtTheoExpDate, bool? isOptinWarranty, int shippingMethodId);
        void AddOrderDetailLink(int orderDetailFkMaster, int orderDetailFkChild);
        void AddOrderDetailAltSellerInfo(int orderDetailFk, decimal? bestOfferPrice, int? totalOfferCount);
        void AddOrderDetailInfo(int orderDetailFk, string xmldata);
        void AddOrderDetailInfoForRecruitingCardNumber(int orderDetailFk, string cardNumber);
        void AddOrderDetailAddOn(int orderDetailFk, int typeArticle, int itemGroup, bool flagVenteService, string partnerName, NumericalAddOn numAddOn, string koboUserId);
        void AddOrderDetailInfoAdh(int orderDetailFk, string xmldata);
        void AddOrderDetailInfoForPosa(int orderDetailFk, string posaCode);
        void AddOrderDetailInfoForFreeBasket(int orderDetailFk, int type, string contractNumber);
        void AddOrderDetailInfoForLongTermRental(int orderDetailFk, string contractNumber);
        void AddOrderDetailInfoForGPSubscription(int orderDetailFk, string contractNumbers);
        void AddOrderDetailInfoForOpenCellSubscription(int orderDetailFk, string subscriptionNumber);
        void AddOrderDetailInfo(int orderDetailFk, OrderDetailInfoType type, string info);
    }
}
