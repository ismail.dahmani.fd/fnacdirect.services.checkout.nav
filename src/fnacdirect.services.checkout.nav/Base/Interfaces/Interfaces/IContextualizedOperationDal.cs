﻿using FnacDirect.Contracts.Online.Model;
using System.Collections.Generic;

namespace FnacDirect.OrderPipe.Base.DAL
{
    public interface IContextualizedOperationDal
    {
        ContextualizedOperation Get(string publicId);
        ContextualizedOperationToken Get(string publicId, string token);
        IDictionary<int, IEnumerable<ArticleReference>> GetExclusiveArticles();
        IEnumerable<ArticleReference> GetArticles(int contextualizedOperationId);
        void AssociateContextualizedOperationTokenToMainOrder(int tokenId, int mainOrderId);
    }

    public class ContextualizedOperation
    {
        public int OperationId { get; set; }
        public int? Origin { get; set; }
    }

    public class ContextualizedOperationToken : ContextualizedOperation
    {
        public int TokenId { get; set; }
    }
}
