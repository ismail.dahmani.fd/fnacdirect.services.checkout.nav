using FnacDirect.Membership.Model.Entities;
using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.OrderPipe.Base.Model.Facets;
using FnacDirect.OrderPipe.Core.Services;
using FnacDirect.Technical.Framework.CoreServices;
using FnacDirect.Technical.Framework.Web.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;

namespace FnacDirect.OrderPipe.Base.Business.Payment
{
    public class FidelityEurosPaymentService : IFidelityEurosPaymentService
    {
        private readonly IUserExtendedContextProvider _userExtendedContextProvider;
        private readonly ICheckArticleService _checkArticleService;
        private readonly ISwitchProvider _switchProvider;
        private readonly IPaymentEligibilityService _paymentEligibilityService;

        public FidelityEurosPaymentService(IUserExtendedContextProvider userExtendedContextProvider,
            ICheckArticleService checkArticleService,
            ISwitchProvider switchProvider,
            IPaymentEligibilityService paymentEligibilityService)
        {
            _userExtendedContextProvider = userExtendedContextProvider;
            _checkArticleService = checkArticleService;
            _switchProvider = switchProvider;
            _paymentEligibilityService = paymentEligibilityService;
        }

        public bool IsAllowed(IGotLineGroups iGotLineGroups, IGotBillingInformations iGotBillingInformations, int delayAfterExpiration, decimal multipleAmount, bool disableEccvCheck = false)
        {
            var rules = _paymentEligibilityService.GetRules("orderpipe.pop.payment.fidelityeuros.eligibilityrules",
                disableEccvCheck: disableEccvCheck);

            if (!_paymentEligibilityService.IsAllowed(iGotLineGroups, rules))
            {
                return false;
            }

            var userExtendedContext = _userExtendedContextProvider.GetCurrentUserExtendedContext();
            var customer = userExtendedContext.Customer;

            var behavior = ConfigurationManager.Current.AppSettings.Settings["front.account.adherent.behavior"].Value;
            if (!string.Equals(behavior, "siebel", StringComparison.OrdinalIgnoreCase)
                && !string.Equals(behavior, "fib", StringComparison.OrdinalIgnoreCase))
            {
                return false;
            }

            var isFib = string.Equals(behavior, "fib", StringComparison.OrdinalIgnoreCase);
            if (iGotLineGroups.IsOnlyNumerical() && !isFib)
            {
                return false;
            }

            if (iGotBillingInformations.GlobalPrices.TotalPriceEur.GetValueOrDefault() < multipleAmount)
            {
                return false;
            }

            if (!customer.IsValidAdherent
                 || customer.MembershipContract == null
                 || (customer.MembershipContract.AdhesionStatus == AdhesionStatus.OutDated && customer.MembershipContract.AdhesionEndDate.AddMonths(delayAfterExpiration) > DateTime.Now))
            {
                return false;
            }

            if (iGotLineGroups.LineGroups.GetArticlesOfType<MarketPlaceArticle>().Any()
                && _switchProvider.IsEnabled("orderpipe.pop.payment.filtersecondaryformarketplace"))
            {
                return false;
            }

            if (_checkArticleService.HasSensibleSupportId(iGotLineGroups.Articles))
            {
                return false;
            }

            return true;
        }
    }
}
