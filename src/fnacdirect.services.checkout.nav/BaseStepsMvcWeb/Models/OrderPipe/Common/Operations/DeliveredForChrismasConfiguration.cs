using System;
using System.Collections.Generic;
using System.Configuration;

namespace FnacDirect.OrderPipe.Base.Business.Operations
{
    /// <summary>
    /// Configuration pour livré à temps pour noel 
    /// </summary>
    public class DeliveredForChrismasConfiguration : ConfigurationSection 
    {
      
        /// <summary>
        /// Les catalogues élligibles
        /// </summary>
        public IEnumerable<int> ElligibleCatalogs { get; set; }
        /// <summary>
        /// Les familles logistiques exclues
        /// </summary>
        public IEnumerable<int> ExcludeFamilies { get; set; }
        /// <summary>
        /// ItemType des téléviseurs
        /// </summary>
        public int TvItemTypeId { get; set; }
        /// <summary>
        /// Date du début du split gratuit et automatique
        /// </summary>
        public DateTime? SplitStartDate { get; set; }
        /// <summary>
        /// Date du fin du split gratuit et automatique
        /// </summary>
        public DateTime? SplitEndDate { get; set; }
        /// <summary>
        /// Règles d'affichage
        /// </summary>
        public IEnumerable<DeliveredForChrismasRule> DisplayRules { get; set; }
    }
}
