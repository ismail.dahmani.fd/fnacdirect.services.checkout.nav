using System.Collections.Generic;

namespace FnacDirect.OrderPipe.Base.BaseModel.Model
{
    public class ClickAndCollectData : GroupData
    {
        private readonly IList<int> _pridsAvailable
            = new List<int>();

        public ClickAndCollectData()
        {

        }

        public IEnumerable<int> PridsAvailable
        {
            get
            {
                return _pridsAvailable;
            }
        }

        public void ClearAvailabilities()
        {
            _pridsAvailable.Clear();
        }

        public void AddAvailable(int prid)
        {
            if(!_pridsAvailable.Contains(prid))
            {
                _pridsAvailable.Add(prid);
            }
        }
    }
}
