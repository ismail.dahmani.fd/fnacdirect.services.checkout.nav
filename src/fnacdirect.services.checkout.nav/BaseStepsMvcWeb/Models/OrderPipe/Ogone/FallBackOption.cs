using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FnacDirect.OrderPipe.Base.Model
{
    public enum FallBackOption
    {
        FallBackECommerce,
        FallBackECommerceWithoutAlias,
        NoFallBack
    }

}
