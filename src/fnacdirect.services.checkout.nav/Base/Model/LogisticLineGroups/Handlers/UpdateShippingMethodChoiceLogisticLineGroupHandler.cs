using FnacDirect.OrderPipe.Base.Business.LogisticLineGroups.Internals.Handlers;
using FnacDirect.OrderPipe.Base.LineGroups;
using System.Collections.Generic;
using System.Linq;

namespace FnacDirect.OrderPipe.Base.Business.LogisticLineGroups.Handlers
{
    public class UpdateShippingMethodChoiceLogisticLineGroupHandler : ILogisticLineGroupGeneratedHandler
    {
        public void OnGenerated(IEnumerable<ILineGroupHeader> source, IEnumerable<ILogisticLineGroup> logisticLineGroups)
        {
            foreach (var logisticLineGroup in logisticLineGroups)
            {
                if (logisticLineGroup.SelectedShippingChoice?.Groups == null)
                {
                    continue;
                }

                var groupsTmp = logisticLineGroup.SelectedShippingChoice.Groups.ToList();

                foreach (var group in logisticLineGroup.SelectedShippingChoice.Groups)
                {
                    if (logisticLineGroup.LogisticTypes.All(l => l.Index != group.Id))
                    {
                        groupsTmp.Remove(group);
                    }
                }

                logisticLineGroup.SelectedShippingChoice.Groups = groupsTmp;
            }
        }
    }
}
