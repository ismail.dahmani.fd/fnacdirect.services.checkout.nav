using FnacDirect.Subscriptions.Models;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace FnacDirect.OrderPipe.Base.BaseModel.Model
{
    public class SubscriptionsData : GroupData
    {
        public List<Subscription> Subscriptions { get; set; }
    }
}
