﻿
namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.OrderThankYou
{
    public class AdvertisingViewModel
    {
        public string ContactEmail { get; set; }

        public decimal TotalPrice { get; set; }

        public bool DisplayZeGive { get; set; }
    }
}
