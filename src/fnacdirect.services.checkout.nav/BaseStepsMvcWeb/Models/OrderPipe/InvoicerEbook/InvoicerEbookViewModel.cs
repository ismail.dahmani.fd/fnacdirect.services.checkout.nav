﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.InvoicerEbook
{
    public class InvoicerEbookViewModel
    {
        public string OrderDate { get; set; }

        public string OrderUserReference { get; set; }

        public Guid OrderReference { get; set; }
    }
}
