using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FnacDirect.OrderPipe.FDV.Model
{
    public class SellerAuthenticationResult
    {
        public bool IsSuccess { get; set; }
        public string ErrorCode { get; set; }
        public string ErrorDescription { get; set; }
        public string SellerFirstName { get; set; }
        public string SellerLastName { get; set; }
        public string SellerComment { get; set; }
        public string SellerMatricule { get; set; }
        public string SellerGroup { get; set; }

        public SellerAuthenticationResult() { }

        private SellerAuthenticationResult(bool isSuccess)
        {
            IsSuccess = isSuccess;
        }

        public SellerAuthenticationResult(bool isSuccess, string errorCode, string errorDescription)
            : this(isSuccess)
        {
            ErrorCode = errorCode;
            ErrorDescription = errorDescription;
        }

        public SellerAuthenticationResult(bool isSuccess, string sellerFirstName, string sellerLastName, string sellerMatricule, string sellerGroup)
            : this(isSuccess)
        {
            SellerFirstName = sellerFirstName;
            SellerLastName = sellerLastName;
            SellerMatricule = sellerMatricule;
            SellerGroup = sellerGroup;
        }
    }
}
