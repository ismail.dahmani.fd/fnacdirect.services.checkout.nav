using System;
using System.Linq;
using FnacDirect.Customer.Model;
using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.OrderPipe.Base.Model.LineGroups;
using System.Collections.Generic;
using FnacDirect.Technical.Framework.Utils;
using FnacDirect.Technical.Framework.Web.Mvc;

namespace FnacDirect.OrderPipe.Base.Business.Billing
{
    public class BillingAddressElegibilityService : IBillingAddressElegibilityService
    {
        private readonly OPContext _opContext;
        private readonly RangeSet<int> _servicebillingkey;
        private readonly RangeSet<int> _servicebillingkeyMonaco;
        private readonly INumericalBusiness _numericalBusiness;
        private readonly bool _executeSpecificBehaviourForES;

        public BillingAddressElegibilityService(OPContext opContext, INumericalBusiness numericalBusiness, IApplicationContext applicationContext)
        {
            _opContext = opContext;
            _numericalBusiness = numericalBusiness;
            _servicebillingkey = RangeSet.Parse<int>(opContext.Pipe.GlobalParameters.Get("services.billing.keys", string.Empty));
            _servicebillingkeyMonaco = RangeSet.Parse<int>(opContext.Pipe.GlobalParameters.Get("services.billing.Monaco.keys", string.Empty));
            _executeSpecificBehaviourForES = applicationContext.IsLocalCountries(FlagsCountries.ES);
        }

        public IEnumerable<AddressEntity> GetElegibleAddress(IEnumerable<ILineGroup> lineGroups, List<AddressEntity> addresses)
        {
            var articles = lineGroups.SelectMany(lg => lg.Articles).ToList();

            var elegibleAddresses = addresses
                .Where(a => IsValidForRetailerMarketCountry(articles, a)
                            && IsValidForService(articles, a)).ToList();

            return elegibleAddresses;
        }

        protected internal List<BillingAddress> ConvertAddressEntityToBillingAddress(List<AddressEntity> addresses)
        {
            return addresses.Select(addressEntity => new BillingAddress(addressEntity)).ToList();
        }

        //TODO: Can be refactored to many if imbricated
        protected internal bool IsValidForService(List<Article> articles, AddressEntity address)
        {
            if (!articles.OfType<StandardArticle>().Any())
            {
                return true;
            }

            var addressValid = address.CountryId == (int)CountryEnum.France
                            || (_executeSpecificBehaviourForES && address.CountryId == (int)CountryEnum.Espagne);

            var addressValidWithMonaco = addressValid || address.CountryId == (int)CountryEnum.Monaco;

            foreach (var article in articles.OfType<StandardArticle>())
            {
                if (!article.Services.OfType<StandardArticle>().Any())
                {
                    return true;
                }

                foreach (var service in article.Services.OfType<BaseArticle>())
                {
                    if (_servicebillingkey.Contains(Convert.ToInt32(service.TypeId)))
                    {
                        if (_servicebillingkeyMonaco.Contains(Convert.ToInt32(service.TypeId)))
                        {
                            if (addressValidWithMonaco)
                            {
                                return true;
                            }
                        }
                        else
                        {
                            if (addressValid)
                            {
                                return true;
                            }
                        }
                    }
                    else
                    {
                        //Cas specifique pour FSH afin d'autoriser l'adresse meme si le panier contient des services
                        //dont le type non referencés. (exemple type 16000)
                        return true;
                    }
                }
            }
            return false;
        }

        protected internal bool IsValidForRetailerMarketCountry(List<Article> articles, AddressEntity address)
        {
            var isAuthorized = true;

            foreach (var article in articles.OfType<StandardArticle>())
            {
                if (article.NumericalAddOn != null)
                {
                    isAuthorized &= _numericalBusiness.IsAuthorizedToSell(address.CountryId,
                        _opContext.SiteContext.MarketId,
                        article.NumericalAddOn.RetailerId.GetValueOrDefault());
                }
            }

            return isAuthorized;
        }
    }
}
