﻿using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.OrderPipe.Base.Model.LineGroups;

namespace FnacDirect.OrderPipe.Base.Business.Stock
{
    public interface IUnavailableArticleHandler
    {
        bool Handle(ILineGroup parentLineGroup, StandardArticle standardArticle);
    }
}
