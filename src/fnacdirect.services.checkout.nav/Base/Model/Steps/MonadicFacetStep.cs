﻿using System;

namespace FnacDirect.OrderPipe
{
    public abstract class MonadicFacetStep<TFacet, TInput, TOutput> : Step
        where TFacet : class, IOF
    {
        protected internal abstract TInput Bind(OPContext opContext, TFacet facet);

        protected internal abstract TFacet Returns(TFacet facet, TOutput output);

        public sealed override StepReturn Check(OPContext opContext)
        {
            return Check(Bind(opContext, opContext.OF as TFacet));
        }

        public sealed override StepReturn Process(OPContext opContext)
        {
            var input = Bind(opContext, opContext.OF as TFacet);

            var processResult = Process(input);

            if (processResult.Output.HasValue)
            {
                opContext.OF = Returns(opContext.OF as TFacet, processResult.Output.Value);
            }

            return processResult.StepReturn;
        }

        protected internal virtual StepReturn Check(TInput input)
        {
            return StepReturn.Process;
        }

        protected internal virtual ProcessResult Process(TInput input)
        {
            return Next();
        }

        protected ProcessResult Next(TOutput output)
        {
            return new ProcessResult(StepReturn.Next, output);
        }

        protected ProcessResult Next()
        {
            return new ProcessResult(StepReturn.Next);
        }

        protected internal class ProcessResult
        {
            private readonly StepReturn _stepReturn;
            private readonly Maybe<TOutput> _output;

            public ProcessResult(StepReturn stepReturn)
            {
                _stepReturn = stepReturn;
                _output = Maybe<TOutput>.Empty();
            }

            public ProcessResult(StepReturn stepReturn, TOutput output)
            {
                _stepReturn = stepReturn;
                _output = output;
            }

            public StepReturn StepReturn
            {
                get { return _stepReturn; }
            }

            public Maybe<TOutput> Output
            {
                get { return _output; }
            }
        }
    }
}
