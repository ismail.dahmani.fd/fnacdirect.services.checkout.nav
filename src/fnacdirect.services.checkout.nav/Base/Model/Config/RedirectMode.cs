﻿namespace FnacDirect.OrderPipe.Config
{
    public enum RedirectMode
    {
        Auto,
        Redirect,
        Transfert,
        AlwaysTransfert
    }
}
