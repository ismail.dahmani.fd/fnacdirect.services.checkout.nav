﻿using FnacDirect.Contracts.Online.Model;
using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.Technical.Framework.Web.Mvc;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Common.Payment.BillingMethodFactories
{
    public class CheckViewModelFactory : BasePaymentViewModelFactory<CheckBillingMethod>
    {
        public override BillingMethodViewModel Get(string tabName, Base.Model.PaymentMethod paymentMethod, OPContext opContext, PopOrderForm popOrderForm)
        {
            return new CheckMethodViewModel()
            {
                Display = true
            };
        }
    }
}
