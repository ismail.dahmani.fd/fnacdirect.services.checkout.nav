using System.Linq;
using System.Web;
using System.Web.Routing;
using FnacDirect.OrderPipe.Config;
using FnacDirect.OrderPipe.CoreRouting;
using FnacDirect.Technical.Framework.Web;
using FnacDirect.Technical.Framework.Web.UrlMgr;

namespace FnacDirect.OrderPipe.CoreMvcWeb.Routing
{
    public class MvcOrderPipeResetStrategy : OrderPipeResetStrategyBase<MvcPipeDescriptor>
    {
        private readonly IHttpContextFactory _httpContextFactory;
        private readonly IUrlManager _urlManager;        

        public MvcOrderPipeResetStrategy(IUrlManager urlManager,
                                         IHttpContextFactory httpContextFactory)
        {
            _urlManager = urlManager;
            _httpContextFactory = httpContextFactory;
        }

        public override string DoResetFor(OPContext opContext, bool applyRedirect)
        {
            InnerDoReset(opContext);

            var httpContext = _httpContextFactory.Create();

            var routeData = RouteTable.Routes.GetRouteData(httpContext);
            var requestContext = new RequestContext(httpContext, routeData);

            var urlHelper = new System.Web.Mvc.UrlHelper(requestContext);

            var routeValueDictionnary = new RouteValueDictionary(httpContext.Request.QueryString.AllKeys
                            .Where(p => p != "step" && p != "reset" && p != "context")
                            .ToDictionary(p => p, p => (object)httpContext.Request.QueryString[p]));

            if (!routeValueDictionnary.ContainsKey("pipe")
                && routeData.Values.ContainsKey("pipe"))
            {
                routeValueDictionnary.Add("pipe", routeData.Values["pipe"]);
            }

            var page = _urlManager.Sites.OrderPipe.GetPage("Root").DefaultUrl;

            foreach (var paramter in routeValueDictionnary)
            {
                page.WithParam(paramter.Key, paramter.Value.ToString());
            }

            SanitizeUrlWriter(page);

            var url = page.ToString();

            if (applyRedirect)
            {
                httpContext.Response.Redirect(url);
            }

            return url;
        }

        protected virtual void InnerDoReset(OPContext opContext)
        {
            opContext.Reset();
        }

        protected virtual void SanitizeUrlWriter(UrlWriter urlWriter)
        {
            if (urlWriter.ContainsParameter("reset") != null)
            {
                urlWriter.WithoutParam("reset");
            }
        }
    }
}
