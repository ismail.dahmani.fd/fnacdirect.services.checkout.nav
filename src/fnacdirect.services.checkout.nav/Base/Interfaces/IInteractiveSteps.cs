using FnacDirect.OrderPipe.Config;

namespace FnacDirect.OrderPipe
{
    /// <summary>
    /// Interface de base des �tapes interactive
    /// </summary>
    public interface IInteractiveStep : IStep
    {
        /// <summary>
        /// Indique vers quelle interface interactive l'utilisateur doit �tre dirig�e
        /// </summary>
        UIDescriptor UIDescriptor { get; set; }

        bool SecurityAgnostic { get; set; }

        bool Secured { get; set; }

        bool Authenticated { get; set; }

        bool IsDirectDefault { get; set; }

        bool CanUseDirectAccess { get; set; }

        string Pass2Shortcut { get; set; }

        bool ExitOnContinue { get; set; }

        /// <summary>
        /// Vrai si l'�tape interactive peut �tre acc�d�e directement (via le chemin de fer du pipe)
        /// </summary>
        bool IsDirect(OPContext opContext);

        /// <summary>
        /// R�initialisation de l'�tape, appell�e lors d'un acc�s direct
        /// </summary>
        void Reset(OPContext opContext);

        NoDirectAccessBehavior NoDirectAccessBehavior { get; set; }
    }
}