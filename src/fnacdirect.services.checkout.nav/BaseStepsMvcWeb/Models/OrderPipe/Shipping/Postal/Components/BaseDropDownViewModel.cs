namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.OnePage.Address
{
    public abstract class BaseDropDownViewModel
    {
        public int Id { get; set; }
        public string Label { get; set; }
    }
}
