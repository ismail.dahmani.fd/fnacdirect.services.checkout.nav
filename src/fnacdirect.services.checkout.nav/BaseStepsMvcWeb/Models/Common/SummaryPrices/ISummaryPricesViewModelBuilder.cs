using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Basket;
using FnacDirect.Technical.Framework.Web.Mvc;
using System.Collections.Generic;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Common.Payment
{
    public interface ISummaryPricesViewModelBuilder
    {
        ReducedPriceViewModel BuildReducedPriceBook5pc(PopOrderForm popOrderForm, decimal total);

        string BuildDifferedSavedAmount(PopOrderForm popOrderForm);

        string BuildTotalReduced(PopOrderForm popOrderForm, decimal oldTotalCost, decimal totalCost, bool withVAT = true);

        PriceDetailsViewModel BuildTotalReducedModel(PopOrderForm popOrderForm, decimal oldTotalCost, decimal totalCost, bool withVAT = true);
    }
}
