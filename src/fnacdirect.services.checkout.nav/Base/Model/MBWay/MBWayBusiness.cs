using FnacDirect.Customer.BLL;
using FnacDirect.MBWay.Business;
using FnacDirect.MBWay.Model;
using FnacDirect.OrderPipe.Base.Business.Configuration;
using FnacDirect.Technical.Framework.Web;
using FnacDirect.Technical.Framework.Web.Mvc;
using System;
using System.Threading.Tasks;

namespace FnacDirect.OrderPipe.Base.Business
{
    public class MBWayBusiness : IMBWayBusiness
    {
        private readonly IApplicationContext _applicationContext;
        private readonly IUrlManager _urlManager;
        private readonly IExternalCustomerService _externalCustomerService;
        private readonly IMBWayTransactionServices _mbwayTransactionServices;
        private readonly IMBWayConfigurationProvider _mbwayConfigurationProvider;

        public MBWayBusiness(IApplicationContext applicationContext, IUrlManager urlManager,
            IExternalCustomerService externalCustomerService,
            IMBWayTransactionServices mbwayTransactionServices, IMBWayConfigurationProvider mbwayConfigurationProvider)
        {
            _applicationContext = applicationContext;
            _urlManager = urlManager;
            _externalCustomerService = externalCustomerService;
            _mbwayTransactionServices = mbwayTransactionServices;
            _mbwayConfigurationProvider = mbwayConfigurationProvider;
        }

        public string GetMBWayUrl()
        {
            var configuration = _mbwayConfigurationProvider.Get();
            var baseMBWayApiUrl = configuration.MBWayApiUrl;

            return baseMBWayApiUrl;
        }

        public MBWayConfiguration GetMBWayApiConfiguration()
        {
            var mbwayConfiguration = new MBWayConfiguration();

            var configuration = _mbwayConfigurationProvider.Get();
            mbwayConfiguration.MBWayApiUrl = configuration.MBWayApiUrl;
            mbwayConfiguration.SqlCommerce = configuration.SqlCommerce;
            mbwayConfiguration.EntityId = configuration.EntityId;
            mbwayConfiguration.Currency = configuration.Currency;
            mbwayConfiguration.PaymentType = configuration.PaymentType;
            mbwayConfiguration.PaymentBrand = configuration.PaymentBrand;
            mbwayConfiguration.AccountId = configuration.AccountId;
            mbwayConfiguration.ShopperResultUrl = configuration.ShopperResultUrl;
            mbwayConfiguration.Authorization = configuration.Authorization;

            return mbwayConfiguration;
        }

        public void AddMBWayTransaction(MBWayTransaction transaction)
        {
            var transactionIsValid = transaction.orderId != 0
                && transaction.transactionReference != Guid.Empty;

            if (transactionIsValid)
                _mbwayTransactionServices.AddMBWayTransaction(transaction);
        }
    }
}
