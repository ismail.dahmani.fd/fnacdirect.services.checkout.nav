﻿using FnacDirect.OrderPipe.Base.Model.UserInformations;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Kobo
{
   

    public class KoboMergeAccountBuilder : IKoboMergeAccountBuilder
    {
        public KoboMergeAccountViewModel Build(UserContextInformations userContext)
        {
            return new KoboMergeAccountViewModel
            {
                Email = userContext.ContactEmail,
                UserReference = userContext.UID
            };
        }
    }
}
