using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.OrderPipe.Base.Model.Facets;
using FnacDirect.OrderPipe.Base.Model.LineGroups;
using FnacDirect.OrderPipe.Core.Services;
using FnacDirect.Technical.Framework.Utils;
using System.Collections.Generic;
using System.Linq;

namespace FnacDirect.OrderPipe.Base.Business.Payment
{
    public class BankPaymentService : IBankPaymentService
    {
        private readonly ICheckOriginService _checkOriginService;
        private readonly IPaymentEligibilityService _paymentEligibilityService;

        private readonly decimal _minimumAmountNoVat;
        private readonly decimal _bankTransferAmountLimit;

        private readonly bool _allowExpress;
        private readonly bool _allowCoursier;
        private readonly RangeSet<int> _expressShippingMethodId;
        private readonly RangeSet<int> _coursierShippingMethodIds;

        public BankPaymentService( IPipeParametersProvider pipeParametersProvider,
            ICheckOriginService checkOriginService,
            IPaymentEligibilityService paymentEligibilityService)
        {
            _checkOriginService = checkOriginService;
            _paymentEligibilityService = paymentEligibilityService;

            _bankTransferAmountLimit = pipeParametersProvider.Get("payment.transfert.amount.limit", 100m);
            _minimumAmountNoVat = pipeParametersProvider.Get("basket.amountnovat.grather.than", 140m);

            _expressShippingMethodId = RangeSet.Parse<int>(pipeParametersProvider.Get("shippingmethod.express", "12"));
            _coursierShippingMethodIds = RangeSet.Parse<int>(pipeParametersProvider.Get("shippingmethod.coursier", "0"));
            _allowExpress = pipeParametersProvider.Get("check.allow.express.shippingmethod", false);
            _allowCoursier = pipeParametersProvider.Get("check.allow.coursier.shippingmethod", false);
        }

        public bool IsBankAllowed(IGotLineGroups iGotLineGroups, IGotUserContextInformations iGotUserContextInformations, IGotShippingMethodEvaluation iGotShippingMethodEvaluation)
        {
            var rules = _paymentEligibilityService.GetRules("orderpipe.pop.payment.bank.eligibilityrules");

            if (!_paymentEligibilityService.IsAllowed(iGotLineGroups, rules))
            {
                return false;
            }

            if (iGotLineGroups.HasMarketPlaceArticle())
            {
                return false;
            }

            var lngroup = iGotLineGroups.LineGroups.FirstOrDefault();
            var gprice = lngroup?.GlobalPrices;

            return IsAllowed(iGotLineGroups.LineGroups, gprice, _bankTransferAmountLimit, iGotUserContextInformations, iGotShippingMethodEvaluation);
        }

        public bool IsCheckAllowed(IGotBillingInformations iGotBillingInformations, IGotLineGroups iGotLineGroups, IGotUserContextInformations iGotUserContextInformations, IGotShippingMethodEvaluation iGotShippingMethodEvaluation)
        {
            var rules = _paymentEligibilityService.GetRules("orderpipe.pop.payment.check.eligibilityrules");

            if (!_paymentEligibilityService.IsAllowed(iGotLineGroups, rules))
            {
                return false;
            }

            var gprice = iGotBillingInformations.GlobalPrices;

            return IsAllowed(iGotLineGroups.LineGroups, gprice, _minimumAmountNoVat, iGotUserContextInformations, iGotShippingMethodEvaluation);
        }

        private bool IsAllowed(IEnumerable<ILineGroup> lineGroups, GlobalPrices gprice, decimal minimumAmount, IGotUserContextInformations iGotUserContextInformations, IGotShippingMethodEvaluation iGotShippingMethodEvaluation)
        {
            if (IsPriceUnderMinimumAmount(gprice, minimumAmount, iGotUserContextInformations))
            {
                return false;
            }

            // si la commande est volumineuse, on fait sauter les règles ci-dessous
            var lngroup = lineGroups.FirstOrDefault();
            if (lngroup?.IsVoluminous ?? false)
            {
                return true;
            }

            if (IsExpressOrCoursierUsed(lineGroups, iGotShippingMethodEvaluation))
            {
                return false;
            }

            return true;
        }

        private bool IsPriceUnderMinimumAmount(GlobalPrices globalPrices, decimal minimumAmount, IGotUserContextInformations iGotUserContextInformations)
        {
            var totalPriceToCheck = _checkOriginService.IsFromFnacPro() ?
                globalPrices?.TotalPriceEurNoVat :
                globalPrices?.TotalPriceEur;

            var disableCheckAmount = _checkOriginService.IsFromFnacProAndCallCenter(iGotUserContextInformations);
            if (totalPriceToCheck < minimumAmount && !disableCheckAmount)
            {
                return true;
            }

            return false;
        }

        private bool IsExpressOrCoursierUsed(IEnumerable<ILineGroup> lineGroups, IGotShippingMethodEvaluation iGotShippingMethodEvaluation)
        {
            foreach (var lineGroup in lineGroups)
            {
                var maybeShippingMethodEvaluationGroup = iGotShippingMethodEvaluation.ShippingMethodEvaluation.GetShippingMethodEvaluationGroup(lineGroup.Seller.SellerId);

                if (maybeShippingMethodEvaluationGroup.HasValue)
                {
                    var shippingmethodId = maybeShippingMethodEvaluationGroup.Value.SelectedShippingMethodEvaluation?.SelectedShippingMethodId ?? 0;

                    if (!_allowExpress && _expressShippingMethodId.Contains(shippingmethodId))
                    {
                        return true;
                    }

                    if (!_allowCoursier && _coursierShippingMethodIds.Contains(shippingmethodId))
                    {
                        return true;
                    }
                }
            }

            return false;
        }
    }
}
