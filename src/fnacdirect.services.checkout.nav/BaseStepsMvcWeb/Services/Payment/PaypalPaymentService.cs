using FnacDirect.Context;
using FnacDirect.OrderPipe.Base.Model;

namespace FnacDirect.OrderPipe.Base.Business.Payment
{
    public class PaypalPaymentService : IPaypalPaymentService
    {
        private readonly IMaintenanceConfigurationService _maintenanceConfigurationService;
        private readonly IPaymentEligibilityService _paymentEligibilityService;


        public PaypalPaymentService(IMaintenanceConfigurationService maintenanceConfigurationService,
            IPaymentEligibilityService paymentEligibilityService)
        {
            _maintenanceConfigurationService = maintenanceConfigurationService;
            _paymentEligibilityService = paymentEligibilityService;
        }

        public bool IsAllowed(PopOrderForm popOrderForm)
        {
            var rules = _paymentEligibilityService.GetRules("orderpipe.pop.payment.paypal.eligibilityrules");

            if (!_paymentEligibilityService.IsAllowed(popOrderForm, rules))
            {
                return false;
            }

            return !_maintenanceConfigurationService.Paypal;
        }
    }
}
