using System;
using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Basket;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Membership
{
    public interface IMembershipViewModelBuilder
    {
        MembershipViewModel Build(PopOrderForm popOrderForm);

        MembershipBindCardViewModel BuildCardBindViewModel();

        MemberBirthDateViewModel BuildMemberBirthDateViewModel(OPContext opContext, PopOrderForm popOrderForm);

        Maybe<MembershipCardViewModel> BuildAdjustedCard(PopOrderForm popOrderForm);

        MembershipCardFormatViewModel BuildCardFormat(OPContext opContext, PopOrderForm popOrderForm);
    }
}
