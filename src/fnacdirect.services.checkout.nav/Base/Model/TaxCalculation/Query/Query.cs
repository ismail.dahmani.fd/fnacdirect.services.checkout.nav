using Module = FnacDirect.OrderPipe.TaxCalculation.Front.Models;

namespace FnacDirect.OrderPipe.Base.Model.TaxCalculation
{
    public class Query
    {
        public OrderContext OrderContext { get; set; }
    }

    public class OrderContext
    {
        public int MarketId { get; set; }
        public int? ReceptionCountryId { get; set; }
        public int BillingCountryId { get; set; }
        public bool IsProAddress { get; set; }
        public string SiteId { get; set; }
        public string PipeName { get; set; }
        public string CustomerTaxNumber { get; set; }

        public Module.OrderContext ToOrderContext()
        {
            return new Module.OrderContext
            {
                MarketId = MarketId,
                BillingCountryId = BillingCountryId,
                ReceptionCountryId = ReceptionCountryId,
                IsProfessionalAddress = IsProAddress,
                SiteId = SiteId,
                PipeName = PipeName,
                CustomerTaxNumber = CustomerTaxNumber
            };
        }
    }
}
