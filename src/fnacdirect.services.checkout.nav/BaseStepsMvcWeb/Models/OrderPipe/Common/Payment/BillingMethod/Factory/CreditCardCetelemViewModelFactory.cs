﻿using FnacDirect.OrderPipe.Base.Model;
using System.Linq;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Common.Payment.BillingMethodFactories
{
    public class CreditCardCetelemViewModelFactory : BasePaymentViewModelFactory<FnacCardCetelemBillingMethod>
    {
        public override BillingMethodViewModel Get(string tabName, PaymentMethod paymentMethod, OPContext opContext, PopOrderForm popOrderForm)
        {
            return new CreditCardCetelemMethodeViewModel()
            {
                CreditCardCetelem = popOrderForm.AvailableCredits.ToList(),
                Display = true
            };
        }
    }
}
