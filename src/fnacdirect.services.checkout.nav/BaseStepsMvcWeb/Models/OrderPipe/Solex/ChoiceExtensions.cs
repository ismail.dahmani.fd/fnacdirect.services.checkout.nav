using System.Collections.Generic;
using System.Linq;

namespace FnacDirect.OrderPipe.Base.Model.Solex
{
    /***
     * Afin de pouvoir mocker l'extension, nous sommes obligé de passer par une interface et une instance de la classe.
     */

    public interface IShippingChoiceExtension
    {
        ShippingChoice GetBestChoice(IEnumerable<ShippingChoice> choices, params int[] filterIds);
    }

    public class ShippingChoiceExtension : IShippingChoiceExtension
    {
        public ShippingChoice GetBestChoice(IEnumerable<ShippingChoice> choices, params int[] filterIds)
        {
            if (!choices.Any())
                return null;

            return choices
                .OrderBy(choice => choice.TotalPrice.Cost)
                .ThenByDescending(choice => choice.Weight)
                .FirstOrDefault(choice =>
                    !filterIds.Contains(choice.MainShippingMethodId)
                    && choice.SelectedChoice != null);
        }
    }

    public static class ShippingChoiceExtensionStatic
    {
        private static IShippingChoiceExtension _instance;
        public static IShippingChoiceExtension Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new ShippingChoiceExtension();
                }
                return _instance;
            }
            set
            {
                _instance = value;
            }
        }

        public static ShippingChoice GetBestChoice(this IEnumerable<ShippingChoice> choices, params int[] filterIds)
        {
            return Instance.GetBestChoice(choices, filterIds);
        }
    }
}
