
namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Basket
{
    public class LoginPopinViewModel
    {
        public string LoginLink { get; set; }
        public string RegisterLink { get; set; }
        public bool ShowPopin { get; set; }
        public bool ContainsRegistrationForm { get; set; }
        public bool IsLogged { get; set; }
    }
}
