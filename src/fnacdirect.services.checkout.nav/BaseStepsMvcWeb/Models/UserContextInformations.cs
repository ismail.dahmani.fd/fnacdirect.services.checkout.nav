using System.Text;
using System.Xml.Serialization;
using FnacDirect.Customer.Model;
using FnacDirect.IdentityImpersonation;
using FnacDirect.Technical.Framework.Web;

namespace FnacDirect.OrderPipe.Base.Model.UserInformations
{
    public class UserContextInformations : IUserContextInformations
    {
        public string UID { get; set; }

        [XmlIgnore]
        public W3Customer W3Customer { get; set; }

        [XmlIgnore]
        public FnacDirect.Contracts.Online.Model.Customer Customer { get; set; }

        [XmlIgnore]
        public CustomerEntity CustomerEntity { get; set; }

        [XmlIgnore]
        public int AccountId { get; set; }
        public string Identity { get; set; }

        public bool IsAdherent { get; set; }

        public bool IsRenewable { get; set; }

        public bool IsGamer { get; set; }

        public string ClientCard { get; set; }
        
        public string ContactEmail { get; set; }

        public string Gender { get; set; }        

        public string LastName { get; set; }

        public string FirstName { get; set; }

        public int BlackListCounter { get; set; }

        public int AccountStatus { get; set; }

        public string CellPhone { get; set; }

        public string KoboUserId { get; set; }

        public bool IsNewCustomer { get; set; }

        public string ZipCode { get; set; }

        [XmlIgnore]
        public int UserConfidence { get; set; }

        [XmlIgnore]
        public bool Scored { get; set; }

        [XmlIgnore]
        public int MailMessageFlag { get; set; }

        [XmlIgnore]
        public int ThankYouMessageFlag { get; set; }

        public string PlasticCardNumber { get; set; }

        [XmlIgnore]
        public IdentityImpersonator IdentityImpersonator { get; set; }

        /// <summary>
		/// Id technique du magasin calculé à partir du refGU dans l'IdentityImpersonator
		/// </summary>
		[XmlIgnore]
        public int StoreCodeId { get; set; }

        /// <summary>
        /// Booleen qui indique que le client a choisi le parcours Guest.
        /// </summary>
        public bool IsGuest { get; set; }

        public override string ToString()
        {
            var user = new StringBuilder();
            user.AppendLine($"Account id {AccountId}");
            user.AppendLine($"Email {ContactEmail}");
            user.AppendLine($"Identity {Identity}");
            user.AppendLine($"AccountStatus {AccountStatus}");
            user.AppendLine($"IsNewCustomer {IsNewCustomer}");
            return user.ToString();
        }
    }
}
