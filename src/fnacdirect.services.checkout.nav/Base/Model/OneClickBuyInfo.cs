using System;

namespace FnacDirect.OrderPipe.Base.Model
{
    public class OneClickBuyInfo
    {
        public bool IsOneClickBuyActivated { get; set; }

        public bool ExistOrder { get; set; }

        public bool IsOneClickBuyAuthenticatedOrder { get; set; }

        public bool CryptoUsed { get; set; }

        public DateTime? LastOrderBuyDate { get; set; }

        public bool IsOCBSessionValid { get; set; }

        public bool IsOCBCreditCardValid { get; set; }

        public bool IsOCBBinOK { get; set; }

        public bool IsOCBCreditCardValidatedUsedForPreviousOrder { get; set; }
    }
}
