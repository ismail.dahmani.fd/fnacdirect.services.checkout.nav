﻿using FnacDirect.OrderPipe.Base.Business.LogisticLineGroups.Internals.Unfolders;
using FnacDirect.OrderPipe.Base.Model;
using System.Collections.Generic;

namespace FnacDirect.OrderPipe.Base.Business.LogisticLineGroups.Unfolders
{
    public class DummyLogisticLineGroupUnfolder : BaseLogisticLineGroupUnfolder
    {
        public override IEnumerable<Article> Unfold(IEnumerable<Article> articles)
        {
            return articles;
        }
    }
}
