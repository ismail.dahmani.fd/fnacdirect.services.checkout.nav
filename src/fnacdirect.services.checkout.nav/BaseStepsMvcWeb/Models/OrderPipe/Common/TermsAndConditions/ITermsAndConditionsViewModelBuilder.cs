using FnacDirect.OrderPipe.Base.Model.LineGroups;
using System.Collections.Generic;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Basket
{
    public interface ITermsAndConditionsViewModelBuilder
    {
        TermsAndConditionsViewModel Build(IEnumerable<ILineGroup> lineGroups);
    }
}
