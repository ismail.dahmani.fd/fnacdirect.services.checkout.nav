﻿using FnacDirect.Appointment.Model;
using FnacDirect.Technical.Framework.Utils;

namespace FnacDirect.OrderPipe.Base.Proxy.Appointment
{
    public interface IFormAtHomeService
    {
        RangeSet<int> GetRelatedArticleTypeIds();
        void ConfirmSales(AppointmentRequest<FormAtHomeAppointment> appointmentRequest, string user);
    }
}
