using System;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Common.Shipping
{
    public abstract class ShippingNetworkViewModel
    {
        public Maybe<ISelectedShippingItem> Selected { get; set; }

        protected ShippingNetworkViewModel()
        {
            Selected = Maybe<ISelectedShippingItem>.Empty();
        }
    }

    public abstract class ShippingNetworkViewModel<T> : ShippingNetworkViewModel
        where T : ISelectedShippingItem
    {
        public bool CurrentMode { get; set; }

        public new Maybe<T> Selected { get; set; }

        public string DefaultShortEstimatedDeliveryDate { get; set; }

        protected ShippingNetworkViewModel()
        {
            Selected = Maybe<T>.Empty();
        }
    }
}
