using System.Collections.Generic;
using System.Linq;

namespace FnacDirect.OrderPipe.Base.Model.TaxCalculation
{
    public class TaxCalcuationLineGroupResult
    {
        private List<TaxCalculationGlobalPriceResult> _globalPrices { get; }
        public List<TaxCalculationResultItem> TaxCalculationResultItems { get; set; }
        public TaxInfos TaxInfos { get; set; }

        public TaxCalcuationLineGroupResult()
        {
            _globalPrices = new List<TaxCalculationGlobalPriceResult>();
            TaxCalculationResultItems = new List<TaxCalculationResultItem>();
        }

        //Create Read Update GlobalPrices
        public TaxCalculationGlobalPriceResult GetGlobalPriceByType(GlobalPriceType globalPriceType)
        {
            return _globalPrices.FirstOrDefault(gp => gp.PriceType == globalPriceType);
        }

        public void AddOrUpdateShippingGlobalPrice(decimal totalDBPriceToApply, decimal totalUserPriceToApply, decimal totalNoTaxUserPriceToApply)
        {
            var shippingGlobalPrice = GetGlobalPriceByType(GlobalPriceType.Shipping);
            if (shippingGlobalPrice != null)
            {
                UpdateShippingGlobalPrice(totalDBPriceToApply, totalUserPriceToApply, totalNoTaxUserPriceToApply, shippingGlobalPrice);
            }
            else
            {
                AddShippingGlobalPrice(totalDBPriceToApply, totalUserPriceToApply, totalNoTaxUserPriceToApply);
            }
        }

        //Read TaxCalculationResultItems
        public IEnumerable<TaxCalculationResultItem> GetPriceResultItemsBySeller(int sellerId)
        {
            return TaxCalculationResultItems.Where(i => i.SellerId == sellerId);
        }

        private void AddShippingGlobalPrice(decimal totalDBPriceToApply, decimal totalUserPriceToApply, decimal totalNoTaxUserPriceToApply)
        {
            var shippingGlobalPriceToAdd = new TaxCalculationGlobalPriceResult(GlobalPriceType.Shipping)
            {
                TotalDBPriceToApply = totalDBPriceToApply,
                TotalUserPriceToApply = totalUserPriceToApply,
                TotalNoTaxUserPriceToApply = totalNoTaxUserPriceToApply
            };

            _globalPrices.Add(shippingGlobalPriceToAdd);
        }

        private static void UpdateShippingGlobalPrice(decimal totalDBPriceToApply, decimal totalUserPriceToApply, decimal totalNoTaxUserPriceToApply, TaxCalculationGlobalPriceResult shippingGlobalPrice)
        {
            shippingGlobalPrice.TotalDBPriceToApply = totalDBPriceToApply;
            shippingGlobalPrice.TotalUserPriceToApply = totalUserPriceToApply;
            shippingGlobalPrice.TotalNoTaxUserPriceToApply = totalNoTaxUserPriceToApply;
        }
    }
}
