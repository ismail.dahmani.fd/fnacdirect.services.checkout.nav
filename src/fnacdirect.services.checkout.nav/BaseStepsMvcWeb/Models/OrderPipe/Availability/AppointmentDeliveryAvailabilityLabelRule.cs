using FnacDirect.OrderPipe.Core.Services;
using FnacDirect.Technical.Framework.Web.Mvc;
using System;
using System.Globalization;

namespace FnacDirect.OrderPipe.Base.Business.Availability
{
    public class AppointmentDeliveryAvailabilityLabelRule : DefaultAvailabilityLabelRule, IAvailabilityLabelRule
    {
        public AppointmentDeliveryAvailabilityLabelRule(IApplicationContext applicationContext, ISwitchProvider switchProvider, IPipeParametersProvider pipeParametersProvider, DateTimeFormatInfo dateTimeFormatInfo, string customRangeDatesFormat) : base(applicationContext, switchProvider, pipeParametersProvider, dateTimeFormatInfo, customRangeDatesFormat)
        {
        }

        public override EstimatedDeliveryDate ComputeSingleEstimatedDeliveryDate(PopAvailabilityProcessorInput availability)
        {
            var min = availability.DateMin;
            var max = availability.DateMax;

            if (max.HasValue && min.HasValue && max.Value == min.Value)
            {
                if (max.Value.Date == DateTime.Today)
                {
                    return new EstimatedDeliveryDate(_applicationContext.GetMessage("orderpipe.pop.shipping.availability.long.from", string.Empty, min.Value.ToString("dd MMMM",_dateTimeFormatInfo)),
                        max.Value.Date.Ticks,
                        availability);
                }
            }

            return base.ComputeSingleEstimatedDeliveryDate(availability);
        }
    }
}
