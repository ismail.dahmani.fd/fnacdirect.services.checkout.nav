using FnacDirect.Contracts.Online.Model;
using FnacDirect.Customer.BLL;
using FnacDirect.Customer.Model;
using FnacDirect.OrderPipe.Base.Business;
using FnacDirect.OrderPipe.Base.Business.Shipping.Postal;
using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.OrderPipe.Base.Model.Facets;
using FnacDirect.OrderPipe.Base.Proxy.Customer;
using FnacDirect.OrderPipe.Base.Steps.Pop;
using FnacDirect.OrderPipe.BaseMvc.Web.Models.Common;
using FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Common.Shipping;
using FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Common.Shipping.Postal;
using FnacDirect.Technical.Framework.CoreServices.Localization;
using FnacDirect.Technical.Framework.Utils;
using FnacDirect.Technical.Framework.Web.Mvc;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.OnePage.Address
{
    public class AddressViewModelBuilder : IAddressViewModelBuilder
    {
        private readonly IApplicationContext _applicationContext;
        private readonly IUIResourceService _iUIResourceService;
        private readonly ICustomerService _customerService;
        private readonly ICountryService _countryService;
        private readonly ICountryBusiness _countryBusiness;
        private readonly IAddressService _addressService;
        private readonly OPContext _opContext;
        private readonly ISwitchProvider _switchProvider;
        private readonly IAddressValidationBusiness _addressValidationBusiness;

        private readonly PopOrderForm _popOrderForm;
        public AddressViewModelBuilder(OPContext opContext,
                                       IApplicationContext applicationContext,
                                       IUIResourceService uiResourceService,
                                       ICustomerService customerService,
                                       ICountryService countryService,
                                       ICountryBusiness countryBusiness,
                                       IAddressService addressService,
                                       ISwitchProvider switchProvider,
                                       IAddressValidationBusiness addressValidationBusiness)
        {
            _opContext = opContext;
            _applicationContext = applicationContext;
            _iUIResourceService = uiResourceService;
            _customerService = customerService;
            _countryService = countryService;
            _countryBusiness = countryBusiness;
            _addressService = addressService;
            _popOrderForm = _opContext.OF as PopOrderForm;
            _switchProvider = switchProvider;
            _addressValidationBusiness = addressValidationBusiness;
        }

        public AddressFormViewModel BuildForm(AddressFormViewModelMode addressFormViewModelMode, AddressViewModel addressViewModel, IEnumerable<IAddress> addresses)
        {
            Populate(addressViewModel, addressFormViewModelMode);
            var qasAddresses = ConvertToQasViewModel(addresses);
            qasAddresses.AddAddress(addressViewModel);
            return new AddressFormViewModel(addressFormViewModelMode, addressViewModel, qasAddresses);
        }

        public AddressFormViewModel BuildForm(AddressFormViewModelMode addressFormViewModelMode, AddressViewModel addressViewModel)
        {
            Populate(addressViewModel, addressFormViewModelMode);
            return new AddressFormViewModel(addressFormViewModelMode, addressViewModel);
        }

        public void Populate(AddressViewModel addressViewModel, AddressFormViewModelMode addressFormViewModelMode)
        {
            addressViewModel.Countries = Countries(addressFormViewModelMode);
            addressViewModel.Genders = Genders();

            var country = addressViewModel.Countries.FirstOrDefault(c => c.Id == addressViewModel.CountryId.ToString());
            if (country != null)
            {
                addressViewModel.Country = country.Label;
            }

            var documentCountry = addressViewModel.Countries.FirstOrDefault(c => c.Id == addressViewModel.DocumentCountryId.ToString());
            if (documentCountry != null)
            {
                addressViewModel.DocumentCountry = documentCountry.Label;
            }
        }

        public AddressFormViewModel BuildForm(AddressFormViewModelMode addressFormViewModelMode)
        {
            var isGuest = _popOrderForm.UserContextInformations.IsGuest;

            var addressViewModel = Build(addressFormViewModelMode);

            var addressFormViewModel = new AddressFormViewModel(addressFormViewModelMode, addressViewModel)
            {
                IsGuest = isGuest
            };

            return addressFormViewModel;
        }

        public AddressViewModel Build(AddressFormViewModelMode addressFormViewModelMode)
        {
            var sellerId = 0;
            var lineGroup = _popOrderForm.LineGroups.FirstOrDefault();
            if (lineGroup != null)
                sellerId = lineGroup.Seller.SellerId;

            AddressViewModel addressViewModel = null;
            var countries = Countries(addressFormViewModelMode);
            var states = GetStates();
            var guestData = _popOrderForm.GetPrivateData<GuestData>(false);
            if (_popOrderForm.UserContextInformations.IsGuest && guestData != null)
            {
                addressViewModel = GetGuestAddressViewModel(sellerId, countries, guestData);
            }
            else
            {
                var customer = _customerService.GetCurrentCustomer();
                addressViewModel = GetCustomerAddressViewModel(sellerId, countries, states, customer);
            }

            SetAddressCountryLabel(addressViewModel);
            FillAddressViewModelGeneralInfos(addressViewModel);

            return addressViewModel;
        }

        private IEnumerable<StateViewModel> GetStates()
        {
            var culture = _applicationContext.GetCulture();
            var states = new List<StateViewModel>();
            var stateEntities = _countryBusiness.GetStatesList(culture);

            if (!stateEntities.Any())
            {
                return states;
            }

            states = stateEntities
                .Select(s => new StateViewModel { Id = s.Label, Label = s.Label })
                .OrderBy(s => s.Label)
                .ToList();

            return states;
        }

        public AddressFormViewModel GetAddressFormViewModelByAddressReference(string addressReference, AddressFormViewModelMode addressFormViewModelMode, int sellerId)
        {
            var addressViewModel = Build(addressReference, addressFormViewModelMode);

            if (addressFormViewModelMode == AddressFormViewModelMode.Shipping)
            {
                var postalShipping = _popOrderForm.ShippingMethodEvaluation.ShippingMethodEvaluationGroups.Where(g => g.PostalShippingMethodEvaluationItem.HasValue).FirstOrDefault();
                addressViewModel.HasMissingInformations = !_addressValidationBusiness.IsAddressValid(postalShipping.PostalShippingMethodEvaluationItem?.Value.ShippingAddress.Convert());
            }
            else
            {
                addressViewModel.HasMissingInformations = !_addressValidationBusiness.IsAddressValid(_popOrderForm.BillingAddress.Convert());
            }

            var isGuest = _popOrderForm.UserContextInformations.IsGuest;


            var addressFormViewModel = new AddressFormViewModel(addressFormViewModelMode, addressViewModel)
            {
                IsAddressHasBeenAlreadyChosen = IsAddressHasBeenAlreadyChosen(addressReference, sellerId),
                IsGuest = isGuest,
                SellerId = sellerId
            };

            return addressFormViewModel;
        }

        private bool IsAddressHasBeenAlreadyChosen(string addressReference, int currentSeller)
        {
            var sellers = SellersWithValidatedShippingChoice(currentSeller);

            return (from shippingMethodEvaluationGroup in
                        _popOrderForm.ShippingMethodEvaluation.ShippingMethodEvaluationGroups.Where(g => g.PostalShippingMethodEvaluationItem.HasValue)
                    let postalAddress = shippingMethodEvaluationGroup.PostalShippingMethodEvaluationItem.Value.ShippingAddress
                    where postalAddress.Reference == addressReference && sellers.Contains(shippingMethodEvaluationGroup.SellerId)
                    select shippingMethodEvaluationGroup).Any();
        }

        private IEnumerable<int> SellersWithValidatedShippingChoice(int sellerToExclude)
        {
            var popShippingData = _popOrderForm.GetPrivateData<PopShippingData>();

            return popShippingData.Sellers.Where(seller => seller.ShippingChoiceValidated && seller.SellerId != sellerToExclude).Select(s => s.SellerId);
        }

        public AddressViewModel Build(string addressReference, AddressFormViewModelMode addressFormViewModelMode)
        {
            var address = _addressService.GetAddressByAddressBookReference(addressReference);
            var franceId = (int)CountryEnum.France;

            var addressViewModel = new AddressViewModel();
            if (address != null)
            {
                addressViewModel.AddressReference = address.AddressBookReference;
                addressViewModel.AddressName = address.AliasName;
                addressViewModel.GenderId = GetGender(address.GenderId);
                addressViewModel.FirstName = address.FirstName;
                addressViewModel.LastName = address.LastName;
                addressViewModel.CompanyName = address.Company;
                addressViewModel.Address = address.AddressLine1;
                addressViewModel.Floor = address.AddressLine2;
                addressViewModel.AddressComplimentary = address.AddressLine3;
                addressViewModel.ZipCode = address.Zipcode;
                addressViewModel.City = address.City;
                addressViewModel.CountryId = address.CountryId;
                addressViewModel.DocumentCountryId = address.DocumentCountryId;
                addressViewModel.TaxIdNumber = address.TaxIdNumber;
                addressViewModel.TaxNumberType = address.TaxIdNumberType;
                addressViewModel.LegalStatus = address.LegalStatusId;

                if (address.CountryId == franceId)
                {
                    addressViewModel.Phone = !string.IsNullOrEmpty(address.CellPhone) ? address.CellPhone : address.Tel;
                }
                else
                {
                    addressViewModel.Phone = !string.IsNullOrEmpty(address.Tel) ? address.Tel : address.CellPhone;
                }
                addressViewModel.UseAsDefaultBillingAddress = address.IsDefaultBilling;
                addressViewModel.State = address.State;
                addressViewModel.States = GetStates();
                addressViewModel.Countries = Countries(addressFormViewModelMode);

                SetAddressCountryLabel(addressViewModel);
                SetAddressDocumentCountryLabel(addressViewModel);

                var currentCountry = _countryService.GetCountry(addressViewModel.CountryId);
                addressViewModel.CountryIsInEU = currentCountry.UseIntraCommunityVat.GetValueOrDefault();

                var listWebSiteCountryIds = _opContext.Pipe.GlobalParameters.GetAsEnumerable<int>("country.extensions");
                addressViewModel.CountryIsWebSiteCountry = listWebSiteCountryIds.Contains(addressViewModel.CountryId);

                addressViewModel.Genders = Genders();
            }

            FillAddressViewModelGeneralInfos(addressViewModel);

            return addressViewModel;
        }

        private void SetAddressDocumentCountryLabel(AddressViewModel addressViewModel)
        {
            var documentCountry = addressViewModel.Countries.FirstOrDefault(c => c.Id == addressViewModel.DocumentCountryId.ToString());
            if (documentCountry != null)
            {
                addressViewModel.DocumentCountry = documentCountry.Label;
            }
        }

        private void FillAddressViewModelGeneralInfos(AddressViewModel addressViewModel)
        {
            addressViewModel.CompanyNameIsRequired = _switchProvider.IsEnabled("pipe.address.company.name.required");
            addressViewModel.DisplayDocumentCountryId = _switchProvider.IsEnabled("pipe.address.display.document.country");

            addressViewModel.LegalStatusViewModels = GetLegalStatusViewModels();
            addressViewModel.TaxNumberTypeViewModels = GetTaxNumberTypeViewModels();
        }

        private AddressViewModel GetCustomerAddressViewModel(int sellerId, IEnumerable<CountryViewModel> countries, IEnumerable<StateViewModel> states, CustomerEntity customer)
        {
            var addressViewModel = new AddressViewModel
            {
                Countries = countries,
                States = states,
                Genders = Genders(),
                FirstName = customer.FirstName,
                LastName = customer.LtName,
                GenderId = GetGender(customer.Gender),
                CountryId = _applicationContext.GetSiteContext().CountryId,
                DocumentCountryId = _applicationContext.GetSiteContext().CountryId,
                SellerId = sellerId,
                UseAsDefaultAddress = true,
                UseAsDefaultBillingAddress = true
            };

            return addressViewModel;
        }

        private AddressViewModel GetGuestAddressViewModel(int sellerId, IEnumerable<CountryViewModel> countries, GuestData guestData)
        {
            var postalAddress = guestData.ShippingAddress as PostalAddress;
            var billingAddress = guestData.BillingAddress;

            var firstname = postalAddress?.Firstname ?? billingAddress?.Firstname ?? string.Empty;
            var lastname = postalAddress?.LastName ?? billingAddress?.Lastname ?? string.Empty;
            var genderId = postalAddress?.GenderId ?? billingAddress?.GenderId ?? 0;

            var addressViewModel = new AddressViewModel
            {
                Countries = countries,
                Genders = Genders(),
                FirstName = firstname,
                LastName = lastname,
                GenderId = GetGender(genderId),
                CountryId = _applicationContext.GetSiteContext().CountryId,
                SellerId = sellerId,
                UseAsDefaultAddress = true,
                UseAsDefaultBillingAddress = true,
                DocumentCountryId = _applicationContext.GetSiteContext().CountryId,
            };

            return addressViewModel;
        }

        private void SetAddressCountryLabel(AddressViewModel addressViewModel)
        {
            var country = addressViewModel.Countries.FirstOrDefault(c => c.Id == addressViewModel.CountryId.ToString());
            if (country != null)
            {
                addressViewModel.Country = country.Label;
            }
        }

        private static string GetGender(int genderId)
        {
            return genderId.ToString(CultureInfo.InvariantCulture);
        }

        private IEnumerable<LegalStatusViewModel> GetLegalStatusViewModels()
        {
            var legalStatusViewModels = GetViewModelFromTypeEnum<LegalStatusViewModel>(typeof(LegalStatus));

            foreach (var itemStatus in legalStatusViewModels)
            {
                switch (itemStatus.Id)
                {
                    case (int)LegalStatus.Undefined:
                        break;
                    case (int)LegalStatus.Particular:
                        itemStatus.Label = _applicationContext.GetMessage("orderpipe.pop.addresses." + (int)LegalStatus.Particular);
                        break;
                    case (int)LegalStatus.Business:
                        itemStatus.Label = _applicationContext.GetMessage("orderpipe.pop.addresses." + (int)LegalStatus.Business);
                        break;
                    default:
                        break;
                }
            }

            return legalStatusViewModels.Where(l => l.Id != (int)LegalStatus.Undefined);
        }

        private IEnumerable<TaxNumberTypeViewModel> GetTaxNumberTypeViewModels()
        {
            var taxNumberTypeViewModels = GetViewModelFromTypeEnum<TaxNumberTypeViewModel>(typeof(TaxNumberType));

            foreach (var item in taxNumberTypeViewModels)
            {
                switch (item.Id)
                {
                    case 1:
                        item.RegExValidationRule = ".*";//Pasaporte
                        break;
                    case 2:
                        item.RegExValidationRule = "[0-9]{8}[A-Z]{1}$|^[KLM]{1}[0-9]{7}[A-Z]{1}";//NIF
                        break;
                    case 3:
                        item.RegExValidationRule = "([A-Z]{1}[0-9]{7}[A-Z0-9]{1})";//CIF
                        break;
                    case 4:
                        item.RegExValidationRule = "[T]{1}[A-Z0-9]{8}$|^[XYZ]{1}[0-9]{7}[A-Z]{1}";//NIE
                        break;

                    default:
                        break;
                }
            }

            return taxNumberTypeViewModels;
        }

        private IEnumerable<T> GetViewModelFromTypeEnum<T>(Type type) where T : BaseDropDownViewModel, new()
        {
            var vm = new List<T>();
            foreach (var value in Enum.GetValues(type))
            {
                var viewModel = new T
                {
                    Id = ((int)value),
                    Label = value.ToString()
                };
                vm.Add(viewModel);
            }

            return vm;
        }


        private IEnumerable<CountryViewModel> Countries(AddressFormViewModelMode addressFormViewModelMode)
        {
            var listWebSiteCountryIds = _opContext.Pipe.GlobalParameters.GetAsEnumerable<int>("country.extensions");

            var countries = _countryService.GetCountries();

            IList<CountryViewModel> countriesWithValidations = _countryService.GetCountriesWithValidations().Select(c => new CountryViewModel
            {
                Id = c.Id.ToString(CultureInfo.InvariantCulture),
                Label = c.Label,
                Code2 = c.Code2,
                CellPhoneValidation = c.CellPhoneValidation,
                HomePhoneValidation = c.HomePhoneValidation,
                PhoneValidation = c.PhoneValidation,
                ZipCodeValidation = c.ZipCodeValidation,
                TaxNumberValidation = c.TaxNumberValidation,
                IsEUCountry = countries.Any(ct => ct.ID == c.Id && ct.UseIntraCommunityVat.GetValueOrDefault()),
                IsWebsiteCountry = listWebSiteCountryIds.Contains(c.Id)
            }).ToList();

            if (addressFormViewModelMode == AddressFormViewModelMode.Shipping)
            {
                var shippingCountries = RangeSet.Parse<int>(_opContext.Pipe.GlobalParameters.Get("orderpipe.pop.shippingaddress.countries", string.Empty));
                if (!shippingCountries.IsEmpty)
                {
                    countriesWithValidations = countriesWithValidations.Where(c => shippingCountries.Contains(int.Parse(c.Id))).ToList();
                }
            }
            else if (addressFormViewModelMode == AddressFormViewModelMode.Billing)
            {
                var billingCountries = RangeSet.Parse<int>(_opContext.Pipe.GlobalParameters.Get("orderpipe.pop.billingaddress.countries", string.Empty));
                if (!billingCountries.IsEmpty)
                {
                    countriesWithValidations = countriesWithValidations.Where(c => billingCountries.Contains(int.Parse(c.Id))).ToList();
                }
            }

            return countriesWithValidations;
        }

        private IEnumerable<CivilityViewModel> Genders()
        {
            var gendersInDB = new[] { OPConstantes.ID_Gender_Mme, OPConstantes.ID_Gender_Mr };
            var genders = new List<CivilityViewModel>();

            foreach (var gender in gendersInDB)
            {
                var resource = _iUIResourceService.GetUIResourceValue("orderpipe.pop.addresses.gender." + gender, _applicationContext.GetCulture());
                if (resource != null)
                {
                    genders.Add(new CivilityViewModel
                    {
                        Id = gender.ToString(),
                        Label = resource
                    });
                }
                else
                {
                    genders.Add(new CivilityViewModel
                    {
                        Id = gender.ToString(),
                        Label = gender.ToString(),
                    });
                }
            }

            return genders;
        }

        private QasViewModel ConvertToQasViewModel(IEnumerable<IAddress> addresses)
        {
            var qasViewModel = new QasViewModel();

            qasViewModel.AddAddresses(addresses.Select(ConvertToAddressViewModel));
            return qasViewModel;
        }

        private AddressViewModel ConvertToAddressViewModel(IAddress address)
        {
            return new AddressViewModel
            {
                Address = address.AddressLine1,
                Floor = address.AddressLine2,
                AddressComplimentary = address.AddressLine3,
                ZipCode = address.Zipcode,
                City = address.City
            };
        }
    }
}
