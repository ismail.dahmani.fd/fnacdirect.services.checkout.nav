using System;
using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.Contracts.Online.IModel;
using FnacDirect.Technical.Framework.CoreServices;
using FnacDirect.OrderPipe.Base.BaseDAL;
using FnacDirect.OrderPipe.Base.BaseModel.Model.MarketPlace;
using FnacDirect.Contracts.Online.Model;
using FnacDirect.OfferService.IBusiness;
using ArticleType = FnacDirect.OrderPipe.Base.Model.ArticleType;
using FnacDirect.Technical.Framework.Switch;
using FnacDirect.Technical.Framework.ServiceLocation;

namespace FnacDirect.OrderPipe.Base.Business
{
    public class MarketPlaceBusiness : IMarketPlaceService
    {
        private readonly MarketPlaceOfferRefDAL _offerRefDAL = new MarketPlaceOfferRefDAL();
        private readonly MarketPlaceOfferDAL _offerDAL = new MarketPlaceOfferDAL();
        private readonly SiteContext _siteContext;
        private readonly IOfferBusiness _offerBusiness;
        private readonly IArticleBusiness3 _articleBusiness; 

        public MarketPlaceBusiness(SiteContext siteContext)
        {
            _siteContext = siteContext;
            _offerBusiness = ServiceLocator.Current.GetInstance<IOfferBusiness>();
            _articleBusiness = ServiceLocator.Current.GetInstance<IArticleBusiness3>();
        }

        public void AddOfferToBasket(BaseOF orderForm, int offerId)
        {
            var art = new MarketPlaceArticle
            {
                OfferId = offerId,
                Type = ArticleType.Saleable,
                Quantity = 1
            };

            orderForm.Articles.Add(art);

            orderForm.Articles.Modified = true;
        }

        public bool GetArticleDetail(MarketPlaceArticle art)
        {
            _offerBusiness.SetSiteContext(_siteContext);
            _offerBusiness.SetPath(System.Reflection.Assembly.GetExecutingAssembly().Location);

            if (art.OfferId > 0)
            {
                art.Offer = _offerBusiness.GetOffer(art.OfferId, Switch.IsEnabled("orderpipe.pop.mparticle.checkreservation"));
            }
            else if (!string.IsNullOrEmpty(art.OfferRef))
            {
                var offerReference = Guid.Empty;
                if (Guid.TryParse(art.OfferRef, out offerReference)
                    && offerReference != Guid.Empty)
                {
                    art.Offer = _offerBusiness.GetOfferByRef(offerReference, Switch.IsEnabled("orderpipe.pop.mparticle.checkreservation"));
                }
            }
            else
            {
                return false;
            }

            if (art.Offer == null || art.Offer.ArticleReference == null)
                return false;

            art.OfferId = art.Offer.OfferId.GetValueOrDefault();

            var sitemanager = ServiceLocator.Current.GetInstance<ISiteManagerMultiStoreBusiness2>();

            var ctx = new FnacDirect.Contracts.Online.Model.Context(null, null, null) { SiteContext = _siteContext };

            var sart = sitemanager.GetArticle(ctx, art.Offer.ArticleReference, ArticleLoadingOption.NoPriceAndInactive);

            if (sart == null)
                return false;

            art.ArticleDTO = sart;
            art.Family = (int)sart.Core.Group;
            art.Referentiel = (int)art.Offer.ArticleReference.Catalog;
            art.ProductID = sart.Reference.PRID;
            art.ID = art.Offer.ItemId;
            art.PublicID = sart.Reference.PRID.Value;

            art.SellerID = art.Offer.Seller.SellerId;

            art.Title = sart.ExtendedProperties.Title;
            art.Format = sart.ExtendedProperties.FormatId;
            art.FormatLabel = sart.ExtendedProperties.Format;
            art.TypeId = sart.Core.Type.ID;
            art.TypeLabel = sart.Core.Type.Label;

            //On vérifie qu'une type logistique virtuel(spécifique aux articles de gros électroménager) n'est pas spécifié, s'il existe on le prends.
            if (art.Offer.VirtualLogisticType != null && art.Offer.VirtualLogisticType > 0)
            {
                art.LogType = art.Offer.VirtualLogisticType;
            }
            else if (sart.MetaInfo != null && sart.MetaInfo.MarketPlaceLogisticID.GetValueOrDefault() > 0)
            {
                art.LogType = sart.MetaInfo.MarketPlaceLogisticID.Value;
            }
            else
            {
                art.LogType = sart.ExtendedProperties.LogisticID;
            }

            art.Label = art.Title;

            art.HandDelivery = art.Offer.HandDelivery;
            art.ZipCodeSeller = art.Offer.ZipCodeDelivery;

            art.PriceUserEur = art.Offer.Price;
            art.PriceRealEur = art.Offer.Price;
            art.PriceNoVATEur = art.Offer.Price;
            art.PriceDBEur = art.Offer.Price;
            art.VATRate = 0.0m;
            art.VATId = null;

            art.PriceExternalRef = sart.Core.PriceExternalRef;

            if (sart.Core.Images != null && sart.Core.Images.Pictures != null)
            {
                foreach (var picto in sart.Core.Images.Pictures)
                {
                    if (picto.Type == PictureType.SmallScan)
                    {
                        art.ScanURL = picto.Location;
                    }
                    else if (picto.Type == PictureType.Big_110x110)
                    {
                        art.ErgoBasketPictureURL = picto.Location;
                    }
                    else if (picto.Type == PictureType.Principal_340x340)
                    {
                        art.Picture_Principal_340x340 = picto.Location;
                    }
                }
            }

            if (art.Offer.ArticleReference.Catalog == ArticleCatalog.FnacDirect)
            {
                GetFnacDirectArticleDetails(art);
            }

            art.AvailableQuantity = Math.Max(0, GetAvailableOfferQuantity(art.OfferId, art.Reservation?.Id));

            if (art.Quantity > art.AvailableQuantity)
            {
                art.Quantity = art.AvailableQuantity;
            }

            return true;
        }

        private void GetFnacDirectArticleDetails(MarketPlaceArticle art)
        {
            var rf = new ArticleReference
            {
                Catalog = art.Offer.ArticleReference.Catalog,
                PRID = art.Offer.ArticleReference.PRID
            };
            var article = _articleBusiness.GetArticle(_siteContext, rf, ArticleLoadingOption.DefaultAndInactive); //siteContext required

            // Cas ou le service artile ne trouve l'article
            if (article == null) return;

            art.Ean13 = article.Reference.Ean;
            art.Family = (int)article.Core.Group;
            if (article.ExtendedProperties.Serie != null)
                art.SerieTitle = article.ExtendedProperties.Serie.Label;

            art.Participant1 = "";
            art.Participant1_id = "";
            art.Participant2 = "";

            if (article.ExtendedProperties.Roles != null)
            {
                foreach (var role in article.ExtendedProperties.Roles)
                {
                    foreach (var participant in role.Participants)
                    {
                        if (role.ID == 6) // editor
                        {
                            art.Editor = participant.FirstName;
                        }
                        if (participant.Level == LevelParticipant.Primary)
                        {
                            art.Participant1 += ";" + participant.FirstName + " " + participant.LastName;
                            art.Participant1_id += "/" + participant.ID;
                        }
                        if (participant.Level == LevelParticipant.Secondary)
                        {
                            art.Participant2 += ";" + participant.FirstName + " " + participant.LastName;
                        }
                    }
                }
            }
            if (!string.IsNullOrEmpty(art.Participant1))
            {
                art.Participant1 = art.Participant1.Substring(1);
                art.Participant1_id = art.Participant1_id.Substring(1);
            }
            if (!string.IsNullOrEmpty(art.Participant2))
            {
                art.Participant2 = art.Participant2.Substring(1);
            }
            art.Label = (string.IsNullOrEmpty(art.SerieTitle) ? "" : art.SerieTitle + " - ") + art.Title;
            if (!string.IsNullOrEmpty(art.Participant1)) art.Label += " -- " + art.Participant1;

            if (art.Label.Length > 255)
                art.Label = art.Label.Substring(0, 255);

            art.Language = article.ExtendedProperties.Language;
            art.Content = article.ExtendedProperties.Content;
            art.CopyControl = (article.ExtendedProperties.HasCopyControl.HasValue && article.ExtendedProperties.HasCopyControl.Value ? "1" : "0");

            if (string.IsNullOrEmpty(art.ScanURL) && string.IsNullOrEmpty(art.ErgoBasketPictureURL) && art.Referentiel == 1)
            {
                if (article.Core.Images != null && article.Core.Images.Pictures != null)
                {
                    foreach (var picto in article.Core.Images.Pictures)
                    {
                        if (picto.Type == PictureType.SmallScan)
                        {
                            art.ScanURL = picto.Location;
                        }
                        else if (picto.Type == PictureType.Big_110x110)
                        {
                            art.ErgoBasketPictureURL = picto.Location;
                        }
                    }
                }
            }
        }

        public void RefreshReservation(MarketPlaceArticle art, int timeoutInSecond)
        {
            try
            {
                var r = _offerRefDAL.RefreshReservationDate(art.Reservation.Id, art.Quantity.GetValueOrDefault(1), timeoutInSecond);
                UpdateMarketPlaceArticle(art, r);
            }
            catch (Exception ex)
            {
                LogException(ex, nameof(RefreshReservation));
            }
        }

        public void AddReservation(MarketPlaceArticle art, int timeoutInSecond)
        {
            try
            {
                var r = _offerRefDAL.AddReservation(art.OfferId, art.Quantity.GetValueOrDefault(1), timeoutInSecond);
                UpdateMarketPlaceArticle(art, r);
            }
            catch(Exception ex)
            {
                LogException(ex, nameof(AddReservation));
            }
        }

        public void ValidateReservation(MarketPlaceArticle art)
        {
            try 
            { 
                _offerRefDAL.ValidateReservation(art.Reservation.Id, art.OrderDetailPk.Value);
            }            
            catch(Exception ex)
            {
                LogException(ex, nameof(ValidateReservation));
            }
        }

        private void UpdateMarketPlaceArticle(MarketPlaceArticle art, MarketPlaceReservation reserv)
        {
            if (reserv == null)
            {
                reserv = new MarketPlaceReservation
                {
                    Id = 0,
                    Quantity = 0
                };
            }
            art.Reservation = reserv;
        }

        public void CancelReservation(MarketPlaceArticle art)
        {
            try
            {
                if (art.Reservation != null)
                {
                    if (art.Reservation.Id > 0)
                        _offerRefDAL.DeleteReservation(art.Reservation.Id);

                    art.Reservation = null;
                }
            }
            catch(Exception ex)
            {
                LogException(ex, nameof(CancelReservation));
            }
        }

        private int GetAvailableOfferQuantity(int offerId, int? reservationId)
        {
            var quantity = 0;

            try
            {
                quantity = _offerDAL.GetAvailableOfferQuantity(offerId, reservationId);
            }
            catch (Exception ex)
            {
                LogException(ex, nameof(GetAvailableOfferQuantity));
            }

            return quantity;
        }

        internal int GetOfferId(Guid offerReference)
        {
            var result = 0;
            _offerBusiness.SetSiteContext(_siteContext);
            //MarketPlaceOfferBusiness mpBus = new MarketPlaceOfferBusiness();
            var mpo = _offerBusiness.GetOfferByRef(offerReference);
            if (mpo != null)
                result = mpo.OfferId.GetValueOrDefault(0);
            return result;
        }

        private void LogException(Exception ex, string methodName)
        {
            BaseBusinessException.BuildException($"Error in method {nameof(MarketPlaceBusiness)}.{methodName}", ex, BusinessRange.Purchase, 0);
        }
    }
}
