﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.OrderThankYou
{
    public class ConversionFollowViewModel
    {
        public string OrderUserReference { get; set; }
        public string TotalPrice { get; set; }
    }
}
