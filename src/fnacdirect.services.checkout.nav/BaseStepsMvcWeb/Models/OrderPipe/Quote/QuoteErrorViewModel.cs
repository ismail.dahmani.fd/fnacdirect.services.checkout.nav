namespace FnacDirect.OrderPipe.BaseMvc.Web.Pop.Models.Quote
{
    public class QuoteErrorViewModel
    {
        public string QuoteError { get; set; }
    }
}
