using System;
using System.Collections.Generic;

namespace FnacDirect.Basket.Model
{    
    /// <summary>
    /// SmallItemIdentifier :  objet embarqu� utilis� cot� javascript en JSON pour actionner un appel ajax param�tr�
    /// </summary>
    public class SmallItemIdentifier : ISmallItemIdentifier
    {
        public int Quantity { get; set; }
        public Guid Offer { get; set; }
        public int Prid { get; set; }
        public ArticleBasketType Type { get; set; }
        public ItemType ItemArticleType { get; set; }
        public string Reference { get; set; }
        public int OfferId { get; set; }
        public int FatherId { get; set; }
        public List<SmallItemIdentifier> BundleItemIdentifiers { get; set; }


        public SmallItemIdentifier()
        {
            Quantity = 1;
        }

        public SmallItemIdentifier(int prid)
            : this()
        {
            Prid = prid;
        }        
    }
}
