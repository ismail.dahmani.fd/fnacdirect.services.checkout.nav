﻿using System;

namespace FnacDirect.OrderPipe.Base.BaseDAL
{
    public interface IAdherentDAL
    {
        bool IsAdherentHolderWithFnacCard(string adherentNumber, DateTime adherentBirthdate);
    }
}