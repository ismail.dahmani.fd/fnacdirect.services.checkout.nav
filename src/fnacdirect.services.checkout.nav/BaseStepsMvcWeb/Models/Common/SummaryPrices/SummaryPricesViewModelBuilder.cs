using FnacDirect.Contracts.Online.Model;
using FnacDirect.OrderPipe.Base.BaseModel;
using FnacDirect.OrderPipe.Base.BaseModel.Model;
using FnacDirect.OrderPipe.Base.Business;
using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.OrderPipe.Base.ReducedPriceBooks;
using FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Basket;
using FnacDirect.OrderPipe.BaseMvc.Web.Services.Pop.Payment;
using FnacDirect.Technical.Framework.Web.Mvc;
using System.Collections.Generic;
using System.Linq;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Common.Payment
{
    public class SummaryPricesViewModelBuilder : ISummaryPricesViewModelBuilder
    {
        private readonly IApplicationContext _applicationContext;
        private readonly IPriceHelper _priceHelper;
      
        public SummaryPricesViewModelBuilder(IApplicationContext applicationContext)
        {
            _applicationContext = applicationContext;
            _priceHelper = applicationContext.GetPriceHelper();
        }

        public ReducedPriceViewModel BuildReducedPriceBook5pc(PopOrderForm popOrderForm, decimal total)
        {
            ReducedPriceViewModel model = null;
            var reducedPriceData = popOrderForm.GetPrivateData<ReducedPriceData>(create: false);
            if (reducedPriceData != null && reducedPriceData.Reduction > 0)
            {
                model = new ReducedPriceViewModel
                {
                    Reduction = _priceHelper.Format(reducedPriceData.Reduction, PriceFormat.CurrencyAsDecimalSeparator),
                    Price = _priceHelper.Format(total - reducedPriceData.Reduction, PriceFormat.CurrencyAsDecimalSeparator)
                };
            }
            return model;
        }

        public string BuildDifferedSavedAmount(PopOrderForm popOrderForm)
        {
            var differedAmount = (popOrderForm.GetPrivateData<PushAdherentData>("PushAdherentGroup",create:false)?.RealDifferedAmount).GetValueOrDefault();

            if (differedAmount > 0)
            {
                return _priceHelper.Format(differedAmount, PriceFormat.CurrencyAsDecimalSeparator);
            }

            return null;
        }

        public string BuildTotalReduced(PopOrderForm popOrderForm, decimal oldTotalCost, decimal totalCost, bool withVAT = true)
        {
            var totalReduced = (oldTotalCost - totalCost);

            string price = null;

            if (totalReduced > 0)
            {
                price = _priceHelper.Format(totalReduced, PriceFormat.CurrencyAsDecimalSeparator);
            }
            
            if (popOrderForm.OrderBillingMethods.OfType<VoucherBillingMethod>().Any())
            {
                var cuu = popOrderForm.OrderBillingMethods.OfType<VoucherBillingMethod>().First();

                var cuuAmount = withVAT ? cuu.AmountEur.GetValueOrDefault() : cuu.AmountEurNoVAT.GetValueOrDefault();

                price = _priceHelper.Format(totalReduced + cuuAmount, PriceFormat.CurrencyAsDecimalSeparator);
            }

            return price;
        }

        public PriceDetailsViewModel BuildTotalReducedModel(PopOrderForm popOrderForm, decimal oldTotalCost, decimal totalCost, bool withVAT = true)
        {
            var totalReduced = (oldTotalCost - totalCost);

            PriceDetailsViewModel price = null;

            if (totalReduced > 0)
            {
                price = _priceHelper.GetDetails(totalReduced);
            }

            if (popOrderForm.OrderBillingMethods.OfType<VoucherBillingMethod>().Any())
            {
                var cuu = popOrderForm.OrderBillingMethods.OfType<VoucherBillingMethod>().First();

                var cuuAmount = withVAT ? cuu.AmountEur.GetValueOrDefault() : cuu.AmountEurNoVAT.GetValueOrDefault();

                price = _priceHelper.GetDetails(totalReduced + cuuAmount);
            }

            return price;
        }
    }
}
