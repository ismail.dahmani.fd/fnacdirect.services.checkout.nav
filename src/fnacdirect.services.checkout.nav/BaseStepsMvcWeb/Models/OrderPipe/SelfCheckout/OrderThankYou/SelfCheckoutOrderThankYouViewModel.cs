using System;
using System.Collections.Generic;
using System.Linq;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Pop.Models.SelfCheckout
{
    public class SelfCheckoutOrderThankYouViewModel
    {
        public DateTime OrderDateAndTime { get; set; }
        public string ShopFullName { get; set; }
        public string OrderExternalReference { get; set; }
        public string InternalCode { get; set; }
        public string BarCodeUrl { get; set; }
        public List<SelfCheckoutOrderThankYouArticleDetails> ArticleDetails { get; set; } = new List<SelfCheckoutOrderThankYouArticleDetails>();
        public int NbArticles
        {
            get
            {
                if (ArticleDetails.Count != 0)
                {
                    return ArticleDetails.Sum(a => a.Quantity);
                }
                return 0;
            }
        }
        public string Total { get; set; }
    }

    public class SelfCheckoutOrderThankYouArticleDetails
    {
        public string PictureUrl { get; set; }
        public string Name { get; set; }
        public string UnitPrice { get; set; }
        public string Price { get; set; }
        public int Quantity { get; set; }
        public string Ean { get; set; }
    }
}
