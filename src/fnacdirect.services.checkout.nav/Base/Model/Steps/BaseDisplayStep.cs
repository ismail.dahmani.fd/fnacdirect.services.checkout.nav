using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Xml.Serialization;

namespace FnacDirect.OrderPipe.Base.Steps
{
    public class DisplayData : StepData
    {
        public DisplayData()
        {
            Items = new DisplayDataItemList();
        }

        public DisplayDataItemList Items { get; set; }

        public DisplayDataItem Get(string key)
        {
            if(!Items.Contains(key))
            {
                var item = new DisplayDataItem()
                {
                    Name = key
                };

                Items.Add(item);

                return item;
            }

            return Items[key];
        }
    }

    public class DisplayDataItemList : KeyedCollection<string, DisplayDataItem>
    {
        protected override string GetKeyForItem(DisplayDataItem item)
        {
            if(item == null)
            {
                return null;
            }

            return item.Name;
        }
    }

    public class DisplayDataItem
    {
        public DisplayDataItem()
        {
            Counter = 0;
            Displayed = false;
        }

        [XmlAttribute]
        public string Name { get; set; }

        [DefaultValue(0)]
        public int Counter { get; set; }

        [DefaultValue(false)]
        public bool Displayed { get; set; }
    }

    public abstract class BaseDisplayStep<O> : BaseDisplayStep<O, NullGroupData>
        where O : OF
    {

    }

    public abstract class BaseDisplayStep<O,G> : InteractiveStep<O, DisplayData, G> 
        where O:OF
        where G:GroupData,new()
    {
        protected override StepReturn Check(OPContext Context, InnerData D)
        {
            if (D.Data.Get(Name).Displayed)
            {
                AddGlobalConstraint(Context, "step." + Name.ToLowerInvariant() + ".displayed");
                return StepReturn.Skip;
            }
            else
                return StepReturn.Process;
        }

        protected override StepReturn Process(OPContext Context, InnerData D)
        {
            AddGlobalConstraint(Context, "step." + Name.ToLowerInvariant() + ".displayed");
            D.Data.Get(Name).Displayed = true;
            D.Data.Get(Name).Counter++;
            return StepReturn.Next;
        }

        public override void Reset(OPContext Context)
        {
            GetInnerData(Context).Data.Get(Name).Displayed = false;
            OP.RemoveGlobalConstraint(Context.OF, "step." + Name.ToLowerInvariant() + ".displayed");
        }
    }
}
