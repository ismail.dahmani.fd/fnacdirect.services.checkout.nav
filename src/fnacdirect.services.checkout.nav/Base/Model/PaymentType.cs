
namespace FnacDirect.OrderPipe.Base.Model.FDV
{
    public enum PaymentType
    {
        None = 0,       
        Tpe = 1,
        EuroFid = 2,
        GiftCardFnac = 31,
        GiftCardIllicado = 32,
        GiftCardFnacEcard = 33,
        GiftCardDarty = 34
    }
}
