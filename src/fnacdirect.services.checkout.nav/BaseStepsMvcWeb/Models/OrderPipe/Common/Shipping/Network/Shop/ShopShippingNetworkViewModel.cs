using System.Collections.Generic;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Common.Shipping.Shop
{
    public class ShopShippingNetworkViewModel : ShippingNetworkViewModel<ShopDetailsViewModel>
    {
        private readonly List<ShopViewModel> _shopViewModels
           = new List<ShopViewModel>();

        public IReadOnlyCollection<ShopViewModel> Shops
        {
            get
            {
                return _shopViewModels.AsReadOnly();
            }
        }

        public bool HasMarketingEvent { get; set; }

        public string MarketingEventPicto { get; set; }

        public bool GeoAutoSelectionEnabled { get; set; }

        public ShopShippingNetworkViewModel With(params ShopViewModel[] shopViewModels)
        {
            _shopViewModels.AddRange(shopViewModels);

            return this;
        }
    }
}
