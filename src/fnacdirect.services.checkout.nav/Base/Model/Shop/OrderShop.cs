﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FnacDirect.OrderPipe.Base.BaseModel.Model
{
	/// <summary>
	/// Représente une ligne dans la table tbl_OrderShop
	/// </summary>
	public class OrderShop
	{
		public string RefGU { get; set; }
		public string VendorCode { get; set; }
		public string VendorLogin { get; set; }
		public int? EAgendaId { get; set; }
		public int? OriginEAgendaId { get; set; }
	}
}
