using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Web.Mvc;
using FnacDirect.Basket.Business;
using FnacDirect.Context;
using FnacDirect.Front.WebBusiness;
using FnacDirect.OrderPipe.Base.Business;
using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Basket;
using FnacDirect.OrderPipe.BaseMvc.Web.Services.Article.Pipe;
using FnacDirect.OrderPipe.BaseMvc.Web.Services.Tracking;
using FnacDirect.OrderPipe.CoreRouting;
using FnacDirect.Technical.Framework.Web;
using FnacDirect.Technical.Framework.Web.UrlMgr;

namespace FnacDirect.OrderPipe.BaseMvc.Web.CrossSellBasket
{
    public class OneClickBasketController : CrossSellBasketController
    {
        private readonly IUserContextProvider _userContextProvider;
        private readonly ISwitchProvider _switchprovider;

        public OneClickBasketController(IUrlManager urlManager,
                                        IUserContextProvider userContextProvider,
                                        IExecutePipeService executePipeService,
                                        IBasketPersistService basketPersistService,
                                        ICrossSellBasketViewModelBuilder crossSellBasketViewModelBuilder,
                                        IW3ArticleService w3ArticleService,
                                        ISwitchProvider switchprovider,
                                        IArticleDetailService articleDetailService,
                                        ITrackingService trackingService)
            : base(urlManager,
                  userContextProvider,
                  executePipeService,
                  basketPersistService,
                  crossSellBasketViewModelBuilder,
                  w3ArticleService,
                  switchprovider,
                  articleDetailService,
                  trackingService)
        {
            _userContextProvider = userContextProvider;
            _switchprovider = switchprovider;
        }

        protected override ActionResult ComputeAddResult(PopOrderForm orderForm, IEnumerable<CrossSellBasketItemInput> basketItems)
        {
            var userContext = _userContextProvider.GetCurrentUserContext();

            var url = InitUrl();
            url.WithParam("context", Uri.EscapeDataString(GetOneClickParameter(basketItems) + ComputeAdditionalContextKeys(basketItems)));

            if (!userContext.IsAuthenticated
                && !_switchprovider.IsEnabled("identityserver"))
            {
                var registerUrl = InitUrl();
                registerUrl.WithParam("context", Uri.EscapeDataString(GetOneClickParameter(basketItems) + "&" + Constants.ContextKeys.SpreadRegistration + ComputeAdditionalContextKeys(basketItems)));

                if (Request.IsAjaxRequest())
                {
                    return PartialView("Login/_LoginPopin", new LoginPopinViewModel()
                    {
                        ContainsRegistrationForm = false,
                        LoginLink = url.ToString(),
                        RegisterLink = registerUrl.ToString(),
                        IsLogged = userContext.IsAuthenticated
                    });
                }

                return Redirect(registerUrl.ToString());
            }

            if (Request.IsAjaxRequest())
            {
                Response.Headers.Add("Location", url.ToString());
                return new HttpStatusCodeResult(HttpStatusCode.Created);
            }

            return Redirect(url.ToString());
        }

        protected virtual string ComputeAdditionalContextKeys(IEnumerable<CrossSellBasketItemInput> basketItems)
        {
            return string.Empty;
        }

        private string GetOneClickParameter(IEnumerable<CrossSellBasketItemInput> basketItems)
        {
            if (!basketItems.Any())
            {
                return Constants.ContextKeys.OneClick;
            }

            var queryString = new StringBuilder();

            for (var i = 0; i < basketItems.Count(); i++)
            {
                var item = basketItems.ElementAt(i);

                if (queryString.Length > 0)
                {
                    queryString.Append("&");
                }

                queryString.AppendFormat("Article[{0}].ProductId={1}", i, item.ProductId);

                if (item.Offer != Guid.Empty)
                {
                    queryString.AppendFormat("&Article[{0}].Offer={1}", i, item.Offer);
                }
                else if (item.MasterProductId > 0)
                {
                    queryString.AppendFormat("&Article[{0}].MasterProductId={1}", i, item.MasterProductId);
                }
            }

            return Constants.ContextKeys.OneClick + "=" + Uri.EscapeDataString(queryString.ToString());
        }

        private UrlWriter InitUrl()
        {
            var url = OrderPipeUrl.Clone();

            // Préservation du paramètre "app" si présent, nécessaire pour la détection de l'application mobile
            // (via méthode d'extension Html.IsMobileApplication())
            var appParamName = ParametersName.MobileAppOrigin.ToLowerInvariant();
            var callingPlatform = HttpContext.Request[appParamName];
            if (!string.IsNullOrEmpty(callingPlatform))
            {
                url.WithParam(appParamName, callingPlatform);
            }

            return url;
        }
    }
}
