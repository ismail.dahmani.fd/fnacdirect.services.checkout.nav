using System;
using System.Collections.Generic;
using System.Text;
using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.Customer.Model;

namespace FnacDirect.Quote.Model
{
    public class QuotePostalAddress : PostalAddress
    {
        public QuotePostalAddress()
        { }

        public QuotePostalAddress(AddressEntity addressEntity)
            : base(addressEntity)
        {            
        }

        public  QuotePostalAddress(PostalAddress postalAddress)
        {
            ModelHelper.Copy(postalAddress,this);
        }
		
    }

}
