using SolexModel = FnacDirect.OrderPipe.Base.Model.Solex;
using FnacDirect.Solex.Shipping.Client.Contract;
using FnacDirect.OrderPipe.Base.BaseModel;
using System.Collections.Generic;

namespace FnacDirect.OrderPipe.Base.Business.Shipping
{
    public interface ISolexObjectConverterService
    {
        PostalAddress BuildPostalAddress(ShippingMethodEvaluationGroup shippingMethodEvaluationGroup, SolexModel.DeliverySlotData deliverySlotData);
        ShopAddress BuildShopAddress(ShippingMethodEvaluationGroup shippingMethodEvaluationGroup);
        RelayAddress BuildRelayAddress(ShippingMethodEvaluationGroup shippingMethodEvaluationGroup);
        IEnumerable<SolexModel.ShippingChoice> ConvertSolexChoicesToShippingChoices(IEnumerable<Choice> choices);
        SolexModel.ShippingChoice ConvertSolexChoiceToShippingChoice(Choice choice);
    }
}
