using FnacDirect.OrderPipe.Base.Model;

namespace FnacDirect.OrderPipe.Base.Business.Quote
{
    public interface IQuoteDetailsBusiness
    {
        PopOrderForm GetQuoteDetailBySID(string quoteSid, PopOrderForm popOrderForm);
    }
}
