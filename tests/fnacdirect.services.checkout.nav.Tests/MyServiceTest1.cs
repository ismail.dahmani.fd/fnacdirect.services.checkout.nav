using System;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Moq;
using Xunit;

namespace fnacdirect.services.checkout.nav.Tests
{
    public class MyServiceTest1
    {
        private IOptionsMonitor<T> GetOptionsMonitor<T>(T appConfig)
        {
            var optionsMonitorMock = new Mock<IOptionsMonitor<T>>();
            optionsMonitorMock.Setup(o => o.CurrentValue).Returns(appConfig);
            return optionsMonitorMock.Object;
        }

        private ILogger<T> GetLogger<T>() => new Mock<ILogger<T>>().Object;

        [Fact]
        public void GetNextValue_Returns_initial_on_first_call()
        {
            var service=new MyService(GetLogger<MyService>(), GetOptionsMonitor<MyServiceOptions>(new MyServiceOptions { SomeOption=false, InitialValue=1000 }) );

            var val=service.GetNextValue();

            Assert.Equal(1001,val);
        }

        [Fact]
        public void GetNextValue_Returns_next_on_subsequent_call()
        {
            var service=new MyService(GetLogger<MyService>(), GetOptionsMonitor<MyServiceOptions>(new MyServiceOptions { SomeOption=false, InitialValue=1000 }) );

            service.GetNextValue();

            var val=service.GetNextValue();

            Assert.Equal(1002,val);
        }

    }
}
