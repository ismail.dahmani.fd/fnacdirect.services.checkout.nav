using FnacDirect.Customer.BLL;
using FnacDirect.Orange.Business;
using FnacDirect.Orange.Model;
using FnacDirect.OrderPipe.Base.Business.Configuration;
using FnacDirect.Technical.Framework.Web;
using FnacDirect.Technical.Framework.Web.Mvc;
using System;
using System.Threading.Tasks;

namespace FnacDirect.OrderPipe.Base.Business
{
    public class OrangeBusiness : IOrangeBusiness
    {
        private readonly IApplicationContext _applicationContext;
        private readonly IUrlManager _urlManager;
        private readonly IExternalCustomerService _externalCustomerService;
        private readonly IOrangeApiServices _orangeApiServices;
        private readonly IOrangeTransactionServices _orangeTransactionServices;
        private readonly IOrangeConfigurationProvider _orangeConfigurationProvider;
        const int ORANGE_TOKEN_TYPE_ID = 2;

        public OrangeBusiness(IApplicationContext applicationContext, IUrlManager urlManager,
            IExternalCustomerService externalCustomerService, IOrangeApiServices orangeApiServices,
            IOrangeTransactionServices orangeTransactionServices, IOrangeConfigurationProvider orangeConfigurationProvider)
        {
            _applicationContext = applicationContext;
            _urlManager = urlManager;
            _externalCustomerService = externalCustomerService;
            _orangeApiServices = orangeApiServices;
            _orangeTransactionServices = orangeTransactionServices;
            _orangeConfigurationProvider = orangeConfigurationProvider;
        }

        public string GetOrangeAuthUrl()
        {
            var configuration = _orangeConfigurationProvider.Get();
            var baseOrangeApiUrl = configuration.OrangeApiUrl;

            var page = _urlManager.Sites.WWWDotNetSecure.GetPage("OrangeReturn");
            var orangeReturnUrl = page?.DefaultUrl.ToString();

            var authUrl = $"{baseOrangeApiUrl}public/webApp/?action=ocAuth&returnUrl={orangeReturnUrl}";
            return authUrl;
        }

        public string GetIse2FromAssociatedAccount(int accountId)
        {
            var token = _externalCustomerService.GetExternalToken(accountId, ORANGE_TOKEN_TYPE_ID);

            return token;
        }

        public bool EnableAccountOrange(string ise2, string accountReference)
        {
            var hasBeenAssociated = false;
            var orangeEnableAccountRequest = new OrangeAssociateAccountRequest
            {
                Ise2 = ise2,
                PartnerAccountId = accountReference
            };

            //Should we add a try catch for timeout or else ?
            var result = Task.Run(async () => { return await _orangeApiServices.EnableAccount(orangeEnableAccountRequest); }).Result;

            var returnCode = result.Status?.Code;
            if (int.Parse(returnCode) == 200)
            {
                var accountInfos = result.AccountInfos;
                var status = accountInfos.AccountStatus;
                if (!string.IsNullOrEmpty(status))//test 1=associated, 3=alreadyassociated
                {
                    hasBeenAssociated = true;
                }
            }
            return hasBeenAssociated;
        }

        public void CreateExternalTokenTypeOrange(string ise2, int accountId)
        {
            var tokenTypeId = 2;
            _externalCustomerService.CreateExternalToken(accountId, tokenTypeId, ise2);
        }

        public void AddOrangeTransaction(OrangeTransaction transaction)
        {
            var transactionIsValid = transaction.OrderId != 0
                && transaction.Ise2 != null
                && transaction.OrderTransactionOrangeReference != Guid.Empty;

            if (transactionIsValid)
                _orangeTransactionServices.AddOrangeTransaction(transaction);
        }
    }
}
