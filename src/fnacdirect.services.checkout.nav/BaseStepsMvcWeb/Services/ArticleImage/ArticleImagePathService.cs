using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.Technical.Framework.Web;
using FnacDirect.Technical.Framework.Web.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Services.Pop
{
    public class ArticleImagePathService : IArticleImagePathService
    {
        private readonly IUrlManager _urlManager;
        private readonly IApplicationContext _applicationContext;

        public ArticleImagePathService(IUrlManager urlManager, IApplicationContext applicationContext)
        {
            _urlManager = urlManager;
            _applicationContext = applicationContext;
        }

        public string BuildPath(BaseArticle article)
        {
            return BuildPath(article, null);
        }

        public string BuildPath(BaseArticle article, Promotion promotion)
        {
            if (article is Service && article.TypeId.HasValue)
            {
                return string.Format("{0}pictos/{1}.png", _applicationContext.GetImagePathWithCulture(), article.TypeId.Value);
            }
            if (article.Type == ArticleType.Free && promotion != null && !string.IsNullOrWhiteSpace(promotion.DisplayImage))
            {
                return string.Format("{0}promo/{1}", _applicationContext.GetImagePathWithCulture(), promotion.DisplayImage);
            }

            var multimediaPath = _urlManager.Sites.Multimedia.BaseRootUrl;

            var url = article.Picture_Principal_340x340;

            // pas d'url
            if (string.IsNullOrEmpty(url))
            {
                url = article.ErgoBasketPictureURL;

                if (string.IsNullOrEmpty(url))
                {
                    url = article.ScanURL;

                    if (string.IsNullOrEmpty(url))
                    {
                        return multimediaPath.WithPage("img/catalog/noscan_47x70.gif").ToString();
                    }
                }
            }

            // url complète, il faut la basculer sur le secure
            if (Uri.IsWellFormedUriString(url, UriKind.Absolute))
            {
                return url.Replace("http://", "https://");
            }

            // url a construire
            try
            {
                return multimediaPath.WithPage(url).ToString();
            }
            catch (Exception)
            {
                return multimediaPath.WithPage("img/catalog/noscan_47x70.gif").ToString();
            }
        }
    }
}
