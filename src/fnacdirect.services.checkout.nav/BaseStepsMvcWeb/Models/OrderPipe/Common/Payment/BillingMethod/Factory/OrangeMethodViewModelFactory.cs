using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.OrderPipe.Base.Model.BillingMethods;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Common.Payment.BillingMethodFactories
{
    public class OrangeMethodViewModelFactory : BasePaymentViewModelFactory<OrangeBillingMethod>
    {
        public override BillingMethodViewModel Get(string tabName, PaymentMethod paymentMethod, OPContext opContext, PopOrderForm popOrderForm)
        {
            var orangeData = popOrderForm.GetPrivateData<OrangeData>("OrangeData",false);
            var isAssociated = orangeData.IsAssociated;
            var orangeAuthUrl = orangeData.OrangeAuthUrl;

            return new OrangeBillingMethodsViewModel()
            {
                OrangeAuthApiUrl = orangeAuthUrl,
                IsAssociated = isAssociated,
                Display = true,
                DisplayFirst = isAssociated || orangeData.ShowOrangeBanner
            };
        }
    }
}
