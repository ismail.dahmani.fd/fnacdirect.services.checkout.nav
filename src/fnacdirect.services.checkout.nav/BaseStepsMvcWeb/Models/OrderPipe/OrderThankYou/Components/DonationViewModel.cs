﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.OrderThankYou
{
    public class DonationViewModel
    {
        public string Name { get; set; }

        public decimal Amount { get; set; }

        public string PictureUrl { get; set; }
    }
}
