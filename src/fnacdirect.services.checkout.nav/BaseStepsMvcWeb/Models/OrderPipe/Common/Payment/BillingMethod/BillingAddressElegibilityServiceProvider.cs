using FnacDirect.OrderPipe.Base.Business.Shipping;
using FnacDirect.Technical.Framework.Web.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FnacDirect.OrderPipe.Base.Business.Billing
{
    public class BillingAddressElegibilityServiceProvider : IBillingAddressElegibilityServiceProvider
    {
        private readonly INumericalBusiness _numericalBusiness;
        private readonly IApplicationContext _applicationContext;

        public BillingAddressElegibilityServiceProvider(INumericalBusiness numericalBusiness, IApplicationContext applicationContext)
        {
            _numericalBusiness = numericalBusiness;
            _applicationContext = applicationContext;
        }

        public IBillingAddressElegibilityService GetBillingAddressElegibilityServiceFor(OPContext opContext)
        {
            return new BillingAddressElegibilityService(opContext, _numericalBusiness, _applicationContext);
        }
    }
}
