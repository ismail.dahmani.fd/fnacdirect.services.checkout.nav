﻿using FnacDirect.FnacStore.Model;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Shipping.ShipToStore
{
    public class StoreViewModel
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Address { get; set; }

        public string ZipCode { get; set; }

        public string City { get; set; }

        public double Latitude { get; set; }

        public double Longitude { get; set; }

        public bool IsFavorite { get; set; }
     
        public string OpeningHours { get; set; }

        public StoreOrganization Organisation { get; set; }
    }
}