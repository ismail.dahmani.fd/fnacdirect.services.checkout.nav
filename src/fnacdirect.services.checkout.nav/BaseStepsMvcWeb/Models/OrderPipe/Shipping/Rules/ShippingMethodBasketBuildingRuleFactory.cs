﻿using System;
using FnacDirect.OrderPipe.Config;
using StructureMap;
using FnacDirect.Technical.Framework.Web.Mvc;
using FnacDirect.OrderPipe.Core.Services;

namespace FnacDirect.OrderPipe.Base.Business.Shipping.Rules
{
    public class ShippingMethodBasketBuildingRuleFactory : IShippingMethodBasketBuildingRuleFactory
    {
        private readonly IContainer _container;
        private readonly IApplicationContext _applicationContext;

        public ShippingMethodBasketBuildingRuleFactory(IContainer container, IApplicationContext applicationContext)
        {
            _container = container;
            _applicationContext = applicationContext;
        }

        public IShippingMethodBasketBuildingRule Get<T>(IPipeParametersProvider pipeParametersProvider) where T : BaseShippingMethodBasketBuildingRule
        {
            var instance = _container.GetInstance(typeof(T)) as BaseShippingMethodBasketBuildingRule;

            instance.PipeParametersProvider = pipeParametersProvider;

            if(!string.IsNullOrEmpty(instance.SwitchId) && !_applicationContext.GetSwitch(instance.SwitchId))
            {
                return null;
            }

            return instance;
        }
    }
}
