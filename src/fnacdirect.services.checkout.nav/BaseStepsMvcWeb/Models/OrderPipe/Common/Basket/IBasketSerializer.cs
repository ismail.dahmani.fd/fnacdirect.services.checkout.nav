﻿using FnacDirect.ShoppingCartEntities;

namespace FnacDirect.Basket.Business.Serialization
{
    public interface IBasketSerializer
    {
        bool Serialize(Orderform orderform);
        Orderform Deserialize(string sid);
    }
}
