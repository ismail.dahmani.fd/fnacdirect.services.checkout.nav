using FnacDirect.OrderPipe.Base.DAL;
using FnacDirect.OrderPipe.Base.Model.FDV.Rebate;
using System.Collections.Generic;

namespace FnacDirect.OrderPipe.Base.Business
{
    public class RemoteRebateBusiness : IRemoteRebateBusiness
    {
        private readonly IShopCommerceDAL _shopCommerceDal;


        public RemoteRebateBusiness(IShopCommerceDAL shopCommerceDal)
        {
            _shopCommerceDal = shopCommerceDal;
        }


        public int AddRemoteRebate(RemoteRebate remoteRebate)
        {
            return _shopCommerceDal.AddRemoteRebate(remoteRebate);
        }

        public RemoteRebate LoadRemoteRebate(int rebateId)
        {
            return _shopCommerceDal.LoadRemoteRebate(rebateId);
        }

        public IEnumerable<RemoteRebate> LoadRemoteRebates()
        {
            return _shopCommerceDal.LoadRemoteRebates();
        }

        public RemoteRebateStatus UpdateRemoteRebate(int remoteRebateId, RemoteRebateStatus rebateStatus, string respMatricule, string respComment)
        {
            return _shopCommerceDal.UpdateRemoteRebate(remoteRebateId, rebateStatus, respMatricule, respComment);
        }
    }
}
