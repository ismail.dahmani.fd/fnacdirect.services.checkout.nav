﻿using System.Collections.Generic;

namespace FnacDirect.OrderPipe.Base.Business.Configuration
{
    public class CountryPhoneConfiguration
    {
        public IEnumerable<CountryPhoneExp> CountryPhoneExps { get; set; }
    }

    public class CountryPhoneExp
    {
        public string CountryName { get; set; }
        public string CountryShortName { get; set; }
        public string RequiredState { get; set; }
        public string CountryPhoneCode { get; set; }
        public string CountryCode { get; set; }
        public string Expression { get; set; }
    }
}
