using FnacDirect.MBWay.Model;
using FnacDirect.Orange.Model;
using FnacDirect.OrderPipe.Base.Business.Configuration;

namespace FnacDirect.OrderPipe.Base.Business
{
    public interface IMBWayBusiness
    {
        string GetMBWayUrl();
        MBWayConfiguration GetMBWayApiConfiguration();
        void AddMBWayTransaction(MBWayTransaction transaction);

    }
}
