using System;
using System.Collections.Generic;
using FnacDirect.OrderPipe.Base.BaseModel;
using FnacDirect.OrderPipe.Base.Model.LineGroups;
using System.Linq;
using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.OrderPipe.Base.LineGroups;
using FnacDirect.OrderPipe.Base.Business.Shipping.Shop;
using FnacDirect.FnacStore.Model;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Services.ShipToStore
{
    public class StoreEligibilityLineGroupsService : IStoreEligibilityLineGroupsService
    {
        private readonly IShipToStoreAvailabilityService _shipToStoreAvailabilityService;

        public StoreEligibilityLineGroupsService(IShipToStoreAvailabilityService shipToStoreAvailabilityService)
        {
            _shipToStoreAvailabilityService = shipToStoreAvailabilityService;
        }

        public IEnumerable<FnacStore.Model.FnacStore> GetStoresEligibleForLineGroups(IEnumerable<FnacStore.Model.FnacStore> fnacStores, IEnumerable<ILineGroup> lineGroups, Maybe<ShopShippingMethodEvaluationItem> shippingMethodEvaluationItem)
        {
            var eligibleStores = new List<FnacStore.Model.FnacStore>();

            var fnacStoreList = fnacStores as IList<FnacStore.Model.FnacStore> ?? fnacStores.ToList();
            var lineGroupList = lineGroups as IList<ILineGroup> ?? lineGroups.ToList();
            var articles = lineGroupList.GetArticles().OfType<StandardArticle>().Where(a => a.LogType.HasValue && a.LogType.Value != 9).ToList();
            var clickAndCollectOnlyArticles = lineGroupList.Where(lg => lg.Seller.Type == SellerType.ClickAndCollectOnly).GetArticles().Cast<StandardArticle>();

            foreach (var fnacStore in fnacStoreList)
            {
                var hasPE = lineGroupList.GetArticles().OfType<StandardArticle>().Any(a => a.IsPE);
                var hasPT = lineGroupList.GetArticles().OfType<StandardArticle>().Any(a => a.IsPT);

                if (hasPE && !fnacStore.IsDeliveryOrClickAndCollectPE())
                    continue;

                if (hasPT && !fnacStore.IsDeliveryOrClickAndCollectPT())
                    continue;

                var availabilities = _shipToStoreAvailabilityService.GetAvailabilities(fnacStore, lineGroupList.GetArticles()).ToList();

                if (!articles.Any(a => availabilities.Any(av => av.ReferenceGu == a.ReferenceGU)))
                {
                    continue;
                }

                // Vérification que le magasin a bien tous les articles 1h indispo central en stock
                if (!clickAndCollectOnlyArticles.All(a => availabilities.Any(av =>
                    av.ReferenceGu == a.ReferenceGU && av is ClickAndCollectShipToStoreAvailability)))
                {
                    continue;
                }

                eligibleStores.Add(fnacStore);
            }

            return eligibleStores;
        }
    }
}
