using FnacDirect.OrderPipe.Base.BaseModel;
using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.OrderPipe.Base.Model.FDV;
using FnacDirect.OrderPipe.Base.Model.LineGroups;
using FnacDirect.OrderPipe.Base.Model.Solex;
using FnacDirect.OrderPipe.Core.Services;
using FnacDirect.Solex.Shipping.Client.Contract;
using FnacDirect.Technical.Framework.Web.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using SolexModel = FnacDirect.OrderPipe.Base.Model.Solex;

namespace FnacDirect.OrderPipe.Base.Business.Shipping
{
    public class ShippingEvaluationBusiness : IShippingEvaluationBusiness
    {
        private readonly ICountryService _countryService;
        private readonly ISolexObjectConverterService _solexObjectConverterService;
        private readonly IShippingChoiceAggregationService _shippingChoiceAggregationService;
        private readonly IEnumerable<int> _storeShippingMethodIds;

        public ShippingEvaluationBusiness(ICountryService countryService,
                                          ISolexObjectConverterService solexObjectConverterService,
                                          IShippingChoiceAggregationService shippingChoiceAggregationService,
                                          IPipeParametersProvider pipeParametersProvider)
        {
            _countryService = countryService;
            _solexObjectConverterService = solexObjectConverterService;
            _shippingChoiceAggregationService = shippingChoiceAggregationService;

            _storeShippingMethodIds = pipeParametersProvider.GetAsEnumerable<int>("shippingmethodid.store.all");
        }
        /// <summary>
        /// Retourne les premiers Choices d'un ShippinNetwork, sans ordre ou condition précise.
        /// </summary>
        public IEnumerable<SolexModel.ShippingChoice> GetFirstChoices<T>(ShippingNetwork<T> shippingNetwork) where T : Address
        {
            var choices = shippingNetwork.Addresses.FirstOrDefault().Choices.FirstOrDefault();

            return _solexObjectConverterService.ConvertSolexChoicesToShippingChoices(choices);
        }

        /// <summary>
        /// Retourne les Choices d'une adresse postale.
        /// </summary>
        public IEnumerable<SolexModel.ShippingChoice> GetPostalChoices(ShippingMethodEvaluationGroup shippingMethodEvaluationGroup, EvaluateShoppingCartResult evaluateShoppingCartResult)
        {
            var countries = _countryService.GetCountries();
            var country = countries.FirstOrDefault(c => c.ID == shippingMethodEvaluationGroup.PostalShippingMethodEvaluationItem.Value.ShippingAddress.CountryId).Code3;
            var zipCode = shippingMethodEvaluationGroup.PostalShippingMethodEvaluationItem.Value.ShippingAddress.ZipCode;
            var city = shippingMethodEvaluationGroup.PostalShippingMethodEvaluationItem.Value.ShippingAddress.City;
            var shippingAddress = shippingMethodEvaluationGroup.PostalShippingMethodEvaluationItem.Value.ShippingAddress;
            var addressLine = string.Format("{0} {1} {2} {3}", shippingAddress.AddressLine1, shippingAddress.AddressLine2, shippingAddress.AddressLine3, shippingAddress.AddressLine4).TrimEnd();

            var choices = evaluateShoppingCartResult.ShippingNetworks
                                                .Postal
                                                .Addresses.FirstOrDefault(a => (a.AddressLine == addressLine
                                                                                && a.ZipCode == zipCode
                                                                                && a.City == city
                                                                                && a.Country == country)).Choices.FirstOrDefault();
            var convertedChoices =  _solexObjectConverterService.ConvertSolexChoicesToShippingChoices(choices);
            
            return _shippingChoiceAggregationService.AggregateShippingChoices(convertedChoices);
        }

        /// <summary>
        /// Retourne le meilleur Choice, selon les paramètres passés.
        /// </summary>
        public SolexModel.ShippingChoice GetBestChoiceForDefaultShippingMethod(ShippingMethodEvaluationItem shippingMethodEvaluationItem, SolexModel.DeliverySlotData deliverySlotData = null)
        {
            var shippingMethodEvaluationItemChoices = shippingMethodEvaluationItem.Choices;

            if (shippingMethodEvaluationItem.ShippingMethodEvaluationType == ShippingMethodEvaluationType.Postal && (deliverySlotData == null || !deliverySlotData.HasSelectedSlot || !deliverySlotData.DeliverySlotSelected.IsSelected))
            {
                return shippingMethodEvaluationItemChoices.GetBestChoice(Constants.ShippingMethods.HandDelivery, Constants.ShippingMethods.ChronopostDelivery, Constants.ShippingMethods.SteedFromStore);
            }
            else
            {
                return shippingMethodEvaluationItemChoices.GetBestChoice(Constants.ShippingMethods.HandDelivery, Constants.ShippingMethods.SteedFromStore);
            }
        }

        /// <summary>
        /// Retourne un Choice généré, pour un magasin spécifique (selon le shippingMethodId passé en paramètre.
        /// Cas Specifique FDV pour forcer la shippingMethode sur le retrait magasin.
        /// </summary>
        public SolexModel.ShippingChoice GetForcedChoiceForShopOrders(IEnumerable<ILineGroup> lineGroups, ShopInfosData shopInfoData)
        {
            var groups = new List<SolexModel.Group>();

            var choice = new SolexModel.ShippingChoice
            {
                MainShippingMethodId = 9,
                AltLabel = "Retrait Magasin",
                Label = "Retrait Magasin",
                TotalPrice = new Model.GenericPrice { Cost = 0 },
                Weight = 100,
                Groups = groups,
                Parcels = new Dictionary<int, SolexModel.Parcel>
                {
                    {
                        0,
                        new SolexModel.Parcel()
                        {
                            ShippingMethodId = 9,
                            Source = new SolexModel.Source()
                            {
                                DeliveryDateMax = DateTime.Now,
                                DeliveryDateMin= DateTime.Now,
                                ShippingDate = DateTime.Now,
                                ShippingLocation = new SolexModel.ShippingLocation() { Type = "SHOP", Identifier = string.Format("{0:0000}", shopInfoData.Store.RefUG) },
                                StockLocation = new SolexModel.StockLocation() { Type = "SHOP", Identifier = string.Format("{0:0000}", shopInfoData.Store.RefUG) }
                            }
                        }
                    }
                }
            };

            foreach (var lineGroup in lineGroups)
            {
                foreach (var article in lineGroup.Articles)
                {
                    if (!article.LogType.HasValue)
                    {
                        continue;
                    }

                    var logisticType = article.LogType.Value;
                    var currentGroup = groups.FirstOrDefault(x => x.Id == logisticType);

                    if (currentGroup == null)
                    {
                        currentGroup = new SolexModel.Group()
                        {
                            Id = logisticType,
                            Items = new List<SolexModel.ItemInGroup>()
                        };

                        groups.Add(currentGroup);
                    }

                    (currentGroup.Items as IList<SolexModel.ItemInGroup>).Add(new SolexModel.ItemInGroup
                    {
                        Identifier = new SolexModel.Identifier() { Prid = article.ProductID.GetValueOrDefault() },
                        ParcelId = 0,
                        Quantity = article.Quantity.GetValueOrDefault()
                    });
                }
            }

            return choice;
        }

        public void UpdateSelectedShippingMethodEvaluationItem(int shippingMethodId, int sellerId, ShippingMethodEvaluation shippingMethodEvaluation)
        {
            var shippingMethodEvaluationGroup = shippingMethodEvaluation.GetShippingMethodEvaluationGroup(sellerId).Value;

            var shippingMethodEvaluationType = shippingMethodEvaluationGroup.GetShippingMethodEvaluationTypeForShippingMethodId(shippingMethodId);

            shippingMethodEvaluationGroup.SelectShippingMethodEvaluationType(shippingMethodEvaluationType);

            shippingMethodEvaluationGroup.ShippingMethodEvaluationItems
                                           .First(x => x.ShippingMethodEvaluationType == shippingMethodEvaluationType)
                                           .SelectedShippingMethodId = shippingMethodId;

            shippingMethodEvaluationGroup.SynchronizeSelectedShippingMethodEvaluationTypeWithSelectedShippingMethod();

        }

        public bool IsDummyAddress(int sellerId, ShippingMethodEvaluation shippingMethodEvaluation)
        {
            var shippingMethodEvaluationGroup = shippingMethodEvaluation.GetShippingMethodEvaluationGroup(sellerId).Value;
            return shippingMethodEvaluationGroup.PostalShippingMethodEvaluationItem.HasValue && shippingMethodEvaluationGroup.PostalShippingMethodEvaluationItem.Value.IsDummyAddress;
        }

        public bool IsArticleShippedInStore(Article article, ShippingMethodEvaluationGroup shippingMethodEvaluationGroup)
        {
            var isSelectedShippingMethodShopType = shippingMethodEvaluationGroup.SelectedShippingMethodEvaluationType == ShippingMethodEvaluationType.Shop;

            var selectedChoice = shippingMethodEvaluationGroup.GetSelectedChoice();
            if (selectedChoice == null)
            {
                return isSelectedShippingMethodShopType;
            }

            var parcel = selectedChoice.GetParcel(article);
            if (parcel == null)
            {
                return isSelectedShippingMethodShopType;
            }

            return (isSelectedShippingMethodShopType && parcel.ShippingMethodId == selectedChoice.MainShippingMethodId)
                || _storeShippingMethodIds.Contains(parcel.ShippingMethodId);
        }
    }
}
