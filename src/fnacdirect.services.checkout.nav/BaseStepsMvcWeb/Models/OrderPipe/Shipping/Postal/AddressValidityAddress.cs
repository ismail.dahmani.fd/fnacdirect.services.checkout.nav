using FnacDirect.Contracts.Online.Model;

namespace FnacDirect.OrderPipe.Base.Business.Shipping.Postal
{
    public class AddressValidityAddress : IAddress
    {
        public AddressValidityAddress(IAddress address)
        {
            Company = address.Company;
            GenderId = address.GenderId;
            FirstName = address.FirstName;
            LastName = address.LastName;
            AddressLine1 = address.AddressLine1;
            AddressLine2 = address.AddressLine2;
            AddressLine3 = address.AddressLine3;
            AddressLine4 = address.AddressLine4;
            Zipcode = address.Zipcode;
            City = address.City;
            State = address.State;
            CountryId = address.CountryId;
            Tel = address.Tel;
            Fax = address.Fax;
            CellPhone = address.CellPhone;
        }

        public string Company { get; set;  }
        public int GenderId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string AddressLine3 { get; set; }
        public string AddressLine4 { get; set; }
        public string Zipcode { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public int CountryId { get; set; }
        public string Tel { get; set; }
        public string Fax { get; set; }
        public string CellPhone { get; set; }
    }
}
