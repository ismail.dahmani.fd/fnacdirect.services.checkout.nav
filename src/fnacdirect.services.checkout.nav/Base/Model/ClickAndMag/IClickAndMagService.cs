﻿namespace FnacDirect.OrderPipe.Base.Proxy
{
    public interface IClickAndMagService
    {
        string GetClientId(string serviceId);
    }
}
