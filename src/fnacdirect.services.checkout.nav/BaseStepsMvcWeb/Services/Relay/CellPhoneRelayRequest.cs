namespace FnacDirect.OrderPipe.BaseMvc.Web.Pop.ApiControllers.Models
{
    public class CellPhoneRelayRequest
    {
        public string CellPhone { get; set; }
    }
}
