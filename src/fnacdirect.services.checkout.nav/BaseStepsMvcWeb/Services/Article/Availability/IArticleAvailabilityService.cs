using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Basket;
using System;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Services.Article.Availability
{
    public interface IArticleAvailabilityService
    {
        AvailabilityViewModel GetAvailability(StandardArticle standardArticle, Func<StandardArticle, AvailabilityViewModel> onComputingAvailability = null);
    }
}
