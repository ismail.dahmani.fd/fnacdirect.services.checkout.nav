using FnacDirect.OrderPipe.Base.Business.FDV;
using FnacDirect.OrderPipe.Base.Business.LogisticLineGroups.Internals.Initializers;
using FnacDirect.OrderPipe.Base.LineGroups;
using FnacDirect.OrderPipe.Base.Model;
using System;
using System.Collections.Generic;
using System.Linq;

namespace FnacDirect.OrderPipe.Base.Business.LogisticLineGroups.Initializers
{
    public class ShopSaleLogisticLineGroupInitializer : BaseLogisticLineGroupInitializer
    {
        private readonly IGuptHelperService _guptHelperService;
        private readonly ILogisticLineGroupGenerator _logisticLineGroupGenerator;
        private readonly ISwitchProvider _switchProvider;

        public ShopSaleLogisticLineGroupInitializer(
            IGuptHelperService guptHelperService,
            ILogisticLineGroupGenerator logisticLineGroupGenerator,
            ISwitchProvider switchProvider
        )
        {
            _guptHelperService = guptHelperService;
            _logisticLineGroupGenerator = logisticLineGroupGenerator;
            _switchProvider = switchProvider;
        }

        public override IEnumerable<ILogisticLineGroup> Initialize(IDictionary<ILineGroupHeader, IEnumerable<Article>> selectedLineGroups)
        {
            var output = new List<ILogisticLineGroup>();

            if (!selectedLineGroups.Any())
            {
                return output;
            }

            var guptHelperResult = _guptHelperService.Process(selectedLineGroups.Values.SelectMany(a => a).OfType<StandardArticle>());

            // Ne devrait pas arriver, blocage fait en amont
            if (!guptHelperResult.CanProcessBasket)
            {
                throw new Exception("Le panier n'est pas réalisable, impossible de créer les LogisticLineGroup.");
            }

            // Pour le libre service, indique s'il faut séparer les PE et PT dans des LogisticLineGroup différents
            var separateEditorialAndProductForSelfService = _switchProvider.IsEnabled("orderpipe.pop.basket.separateEditorialAndProductForSelfService", false);

            // LogisticLineGroup hors GUPT intra mag libre service
            LogisticLineGroup notGuptLlg = null;
            // LogisticLineGroup GUPT intra mag libre service
            LogisticLineGroup guptSelfServiceLlg = null;
            // LogisticLineGroup GUPT intra mag stocké
            LogisticLineGroup guptStockLlg = null;

            foreach (var lineGroupHeader in selectedLineGroups.Keys)
            {
                var articles = selectedLineGroups[lineGroupHeader].OfType<StandardArticle>();
                var editorialProducts = articles.Where(a => !a.IsStockManagedByGupt);
                // Articles GUPT en libre service
                var technicalProductsSelfService = articles
                    .Where(a => a.IsStockManagedByGupt && !_guptHelperService.GetGuptManagedSellerTypes().Contains(SellerIds.GetType(a.SellerID.GetValueOrDefault())));
                // Articles GUPT en stocké
                var technicalProductsStock = articles
                    .Where(a => a.IsStockManagedByGupt && _guptHelperService.GetGuptManagedSellerTypes().Contains(SellerIds.GetType(a.SellerID.GetValueOrDefault())));
                
                // Produits hors GUPT
                if (editorialProducts.Any())
                {
                    // Les non GUPT sont dans leur propre LLG
                    if (separateEditorialAndProductForSelfService)
                    {
                        SetLogisticLineGroup(ref notGuptLlg, lineGroupHeader, editorialProducts, true);
                    }
                    // Les non GUPT sont dans le même LLG que les GUPT en libre service
                    else if (!separateEditorialAndProductForSelfService)
                    {
                        SetLogisticLineGroup(ref guptSelfServiceLlg, lineGroupHeader, editorialProducts, true);
                    }
                }

                // Produits GUPT libre service
                if (technicalProductsSelfService.Any())
                {
                    // Les GUPT sont dans le LLG libre service
                    if (!guptHelperResult.ShouldPutSelfServiceItemsInShopStockLineGroups)
                    {
                        SetLogisticLineGroup(ref guptSelfServiceLlg, lineGroupHeader, technicalProductsSelfService, true);
                    }
                    // Les GUPT sont dans le LLG stocké
                    else if (guptHelperResult.ShouldPutSelfServiceItemsInShopStockLineGroups)
                    {
                        SetLogisticLineGroup(ref guptStockLlg, lineGroupHeader, technicalProductsSelfService, false);
                    }
                }

                // Produits GUPT stockés
                if (technicalProductsStock.Any())
                {
                    SetLogisticLineGroup(ref guptStockLlg, lineGroupHeader, technicalProductsStock, false);
                }
            }

            // Ajout des LogisticLineGroup initialisés
            if (notGuptLlg?.Articles?.Any() == true)
            {
                output.Add(notGuptLlg);
            }
            if (guptSelfServiceLlg?.Articles?.Any() == true)
            {
                output.Add(guptSelfServiceLlg);
            }
            if (guptStockLlg?.Articles?.Any() == true)
            {
                output.Add(guptStockLlg);
            }

            return output;
        }

        private void SetLogisticLineGroup(ref LogisticLineGroup logisticLineGroup, ILineGroupHeader lineGroupHeader, IEnumerable<Article> articles, bool isSelfService)
        {
            if (lineGroupHeader == null || articles == null || !articles.Any())
            {
                return;
            }

            // Création
            if (logisticLineGroup == null)
            {
                logisticLineGroup = _logisticLineGroupGenerator.Generate(lineGroupHeader);
                logisticLineGroup.Articles.AddRange(articles);
                logisticLineGroup.Informations.OrderType = (int)(isSelfService ? OrderInfoOrderTypeEnum.IntramagPE : OrderInfoOrderTypeEnum.Intramag);
                logisticLineGroup.OrderType = isSelfService ? OrderTypeEnum.IntraMagPE : OrderTypeEnum.IntraMag;

                Helper.CopyShippingMethodEvaluation(lineGroupHeader, logisticLineGroup, Context.IGotShippingMethodEvaluation.ShippingMethodEvaluation);
            }
            // Fusion
            else
            {
                logisticLineGroup.Articles.AddRange(articles);
                logisticLineGroup.RemainingTotal += lineGroupHeader.RemainingTotal;
                if (logisticLineGroup.GlobalPrices != null && lineGroupHeader.GlobalPrices != null)
                {
                    logisticLineGroup.GlobalPrices.TotalEcoTaxEur += lineGroupHeader.GlobalPrices.TotalEcoTaxEur;
                    logisticLineGroup.GlobalPrices.TotalEcoTaxEurNoVAT += lineGroupHeader.GlobalPrices.TotalEcoTaxEurNoVAT;
                    logisticLineGroup.GlobalPrices.TotalPriceEur += lineGroupHeader.GlobalPrices.TotalPriceEur;
                    logisticLineGroup.GlobalPrices.TotalShoppingCartBusinessPriceDBEur += lineGroupHeader.GlobalPrices.TotalShoppingCartBusinessPriceDBEur;
                    logisticLineGroup.GlobalPrices.TotalShoppingCartBusinessPriceNoVatEur += lineGroupHeader.GlobalPrices.TotalShoppingCartBusinessPriceNoVatEur;
                    logisticLineGroup.GlobalPrices.TotalShoppingCartBusinessPriceUserEur += lineGroupHeader.GlobalPrices.TotalShoppingCartBusinessPriceUserEur;
                }
            }

            if (logisticLineGroup.SelectedShippingChoice != null)
            {
                foreach (var article in logisticLineGroup.Articles.OfType<StandardArticle>())
                {
                    article.ShipMethod = logisticLineGroup.SelectedShippingChoice.MainShippingMethodId;
                }
            }
        }
    }
}
