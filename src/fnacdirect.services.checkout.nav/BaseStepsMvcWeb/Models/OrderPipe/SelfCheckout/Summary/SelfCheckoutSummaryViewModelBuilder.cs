using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.OrderPipe.Base.Model.LineGroups;
using FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Basket;
using FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Common.Payment;
using FnacDirect.Technical.Framework.Web.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Pop.Models.SelfCheckout
{
    public class SelfCheckoutSummaryViewModelBuilder : ISelfCheckoutSummaryViewModelBuilder
    {
        private readonly IPriceHelper _priceHelper;
        private readonly ISplitViewModelBuilder _splitViewModelBuilder;
        private readonly ICustomerCreditCardBaseViewModelBuilder _customerCreditCardBaseViewModelBuilder;
        private readonly ITermsAndConditionsBuilder _termsAndConditionsBuilder;

        public SelfCheckoutSummaryViewModelBuilder(IApplicationContext applicationContext,
                                                  ISplitViewModelBuilder splitViewModelBuilder,
                                                  ITermsAndConditionsBuilder termsAndConditionsBuilder,
                                                  ICustomerCreditCardBaseViewModelBuilder customerCreditCardBaseViewModelBuilder)
        {
            _priceHelper = applicationContext.GetPriceHelper();
            _splitViewModelBuilder = splitViewModelBuilder;
            _termsAndConditionsBuilder = termsAndConditionsBuilder;
            _customerCreditCardBaseViewModelBuilder = customerCreditCardBaseViewModelBuilder;
        }

        /// <summary>
        /// This Method will build the view model return by the PoApiSelfCheckoutController based on the orderform 
        /// </summary>
        public SelfCheckoutSummaryViewModel Build(PopOrderForm popOrderForm)
        {
            var numberOfProducts = popOrderForm.GetTotalArticleQuantities();

            var totalGlobal = CalculateTotalGlobal(popOrderForm);

            var totalGlobalString = _priceHelper.Format(totalGlobal, PriceFormat.CurrencyAsDecimalSeparator);

            var totalGlobalModel = _priceHelper.GetDetails(totalGlobal);

            var userReference = popOrderForm.UserContextInformations.UID;

            var customerCreditCardBase = _customerCreditCardBaseViewModelBuilder.Build(userReference);

            var termsAndConditions = _termsAndConditionsBuilder.GetTermsAndConditionsFor(popOrderForm.LineGroups);

            var selfCheckoutSummaryViewModel = new SelfCheckoutSummaryViewModel
            {
                ProductsCount = numberOfProducts,
                TotalGlobal = totalGlobalString,
                CreditCard = customerCreditCardBase,
                TermsAndConditions = termsAndConditions,
                TotalGlobalModel = totalGlobalModel
            };

            return selfCheckoutSummaryViewModel;
        }

        /// <summary>
        /// Calculate global cost of the orderForm: total, shipping, split, wrap
        /// </summary>
        private decimal CalculateTotalGlobal(PopOrderForm popOrderForm)
        {
            var splitCost = _splitViewModelBuilder.SplitCost(popOrderForm);
            var wrapCost = popOrderForm.LineGroups
                                       .Fnac()
                                       .Sum(lg => lg.GlobalPrices.TotalShoppingCartBusinessWrapPriceUserEur.GetValueOrDefault());

            var standardArticles = popOrderForm.LineGroups
                                               .GetArticles()
                                               .Where(a => a is StandardArticle)
                                               .Cast<StandardArticle>();

            var totalCost = GetTotalCost(standardArticles);

            var totalShippingCost = popOrderForm.LineGroups.Sum(lg => lg.GlobalPrices.TotalFnacShipPriceUserEur.GetValueOrDefault());

            var totalGlobal = totalCost + totalShippingCost + splitCost + wrapCost;            

            return totalGlobal;
        }

        /// <summary>
        /// Calculate total cost of the basket (articles + services)
        /// </summary>
        private decimal GetTotalCost(IEnumerable<StandardArticle> standardArticles)
        {
            var totalCost = 0m;

            foreach (var article in standardArticles)
            {
                var servicesTotalCost = 0m;

                foreach (var service in article.Services)
                {
                    servicesTotalCost += service.PriceUserEur.GetValueOrDefault(0m) * service.Quantity.GetValueOrDefault(1);
                }

                totalCost += (article.PriceUserEur.GetValueOrDefault(0m) + article.EcoTaxEur.GetValueOrDefault(0m)) * article.Quantity.GetValueOrDefault(1) + servicesTotalCost;
            }

            return totalCost;
        }
    }
}
