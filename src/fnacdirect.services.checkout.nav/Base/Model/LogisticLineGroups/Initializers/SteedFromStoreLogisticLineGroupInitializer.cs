using System.Collections.Generic;
using System.Linq;
using FnacDirect.OrderPipe.Base.LineGroups;
using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.OrderPipe.Base.Business.LogisticLineGroups.Handlers;
using FnacDirect.Technical.Framework.Web.Mvc;
using FnacDirect.OrderPipe.Base.Business.Shipping;
using FnacDirect.OrderPipe.Core.Services;

namespace FnacDirect.OrderPipe.Base.Business.LogisticLineGroups.Initializers
{
    public class SteedFromStoreLogisticLineGroupInitializer : ClickAndCollectLogisticLineGroupInitializer
    {
        public SteedFromStoreLogisticLineGroupInitializer(IApplicationContext applicationContext,
                                                   ILogisticLineGroupGenerator logisticLineGroupGenerator,
                                                   IShippingMethodBasketServiceProvider shippingMethodBasketServiceProvider,
                                                   IPipeParametersProvider pipeParametersProvider)
            : base(applicationContext, logisticLineGroupGenerator, shippingMethodBasketServiceProvider, pipeParametersProvider)
        {
        }

        public override IEnumerable<ILogisticLineGroup> Initialize(IDictionary<ILineGroupHeader, IEnumerable<Article>> selectedLineGroups)
        {
            var logisticLineGroups = base.Initialize(selectedLineGroups);

            foreach (var logisticLineGroup in logisticLineGroups.OfType<LogisticLineGroup>())
            {
                logisticLineGroup.IsSteedFromStore = true;
            }

            if (logisticLineGroups.Any())
            {
                var sourceIds = selectedLineGroups.Keys.Select(l => l.UniqueId).Distinct().ToList();
                var destinationIds = logisticLineGroups.Select(l => l.UniqueId).Distinct().ToList();

                Context.AddProcessCompletedHandler(new DispatchFnacShippingFeesEquallyProcessCompletedHandlerDescriptor(sourceIds.Distinct(), destinationIds.Distinct()));
            }

            return logisticLineGroups;
        }

        public override void ResetShippingMethodChoices(LogisticLineGroup logisticLineGroup)
        {
            // Do Nothing: we keep ShippingMethodChoice property of the logisticLineGroup initialized by ShippingMethodEvaluation: Helper.CopyShippingMethodEvaluation(lineGroup, logisticLineGroup, shippingMethodEvaluation)
        }
    }
}
