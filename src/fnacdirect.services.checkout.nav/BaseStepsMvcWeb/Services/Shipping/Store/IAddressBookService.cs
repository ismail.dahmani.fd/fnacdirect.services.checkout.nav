
using FnacDirect.Customer.Model;
using System.Collections.Generic;

namespace FnacDirect.OrderPipe.Base.Proxy.Customer
{
    public interface IAddressBookService
    {
        void AddAddress(AddressEntity addressEntity);
        void DeleteAddress(AddressEntity addressEntity);
        AddressEntity GetAddressByAddressBookReference(string addressReference);
        IEnumerable<AddressEntity> GetAddresses();
        IEnumerable<AddressEntity> GetBillingAddresses();
        AddressEntity GetDefaultBillingAddress();
        ShippingAddressEntity GetDefaultShippingAddress();
        IEnumerable<AddressEntity> GetShippingAddresses();
        void UpdateAddress(AddressEntity addressEntity);
    }
}
