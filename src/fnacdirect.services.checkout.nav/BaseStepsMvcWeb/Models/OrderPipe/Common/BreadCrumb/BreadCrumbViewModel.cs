﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Common
{
    public class BreadCrumbViewModel
    {
        private readonly List<BreadCrumbItemViewModel> _viewModels
            = new List<BreadCrumbItemViewModel>();

        public IReadOnlyCollection<BreadCrumbItemViewModel> Items
        {
            get
            {
                return _viewModels.AsReadOnly();
            }
        }

        public BreadCrumbViewModel WithItem(params BreadCrumbItemViewModel[] items)
        {
            _viewModels.AddRange(items);

            return this;
        }
    }
}
