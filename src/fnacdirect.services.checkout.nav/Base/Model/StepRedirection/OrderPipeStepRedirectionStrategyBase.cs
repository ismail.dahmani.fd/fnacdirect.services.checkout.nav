﻿using FnacDirect.OrderPipe.Config;

namespace FnacDirect.OrderPipe.CoreRouting
{
    public abstract class OrderPipeStepRedirectionStrategyBase<TDescriptor> 
        : IOrderPipeStepRedirectionStrategy<TDescriptor>
        where TDescriptor : class, IUiDescriptor
    {
        public abstract string DoRedirectionFor(TDescriptor descriptor, OPContext opContext);

        public string DoRedirectionFor(IUiDescriptor uiDescriptor, OPContext opContext)
        {
            return DoRedirectionFor(uiDescriptor as TDescriptor, opContext);
        }
    }
}
