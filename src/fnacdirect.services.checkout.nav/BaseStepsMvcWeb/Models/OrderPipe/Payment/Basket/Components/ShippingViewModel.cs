﻿using FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Common.Shipping;
using FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Common.Shipping.Relay;
using FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Common.Shipping.Shop;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Payment
{
    public interface IShippingViewModel
    {
        string ShippingCost { get; set; }
        string Link { get; set; }
    }

    public abstract class ShippingBaseViewModel : IShippingViewModel
    {
        public string ShippingCost { get; set; }
        public string Link { get; set; }
    }

    public class PostalShippingViewModel : ShippingBaseViewModel
    {
        private readonly AddressViewModel _address;

        public AddressViewModel Address
        {
            get
            {
                return _address;
            }
        }

        public PostalShippingViewModel(AddressViewModel addressViewModel)
        {
            _address = addressViewModel;
        }
    }

    public class ShopShippingViewModel : ShippingBaseViewModel
    {
        private readonly ShopAddressViewModel _address;

        public ShopAddressViewModel Address
        {
            get
            {
                return _address;
            }
        }

        public ShopShippingViewModel(ShopAddressViewModel shopAddressViewModel)
        {
            _address = shopAddressViewModel;
        }
    }

    public class RelayShippingViewModel : ShippingBaseViewModel
    {
        private readonly RelayAddressViewModel _address;

        public RelayAddressViewModel Address
        {
            get
            {
                return _address;
            }
        }

        public RelayShippingViewModel(RelayAddressViewModel relayAddressViewModel)
        {
            _address = relayAddressViewModel;
        }
    }
}
    