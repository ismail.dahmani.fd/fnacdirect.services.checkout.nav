namespace FnacDirect.OrderPipe.Base.Business.FDV
{
    /// <summary>
    /// Classe indiquant le comportement à adopter en prévision de l'appel de la GUPT et la composition des LogisticLineGroups.
    /// </summary>
    public class GuptHelperResult
    {
        /// <summary>Indique si le panier peut être traité</summary>
        /// <example>Si le nombre d'articles techniques n'est pas gérable, le panier ne pourra pas être traité</example>
        public bool CanProcessBasket { get; set; }

        /// <summary>Indique si le paiement en caisse est possible</summary>
        public bool CanUseCheckoutBillingMethod { get; set; }

        /// <summary>Indique la future présence d'un LogisticLineGroups intra mag libre service</summary>
        public bool ShouldHaveShopSelfServiceLineGroups { get; set; }

        /// <summary>Indique la future présence d'un LogisticLineGroups intra mag stocké</summary>
        public bool ShouldHaveShopStockLineGroups { get; set; }

        /// <summary>Indique si les articles en libre service doivent être dans le LogisticLineGroups intra mag stocké</summary>
        public bool ShouldPutSelfServiceItemsInShopStockLineGroups { get; set; }
    }
}
