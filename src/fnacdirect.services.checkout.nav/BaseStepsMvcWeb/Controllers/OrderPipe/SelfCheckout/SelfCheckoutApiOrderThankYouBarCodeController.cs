using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.OrderPipe.CoreMvcWeb.Http;
using FnacDirect.OrderPipe.CoreMvcWeb.Routing;
using FnacDirect.Technical.Framework.Web.BarcodeGeneration;
using System.Web.Http;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Pop.Controllers
{
    [RoutePrefix("orderpipe/api/v1/{pipe}/thankyou")]
    public class SelfCheckoutApiOrderThankYouBarCodeController : OrderPipeApiController
    {
        private readonly IBarcodeGenerator _barcodeGenerator;

        public SelfCheckoutApiOrderThankYouBarCodeController(IBarcodeGeneratorFactory barcodeGeneratorFactory)
        {
            _barcodeGenerator = barcodeGeneratorFactory.Get(BarcodeType.Code128);
        }

        [Route("BarCode"), HttpGet]
        public IHttpActionResult BarCode([FromOpContext]PopOrderForm popOrderForm)
        {
            var reference = popOrderForm.OrderGlobalInformations.MainOrderUserReference;

            if (reference != null)
            {
                return this.Png(_barcodeGenerator.GetBarcode(reference));
            }
            else
            {
                return null;
            }
        }
    }
}
