using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.OrderPipe.Base.Model.Facets;
using FnacDirect.OrderPipe.Base.Model.LineGroups;
using FnacDirect.OrderPipe.Base.Model.UserInformations;
using System.Collections.Generic;

namespace FnacDirect.OrderPipe.Base.Business.Payment
{
    public class BillingMethodService : IBillingMethodService
    {
        private readonly IAvoirOccazPaymentService _avoirOccazPaymentService;
        private readonly IBankPaymentService _bankPaymentService;
        private readonly ICUUPaymentService _cuuPaymentService;
        private readonly IDeferredPaymentService _deferredPaymentService;
        private readonly IFidelityEurosPaymentService _fidelityEurosPaymentService;
        private readonly IFidelityPointsOccazPaymentService _fidelityPointsOccazPaymentService;
        private readonly IGiftPaymentService _giftPaymentService;
        private readonly IOgonePaymentService _ogonePaymentService;
        private readonly IOrangePaymentService _orangePaymentService;
        private readonly IPaypalPaymentService _paypalPaymentService;
        private readonly IPhonePaymentService _phonePaymentService;
        private readonly ICheckArticleService _checkArticleService;
        private readonly IMBWayPaymentService _mbwayPaymentService;


        public BillingMethodService(IAvoirOccazPaymentService avoirOccazPaymentService,
            IBankPaymentService bankPaymentService,
            ICUUPaymentService cuuPaymentService,
            IDeferredPaymentService deferredPaymentService,
            IFidelityEurosPaymentService fidelityEurosPaymentService,
            IFidelityPointsOccazPaymentService fidelityPointsOccazPaymentService,
            IGiftPaymentService giftPaymentService,
            IOgonePaymentService ogonePaymentService,
            IOrangePaymentService orangePaymentService,
            IPaypalPaymentService paypalPaymentService,
            IPhonePaymentService phonePaymentService,
            ICheckArticleService checkArticleService,
            IMBWayPaymentService mbwayPaymentService
            )
        {
            _avoirOccazPaymentService = avoirOccazPaymentService;
            _bankPaymentService = bankPaymentService;
            _cuuPaymentService = cuuPaymentService;
            _deferredPaymentService = deferredPaymentService;
            _fidelityEurosPaymentService = fidelityEurosPaymentService;
            _fidelityPointsOccazPaymentService = fidelityPointsOccazPaymentService;
            _giftPaymentService = giftPaymentService;
            _ogonePaymentService = ogonePaymentService;
            _orangePaymentService = orangePaymentService;
            _paypalPaymentService = paypalPaymentService;
            _phonePaymentService = phonePaymentService;
            _checkArticleService = checkArticleService;
            _mbwayPaymentService = mbwayPaymentService;

        }

        public bool HasSensibleSupportId(IEnumerable<Article> articles)
        {
            return _checkArticleService.HasSensibleSupportId(articles);
        }

        public bool IsPhonePaymentAllowed(OPContext opContext, PopOrderForm popOrderForm, bool disableEccvCheck = false)
        {
            return _phonePaymentService.IsAllowed(opContext, popOrderForm, disableEccvCheck);
        }

        public bool IsCuuAllowed(IGotLineGroups iGotLineGroups, bool disableClickAndCollectCheck = false, bool disableEccvCheck = false)
        {
            return _cuuPaymentService.IsAllowed(iGotLineGroups, disableClickAndCollectCheck, disableEccvCheck);
        }

        public bool IsCcvAllowed(PopOrderForm popOrderForm, IEnumerable<ILineGroup> lineGroups, decimal totalPriceEur, int anonymousBillingMethodsMaxAmount, bool isGuest, bool disableClickAndCollectCheck = false, bool disableMaxAmount = false, bool disableEccvCheck = false)
        {
            return _giftPaymentService.IsCcvAllowed(popOrderForm, lineGroups, totalPriceEur, anonymousBillingMethodsMaxAmount, isGuest, disableClickAndCollectCheck, disableMaxAmount, disableEccvCheck);
        }

        public bool IsGiftCardAllowed(IGotLineGroups iGotLineGroups, decimal totalPriceEur, int anonymousBillingMethodsMaxAmount, bool isGuest, bool disableClickAndCollectCheck = false, bool disableMaxAmount = false, bool disableEccvCheck = false)
        {
            return _giftPaymentService.IsGiftCardAllowed(iGotLineGroups, totalPriceEur, anonymousBillingMethodsMaxAmount, isGuest, disableClickAndCollectCheck, disableMaxAmount, disableEccvCheck);
        }

        public bool IsFidelityEurosAllowed(IGotLineGroups iGotLineGroups, IGotBillingInformations iGotBillingInformations, int delayAfterExpiration, decimal multipleAmount, bool disableEccvCheck = false)
        {
            return _fidelityEurosPaymentService.IsAllowed(iGotLineGroups, iGotBillingInformations, delayAfterExpiration, multipleAmount, disableEccvCheck);
        }

        public bool IsFidelityPointsOccazAllowed(IGotBillingInformations iGotBillingInformations, IGotLineGroups iGotLineGroups, IUserContextInformations userContext, bool disableClickAndCollectCheck = false, bool disableEccvCheck = false)
        {
            return _fidelityPointsOccazPaymentService.IsAllowed(iGotBillingInformations, iGotLineGroups, userContext, disableClickAndCollectCheck, disableEccvCheck);
        }

        public bool IsDeferredPaymentAllowed(PopOrderForm popOrderForm, IGotBillingInformations iGotBillingInformations, IGotUserContextInformations iGotUserContextInformations)
        {
            return _deferredPaymentService.IsAllowed(popOrderForm, iGotBillingInformations, iGotUserContextInformations);
        }

        public bool IsAvoirOccazAllowed(IGotLineGroups iGotLineGroups, int delayAfterExpiration, bool disableClickAndCollectCheck = false, bool disableEccvCheck = false)
        {
            return _avoirOccazPaymentService.IsAllowed(iGotLineGroups, delayAfterExpiration, disableClickAndCollectCheck, disableEccvCheck);
        }

        public bool IsPaypalAllowed(PopOrderForm popOrderForm)
        {
            return _paypalPaymentService.IsAllowed(popOrderForm);
        }

        public bool IsOgoneAllowed(IGotLineGroups iContainsLineGroups, IGotBillingInformations iContainsBillingInformations, IGotUserContextInformations iContainsUserContextInformations)
        {
            return _ogonePaymentService.IsAllowed(iContainsLineGroups, iContainsBillingInformations, iContainsUserContextInformations);
        }

        public bool IsOrangeAllowed(IGotBillingInformations iGotBillingInformations, IGotLineGroups iGotLineGroups, IGotShippingMethodEvaluation shippingMethodEvaluation)
        {
            return _orangePaymentService.IsAllowed(iGotBillingInformations, iGotLineGroups, shippingMethodEvaluation);
        }

        public bool IsCheckAllowed(IGotBillingInformations iGotBillingInformations, IGotLineGroups iGotLineGroups, IGotUserContextInformations iGotUserContextInformations, IGotShippingMethodEvaluation iGotShippingMethodEvaluation)
        {
            return _bankPaymentService.IsCheckAllowed(iGotBillingInformations, iGotLineGroups, iGotUserContextInformations, iGotShippingMethodEvaluation);
        }

        public bool IsBankAllowed(IGotLineGroups iGotLineGroups, IGotUserContextInformations iGotUserContextInformations, IGotShippingMethodEvaluation iGotShippingMethodEvaluation)
        {
            return _bankPaymentService.IsBankAllowed(iGotLineGroups, iGotUserContextInformations, iGotShippingMethodEvaluation);
        }
        public bool IsMBWayAllowed(PopOrderForm popOrderForm)
        {
            return _mbwayPaymentService.IsAllowed(popOrderForm);
        }
    }
}
