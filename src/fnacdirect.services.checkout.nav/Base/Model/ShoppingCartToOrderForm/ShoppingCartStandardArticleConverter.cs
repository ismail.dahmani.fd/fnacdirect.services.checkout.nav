﻿using FnacDirect.OrderPipe.Base.LineGroups;
using FnacDirect.OrderPipe.Base.Model;
using System.Linq;

namespace FnacDirect.OrderPipe.Base.Business.ArticleConversions.ShoppingCartToOrderForm
{
    public partial class ShoppingCartToOrderFormArticleConverter
    {
        protected virtual void ApplyCustomOn(ShoppingCartEntities.StandardArticle input, StandardArticle output)
        {
            output.Services = input.Services.Cast<ShoppingCartEntities.Service>()
                                       .Select(Convert<Service>)
                                       .Where(s => s != null)
                                       .ToServiceList();
        }

        protected TResult Convert<TResult>(ShoppingCartEntities.StandardArticle a, StandardArticle article = null)
          where TResult : StandardArticle, new()
        {
            StandardArticle art;

            if (article == null)
                art = new TResult();
            else
                art = article;

            art.InsertDate = a.InsertDate;
            art.OriginCode = a.OriginCode;
            art.ProductID = a.ProductID;
            art.Quantity = a.Quantity.GetValueOrDefault(1);

            art.SplitLevel = a.SplitLevel;

            art.FatherProductId = a.FatherProductId;
            art.InsuranceStart = a.InsuranceStart;
            art.IsPosterioriSelling = a.IsPosterioriSelling;
            art.PosterioriMasterSKU = a.PosterioriMasterSKU;
            art.RefInvoicePosterioriService = a.RefInvoicePosterioriService;
            art.InvoiceIdPosterioriService = a.InvoiceIdPosterioriService;
            ApplyCustomOn(a, art);

            if (art.Services.Count == 0)
            {
                art.Services = null;
            }

            art.OldPrice = a.OldPrice;
            art.OldPriceRecordedDate = a.OldPriceRecordedDate;

            art.CollectInShop = a.CollectState.Equals((int)ShoppingCartEntities.CollectInShopState.Immediate);
            art.CollectAvailabilityLabel = a.CollectAvailabilityLabel;
            art.ReferenceGU = a.ReferenceGU;
            art.ItemPhysicalType = a.ItemPhysicalType;
            if (a.ItemPhysicalType == FnacDirect.Contracts.Online.Model.PhysicalType.Numeric)
                art.SellerID = SellerIds.GetId(SellerType.Numerical);
            if (!string.IsNullOrEmpty(a.RemiseMotifRemise))
                art.Remise = new RemiseClient(a.RemiseOrderDiscount, a.RemiseMotifRemise, a.RemiseMontantRemise, a.RemiseCodeMotif);
            return (TResult)art;
        }

        protected StandardArticle Convert(ShoppingCartEntities.StandardArticle @from, StandardArticle article = null)
        {
            return Convert<StandardArticle>(@from, article);
        }
    }
}
