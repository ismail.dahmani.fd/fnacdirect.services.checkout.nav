﻿using FnacDirect.OrderPipe.Base.Business.LogisticLineGroups.Internals.Initializers;
using FnacDirect.OrderPipe.Base.LineGroups;
using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.Technical.Framework.Velocity;
using System.Collections.Generic;
using System.Linq;

namespace FnacDirect.OrderPipe.Base.Business.LogisticLineGroups.Initializers
{
    public class FnacB2BLogisticLineGroupInitializer : BaseLogisticLineGroupInitializer
    {
        private readonly ILogisticLineGroupGenerator _logisticLineGroupGenerator;

        public FnacB2BLogisticLineGroupInitializer(ILogisticLineGroupGenerator logisticLineGroupGenerator)
        {
            _logisticLineGroupGenerator = logisticLineGroupGenerator;
        }

        public override IEnumerable<ILogisticLineGroup> Initialize(IDictionary<ILineGroupHeader, IEnumerable<Article>> selectedLineGroups)
        {
            var shippingMethodEvaluation = Context.IGotShippingMethodEvaluation.ShippingMethodEvaluation;

            var output = new List<ILogisticLineGroup>();

            foreach (var lineGroup in selectedLineGroups.Keys)
            {
                var logisticLineGroup = _logisticLineGroupGenerator.Generate(lineGroup);

                logisticLineGroup.Informations.OrderType = (int)OrderInfoOrderTypeEnum.StandardPro;
                logisticLineGroup.OrderType = OrderTypeEnum.Pro;
                logisticLineGroup.Articles = new List<Article>(selectedLineGroups[lineGroup]);

                Helper.CopyShippingMethodEvaluation(lineGroup, logisticLineGroup, shippingMethodEvaluation);

                output.Add(logisticLineGroup);
            }

            return output;
        }
    }
}
