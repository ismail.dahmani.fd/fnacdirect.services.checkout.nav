namespace FnacDirect.OrderPipe.Base.Model.FDV
{
    public class PaymentTransactionResult
    {
        public PaymentTransactionResult()
        {
            TpeTransactionInfo = new TpeTransactionInfo();
            EuroFidTransactionInfo = new EuroFidTransactionInfo();
            GiftCardTransactionInfo = new GiftCardTransactionInfo();
        }

        public int Id { get; set; }

        public decimal Amount { get; set; }

        public int Status { get; set; }

        public bool IsSuccess { get; set; }

        public PaymentType PaymentType { get; set; }

        public string PaymentTypeLabel { get; set; }

        public TpeTransactionInfo TpeTransactionInfo { get; set; }

        public GiftCardTransactionInfo GiftCardTransactionInfo { get; set; }

        public EuroFidTransactionInfo EuroFidTransactionInfo { get; set; }
    }
}
