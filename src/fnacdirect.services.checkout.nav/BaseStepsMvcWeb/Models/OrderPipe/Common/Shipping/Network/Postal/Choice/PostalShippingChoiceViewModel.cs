using FnacDirect.OrderPipe.Base.Business.Availability;
using FnacDirect.OrderPipe.BaseMvc.Web.Pop.Models.Common;
using FnacDirect.Technical.Framework.Web.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using static FnacDirect.OrderPipe.Constants;


namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Common.Shipping.Postal
{
    public class PostalShippingChoiceViewModel : IBaseApiRootViewModel
    {
        public List<PipeMessageViewModel> PipeMessages { get; } = new List<PipeMessageViewModel>();

        public string ShippingCost { get; set; }

        public string ShippingCostNoVat { get; set; }

        public string ShippingCostToDisplay { get; set; }

        public PriceDetailsViewModel ShippingCostModel { get; set; }

        public PriceDetailsViewModel ShippingCostNoVatModel { get; set; }

        public PriceDetailsViewModel ShippingCostModelToDisplay { get; set; }

        public bool IsFreeShipping { get; set; }
        public IList<DelayModel> Delays { get; set; }
        public bool HasOneDelay { get; set; }
        public int MethodId { get; set; }
        public bool IsSelected { get; set; }
        public bool IsAvailable { get; set; }

        public string ShortEstimatedDeliveryDate { get; set; }
        public IEnumerable<PopAvailabilityProcessorOutput<PopAvailabilityProcessorOutput>> LongEstimatedDeliveryDate { get; set; }
        public DateTime? WorstDeliveryDate { get; set; }
        public bool BeforeXmas
        {
            get
            {
                return LongEstimatedDeliveryDate.SelectMany(a => a.Items).ComputeBeforeXmas();
            }
        }

        public string PictoLabel
        {
            get
            {
                return LongEstimatedDeliveryDate.SelectMany(a => a.Items).FirstOrDefault(a => a.BeforeXmas)?.PictoLabel;
            }
        }

        public bool HasManyLongEstimatedDeliveryDate
        {
            get
            {
                return LongEstimatedDeliveryDate.Count() > 1;
            }
        }

        public bool HasOneLongEstimatedDeliveryDateWithSeveralDates
        {
            get
            {
                return LongEstimatedDeliveryDate.Any(x => x.Items.Count() > 1);
            }
        }

        public bool DisplayLongEstimatedDeliveryDates { get; set; }

        public bool IsAppointment
        {
            get
            {
                return MethodId == ShippingMethods.Fly5Hours;
            }
        }

        public bool IsAppointmentDelivery { get; set; }

        public bool OpenAppointmentDeliveryCalendar { get; set; }

        public int? DefaultShippingMethodId { get; set; }

        public bool HasSelectedSlot { get; set; }

        public bool CanOpenDeliveryCalendar { get; set; }

        public int Weight { get; set; }

        public decimal TotalPrice { get; set; }

        public decimal? CarbonFootprint { get; set; }
    }

    public class DelayModel
    {
        public int NbProducts { get; set; }

        public string Delay { get; set; }

        public bool HasSeveralProducts
        {
            get
            {
                return NbProducts > 1;
            }
        }
    }
}
