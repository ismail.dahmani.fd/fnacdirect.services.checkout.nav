using FnacDirect.Context;
using FnacDirect.Contracts.Online.Model;
using FnacDirect.Front.WebBusiness;
using FnacDirect.Membership.Business.Common;
using FnacDirect.Membership.Model.Constants;
using FnacDirect.Membership.Model.Entities;
using FnacDirect.OrderPipe.Base.BaseModel.Model;
using FnacDirect.OrderPipe.Base.Business;
using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.OrderPipe.Base.Model.Adherent;
using FnacDirect.OrderPipe.Base.Model.ClickAndCollect;
using FnacDirect.OrderPipe.Base.Model.Facets;
using FnacDirect.OrderPipe.Base.Model.LineGroups;
using FnacDirect.OrderPipe.Base.Model.Pop;
using FnacDirect.OrderPipe.Base.Proxy.Membership;
using FnacDirect.OrderPipe.Base.Steps.Pop;
using FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Basket;
using FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Common.Basket;
using FnacDirect.Technical.Framework.Utils;
using FnacDirect.Technical.Framework.Web.Mvc;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using ArticleType = FnacDirect.OrderPipe.Base.Model.ArticleType;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Membership
{
    public class MembershipViewModelBuilder : IMembershipViewModelBuilder
    {
        private readonly IMembershipDateBusiness _dateBusiness;
        private readonly IPriceHelper _priceHelper;
        private readonly IW3ArticleService _w3ArticleService;
        private readonly IApplicationContext _applicationContext;
        private readonly IUserExtendedContext _userExtendedContext;
        private readonly IUserContextProvider _userContextProvider;
        private readonly IPricingService _pricingService;
        private readonly IMembershipConfigurationService _membershipConfigurationService;
        private readonly IMembershipCardBindingConfigurationBusiness _membershipCardBindingConfigurationBusiness;
        private readonly FidelityBusiness<FidelityEurosBillingMethod> _fidelityEurosBusiness;
        private readonly FidelityBusiness<AvoirOccazBillingMethod> _avoirOccazBusiness;
        private readonly FidelityBusiness<FidelityPointsOccazBillingMethod> _fidelityPointsOccazBusiness;
        private readonly IFnacPlusViewModelBuilder _fnacplusViewModelBuilder;
        private readonly ISwitchProvider _switchProvider;
        private readonly IDateTimeProvider _dateTimeProvider;

        private readonly int _youngMaxAge;
        private readonly int _delayBeforeExpiration;
        private readonly int _delayAfterExpiration;
        private readonly int _minAdvantageAmountToDisplayConfirmationPush;
        private readonly bool _isFib;

        public MembershipViewModelBuilder(IApplicationContext applicationContext,
            IUserExtendedContextProvider userExtendedContextProvider,
            IPricingServiceProvider pricingServiceProvider,
            IUserContextProvider userContextProvider,
            IMembershipConfigurationService membershipConfigurationService,
            IW3ArticleService w3ArticleService,
            IMembershipCardBindingConfigurationBusiness membershipCardBindingConfigurationBusiness,
            IMembershipDateBusiness dateBusiness,
            IFnacPlusViewModelBuilder fnacplusViewModelBuilder,
            ISwitchProvider switchProvider,
            IDateTimeProvider dateTimeProvider)
        {
            _priceHelper = applicationContext.GetPriceHelper();
            _membershipConfigurationService = membershipConfigurationService;
            _applicationContext = applicationContext;
            _userExtendedContext = (IUserExtendedContext)userExtendedContextProvider.GetCurrentUserExtendedContext();
            _pricingService = pricingServiceProvider.GetPricingService(applicationContext.GetSiteContext().MarketId);
            _fidelityEurosBusiness = new FidelityBusiness<FidelityEurosBillingMethod>();
            _avoirOccazBusiness = new FidelityBusiness<AvoirOccazBillingMethod>();
            _fidelityPointsOccazBusiness = new FidelityBusiness<FidelityPointsOccazBillingMethod>();
            _userContextProvider = userContextProvider;
            _delayBeforeExpiration = membershipConfigurationService.DelayBeforeExpiration; //ie: -180
            _delayAfterExpiration = membershipConfigurationService.DelayAfterExpiration; //ie: 365
            _w3ArticleService = w3ArticleService;
            _fnacplusViewModelBuilder = fnacplusViewModelBuilder;
            _isFib = _membershipConfigurationService.BusinessType() == BusinessType.Fib;
            _youngMaxAge = 25;
            _membershipCardBindingConfigurationBusiness = membershipCardBindingConfigurationBusiness;
            _dateBusiness = dateBusiness;
            _switchProvider = switchProvider;
            _dateTimeProvider = dateTimeProvider;

            var minString = System.Web.Configuration.WebConfigurationManager.AppSettings["pushFnacCard.minAdvantageAmountToDisplayConfirmationPush"];
            if (string.IsNullOrEmpty(minString) || !int.TryParse(minString, out _minAdvantageAmountToDisplayConfirmationPush))
                throw new Exception("La variable de configuration pushFnacCard.minAdvantageAmountToDisplayConfirmationPush n'est pas définit correctement");
        }

        /// <summary>
        /// Récupère les prix des prid passés en param
        /// </summary>
        /// <param name="prids"></param>
        /// <param name="popOrderForm"></param>
        /// <returns>Un Dico Prid,Price</returns>
        private IDictionary<int, decimal> GetArticlesPrice(IEnumerable<int> prids, PopOrderForm popOrderForm)
        {
            var dico = new Dictionary<int, decimal>();

            var articles = new ArticleList();

            articles.AddRange(prids.Select(prid => new StandardArticle
            {
                ProductID = prid,
                Type = ArticleType.Saleable,
                Quantity = 1
            }));

            var shoppingCart = _pricingService.GetArticlesPrices(_applicationContext.GetSiteContext(),
                _applicationContext.GetCountryId(),
                _userExtendedContext.ContractDto,
                new MultiFacet<IGotLineGroups, IGotMobileInformations, IGotBillingInformations, IGotShippingMethodEvaluation, IGotLogisticLineGroups>(popOrderForm, popOrderForm, popOrderForm, popOrderForm, popOrderForm),
                articles,
                null,
                GetArticlesPricesParameters.Default,
                useCache: true);

            foreach (var prid in prids)
            {
                var shoppingCartLineItem = shoppingCart.LineItems.FirstOrDefault(i => i.Reference.PRID.GetValueOrDefault() == prid);
                if (shoppingCartLineItem != null)
                {
                    dico.Add(prid, shoppingCartLineItem.SalesInfo.ReducedPrice.TTC);
                }
            }
            return dico;
        }

        public MembershipViewModel Build(PopOrderForm popOrderForm)
        {
            var adherentPush = popOrderForm.GetPrivateData<PushAdherentData>("PushAdherentGroup");
            int? adherentRenewCard = null;

            var pushType = PushType.None;
            var membershipViewModel = new MembershipViewModel();

            if (popOrderForm.UserContextInformations.IsGuest)
            {
                return membershipViewModel;
            }

            if (IsIdentified)
            {
                BuildProfil(popOrderForm, membershipViewModel);
            }

            membershipViewModel.Push.IsAdherent = _userExtendedContext.Customer.IsValidAdherent;

            membershipViewModel.Push.IsFnacPlusEnabled = _membershipConfigurationService.IsFnacPlusEnabled;

            membershipViewModel.Push.IsFnacPlusCanalPlayEnabled = _membershipConfigurationService.IsFnacPlusCanalPlayEnabled;

            if (membershipViewModel.Push.IsFnacPlusEnabled)
            {
                var fnacPlusData = popOrderForm.GetPrivateData<PushFnacPlusAdherentData>(create: false);

                if (fnacPlusData != null && fnacPlusData.CardPlusInfos != null)
                {
                    _fnacplusViewModelBuilder.Fill(membershipViewModel.Push, fnacPlusData);
                    pushType = GetFnacPlusPushType(popOrderForm, membershipViewModel.Push);
                }
            }
            else if (!_switchProvider.IsEnabled("orderpipe.adherents.RiposteFnacPlus.Enabled"))
            {
                pushType = GetPushType(popOrderForm);
            }

            if (pushType == PushType.None)
            {
                return membershipViewModel;
            }

            if (pushType == PushType.Renew)
            {
                adherentRenewCard = GetMembeshipRenewCardPridFromSeibel();

                if (!adherentRenewCard.HasValue && !membershipViewModel.Push.IsFnacPlusEnabled)
                {
                    return membershipViewModel;
                }
            }

            membershipViewModel.Push.Type = pushType;

            membershipViewModel.Push.IsTryCard = HasTryCard(popOrderForm);

            var isSimulationMode = pushType == PushType.Recruit || (pushType == PushType.Renew && IsObsoleteMembership);

            membershipViewModel.Push.IsIdentified = IsIdentified;

            membershipViewModel.Push.HasAdherentNumber = popOrderForm.UserContextInformations.CustomerEntity != null && !string.IsNullOrEmpty(popOrderForm.UserContextInformations.CustomerEntity.AdherentCardNumber);

            try
            {
                //Temporary method necessary while RiposteFnacPlus isn't activated in PRD
                FillTempShippingProperties(membershipViewModel.Push, popOrderForm);
            }
            catch (Exception) { }


            if (IsIdentified)
            {
                membershipViewModel.Push.IsMembershipAboutToExpire = IsMembershipAboutToExpire;

                membershipViewModel.Push.IsMembershipExpired = IsObsoleteMembership;

                var amountCagnotteFnac = _fidelityEurosBusiness.GetAvailableAmount(_userExtendedContext.Customer);
                var amountCagnotteAvoirGaming = _avoirOccazBusiness.GetAvailableAmount(_userExtendedContext.Customer);
                var amountCagnotteGaming = _fidelityPointsOccazBusiness.GetAvailableAmount(_userExtendedContext.Customer);

                if (amountCagnotteFnac > 0)
                {
                    var euroFirstExpirency = _fidelityEurosBusiness.GetFirstExpiry(_userExtendedContext.Customer);

                    membershipViewModel.Fnac = new MembershipFidelityAccountViewModel
                    {
                        AvailableAmount = _priceHelper.Format(amountCagnotteFnac, PriceFormat.CurrencyAsDecimalSeparator)
                    };
                    if (euroFirstExpirency != null && euroFirstExpirency.Amount > 0)
                    {
                        membershipViewModel.Fnac.FirstExpiryAmount = _priceHelper.Format(euroFirstExpirency.Amount, PriceFormat.CurrencyAsDecimalSeparator);
                        membershipViewModel.Fnac.FirstExpiryDate = euroFirstExpirency.ExpirationDate.ToShortDateString();
                    }
                }

                if (amountCagnotteAvoirGaming > 0)
                {
                    var avoirFirstExpirency = _avoirOccazBusiness.GetFirstExpiry(_userExtendedContext.Customer);

                    membershipViewModel.AvoirGaming = new MembershipFidelityAccountViewModel
                    {
                        AvailableAmount = _priceHelper.Format(amountCagnotteAvoirGaming, PriceFormat.CurrencyAsDecimalSeparator)
                    };
                    if (avoirFirstExpirency != null && avoirFirstExpirency.Amount > 0)
                    {
                        membershipViewModel.AvoirGaming.FirstExpiryAmount = _priceHelper.Format(avoirFirstExpirency.Amount, PriceFormat.CurrencyAsDecimalSeparator);
                        membershipViewModel.AvoirGaming.FirstExpiryDate = avoirFirstExpirency.ExpirationDate.ToShortDateString();
                    }
                }

                if (amountCagnotteGaming > 0)
                {
                    membershipViewModel.Gaming = new MembershipFidelityAccountViewModel
                    {
                        AvailableAmount = _priceHelper.Format(amountCagnotteGaming, PriceFormat.CurrencyAsDecimalSeparator)
                    };
                }
            }

            membershipViewModel.Profil.CardLabel = IsAdherentOne ? "One" : "Fnac";
            if (membershipViewModel.Push.IsFnacPlusEnabled)
            {
                membershipViewModel.Profil.CardLabel = "Fnac";
            }
            if (_switchProvider.IsEnabled("orderpipe.pop.adherents.eligibleinternationalcountry", false))
            {

                var _shipping = popOrderForm.ShippingMethodEvaluation.ShippingMethodEvaluationGroups.FirstOrDefault();
                var countrycode = string.Empty;
                if (_shipping != null && _shipping.ShippingMethodEvaluationItems.Any() && _shipping.ShippingMethodEvaluationItems.FirstOrDefault().ShippingAddress != null)
                {
                    countrycode = _shipping.ShippingMethodEvaluationItems.FirstOrDefault().ShippingAddress.CountryCode2;
                }

                if (_applicationContext.GetSiteContext().Country != countrycode)
                {
                    membershipViewModel.Push.IsShippingCostOffered = true;
                }
                else
                {
                    membershipViewModel.Push.IsShippingCostOffered = adherentPush.SimulateNonAdherentShippingCost != decimal.Zero && adherentPush.SimulateAdherentShippingCost == decimal.Zero;
                }
            }
            else
            {
                membershipViewModel.Push.IsShippingCostOffered = adherentPush.SimulateNonAdherentShippingCost != decimal.Zero && adherentPush.SimulateAdherentShippingCost == decimal.Zero;
            }

            membershipViewModel.Push.IsFreeShippingForNonAdherent = adherentPush.SimulateNonAdherentShippingCost == decimal.Zero;

            membershipViewModel.Push.ProfilKey = adherentPush.ProfilKey;

            membershipViewModel.Push.PromotionKey = adherentPush.PromotionKey;


            if (isSimulationMode)
            {
                membershipViewModel.Push.ImmediateDiscount = _priceHelper.Format(adherentPush.SimulateSavingAmount, PriceFormat.CurrencyAsDecimalSeparator);

                membershipViewModel.Push.DifferedDiscount = _priceHelper.Format(adherentPush.SimulateDifferedAmount, PriceFormat.CurrencyAsDecimalSeparator);

                membershipViewModel.Push.ShippingDiscount = _priceHelper.Format(adherentPush.SimulateSavingShippingCostAmount, PriceFormat.CurrencyAsDecimalSeparator);

                membershipViewModel.Push.HasDiscount = (adherentPush.SimulateSavingAmount + adherentPush.SimulateDifferedAmount > 0);

                membershipViewModel.Push.HasImmediatDiscount = adherentPush.SimulateSavingAmount > 0;

                membershipViewModel.Push.HasDifferedDiscount = adherentPush.SimulateDifferedAmount > 0;

                membershipViewModel.Push.DisplayShippingDiscount = adherentPush.SimulateSavingShippingCostAmount > 0;
            }
            else
            {
                membershipViewModel.Push.ImmediateDiscount = _priceHelper.Format(adherentPush.RealSavingAmount, PriceFormat.CurrencyAsDecimalSeparator);

                membershipViewModel.Push.DifferedDiscount = _priceHelper.Format(adherentPush.RealDifferedAmount, PriceFormat.CurrencyAsDecimalSeparator);

                membershipViewModel.Push.ShippingDiscount = _priceHelper.Format(adherentPush.RealSavingShippingCostAmount, PriceFormat.CurrencyAsDecimalSeparator);

                membershipViewModel.Push.HasDiscount = (adherentPush.RealSavingAmount + adherentPush.RealDifferedAmount > 0);

                membershipViewModel.Push.HasImmediatDiscount = adherentPush.RealSavingAmount > 0;

                membershipViewModel.Push.HasDifferedDiscount = adherentPush.RealDifferedAmount > 0;

                membershipViewModel.Push.DisplayShippingDiscount = adherentPush.RealSavingShippingCostAmount > 0;
            }

            membershipViewModel.Push.IsMixed = popOrderForm.IsMixed();

            if (pushType == PushType.Recruit || (pushType == PushType.Renew && adherentRenewCard != null))
            {
                var adherentSouscriptionCard = _membershipConfigurationService.GetAdherentCardByKey(!IsYoung || _isFib ? "siebel.adherent.carte.normal.newadh.3ans" : "siebel.adherent.carte.jeune.newadh.3ans");

                if (adherentSouscriptionCard != null)
                {
                    var adherentSouscriptionCardPrid = adherentSouscriptionCard.Prid;

                    var articlesPrice = GetArticlesPrice(new List<int> { adherentSouscriptionCardPrid, adherentRenewCard.GetValueOrDefault() }, popOrderForm);

                    if (pushType == PushType.Recruit && articlesPrice.ContainsKey(adherentSouscriptionCardPrid))
                    {
                        membershipViewModel.Push.CardPrice = _priceHelper.Format(articlesPrice[adherentSouscriptionCardPrid], PriceFormat.CurrencyAsDecimalSeparator);
                        membershipViewModel.Push.CardId = adherentSouscriptionCardPrid;
                        membershipViewModel.Push.IsProfitable =
                            (adherentPush.SimulateDifferedAmount + adherentPush.SimulateSavingAmount >= articlesPrice[adherentSouscriptionCardPrid])
                            || _switchProvider.IsEnabled("bypass.minAdvantageAmountToDisplayConfirmationPush");
                    }
                    if (pushType == PushType.Renew)
                    {
                        decimal? cardPrice = null;
                        decimal? souscriptionCardPrice = null;
                        if (articlesPrice.ContainsKey(adherentRenewCard.Value))
                        {
                            cardPrice = articlesPrice[adherentRenewCard.Value];
                            membershipViewModel.Push.CardId = adherentRenewCard.Value;
                            membershipViewModel.Push.CardPrice = _priceHelper.Format(cardPrice.Value, PriceFormat.CurrencyAsDecimalSeparator);

                            if (IsObsoleteMembership)
                            {
                                membershipViewModel.Push.IsProfitable = (adherentPush.SimulateDifferedAmount + adherentPush.SimulateSavingAmount >= articlesPrice[adherentRenewCard.Value]);
                            }
                            else
                            {
                                membershipViewModel.Push.IsProfitable = false;
                            }
                        }
                        if (articlesPrice.ContainsKey(adherentSouscriptionCardPrid))
                        {
                            souscriptionCardPrice = articlesPrice[adherentSouscriptionCardPrid];
                            membershipViewModel.Push.SouscriptionCardPrice = _priceHelper.Format(souscriptionCardPrice.Value, PriceFormat.CurrencyAsDecimalSeparator);
                        }
                        membershipViewModel.Push.DisplayPriceBenefit = (cardPrice.HasValue && souscriptionCardPrice.HasValue && cardPrice.Value < souscriptionCardPrice.Value);
                    }
                }
            }

            var steedFromStoreData = popOrderForm.GetPrivateData<SteedFromStoreData>(false);

            if (steedFromStoreData == null || !steedFromStoreData.StoreId.HasValue)
            {
                membershipViewModel.Push.IsSteedFromStoreAvailable = false;
            }
            else
            {
                membershipViewModel.Push.IsSteedFromStoreAvailable = true;
            }
            return membershipViewModel;
        }

        private void FillTempShippingProperties(MembershipPushViewModel push, PopOrderForm of)
        {
            push.IsFnacSeller = false;
            push.ShowPush = false;
            push.IsFullMP = false;
            push.ShowPopin = false;

            var fnacPlusData = of.GetPrivateData<PushFnacPlusAdherentData>(create: false);
            var popShippingData = of.GetPrivateData<PopShippingData>(create: false);
            if (fnacPlusData == null || popShippingData == null)
                return;

            var popShippingSellerData = popShippingData.GetCurrentShippingSellerData();

            push.IsFnacSeller = popShippingSellerData != null ? popShippingSellerData.SellerId == 0 : false;

            push.ShowPush =
                (fnacPlusData.IsEligibleEssaiFnacPlus
                    || fnacPlusData.IsEligiblePushRecruit)
                && !fnacPlusData.HasFnacPlusCardInBasket
                && ((of.Path == AppliablePath.Full)
                    || (of.Path == AppliablePath.OnePage));

            push.IsFullMP = of.HasOnlyMarketPlaceArticle();

            push.ShowPopin = push.ShowPush
                && !fnacPlusData.HasFnacPlusCardInBasket
                && !fnacPlusData.IsRemovedFromShippingStep
                && ((_switchProvider.IsEnabled("orderpipe.pop.shipping.fnacplus.popin") && !push.IsFullMP)
                    || (_switchProvider.IsEnabled("orderpipe.pop.shipping.fnacplus.popin.mp") && push.IsFullMP));
        }

        private void BuildProfil(PopOrderForm popOrderForm, MembershipViewModel membershipViewModel)
        {
            membershipViewModel.Profil.FirstName = popOrderForm.UserContextInformations.FirstName;
            membershipViewModel.Profil.LastName = popOrderForm.UserContextInformations.LastName;
            membershipViewModel.Profil.BirthDay = popOrderForm.UserContextInformations.CustomerEntity?.Birthdate.Day ?? 0;
            membershipViewModel.Profil.BirthMonth = popOrderForm.UserContextInformations.CustomerEntity?.Birthdate.Month ?? 0;
            membershipViewModel.Profil.BirthYear = popOrderForm.UserContextInformations.CustomerEntity?.Birthdate.Year ?? 0;
            membershipViewModel.Profil.Type = GetAdherentProfil();

            var popAdherentData = popOrderForm.GetPrivateData<PopAdherentData>(create: false);
            membershipViewModel.Profil.FiscalTaxNumberType = popAdherentData?.TypeId ?? string.Empty;
            membershipViewModel.Profil.FiscalTaxNumber = popAdherentData?.NIENIF ?? string.Empty;
        }

        private bool HasEligibleCardForRenew()
        {
            return CurrentRenewContract != null && CurrentRenewContract.EligibilityRenewCards != null &&
                   CurrentRenewContract.EligibilityRenewCards.Any(c =>
                       c.ProgramDuration == ProgramType.ThreeYears ||
                       c.ProgramDuration == ProgramType.Amex ||
                       c.ProgramDuration == ProgramType.OneAmex);

        }
        private int? GetMembeshipRenewCardPridFromSeibel()
        {
            if (!HasEligibleCardForRenew())
            {
                return null;
            }
            var card = CurrentRenewContract.EligibilityRenewCards.First(c =>
                       c.ProgramDuration == ProgramType.ThreeYears ||
                       c.ProgramDuration == ProgramType.Amex ||
                       c.ProgramDuration == ProgramType.OneAmex);

            var ar = new ArticleReference { ReferenceGU = card.ArticleRefGU };

            var renewCard = _w3ArticleService.GetW3Article(ar, false, false);

            if (renewCard != null && renewCard.IsActive && renewCard.ArticleReference.PRID.HasValue)
            {
                return renewCard.ArticleReference.PRID.Value;
            }
            return null;
        }
        private RenewContract CurrentRenewContract
        {
            get
            {
                if (_userExtendedContext.Customer != null && _userExtendedContext.Customer.IsAdherentRenewable
                    && _userExtendedContext.Customer.MembershipContract != null
                    && _userExtendedContext.Customer.MembershipContract.MemberCard != null
                    && _userExtendedContext.Customer.MembershipContract.MemberCard.HolderType == CardHolderType.Titular)
                {
                    return _userExtendedContext.Customer.RenewContract;
                }
                return null;

            }
        }

        private static bool HasAdherentCardInBasket(IEnumerable<ILineGroup> lineGroups)
        {
            return lineGroups.GetArticles().OfType<StandardArticle>().Any(a => a.TypeId.GetValueOrDefault() == (int)CategoryCardType.Renew || a.TypeId.GetValueOrDefault() == (int)CategoryCardType.Recruit);
        }

        private static decimal TotalRealDiscount(PushAdherentData pushAdherentData)
        {
            return pushAdherentData.RealSavingAmount + pushAdherentData.RealDifferedAmount;
        }

        private static decimal TotalSimulateDiscount(PushAdherentData pushAdherentData)
        {
            return pushAdherentData.SimulateSavingAmount + pushAdherentData.SimulateDifferedAmount;
        }

        private PushType GetFnacPlusPushType(PopOrderForm popOrderForm, MembershipPushViewModel viewModel)
        {
            var excludedLogTypes = popOrderForm.ShippingMethodEvaluation.ExcludedLogisticTypes;
            var containsAtLeastOnShippableFnacComArticle = popOrderForm.LineGroups.GetArticles().Any(a => a.SellerID == null && a.LogType.HasValue && !excludedLogTypes.Contains(a.LogType.Value));
            var disablePush = !viewModel.HasDiscount && !containsAtLeastOnShippableFnacComArticle;

            if (disablePush && popOrderForm.LineGroups.Any(x => x.OrderType == OrderTypeEnum.MarketPlace) && !viewModel.IsAdherent && viewModel.HasFnacPlusDiscount)
                disablePush = false;

            if (viewModel.IsEligiblePushConfirm)
                return disablePush ? PushType.None : PushType.Confirmation;
            else if (viewModel.IsEligiblePushRenouvFnacPlus || viewModel.IsEligiblePushRenouvAdh)
                return PushType.Renew;
            else if (viewModel.IsEligiblePushRecruit)
                return disablePush ? PushType.None : PushType.Recruit;
            else
                return PushType.None;
        }

        private PushType GetPushType(PopOrderForm popOrderForm)
        {
            var adherentPush = popOrderForm.GetPrivateData<PushAdherentData>("PushAdherentGroup");

            //Confirmation : 
            //Le client est adhérent + n'est pas un future périmé + pas déjà périmé + bénéficie d'une remise adhérent sur son panier
            //OU
            //Le client à une carte adhérent dans son panier + bénéficie d'une remise adhérent sur son panier
            if ((
                    (
                        popOrderForm.UserContextInformations.IsAdherent &&
                        !IsMembershipAboutToExpire &&
                        !IsObsoleteMembership
                    ) ||
                    HasAdherentCardInBasket(popOrderForm.LineGroups)
                ) &&
                    TotalRealDiscount(adherentPush) > 0)
            {
                return PushType.Confirmation;
            }
            //Renouvellement :
            //Le client est identifié
            //+ future périmé
            //+ éligible au renouvellement
            //+ aucun avantage adhérent n'est présent dans le panier OU le seul avantage dispo est la livraison gratuite/ réduite
            if (!_isFib &&
                IsIdentified &&
                HasEligibleCardForRenew() &&
                _userExtendedContext.Customer.IsAdherentRenewable &&
                !HasAdherentCardInBasket(popOrderForm.LineGroups) &&
                (IsMembershipAboutToExpire || IsObsoleteMembership) &&
                _userExtendedContext.Customer.MembershipContract.MemberCard.HolderType == CardHolderType.Titular)
            {
                return PushType.Renew;
            }

            //Push Recrutement :
            //Le client n'est pas identifié ou identifié et pas adhérent
            var pushRecruitIsValid = !HasAdherentCardInBasket(popOrderForm.LineGroups)
                && ((!IsMembershipAboutToExpire && !IsObsoleteMembership) || _isFib)
                && (!IsIdentified || (IsIdentified && !popOrderForm.UserContextInformations.IsAdherent));

            //et le montant total des discount Immédiates + différées > 5€ 
            if (pushRecruitIsValid
                && ((TotalSimulateDiscount(adherentPush) > _minAdvantageAmountToDisplayConfirmationPush)
                    || _switchProvider.IsEnabled("bypass.minAdvantageAmountToDisplayConfirmationPush")))
            {
                return PushType.Recruit;
            }

            return PushType.None;
        }

        private AdhesionType GetAdherentProfil()
        {
            if(_userExtendedContext.ContractDto == null)
                return AdhesionType.None;

            if (_userExtendedContext.ContractDto.Segments.Contains(CustomerSegment.MemberOne))
            {
                return AdhesionType.AdherentOne;
            }
            if (_userExtendedContext.ContractDto.Segments.Contains(CustomerSegment.MemberOneAmex))
            {
                return AdhesionType.AdherentOneAmex;
            }
            if (_userExtendedContext.ContractDto.Segments.Contains(CustomerSegment.MemberAmexPlus))
            {
                return AdhesionType.AdherentAmexPlus;
            }
            if (_userExtendedContext.ContractDto.Segments.Contains(CustomerSegment.MemberAmex))
            {
                return AdhesionType.AdherentAmex;
            }

            if (_userExtendedContext.ContractDto.Segments.Contains(CustomerSegment.Member3Years))
            {
                return AdhesionType.AdherentThreeYears;
            }
            if (_userExtendedContext.ContractDto.Segments.Contains(CustomerSegment.Member))
            {
                return AdhesionType.AdherentOneYear;
            }
            return AdhesionType.None;
        }

        //_delayBeforeExpiration is negative
        private bool IsMembershipAboutToExpire
        {
            get
            {
                var customer = _userExtendedContext.Customer;
                if (customer != null && customer.MembershipContract != null
                    && !_switchProvider.IsEnabled("orderpipe.pop.MembershipAbountToExpire", false))
                {
                    return (customer.MembershipContract.AdhesionEndDate <= _dateTimeProvider.Now() &&
                        customer.MembershipContract.AdhesionEndDate.AddDays(_delayBeforeExpiration) <= _dateTimeProvider.Now());
                }
                return false;
            }
        }

        //_delayAfterExpiration is positive
        private bool IsObsoleteMembership
        {
            get
            {
                if (_userExtendedContext.Customer != null && _userExtendedContext.Customer.MembershipContract != null)
                {
                    return (_userExtendedContext.Customer.MembershipContract.AdhesionEndDate <= _dateTimeProvider.Now()
                    && _userExtendedContext.Customer.MembershipContract.AdhesionEndDate.AddDays(_delayAfterExpiration) >= _dateTimeProvider.Now());
                }
                return false;
            }
        }

        private bool IsYoung
        {
            get { return IsIdentified && _userExtendedContext.Customer.DTO.Birthdate.AddYears(_youngMaxAge) >= DateTime.Now; }
        }

        private bool IsIdentified
        {
            get { return _userContextProvider.GetCurrentUserContext().IsIdentified; }
        }

        private bool IsAdherentOne
        {
            get { return _userExtendedContext.Customer.IsAdherentOne; }
        }


        public MembershipBindCardViewModel BuildCardBindViewModel()
        {
            var cardPrefix = _membershipCardBindingConfigurationBusiness.GetPrefixes();
            return new MembershipBindCardViewModel
            {
                Days = Enumerable.Range(1, 31),
                Months = Enumerable.Range(1, 12),
                AdherentCardsPrefix = cardPrefix,
                DisplayAdherentCardPrefix = cardPrefix.Any(),
                AdherentCardLenght = _membershipCardBindingConfigurationBusiness.GetMaxLength()
            };
        }

        private bool HasTryCard(PopOrderForm popOrderForm)
        {
            return popOrderForm.LineGroups.GetArticles().Any(a => a.ProductID == _membershipConfigurationService.GetTryCardPrid());
        }

        private int GetTryCardPrid()
        {
            return _membershipConfigurationService.GetTryCardPrid();
        }

        public MemberBirthDateViewModel BuildMemberBirthDateViewModel(OPContext opContext, PopOrderForm popOrderForm)
        {
            var aboIlliData = popOrderForm.GetPrivateData<AboIlliData>("AboIlliGroup", true);
            var hasBirthDate = IsIdentified && popOrderForm.UserContextInformations.CustomerEntity != null && !popOrderForm.UserContextInformations.CustomerEntity.Birthdate.Equals(new DateTime(1900, 1, 1));
            var isTooYoung = popOrderForm.UserContextInformations.CustomerEntity != null && _dateBusiness.IsLowerThanAdherenAgeMin(popOrderForm.UserContextInformations.CustomerEntity.Birthdate);
            var hasFnacPlusTryCardInBasket = popOrderForm.LineGroups.Fnac().GetArticles().Any(a =>
                    a.TypeId == _membershipConfigurationService.FnacPlusTryCardType);
            var hasFnacPlusCardInBasket = popOrderForm.LineGroups.Fnac().GetArticles().Any(a =>
                    _membershipConfigurationService.FnacPlusCardTypes.Contains(a.TypeId.GetValueOrDefault()));
            var fnacPlusTryCardDuration = HtmlHelpers.Pop.Membership.AdherentCardHelper.GetFnacPlusTryCardDuration(null);
            var hasAboilliCarInBasket = popOrderForm.LineGroups.Fnac().GetArticles().Any(a =>
                 a.ProductID.Value == aboIlliData.AboilliPrid || a.ProductID.Value == aboIlliData.TrialAboilliPrid);

            var aboilliCarPrid = 0;
            if (hasAboilliCarInBasket)
                aboilliCarPrid = popOrderForm.LineGroups.Fnac().GetArticles().First(a =>
                 a.ProductID.Value == aboIlliData.AboilliPrid || a.ProductID.Value == aboIlliData.TrialAboilliPrid).ProductID.Value;

            var fnacplusCardPrid = 0;
            if (hasFnacPlusCardInBasket)
                fnacplusCardPrid = popOrderForm.LineGroups.Fnac().GetArticles().First(a =>
                _membershipConfigurationService.FnacPlusCardTypes.Contains(a.TypeId.GetValueOrDefault())).ProductID.Value;


            return new MemberBirthDateViewModel
            {
                IsTooYoung = isTooYoung,
                HasFnacPlusCardInBasket = hasFnacPlusCardInBasket,
                HasFnacPlusTryCardInBasket = hasFnacPlusTryCardInBasket,
                FnacPlusTryCardDuration = fnacPlusTryCardDuration,
                FnacPlusCardPrid = fnacplusCardPrid,
                HasTryCard = HasTryCard(popOrderForm),
                TryCardPrid = GetTryCardPrid(),
                TryCardDuration = opContext.Pipe.GlobalParameters.GetAsInt("membership.try.duration"),
                HasAboilliCartInBasket = hasAboilliCarInBasket,
                AboilliPrid = aboilliCarPrid,
                HasBirthDate = hasBirthDate,
                Days = Enumerable.Range(1, 31).Select(i => new ChoiceViewModel
                {
                    Id = i.ToString(CultureInfo.InvariantCulture),
                    Label = i.ToString(CultureInfo.InvariantCulture),
                    IsSelected = hasBirthDate && popOrderForm.UserContextInformations.CustomerEntity.Birthdate.Day == i
                }),
                Months = Enumerable.Range(1, 12).Select(i => new ChoiceViewModel
                {
                    Id = i.ToString(CultureInfo.InvariantCulture),
                    Label = i.ToString(CultureInfo.InvariantCulture),
                    IsSelected = hasBirthDate && popOrderForm.UserContextInformations.CustomerEntity.Birthdate.Month == i
                }),

                FirstName = popOrderForm.UserContextInformations.FirstName,
                BirthDay = hasBirthDate ? popOrderForm.UserContextInformations.CustomerEntity.Birthdate.Day : (int?)null,
                BirthMonth = hasBirthDate ? popOrderForm.UserContextInformations.CustomerEntity.Birthdate.Month : (int?)null,
                BirthYear = hasBirthDate ? popOrderForm.UserContextInformations.CustomerEntity.Birthdate.Year : (int?)null,
            };
        }

        public Maybe<MembershipCardViewModel> BuildAdjustedCard(PopOrderForm popOrderForm)
        {
            var data = popOrderForm.GetPrivateData<PopMembershipData>(false);
            if (data != null && data.DisplayNotification)
            {
                return new MembershipCardViewModel
                {
                    CardPrice = _priceHelper.Format(data.CardPrice, PriceFormat.CurrencyAsDecimalSeparator),
                    ReducedCardPrice = _priceHelper.Format(data.AdjustedCardPrice, PriceFormat.CurrencyAsDecimalSeparator),
                    IsRenewable = data.IsRenewable,
                    DisplayPopin = data.DisplayNotification
                };
            }
            return Maybe.Empty<MembershipCardViewModel>();
        }

        public MembershipCardFormatViewModel BuildCardFormat(OPContext opContext, PopOrderForm popOrderForm)
        {
            var cardPrefix = _membershipCardBindingConfigurationBusiness.GetPrefixes();
            return new MembershipCardFormatViewModel
            {
                CardsPrefix = cardPrefix,
                DisplayCardPrefix = cardPrefix.Any(),
                IsTryCard = HasTryCard(popOrderForm),
                TryCardDuration = opContext.Pipe.GlobalParameters.GetAsInt("membership.try.duration"),
                Pattern = _membershipCardBindingConfigurationBusiness.GetPattern(),
                MaxLength = _membershipCardBindingConfigurationBusiness.GetMaxLength(),
            };
        }
    }
}
