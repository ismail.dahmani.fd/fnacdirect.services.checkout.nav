﻿using System.Linq;
using System.Collections.Generic;
using FnacDirect.OrderPipe.Base.LineGroups;
using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.OrderPipe.Base.Model.LineGroups;
using FnacDirect.OrderPipe.Base.BaseModel.Model;
using FnacDirect.OrderPipe.Base.Business.LogisticLineGroups.Internals.InitialSelectors;

namespace FnacDirect.OrderPipe.Base.Business.LogisticLineGroups.InitialSelectors
{
    public class ClickAndCollectLineGroupInitialSelector : BaseLineGroupInitialSelector
    {
        public override IDictionary<ILineGroupHeader, IEnumerable<Article>> Select(IEnumerable<ILineGroup> lineGroups)
        {
            var fnacLineGroups = lineGroups.Fnac();

            var output = new Dictionary<ILineGroupHeader, IEnumerable<Article>>();

            var clickAndCollectData = Context.PrivateDataContainer.GetPrivateData<ClickAndCollectData>(false);

            if(clickAndCollectData == null)
            {
                return output;
            }

            foreach(var productAvailability in clickAndCollectData.PridsAvailable)
            {
                foreach (var lineGroup in fnacLineGroups)
                {
                    var matchingArticle = lineGroup.Articles.OfType<StandardArticle>().FirstOrDefault(a => a.ProductID.GetValueOrDefault(0) == productAvailability);

                    if(matchingArticle != null)
                    {
                        if(!output.ContainsKey(lineGroup))
                        {
                            output.Add(lineGroup, new List<Article>());
                        }

                        var entry = output[lineGroup];

                        if (!entry.Any(a => a.ProductID.GetValueOrDefault(0) == productAvailability))
                        {
                            (entry as IList<Article>).Add(matchingArticle);
                        }
                    }
                }
            }

            return output;
        }
    }
}
