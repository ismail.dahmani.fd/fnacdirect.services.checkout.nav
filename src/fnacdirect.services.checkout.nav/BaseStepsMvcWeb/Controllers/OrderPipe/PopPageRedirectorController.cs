using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.OrderPipe.Base.Model.AddOn;
using FnacDirect.OrderPipe.BaseMvc.Web.Pop.Models.PageRedirector;
using System;
using System.Linq;
using System.Web.Mvc;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Controllers.Pop
{
    public class PopPageRedirectorController : PopBaseController
    {
        private readonly OPContext _opContext;
        private readonly PopOrderForm _popOrderForm;

        public PopPageRedirectorController(OPContext opContext)
        {
            _opContext = opContext;
            _popOrderForm = _opContext.OF as PopOrderForm;

        }

        public ActionResult Index()
        {
            var pageRedirectData = _popOrderForm.PrivateData.GetByType<PageRedirectData>();

            if(pageRedirectData == null)
            {
                throw new InvalidOperationException();
            }

            if (string.IsNullOrEmpty(pageRedirectData.Url))
            {
                throw new InvalidOperationException();
            }

            var url = pageRedirectData.Url;
            var parameters = pageRedirectData.Parameters;

            pageRedirectData.Url = null;
            pageRedirectData.Parameters = null;

            switch (pageRedirectData.Type)
            {
                case PageRedirectType.Post:
                    return View(new PageRedirectorViewModel
                    {
                        Url = url,
                        Parameters = parameters
                    });
                case PageRedirectType.Get:
                default:
                    if(parameters.Any())
                    {
                        var queryString = string.Join("&", parameters.Select(p => string.Format("{0}={1}", p.Key, p.Value)));

                        return Redirect(string.Format("{0}?{1}", url, queryString));
                    }

                    return Redirect(url);
            }
        }
    }
}
