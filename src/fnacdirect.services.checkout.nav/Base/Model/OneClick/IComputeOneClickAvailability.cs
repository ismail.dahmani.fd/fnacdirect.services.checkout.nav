using FnacDirect.OrderPipe.Base.Model;

namespace FnacDirect.OrderPipe.Base.Business.OneClick
{
    public interface IComputeOneClickAvailability
    {
        StepReturn ComputeAvailability(PopOrderForm orderForm);
    }
}
