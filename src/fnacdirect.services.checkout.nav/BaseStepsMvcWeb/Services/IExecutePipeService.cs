using System;

namespace FnacDirect.OrderPipe.CoreRouting
{
    public interface IExecutePipeService
    {
        PipeExecutionResult ExecuteOrderPipe(string pipeName);
        PipeExecutionResult ExecuteOrderPipe(string pipeName, Action<OPContext> opContextSetup);
        PipeExecutionResult ExecuteOrderPipe(string pipeName, bool pipeReset, Func<IOP, string> getForceStep);
        PipeExecutionResult ExecuteOrderPipe(string pipeName, bool pipeReset, Func<IOP, string> getForceStep, Action<OPContext> opContextSetup);
        PipeExecutionResult ExecuteOrderPipe(string pipeName, bool pipeReset, Func<IOP, string> getForceStep, Action<OPContext> opContextSetup, bool applyRedirect);

        bool IsKnownOrderPipe(string pipeName);
        bool CanExecute(string pipeName);
    }
}
