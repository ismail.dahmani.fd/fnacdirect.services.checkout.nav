﻿using System;

using FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.InvoicerEbook;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Common
{
    public class OrderFinalizationViewModel
    {
        public OrderFinalizationViewModel()
        {
            Invocer = Maybe<InvoicerEbookViewModel>.Empty();            
        }
        public Maybe<InvoicerEbookViewModel> Invocer;        
    }
}
