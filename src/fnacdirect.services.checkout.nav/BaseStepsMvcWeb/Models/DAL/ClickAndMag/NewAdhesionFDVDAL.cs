using System;
using FnacDirect.Technical.Framework.CoreServices;
using FnacDirect.OrderPipe.CM.Model;

namespace FnacDirect.OrderPipe.Base.DAL
{

    public class NewAdhesionFDVDal : INewAdhesionFDVDal
    {
        private const string DATABASE_SqlAdherent = "SqlAdherent";

        /// <summary>
        /// Crée une demande d'adhésion en base.
        /// </summary>
        /// <param name="accountId"></param>
        /// <param name="articleID"></param>
        /// <param name="statut"></param>
        /// <returns></returns>
        public int CreateAdhesion(int accountId, int articleID, int statut, int purchaseOrder, string purchaseOrderUserReference)
        {
            var returnvalue = -1;
            try
            {
                using(var database = Database.CreateInstance(DATABASE_SqlAdherent))
                {
                    using (var command = database.GetStoredProcCommand("spNEWADHCM_Create"))
                    {
                        database.AddInParameter(command, "intAccountId", System.Data.SqlDbType.Int, accountId);
                        database.AddInParameter(command, "intArticleId", System.Data.SqlDbType.Int, articleID);
                        database.AddInParameter(command, "intStatus", System.Data.SqlDbType.Int, statut);
                        database.AddInParameter(command, "intPurchaseOrder", System.Data.SqlDbType.Int, purchaseOrder);
                        database.AddInParameter(command, "varcharPurchaseOrderUserReference", System.Data.SqlDbType.VarChar, purchaseOrderUserReference);
                        returnvalue = ((int)command.ExecuteScalar());
                    }
                }
            }
            catch (Exception ex)
            {
                throw DALException.BuildException(ex, DALRange.Adherent, 1);
            }
            return returnvalue;
        }

        /// <summary>
        /// Maj la demande d'adhésion avec le statut de la demande.
        /// </summary>
        /// <param name="adhesionId">Identifiant de l'adhésion</param>
        /// <param name="status">Statut de la demande</param>
        public void AddStatutInformation(int adhesionId, int status)
        {
            try
            {
                using (var database = Database.CreateInstance(DATABASE_SqlAdherent))
                {
                    using (var command = database.GetStoredProcCommand("spNEWADHCM_UpdateStatus"))
                    {
                        database.AddInParameter(command, "intAdhesionId", System.Data.SqlDbType.Int, adhesionId);
                        database.AddInParameter(command, "intStatus", System.Data.SqlDbType.Int, status);
                        command.ExecuteScalar();
                    }
                }
            }
            catch (Exception ex)
            {
                throw DALException.BuildException(ex, DALRange.Adherent, 1);
            }
            return;
        }

        /// <summary>
        /// Maj la demande d'adhésion avec les informations issues de la commande.
        /// </summary>
        /// <param name="adhesionId">Identifiant de l'adhésion</param>
        /// <param name="AccountId">Identifiant du compte</param>
        /// <param name="OrderId">Identifiant de la commande</param>
        /// <param name="OrderUserReference">Référence de la commande</param>
        public void AddOrderInformation(int adhesionId, int purchaseOrder, string purchaseOrderUserReference)
        {
            try
            {
                using (var database = Database.CreateInstance(DATABASE_SqlAdherent))
                {
                    using (var command = database.GetStoredProcCommand("spNEWADHCM_UpdateOrder"))
                    {
                        database.AddInParameter(command, "intAdhesionId", System.Data.SqlDbType.Int, adhesionId);
                        database.AddInParameter(command, "intPurchaseOrder", System.Data.SqlDbType.Int, purchaseOrder);
                        database.AddInParameter(command, "varcharPurchaseOrderUserReference", System.Data.SqlDbType.VarChar, purchaseOrderUserReference);
                        command.ExecuteScalar();
                    }
                }
            }
            catch (Exception ex)
            {
                throw DALException.BuildException(ex, DALRange.Adherent, 1);
            }
            return;
        }
        
        /// <summary>
        /// Permet de récupérer les informations du client à partir d'un numéro de carte adhérent
        /// et des infos contenues dans les tables d'adhésion
        /// </summary>
        /// <param name="cardNumber">Numéro de carte</param>
        /// <param name="cad">Parametre de groupe utilisée pour la création d'un compte client
        /// sur le pipe Click et Mag</param>
        public void GetCustomerInfosFromPlasticCardNumber(string cardNumber, CreateAccountData cad)
        {
            try
            {
                if (string.IsNullOrEmpty(cardNumber))
                    throw new ArgumentNullException("cardNumber");

                if (cad == null)
                    throw new ArgumentNullException("cad");

                using (var database = Database.CreateInstance(DATABASE_SqlAdherent))
                using (var command = database.GetStoredProcCommand("spPipe_GetCustomerInfosFromPlasticCardNumber"))
                {
                    database.AddInParameter(command, "plasticCardNumber", System.Data.SqlDbType.VarChar, cardNumber);
                    using (var reader = database.ExecuteReader(command))
                    {
                        if (reader.Read())
                        {
                            cad.Address1 = DataReaderHelper.GetField<string>(reader, "Address3").Trim();
                            cad.Address2 = string.Format("{0} {1} {2}", DataReaderHelper.GetField<string>(reader, "Address1"),
                                DataReaderHelper.GetField<string>(reader, "Address2"),
                                DataReaderHelper.GetField<string>(reader, "Address4")).Trim();
                            cad.AdhNumber = DataReaderHelper.GetField<string>(reader, "AdhNumber").Trim();
                            cad.Birthdate = DataReaderHelper.GetField<DateTime>(reader, "Birthdate");
                            cad.City = DataReaderHelper.GetField<string>(reader, "City").Trim();
                            cad.Email = DataReaderHelper.GetField<string>(reader, "Email").Trim();
                            cad.Firstname = DataReaderHelper.GetField<string>(reader, "Firstname").Trim();
                            cad.Gender = DataReaderHelper.GetField<string>(reader, "Gender").Trim();
                            cad.Lastname = DataReaderHelper.GetField<string>(reader, "Lastname").Trim();
                            cad.PostalCode = DataReaderHelper.GetField<string>(reader, "Zipcode").Trim();
                            cad.CardStatus = DataReaderHelper.GetField<string>(reader, "CardStatus").Trim();
                            cad.AdhesionStatus = DataReaderHelper.GetField<string>(reader, "AdhesionStatus").Trim();
                            cad.LastUpdated = DataReaderHelper.GetField<DateTime>(reader, "LastUpdated");
                            cad.CardNumber = cardNumber;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw DALException.BuildException(ex, DALRange.Adherent, 1);
            }
        }

        /// <summary>
        /// Permet de récupérer les informations du client à partir d'un numéro d'adhérent
        /// et des infos contenues dans les tables d'adhésion
        /// </summary>
        /// <param name="cardNumber">Numéro d'adhérent</param>
        /// <param name="cad">Parametre de groupe utilisée pour la création d'un compte client
        /// sur le pipe Click et Mag</param>
        public void GetCustomerInfosFromAdherentNumber(string adhNumber, DateTime? birthDate, CreateAccountData cad)
        {
            try
            {
                if (string.IsNullOrEmpty(adhNumber))
                    throw new ArgumentNullException("adhNumber");

                if (cad == null)
                    throw new ArgumentNullException("cad");

                using (var database = Database.CreateInstance(DATABASE_SqlAdherent))
                using (var command = database.GetStoredProcCommand("spPipe_GetCustomerInfosFromAdherentNumber"))
                {
                    database.AddInParameter(command, "adherentNumber", System.Data.SqlDbType.VarChar, adhNumber);
                    if (birthDate.HasValue)
                        database.AddInParameter(command, "birthDate", System.Data.SqlDbType.DateTime, birthDate.Value);
                    using (var reader = database.ExecuteReader(command))
                    {
                        if (reader.Read())
                        {
                            cad.Address1 = DataReaderHelper.GetField<string>(reader, "Address3").Trim();
                            cad.Address2 = string.Format("{0} {1} {2}", DataReaderHelper.GetField<string>(reader, "Address1"),
                                DataReaderHelper.GetField<string>(reader, "Address2"),
                                DataReaderHelper.GetField<string>(reader, "Address4")).Trim();
                            cad.AdhNumber = DataReaderHelper.GetField<string>(reader, "AdhNumber").Trim();
                            cad.Birthdate = DataReaderHelper.GetField<DateTime>(reader, "Birthdate");
                            cad.City = DataReaderHelper.GetField<string>(reader, "City").Trim();
                            cad.Email = DataReaderHelper.GetField<string>(reader, "Email").Trim();
                            cad.Firstname = DataReaderHelper.GetField<string>(reader, "Firstname").Trim();
                            cad.Gender = DataReaderHelper.GetField<string>(reader, "Gender").Trim();
                            cad.Lastname = DataReaderHelper.GetField<string>(reader, "Lastname").Trim();
                            cad.PostalCode = DataReaderHelper.GetField<string>(reader, "Zipcode").Trim();
                            cad.CardNumber = DataReaderHelper.GetField<string>(reader, "CardNumber").Trim();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw DALException.BuildException(ex, DALRange.Adherent, 1);
            }
        }

        public void AddMembershipAccountLight(
            string adherentID,
            string adherentNumber,
            int intAdherentGender,
            string strAdherentFirstname,
            string strAdherentLastname,
            DateTime dteAdherentBirthDate,
            string strAdherentAddress,
            string strAdherentZipCode,
            string strAdherentCity,
            string strAdherentCountryIso3,
            string strAdherentEmail,
            int intAdherentStopPubEmail,
            int intAdhesionType,
            string strAdhesionProgramme,
            int intContractType,
            int intAdhesionDuration,
            DateTime dteAdherentStartDate,
            DateTime dteAdherentEndDate,
            string strAdherentStatus,
            string strOrigine,
            string strRegGU
            )
        {
            try
            {
                using (var database = Database.CreateInstance(DATABASE_SqlAdherent))
                {
                    using (var command = database.GetStoredProcCommand("spNEWADHCM_CreateAdherent"))
                    {
                        database.AddInParameter(command, "strAdherentID", System.Data.SqlDbType.VarChar, adherentID);
                        database.AddInParameter(command, "strAdherentNumber", System.Data.SqlDbType.VarChar, adherentNumber);
                        database.AddInParameter(command, "intAdherentGender", System.Data.SqlDbType.Int, intAdherentGender);
                        database.AddInParameter(command, "strAdherentFirstname", System.Data.SqlDbType.VarChar, strAdherentFirstname);
                        database.AddInParameter(command, "strAdherentLastname", System.Data.SqlDbType.VarChar, strAdherentLastname);
                        database.AddInParameter(command, "dteAdherentBirthDate", System.Data.SqlDbType.DateTime, dteAdherentBirthDate);
                        database.AddInParameter(command, "strAdherentAddress", System.Data.SqlDbType.VarChar, strAdherentAddress);
                        database.AddInParameter(command, "strAdherentZipCode", System.Data.SqlDbType.VarChar, strAdherentZipCode);
                        database.AddInParameter(command, "strAdherentCity", System.Data.SqlDbType.VarChar, strAdherentCity);
                        database.AddInParameter(command, "strAdherentCountryIso3", System.Data.SqlDbType.VarChar, strAdherentCountryIso3);
                        database.AddInParameter(command, "strAdherentEmail", System.Data.SqlDbType.VarChar, strAdherentEmail);
                        database.AddInParameter(command, "intAdherentStopPubEmail", System.Data.SqlDbType.Int, intAdherentStopPubEmail);
                        database.AddInParameter(command, "intAdhesionType", System.Data.SqlDbType.Int, intAdhesionType);
                        database.AddInParameter(command, "strAdhesionProgramme", System.Data.SqlDbType.VarChar, strAdhesionProgramme);
                        database.AddInParameter(command, "intContractType", System.Data.SqlDbType.VarChar, intContractType);
                        database.AddInParameter(command, "intAdhesionDuration", System.Data.SqlDbType.Int, intAdhesionDuration);
                        database.AddInParameter(command, "dteAdherentStartDate", System.Data.SqlDbType.DateTime, dteAdherentStartDate);
                        database.AddInParameter(command, "dteAdherentEndDate", System.Data.SqlDbType.DateTime, dteAdherentEndDate);
                        database.AddInParameter(command, "strAdherentStatus", System.Data.SqlDbType.VarChar, strAdherentStatus);
                        database.AddInParameter(command, "strOrigine", System.Data.SqlDbType.VarChar, strOrigine);
                        database.AddInParameter(command, "strRegGU", System.Data.SqlDbType.VarChar, strRegGU);

                        command.ExecuteScalar();
                    }
                }
            }
            catch (Exception ex)
            {
                throw DALException.BuildException(ex, DALRange.Adherent, 1);
            }
            return;

        }
    }
}
