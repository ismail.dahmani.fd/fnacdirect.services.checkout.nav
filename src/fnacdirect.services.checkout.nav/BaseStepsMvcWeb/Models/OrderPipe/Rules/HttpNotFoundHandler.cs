﻿using System.Web;

namespace FnacDirect.OrderPipe.CoreRouting.Rules
{
    public class HttpNotFoundHandler : IHttpHandler
    {
        public bool IsReusable
        {
            get
            {
                return true;
            }
        }

        public void ProcessRequest(HttpContext context)
        {
            context.Response.StatusCode = 404;
            context.Response.StatusDescription = "Not Found";
        }
    }
}
