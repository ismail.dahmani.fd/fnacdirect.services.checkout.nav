using FnacDirect.OrderPipe.Base.Model;
using System;


namespace FnacDirect.OrderPipe.Base.Business.Availability
{
    public class PopAvailabilityProcessorInput
    {
        private readonly BaseArticle _article;
        private readonly int? _shippingMethodId;
        private DateTime? _dateMin;
        private DateTime? _dateMax;
        private readonly int? _parcelShippingMethodId;

        public BaseArticle Article
        {
            get
            {
                return _article;
            }
        }

        public DateTime? DateMax
        {
            get
            {
                return _dateMax;
            }
            set
            {
                _dateMax = value;
            }
        }

        public DateTime? DateMin
        {
            get
            {
                return _dateMin;
            }
            set
            {
                _dateMin = value;
            }
        }

        public int? ShippingMethodId
        {
            get
            {
                return _shippingMethodId;
            }
        }
        public int? ParcelShippingMethodId
        {
            get
            {
                return _parcelShippingMethodId;
            }
        }
        public PopAvailabilityProcessorInput(BaseArticle article, DateTime? dateMin, DateTime? dateMax, int? shippingMethodId = null, int? parcelShippingMethodId = null)
        {
            _article = article;
            _dateMin = dateMin;
            _dateMax = dateMax;
            _shippingMethodId = shippingMethodId;
            _parcelShippingMethodId = parcelShippingMethodId;
        }

        public bool IsAvailabilityValid()
        {
            if (Article is MarketPlaceArticle mpArticle)
            {
                if (!DateMin.HasValue ||
                    !DateMax.HasValue ||
                    !mpArticle.Offer.PreparationTimeInDays.HasValue)
                    return false;
            }
            return true;
        }
    }
}
