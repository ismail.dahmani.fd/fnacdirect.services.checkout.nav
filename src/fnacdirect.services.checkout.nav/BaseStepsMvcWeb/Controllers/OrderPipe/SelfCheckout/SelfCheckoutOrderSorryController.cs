using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.OrderPipe.BaseMvc.Web.Controllers.Pop;
using FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.DataLayer;
using FnacDirect.OrderPipe.BaseMvc.Web.Pop.Models.SelfCheckout;
using FnacDirect.OrderPipe.BaseMvc.Web.Services.Pop.DataLayer;
using System.Web.Mvc;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Pop.Controllers.SelfCheckout
{
    public class SelfCheckoutOrderSorryController : PopBaseController
    {
        private readonly IDataLayerService _dataLayerService;

        public SelfCheckoutOrderSorryController(IDataLayerService dataLayerService)
        {
            _dataLayerService = dataLayerService;

        }

        [HttpGet]
        public ViewResult Index()
        {
            var opContext = DependencyResolver.Current.GetService<OPContext>();
            var popOrderForm = opContext.OF as PopOrderForm;

            var datalayerViewModel = new DatalayerViewModel()
            {
                PageType = PageTypeEnum.ScoOrderThankYou,
                PopOrderForm = popOrderForm,
            };

            var viewResult = View();
            _dataLayerService.ComputeTransaction(datalayerViewModel, viewResult as ViewResultBase, ControllerContext);

            return viewResult;
        }
    }
}
