﻿
namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Common.Basket
{
    public class MarketPlaceSellerInformationsViewModel : SellerInformationsViewModel
    {
        private readonly string _country;
        private readonly bool _shouldDisplayCountry;
        private readonly bool _isPro;

        public MarketPlaceSellerInformationsViewModel(string name)
            : this(name, string.Empty, false, false)
        {
        }

        public MarketPlaceSellerInformationsViewModel(string name, string country, bool shouldDisplayCountry, bool isPro)
            : base(name)
        {
            _country = country;
            _shouldDisplayCountry = shouldDisplayCountry;
            _isPro = isPro;
        }

        public string Country
        {
            get
            {
                return _country;
            }
        }

        public bool ShouldDisplayCountry
        {
            get
            {
                return _shouldDisplayCountry;
            }
        }

        public bool IsPro
        {
            get
            {
                return _isPro;
            }
        }

        public override bool IsFnac
        {
            get
            {
                return false;
            }
        }

        public override bool IsClickAndCollectOnly
        {
            get
            {
                return false;
            }
        }

        public decimal RoundedRating { get; private set; }

        public decimal Rating
        {
            set
            {
                var number = value;

                number *= 100;

                var modulo = number % 25;

                if (modulo < 12)
                {
                    number -= modulo;
                }
                else
                {
                    number += (25 - modulo);
                }

                RoundedRating = number / 100;
            }
        }

        public int? Sales { get; set; }

        public bool IsNewSeller
        {
            get { return Sales.GetValueOrDefault() == 0; }
        }

        public bool IsTopSeller { get; set; }
    }
}
