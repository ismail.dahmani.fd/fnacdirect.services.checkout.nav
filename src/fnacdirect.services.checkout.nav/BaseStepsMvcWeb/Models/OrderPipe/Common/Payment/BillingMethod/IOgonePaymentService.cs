using FnacDirect.OrderPipe.Base.Model.Facets;

namespace FnacDirect.OrderPipe.Base.Business.Payment
{
    public interface IOgonePaymentService
    {
        bool IsAllowed(IGotLineGroups iContainsLineGroups, IGotBillingInformations iContainsBillingInformations, IGotUserContextInformations iContainsUserContextInformations);
    }
}
