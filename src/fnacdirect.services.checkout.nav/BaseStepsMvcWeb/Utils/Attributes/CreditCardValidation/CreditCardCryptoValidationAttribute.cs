﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using System.Linq;
using StructureMap;
using FnacDirect.Context;
using FnacDirect.Contracts.Online.Model;
using FnacDirect.Customer.Model;
using FnacDirect.OrderPipe.Base.Business;
using FnacDirect.OrderPipe.BaseMvc.Web.Services.Pop;
using FnacDirect.Technical.Framework.Web.Mvc;
using FnacDirect.OrderPipe.Base.Business.Payment;
using FnacDirect.Technical.Framework.ServiceLocation;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Validations
{
    public class CreditCardCryptoValidationAttribute : ValidationAttribute, IClientValidatable
    {
        private readonly string _propertyCreditCardType;

        private readonly IApplicationContext _applicationContext;
        private readonly IPaymentSelectionBusiness _paymentSelectionBusiness;
        private readonly SiteContext _siteContext;
        private readonly string _culture;
        private readonly string _configShippingMethodSelection;

        public CreditCardCryptoValidationAttribute(string propertyCreditCardType)
        {
            _propertyCreditCardType = propertyCreditCardType;

            _applicationContext = ServiceLocator.Current.GetInstance<IApplicationContext>();
            _siteContext = _applicationContext.GetSiteContext();
            _culture = _siteContext != null ? _siteContext.Culture : "";
            _configShippingMethodSelection = _applicationContext.GetAppSetting("config.shippingMethodSelection");

            _paymentSelectionBusiness = ServiceLocator.Current.GetInstance<IPaymentSelectionBusiness>();
        }

        public IEnumerable<ModelClientValidationRule> GetClientValidationRules(ModelMetadata metadata, ControllerContext context)
        {
            var modelClientValidationRule = new ModelClientValidationRule
            {
                ErrorMessage = _paymentSelectionBusiness.GetErrorMessage("OP.CARD.ErrMsg.crypto"),
                ValidationType = "creditcardcryptovalidation"
            };

            modelClientValidationRule.ValidationParameters["propertycreditcardtype"] = _propertyCreditCardType;

            yield return modelClientValidationRule;
        }

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            //Value : Crypto
            if (value == null)
                return ValidationResult.Success;

            var creditCardCrypto = value.ToString();

            if(string.IsNullOrEmpty(_propertyCreditCardType))
            {
                return CheckFnacCardCrypto(creditCardCrypto);
            }
            //Selected Credit Card Type 
            var propertyCreditCardTypeInfo = validationContext.ObjectType.GetProperty(_propertyCreditCardType);

            if (propertyCreditCardTypeInfo == null)
                return ValidationResult.Success;

            var propertyCreditCardTypeValue = propertyCreditCardTypeInfo.GetGetMethod().Invoke(validationContext.ObjectInstance, null);

            if (!(propertyCreditCardTypeValue is int))
                return ValidationResult.Success;

            var creditCardTypeId = (int)propertyCreditCardTypeValue;

            var creditCardDescriptorBusiness = ServiceLocator.Current.GetInstance<ICreditCardDescriptorBusiness>();
            var creditCardDescriptor = creditCardDescriptorBusiness.GetCreditCardDescriptor(creditCardTypeId, _configShippingMethodSelection, _culture);

            return CheckCreditCardCrypto(creditCardCrypto, creditCardDescriptor);
        }

        private ValidationResult CheckCreditCardCrypto(string creditCardCrypto, CustCreditCardDescriptor creditCardDescriptor)
        {
            var validationResult = _paymentSelectionBusiness.CheckSipsCreditCardVerificationCode(creditCardCrypto, creditCardDescriptor.CryptoLength);

            return validationResult;
        }

        private ValidationResult CheckFnacCardCrypto(string creditCardCrypto)
        {
            var validationResult = _paymentSelectionBusiness.CheckSipsCreditCardVerificationCode(creditCardCrypto, 0, true);

            return validationResult;
        }
    }
}
