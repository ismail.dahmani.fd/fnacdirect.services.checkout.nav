﻿using FnacDirect.OrderPipe.Base.Model;

namespace FnacDirect.OrderPipe.BaseMvc.Web.LayerBasket
{
    public interface ILayerBasketViewModelBuilder
    {
        LayerBasketViewModel Build(PopOrderForm orderForm);
    }
}
