namespace FnacDirect.OrderPipe.Base.Model
{
    public class MBWayBillingMethod : BillingMethod
    {
        public const int IdBillingMethod = 64;

        public override bool IsPrimary => true;

        public static string BillingMethodName
        {
            get
            {
                return "MBWayBillingMethod";
            }
        }

        public MBWayBillingMethod()
        {
            BillingMethodId = IdBillingMethod;
        }

        public string PhoneNumber { get; set; }

        public override bool UseTransaction
        {
            get
            {
                return true;
            }
        }

        public override bool IsGlobalPayment
        {
            get
            {
                return true;
            }
        }
    }
}

