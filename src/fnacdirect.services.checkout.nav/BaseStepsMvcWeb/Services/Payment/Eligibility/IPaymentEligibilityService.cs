using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.OrderPipe.Base.Model.Facets;

namespace FnacDirect.OrderPipe.Base.Business.Payment
{
    public interface IPaymentEligibilityService
    {
        PaymentEligibilityRule GetRules(string property, bool disableEccvCheck = false, bool disableClickAndCollectCheck = false);
        bool IsAllowed(IGotLineGroups gotLineGroups, PaymentEligibilityRule rules);
    }
}
