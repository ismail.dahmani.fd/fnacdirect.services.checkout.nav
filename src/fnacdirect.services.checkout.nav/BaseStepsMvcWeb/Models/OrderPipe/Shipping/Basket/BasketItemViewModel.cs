using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Shipping
{
    public class BasketItemViewModel
    {
        public string Title { get; set; }
        public string NormalImage { get; set; }
        public bool IsBook { get; set; }
        public bool IsEbook { get; set; }
        public bool HasD3E { get; set; }
        public bool HasRCP { get; set; }
    }
}
