using FnacDirect.Context;
using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Common.Payment;
using FnacDirect.OrderPipe.BaseMvc.Web.Services.Pop.Payment;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Pop.Models.SelfCheckout
{
    public class SelfCheckoutCryptoViewModelBuilder : ISelfCheckoutCryptoViewModelBuilder
    {
        private readonly ICustomerCreditCardBaseViewModelBuilder _customerCreditCardBaseViewModelBuilder;
        private readonly IUserContextProvider _userContextProvider;
        private readonly IPaymentMethodService _paymentMethodService;

        public SelfCheckoutCryptoViewModelBuilder(ICustomerCreditCardBaseViewModelBuilder customerCreditCardBaseViewModelBuilder,
                                                   IPaymentMethodService paymentMethodService,
                                                   IUserContextProvider userContextProvider)
        {
            _customerCreditCardBaseViewModelBuilder = customerCreditCardBaseViewModelBuilder;
            _paymentMethodService = paymentMethodService;
            _userContextProvider = userContextProvider;
        }

        public SelfCheckoutCryptoViewModel Build(OPContext opContext)
        {
            var popOrderForm = opContext.OF as PopOrderForm;
            var checkCardValidation = new SelfCheckoutCryptoViewModel();
            var accountReference = _userContextProvider.GetCurrentUserContext().GetUserGuid().ToString();

            checkCardValidation.CustomerCreditCardViewModel = _customerCreditCardBaseViewModelBuilder.BuildSelfCheckoutCreditCardViewModel(popOrderForm, accountReference);
            checkCardValidation.OgoneAliasGatewayViewModel = _paymentMethodService.SetAliasGatewayViewModelForSelfCheckout(checkCardValidation.CustomerCreditCardViewModel, opContext ,popOrderForm);

            return checkCardValidation;
        }
    }
}
