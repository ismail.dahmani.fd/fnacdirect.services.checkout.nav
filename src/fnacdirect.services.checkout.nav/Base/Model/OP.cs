using FnacDirect.OrderPipe.Config;
using FnacDirect.OrderPipe.Core.Model.Attributes;
using FnacDirect.OrderPipe.Core.Services;
using FnacDirect.Technical.Framework.CoreServices;
using FnacDirect.Technical.Framework.ServiceLocation;
using Spring.Core.TypeResolution;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace FnacDirect.OrderPipe
{
    /// <summary>
    /// Implémentation du moteur de pipe
    /// </summary>
    public class OP : IOP
    {
        private readonly ISwitchProvider _switchProvider;

        private readonly static object InitializationLock
            = new object();

        private string _configKey = "Default";
        private string _controllerName = "op.opcontroller.default";

        /// <summary>
        /// Clé de configuration de cette instance du pipe dans la config retournée par OPCOnfigLoader
        /// </summary>
        public string ConfigKey
        {
            get { return _configKey; }
            set { _configKey = value; }
        }

        public string PipeName { get; internal set; }

        public string ControllerName
        {
            get { return _controllerName; }
            set { _controllerName = value; }
        }

        public string LogonType { get; set; }

        /// <summary>
        /// Chargeur de configuration
        /// </summary>
        public IOPConfigLoader OPConfigLoader { get; set; }

        /// <summary>
        /// Gestionnaire d'OrderForm
        /// </summary>
        public IOFManager OFManager { get; set; }

        public TypeResolver TypeResolver { get; set; }

        private readonly List<IStep> _steps = new List<IStep>();

        /// <summary>
        /// Liste des étapes
        /// </summary>
        public IEnumerable<IStep> Steps
        {
            get
            {
                return _steps;
            }
        }

        private readonly IDictionary<string, IStep> _stepIndex = new Dictionary<string, IStep>();

        public UIPipeDescriptor UIDescriptor { get; set; }

        private static readonly Regex NameRegEx = new Regex("^(.*[^0-9])([0-9]*)$");

        /// <summary>
        /// Méthode de configuration du Pipe
        /// </summary>
        public void LoadConfiguration()
        {
            try
            {
                lock (InitializationLock)
                {
                    // lecture de la configuration
                    var pipe = GetConfig();

                    // Chargement du descripteur d'interface global du pipe
                    UIDescriptor = pipe.UIDescriptor;
                    UIDescriptor.Init(this, pipe);
                    // Chargement des paramètres globaux
                    GlobalParameters = pipe.Parameters ?? new ParameterList();
                    // Configuration du gestionnaire d'orderform
                    OFManager.Type = TypeResolver.Resolve(pipe.OrderFormClass);
                }
            }
            catch (OPException)
            {
                throw;
            }
            catch (Exception e)
            {
                throw OPException.BuildException("Setup OP failed", e, OPRange.Run, 7, "CONFIG", _configKey);
            }
        }

        public void SetupSteps()
        {
            lock (InitializationLock)
            {
                // lecture de la configuration
                var pipe = GetConfig();

                LoadSteps(pipe);

                // Initialisation
                Initialize();
            }
        }

        public void SetupStepsWithoutInit()
        {
            lock (InitializationLock)
            {
                // lecture de la configuration
                var pipe = GetConfig();

                LoadSteps(pipe, true);
            }
        }

        private Pipe GetConfig()
        {
            var pipeConfig = OPConfigLoader.ReadConfig();

            if (pipeConfig == null)
            {
                throw OPException.BuildException("OP engine configuration not found", OPRange.Config, 5);
            }

            Pipe pipe;

            if (pipeConfig.Pipe.Contains(ConfigKey))
            {
                pipe = pipeConfig.Pipe[ConfigKey];
            }
            else
            {
                throw OPException.BuildException("OP configuration not found", OPRange.Config, 8, "CONFIG", _configKey);
            }

            return pipe;
        }

        private void LoadSteps(Pipe p, bool loadLigth = false)
        {
            // Configuration des étapes
            _steps.Clear();
            _stepIndex.Clear();
            foreach (var stepConfiguration in p.Steps)
            {
                IStep step;

                if (loadLigth)
                {
                    step = ConfigLightStep(stepConfiguration, p);
                }
                else
                {
                    step = ConfigStep(stepConfiguration, p);
                }

                if (step == null)
                {
                    continue;
                }

                // Modification forcée du nom de l'étape auto-généré
                if (_stepIndex.ContainsKey(step.Name))
                {
                    if (string.IsNullOrEmpty(stepConfiguration.Name))
                    {
                        var m = NameRegEx.Match(step.Name);
                        var name = m.Groups[1].Value;
                        var number = m.Groups[2].Value;
                        var n = string.IsNullOrEmpty(number) ? 1 : int.Parse(number, CultureInfo.InvariantCulture) + 1;
                        step.Name = name + n;
                    }
                    else
                    {
                        throw OPException.BuildException($"Duplicate name in pipe definition: {step.Name}", OPRange.Config, 6);
                    }
                }

                _steps.Add(step);
                _stepIndex.Add(step.Name, step);
            }
        }
        

        /// <summary>
        /// Configure une étape
        /// </summary>
        /// <param name="stepConfiguration">Configuration d'une étape</param>
        /// <param name="pipe">Configuration du pipe </param>
        /// <returns></returns>
        private IStep ConfigLightStep(Core.Model.Config.BaseStep stepConfiguration, Pipe pipe)
        {
            if (!CheckSwitch(stepConfiguration))
            {
                return null;
            }

            // instanciation de l'étape
            var step = ServiceLocator.Current.GetInstance(TypeResolver.Resolve(stepConfiguration.Class)) as IStep;

            if (stepConfiguration.Name != null)
            {
                step.Name = stepConfiguration.Name;
            }

            // configuration d'une étape de groupe
            if (step is GroupStep groupStep && stepConfiguration is Core.Model.Config.GroupStep groupStepConfiguration)
            {
                groupStep.TransactionManager = TransactionManager;
                groupStep.UseTransaction = groupStepConfiguration.UseTransaction;

                foreach (var childStepConfiguration in groupStepConfiguration.Steps)
                {
                    var childStep = ConfigLightStep(childStepConfiguration, pipe);

                    if (childStep != null)
                    {
                        groupStep.Steps.Add(childStep);
                    }
                }
            }

            step.Parameters = stepConfiguration.Parameters ?? new ParameterList();

            return step;
        }

        /// <summary>
        /// Configure une étape
        /// </summary>
        /// <param name="stepConfiguration">Configuration d'une étape</param>
        /// <param name="pipe">Configuration du pipe </param>
        /// <returns></returns>
        private IStep ConfigStep(Core.Model.Config.BaseStep stepConfiguration, Pipe pipe)
        {
            if (!CheckSwitch(stepConfiguration))
            {
                return null;
            }

            // instanciation de l'étape
            var step = ServiceLocator.Current.GetInstance(TypeResolver.Resolve(stepConfiguration.Class)) as IStep;

            if (stepConfiguration.Name != null)
            {
                step.Name = stepConfiguration.Name;
            }

            step.Group = stepConfiguration.Group;
            if (stepConfiguration.Shortcuts != null)
            {
                step.Shortcuts.AddRange(stepConfiguration.Shortcuts.Split(new char[] { ',', ';', ' ' }, StringSplitOptions.RemoveEmptyEntries));
            }

            if (step is TransactionStep tstep)
            {
                tstep.TransactionManager = TransactionManager;
            }

            // configuration d'une étape de groupe
            if (step is GroupStep groupStep && stepConfiguration is Core.Model.Config.GroupStep groupStepConfiguration)
            {
                groupStep.TransactionManager = TransactionManager;
                groupStep.UseTransaction = groupStepConfiguration.UseTransaction;

                foreach (var childStepConfiguration in groupStepConfiguration.Steps)
                {
                    var childStep = ConfigStep(childStepConfiguration, pipe);

                    if (childStep != null)
                    {
                        groupStep.Steps.Add(childStep);
                    }
                }
            }

            // configuration d'une étape interactive (configuration de la redirection)
            var interactiveStep = step as IInteractiveStep;
            if (stepConfiguration is InteractiveStep cistep)
            {
                interactiveStep.CanUseDirectAccess = cistep.UseDirectAccess;
                interactiveStep.IsDirectDefault = cistep.DirectAccess;

                interactiveStep.Pass2Shortcut = cistep.Pass2Shortcut;

                switch (cistep.SecurityMode)
                {
                    case SecurityMode.None:
                        break;
                    case SecurityMode.Secured:
                        interactiveStep.Secured = true;
                        break;
                    case SecurityMode.Authenticated:
                        interactiveStep.Secured = true;
                        interactiveStep.Authenticated = true;
                        break;
                    case SecurityMode.DontModify:
                        interactiveStep.SecurityAgnostic = true;
                        break;
                }

                interactiveStep.UIDescriptor = cistep.UIDescriptor;

                if (string.IsNullOrEmpty(cistep.NoDirectAccessBehavior))
                    interactiveStep.NoDirectAccessBehavior = NoDirectAccessBehavior.Next;
                else
                {
                    var noDirectAccessBehavior = Enum.Parse(typeof(NoDirectAccessBehavior), cistep.NoDirectAccessBehavior, true);
                    if (noDirectAccessBehavior != null && noDirectAccessBehavior.GetType().Equals(typeof(NoDirectAccessBehavior)))
                    {
                        interactiveStep.NoDirectAccessBehavior = (NoDirectAccessBehavior)noDirectAccessBehavior;
                    }
                    else
                    {
                        interactiveStep.NoDirectAccessBehavior = NoDirectAccessBehavior.Next;
                    }
                }

                interactiveStep.UIDescriptor.Init(interactiveStep, this, cistep, pipe);
            }

            // paramètres de l'étape
            step.Parameters = stepConfiguration.Parameters ?? new ParameterList();

            // transitions spécifiques de l'étape
            if (step is Step forwardableStep && stepConfiguration is Core.Model.Config.Step forwardableStepConfiguration)
            {
                forwardableStep.Forwards = forwardableStepConfiguration.Forwards ?? new ForwardList();
            }

            return step;
        }

        public bool CheckSwitch(Core.Model.Config.BaseStep stepConfiguration)
        {
            if (string.IsNullOrEmpty(stepConfiguration.SwitchId)
                && string.IsNullOrEmpty(stepConfiguration.Name))
            {
                return true;
            }

            if (string.IsNullOrEmpty(stepConfiguration.SwitchId))
            {
                return true;
            }

            if (!string.IsNullOrEmpty(stepConfiguration.SwitchIdNegate)
                && string.Equals(stepConfiguration.SwitchIdNegate, "true", StringComparison.InvariantCultureIgnoreCase))

            {
                return !_switchProvider.IsEnabled(stepConfiguration.SwitchId);
            }

            return _switchProvider.IsEnabled(stepConfiguration.SwitchId);
        }

        /// <summary>
        /// Initialisation du pipe
        /// </summary>
        private void Initialize()
        {
            var switchProvider = ServiceLocator.Current.GetInstance<ISwitchProvider>();

            Stopwatch.SetDebugMode(DebugMode);

            // initialisation du gestionnaire d'orderform
            OFManager.Initialize(this);

            // initialisation des étapes
            foreach (var s in Steps)
            {
                try
                {
                    s.Init(this, switchProvider);
                }
                catch (Exception e)
                {
                    throw OPException.BuildException("OP initialization failed", e, OPRange.Config, 1, "CONFIG",
                        _configKey, "STEP", s.Name);
                }
            }
        }

        /// <summary>
        /// Exécution du pipe
        /// </summary>
        /// <param name="context">Contexte d'exécution</param>
        /// <param name="forceistep">Eventuellement, nom de l'étape dont l'exécution doit être forcée</param>
        /// <param name="pass2">Vrai si on traite un PostBack d'étape interactive (seconde passe)</param>
        /// <returns></returns>
        public PipeResult Run(OPContext context, string forceistep, bool pass2)
        {
            Stopwatch.SetDebugMode(DebugMode);
            try
            {
                using (new Stopwatch("Run"))
                {
                    var res = new PipeResult();

                    // lecture/initialisation de l'orderform
                    var of = OFManager.Read(context, context.SID);

                    var executionContextKeysProvider = ServiceLocator.Current.GetInstance<IExecutionContextKeysProvider>();
                    var contextKeys = executionContextKeysProvider.Get();
                    foreach (var contextItem in contextKeys)
                    {
                        of.ExecutionContext.Set(contextItem.Key, contextItem.Value);
                    }

                    // mise à jour du contexte
                    context.OF = of;
                    context.Pipe = this;
                    context.ErrorInfo = null;
                    context.ErrorStep = null;

                    // étape théorique (correspond à la dernière étape exécutée)
                    res.TheoricStep = context.OF.CurrentStep;

                    // si on ne force pas l'étape, c'est qu'on est toujours dans l'étape interactive courante
                    // il faut donc forcer l'affichage de l'étape courante
                    forceistep = forceistep ?? context.OF.CurrentStep;

                    if (of.IsCurrentlyRunningCriticalSection)
                    {
                        Logging.Current.WriteWarning("OP have ben asked to run while it was already running inside critical section");

                        return res;
                    }

                    string shortcut = null;

                    if (forceistep != null)
                    {
                        if (GetStepByName(forceistep) is IInteractiveStep istp)
                        {
                            // réinitialisation de l'étape forcée
                            istp.Reset(context);
                            // si il s'agit d'une passe 2 sur une étape interactive on traite le raccourci
                            if (pass2 && istp.Pass2Shortcut != null)
                            {
                                shortcut = istp.Pass2Shortcut;
                            }
                        }
                    }

                    Stopwatch.Current.Result += " forceistep=" + forceistep + " - pass2=" + pass2 + " - shortcut=" + shortcut;

                    // boucle sur les étapes, en remplissant le résultat res
                    LoopOnSteps(context, forceistep, res, null, false, shortcut);

                    // déshydrate l'order form si il n'y a pas eu d'erreur.
                    if (context.ErrorInfo == null)
                    {
                        context.OF.IsCurrentlyRunningCriticalSection = false;
                        OFManager.Write(context, context.OF);
                    }

                    Stopwatch.Current.Result += res.CurrentStep.Name;

                    return res;
                }
            }
            catch (OPException)
            {
                throw;
            }
            catch (Exception e)
            {
                throw OPException.BuildException("RUN OP failed", context, e, OPRange.Run, 3);
            }
        }

        public ITransactionManager TransactionManager { get; set; }

        private string CreateLoopExceptionInfos(OPContext context)
        {
            var stringBuilder = new StringBuilder();

            stringBuilder.AppendLine("<ExecutedSteps>");
            foreach (var step in context.ExecutedSteps)
            {
                stringBuilder.AppendLine(string.Format("<Step>{0}</Step>", step));
            }
            stringBuilder.AppendLine("</ExecutedSteps>");

            return stringBuilder.ToString();
        }

        /// <summary>
        /// Exécute les étapes du pipe
        /// </summary>
        /// <param name="context">Contexte d'exécution</param>
        /// <param name="forceistep">Etape où il faut arrêter l'exécution</param>
        /// <param name="res">Résultat de l'exécution</param>
        /// <param name="startfromstep">Etape où il faut commencer l'exécution</param>
        /// <param name="continueMode">Mode continuation, après une étape interactive</param>
        /// <param name="shortcut">Nom du raccourci à utiliser</param>
        private void LoopOnSteps(OPContext context, string forceistep, PipeResult res,
            string startfromstep, bool continueMode, string shortcut)
        {
            var transac_error = false;
            try
            {
                bool loop;
                var loopCount = 0;
                context.ExecutedSteps = new List<string>();
                context.ContinueMode = continueMode;
                do
                {
                    context.ForcedStep = forceistep;
                    loopCount++;
                    if (loopCount > 10)
                    {
                        var hashcode = string.Join("_", context.ExecutedSteps).GetHashCode();
                        var additionalInfos = CreateLoopExceptionInfos(context);
                        throw OPException.BuildException("Orderpipe loop exception #" + hashcode, context, null, OPRange.Config, 2, new[] {
                            "DUMP", additionalInfos
                        });
                    }
                    loop = false;
                    var found = false;
                    var exit = false;
                    foreach (var s in Steps.ToList())
                    {
                        // on teste si il faut prendre un raccourci
                        if (shortcut != null)
                        {
                            if (s.Name == forceistep)
                            {
                                shortcut = null; // on arrête les raccourcis après l'étape à atteindre   
                            }
                            else if (!s.Shortcuts.Contains(shortcut))
                            {
                                context.Log("Step " + s.Name + " is shortcuted");
                                continue;
                            }
                        }

                        // on boucle jusqu'à trouver la première étape à exécuter
                        if (startfromstep == null || s.Name == startfromstep)
                            found = true;

                        if (found)
                        {
                            context.OF.CurrentStep = s.Name;

                            context.OF.PipeMessages.AutoClearFor(s.Name);

                            var inTransaction = context.IsInTransaction;
                            try
                            {
                                context.ExecutedSteps.Add(s.Name);
                                exit = s.Run(context, res, s.Name == forceistep);

                                if (!string.IsNullOrWhiteSpace(res.LastStepReturn.ReturnInfos))
                                {
                                    var index = context.ExecutedSteps.Count;
                                    context.ExecutedSteps[index - 1] += string.Format(" exitinfos=\"{0}\"", res.LastStepReturn.ReturnInfos);
                                }
                            }
                            catch (Exception e)
                            {
                                // En cas d'exception, on fait un forward vers le step de gestion d'erreur

                                if (TransactionManager != null)
                                {
                                    TransactionManager.Dispose(context);
                                }

                                context.Log(string.Format("Exception in {0} : {1}", s.Name, e));

                                context.OF.AddErrorMessage("OrderProcess.Error", "orderpipe.wsorderprocess.order.error", e.Message);

                                var inf = new ErrorInfo()
                                {
                                    Exception = e,
                                    FailedStep = s,
                                    Error = null,
                                    FailedInTransaction = inTransaction
                                };
                                context.ErrorInfo = inf;

                                if (string.IsNullOrEmpty(context.ErrorStep))
                                {
                                    if (context.OF.IsCurrentlyRunningCriticalSection)
                                    {
                                        context.OF.IsCurrentlyRunningCriticalSection = false;
                                        OFManager.Write(context, context.OF);
                                    }

                                    string[] dumps = null;
                                    if(_switchProvider.IsEnabled("orderpipe.pop.log.executedsteps"))
                                    {
                                        var additionalInfos = CreateLoopExceptionInfos(context);
                                        dumps = new[] { "DUMP", additionalInfos };
                                    }

                                    throw OPException.BuildException("Step run failed", context, e, OPRange.Run, 1, dumps);
                                }

                                context.Log("Forwarding from " + s.Name + " to " + context.ErrorStep);
                                startfromstep = context.ErrorStep;
                                loop = true;
                                exit = true;
                                break;

                            }
                            // si il faut sortir du pipe, on regarde le résultat de la dernière étape
                            if (exit)
                            {
                                // on recommence l'exécution depuis le début
                                if (res.LastStepReturn is RewindStepReturn)
                                {
                                    context.Log("Rewinding from " + s.Name);
                                    startfromstep = null;
                                    loop = true;
                                }
                                else
                                    // on recommence le pipe en commençant à l'étape indiquée
                                    if (res.LastStepReturn is StepReturnForward)
                                {
                                    context.Log("Forwarding from " + s.Name + " to " + ((StepReturnForward)res.LastStepReturn).Forward);
                                    startfromstep = ((StepReturnForward)res.LastStepReturn).Forward;
                                    loop = true;
                                }
                                else if (res.LastStepReturn is StepReturnError stepReturnError)
                                {
                                    context.Log("Error in " + s.Name);

                                    var inf = new ErrorInfo
                                    {
                                        Exception = null,
                                        FailedStep = s,
                                        Error = stepReturnError
                                    };
                                    context.ErrorInfo = inf;

                                    if (string.IsNullOrEmpty(context.ErrorStep))
                                    {
                                        throw OPException.BuildException("Step returned error", context, null, OPRange.Run, 2);
                                    }
                                    else
                                    {
                                        context.Log("Forwarding from " + s.Name + " to " + context.ErrorStep);
                                        startfromstep = context.ErrorStep;
                                        loop = true;
                                        break;
                                    }

                                }
                                break;
                            }
                        }
                    }
                    if (!exit || !found)
                        throw OPException.BuildException("Orderpipe config should end with an uncontinuing interactive step", context, null, OPRange.Config, 2);

                    forceistep = null;

                    if (loop)
                        context.ExecutedSteps.Add("<loop>");

                } while (loop);
            }
            finally
            {
                if (TransactionManager != null)
                {
                    transac_error = TransactionManager.Dispose(context);
                }
                context.ForcedStep = null;
            }
            if (transac_error)
                throw OPException.BuildException("Interactive step is forbidden in transaction", context, null, OPRange.Config, 3);

        }

        /// <summary>
        /// Continuation après étape interactive.
        /// Run a déjà été appelé.
        /// </summary>
        /// <param name="context">Contexte d'exécution</param>
        /// <returns></returns>
        public PipeResult Continue(OPContext context)
        {
            Stopwatch.SetDebugMode(DebugMode);

            try
            {
                using (new Stopwatch("Continue"))
                {
                    var res = new PipeResult();

                    context.ContinuingStep = context.OF.CurrentStep;

                    if (GetStepByName(context.OF.CurrentStep) is IInteractiveStep istp)
                    {
                        // Si continuation d'une étape avec raccourci, on rejoue le pipe depuis le début
                        if (istp.Pass2Shortcut != null)
                        {
                            LoopOnSteps(context, null, res, null, true, null);
                        }
                        else
                        {
                            LoopOnSteps(context, null, res, context.OF.CurrentStep, true, null);
                        }
                    }

                    if (context.ErrorInfo == null)
                    {
                        context.OF.IsCurrentlyRunningCriticalSection = false;
                        OFManager.Write(context, context.OF);
                    }
                    context.CurrentStep = res.CurrentStep;

                    Stopwatch.Current.Result += res.CurrentStep.Name;

                    return res;
                }
            }
            catch (OPException)
            {
                throw;
            }
            catch (Exception e)
            {
                throw OPException.BuildException("CONTINUE OP failed", context, e, OPRange.Run, 4);
            }
        }

        /// <summary>
        /// Rembobbinage pendant étape interactive.
        /// </summary>
        /// <param name="context">Contexte d'exécution</param>
        /// <param name="applyshortcut">Vrai si il faut utiliser le raccourci</param>
        /// <param name="forceistep">Si le rewind doit forcer le retour sur une step précise</param>
        /// <returns></returns>
        public PipeResult Rewind(OPContext context, bool applyshortcut, string forceistep = null)
        {
            Stopwatch.SetDebugMode(DebugMode);

            try
            {
                using (new Stopwatch("Rewind"))
                {
                    var pipeResult = new PipeResult();

                    string shortcut = null;

                    if (applyshortcut)
                    {
                        if (GetStepByName(context.OF.CurrentStep) is IInteractiveStep interactiveStep)
                        {
                            shortcut = interactiveStep.Pass2Shortcut;
                        }
                    }

                    LoopOnSteps(context, forceistep ?? context.OF.CurrentStep, pipeResult, null, false, shortcut);

                    OFManager.Write(context, context.OF);
                    context.CurrentStep = pipeResult.CurrentStep;

                    Stopwatch.Current.Result += pipeResult.CurrentStep.Name;

                    return pipeResult;
                }
            }
            catch (OPException)
            {
                throw;
            }
            catch (Exception e)
            {
                throw OPException.BuildException("REWIND OP failed", context, e, OPRange.Run, 5);
            }
        }

        /// <summary>
        /// Ajoute une contrainte globale
        /// </summary>
        /// <param name="of">OrderForm</param>
        /// <param name="step">Nom de l'étape contraignante</param>
        /// <param name="name">Nom de la contrainte</param>
        /// <returns>L'objet contrainte ajouté</returns>
        public static Constraint AddGlobalConstraint(IOF of, string step, string name)
        {
            if (of.Constraints != null && of.Constraints.Contains(name))
            {
                return null;
            }

            var constraint = new Constraint
            {
                Name = name,
                ConstrainerStepName = step
            };

            if (of.Constraints == null)
            {
                of.Constraints = new ConstraintList();
            }

            of.Constraints.Add(constraint);

            return constraint;
        }

        /// <summary>
        /// Lit une contrainte globale
        /// </summary>
        /// <param name="of">OrderForm</param>
        /// <param name="cname">Nom de la contrainte</param>
        /// <returns>La contrainte ou null si absente</returns>
        public static Constraint GetGlobalConstraint(IOF of, string cname)
        {
            if (of.Constraints != null && of.Constraints.Contains(cname))
            {
                return of.Constraints[cname];
            }

            return null;
        }

        public static void RemoveGlobalConstraint(IOF of, string cname)
        {
            if (of.Constraints != null && of.Constraints.Contains(cname))
            {
                of.Constraints.Remove(cname);
            }
        }

        /// <summary>
        /// Ensemble des paramètres globaux du Pipe, tels que définis dans la config.
        /// </summary>
        public ParameterList GlobalParameters { get; internal set; }

        public void Reset(OPContext opContext)
        {
            Stopwatch.SetDebugMode(DebugMode);

            try
            {
                //Récupération de l'OF actuel pour gérer ensuite les NoReset
                var currentOF = OFManager.Read(opContext, opContext.SID);

                // initialisation de l'orderform
                opContext.OF = OFManager.BuildOF(opContext, opContext.SID);

                //Récupération des valeurs en NoReset pour les réaffecter à l'OrderForm réinitialisé
                var properties = OFManager.Type.GetProperties().Where(p => p.GetCustomAttributes(typeof(NoReset), true).Length > 0);
                foreach (var pi in properties)
                {
                    OFManager.Type.GetProperty(pi.Name).SetValue(opContext.OF, OFManager.Type.GetProperty(pi.Name).GetValue(currentOF, null), null);
                }

                var executionContextKeysProvider = ServiceLocator.Current.GetInstance<IExecutionContextKeysProvider>();
                var contextKeys = executionContextKeysProvider.Get();
                foreach (var contextItem in contextKeys)
                {
                    opContext.OF.ExecutionContext.Set(contextItem.Key, contextItem.Value);
                }

                // sauvegarde
                OFManager.Write(opContext, opContext.OF);
            }
            catch (OPException)
            {
                throw;
            }
            catch (Exception e)
            {
                throw OPException.BuildException("RESET OP failed", opContext, e, OPRange.Run, 6);
            }
        }

        public IStep GetStepByName(string stepName)
        {
            if (_stepIndex.ContainsKey(stepName))
            {
                return _stepIndex[stepName];
            }

            throw OPException.BuildException("Step not found", OPRange.Config, 4, "STEPNAME", stepName);
        }

        public OP()
        {
            LogonType = null;
            _switchProvider = ServiceLocator.Current.GetInstance<ISwitchProvider>();
        }

        public bool DebugMode { get; set; }
        public string SwitchId { get; set; }
    }
}
