namespace FnacDirect.OrderPipe.Base.Business.Tickets.ServiceClient
{
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public partial class TicketGeneratorServiceClient : System.ServiceModel.ClientBase<FnacDirect.OrderPipe.Base.Business.Tickets.ServiceClient.ITicketGeneratorService>, FnacDirect.OrderPipe.Base.Business.Tickets.ServiceClient.ITicketGeneratorService
    {

        public TicketGeneratorServiceClient()
        {
        }

        public TicketGeneratorServiceClient(string endpointConfigurationName) :
                base(endpointConfigurationName)
        {
        }

        public TicketGeneratorServiceClient(string endpointConfigurationName, string remoteAddress) :
                base(endpointConfigurationName, remoteAddress)
        {
        }

        public TicketGeneratorServiceClient(string endpointConfigurationName, System.ServiceModel.EndpointAddress remoteAddress) :
                base(endpointConfigurationName, remoteAddress)
        {
        }

        public TicketGeneratorServiceClient(System.ServiceModel.Channels.Binding binding, System.ServiceModel.EndpointAddress remoteAddress) :
                base(binding, remoteAddress)
        {
        }

        public FnacDirect.OrderPipe.Base.Business.Tickets.ServiceClient.GeneratedTicket GenerateTGVTicket(string refgu, string numCaisse)
        {
            return base.Channel.GenerateTGVTicket(refgu, numCaisse);
        }
    }
}
