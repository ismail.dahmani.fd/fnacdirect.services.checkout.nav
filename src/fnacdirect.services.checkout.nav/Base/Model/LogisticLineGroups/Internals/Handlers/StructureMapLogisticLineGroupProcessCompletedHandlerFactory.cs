﻿using StructureMap;
using StructureMap.Pipeline;
using System;

namespace FnacDirect.OrderPipe.Base.Business.LogisticLineGroups.Internals.Handlers
{
    public class StructureMapLogisticLineGroupProcessCompletedHandlerFactory : ILogisticLineGroupProcessCompletedHandlerFactory
    {
        private readonly IContainer _container;

        public StructureMapLogisticLineGroupProcessCompletedHandlerFactory(IContainer container)
        {
            _container = container;
        }

        private void EnsureTypeIsILogisticLineGroupProcessCompletedHandler(Type type)
        {
            if(!typeof(ILogisticLineGroupProcessCompletedHandler).IsAssignableFrom(type))
            {
                throw new ArgumentException("Type should be ILogisticLineGroupProcessCompletedHandler");
            }
        }
        
        public ILogisticLineGroupProcessCompletedHandler Get(ILogisticLineGroupProcessCompletedHandlerDescriptor logisticLineGroupProcessCompletedHandlerDescriptor)
        {
            var type = logisticLineGroupProcessCompletedHandlerDescriptor.Type;

            EnsureTypeIsILogisticLineGroupProcessCompletedHandler(type);

            return _container.GetInstance(type, logisticLineGroupProcessCompletedHandlerDescriptor.GetExplicitArguments()) as ILogisticLineGroupProcessCompletedHandler;
        }
    }
}
