﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.TagCommander
{
    public class TagCommanderThankYouViewModel : TagCommanderOrderViewModel
    {
        public TagCommanderThankYouViewModel()
            : base()
        {
        }

        public string OrderId { get; set; }
        public string OrderUserRefs { get; set; }
        public string OrderPayMethod { get; set; }
    }
}
