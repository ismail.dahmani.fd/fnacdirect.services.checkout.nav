using FnacDirect.Ogone.BLL;
using FnacDirect.OrderPipe.Base.Model.Facets;
using FnacDirect.OrderPipe.Base.Model.FDV;

namespace FnacDirect.OrderPipe.Base.Proxy.Ogone.PspId
{
    /// <summary>
    /// Permet de recuperer le bon service, selon le pipe et les parametres.
    /// </summary>
    public class PspIdServiceFactory : IPspIdServiceFactory
    {
        private readonly IOgoneConfigurationService _ogoneConfigurationService;
        private readonly IOgoneConfigurationProvider _ogoneConfigurationProvider;

        public PspIdServiceFactory(IOgoneConfigurationService ogoneConfigurationService, IOgoneConfigurationProvider ogoneConfigurationProvider)
        {
            _ogoneConfigurationService = ogoneConfigurationService;
            _ogoneConfigurationProvider = ogoneConfigurationProvider;
        }

        private AOgoneConfigurationManager ConfigurationManager
        {
            get { return _ogoneConfigurationProvider.GetOgoneConfigurationManager(); }
        }

        public IPspIdService GetInstance(string accountId, int? legalEntityId = null)
        {
            if (legalEntityId.HasValue)
            {
                return new SelfCheckoutPspIdService(legalEntityId.Value, ConfigurationManager);
            }

            return new DefaultPspIdService(accountId, _ogoneConfigurationService);
        }
    }
}
