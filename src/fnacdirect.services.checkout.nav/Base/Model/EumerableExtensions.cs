﻿using System.Collections.Generic;

namespace FnacDirect.OrderPipe.Base.Model
{
    public static class EumerableExtensions
    {
        public static ServiceList ToServiceList(this IEnumerable<Service> services)
        {
            var serviceList = new ServiceList();
            
            serviceList.AddRange(services);

            return serviceList;
        }
    }
}
