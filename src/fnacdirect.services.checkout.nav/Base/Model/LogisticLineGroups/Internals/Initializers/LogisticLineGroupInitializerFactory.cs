﻿using FnacDirect.Technical.Framework.Velocity;
using System;

namespace FnacDirect.OrderPipe.Base.Business.LogisticLineGroups.Internals.Initializers
{
    public class LogisticLineGroupInitializerFactory<T> : ILogisticLineGroupInitializerFactory<T>
            where T : BaseLogisticLineGroupInitializer
    {
        private readonly Func<T> _factory;
        private readonly ICloneService _cloneService;
        private readonly ISwitchProvider _switchProvider;

        public LogisticLineGroupInitializerFactory(Func<T> factory, 
                                                   ICloneService cloneService,
                                                   ISwitchProvider switchProvider)
        {
            _factory = factory;
            _cloneService = cloneService;
            _switchProvider = switchProvider;
        }
        
        public ILogisticLineGroupInitializer Get(ILogisticLineGroupGenerationProcessContext context)
        {
            var instance = _factory();

            instance.Context = context;
            instance.Helper = new LogisticLineGroupInitializerHelper(_cloneService, _switchProvider);

            return instance;
        }
    }
}
