using FnacDirect.Context;
using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.OrderPipe.Base.Model.Facets;
using FnacDirect.OrderPipe.Base.Model.LineGroups;
using FnacDirect.OrderPipe.Core.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using Vanilla.Middle.Arborescence;

namespace FnacDirect.OrderPipe.Base.Business.Payment
{
    public class GiftPaymentService : IGiftPaymentService
    {
        private readonly IMaintenanceConfigurationService _maintenanceConfigurationService;
        private readonly ISwitchProvider _switchProvider;
        private readonly ICheckArticleService _checkArticleService;
        private readonly IPipeParametersProvider _pipeParametersProvider;
        private readonly ITreePathServices _treePathServices;
        private readonly IPaymentEligibilityService _paymentEligibilityService;

        public GiftPaymentService(IMaintenanceConfigurationService maintenanceConfigurationService,
            ISwitchProvider switchProvider,
            ICheckArticleService checkArticleService,
            IPipeParametersProvider pipeParametersProvider,
            ITreePathServices treePathServices,
            IPaymentEligibilityService paymentEligibilityService)
        {
            _maintenanceConfigurationService = maintenanceConfigurationService;
            _switchProvider = switchProvider;
            _checkArticleService = checkArticleService;
            _pipeParametersProvider = pipeParametersProvider;
            _treePathServices = treePathServices;
            _paymentEligibilityService = paymentEligibilityService;
        }

        public bool IsGiftCardAllowed(IGotLineGroups iGotLineGroups, decimal totalPriceEur, int anonymousBillingMethodsMaxAmount, bool isGuest, bool disableClickAndCollectCheck = false, bool disableMaxAmount = false, bool disableEccvCheck = false)
        {
            var rules = _paymentEligibilityService.GetRules("orderpipe.pop.payment.giftcard.eligibilityrules",
                disableClickAndCollectCheck: disableClickAndCollectCheck,
                disableEccvCheck: disableEccvCheck);

            if (!_paymentEligibilityService.IsAllowed(iGotLineGroups, rules))
            {
                return false;
            }

            var articles = iGotLineGroups.Articles;

            if (isGuest)
            {
                return false;
            }

            if (_maintenanceConfigurationService.Kadeos)
            {
                return false;
            }

            if (iGotLineGroups.LineGroups.GetArticlesOfType<MarketPlaceArticle>().Any()
                && _switchProvider.IsEnabled("orderpipe.pop.payment.filtersecondaryformarketplace"))
            {
                return false;
            }

            return IsAllowed(articles, totalPriceEur, anonymousBillingMethodsMaxAmount, disableMaxAmount);
        }

        public bool IsCcvAllowed(PopOrderForm popOrderForm, IEnumerable<ILineGroup> lineGroups, decimal totalPriceEur, int anonymousBillingMethodsMaxAmount, bool isGuest, bool disableClickAndCollectCheck = false, bool disableMaxAmount = false, bool disableEccvCheck = false)
        {
            var rules = _paymentEligibilityService.GetRules("orderpipe.pop.payment.ccvaccount.eligibilityrules",
                disableClickAndCollectCheck: disableClickAndCollectCheck,
                disableEccvCheck: disableEccvCheck);

            if (!_paymentEligibilityService.IsAllowed(popOrderForm, rules))
            {
                return false;
            }

            var articles = lineGroups.GetArticles();

            if (isGuest)
            {
                return false;
            }

            if (_maintenanceConfigurationService.CCV)
            {
                return false;
            }

            if (lineGroups.GetArticlesOfType<MarketPlaceArticle>().Any()
                && _switchProvider.IsEnabled("orderpipe.pop.payment.filtersecondaryformarketplace"))
            {
                return false;
            }

            if (articles.OfType<StandardArticle>().Any(x => x.HasRemiseClient))
            {
                return false;
            }

            return IsAllowed(articles, totalPriceEur, anonymousBillingMethodsMaxAmount, disableMaxAmount);
        }

        private bool IsAllowed(IEnumerable<Article> articles, decimal totalPriceEur, int anonymousBillingMethodsMaxAmount, bool disableMaxAmount = false)
        {
            if (totalPriceEur > anonymousBillingMethodsMaxAmount && !disableMaxAmount && _switchProvider.IsEnabled("orderpipe.pop.max.amount.anonymous.billing.methods"))
            {
                return false;
            }

            if (_checkArticleService.HasDonation(articles))
            {
                return false;
            }

            if (_checkArticleService.HasSensibleSupportId(articles))
            {
                return false;
            }

            return true;
        }
    }
}
