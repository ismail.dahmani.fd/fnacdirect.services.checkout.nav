using Models = FnacDirect.OrderPipe.TaxCalculation.Front.Models;
using System.Collections.Generic;
using System.Linq;

namespace FnacDirect.OrderPipe.Base.Model.TaxCalculation
{
    public class OrderPipeLineGroupQuery : Query
    {
        private List<TaxCalculationQueryGlobalPrice> GlobalPrices { get; set; }
        public List<TaxCalculationQueryItem> Items { get; set; }
        public bool HasShippableItems { get; set; }

        public OrderPipeLineGroupQuery()
        {
            GlobalPrices = new List<TaxCalculationQueryGlobalPrice>();
            Items = new List<TaxCalculationQueryItem>();
            HasShippableItems = false;
        }

        public TaxCalculationQueryGlobalPrice GetGlobalPriceByType(GlobalPriceType globalPriceType)
        {
            return GlobalPrices.FirstOrDefault(gp => gp.PriceType == globalPriceType);
        }

        public void AddShippingGlobalPrice(decimal cost, int taxType)
        {
            var shippingGlobalPrice = GetGlobalPriceByType(GlobalPriceType.Shipping);
            if (shippingGlobalPrice != null)
            {
                shippingGlobalPrice.Cost = cost;
                shippingGlobalPrice.TaxInfos.TaxType = taxType;
            }
            else
            {
                var shippingGlobalPriceToAdd = new TaxCalculationQueryGlobalPrice(GlobalPriceType.Shipping, taxType, cost);

                GlobalPrices.Add(shippingGlobalPriceToAdd);
            }
        }

        public Models.LineGroupQuery ToLineGroupQuery()
        {
            var lineGroupQuery = new Models.LineGroupQuery
            {
                OrderContext = OrderContext.ToOrderContext(),
                HasShippableItems = HasShippableItems
            };

            lineGroupQuery.Items = Items.Select(i => new Models.ItemRequest
            {
                Identifier = new Models.Identifier
                {
                    Prid = i.Identifier.Prid,
                    OfferReference = i.Identifier.OfferReference
                },
                SellerId = i.SellerId,
                IsDematItem = i.IsDematItem,
                IsService = i.IsService,
                SalesInfo = i.SalesInfo.ToTaxCalculationSalesInfo()
            }).ToList();

            var shippingTaxCalculationQueryGlobalPrice = GetGlobalPriceByType(GlobalPriceType.Shipping);
            lineGroupQuery.AddGlobalPriceByType(Models.Enums.GlobalPriceType.Shipping, shippingTaxCalculationQueryGlobalPrice.Cost, shippingTaxCalculationQueryGlobalPrice.TaxInfos.TaxType);

            return lineGroupQuery;
        }
    }

    public class TaxCalculationQueryGlobalPrice
    {
        public GlobalPriceType PriceType { get; set; }
        public TaxInfos TaxInfos { get; set; }
        public decimal Cost { get; set; }

        public TaxCalculationQueryGlobalPrice(GlobalPriceType globalPriceType, int taxType, decimal cost)
        {
            PriceType = globalPriceType;
            TaxInfos = new TaxInfos { TaxType = taxType };
            Cost = cost;
        }
    }
}
