﻿using FnacDirect.OrderPipe.Base.BaseModel.Model;
using FnacDirect.OrderPipe.Base.LineGroups;
using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.OrderPipe.Base.Model.Pop;
using FnacDirect.OrderPipe.Base.Model.UserInformations;
using System.Collections.Generic;

namespace FnacDirect.OrderPipe.Base.Business
{
    public interface IGuptFnacComBusiness
    { 
        string GetResultReserveClickAndCollect(ILogisticLineGroup logisticLineGroup, 
                                               IUserContextInformations userContextInformations, 
                                               IEnumerable<BillingMethod> orderBillingMethods, 
                                               BillingAddress billingAddress, 
                                               ShopData shopData, 
                                               TelevisualRoyaltyData televisualRoyaltyData, 
                                               ShopAddress shopAddress, 
                                               bool isMobile, 
                                               PopAdherentData popAdherentData);
    }
}
