﻿namespace FnacDirect.OrderPipe.Base.Model.AddOn
{
    public enum PageRedirectType
    {
        Get,
        Post
    }
}
