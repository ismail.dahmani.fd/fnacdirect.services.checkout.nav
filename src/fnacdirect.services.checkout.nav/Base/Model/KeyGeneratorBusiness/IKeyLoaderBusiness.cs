using FnacDirect.KeyGenerator;
using FnacDirect.OrderPipe.Base.LineGroups;
using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.OrderPipe.Base.Model.OrderInformations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FnacDirect.OrderPipe.Base.Business.MainOrderTransactionInfo
{
    public interface IKeyLoaderBusiness
    {
        /// <summary>
        /// Renseigne la commande de l'OrderForm à partir de la clé 'mkey'
        /// </summary>
        void LoadKeyInOrderForm(MultiKey mkey, IOF of, IOrderGlobalInformations orderGlobalInformations, IEnumerable<ILogisticLineGroup> logisticLineGroups, IEnumerable<BillingMethod> orderBillingMethods);
    }
}
