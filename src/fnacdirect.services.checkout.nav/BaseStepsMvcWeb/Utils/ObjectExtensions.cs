namespace FnacDirect.OrderPipe
{
    public static class ObjectExtensions
    {
        public static string ToStringOrDefault(this object instance)
        {
            return (instance ?? string.Empty).ToString();
        }

        public static string ToStringOrDefault(this object instance, string value)
        {
            return (instance ?? value).ToString();
        }
    }
}
