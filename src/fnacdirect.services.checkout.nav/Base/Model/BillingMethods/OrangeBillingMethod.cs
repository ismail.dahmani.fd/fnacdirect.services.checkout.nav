using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FnacDirect.Ogone.Model;

namespace FnacDirect.OrderPipe.Base.Model
{
    public class OrangeBillingMethod : BillingMethod
    {
        public const int IdBillingMethod = 536870912;

        public override bool IsPrimary => true;

        public static string BillingMethodName
        {
            get
            {
                return "OrangeBillingMethod";
            }
        }

        public OrangeBillingMethod()
        {
            BillingMethodId = IdBillingMethod;
            Name = BillingMethodName;
        }

        public override bool UseTransaction
        {
            get
            {
                return true;
            }
        }

        public TransactionOperationType OperationType
        {
            get
            {
                return TransactionOperationType.RES;
            }
        }
    }
}
