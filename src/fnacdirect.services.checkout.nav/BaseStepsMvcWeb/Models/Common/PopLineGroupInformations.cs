using FnacDirect.OrderPipe.Base.Model;
using System;
using System.ComponentModel;
using System.Xml.Serialization;
using FnacDirect.Technical.Framework.Velocity;

namespace FnacDirect.OrderPipe.Base.LineGroups
{
    public class PopLineGroupInformations : ILineGroupInformations
    {
        public PopLineGroupInformations()
        {
            OrderTransactionInfos = new OrderTransactionInfoList();
        }

        public PopLineGroupInformations(PopLineGroupInformations popLineGroupInformations)
        {
            OrderType = popLineGroupInformations.OrderType;
            OrderTransactionInfos = CloneUtil.DeepCopy(popLineGroupInformations.OrderTransactionInfos);
            GiftOption = popLineGroupInformations.GiftOption;
            OrderMessage = popLineGroupInformations.OrderMessage;
            ChosenWrapMethod = popLineGroupInformations.ChosenWrapMethod;
            OrderDate = popLineGroupInformations.OrderDate;
            OrderStatusId = popLineGroupInformations.OrderStatusId;
            ShippingAddressId = popLineGroupInformations.ShippingAddressId;
        }

        public PopLineGroupInformations(LineGroupInfo lineGroupInfo)
        {
            OrderType = lineGroupInfo.OrderType;
            OrderTransactionInfos = CloneUtil.DeepCopy(lineGroupInfo.OrderTransactionInfos);
            GiftOption = lineGroupInfo.GiftOption;
            OrderMessage = lineGroupInfo.OrderMessage;
            ChosenWrapMethod = lineGroupInfo.ChosenWrapMethod;
            OrderDate = lineGroupInfo.OrderDate;
            OrderStatusId = lineGroupInfo.OrderStatusId;
            ShippingAddressId = lineGroupInfo.ShippingAddressId;
        }

        public int? OrderType { get; set; }

        public OrderTransactionInfoList OrderTransactionInfos { get; set; }

        public bool GiftOption { get; set; }

        public string OrderMessage { get; set; }

        [DefaultValue(0)]
        public int ChosenWrapMethod { get; set; }

        public DateTime OrderDate { get; set; }

        public int? OrderStatusId { get; set; }

        [XmlIgnore]
        public int ShippingAddressId { get; set; }
    }
}
