namespace FnacDirect.OrderPipe.Base.Model
{
    public class LastCUUMemoryData : StringValueData
    {
        private bool _addMode;

        public bool AddMode
        {
            get { return _addMode; }
            set { _addMode = value; }
        }
    }
}
