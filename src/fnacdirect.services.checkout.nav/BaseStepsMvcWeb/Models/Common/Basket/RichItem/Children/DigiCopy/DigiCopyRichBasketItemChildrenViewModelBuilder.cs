﻿using System.Linq;
using System.Collections.Generic;
using FnacDirect.Front.WebBusiness;
using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.Technical.Framework.Web.Mvc;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Basket.Children
{
    public class DigiCopyRichBasketItemChildrenViewModelBuilder : IRichBasketItemChildrenViewModelBuilder
    {
        private readonly IPriceHelper _priceHelper;

        public DigiCopyRichBasketItemChildrenViewModelBuilder(IApplicationContext applicationContext)
        {
            _priceHelper = applicationContext.GetPriceHelper();
        }

        public IEnumerable<ChildBasketItemViewModel> Build(StandardArticle standardArticle, IEnumerable<Promotion> globalPromotions, bool applyAdherentPromotion)
        {
            if (standardArticle != null
                && standardArticle.ArticleDTO != null
                && standardArticle.ArticleDTO.PartnerLinks != null 
                && standardArticle.ArticleDTO.PartnerLinks.Any(p => p.PartnerId == (int)PartnerId.DigitalLocker))
            {
                yield return new DigiCopyChildBasketItemViewModel
                {
                    Price = new PriceViewModel()
                    {
                        Free = true,
                        Price = _priceHelper.Format(0)
                    }
                };
            }
        }
    }
}
