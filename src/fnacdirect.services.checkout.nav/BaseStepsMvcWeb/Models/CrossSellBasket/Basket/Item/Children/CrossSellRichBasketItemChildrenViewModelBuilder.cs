using FnacDirect.Contracts.Online.IModel;
using FnacDirect.Contracts.Online.Model;
using FnacDirect.OrderPipe.Base.Business;
using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.OrderPipe.BaseMvc.Web.Helpers;
using FnacDirect.OrderPipe.BaseMvc.Web.Services.Article.Pipe;
using FnacDirect.Technical.Framework.Web;
using FnacDirect.Technical.Framework.Web.Mvc;
using System.Collections.Generic;
using System.Linq;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Basket.Children
{
    public class CrossSellRichBasketItemChildrenViewModelBuilder : ICrossSellRichBasketItemChildrenViewModelBuilder
    {
        private readonly IApplicationContext _applicationContext;
        private readonly ISiteManagerBusiness2 _siteManagerBusiness2;
        private readonly IPriceHelper _priceHelper;
        private readonly IUrlManager _urlManager;
        private readonly ISwitchProvider _switchProvider;
        private readonly IPipeArticleService _pipeArticleService;
        private readonly IArticleService _articleService;

        private readonly string _warrantiesLink;
        private readonly bool _isFnacPro;

        private readonly int _maxServicesToDisplay;

        public CrossSellRichBasketItemChildrenViewModelBuilder(ISiteManagerBusiness2 siteManagerBusiness2,
                                                               IApplicationContext applicationContext,
                                                               IUrlManager urlManager,
                                                               ISwitchProvider switchProvider,
                                                               IPipeArticleService pipeArticleService,
                                                               IArticleService articleService)
        {
            _maxServicesToDisplay = 1;

            var maxServicesToDisplayConfiguration = applicationContext.GetAppSetting("crosssell.services.items.to.display");

            int.TryParse(maxServicesToDisplayConfiguration, out _maxServicesToDisplay);

            _siteManagerBusiness2 = siteManagerBusiness2;
            _applicationContext = applicationContext;
            _priceHelper = _applicationContext.GetPriceHelper();
            _switchProvider = switchProvider;
            _pipeArticleService = pipeArticleService;
            _articleService = articleService;

            _urlManager = urlManager;
            var popWarrantiesPage = urlManager.Sites.WWWDotNetSecure.GetPage("popwarranties");
            if (popWarrantiesPage != null)
            {
                _warrantiesLink = popWarrantiesPage.DefaultUrl.ToString();
            }

            _isFnacPro = applicationContext.GetSiteId() == (int)FnacSites.FNACPRO;
        }

        public IEnumerable<CrossSellRichItemChildrenViewModel> Build(StandardArticle standardArticle)
        {
            return Build(standardArticle, Enumerable.Empty<Base.Model.Promotion>(), false).OfType<CrossSellRichItemChildrenViewModel>();
        }

        public IEnumerable<ChildBasketItemViewModel> Build(StandardArticle standardArticle, IEnumerable<Base.Model.Promotion> globalPromotions, bool applyAdherentPromotion)
        {
            if (standardArticle.Availability == StockAvailability.ClickAndCollectOnly)
            {
                return Enumerable.Empty<ChildBasketItemViewModel>();
            }

            /* Pour une certaine raison (historique ?), on ne doit pas retourner tous les services, mais seulement de la première catégorie récupérée.
             * De ce fait, au lieu d'utiliser _pipeArticleService.GetServices() (malheureusement), nous devons récupérer les catégories et services à notre façon.
             */
            var categoryLinkedArticleList = _pipeArticleService.GetLinkedArticles(standardArticle);
            var firstCategory = GetFirstCategory(categoryLinkedArticleList);

            if (firstCategory?.Articles == null ||
                !firstCategory.Articles.Any())
            {
                return Enumerable.Empty<ChildBasketItemViewModel>();
            }

            var context = new FnacDirect.Contracts.Online.Model.Context()
            {
                SiteContext = _applicationContext.GetSiteContext()
            };

            return (from item in firstCategory.Articles
                    let salesArticle = _siteManagerBusiness2.GetSalesArticles(context, new List<ArticleReference> { item.Reference }, false).FirstOrDefault()
                    where salesArticle != null
                        && salesArticle.Availability != null
                        && salesArticle.Availability.IsAvailable
                    let price = salesArticle.SalesInfo.ReducedPrice.TTC
                    let priceNoVAT = salesArticle.SalesInfo.ReducedPrice.HT
                    select new CrossSellRichItemChildrenViewModel()
                    {
                        QuantityLabel = standardArticle.Quantity != null && standardArticle.Quantity > 1 ? _applicationContext.GetMessage("orderpipe.pop.basket.xsell.quantity", standardArticle.Quantity) : string.Empty,
                        ProductId = item.Reference.PRID.GetValueOrDefault(),
                        Title = item.Core.Title,
                        PriceDetails = _priceHelper.GetDetails(price),
                        Price = _priceHelper.Format(price * standardArticle.Quantity ?? 1),
                        PriceNoVAT = _priceHelper.Format(priceNoVAT * standardArticle.Quantity ?? 1),
                        PriceToDisplay = _isFnacPro ? _priceHelper.Format(priceNoVAT, PriceFormat.CurrencyAsDecimalSeparator) :
                                               _priceHelper.Format(price, PriceFormat.CurrencyAsDecimalSeparator),

                        UnitPrice = _priceHelper.Format(price),
                        UnitPriceNoVAT = _priceHelper.Format(priceNoVAT),

                        UnitPriceToDisplay = _isFnacPro ? _priceHelper.Format(priceNoVAT) : _priceHelper.Format(price),

                        UnitMonthlyPrice = _priceHelper.Format(item.ContractValidity.ToMonthlyPrice(price)),
                        UnitMonthlyPriceNoVAT = _priceHelper.Format(item.ContractValidity.ToMonthlyPrice(priceNoVAT)),

                        UnitMonthlyPriceToDisplay = _isFnacPro ? _priceHelper.Format(item.ContractValidity.ToMonthlyPrice(priceNoVAT)) :
                                                _priceHelper.Format(item.ContractValidity.ToMonthlyPrice(price)),

                        MonthlyPrice = _priceHelper.Format(item.ContractValidity.ToMonthlyPrice(price) * standardArticle.Quantity ?? 1),
                        MonthlyPriceNoVAT = _priceHelper.Format(item.ContractValidity.ToMonthlyPrice(priceNoVAT) * standardArticle.Quantity ?? 1),

                        MonthlyPriceToDisplay = _isFnacPro ? _priceHelper.Format(item.ContractValidity.ToMonthlyPrice(priceNoVAT) * standardArticle.Quantity ?? 1) :
                                               _priceHelper.Format(item.ContractValidity.ToMonthlyPrice(price) * standardArticle.Quantity ?? 1),

                        MoreInfoLink = item.MultimediaLinks != null && item.MultimediaLinks.Any(l => l.DataType == MultimediaHostingDataType.Popin)
                                            ? _urlManager.InitialPageUrl(false).WithPage(item.MultimediaLinks.FirstOrDefault(l => l.DataType == MultimediaHostingDataType.Popin).FullDataURL).ToString() : _warrantiesLink,


                        Type = CrossSellRichItemChildrenViewModelType.Link
                    }).Take(_maxServicesToDisplay).ToList();
        }

        #region privates

        private Category GetFirstCategory(CategoryLinkedArticleList categoryLinkedArticleList)
        {
            if (categoryLinkedArticleList?.Categories == null)
            {
                return null;
            }

            var categories = categoryLinkedArticleList.Categories.Select(c => c);

            // Filtrer les services fly
            categories = categories.Where(c => !HasFlyServices(c));

            var selectedCategory = categories.FirstOrDefault();

            return selectedCategory;
        }

        private bool HasFlyServices(Category category)
        {
            foreach (var article in category.Articles)
            {
                if(_articleService.IsFlyService(article))
                {
                    return true;
                }
            }
            return false;
        }

        #endregion

    }
}
