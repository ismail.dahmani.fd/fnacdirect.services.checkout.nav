using FnacDirect.Ogone.Model;
using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.OrderPipe.Base.Model.SelfCheckout;
using FnacDirect.Technical.Framework.CoreServices;
using FnacDirect.Technical.Framework.CoreServices.Localization;
using FnacDirect.Technical.Framework.Web;
using FnacDirect.Technical.Framework.Web.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Xml.Linq;

namespace FnacDirect.OrderPipe.Base.Proxy.Ogone
{
    //this logic can be moved in ogone.sln ?
    public class OgoneDirectLinkService : IOgoneDirectLinkService
    {
        private readonly IOgoneConfigurationService _ogoneConfigurationService;
        private readonly IApplicationContext _applicationContext;
        private readonly IUIResourceService _uiResourceService;

        public OgoneDirectLinkService(IOgoneConfigurationService ogoneConfigurationService, IApplicationContext applicationContext, IUIResourceService uIResourceService)
        {
            _ogoneConfigurationService = ogoneConfigurationService;
            _applicationContext = applicationContext;
            _uiResourceService = uIResourceService;
        }

        public SerializableDictionary<string, string> CallDirectLink(string url, Dictionary<string,string> dataDictionary, OrderTransactionOgone otDirectLinkData, bool saveAlias, OPContext opContext)
        {
            var encoder = new ASCIIEncoding();
            var urlWithQueryParams = $"{string.Join("&", dataDictionary.Select(kvp => $"{kvp.Key}={kvp.Value}"))}";
            var data = encoder.GetBytes(urlWithQueryParams);
            var httpWebRequest = WebRequest.Create(url);
            var myProxy = new WebProxy();

            var configProxy = System.Configuration.ConfigurationManager.AppSettings["ProxyDirectLink"];
            if (!string.IsNullOrEmpty(configProxy))
            {
                // Create a new Uri object.
                var newUri = new Uri(configProxy);
                // Associate the new Uri object to the myProxy object.
                myProxy.Address = newUri;
                httpWebRequest.Proxy = myProxy;
            }           

            httpWebRequest.ContentLength = data.Length;
            httpWebRequest.ContentType = "application/x-www-form-urlencoded";
            httpWebRequest.Method = "POST";

            //** Appel Direct Link
            httpWebRequest.GetRequestStream().Write(data, 0, data.Length);
            var webResponse2 = httpWebRequest.GetResponse();

            //** Response
            var xelement = XElement.Load(webResponse2.GetResponseStream());
            var responseDirectLink = new SerializableDictionary<string, string>();
            string Status = "", NCERROR = "", NCSTATUS = "";
            foreach (var attribute in xelement.Attributes())
            {
                responseDirectLink.Add(attribute.Name.ToString(), attribute.Value);
                if (attribute.Name.ToString() == "STATUS")
                    Status = attribute.Value;
                else if (attribute.Name.ToString() == "NCERROR")
                    NCERROR = attribute.Value;
                else if (attribute.Name.ToString() == "NCSTATUS")
                    NCSTATUS = attribute.Value;
            }

            // Response 3DS
            if (xelement.Element("HTML_ANSWER") != null)
            {
                var base64HtmlAnswer = xelement.Element("HTML_ANSWER").Value;
                var htmlDecoded = Convert.FromBase64String(base64HtmlAnswer);
                var decodedHtmlAnswer = Encoding.UTF8.GetString(htmlDecoded);
                responseDirectLink.Add("3DSHtmlAnswer", decodedHtmlAnswer);
            }
           
            var authorizeUncertain = PaymentUncertainIsAllowed(opContext);
            dataDictionary.TryGetValue("SHASign", out string shaSign);
            AddToDirectLinkDataDico(responseDirectLink, "OgoneResultDirectLink", TransactionAccepted(Status, NCERROR, NCSTATUS, authorizeUncertain));
            AddToDirectLinkDataDico(responseDirectLink, "CARDNO", otDirectLinkData.CardNo);
            AddToDirectLinkDataDico(responseDirectLink, "CN", otDirectLinkData.Cn);
            AddToDirectLinkDataDico(responseDirectLink, "DIGESTCARDNO", otDirectLinkData.DigestCardNumber);
            AddToDirectLinkDataDico(responseDirectLink, "ED", otDirectLinkData.ExpirationDate);
            AddToDirectLinkDataDico(responseDirectLink, "SaveAlias", saveAlias.ToString());
            AddToDirectLinkDataDico(responseDirectLink, "pipe", opContext.Pipe.PipeName);
            AddToDirectLinkDataDico(responseDirectLink, "CreditCardTypeId", otDirectLinkData.CreditCardTypeId.ToString());

            AddToDirectLinkDataDico(responseDirectLink, "AID", FnacContext.Current.Session.AuthenticationId);
            AddToDirectLinkDataDico(responseDirectLink, "SHASIGN", shaSign);
            AddToDirectLinkDataDico(responseDirectLink, "OrderTemplateTemplateRef", opContext.OF.GetPrivateData<DirectLinkData>().OrderTemplateTemplateRef);
            AddToDirectLinkDataDico(responseDirectLink, "AliasSend", opContext.OF.GetPrivateData<DirectLinkData>().AliasSend);
            AddToDirectLinkDataDico(responseDirectLink, "Scoring", otDirectLinkData.Scoring.ToString());
            AddToDirectLinkDataDico(responseDirectLink, "CGVVersion", otDirectLinkData.CGVVersion.ToString());
            AddToDirectLinkDataDico(responseDirectLink, "CreditCardReference", opContext.OF.GetPrivateData<DirectLinkData>().CreditCardReference);


            // Gestion Erreurs
            if (responseDirectLink["OgoneResultDirectLink"] != OgoneResultEnum.DIRECTLINK_RESULT_ACCEPT
                    && responseDirectLink["OgoneResultDirectLink"] != OgoneResultEnum.DIRECTLINK_RESULT_WAITING3DS) //3DS
            {
                // Récupère le message correspondant au code erreur
                var ncErrorCode = _ogoneConfigurationService.GetDirectLinkNcErrorCodeByKey(NCERROR);

                var parameters = new object[0];
                var userMessageId = string.Empty;

                var ncerrorplus = string.Empty;
                if (responseDirectLink.ContainsKey("NCERRORPLUS"))
                    ncerrorplus = responseDirectLink["NCERRORPLUS"];

                var errorMsgFromUiRessource = string.Empty;
                // Si le code erreur ogone est connu on essai de récupérer le msg uiressource associé à la clé.
                if (ncErrorCode != null)
                {
                    userMessageId = ncErrorCode.explanation;
                    errorMsgFromUiRessource = GetErrorMessage(userMessageId);
                }

                // Si on ogone n'a pas renvoyé d'errorCode ou si le msg correspondant dans DirectLink.config n'est pas une clé d'UIRessource 
                // On init un message par défaut
                if (ncErrorCode == null || string.IsNullOrEmpty(errorMsgFromUiRessource))
                {
                    userMessageId = "OP.CARD.ErrMsg.directlink";
                    parameters = new object[] { ncerrorplus };
                }

                var orderForm = opContext.OF;
                orderForm.AddErrorMessage("Payment.OgoneDirectLinkMessage", userMessageId, parameters);

                Logging.Current.WriteWarning(string.Concat("Ogone DirectLink call error: ", errorMsgFromUiRessource, " / ", userMessageId, " / ", ncerrorplus));
            }

            webResponse2.Close();
            return responseDirectLink;
        }

        public bool PaymentUncertainIsAllowed(OPContext opContext)
        {
            if (opContext.OF.GetPrivateData<SelfCheckoutData>(create: false) != null)
            {
                return false;
            }

            return true;
        }

        private void AddToDirectLinkDataDico(Dictionary<string, string> responseDirectLink, string key, string value)
        {
            if (!responseDirectLink.ContainsKey(key))
                responseDirectLink.Add(key, value);
        }
        private string GetErrorMessage(string messageId)
        {
            var siteContext = _applicationContext.GetSiteContext();

            if (siteContext != null)
            {
                if (_uiResourceService.TryGetUIResourceValue(messageId, siteContext.Culture, out var ressource))
                {
                    return ressource;
                }
            }

            return string.Empty;
        }

        public bool IsUncertainStatus(string status)
        {
            if (status == ((int)OgoneStatusCode.AuthorizationWaiting).ToString()
                || status == ((int)OgoneStatusCode.AuthorizationNotKnown).ToString()
                || status == ((int)OgoneStatusCode.PaymentUncertain).ToString())
            {
                return true;
            }
            return false;
        }


        private string TransactionAccepted(string status, string ncerror, string ncstatus, bool authorizeUncertain)
        {
            // Cas payment authent OK
            if ((status == ((int)OgoneStatusCode.Authorized).ToString() || status == ((int)OgoneStatusCode.PaymentRequested).ToString())
                && ncerror == ((int)OgoneDirectLinkErrorStatusCode.ExecutionOK).ToString()
                && ncstatus == ((int)OgoneDirectLinkErrorStatusCode.ExecutionOK).ToString())
            {
                return OgoneResultEnum.DIRECTLINK_RESULT_ACCEPT;
            }
            // Cas paiement incertain: on fait comme si l'autorisation est OK. => OTY. Si finalement elle ko => le client aura le statut dans son compte
            else if (IsUncertainStatus(status))
            {
                if (authorizeUncertain)
                {
                    return OgoneResultEnum.DIRECTLINK_RESULT_ACCEPT;
                }
                else
                {
                    return OgoneResultEnum.DIRECTLINK_RESULT_EXCEPT;
                }
            }
            // Cas 3D-Secure
            else if (status == ((int)OgoneStatusCode.WaitingClientIdentification).ToString() && ncerror == ((int)OgoneDirectLinkErrorStatusCode.ExecutionOK).ToString())
            {
                return OgoneResultEnum.DIRECTLINK_RESULT_WAITING3DS;
            }
            // Cas Decline
            else if (status == ((int)OgoneStatusCode.AuthorizationRefused).ToString() && ncstatus == ((int)OgoneDirectLinkErrorStatusCode.ExecutionDeclined).ToString())
            {
                return OgoneResultEnum.DIRECTLINK_RESULT_DECLINE;
            }
            // Default = except: invalid, incomplete or status and ncstatus does not correspond
            else
            {
                return OgoneResultEnum.DIRECTLINK_RESULT_EXCEPT;
            }
        }

    }
}
