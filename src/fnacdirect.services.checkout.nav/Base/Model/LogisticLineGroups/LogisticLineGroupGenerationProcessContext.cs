using FnacDirect.OrderPipe.Base.Business.LogisticLineGroups.Internals.Handlers;
using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.OrderPipe.Base.Model.Facets;
using FnacDirect.OrderPipe.Base.Model.LineGroups;
using System;
using System.Collections.Generic;
using System.Linq;

namespace FnacDirect.OrderPipe.Base.Business.LogisticLineGroups
{
    public class LogisticLineGroupGenerationProcessContext : ILogisticLineGroupGenerationProcessContext
    {
        private readonly IPrivateDataContainer _privateDataContainer;

        private readonly IGotShippingMethodEvaluation _iGotShippingMethodEvaluation;
        private readonly IGotBillingInformations _iGotBillingInformations;

        private readonly IDictionary<string, List<Article>> _articlesBackup;
        private readonly IList<ILineGroup> _lineGroups;

        private readonly IList<ILogisticLineGroupProcessCompletedHandlerDescriptor> _logisticLineGroupProcessCompletedHandlers;

        private bool _isDisposed;

        public LogisticLineGroupGenerationProcessContext(MultiFacet<IGotLineGroups, IGotBillingInformations, IGotShippingMethodEvaluation> multiFacet)
        {
            _privateDataContainer = multiFacet;

            _iGotShippingMethodEvaluation = multiFacet.Facet3;
            _iGotBillingInformations = multiFacet.Facet2;

            _articlesBackup = new Dictionary<string, List<Article>>();
            _lineGroups = new List<ILineGroup>();
            _logisticLineGroupProcessCompletedHandlers = new List<ILogisticLineGroupProcessCompletedHandlerDescriptor>();

            foreach (var lineGroup in multiFacet.Facet1.LineGroups)
            {
                var articles = new List<Article>(lineGroup.Articles.ToList());
                _articlesBackup.Add(lineGroup.UniqueId, articles);

                _lineGroups.Add(lineGroup);
            }
        }

        public IEnumerable<ILogisticLineGroupProcessCompletedHandlerDescriptor> ProcessCompletedHandlers
        {
            get
            {
                return _logisticLineGroupProcessCompletedHandlers;
            }
        }

        public IEnumerable<ILineGroup> LineGroups
        {
            get
            {
                return _lineGroups;
            }
        }

        public IPrivateDataContainer PrivateDataContainer
        {
            get
            {
                return _privateDataContainer;
            }
        }

        public IGotShippingMethodEvaluation IGotShippingMethodEvaluation
        {
            get
            {
                return _iGotShippingMethodEvaluation;
            }
        }

        public IGotBillingInformations IGotBillingInformations
        {
            get
            {
                return _iGotBillingInformations;
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public void AddProcessCompletedHandler(ILogisticLineGroupProcessCompletedHandlerDescriptor logisticLineGroupProcessCompletedHandlerDescriptor)
        {
            _logisticLineGroupProcessCompletedHandlers.Add(logisticLineGroupProcessCompletedHandlerDescriptor);
        }

        protected virtual void Dispose(bool isDisposing)
        {
            if (!_isDisposed)
            {
                if (isDisposing)
                {
                    foreach (var lineGroup in _lineGroups)
                    {
                        lineGroup.Articles.Clear();

                        lineGroup.Articles.AddRange(_articlesBackup[lineGroup.UniqueId].ToList());
                    }
                }

                _isDisposed = true;
            }
        }
    }
}
