﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace FnacDirect.OrderPipe
{
    public interface IOrderPipeUrlBuilder
    {
        string GetUrl(string stepName);
        string GetUrl(string stepName, RouteValueDictionary routeValues);
    }
}