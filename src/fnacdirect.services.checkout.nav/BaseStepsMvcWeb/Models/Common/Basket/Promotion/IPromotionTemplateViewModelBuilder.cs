﻿using FnacDirect.Contracts.Online.Model;
using System.Collections.Generic;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Basket
{
    public interface IPromotionTemplateViewModelBuilder
    {
        IEnumerable<PromotionTemplateViewModel> Build(IEnumerable<Promotion> promotions);
        List<RichBasketItemViewModel> CleanDeezerPromotions(List<RichBasketItemViewModel> basketItemViewModelList);
    }
}
