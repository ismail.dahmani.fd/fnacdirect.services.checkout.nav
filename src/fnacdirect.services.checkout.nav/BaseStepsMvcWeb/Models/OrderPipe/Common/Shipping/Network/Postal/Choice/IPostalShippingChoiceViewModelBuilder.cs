﻿using FnacDirect.OrderPipe.Base.BaseModel;
using FnacDirect.OrderPipe.Base.Model.Solex;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Common.Shipping.Postal
{
    public interface IPostalShippingChoiceViewModelBuilder
    {
        PostalShippingChoiceViewModel Builder(ShippingChoice choice, PostalShippingMethodEvaluationItem postalShippingMethodEvaluationItem);
        void CheckVatEligibility(PostalShippingChoiceViewModel postalShippingChoiceViewModel, PostalShippingMethodEvaluationItem postalShippingMethodEvaluationItem, ShippingChoice choice);
        void BuildDelays(PostalShippingChoiceViewModel postalShippingChoiceViewModel, int articlesCount, string label, int currentShippingMethodId, ShippingChoice choice);
    }
}
