using System.Collections.Generic;

namespace FnacDirect.OrderPipe.Base.Business.Shipping
{
    public class AggregatedMethodConfiguration
    {
        public List<int> ExclusiveShippingMethods { get; set; } = new List<int>();
        public List<int> InclusiveShippingMethods { get; set; } = new List<int>();
        public string SwitchId { get; set; } = string.Empty;
    }
}
