using FnacDirect.OrderPipe.Base.Business.PaymentConfiguration;
using FnacDirect.OrderPipe.Base.Model.Facets;
using FnacDirect.OrderPipe.Base.Model.LineGroups;
using System.Collections.Generic;

namespace FnacDirect.OrderPipe.Base.Business.Ogone
{
    public class OgoneBusiness3DSecure : IOgoneBusiness3DSecure
    {
        private readonly IPaymentConfigurationBusiness _paymentConfigurationBusiness;

        public OgoneBusiness3DSecure(IPaymentConfigurationBusiness paymentConfigurationBusiness)
        {
            _paymentConfigurationBusiness = paymentConfigurationBusiness;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="transactionAmount"></param>
        /// <param name="creditCardTypeId"></param>
        /// <param name="iGotUserContextInformations"></param>
        /// <param name="logisticLineGroups"></param>
        /// <param name="shippingMethodEvaluation"></param>
        /// <returns>Flag3D value. null if no scoring param exist</returns>
        public string Get3DSecureInfos(decimal transactionAmount,
                                       int creditCardTypeId,
                                       IGotUserContextInformations iGotUserContextInformations,
                                       IEnumerable<LineGroups.ILogisticLineGroup> logisticLineGroups,
                                       BaseModel.ShippingMethodEvaluation shippingMethodEvaluation,
                                       bool isGuest)
        {
            var userContextInformations = iGotUserContextInformations.UserContextInformations;

            var securityResult = _paymentConfigurationBusiness.GetPaymentSecurityConfiguration(logisticLineGroups, userContextInformations, transactionAmount / 100m, creditCardTypeId);

            if (securityResult == OrderPipe.PaymentConfiguration.Results.SecurityType._3DS)
            {
                return "Y";
            }

            return "N";
        }

        public string Get3DSecureInfos(decimal transactionAmount,
                                       int creditCardTypeId,
                                       IGotUserContextInformations iGotUserContextInformations,
                                       IEnumerable<ILineGroup> lineGroups,
                                       BaseModel.ShippingMethodEvaluation shippingMethodEvaluation,
                                       bool isGuest)
        {
            var userContextInformations = iGotUserContextInformations.UserContextInformations;

            var securityResult = _paymentConfigurationBusiness.GetPaymentSecurityConfiguration(lineGroups, userContextInformations, shippingMethodEvaluation, transactionAmount / 100m, creditCardTypeId);

            if (securityResult == OrderPipe.PaymentConfiguration.Results.SecurityType._3DS)
            {
                return "Y";
            }

            return "N";
        }
    }
}
