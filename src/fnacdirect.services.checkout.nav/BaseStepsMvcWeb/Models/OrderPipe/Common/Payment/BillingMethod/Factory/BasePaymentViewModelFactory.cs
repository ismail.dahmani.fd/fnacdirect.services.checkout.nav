﻿using System;
using FnacDirect.OrderPipe.Base.Model;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Common.Payment.BillingMethodFactories
{
    public abstract class BasePaymentViewModelFactory<TBillingMethod> : IPaymentViewModelFactory<TBillingMethod>
        where TBillingMethod : BillingMethod
    {
        public abstract BillingMethodViewModel Get(string tabName, PaymentMethod paymentMethod, OPContext opContext, PopOrderForm popOrderForm);
    }
}
