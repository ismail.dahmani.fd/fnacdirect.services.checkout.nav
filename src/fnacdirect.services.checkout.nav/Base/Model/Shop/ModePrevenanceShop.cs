using System.Collections.Generic;
using System.Xml.Serialization;

namespace FnacDirect.OrderPipe.Base.BaseModel.Model
{
    public class ModePrevenanceShop
    {
        #region [Private Members]
        private int dbValue;
        private string name;
        private string label;
        private static List<ModePrevenanceShop> modesPrevenanceShop;
        #endregion

        #region [Public Properties]
        public int DbValue
        {
            get { return dbValue; }
            set { dbValue = value; }
        }

        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        [XmlIgnore]
        public string Label
        {
            get { return label; }
            set { label = value; }
        }

        public static List<ModePrevenanceShop> ModesPrevenancesSHOP
        {
            get
            {
                if (modesPrevenanceShop == null)
                {
                    modesPrevenanceShop = new List<ModePrevenanceShop>
                    {
                        Mail,
                        Sms,
                        Phone,
                        None
                    };
                }
                return modesPrevenanceShop;
            }
        }

        public static readonly ModePrevenanceShop Mail = new ModePrevenanceShop(0, "Mail");

        public static readonly ModePrevenanceShop Sms = new ModePrevenanceShop(1, "SMS");

        public static readonly ModePrevenanceShop Phone = new ModePrevenanceShop(0, "Phone", "Appel téléphonique");

      
        public static readonly ModePrevenanceShop None = new ModePrevenanceShop(0, "None", "Aucune prévenance");


        #endregion

        #region [Ctor]

        public ModePrevenanceShop(int dbValue, string name, string label)
        {
            this.dbValue = dbValue;
            this.name = name;
            this.label = label;
        }

        public ModePrevenanceShop(int dbValue, string name)
        {
            this.dbValue = dbValue;
            this.name = label = name;
        }

        public ModePrevenanceShop() { }
        #endregion

        #region [Override operators]
        public override bool Equals(System.Object obj)
        {
            var p = obj as ModePrevenanceShop;
            if ((object)p == null)
            {
                return false;
            }

            return base.Equals(obj) && Name == p.Name;
        }

        public bool Equals(ModePrevenanceShop p)
        {
            return base.Equals((ModePrevenanceShop)p) && Name == p.Name;
        }

        public static bool operator ==(ModePrevenanceShop a, ModePrevenanceShop b)
        {
            if (object.ReferenceEquals(a, b))
                return true;

            if (((object)a == null) || ((object)b == null))
                return false;

            return a.Name == b.Name;
        }

        public static bool operator !=(ModePrevenanceShop a, ModePrevenanceShop b)
        {
            return !(a == b);
        }

        public override int GetHashCode()
        {
            return dbValue.GetHashCode() ^ name.GetHashCode();
        }
        #endregion
    }
}
