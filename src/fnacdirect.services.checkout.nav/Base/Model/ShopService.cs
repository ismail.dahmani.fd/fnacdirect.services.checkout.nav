﻿using FnacDirect.OrderPipe.Base.Model;
using System;
using System.Xml.Serialization;

namespace FnacDirect.OrderPipe.FDV.Model
{
    [Serializable]
    public class ShopService : Service, IComparable<ShopService>
    {
        [XmlIgnore]
        public string ServiceType { get; set; }
        [XmlIgnore]
        public string LibelleType { get; set; }
        [XmlIgnore]
        public bool IsAdhService { get; set; }
        [XmlIgnore]
        public bool IsDefaultService { get; set; }
        [XmlIgnore]
        public bool CanBeSoldAlone { get; set; }
        [XmlIgnore]
        public bool CanModifyQuantity { get; set; }
        [XmlIgnore]
        public bool CanDoRebate { get; set; }
        [XmlIgnore]
        public bool IsS18Service { get; set; }
        [XmlIgnore]
        public decimal MinPrice { get; set; }
        [XmlIgnore]
        public decimal MaxPrice { get; set; }
        [XmlIgnore]
        public decimal MasterArticlePrice { get; private set; }
        [XmlIgnore]
        public bool IsRestrictive { get; private set; }

        public ShopService() { }

        public ShopService(StoreAvailability.Model.FDV.ShopService pInternalService)
        {
            if (pInternalService == null)
                throw new ArgumentNullException("pInternalService");

            ReferenceGU = pInternalService.Sku.ToString();
            ServiceType = pInternalService.ServiceType;
            LibelleType = pInternalService.Libelle;
            IsDefaultService = pInternalService.DefaultPriceMode;
            IsAdhService = pInternalService.AdhPriceMode;
            MinPrice = pInternalService.MinPrice;
            MaxPrice = pInternalService.MaxPrice;
            MasterArticlePrice = pInternalService.MasterArticlePrice;
            IsRestrictive = MinPrice <= MasterArticlePrice && MasterArticlePrice <= MaxPrice;
        } 

        public int CompareTo(ShopService other)
        {
            if (ServiceType == "G10I" && other.ServiceType == "G10I")
                return 0;
            if (ServiceType == "G10I")
                return 1;
            if (other.ServiceType == "G10I")
                return -1;
            return 0;
        }
    }
}
