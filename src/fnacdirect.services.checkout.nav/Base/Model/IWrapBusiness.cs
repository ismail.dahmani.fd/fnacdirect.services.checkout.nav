using FnacDirect.OrderPipe.Base.Model;

namespace FnacDirect.OrderPipe.Base.Business.Wrap
{
    public interface IWrapBusiness
    {
        bool IsEligible(PopOrderForm orderForm);
    }
}
