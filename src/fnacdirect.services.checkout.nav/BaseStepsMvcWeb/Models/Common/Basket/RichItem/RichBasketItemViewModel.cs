using FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Basket.Children;
using FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Common.Basket;
using FnacDirect.Technical.Framework.Web;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Basket
{
    public class RichBasketItemViewModel
    {
        private readonly List<ChildBasketItemViewModel> _children
            = new List<ChildBasketItemViewModel>();

        public IReadOnlyCollection<ChildBasketItemViewModel> Children
        {
            get
            {
                return _children;
            }
        }

        public void WithChildren(params ChildBasketItemViewModel[] childBasketItemViewModels)
        {
            _children.AddRange(childBasketItemViewModels);
        }

        public void RemoveChildrenDigicopy()
        {
            _children.RemoveAll(c => c is DigiCopyChildBasketItemViewModel);
        }

        public RichBasketItemViewModel(BasketItemWithPictureViewModel item)
        {
            Quantity = new QuantityViewModel();
            Image = string.Empty;
            OfferId = Guid.Empty;
            Title = item.Title;
            Image = item.NormalImage;
            IsNumerical = item.IsNumerical;
            IsEbook = item.IsEbook;
            AvailabilityFnacId = item.AvailabilityId;
            TotalOfferCount = item.TotalOfferCount;
            IsBook = item.IsBook;
        }

        public int ReferentialId { get; set; }

        public int ProductId { get; set; }

        public Guid OfferId { get; set; }

        /// <summary>
        /// Basket item's unique identifier within the whole basket (seller id + offer ref + product id)
        /// </summary>
        public string UniqueId { get; set; }

        public int MasterProductId { get; set; }

        public string Title { get; set; }

        public string SubTitle { get; set; }

        public string Image { get; set; }

        public string ProductUrl { get; set; }

        /// <summary>
        /// Commentaires du vendeur (marketplace)
        /// </summary>
        public string Comments { get; set; }

        /// <summary>
        /// Etat du produit (neuf, etc. marketplace)
        /// </summary>
        public string Status { get; set; }

        public AvailabilityViewModel Availability { get; set; }

        /// <summary>
        /// Niveaux de service pris en charge par le vendeur (marketplace)
        /// </summary>
        public string AvailableShippings { get; set; }

        /// <summary>
        /// Est-ce simplement un enfant de type digicopy ?
        /// </summary>
        public bool DigiCopy { get; set; }

        /// <summary>
        /// Type de l'article - utilisé pour conditionné le texte a afficher dans les items enfants du panier (différents types de service)
        /// </summary>
        public int? TypeId { get; set; }

        public PriceViewModel Price { get; set; }

        /// <summary>
        /// La quantité de l'article peut être modifiée
        /// </summary>
        public bool CanUpdate { get; set; }

        /// <summary>
        /// L'article peut être supprimé
        /// </summary>
        public bool CanDelete { get; set; }

        public QuantityViewModel Quantity { get; set; }

        public bool IsNumerical { get; set; }

        public bool IsEbook { get; set; }

        public bool IsBook { get; set; }

        public bool IsAvailable { get; set; }

        public bool IsMembershipRenewCard { get; set; }

        public bool IsMembershipRecruitCard { get; set; }

        public bool IsTryCard { get; set; }
        public bool IsFnacPlusTryCard { get; set; }
        public bool IsFnacPlusCard { get; set; }

        public string MembershipPushFnacCard
        {
            get
            {
                var staticRessourceManager = DependencyResolver.Current.GetService<IStaticResourceManager>();

                if (IsTryCard)
                {
                    return staticRessourceManager.GetImageUrl("~/orderpipe/pop/images/membershippushtrycardfnac.png");

                }
                return staticRessourceManager.GetImageUrl("~/orderpipe/pop/images/membershippushcardfnac.png");
            }
        }

        public string MembershipPushFnacPlusCard
        {
            get
            {
                var staticRessourceManager = DependencyResolver.Current.GetService<IStaticResourceManager>();

                if (IsFnacPlusTryCard)
                {
                    return staticRessourceManager.GetImageUrl("~/orderpipe/pop/images/membershippushtrycardfnacPlus.png");

                }
                return staticRessourceManager.GetImageUrl("~/orderpipe/pop/images/membershippushcardfnacPlus.png");
            }
        }

        public IEnumerable<ServiceChildBasketItemViewModel> Services
        {
            get
            {
                return _children.OfType<ServiceChildBasketItemViewModel>();
            }
        }

        public bool HasCrossSellChildItems { get; set; }

        public ExpressPlusViewModel ExpressPlusInfo { get; set; }

        public bool IsEligibleTo5pctReduction { get; set; }

        public ReducedPriceViewModel ReducedPriceBook5pc { get; set; }

        public bool ShowIfArticleIsFreePruduct { get; set; }

        public bool IsDonation { get; set; }

        public int AvailabilityFnacId { get; set; }

        public int TotalOfferCount { get; set; }

        public bool IsShopShipping { get; set; }

        public bool IsPureMarketPlace { get; set; }
		
        public bool CanBeDelivredBeforChristmas { get; set; }

        public string CanBeDelivredBeforChristmasPicto { get; set; }

        public string CanBeDelivredBeforChristmasWording { get; set; }

        public OptinWarrantyViewModel OptinWarranty { get; set; }

        public bool CanSetAside
        {
            get
            {
                // Propriétés nécessaires pour mettre de côté un article
                return ProductId > 0
                    && ReferentialId > 0;
            }
        }
    }
}
