using System;
using System.Collections.Generic;
using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.Technical.Framework.CoreServices;
using System.Data;
using System.Transactions;
using FnacDirect.Contracts.Online.Model;

namespace FnacDirect.OrderPipe.Base.DAL
{
    public struct ItemInfosResult
    {
        public int? AccountUniverseId;
        public int? DistributorAvailabilityTypeId;
    }

    public class CatalogDAL : ICatalogDAL
    {
        private const string DatabaseSqlCatalog = "SqlCatalog";

        public bool GetRetailerMarketCountry(int countryId, int marketId, int retailerId)
        {
            try
            {
                using (var database = Database.CreateInstance(DatabaseSqlCatalog))
                {
                    using (var command = database.GetStoredProcCommand("spPipe_GetRetailerMarketCountry"))
                    {
                        database.AddOutParameter(command, "ReturnIndex", SqlDbType.Int);
                        database.AddInParameter(command, "RetailerId", SqlDbType.Int, retailerId);
                        database.AddInParameter(command, "MarketId", SqlDbType.Int, marketId);
                        database.AddInParameter(command, "CountryId", SqlDbType.Int, countryId);
                        command.ExecuteNonQuery();
                        return 1 == (int)database.GetParameterValue(command, "ReturnIndex");
                    }
                }
            }
            catch (Exception ex)
            {
                throw DALException.BuildException(ex, DALRange.Catalog, 2);
            }
        }

        public string GetFDV_WEEETaxBySKU(string sku)
        {
            var returnvalue = "";
            try
            {
                using (new TransactionScope(TransactionScopeOption.Suppress))
                {
                    using (var database = Database.CreateInstance(DatabaseSqlCatalog))
                    {
                        using (var command = database.GetStoredProcCommand("spfdv_ShopsGetWEEETaxBySKU"))
                        {
                            database.AddInParameter(command, "sku", SqlDbType.Int, sku);
                            using (var reader = database.ExecuteReader(command))
                            {
                                while (reader.Read())
                                {
                                    returnvalue = DataReaderHelper.GetField<string>(reader, "WEEETaxSku");
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw DALException.BuildException(ex, DALRange.Catalog, 1);
            }
            return returnvalue;
        }

        public VatInfoList GetVATRates(int marketId)
        {
            var returnvalue = new VatInfoList();
            try
            {
                using (new TransactionScope(TransactionScopeOption.Suppress))
                {
                    using (var database = Database.CreateInstance(DatabaseSqlCatalog))
                    {
                        using (var command = database.GetStoredProcCommand("spPipe_GetTVAIntra"))
                        {
                            database.AddInParameter(command, "MarketId", SqlDbType.Int, marketId);
                            using (var reader = database.ExecuteReader(command))
                            {
                                while (reader.Read())
                                {
                                    var vi = new VatInfo
                                    {
                                        CountryCode = DataReaderHelper.GetField<int>(reader, "CountryCode"),
                                        VatCategory = DataReaderHelper.GetField<int>(reader, "VatCategoryId_fk"),
                                        Rate = DataReaderHelper.GetField<decimal>(reader, "VATRate"),
                                        CodeGU = DataReaderHelper.GetField<string>(reader, "CodeGu")
                                    };

                                    returnvalue.Add(vi);
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw DALException.BuildException(ex, DALRange.Catalog, 1);
            }
            return returnvalue;
        }

        public bool ValidateCUUConditions(string artlist, string conditionList, decimal totalPrice, decimal shippingPrice, out decimal basketAmount, int marketId)
        {
            try
            {
                using (var database = Database.CreateInstance(DatabaseSqlCatalog))
                {
                    using (var command = database.GetStoredProcCommand("spPipe_CheckCUUConditions"))
                    {
                        database.AddOutParameter(command, "ReturnIndex", SqlDbType.Int);
                        database.AddOutParameter(command, "prmBasketAmount", SqlDbType.Money);
                        database.AddInParameter(command, "prmszArtIdLst", SqlDbType.VarChar, artlist);
                        database.AddInParameter(command, "prmszConditionLst", SqlDbType.VarChar, conditionList);
                        database.AddInParameter(command, "prmdblTotalPrice", SqlDbType.Money, totalPrice);
                        database.AddInParameter(command, "prmdblShippingPrice", SqlDbType.Money, shippingPrice);
                        database.AddInParameter(command, "MarketId", SqlDbType.Int, marketId);
                        command.ExecuteNonQuery();
                        basketAmount = (decimal)database.GetParameterValue(command, "prmBasketAmount");
                        return 0 == (int)database.GetParameterValue(command, "ReturnIndex");
                    }
                }
            }
            catch (Exception ex)
            {
                throw DALException.BuildException(ex, DALRange.Catalog, 2);
            }
        }

        public void GetNumericalAddOn(StandardArticle art, SiteContext siteContext)
        {
            try
            {
                using (var database = Database.CreateInstance(DatabaseSqlCatalog))
                {
                    using (var command = database.GetStoredProcCommand("spPipe_GetNumerialAddOn"))
                    {
                        database.AddInParameter(command, "ProductId", SqlDbType.Int, art.ProductID.Value);
                        var siteId = (int)siteContext.SiteID;
                        database.AddInParameter(command, "CustomersType", SqlDbType.Int, siteId);
                        database.AddInParameter(command, "MarketId", SqlDbType.Int, siteContext.MarketId);
                        database.AddInParameter(command, "LanguageId", SqlDbType.VarChar, siteContext.Culture);
                        using (var reader = database.ExecuteReader(command))
                        {
                            while (reader.Read())
                            {
                                var na = new NumericalAddOn
                                {
                                    ExternalRef = DataReaderHelper.GetField<string>(reader, "ExternalRef"),
                                    ReferenceFeed4 = DataReaderHelper.GetField<string>(reader, "ReferenceFeed4"),
                                    FormatId = DataReaderHelper.GetField<int?>(reader, "FormatId"),
                                    FormatLabel = DataReaderHelper.GetField<string>(reader, "FormatLabel"),
                                    DistributorId = DataReaderHelper.GetField<int?>(reader, "DistributorId"),
                                    DistributorLabel = DataReaderHelper.GetField<string>(reader, "DistributorLabel"),
                                    EditorId = DataReaderHelper.GetField<int?>(reader, "EditorId"),
                                    EditorLabel = DataReaderHelper.GetField<string>(reader, "EditorLabel"),
                                    Flag = DataReaderHelper.GetField<int?>(reader, "Flag"),
                                    SellingPrice = DataReaderHelper.GetField<decimal?>(reader, "SellingPrice"),
                                    PurchasePrice = DataReaderHelper.GetField<decimal?>(reader, "PurchasePrice"),
                                    InvoicingMode = DataReaderHelper.GetField<int?>(reader, "InvoicingMode"),
                                    SubscriptionMode = DataReaderHelper.GetField<int?>(reader, "SubscriptionMode"),
                                    AccountingFamily = DataReaderHelper.GetField<int?>(reader, "AccountingFamily"),
                                    RetailerId = DataReaderHelper.GetField<int?>(reader, "retailerId"),
                                    RetailerCode = DataReaderHelper.GetField<string>(reader, "SAPRetailerId"),
                                    RetailerLabel = DataReaderHelper.GetField<string>(reader, "RetailerLabel"),
                                    SellingPriceBeforeTax = DataReaderHelper.GetField<decimal?>(reader, "SellingPriceBeforeTax")
                                };
                                art.NumericalAddOn = na;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw DALException.BuildException(ex, DALRange.Catalog, 3);
            }
        }

        public virtual ItemInfosResult GetItemInfos(int productId)
        {
            var res = new ItemInfosResult();
            try
            {
                using (var database = Database.CreateInstance(DatabaseSqlCatalog))
                {
                    using (var command = database.GetStoredProcCommand("spPipe_GetItemInfos"))
                    {
                        command.Parameters.AddWithValue("ProductId", productId);
                        using (var reader = database.ExecuteReader(command))
                        {
                            while (reader.Read())
                            {
                                res.AccountUniverseId = DataReaderHelper.GetField<int?>(reader, "AccountUniverseId");
                                res.DistributorAvailabilityTypeId = DataReaderHelper.GetField<int?>(reader, "DistributorAvailabilityTypeId");
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw DALException.BuildException(ex, DALRange.Catalog, 3);
            }

            return res;
        }       

        public IDictionary<int, Dictionary<int, decimal>> GetVolumesData()
        {
            var result = new Dictionary<int, Dictionary<int, decimal>>();
            try
            {
                using (var database = Database.CreateInstance(DatabaseSqlCatalog))
                {
                    using (var command = database.GetStoredProcCommand("spPipe_GetVolumesData"))
                    {
                        using (var reader = database.ExecuteReader(command))
                        {
                            while (reader.Read())
                            {
                                var typeId = DataReaderHelper.GetField<int>(reader, "TypeId");
                                var supportId = DataReaderHelper.GetField<int>(reader, "SupportId");
                                var volume = DataReaderHelper.GetField<decimal>(reader, "Volume");

                                if (!result.ContainsKey(typeId))
                                    result[typeId] = new Dictionary<int, decimal>();

                                result[typeId][supportId] = volume;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw DALException.BuildException(ex, DALRange.Catalog, 4);
            }

            return result;
        }
    }
}
