﻿using FnacDirect.Technical.Framework.Web.Mvc;
using Microsoft.Extensions.Localization;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
namespace FnacDirect.OrderPipe.Base.Business.Configuration
{
    public class StringLocalizerProvider : IStringLocalizer
    {
        private readonly IApplicationContext applicationContext;
        public StringLocalizerProvider(IApplicationContext applicationContext)
        {
            this.applicationContext = applicationContext;
        }
        public LocalizedString this[string name] => new LocalizedString(name, applicationContext.TryGetMessage(name));
        public LocalizedString this[string name, params object[] arguments] => new LocalizedString(name, applicationContext.TryGetMessage(name, arguments));
        public IEnumerable<LocalizedString> GetAllStrings(bool includeParentCultures)
        {
            return Enumerable.Empty<LocalizedString>();
        }
        public IStringLocalizer WithCulture(CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
