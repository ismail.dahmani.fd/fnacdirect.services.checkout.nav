using FnacDirect.Front.WebBusiness.WebMobile;
using FnacDirect.OrderPipe.Base.Model.Facets;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace FnacDirect.OrderPipe.BaseMvc.Web.HtmlHelpers.Pop
{
    public static class MobileInformations
    {
        public static bool IsMobileApplicationAndroid(this HtmlHelper htmlHelper)
        {
            var opContext = DependencyResolver.Current.GetService<OPContext>();

            if (opContext == null)
            {
                return false;
            }

            var iGotMobileInformations = opContext.OF as IGotMobileInformations;

            if (iGotMobileInformations == null)
            {
                return false;
            }

            var callingPlatform = iGotMobileInformations.OrderMobileInformations.CallingPlatform;

            var result = MobileAppValues.IsAndroidApp(callingPlatform);

            if (!result)
            {
                var queryStrings = htmlHelper.ViewContext.RequestContext.HttpContext.Request.QueryString;

                if (queryStrings["APP"] != null)
                {
                    result = MobileAppValues.IsAndroidApp(queryStrings["APP"]);
                }
            }

            return result;
        }

        public static bool IsMobileApplicationIphone(this HtmlHelper htmlHelper)
        {
            var opContext = DependencyResolver.Current.GetService<OPContext>();

            if (opContext == null)
            {
                return false;
            }

            var iGotMobileInformations = opContext.OF as IGotMobileInformations;

            if (iGotMobileInformations == null)
            {
                return false;
            }

            var callingPlatform = iGotMobileInformations.OrderMobileInformations.CallingPlatform;

            var result = MobileAppValues.IsIphoneApp(callingPlatform);

            if (!result)
            {
                var queryStrings = htmlHelper.ViewContext.RequestContext.HttpContext.Request.QueryString;

                if (queryStrings["APP"] != null)
                {
                    result = MobileAppValues.IsIphoneApp(queryStrings["APP"]);
                }
            }

            return result;
        }

        public static bool IsPipeMobileApplication(this HtmlHelper htmlHelper)
        {
            var opContext = DependencyResolver.Current.GetService<OPContext>();

            if (opContext == null)
            {
                return false;
            }

            var iGotMobileInformations = opContext.OF as IGotMobileInformations;

            if (iGotMobileInformations == null)
            {
                return false;
            }

            var callingPlatform = iGotMobileInformations.OrderMobileInformations.CallingPlatform;

            var result = MobileAppValues.IsIphoneOrIpadApp(callingPlatform) || MobileAppValues.IsAndroidOrTabletApp(callingPlatform);

            if (!result)
            {
                var queryStrings = htmlHelper.ViewContext.RequestContext.HttpContext.Request.QueryString;

                if (queryStrings["APP"] != null)
                {
                    result = MobileAppValues.IsIphoneOrIpadApp(queryStrings["APP"]) || MobileAppValues.IsAndroidOrTabletApp(queryStrings["APP"]);
                }
            }

            return result;
        }

        public static bool IsMobilePhoneApplication(this HtmlHelper htmlHelper)
        {
            var opContext = DependencyResolver.Current.GetService<OPContext>();

            if (opContext == null)
            {
                return false;
            }

            var iGotMobileInformations = opContext.OF as IGotMobileInformations;

            if (iGotMobileInformations == null)
            {
                return false;
            }

            var callingPlatform = iGotMobileInformations.OrderMobileInformations.CallingPlatform;

            var result = MobileAppValues.IsIphoneApp(callingPlatform) || MobileAppValues.IsAndroidApp(callingPlatform);

            if (!result)
            {
                var queryStrings = htmlHelper.ViewContext.RequestContext.HttpContext.Request.QueryString;

                if (queryStrings["APP"] != null)
                {
                    result = MobileAppValues.IsIphoneApp(queryStrings["APP"]) || MobileAppValues.IsAndroidApp(queryStrings["APP"]);
                }
            }

            return result;
        }

        public static bool IsMobileTabletApplication(this HtmlHelper htmlHelper)
        {
            var opContext = DependencyResolver.Current.GetService<OPContext>();

            if (opContext == null)
            {
                return false;
            }

            var iGotMobileInformations = opContext.OF as IGotMobileInformations;

            if (iGotMobileInformations == null)
            {
                return false;
            }

            var callingPlatform = iGotMobileInformations.OrderMobileInformations.CallingPlatform;

            var result = MobileAppValues.IsIpadApp(callingPlatform) || MobileAppValues.IsAndroidTabletApp(callingPlatform);

            if (!result)
            {
                var queryStrings = htmlHelper.ViewContext.RequestContext.HttpContext.Request.QueryString;

                if (queryStrings["APP"] != null)
                {
                    result = MobileAppValues.IsIpadApp(queryStrings["APP"]) || MobileAppValues.IsAndroidTabletApp(queryStrings["APP"]);
                }
            }

            return result;
        }

        public static bool IsMobileTabletIpadApplication(this HtmlHelper htmlHelper)
        {
            var opContext = DependencyResolver.Current.GetService<OPContext>();

            if (opContext == null)
            {
                return false;
            }

            var iGotMobileInformations = opContext.OF as IGotMobileInformations;

            if (iGotMobileInformations == null)
            {
                return false;
            }

            var callingPlatform = iGotMobileInformations.OrderMobileInformations.CallingPlatform;

            var result = MobileAppValues.IsIpadApp(callingPlatform);

            if (!result)
            {
                var queryStrings = htmlHelper.ViewContext.RequestContext.HttpContext.Request.QueryString;

                if (queryStrings["APP"] != null)
                {
                    result = MobileAppValues.IsIpadApp(queryStrings["APP"]);
                }
            }

            return result;
        }

        public static bool IsMobileTabletAndroidApplication(this HtmlHelper htmlHelper)
        {
            var opContext = DependencyResolver.Current.GetService<OPContext>();

            if (opContext == null)
            {
                return false;
            }

            var iGotMobileInformations = opContext.OF as IGotMobileInformations;

            if (iGotMobileInformations == null)
            {
                return false;
            }

            var callingPlatform = iGotMobileInformations.OrderMobileInformations.CallingPlatform;

            var result = MobileAppValues.IsAndroidTabletApp(callingPlatform);

            if (!result)
            {
                var queryStrings = htmlHelper.ViewContext.RequestContext.HttpContext.Request.QueryString;

                if (queryStrings["APP"] != null)
                {
                    result = MobileAppValues.IsAndroidTabletApp(queryStrings["APP"]);
                }
            }

            return result;
        }
    }
}
