namespace FnacDirect.OrderPipe.Base.LineGroups
{
    /// <summary>
    /// La notion de vendeur (/ SellerId) permettait historiquement de différencier la FNAC (SellerId = 0) des vendeurs Market Place (SellerId > 0).
    /// Depuis, cette notion a été détournée pour catégoriser les LineGroups selon leur contenu (SellerId négatif).
    /// </summary>
    public enum SellerType
    {
        /// <summary>Article classique FNAC</summary>
        Fnac = 0,

        /// <summary>Market Place</summary>
        MarketPlace = 1,

        /// <summary>Dématérialisé</summary>
        Numerical = 2,

        /// <summary>Chèque cadeau virtuel</summary>
        VirtualGiftCheck = 3,

        /// <summary>Retrait en magasin</summary>
        Shop = 4,

        /// <summary>Retrait en magasin, dispo en 1h</summary>
        ClickAndCollect = 5,

        /// <summary>Logiciel</summary>
        Software = 6,

        /// <summary>Disponible uniquement en magasin retrait 1h (indispo central)</summary>
        ClickAndCollectOnly = 7,

        /// <summary>Article en flux Rayon</summary>
        Shelf = 8,

        /// <summary>Article en flux Rayon Forcé</summary>
        ShelfForced = 9,

        /// <summary>Article en flux Stock</summary>
        Stock = 10,

        /// <summary>Article en flux Stock Forcé</summary>
        StockForced = 11,

        /// <summary>Article en flux Rayon Défraîchi</summary>
        ShelfFaded = 12,

        /// <summary>Article en flux Stock Défraîchi</summary>
        StockFaded = 13,

        /// <summary>Article en flux Vitrine</summary>
        Vitrine = 14
    }
}
