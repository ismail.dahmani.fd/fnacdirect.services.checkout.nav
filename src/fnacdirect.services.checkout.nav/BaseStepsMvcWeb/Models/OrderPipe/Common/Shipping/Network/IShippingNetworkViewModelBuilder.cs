﻿using FnacDirect.OrderPipe.Base.Model;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Common.Shipping
{
    public interface IShippingNetworkViewModelBuilder<out T>
            where T : ShippingNetworkViewModel
    {
        T Build(PopOrderForm popOrderForm, int sellerId);
    }
}
