using System.Collections.Generic;
using FnacDirect.OrderPipe.Config;

namespace FnacDirect.OrderPipe
{
    public interface IOP
    {
        string PipeName { get; }

        string ConfigKey { get; set; }

        string ControllerName { get; set; }

        string LogonType { get; }

        UIPipeDescriptor UIDescriptor { get; set; }

        IEnumerable<IStep> Steps { get; }

        IStep GetStepByName(string stepName);

        bool DebugMode { get; }

        string SwitchId { get; }

        ParameterList GlobalParameters { get;} 
        IOPConfigLoader OPConfigLoader { get; set; }
        IOFManager OFManager { get; set; }

        void LoadConfiguration();
        void SetupSteps();

        PipeResult Run(OPContext context, string forceistep, bool pass2);
        PipeResult Continue(OPContext context);
        PipeResult Rewind(OPContext context, bool applyshortcut, string forceistep = null);

        void Reset(OPContext opContext);
    }
}
