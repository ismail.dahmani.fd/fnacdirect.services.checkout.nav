﻿using System.Collections.Generic;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Shipping.ShipToStore
{
    public class SearchResultStoreViewModel : StoreViewModel
    {
        public IEnumerable<StoreArticleAvailabilityViewModel> ClickAndCollectAvailabilities { get; set; }

        public IEnumerable<StoreArticleAvailabilityViewModel> DefferedAvailabilities { get; set; }

        public string ClickAndCollectAvailability { get; internal set; }

        public string DefferedAvailability { get; internal set; }
    }
}