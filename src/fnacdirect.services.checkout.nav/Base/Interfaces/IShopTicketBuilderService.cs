using FnacDirect.OrderPipe.Base.LineGroups;
using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.OrderPipe.Base.Model.Facets;
using FnacDirect.OrderPipe.Base.Model.SelfCheckout;
using System.Collections.Generic;

namespace FnacDirect.OrderPipe.Base.Business
{
    public interface IShopTicketBuilderService
    {
        IEnumerable<ShopTicketOrder> GetIntraMagPEOrders(IEnumerable<LogisticLineGroup> logisticLineGroups, IGotUserContextInformations gotUserContextInformations);
        IEnumerable<ShopTicketVATLine> GetVATLines(IEnumerable<StandardArticle> articles);
        ShopTicketCustomerData GetCustomerShopTicket(IGotUserContextInformations gotUserContextInformations, BillingAddress billingAddress);
    }
}
