using Common.Logging;
using FnacDirect.Contracts.Online.IModel;
using FnacDirect.Membership.Configuration;
using FnacDirect.Membership.Model.Constants;
using FnacDirect.OrderPipe.Base.BaseModel.Model;
using FnacDirect.OrderPipe.Base.LineGroups;
using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.OrderPipe.Base.Model.Facets;
using FnacDirect.OrderPipe.Base.Model.LineGroups;
using FnacDirect.OrderPipe.Base.Proxy;
using FnacDirect.OrderPipe.Base.Proxy.Membership;
using FnacDirect.Subscriptions.Business;
using FnacDirect.Technical.Framework.Web.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using DTO = FnacDirect.Contracts.Online.Model;

namespace FnacDirect.OrderPipe.Base.Business
{
    public class FnacPlusCardService : IFnacPlusCardService
    {
        private readonly IMembershipConfigurationService _membershipConfigurationService;
        private readonly ICustomerContextProvider _customerContextProvider;
        private readonly IApplicationContext _applicationContext;
        private readonly ISubscriptionBusiness _subscriptionBusiness;
        private readonly ISiteManagerBusiness2 _siteManagerBusiness2;
        private readonly ILog _log;
        private readonly int _localCountry;
        private readonly PriceBusiness _priceBusiness;

        public FnacPlusCardService(IMembershipConfigurationService membershipConfigurationService,
                                   ICustomerContextProvider customerContextProvider,
                                   IApplicationContext applicationContext,
                                   ISubscriptionBusiness subscriptionBusiness,
                                   ISiteManagerBusiness2 siteManagerBusiness2,
                                   IArticleBusiness3 articleBusiness3,
                                   ILog log)
        {
            _membershipConfigurationService = membershipConfigurationService;
            _customerContextProvider = customerContextProvider;
            _applicationContext = applicationContext;
            _subscriptionBusiness = subscriptionBusiness;
            _siteManagerBusiness2 = siteManagerBusiness2;
            _log = log;

            _localCountry = _applicationContext.GetCountryId();
            _priceBusiness = new PriceBusiness(_applicationContext.GetSiteContext().MarketId);
        }

        public CardInfosData GetRenewCardInfos(MultiFacet<IGotLineGroups, IGotUserContextInformations, IGotBillingInformations, IGotShippingMethodEvaluation, IGotMobileInformations> multiFacetOrderForm, int cardId)
        {
            var renewPrid = _subscriptionBusiness.GetRenewPrid(cardId);
            if (renewPrid.HasValue)
            {
                return GetCardInfosFromPricer(multiFacetOrderForm, renewPrid.Value);
            }

            return null;
        }

        public CardInfosData GetCardInfosFromPricer(MultiFacet<IGotLineGroups, IGotUserContextInformations, IGotBillingInformations, IGotShippingMethodEvaluation, IGotMobileInformations> multiFacetOrderForm, string cardKey)
        {
            var card = _membershipConfigurationService.GetAdherentCardByKey(cardKey);
            return GetCardInfosFromPricer(multiFacetOrderForm, card);
        }

        public CardInfosData GetCardInfosFromPricer(MultiFacet<IGotLineGroups, IGotUserContextInformations, IGotBillingInformations, IGotShippingMethodEvaluation, IGotMobileInformations> multiFacetOrderForm, int cardPrid)
        {
            var card = _membershipConfigurationService.GetAdherentCardArticleByPrid(cardPrid);

            if (card == null)
            {
                _log.Error("Card not found / configured GetCardInfosFromPricer prid " + cardPrid);
                return null;
            }

            return GetCardInfosFromPricer(multiFacetOrderForm, card);
        }

        public DTO.ShoppingCartLineItem GetCardFromPricer(MultiFacet<IGotLineGroups, IGotUserContextInformations, IGotBillingInformations, IGotShippingMethodEvaluation, IGotMobileInformations> multiFacetOrderForm, StandardArticle card, int? refGu = null)
        {
            var articles = new ArticleList
            {
                card
            };
            var replacedArticles = new List<DTO.ShoppingCartLineItem>();
            var currentCustomerContext = _customerContextProvider.GetCurrentCustomerContext();

            //Pour FDV nous avons besoin de connaitre la refGu lors de l'appel au pricer
            var articlePricesParam = GetArticlesPricesParameters.Default;
            if (refGu.HasValue)
                articlePricesParam.ShopRefUg = refGu.Value;

            var shoppingCart = _priceBusiness.GetArticlesPrices(_applicationContext.GetSiteContext(),
                _localCountry,
                currentCustomerContext.GetContractDTO(),
                new MultiFacet<IGotLineGroups, IGotMobileInformations, IGotBillingInformations, IGotShippingMethodEvaluation>(multiFacetOrderForm.Facet1, multiFacetOrderForm.Facet5, multiFacetOrderForm.Facet3, multiFacetOrderForm.Facet4),
                articles,
                replacedArticles,
                articlePricesParam,
                useCache: true);

            var resultingCard = shoppingCart.LineItems.FirstOrDefault();
            return resultingCard;
        }

        public CardInfosData GetCardInfosFromPricer(MultiFacet<IGotLineGroups, IGotUserContextInformations, IGotBillingInformations, IGotShippingMethodEvaluation, IGotMobileInformations> multiFacetOrderForm, AdherentCardArticle card)
        {
            var prid = card.Prid;
            var typeId = (int)card.CategoryCardType;

            var result = new CardInfosData
            {
                Prid = prid,
                TypeId = typeId
            };

            var cardArticle = new StandardArticle
            {
                ProductID = prid,
                Type = ArticleType.Saleable,
                TypeId = typeId,
                Quantity = 1
            };

            var resultingCard = GetCardFromPricer(multiFacetOrderForm, cardArticle);

            if (resultingCard != null && resultingCard.SalesInfo != null)
            {
                // Get new PRID from Pricer result
                result.Prid = resultingCard.Reference.PRID.Value;

                // Calculate OPC from Pricer results
                result.HasOPC = resultingCard.SalesInfo.StandardPrice.TTC > resultingCard.SalesInfo.ReducedPrice.TTC;
                result.StandardPrice = resultingCard.SalesInfo.StandardPrice.TTC;
                result.PriceWithOffer = resultingCard.SalesInfo.ReducedPrice.TTC;

                // Get duration and typeId from New Ref
                var context = new DTO.Context()
                {
                    SiteContext = _applicationContext.GetSiteContext()
                };

                var articleReference = new DTO.ArticleReference(resultingCard.Reference.PRID.Value, null, null, null, DTO.ArticleCatalog.FnacDirect);

                var item = _siteManagerBusiness2.GetArticle(context, articleReference);

                if (item == null)
                {
                    return result;
                    //return null;
                }

                result.TypeId = item.Core.Type.ID;
                result.ValidityType = GetValidityTypeString(item.ContractValidity.Type, item.ContractValidity.Value);
                result.ValidityDuration = item.ContractValidity.Value;
            }

            return result;
        }

        private string GetValidityTypeString(DTO.ContractValidityType type, int duration)
        {
            var suffix = duration > 1 ? "s" : string.Empty;
            var id = "orderpipe.pop.membership.contractvaliditytype." + type.ToString().ToLowerInvariant() + suffix;

            try
            {
                return MessageHelper.Get(_applicationContext.GetSiteContext(), id, null);
            }
            catch (Exception ex)
            {
                _log.Error("uiRessource " + id, ex);
            }

            return string.Empty;
        }

        public bool HasManyFnacPlusCardFromBasket(IEnumerable<ILineGroup> lineGroups)
        {
            return lineGroups.GetArticles().OfType<StandardArticle>().Count(a =>
                _membershipConfigurationService.FnacPlusCardTypes.Contains(a.TypeId.GetValueOrDefault())) > 1;
        }

        public StandardArticle GetFnacPlusCardFromBasket(IEnumerable<ILineGroup> lineGroups)
        {
            return lineGroups.GetArticles().OfType<StandardArticle>().FirstOrDefault(a =>
                _membershipConfigurationService.FnacPlusCardTypes.Contains(a.TypeId.GetValueOrDefault()));
        }

        public StandardArticle GetFnacPlusTryCardFromBasket(IEnumerable<ILineGroup> lineGroups)
        {
            return lineGroups.GetArticles().OfType<StandardArticle>().FirstOrDefault(a =>
                _membershipConfigurationService.FnacPlusTryCardType == a.TypeId.GetValueOrDefault());
        }

        public StandardArticle GetAdhCardFromBasket(IEnumerable<ILineGroup> lineGroups)
        {
            return lineGroups.GetArticles().OfType<StandardArticle>().FirstOrDefault(a =>
                a.ProductID.GetValueOrDefault() == _membershipConfigurationService.GetTryCardPrid()
                || a.TypeId.GetValueOrDefault() == (int)CategoryCardType.Recruit
                || a.TypeId.GetValueOrDefault() == (int)CategoryCardType.Renew);
        }

        public StandardArticle GetAdhTryCardFromBasket(IEnumerable<ILineGroup> lineGroups)
        {
            return lineGroups.GetArticles().OfType<StandardArticle>().FirstOrDefault(a =>
                a.ProductID.GetValueOrDefault() == _membershipConfigurationService.GetTryCardPrid());
        }

        public bool FnacPlusEnabled
        {
            get
            {
                return _membershipConfigurationService.IsFnacPlusEnabled;
            }
        }

        public bool FnacPlusCanalPlayEnabled
        {
            get
            {
                return _membershipConfigurationService.IsFnacPlusCanalPlayEnabled;
            }
        }

        public bool HasManyFnacPlusCardFromBasket(IEnumerable<ILogisticLineGroup> lineGroups)
        {
            return lineGroups.SelectMany(l => l.Articles.OfType<StandardArticle>()).Count(a =>
                  _membershipConfigurationService.FnacPlusCardTypes.Contains(a.TypeId.GetValueOrDefault())) > 1;
        }

        public StandardArticle GetFnacPlusCardFromBasket(IEnumerable<ILogisticLineGroup> lineGroups)
        {
            return lineGroups.SelectMany(l => l.Articles.OfType<StandardArticle>()).FirstOrDefault(a =>
                _membershipConfigurationService.FnacPlusCardTypes.Contains(a.TypeId.GetValueOrDefault()));
        }

        public StandardArticle GetFnacPlusTryCardFromBasket(IEnumerable<ILogisticLineGroup> lineGroups)
        {
            return lineGroups.SelectMany(l => l.Articles.OfType<StandardArticle>()).FirstOrDefault(a =>
                _membershipConfigurationService.FnacPlusTryCardType == a.TypeId.GetValueOrDefault());
        }

        public StandardArticle GetAdhCardFromBasket(IEnumerable<ILogisticLineGroup> lineGroups)
        {
            return lineGroups.SelectMany(l => l.Articles.OfType<StandardArticle>()).FirstOrDefault(a =>
                a.ProductID.GetValueOrDefault() == _membershipConfigurationService.GetTryCardPrid()
                || a.TypeId.GetValueOrDefault() == (int)CategoryCardType.Recruit
                || a.TypeId.GetValueOrDefault() == (int)CategoryCardType.Renew);
        }

        public StandardArticle GetAdhTryCardFromBasket(IEnumerable<ILogisticLineGroup> lineGroups)
        {
            return lineGroups.SelectMany(l => l.Articles.OfType<StandardArticle>()).FirstOrDefault(a =>
                a.ProductID.GetValueOrDefault() == _membershipConfigurationService.GetTryCardPrid());
        }
    }

}
