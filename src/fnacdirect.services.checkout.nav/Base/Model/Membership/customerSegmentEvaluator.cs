using FnacDirect.Context;
using FnacDirect.Contracts.Online.Model;
using FnacDirect.Membership.Model.Constants;
using FnacDirect.Membership.Model.Entities;
using FnacDirect.OrderPipe.Base.BaseModel.Model;
using FnacDirect.OrderPipe.Base.Proxy.Membership;
using FnacDirect.Technical.Framework.Web.Mvc;
using System;
using System.Linq;

namespace FnacDirect.OrderPipe.Base.Business.Membership
{
    public class CustomerSegmentEvaluator
    {
        private readonly IUserContextProvider _userContextProvider;
        private readonly IUserExtendedContextProvider _userExtendedContextProvider;
        private readonly IPrivateDataContainer _orderFormPrivateDataContainer;
        private readonly IMembershipConfigurationService _membershipConfigurationService;

        public CustomerSegmentEvaluator(IUserContextProvider userContextProvider,
            IUserExtendedContextProvider userExtendedContextProvider,
            IMembershipConfigurationService membershipConfigurationService,
            IPrivateDataContainer orderFormPrivateDataContainer)
        {
            _userContextProvider = userContextProvider;
            _userExtendedContextProvider = userExtendedContextProvider;
            _orderFormPrivateDataContainer = orderFormPrivateDataContainer;
            _membershipConfigurationService = membershipConfigurationService;
        }

        private bool IsIdentified => _userContextProvider.GetCurrentUserContext().IsIdentified;
        public bool IsAdherent => _userExtendedContextProvider.GetCurrentUserExtendedContext().Customer.IsValidAdherent;

        public bool EvaluateSegments(params CustomerSegmentQuery[] segmentQueries)
        {
            var customerSegments = _userExtendedContextProvider.GetCurrentUserExtendedContext().Customer.ContractDTO.Segments;
            var result = true;

            foreach (var segmentQuery in segmentQueries)
            {
                result = segmentQuery.Evaluation
                    ? customerSegments.Contains(segmentQuery.Segment)
                    : !customerSegments.Contains(segmentQuery.Segment);
                if (!result)
                    return result;
            }

            return result;
        }

        public bool IsEligiblePushRecruit()
        {
            if (IsAdherent)
            {
                if (EvaluateSegments(
                    new CustomerSegmentQuery { Segment = CustomerSegment.ExpressPlus, Evaluation = false },
                    new CustomerSegmentQuery { Segment = CustomerSegment.EligibleEssaiExpressPlus, Evaluation = true },
                    new CustomerSegmentQuery { Segment = CustomerSegment.EssaiPlus, Evaluation = false }))
                    return true;

                if (EvaluateSegments(
                    new CustomerSegmentQuery { Segment = CustomerSegment.ExpressPlus, Evaluation = false },
                    new CustomerSegmentQuery { Segment = CustomerSegment.EligibleEssaiExpressPlus, Evaluation = false },
                    new CustomerSegmentQuery { Segment = CustomerSegment.EssaiPlus, Evaluation = false }))
                    return true;

                //if (IsEligiblePushRenouvAdherent())
                //{
                //    if (EvaluateSegments(
                //        new CustomerSegmentQuery { Segment = CustomerSegment.ExpressPlus, Evaluation = false },
                //        new CustomerSegmentQuery { Segment = CustomerSegment.EligibleEssaiExpressPlus, Evaluation = true },
                //        new CustomerSegmentQuery { Segment = CustomerSegment.EssaiPlus, Evaluation = false }))
                //        return true;

                //    if (EvaluateSegments(
                //        new CustomerSegmentQuery { Segment = CustomerSegment.ExpressPlus, Evaluation = false },
                //        new CustomerSegmentQuery { Segment = CustomerSegment.EligibleEssaiExpressPlus, Evaluation = false },
                //        new CustomerSegmentQuery { Segment = CustomerSegment.EssaiPlus, Evaluation = false }))
                //        return true;
                //}
            }
            else
            {
                if (EvaluateSegments(
                    new CustomerSegmentQuery { Segment = CustomerSegment.ExpressPlus, Evaluation = false },
                    new CustomerSegmentQuery { Segment = CustomerSegment.EligibleEssaiExpressPlus, Evaluation = true }))
                    return true;

                if (EvaluateSegments(
                    new CustomerSegmentQuery { Segment = CustomerSegment.ExpressPlus, Evaluation = false },
                    new CustomerSegmentQuery { Segment = CustomerSegment.EligibleEssaiExpressPlus, Evaluation = false }))
                    return true;
            }

            return false;
        }

        public bool IsEligiblePushRenew()
        {
            if (IsAdherent)
            {
                if (EvaluateSegments(
                    new CustomerSegmentQuery { Segment = CustomerSegment.ExpressPlus, Evaluation = true },
                    new CustomerSegmentQuery { Segment = CustomerSegment.EligibleEssaiExpressPlus, Evaluation = false },
                    new CustomerSegmentQuery { Segment = CustomerSegment.PushRenouvellementExpressPlus, Evaluation = true }))
                    return true;

                if (EvaluateSegments(
                    new CustomerSegmentQuery { Segment = CustomerSegment.EssaiPlus, Evaluation = true },
                    new CustomerSegmentQuery { Segment = CustomerSegment.EligibleEssaiExpressPlus, Evaluation = false },
                    new CustomerSegmentQuery { Segment = CustomerSegment.PushRenouvellementExpressPlus, Evaluation = true }))
                    return true;
            }

            if (EvaluateSegments(new CustomerSegmentQuery { Segment = CustomerSegment.ExpressPlusNonRenouvele, Evaluation = true }))
                return true;

            return false;
        }

        public bool IsEligiblePushTrial()
        {
            if (IsAdherent)
            {
                if (EvaluateSegments(
                    new CustomerSegmentQuery { Segment = CustomerSegment.ExpressPlus, Evaluation = false },
                    new CustomerSegmentQuery { Segment = CustomerSegment.EssaiPlus, Evaluation = false },
                    new CustomerSegmentQuery { Segment = CustomerSegment.EligibleEssaiExpressPlus, Evaluation = true }))
                    return true;

                //if (EvaluateSegments(
                //    new CustomerSegmentQuery { Segment = CustomerSegment.ExpressPlus, Evaluation = false },
                //    new CustomerSegmentQuery { Segment = CustomerSegment.EssaiPlus, Evaluation = false },
                //    new CustomerSegmentQuery { Segment = CustomerSegment.EligibleEssaiExpressPlus, Evaluation = true },
                //    new CustomerSegmentQuery { Segment = CustomerSegment.PushRenouvellementExpressPlus, Evaluation = true }))
                //    return true;
            }
            else
            {
                if (EvaluateSegments(
                    new CustomerSegmentQuery { Segment = CustomerSegment.ExpressPlus, Evaluation = false },
                    new CustomerSegmentQuery { Segment = CustomerSegment.EligibleEssaiExpressPlus, Evaluation = true }))
                    return true;
            }

            return false;
        }

        #region private methods

        private bool IsEligiblePushRenouvAdherent()
        {
            var pushFnacPlusAdherentData = _orderFormPrivateDataContainer.GetPrivateData<PushFnacPlusAdherentData>(create: false);

            return HasEligibleCardForRenew() &&
                (GetMembershipAboutToExpireStatus() || GetObsoleteMembershipStatus()) &&
                !pushFnacPlusAdherentData.HasAdhCardInBasket;
        }

        private bool GetMembershipAboutToExpireStatus()
        {
            var userExtendedContext = _userExtendedContextProvider.GetCurrentUserExtendedContext();

            if (userExtendedContext.Customer?.MembershipContract != null)
            {
                return (userExtendedContext.Customer.MembershipContract.AdhesionEndDate >= DateTime.Now
                && userExtendedContext.Customer.MembershipContract.AdhesionEndDate.AddDays(_membershipConfigurationService.DelayBeforeExpiration) <= DateTime.Now);
            }

            return false;
        }

        private bool GetObsoleteMembershipStatus()
        {
            var userExtendedContext = _userExtendedContextProvider.GetCurrentUserExtendedContext();

            if (userExtendedContext.Customer?.MembershipContract != null)
            {
                return (userExtendedContext.Customer.MembershipContract.AdhesionEndDate <= DateTime.Now
                && userExtendedContext.Customer.MembershipContract.AdhesionEndDate.AddDays(_membershipConfigurationService.DelayAfterExpiration) >= DateTime.Now);
            }
            return false;
        }

        private bool HasEligibleCardForRenew()
        {
            var contract = GetCurrentRenewContract();
            return contract?.EligibilityRenewCards != null &&
                   contract.EligibilityRenewCards.Any(c =>
                       c.ProgramDuration == ProgramType.ThreeYears ||
                       c.ProgramDuration == ProgramType.Amex ||
                       c.ProgramDuration == ProgramType.OneAmex);
        }

        private RenewContract GetCurrentRenewContract()
        {
            var userExtendedContext = _userExtendedContextProvider.GetCurrentUserExtendedContext();
            if (userExtendedContext.Customer != null &&
                userExtendedContext.Customer.IsAdherentRenewable &&
                userExtendedContext.Customer.MembershipContract?.MemberCard?.HolderType == CardHolderType.Titular)
            {
                return userExtendedContext.Customer.RenewContract;
            }

            return null;
        }

        private bool IsEligibleEssaiFnacPlus()
        {
            if (!IsIdentified)
                return true;

            var pushFnacPlusAdherentData = _orderFormPrivateDataContainer.GetPrivateData<PushFnacPlusAdherentData>(create: false);

            return pushFnacPlusAdherentData.CardPlusInfos != null && (
                pushFnacPlusAdherentData.CardPlusInfos.TypeId == (int)CategoryCardType.ConversionFnacPlus ||
                pushFnacPlusAdherentData.CardPlusInfos.TypeId == (int)CategoryCardType.SouscriptionFnacPlusTrial ||
                pushFnacPlusAdherentData.CardPlusInfos.TypeId == (int)CategoryCardType.SouscriptionFnacPlusTrialDecouverte);
        }
        #endregion
    }

    public struct CustomerSegmentQuery
    {
        public CustomerSegment Segment;
        public bool Evaluation;
    }
}
