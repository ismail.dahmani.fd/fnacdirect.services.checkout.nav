﻿using System.Linq;
using System.Collections.Generic;
using FnacDirect.OrderPipe.Base.LineGroups;
using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.OrderPipe.Base.Model.LineGroups;
using FnacDirect.OrderPipe.Base.Business.LogisticLineGroups.Internals.InitialSelectors;

namespace FnacDirect.OrderPipe.Base.Business.LogisticLineGroups.InitialSelectors
{
    public class SupplySourceLineGroupInitialSelector : BaseLineGroupInitialSelector
    {
        public override IDictionary<ILineGroupHeader, IEnumerable<Article>> Select(IEnumerable<ILineGroup> lineGroups)
        {
            var output = new Dictionary<ILineGroupHeader, IEnumerable<Article>>();

            foreach(var lineGroup in lineGroups.Where(l => l.HasFnacComArticle))
            {
                var articles = lineGroup.Articles.OfType<StandardArticle>().Where(a => a.SupplySource != null);

                if(articles.Any())
                {
                    output.Add(lineGroup, articles);
                }
            }

            return output;
        }
    }
}
