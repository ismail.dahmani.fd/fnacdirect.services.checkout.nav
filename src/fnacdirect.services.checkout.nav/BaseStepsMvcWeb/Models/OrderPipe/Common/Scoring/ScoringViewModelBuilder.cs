using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.OrderPipe.Common.Scoring
{
    public class ScoringViewModelBuilder : IScoringViewModelBuilder
    {
        private readonly ISwitchProvider _switchProvider;
        public ScoringViewModelBuilder(ISwitchProvider switchProvider)
        {
            _switchProvider = switchProvider;
        }

        public ScoringViewModel Build()
        {
            var model = new ScoringViewModel()
            {
                IsLegacy = !_switchProvider.IsEnabled("orderpipe.pop.scoring.useinauth")
            };

            return model;
        }
    }
}
