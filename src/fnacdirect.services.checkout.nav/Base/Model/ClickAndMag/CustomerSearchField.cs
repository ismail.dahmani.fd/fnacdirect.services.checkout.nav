namespace FnacDirect.OrderPipe.CM.Model
{
    public enum CustomerSearchField
    {
        Undefined = 0,
        MembershipNumber,
        LastName,
        FirstName,
        EmailAddress,
        PostalCode
    }
}
