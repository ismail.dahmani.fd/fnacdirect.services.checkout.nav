﻿namespace FnacDirect.OrderPipe.Base.BaseModel.Model.OrderDetails
{
    public class OrderDetailRelayAddress : OrderDetailBaseAddress
    {
        public OrderDetailRelayAddress()
        {
            Type = "RelayAddress";
        }

        public string Name { get; set; }

        public string State { get; set; }
    }
}
