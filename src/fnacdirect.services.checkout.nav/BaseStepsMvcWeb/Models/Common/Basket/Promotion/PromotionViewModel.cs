﻿
namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Basket
{
    public class PromotionViewModel
    {
        public FnacDirect.Contracts.Online.Model.PromotionUsage Usage { get; set; }

        public string Push { get; set; }        
    }
}
