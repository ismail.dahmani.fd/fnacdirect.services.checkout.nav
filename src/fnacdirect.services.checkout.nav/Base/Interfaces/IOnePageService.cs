using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.OrderPipe.Base.Model.Pop;
using System.Collections.Generic;

namespace FnacDirect.OrderPipe.Base.Business.OnePage
{
    public interface IOnePageService
    {
        bool IsEligible(PopOrderForm popOrderForm, IEnumerable<int> excludedLogisticTypes, bool reactMode);

        AppliablePath GetPath(PopOrderForm popOrderForm, PopData popData, int siteId);
    }
}
