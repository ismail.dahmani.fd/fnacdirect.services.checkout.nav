using FnacDirect.OrderPipe.Base.Model.FDV.Rebate;
using System.Collections.Generic;

namespace FnacDirect.OrderPipe.Base.Business
{
    public interface IRemoteRebateBusiness
    {
        int AddRemoteRebate(RemoteRebate remoteRebate);

        RemoteRebate LoadRemoteRebate(int rebateId);

        IEnumerable<RemoteRebate> LoadRemoteRebates();

        RemoteRebateStatus UpdateRemoteRebate(int remoteRebateId, RemoteRebateStatus rebateStatus, string respMatricule, string respComment);
    }
}
