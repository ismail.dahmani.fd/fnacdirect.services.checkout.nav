using FnacDarty.FnacCom.DataLayer.Interfaces;
using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.DataLayer;
using FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Omniture;
using FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.TagCommander;
using System.Web.Mvc;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Services.Pop.DataLayer
{
    public class DataLayerService : IDataLayerService
    {
        private readonly ICeddlModelBuilder _ceddlModelBuilder;
        private readonly ISwitchProvider _switchProvider;
        private readonly IDataLayerBuilder _dataLayerBuilder;

        public DataLayerService(ICeddlModelBuilder ceddlModelBuilder, ISwitchProvider switchProvider, IDataLayerBuilder dataLayerBuilder)
        {
            _ceddlModelBuilder = ceddlModelBuilder;
            _switchProvider = switchProvider;
            _dataLayerBuilder = dataLayerBuilder;
        }

        public void ComputeShoppingCart(DatalayerViewModel model, ViewResultBase viewResultBase, ControllerContext controllerContext)
        {
            if (!_switchProvider.IsEnabled("datalayer.fnac.enabled"))
                return;

            var pageRequest = _dataLayerBuilder.BuildPageRequest(model, viewResultBase, controllerContext);
            var userRequest = _dataLayerBuilder.BuildUserRequest();
            var cartRequest = _dataLayerBuilder.BuildCartRequest(model.PopOrderForm);

            _ceddlModelBuilder
                            .WithPagePart(pageRequest)
                            .WithUsersPart(userRequest)
                            .WithCartPart(cartRequest);
        }

        public void ComputeTransaction(DatalayerViewModel model, ViewResultBase viewResultBase, ControllerContext controllerContext)
        {
            if (!_switchProvider.IsEnabled("datalayer.fnac.enabled"))
                return;

            var pageRequest = _dataLayerBuilder.BuildPageRequest(model, viewResultBase, controllerContext);
            var userRequest = _dataLayerBuilder.BuildUserRequest();
            var transactionRequest = _dataLayerBuilder.BuildTransactionRequest(model);
            var orderRequests = _dataLayerBuilder.BuildOrderRequests(model);

            _ceddlModelBuilder
                            .WithPagePart(pageRequest)
                            .WithUsersPart(userRequest)
                            .WithTransactionPart(transactionRequest)
                            .WithOrderPart(orderRequests);
        }
    }
}
