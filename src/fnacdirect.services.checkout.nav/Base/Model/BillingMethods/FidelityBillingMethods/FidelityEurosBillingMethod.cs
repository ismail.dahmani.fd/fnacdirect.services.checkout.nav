using System;
using System.Xml.Serialization;

namespace FnacDirect.OrderPipe.Base.Model
{
    public class FidelityEurosBillingMethod : FidelityBillingMethod
    {
        public static int IdBillingMethod
        {
            get { return 16777216; }
        }

        public FidelityEurosBillingMethod()
        {
            BillingMethodId = IdBillingMethod;
            Priority = 10;
            CanModifyAmountDown = false;
            CanModifyAmountUp = false;
            AdvantageCodeType = Membership.Model.Constants.AdvantageCodeType.SIEBEL_EUROS;
        }
    }
}