namespace FnacDirect.OrderPipe.Base.Model.OrderInformations
{
    public interface IOrderContextInformations
    {
        string Affiliate { get; set; }

        string Catalogue { get; set; }

        string Culture { get; set; }

        string IPAddress { get; set; }

        bool IsTransfertToShoppingCartEnabled { get; set; }

        bool OrderInSession { get; set; }

        string Site { get; set; }

        string WebFarm { get; set; }

        int? MasterOrder { get; set; }

        bool IsOrderWithAuthentication { get; set; }
        string SenderEmail { get; set; }
    }
}
