using FnacDirect.OrderPipe.Base.Model.Solex;
using System.Collections.Generic;

namespace FnacDirect.OrderPipe.Base.Business.Shipping
{
    public interface IShippingChoiceAggregationService
    {
        List<ShippingChoice> AggregateShippingChoices(IEnumerable<ShippingChoice> choices);
    }
}
