using FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Membership;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Common.Basket
{
    public interface IGotFnacPlusData
    {
        bool IsFnacPlusEnabled { get; set; }

        bool IsFnacPlusCanalPlayEnabled { get; set; }

        bool IsEligibleEssaiFnacPlus { get; set; }

        bool IsEligiblePushRecruit { get; set; }

        bool IsEligiblePushRenouvAdh { get; set; }

        bool IsEligiblePushRenouvFnacPlus { get; set; }

        bool IsEligiblePushConfirm { get; set; }

        bool HasAdherentCardInBasket { get; set; }

        bool HasFnacPlusCardInBasket { get; set; }

        MembershipPushCardViewModel CardPlusInfos { get; set; }

        string FnacPlusImmediateDiscount { get; set; }

        string FnacPlusDifferedDiscount { get; set; }

        bool HasFnacPlusDiscount { get; set; }

        bool HasFnacPlusImmediateDiscount { get; set; }

        bool HasFnacPlusDifferedDiscount { get; set; }
    }
}
