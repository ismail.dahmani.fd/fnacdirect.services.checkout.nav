using FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.OnePage.Omniture;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Basket
{
    public class OmnitureBasketViewModel : OmnitureCommonViewModel
    {
        public string Channel { get; set; }
        public string Server { get; set; }
        public string StatusMembership { get; set; }
        public string UID { get; set; }       
        public string PipeType { get; set; }
        public string PlateformeType { get; set; }
        public string StatusIdentification { get; set; }
        public string SubscriptionType { get; set; }
        public string StatusLoggued { get; set; }
        public string BasketSource { get; set; }
        public string BasketMix { get; set; }
        public string BasketAmount { get; set; }
    }
}
