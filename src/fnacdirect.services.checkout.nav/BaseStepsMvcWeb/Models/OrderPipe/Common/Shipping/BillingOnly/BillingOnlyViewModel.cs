namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Common.Shipping
{
    public class BillingOnlyViewModel
    {
        public bool HasEbook { get; set; }

        public bool HasDematSoft { get; set; }

        public bool HasAdherentCard { get; set; }

        public bool HasExpressPlus { get; set; }

        public bool HasService { get; set; }

        public bool HasAppointment { get; set; }

        public bool HasECCV { get; set; }

        public bool HasDonation { get; set; }

        public bool HasGamingSubscription { get; set; }
    }
}
