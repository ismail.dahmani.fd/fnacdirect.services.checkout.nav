using System.Collections.Generic;
using FnacDirect.OrderPipe.Base.Model.TaxCalculation;
using tax = FnacDirect.OrderPipe.TaxCalculation.Front;
using TaxModel = FnacDirect.OrderPipe.TaxCalculation.Front.Models;
using System.Linq;
using FnacDirect.OrderPipe.Base.Model;
using System;

namespace FnacDirect.OrderPipe.Base.Proxy.TaxCalculation
{
    public class TaxCalculationService : ITaxCalculationService
    {
        private readonly tax.ITaxCalculationService _taxCalculationService;

        public TaxCalculationService(tax.ITaxCalculationService taxCalculationService)
        {
            _taxCalculationService = taxCalculationService;
        }

        public List<TaxCalculationResultItem> GetExtendedPriceForArticles(OrderPipeItemsQuery orderPipeItemsQuery)
        {
            var itemsQuery = orderPipeItemsQuery.ToItemsQuery();

            var items = _taxCalculationService.GetExtendedPriceForArticles(itemsQuery);

            var orderPipeItems = MappingTaxCalculationResultItem(items);

            return orderPipeItems;
        }

        public TaxCalcuationLineGroupResult SetLineGroupsAllPricesAndTaxInfos(OrderPipeLineGroupQuery orderPipeLineGroupQuery)
        {
            var lineGroupQuery = orderPipeLineGroupQuery.ToLineGroupQuery();

            var lineGroupResult = _taxCalculationService.SetLineGroupsAllPricesAndTaxInfos(lineGroupQuery);

            var taxCalcuationLineGroupResult = MappingToTaxCalculationResultItem(lineGroupResult);

            return taxCalcuationLineGroupResult;
        }

        public TaxCalculationShippingChoicesResult SetShippingChoicesPrices(OrderPipeShippingChoicesQuery orderPipeShippingChoicesQuery)
        {
            var shippingChoiceQuery = orderPipeShippingChoicesQuery.ToShippingChoiceQuery();

            var shippingChoicesResult = _taxCalculationService.SetShippingChoicesPrices(shippingChoiceQuery);

            var taxCalculationShippingChoicesResult = MappingToTaxCalculationShippingChoicesResult(shippingChoicesResult);

            return taxCalculationShippingChoicesResult;
        }

        public List<TaxCalculationAvailableWrapResult> SetAvailableWrapPriceAndTaxInfos(OrderPipeWrapQuery orderPipeWrapQuery)
        {
            var wrapsQuery = orderPipeWrapQuery.ToWrapsQuery();

            var wrapsResult = _taxCalculationService.SetWrapsPrices(wrapsQuery);

            var taxCalculationAvailableWrapsResult = MappingToTaxCalculationWrapsResult(wrapsResult);

            return taxCalculationAvailableWrapsResult;
        }

        public TaxCalculationSplitMethodResult SetSplitMethodPrices(OrderPipeSplitMethodQuery orderPipeSplitMethodQuery)
        {
            var splitMethodQuery = orderPipeSplitMethodQuery.ToSplitMethodQuery();

            var splitMethodResult = _taxCalculationService.SetSplitMethodPrices(splitMethodQuery);

            var taxCalcultationSplitMethodResult = MappingToTaxCalculationSplitMethodResult(splitMethodResult);

            return taxCalcultationSplitMethodResult;
        }

        #region private methods

        private TaxCalculationSplitMethodResult MappingToTaxCalculationSplitMethodResult(TaxModel.SplitMethodResult splitMethodResult)
        {
            var taxCalculationSplitMethodResult = new TaxCalculationSplitMethodResult
            {
                GenericGlobalPrice = MappingToGenericPrice(splitMethodResult.GlobalPrice),
                GenericUnitPrice = MappingToGenericPrice(splitMethodResult.UnitPrice)
            };

            return taxCalculationSplitMethodResult;   
        }

        private List<TaxCalculationAvailableWrapResult> MappingToTaxCalculationWrapsResult(List<TaxModel.WrapResult> wrapsResult)
        {
            var availableWrapResultList = wrapsResult.Select(w => new TaxCalculationAvailableWrapResult
                                                                  {
                                                                      Id = w.Id,
                                                                      TaxInfos = MappingToTaxInfos(w.TaxInfos),
                                                                      PriceDetail = MappingToGenericPrice(w.PriceDetail),
                                                                      PriceGlobal = MappingToGenericPrice(w.PriceGlobal)
                                                                  }
                                                            )
                                                     .ToList();

            return availableWrapResultList;
        }      

        private TaxCalculationShippingChoicesResult MappingToTaxCalculationShippingChoicesResult(TaxModel.ShippingChoicesResult shippingChoicesResult)
        {
            var taxCalculationShippingChoicesResult = new TaxCalculationShippingChoicesResult
            {
                TaxInfos = MappingToTaxInfos(shippingChoicesResult.TaxInfos),
                Choices = MappingToTaxCalculationShippingChoiceResult(shippingChoicesResult.Choices)
            };

            return taxCalculationShippingChoicesResult;
        }

        private List<TaxCalcultationShippingChoiceResult> MappingToTaxCalculationShippingChoiceResult(List<TaxModel.ShippingChoiceResult> choices)
        {
            var taxCalcultationShippingChoiceResultList = choices.Select(c => new TaxCalcultationShippingChoiceResult
            {
                MainShippingMethodId = c.MainShippingMethodId,
                TotalPrice = MappingToGenericPrice(c.TotalPrice),
                Groups = c.Groups.Select(g => new TaxCalculationGroupResult
                {
                    Id = g.Id,
                    GlobalPrice = MappingToGenericPrice(g.GlobalPrice),
                    Items = g.Items.Select(i => new TaxCalcuationItemInGroupResult
                    {
                        Identifier = new Identifier
                        {
                            Prid = i.Identifier.Prid,
                            OfferReference = i.Identifier.OfferReference
                        },
                        UnitPrice = MappingToGenericPrice(i.UnitPrice)
                    })
                })
            }).ToList();

            return taxCalcultationShippingChoiceResultList;
        }

        private GenericPrice MappingToGenericPrice(TaxModel.Price price)
        {
            return new GenericPrice
            {
                Cost = Math.Round(price.Cost, 2, MidpointRounding.ToEven),
                CostToApply = Math.Round(price.CostToApply.GetValueOrDefault(), 2, MidpointRounding.ToEven),
                NoTaxCost = Math.Round(price.NoTaxCost.GetValueOrDefault(), 2, MidpointRounding.ToEven),
                TaxInfos = MappingToVatInfo(price.TaxInfos)
            };
        }

        private VatInfo MappingToVatInfo(TaxModel.TaxInfos taxInfos)
        {
            if (taxInfos == null)
                return null;

            return new VatInfo
            {
                CountryCode = taxInfos.CountryId,
                Rate = taxInfos.TaxRateToApply,
                VatCategory = taxInfos.TaxType,
                CodeGU = taxInfos.CodeGu
            };
        }

        private TaxInfos MappingToTaxInfos(TaxModel.TaxInfos taxInfos)
        {
            return new TaxInfos
            {
                CountryId = taxInfos.CountryId,
                CountryTaxRate = taxInfos.TaxRateToApply,
                TaxRate = taxInfos.TaxRate,
                TaxType = taxInfos.TaxType,
                CodeGu = taxInfos.CodeGu,
                UseTax = taxInfos.UseTax
            };
        }

        private TaxCalcuationLineGroupResult MappingToTaxCalculationResultItem(TaxModel.LineGroupResult lineGroupResult)
        {
            var taxInfoToApply = lineGroupResult.GetTaxInfoToApply();
            var taxCalcuationLineGroupResult = new TaxCalcuationLineGroupResult
            {
                TaxInfos = new TaxInfos
                {
                    //LigneGroupResult.TaxInfos est la valeur par défaut quand récupère dans la BDD
                    //Mais le countryId (pays fiscal) peut varier en fonction des règles metiers
                    CountryId = taxInfoToApply.CountryId,
                    UseTax = taxInfoToApply.UseTax,
                },
                TaxCalculationResultItems = lineGroupResult.Items.Select(item => new TaxCalculationResultItem
                {
                    Identifier = new Identifier()
                    {
                        Prid = item.Identifier.Prid,
                        OfferReference = item.Identifier.OfferReference
                    },
                    IsDematItem = item.IsDematItem,
                    IsService = item.IsService,
                    SellerId = item.SellerId,
                    ItemPrice = new TaxCalculationPriceResult()
                    {
                        ReducedPriceToApply = new OrderPipeSalesInfoPrice { TTC = item.ItemPrice.ReducedPriceToApply.TTC, HT = item.ItemPrice.ReducedPriceToApply.HT },
                        PublicPriceToApply = new OrderPipeSalesInfoPrice { TTC = item.ItemPrice.PublicPriceToApply.TTC, HT = item.ItemPrice.PublicPriceToApply.HT },
                        StandardPriceToApply = new OrderPipeSalesInfoPrice { TTC = item.ItemPrice.StandardPriceToApply.TTC, HT = item.ItemPrice.PublicPriceToApply.HT },
                        EcoTaxAmountToApply = new OrderPipeSalesInfoPrice { TTC = item.ItemPrice.EcoTaxAmountToApply.TTC, HT = item.ItemPrice.EcoTaxAmountToApply.HT },
                        TaxToApplyInfos = new TaxInfos()
                        {
                            TaxType = item.ItemPrice.TaxInfosToApply.TaxType,
                            TaxRate = item.ItemPrice.TaxInfosToApply.TaxRate,
                            CountryId = item.ItemPrice.TaxInfosToApply.CountryId,
                            CountryTaxRate = item.ItemPrice.TaxInfosToApply.TaxRateToApply,
                            CodeGu = item.ItemPrice.TaxInfosToApply.CodeGu,
                            UseTax = item.ItemPrice.TaxInfosToApply.UseTax
                        }
                    }
                }).ToList()
            };

            var shippingGlobalPriceResut = lineGroupResult.GetGlobalPriceByType(tax.Models.Enums.GlobalPriceType.Shipping);
            if (shippingGlobalPriceResut != null)
            {
                taxCalcuationLineGroupResult.AddOrUpdateShippingGlobalPrice(shippingGlobalPriceResut.Cost, shippingGlobalPriceResut.CostToApply, shippingGlobalPriceResut.NoTaxCost);
            }

            return taxCalcuationLineGroupResult;
        }

        private List<TaxCalculationResultItem> MappingTaxCalculationResultItem(List<TaxModel.ItemResult> items)
        {
            return items.Select(item => new TaxCalculationResultItem()
            {
                ItemPrice = new TaxCalculationPriceResult()
                {
                    ReducedPriceToApply = new OrderPipeSalesInfoPrice { TTC = item.ItemPrice.ReducedPriceToApply.TTC, HT = item.ItemPrice.ReducedPriceToApply.HT },
                    PublicPriceToApply = new OrderPipeSalesInfoPrice { TTC = item.ItemPrice.PublicPriceToApply.TTC, HT = item.ItemPrice.PublicPriceToApply.HT },
                    StandardPriceToApply = new OrderPipeSalesInfoPrice { TTC = item.ItemPrice.StandardPriceToApply.TTC, HT = item.ItemPrice.PublicPriceToApply.HT },
                    TaxToApplyInfos = new TaxInfos()
                    {
                        TaxType = item.ItemPrice.TaxInfosToApply.TaxType,
                        TaxRate = item.ItemPrice.TaxInfosToApply.TaxRate,
                        CountryId = item.ItemPrice.TaxInfosToApply.CountryId,
                        CountryTaxRate = item.ItemPrice.TaxInfosToApply.TaxRateToApply,
                        CodeGu = item.ItemPrice.TaxInfosToApply.CodeGu,
                        UseTax = item.ItemPrice.TaxInfosToApply.UseTax
                    }
                },
                Identifier = new Identifier()
                {
                    Prid = item.Identifier.Prid,
                    OfferReference = item.Identifier.OfferReference
                },
                SellerId = item.SellerId,
                IsDematItem = item.IsDematItem,
                IsService = item.IsService
            }).ToList();
        }

        #endregion
    }
}
