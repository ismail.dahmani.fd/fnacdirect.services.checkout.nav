using System;
using FnacDirect.OrderPipe.Config;

namespace FnacDirect.OrderPipe
{
    /// <summary>
    /// Structure encapsulant le r�sultat de l'ex�cution du pipe
    /// </summary>
    public class PipeResult
    {
        /// <summary>
        /// Url de redirection vers l'�tape interactive courante
        /// </summary>
        public UIDescriptor UIDescriptor { get; set; }

        /// <summary>
        /// Etape interactive courante
        /// </summary>
        public IInteractiveStep CurrentStep { get; set; }

        /// <summary>
        /// Etape th�orique courante
        /// </summary>
        public string TheoricStep { get; set; }

        /// <summary>
        /// Retour de la derni�re �tape ex�cut�e
        /// </summary>
        public StepReturn LastStepReturn { get; set; }
    }

}
