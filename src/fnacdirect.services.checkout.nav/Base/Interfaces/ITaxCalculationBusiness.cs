using FnacDirect.Contracts.Online.Model;
using FnacDirect.OrderPipe.Base.BaseModel;
using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.OrderPipe.Base.Model.LineGroups;
using System.Collections.Generic;

namespace FnacDirect.OrderPipe.Base.Business.TaxCalculation
{
    public interface ITaxCalculationBusiness
    {
        void SetGlobalPriceLineGroup(IEnumerable<ILineGroup> lineGroups, BillingAddress billingAddress, ShippingMethodEvaluationItem selectedShippingMethodEvaluationItem, ShippingMethodEvaluationGroup shippingMethodEvaluationGroup, string pipeName, string customerTaxNumber);
        void SetShippingMethodEvaluationItemsShippingChoicesPrices(BillingAddress billingAddress, ShippingMethodEvaluationGroup shippingMethodEvaluationGroup, string pipeName, string customerTaxNumber);
        void SetAvailabelWrapMethods(WrapMethodList availableWrapMethods, int? taxCountryId, bool isProfessionalAddress, string pipeName, string customerTaxNumber);
        void SetAvailableSplitPrice(SplitMethod splitMethod, int? taxCountryId, bool isProfessionalAddress, string pipeName, string customerTaxNumber);
        List<StandardArticle> SetExtendedBundleComponentPrices(List<SalesArticle> bundleComponentPrices, int taxCountryId, bool isProfessionalAddress, string pipeName, string customerTaxNumber);
    }
}
