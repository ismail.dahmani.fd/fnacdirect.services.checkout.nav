
using System;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Membership
{
    public enum AdhesionType
    {
        None,
        AdherentOneYear,
        AdherentThreeYears,
        AdherentOne,
        AdherentAmex,
        AdherentAmexPlus,
        AdherentOneAmex
    }

    public class MembershipProfilViewModel
    {
        public string FirstName { get; set; }

        public string LastName { get; set; }

        public int BirthDay { get; set; }

        public int BirthMonth { get; set; }

        public int BirthYear { get; set; }

        public string CardLabel { get; set; }

        public AdhesionType Type { get; set; }

        public string FiscalTaxNumberType { get; set; }
        public string FiscalTaxNumber { get; set; }
    }
}
