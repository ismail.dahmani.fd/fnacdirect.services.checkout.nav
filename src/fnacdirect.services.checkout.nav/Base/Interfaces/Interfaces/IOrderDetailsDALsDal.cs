using FnacDirect.OrderPipe.Base.LineGroups;
using FnacDirect.OrderPipe.Base.Model;
using System.Collections.Generic;

namespace FnacDirect.OrderPipe.Base.DAL
{
    public interface IOrderDetailsDAL
    {
        List<PreviousLineGroup> GetOrderDetails(string prmszOrderReference, int referenceType);
        string GetAdvantageCode(int orderId);
        void AddOrderDetailAddOn(int orderDetailFk, int typeArticle, int itemGroup, bool flagVenteService, string partnerName, NumericalAddOn numAddOn, string koboUserId);
    }
}
