using System.Collections.Generic;

namespace FnacDirect.OrderPipe.Base.Proxy.Customer
{
    public interface IStoreAddressBookService
    {
        IEnumerable<FnacStore.Model.FnacStore> GetAddresses();
    }
}
