using FnacDirect.Context;
using FnacDirect.OrderPipe.Base.Model.Facets;
using FnacDirect.OrderPipe.Base.Proxy.Membership;
using FnacDirect.Technical.Framework.Web.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FnacDirect.OrderPipe.Base.Business.Membership
{
    public interface ICustomerSegmentEvaluatorProvider
    {
        CustomerSegmentEvaluator GetCustomerSegmentEvaluatorInstance(IPrivateDataContainer orderFormPrivateDataContainer);
    }

    public class CustomerSegmentEvaluatorProvider : ICustomerSegmentEvaluatorProvider
    {
        private readonly IUserContextProvider _userContextProvider;
        private readonly IUserExtendedContextProvider _userExtendedContextProvider;
        private readonly IMembershipConfigurationService _membershipConfigurationService;

        public CustomerSegmentEvaluatorProvider(
            IUserContextProvider userContextProvider,
            IUserExtendedContextProvider userExtendedContextProvider,
            IMembershipConfigurationService membershipConfigurationService)
        {
            _userContextProvider = userContextProvider;
            _userExtendedContextProvider = userExtendedContextProvider;
            _membershipConfigurationService = membershipConfigurationService;
        }

        public CustomerSegmentEvaluator GetCustomerSegmentEvaluatorInstance(IPrivateDataContainer orderFormPrivateDataContainer)
        {
            return new CustomerSegmentEvaluator(
                _userContextProvider,
                _userExtendedContextProvider,
                _membershipConfigurationService,
                orderFormPrivateDataContainer);
        }
    }
}
