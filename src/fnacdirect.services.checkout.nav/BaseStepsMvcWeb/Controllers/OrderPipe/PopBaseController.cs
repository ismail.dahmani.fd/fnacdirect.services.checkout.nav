﻿using FnacDirect.OrderPipe.CoreMvcWeb.Routing;
using FnacDirect.Technical.Framework.Web.Mvc.Nustache;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Controllers.Pop
{
    public abstract class PopBaseController : OrderPipeController
    {
        protected override IEnumerable<IViewEngine> GetViewEngines()
        {
            yield return DependencyResolver.Current.GetService<PopRazorViewEngine>();
            yield return DependencyResolver.Current.GetService<PopNustacheViewEngine>();
        }
    }
}
