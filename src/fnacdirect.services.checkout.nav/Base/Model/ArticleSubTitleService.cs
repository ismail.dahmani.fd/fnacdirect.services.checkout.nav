﻿using FnacDirect.Technical.Framework.Web.Mvc;
using System.Collections.Generic;
using System.Text;
using FnacDirect.Contracts.Online.Model;
using FnacDirect.OrderPipe.Base.Model;
using System.Linq;
using FnacDirect.Front.WebBusiness;
using BaseArticle = FnacDirect.OrderPipe.Base.Model.BaseArticle;
using DTO = FnacDirect.Contracts.Online.Model;

namespace FnacDirect.OrderPipe.Base.Business
{
    public class ArticleSubTitleService : IArticleSubTitleService
    {
        private readonly int _donationTypeId;
        private readonly IApplicationContext _applicationContext;

        public ArticleSubTitleService(IApplicationContext applicationContext)
        {
            _applicationContext = applicationContext;
            _donationTypeId = int.Parse(_applicationContext.GetAppSetting("DonationTypeId"));
        }

        private string BuildSubtitleForMarketPlacePt(BaseArticle baseArticle)
        {
            var stringBuilder = new StringBuilder();

            var parts = new List<string>();

            if (!string.IsNullOrEmpty(baseArticle.FormatLabel))
            {
                parts.Add(baseArticle.FormatLabel);
            }

            if (parts.Any())
            {
                stringBuilder.Append(string.Join(" - ", parts));
            }

            return stringBuilder.ToString();
        }

        private string BuildSubtitleForMarketPlaceBook(BaseArticle baseArticle, DTO.Article dtoArticle)
        {
            var stringBuilder = new StringBuilder();

            var participants = string.Empty;
            if (dtoArticle != null && dtoArticle.ExtendedProperties != null)
            {
                if (dtoArticle.ExtendedProperties.Roles != null)
                {
                    var primaryParticipants = dtoArticle.ExtendedProperties.Roles.Where(r => r.ID == (int)RoleType.Author).SelectMany(r => r.Participants).ToList();
                    if (primaryParticipants.Any())
                    {
                        participants = string.Join(", ", primaryParticipants.Select(p => string.Format("{0} {1}", p.FirstName, p.LastName)).ToArray());
                    }
                }

                var parts = new List<string>();

                if (!string.IsNullOrEmpty(participants))
                {
                    parts.Add(participants);
                }

                if (parts.Any())
                {
                    stringBuilder.Append(string.Join(" - ", parts));
                }
            }
            if (!string.IsNullOrEmpty(baseArticle.FormatLabel))
            {
                stringBuilder.Append(string.Format(" ({0})", baseArticle.FormatLabel));
            }

            return stringBuilder.ToString();
        }

        private string BuildSubtitleForMarketPlaceAudio(BaseArticle baseArticle, DTO.Article dtoArticle)
        {
            var stringBuilder = new StringBuilder();

            var participants = string.Empty;
            if (dtoArticle != null && dtoArticle.ExtendedProperties != null)
            {
                if (dtoArticle.ExtendedProperties.Roles != null)
                {
                    var primaryParticipants = dtoArticle.ExtendedProperties.Roles.Where(r => r.ID == (int)RoleType.Compositor || r.ID == (int)RoleType.Performer).SelectMany(r => r.Participants).ToList();

                    if (primaryParticipants.Any())
                    {
                        participants = string.Join(", ", primaryParticipants.Select(p => string.Format("{0} {1}", p.FirstName, p.LastName)).ToArray());
                    }
                }
                var parts = new List<string>();

                if (!string.IsNullOrEmpty(participants))
                {
                    parts.Add(participants);
                }

                if (parts.Any())
                {
                    stringBuilder.Append(string.Join(" - ", parts));
                }
            }
            if (!string.IsNullOrEmpty(baseArticle.FormatLabel))
            {
                stringBuilder.Append(string.Format(" ({0})", baseArticle.FormatLabel));
            }

            return stringBuilder.ToString();
        }

        private string BuildSubtitleForMarketPlaceSoftware(BaseArticle baseArticle)
        {
            var stringBuilder = new StringBuilder();

            if (!string.IsNullOrEmpty(baseArticle.FormatLabel))
            {
                stringBuilder.Append(baseArticle.FormatLabel);
            }

            return stringBuilder.ToString();
        }

        private string BuildSubtitleForMarketPlaceVideo(BaseArticle baseArticle, DTO.Article dtoArticle)
        {
            var stringBuilder = new StringBuilder();

            var participants = string.Empty;
            if (dtoArticle != null && dtoArticle.ExtendedProperties != null && dtoArticle.ExtendedProperties.Roles != null)
            {

                var primaryParticipants = dtoArticle.ExtendedProperties.Roles.Where(r => r.ID == (int)RoleType.Director).SelectMany(r => r.Participants).ToList();

                if (primaryParticipants.Any())
                {
                    participants = string.Join(", ", primaryParticipants.Select(p => string.Format("{0} {1}", p.FirstName, p.LastName)).ToArray());
                }
            }

            stringBuilder.Append(participants);

            return stringBuilder.ToString();
        }

        private string BuildSubtitleForPt(BaseArticle baseArticle)
        {
            var stringBuilder = new StringBuilder();

            var parts = new List<string>();

            if (!string.IsNullOrEmpty(baseArticle.FormatLabel))
            {
                parts.Add(baseArticle.FormatLabel);
            }

            if (!string.IsNullOrEmpty(baseArticle.Editor))
            {
                parts.Add(baseArticle.Editor);
            }

            if (parts.Any())
            {
                stringBuilder.Append(string.Join(" - ", parts));
            }

            return stringBuilder.ToString();
        }

        private string BuildSubtitleForBook(BaseArticle baseArticle, DTO.Article dtoArticle)
        {
            var stringBuilder = new StringBuilder();

            var participants = string.Empty;
            if (dtoArticle != null && dtoArticle.ExtendedProperties != null)
            {

                if (dtoArticle.ExtendedProperties.Roles != null)
                {
                    var primaryParticipants = dtoArticle.ExtendedProperties.Roles.SelectMany(r => r.Participants).Where(p => p.Level == LevelParticipant.Primary).ToList();

                    if (primaryParticipants.Any())
                    {
                        participants = string.Join(", ", primaryParticipants.Select(p => string.Format("{0} {1}", p.FirstName, p.LastName)).ToArray());
                    }
                }

                var parts = new List<string>();

                var subTitle = dtoArticle.ExtendedProperties.SubTitle;
                if (!string.IsNullOrEmpty(subTitle) && subTitle != baseArticle.Title && subTitle != baseArticle.SerieTitle)
                {
                    parts.Add(subTitle);
                }

                if (!string.IsNullOrEmpty(baseArticle.SerieTitle) && baseArticle.SerieTitle != baseArticle.Title)
                {
                    parts.Add(baseArticle.SerieTitle);
                }

                if (dtoArticle.ExtendedProperties.Volume.HasValue)
                {
                    parts.Add(string.Format(_applicationContext.GetMessage("orderpipe.pop.basket.subtitle.tomaison"), dtoArticle.ExtendedProperties.Volume.Value));
                }

                if (!string.IsNullOrEmpty(participants))
                {
                    parts.Add(participants);
                }

                if (parts.Any())
                {
                    stringBuilder.Append(string.Join(" - ", parts));
                }
            }

            if (!string.IsNullOrEmpty(baseArticle.FormatLabel))
            {
                stringBuilder.Append(string.Format(" ({0})", baseArticle.FormatLabel));
            }

            return stringBuilder.ToString();
        }

        private string BuildSubtitleForAudio(BaseArticle baseArticle, DTO.Article dtoArticle)
        {
            var stringBuilder = new StringBuilder();

            var participants = string.Empty;
            if (dtoArticle != null && dtoArticle.ExtendedProperties != null)
            {
                if (dtoArticle.ExtendedProperties.Roles != null)
                {

                    var primaryParticipants = dtoArticle.ExtendedProperties.Roles.SelectMany(r => r.Participants).Where(p => p.Level == LevelParticipant.Primary).ToList();
                    if (primaryParticipants.Any())
                    {
                        participants = string.Join(", ", primaryParticipants.Select(p => string.Format("{0} {1}", p.FirstName, p.LastName)).ToArray());
                    }
                }
                var parts = new List<string>();

                if (!string.IsNullOrEmpty(participants))
                {
                    parts.Add(participants);
                }

                if (!string.IsNullOrEmpty(baseArticle.SerieTitle))
                {
                    parts.Add(baseArticle.SerieTitle);
                }

                if (!string.IsNullOrEmpty(baseArticle.SerieTitle) && baseArticle.SerieTitle != baseArticle.Title)
                {
                    parts.Add(baseArticle.SerieTitle);
                }

                if (dtoArticle.ExtendedProperties.Volume.HasValue)
                {
                    parts.Add(string.Format(_applicationContext.GetMessage("orderpipe.pop.basket.subtitle.tomaison"), dtoArticle.ExtendedProperties.Volume.Value));
                }

                if (!string.IsNullOrEmpty(baseArticle.FormatLabel))
                {
                    if (dtoArticle.ExtendedProperties.NumberOfUnits > 1)
                    {
                        var ressource = _applicationContext.GetMessage("orderpipe.pop.basket.subtitle.audio.format", dtoArticle.ExtendedProperties.NumberOfUnits);

                        parts.Add(string.Format("{0} ({1})", baseArticle.FormatLabel, ressource));
                    }
                    else
                    {
                        parts.Add(baseArticle.FormatLabel);
                    }
                }

                if (parts.Any())
                {
                    stringBuilder.Append(string.Join(" - ", parts));
                }
            }
            return stringBuilder.ToString();
        }

        private string BuildSubtitleForVideo(BaseArticle baseArticle, DTO.Article dtoArticle)
        {
            var stringBuilder = new StringBuilder();

            var directors = string.Empty;
            if (dtoArticle != null && dtoArticle.ExtendedProperties != null)
            {

                if (dtoArticle.ExtendedProperties.Roles != null)
                {
                    var primaryParticipants = dtoArticle.ExtendedProperties.Roles.Where(r => r.ID == (int)RoleType.Director).SelectMany(r => r.Participants).Where(p => p.Level == LevelParticipant.Primary).ToList();
                    if (primaryParticipants.Any())
                    {
                        directors = string.Join(", ", primaryParticipants.Select(p => string.Format("{0} {1}", p.FirstName, p.LastName)).ToArray());
                    }
                }
                var parts = new List<string>();

                if (!string.IsNullOrEmpty(directors))
                {
                    parts.Add(directors);
                }

                if (!string.IsNullOrEmpty(baseArticle.SerieTitle) && baseArticle.SerieTitle != baseArticle.Title)
                {
                    parts.Add(baseArticle.SerieTitle);
                }

                if (dtoArticle.ExtendedProperties.Volume.HasValue)
                {
                    parts.Add(string.Format(_applicationContext.GetMessage("orderpipe.pop.basket.subtitle.tomaison"), dtoArticle.ExtendedProperties.Volume.Value));
                }

                if (parts.Any())
                {
                    stringBuilder.Append(string.Join(" - ", parts));
                }
            }

            return stringBuilder.ToString();
        }

        private string BuildSubtitleForSoftware(BaseArticle baseArticle)
        {
            var stringBuilder = new StringBuilder();

            var parts = new List<string>();

            if (!string.IsNullOrEmpty(baseArticle.Content))
            {
                parts.Add(baseArticle.Content);
            }

            if (!string.IsNullOrEmpty(baseArticle.SerieTitle) && baseArticle.SerieTitle != baseArticle.Title)
            {
                parts.Add(baseArticle.SerieTitle);
            }

            if (parts.Any())
            {
                stringBuilder.Append(string.Join(" - ", parts));
            }

            return stringBuilder.ToString();
        }

        public string BuildSubTitle(BaseArticle baseArticle, DTO.Article dtoArticle)
        {
            // s'agit il d'un article mp pur ?
            var onlyMp = baseArticle is MarketPlaceArticle && (baseArticle as MarketPlaceArticle).Offer.ArticleReference.Catalog == ArticleCatalog.MarketPlace;

            var articleGroup = (ArticleGroup)baseArticle.Family.GetValueOrDefault(0);

            if (onlyMp)
            {
                if (baseArticle.IsPT)
                {
                    return BuildSubtitleForMarketPlacePt(baseArticle);
                }
                else
                {
                    if (articleGroup == ArticleGroup.Book)
                    {
                        return BuildSubtitleForMarketPlaceBook(baseArticle, dtoArticle);
                    }
                    else if (articleGroup == ArticleGroup.Audio)
                    {
                        return BuildSubtitleForMarketPlaceAudio(baseArticle, dtoArticle);
                    }
                    else if (articleGroup == ArticleGroup.Video)
                    {
                        return BuildSubtitleForMarketPlaceVideo(baseArticle, dtoArticle);
                    }
                    else if (articleGroup == ArticleGroup.Software)
                    {
                        return BuildSubtitleForMarketPlaceSoftware(baseArticle);
                    }
                }
            }
            else
            {
                if (baseArticle.TypeId == _donationTypeId)
                {
                    return BuildSubtitleForDonation(baseArticle);
                }
                else if (baseArticle.IsPT)
                {
                    return BuildSubtitleForPt(baseArticle);
                }
                else
                {
                    if (articleGroup == ArticleGroup.Book)
                    {
                        return BuildSubtitleForBook(baseArticle, dtoArticle);
                    }
                    else if (articleGroup == ArticleGroup.Audio)
                    {
                        return BuildSubtitleForAudio(baseArticle, dtoArticle);
                    }
                    else if (articleGroup == ArticleGroup.Video)
                    {
                        return BuildSubtitleForVideo(baseArticle, dtoArticle);
                    }
                    else if (articleGroup == ArticleGroup.Software)
                    {
                        return BuildSubtitleForSoftware(baseArticle);
                    }
                }
            }

            return string.Empty;
        }

        private string BuildSubtitleForDonation(BaseArticle baseArticle)
        {
            var stdArticle = baseArticle as StandardArticle;
            if (stdArticle == null)
                return string.Empty;

            return stdArticle.SubTitle1;
        }
    }
}
