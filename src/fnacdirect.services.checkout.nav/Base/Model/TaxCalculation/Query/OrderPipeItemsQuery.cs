using FnacDirect.OrderPipe.TaxCalculation.Front.Models;
using System.Collections.Generic;
using System.Linq;

namespace FnacDirect.OrderPipe.Base.Model.TaxCalculation
{
    public class OrderPipeItemsQuery : Query
    {
        public List<TaxCalculationQueryItem> Items { get; set; }

        public ItemsQuery ToItemsQuery()
        {
            var itemsQuery = new ItemsQuery
            {
                Items = Items.Select(i => new ItemRequest
                {
                    Identifier = new OrderPipe.TaxCalculation.Front.Models.Identifier
                    {
                        Prid = i.Identifier.Prid,
                        OfferReference = i.Identifier.OfferReference
                    },
                    IsDematItem = i.IsDematItem,
                    IsService = i.IsService,
                    SalesInfo = i.SalesInfo.ToTaxCalculationSalesInfo(),
                    SellerId = i.SellerId
                }).ToList(),
                OrderContext = OrderContext.ToOrderContext()
            };

            return itemsQuery;
        }
    }
}
