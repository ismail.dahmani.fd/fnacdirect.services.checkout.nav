﻿using FnacDirect.Front.WebBusiness;
using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.Technical.Framework.CoreServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.OrderThankYou
{
    public enum OrderDetailShippingModeEnum
    {
        Postal,
        Relay,
        Shop
    }

    public class OrderDetailViewModel
    {
        //Panier x/y
        public int Index { get; set; }
        public int Total { get; set; }

        public List<string> References { get; set; }

        public bool IsFnac { get; set; }

        public string Seller { get; set; }
        public bool DisplaySeller { get; set; }

        public OrderDetailAddress Address { get; set; }

        public OrderDetailShippingModeEnum ShippingMode { get; set; }

        private List<OrderDetailArticle> _articles = new List<OrderDetailArticle>();

        public OrderDetailArticle WithArticle(Base.BaseModel.Model.OrderDetails.OrderDetailArticle orderDetailArticle, Article article, bool hasClickAndCollect)
        {
            var articleViewModel = new OrderDetailArticle();

            var services = new List<OrderDetailArticle>();

            articleViewModel.Prid = orderDetailArticle.Prid;

            articleViewModel.Title = orderDetailArticle.Title;
            articleViewModel.PicturUrl = orderDetailArticle.UrlPicture;

            articleViewModel.Quantity = orderDetailArticle.Quantity;

            articleViewModel.OrderDetailArticleShippingMode = OrderDetailArticleShippingModeEnum.None;

            if (article != null)
            {

                if (article is StandardArticle standardArticle)
                {
                    articleViewModel.DeliveryDates = orderDetailArticle.TheoreticalDeliveryDate;

                    if (standardArticle.IsNumerical)
                    {
                        articleViewModel.OrderDetailArticleShippingMode = OrderDetailArticleShippingModeEnum.Demat;
                    }
                    if (standardArticle.IsEbook)
                    {
                        articleViewModel.OrderDetailArticleShippingMode = OrderDetailArticleShippingModeEnum.Ebook;
                    }
                    if (standardArticle.IsAdhesionCard)
                    {
                        articleViewModel.OrderDetailArticleShippingMode = OrderDetailArticleShippingModeEnum.AdherentCard;
                    }
                    if (hasClickAndCollect)
                    {
                        articleViewModel.OrderDetailArticleShippingMode = OrderDetailArticleShippingModeEnum.ClickAndCollect;
                    }

                    foreach (var service in orderDetailArticle.Services)
                    {
                        var serviceModel = new OrderDetailArticle
                        {
                            Prid = service.Prid,
                            Quantity = service.Quantity,
                            Title = service.Title,
                            PicturUrl = service.UrlPicture
                        };

                        services.Add(serviceModel);
                    }
                }

                if (article.ArticleDTO != null && article.ArticleDTO.PartnerLinks != null && article.ArticleDTO.PartnerLinks.Any(p => p.PartnerId == (int)PartnerId.DigitalLocker))
                {
                    articleViewModel.HasDigiCopy = true;
                }
            }
            _articles.Add(articleViewModel);

            _articles.AddRange(services);

            return articleViewModel;
        }

        public void AddRangeArticle(List<OrderDetailArticle> newArticles)
        {
            _articles.AddRange(newArticles);
        }

        public IReadOnlyCollection<OrderDetailArticle> Articles
        {
            get
            {
                return _articles.AsReadOnly();
            }
        }
    }
}
