﻿namespace FnacDirect.OrderPipe.Core
{
    public interface IOPContextProvider
    {
        OPContext GetOrCreateCurrentContext(string sid, string uid, IOP pipe);
        OPContext GetCurrentContext(string pipeName);
    }
}
