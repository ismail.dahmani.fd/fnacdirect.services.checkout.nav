namespace FnacDirect.OrderPipe.Base.Model.BillingMethods
{
    public class FibBillingMethod : BillingMethod
    {
        public const int IdBillingMethod = 1073741824;

        public override bool IsPrimary => true;

        public static string BillingMethodName
        {
            get
            {
                return "FibBillingMethodName";
            }
        }

        public override bool UseTransaction
        {
            get
            {
                return true;
            }
        }

        public override bool IsGlobalPayment
        {
            get
            {
                return true;
            }
        }

        public FibBillingMethod()
        {
            BillingMethodId = IdBillingMethod;
            Name = BillingMethodName;
            CanModifyAmountUp = true;
            CanModifyAmountDown = true;
            ImplyValidateOrder = true;
        }
    }
}
