﻿using FnacDirect.OrderPipe.Config;
using FnacDirect.Context;
using FnacDirect.OrderPipe.Core.Web.Routing;
using FnacDirect.OrderPipe.CoreMvcWeb;
using FnacDirect.OrderPipe.CoreMvcWeb.Routing;
using FnacDirect.OrderPipe.CoreRouting;
using StructureMap;
using FnacDirect.OrderPipe.Context;
using FnacDirect.OrderPipe.Mvc;
using FnacDirect.OrderPipe;

namespace StructureMap.Configuration.DSL
{
    public static class OrderPipeCoreMvcRegistryExtensions
    {
        public static void AddOrderPipeCoreMvc(this IRegistry registry)
        {
            registry.AddOrderPipeCoreRouting();

            registry.For<OrderPipeRazorViewEngine>().Singleton().Use<OrderPipeRazorViewEngine>();

            registry.For<IOrderPipeInteractiveStepHandlerStrategy<MvcUiDescriptor>>().Use<MvcOrderPipeInteractiveStepHandlerStrategy>();
            registry.For<IOrderPipeStepRedirectionStrategy<MvcUiDescriptor>>().Use<MvcOrderPipeStepRedirectionStrategy>();
            registry.For<IOrderPipeResetStrategy<MvcPipeDescriptor>>().Use<MvcOrderPipeResetStrategy>();
            registry.For<IOrderPipeForceStepStrategy<MvcPipeDescriptor>>().Use<MvcOrderPipeForceStepStrategy>();
            registry.For<IUiDescriptorRenderer<MvcUiDescriptor>>().Use<MvcUiDescriptorRenderer>();

            registry.For<IOrderPipeUrlBuilder>().Use<OrderPipeUrlBuilder>();
        }
    }
}