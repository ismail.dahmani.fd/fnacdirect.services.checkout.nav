﻿using System.Web.Mvc;

namespace FnacDirect.OrderPipe.BaseMvc.Web.HtmlHelpers.Pop
{
    public static class ContextHelper
    {
        public static bool IsContext(this HtmlHelper htmlHelper, string contextKey)
        {
            var opContext = DependencyResolver.Current.GetService<OPContext>();
            if (opContext != null)
            {
                if (opContext.OF is IOF orderForm)
                {
                    return orderForm.ExecutionContext.IsIn(contextKey);
                }
            }

            return false;
        }
    }
}
