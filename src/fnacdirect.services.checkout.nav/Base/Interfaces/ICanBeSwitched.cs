﻿namespace FnacDirect.OrderPipe
{
    public interface ICanBeSwitched
    {
        string SwitchId { get; set; }

        string SwitchMode { get; set; }
    }
}
