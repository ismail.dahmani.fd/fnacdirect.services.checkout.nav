﻿using FnacDirect.Technical.Framework.Web.Mvc.DataAnnotations;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Kobo
{
    public class KoboMergeAccountViewModel
    {
        public string UserReference { get; set; }

        [DisplayName("orderpipe.pop.kobo.email")]
        [System.ComponentModel.DataAnnotations.Required]
        [EmailAddress]
        public string Email { get; set; }

        [DisplayName("orderpipe.pop.kobo.password")]
        [System.ComponentModel.DataAnnotations.Required]        
        public string Password { get; set; }
    }
}
