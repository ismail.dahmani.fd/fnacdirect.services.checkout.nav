﻿namespace FnacDirect.OrderPipe
{
    using System;
    using Spring.Context;
    using Spring.Context.Support;
    using System.Linq;

    public static class ObjectFactory
    {
        public static T Get<T>(string name) where T : class
        {
            var ctx = ContextRegistry.GetContext();
            var lname = ctx.GetObjectDefinitionNames().FirstOrDefault(s => string.Equals(s, name, StringComparison.InvariantCultureIgnoreCase));
            if (string.IsNullOrWhiteSpace(lname)) return null;
            return ctx.GetObject(lname, typeof(T)) as T;
        }

        public static bool ContainsDefinition(string name)
        {
            var context = ContextRegistry.GetContext();
            return context.GetObjectDefinitionNames().Any(x => string.Equals(x, name, StringComparison.InvariantCultureIgnoreCase));
        }

        public static T Get<T>() where T : class
        {
            var ctx = ContextRegistry.GetContext();
            var names = ctx.GetObjectNamesForType(typeof(T));
            if (names != null && names.Count() == 1)
                return Get<T>(names[0]);
            else
                throw new InvalidOperationException("Type is not unique in objects definition");
        }
    }
}
