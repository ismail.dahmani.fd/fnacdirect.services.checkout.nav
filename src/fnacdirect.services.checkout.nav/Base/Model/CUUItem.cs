namespace FnacDirect.OrderPipe.Base.Business
{
    public class CUUItem
    {
        public decimal Amount;
        public string Code;
        public bool Valid;

        public string MaskedCode
        {
            get
            {
                return "****-****-" + Code.Substring(10, 4);
            }
        }

        public CUUItem(string code, decimal amount, bool valid)
        {
            Code = code;
            Amount = amount;
            Valid = valid;
        }
    }

}
