﻿using FnacDirect.FnacStore.Business;
using FnacDirect.OrderPipe.Base.Business.Shipping.Shop;
using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.OrderPipe.Base.Model.ClickAndCollect;
using FnacDirect.OrderPipe.Base.Model.LineGroups;
using FnacDirect.OrderPipe.Shipping.Model;
using System;
using System.Collections.Generic;
using System.Linq;

namespace FnacDirect.OrderPipe.Base.Business.Shipping.Rules
{
    public class SteedFromStoreShippingMethodBasketBuildingRule : BaseShippingMethodBasketBuildingRule
    {
        private readonly IShipToStoreAvailabilityService _shipToStoreAvailabilityService;
        private readonly IFnacStoreBusiness _fnacStoreBusiness;

        public SteedFromStoreShippingMethodBasketBuildingRule(IFnacStoreBusiness fnacStoreBusiness, IShipToStoreAvailabilityService shipToStoreAvailabilityService)
        {
            _fnacStoreBusiness = fnacStoreBusiness;
            _shipToStoreAvailabilityService = shipToStoreAvailabilityService;
        }

        public override string SwitchId
        {
            get
            {
                return "orderpipe.pop.steedfromstore";
            }
        }

        public override Func<ShippingMethodBasket, ShippingMethodBasket> GetModifier(IPrivateDataContainer privateDataContainer, IEnumerable<Article> articles, ShippingAddress shippingAddress)
        {
            var steedFromStoreData = privateDataContainer.GetPrivateData<SteedFromStoreData>(false);

            if(steedFromStoreData == null || !steedFromStoreData.StoreId.HasValue)
            {
                return shippingMethodBasket => shippingMethodBasket;
            }

            var logisticTypes = new List<int>();

            foreach(var productId in steedFromStoreData.PridsAvailable)
            {
                var matchingArticle = articles.FirstOrDefault(a => a.ProductID.GetValueOrDefault() == productId);

                if(matchingArticle != null && matchingArticle.LogType.HasValue)
                {
                    logisticTypes.Add(matchingArticle.LogType.GetValueOrDefault());
                }
            }

            var steedFromStoreShippingMethodId = PipeParametersProvider.Get<int>("shippingmethodid.steedfromstore");

            var trigger = new List<ITriggerShadowShippingMethod>()
            {
                new ByLogisticTypeTriggerShadowShippingMethod(steedFromStoreShippingMethodId, logisticTypes.Distinct().ToArray())
            };

            return shippingMethodBasket =>
            {
                shippingMethodBasket.TriggerShadowShippingMethods = trigger;
                return shippingMethodBasket;
            };
        }
    }
}
