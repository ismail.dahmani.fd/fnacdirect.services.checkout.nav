using FnacDirect.AccountManager.Proxy.WebService;
using FnacDirect.Customer.Model;
using FnacDirect.Technical.Framework.CoreServices;
using System;

namespace FnacDirect.OrderPipe.Reporting
{
    /// <summary>
    /// classe déplacé de la solution customer core car elle est utilisé seulement ici et dans Quote.Business
    /// </summary>
    public class SalesManBusiness
    {
        /// <summary>
        /// Renvoie un commercial du compte spécifié
        /// (si un commercial n'est pas encore défini pour le compte spécifié,
        /// on renvoie une valeur nulle)
        /// </summary>
        /// <param name="accountId"></param>
        /// <returns></returns>
        public SalesMan GetSalesMan(int accountId)
        {
            SalesMan salesMan = null;
            FnacUser fnacUser = null;

            fnacUser = GetFnacUserByAccountId(accountId);
            if (fnacUser != null)
                salesMan = CreateSalesMan(fnacUser);
            return salesMan;
        }

        /// <summary>
        /// Renvoie un commercial du compte spécifié
        /// </summary>
        public FnacUser GetFnacUserByAccountId(int accountId)
        {
            FnacUser fnacUser = null;
            IServiceAccountManager service = null;

            try
            {
                service = ServiceAccountManagerFactory.Instance;
                fnacUser = service.GetFnacUserByAccountId(accountId);
            }
            catch (Exception ex)
            {
                Logging.Current.WriteError("Erreur pendant l'accès au service SalesManagerInfo", ex);
            }
            return fnacUser;
        }

        private SalesMan CreateSalesMan(FnacUser fnacUser)
        {
            var salesMan = new SalesMan
            {
                Identifier = fnacUser.Guid,
                ID = fnacUser.Id,
                FirstName = fnacUser.FirstName,
                LastName = fnacUser.LastName,
                Email = fnacUser.Email,
                Phone = fnacUser.Phone,
                Photo = fnacUser.PhotoBytes,
                Login = fnacUser.Login
            };
            return salesMan;
        }
    }
}
