using FnacDirect.OrderPipe.Base.BaseModel.Model.OrderDetails;
using System;
using System.Collections.Generic;

namespace FnacDirect.OrderPipe.Base.Model.ThankYou
{
    public class ThankYouOrderDetailData : GroupData
    {
        public List<ThankYouOrderDetail> ThankYouOrdersDetails { get; set; }

        public bool IsIdentityClash { get; set; }
        public string MainOrderUserReference { get; set; }
        public bool IsKafkaOrderTrackingSent { get; set; }
    }

    public class ThankYouOrderDetail
    {
        public int OrderPk { get; set; }
        public string OrderUserReference { get; set; }
        public Guid OrderReference { get; set; }
        public DateTime OrderDate { get; set; }        
        public List<OrderDetailArticle> Articles { get; set; }
        public OrderDetailBaseAddress ShippingAddress { get; set; }
        public string Seller { get; set; }
        public bool IsFnac { get; set; }
        public bool IsShipToStore { get; set; }
        public bool IsClickAndCollect { get; set; }
        public string ShopFullname { get; set; }
        public GlobalPrices GlobalPrices { get; set; }
        public string ShippingMethod { get; set; }
        public int ShippingMethodId { get; set; }
    }
}
