﻿using FnacDirect.OrderPipe.Base.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FnacDirect.OrderPipe.Base.Business
{
    public interface IBillingMethodsManager
    {
        List<BillingMethodDescriptor> ListRemainingMethods(int remainingMethodMask);
        BillingMethodDescriptor GetBillingMethodDescriptor(int billingMethodId);
    }
}
