using FnacDirect.OrderPipe.Base.DAL;
using FnacDirect.OrderPipe.Base.LineGroups;
using FnacDirect.OrderPipe.Base.Model;
using System.Collections.Generic;
using System.Linq;

namespace FnacDirect.OrderPipe.Base.Business.Stock
{
    public class Stock2DatabaseReservationService : IStockReservationService
    {
        private readonly IStockDal _stockDAL;

        public Stock2DatabaseReservationService(IStockDal stockDAL)
        {
            _stockDAL = stockDAL;
        }

        public void Reserve(IEnumerable<ILogisticLineGroup> logisticLineGroups, string market, int userId)
        {
            foreach (var logisticLineGroup in logisticLineGroups.Where(a => a.IsFnacCom && !a.IsClickAndCollect))
            {
                foreach (var standardArticle in logisticLineGroup.Articles.OfType<StandardArticle>())
                {
                    //TODO: Refacto possible (après avoir fait son TU)
                    if (standardArticle.IsBundle.GetValueOrDefault())
                    {
                        var bundle = standardArticle as Bundle;
                        foreach (var component in bundle.BundleComponents.OfType<StandardArticle>())
                        {
                            if (int.TryParse(component.ReferenceGU, out var refgu))
                            {
                                standardArticle.FlagStock = _stockDAL.TryReserve(
                                    logisticLineGroup.OrderUserReference,
                                    standardArticle.OrderDetailPk.Value,
                                    refgu,
                                    component.Ean13,
                                    standardArticle.Ean13,
                                    component.Quantity.GetValueOrDefault(),
                                    market,
                                    userId,
                                    logisticLineGroup.OrderDate
                                );

                                var artBundlePersist = logisticLineGroup.Articles.GetByPrid<StandardArticle>(standardArticle.ProductID.Value);
                                if (artBundlePersist != null)
                                {
                                    artBundlePersist.FlagStock = standardArticle.FlagStock;
                                }
                            }
                        }
                    }
                    else
                    {
                        if (int.TryParse(standardArticle.ReferenceGU, out var refgu))
                        {
                            standardArticle.FlagStock = _stockDAL.TryReserve(
                                logisticLineGroup.OrderUserReference,
                                standardArticle.OrderDetailPk.Value,
                                refgu,
                                standardArticle.Ean13,
                                null,
                                standardArticle.Quantity.GetValueOrDefault(),
                                market,
                                userId,
                                logisticLineGroup.OrderDate
                            );

                            var artPersist = logisticLineGroup.Articles.GetByPrid<StandardArticle>(standardArticle.ProductID.Value);
                            if (artPersist != null)
                            {
                                artPersist.FlagStock = standardArticle.FlagStock;
                            }
                        }
                    }
                }
            }
        }
    }
}
