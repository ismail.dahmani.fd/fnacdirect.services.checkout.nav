﻿
using FnacDirect.OrderPipe.Base.Model;

namespace FnacDirect.OrderPipe.Base.Business.Shipping.Relay
{
    public interface IRelayAddressService
    {
        /// <summary>
        /// Transform the CountryId and the ShippingZone according to the ZipCode because countries can deliver in several ShippingZone. 
        /// </summary>
        void TransformCountryWithZipCode(RelayAddress address, OPContext opContext);

        /// <summary>
        /// Adjust the address fields for ES-PT because the behavior is not the same as France
        /// </summary>
        void AdjustAddressFields(RelayAddress relayAddress);
    }
}
