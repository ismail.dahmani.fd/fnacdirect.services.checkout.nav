﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Common.Payment
{
    public class FidelityPointsOccazViewModel : BillingMethodViewModel
    {
        public FidelityPointsOccazViewModel()
        {
            Values = Enumerable.Empty<SelectListItem>();
        }

        public bool Active { get; set; }

        public bool HasUniqueExpiration { get; set; }
        public bool ExpireInLessThanOneMonth { get; set; }

        public string Used { get; set; }
        public string Remaining { get; set; }        

        public string Amount { get; set; }
        public string AmountToExpire { get; set; }

        public string Expiration { get; set; }

        public IEnumerable<SelectListItem> Values { get; set; }
    }
}
