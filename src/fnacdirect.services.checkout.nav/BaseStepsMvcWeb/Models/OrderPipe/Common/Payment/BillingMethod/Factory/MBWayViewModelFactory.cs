using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.OrderPipe.Base.Proxy.Customer;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Common.Payment.BillingMethodFactories
{
    public class MBWayViewModelFactory : BasePaymentViewModelFactory<MBWayBillingMethod>
    {
        public override BillingMethodViewModel Get(string tabName, PaymentMethod paymentMethod, OPContext opContext, PopOrderForm popOrderForm)
        {
            var phone = popOrderForm.BillingAddress.CellPhone;

            if(string.IsNullOrEmpty(phone))
            {
                phone = popOrderForm.BillingAddress.Phone;
            }

            return new MBWayBillingMethodViewModel
            {
                Display = true,
                PhoneNumber = phone
            };
        }
    }
}
