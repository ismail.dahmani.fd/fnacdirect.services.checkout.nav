using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using FnacDirect.OrderPipe.Base.BaseModel.Model;
using FnacDirect.OrderPipe.Base.Business.Mailing;
using FnacDirect.OrderPipe.Base.Business;
using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop;
using FnacDirect.Technical.Framework.Utilities;
using FnacDirect.Technical.Framework.Web;
using FnacDirect.Technical.Framework.Web.UrlMgr;
using FnacDirect.OrderPipe.Base.Steps;
using System;
using FnacDirect.OrderPipe.Base.Steps.Mailing;
using System.Xml.Serialization;
using System.IO;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Controllers.Pop
{
    public class PopMailController : PopBaseController
    {
        private readonly OPContext _opContext;
        private readonly PopOrderForm _popOrderForm;

        public PopMailController(OPContext opContext)
        {
            _opContext = opContext;
            _popOrderForm = _opContext.OF as PopOrderForm;

        }

        public ActionResult Index()
        {
            var popMailData = _popOrderForm.GetPrivateData<PopMailData>();

            if (popMailData == null || popMailData.Model == null)
            {
                return Content(string.Empty);
            }

            return View("Index", popMailData.Model);
        }

        [HttpGet]
        public ActionResult Debug()
        {
            var popDebugMailData = _popOrderForm.GetPrivateData<PopDebugMailData>(create: false);

            if (popDebugMailData == null || string.IsNullOrEmpty(popDebugMailData.Model))
            {
                return Content(string.Empty);

            }

            var xmlSerializer = new XmlSerializer(typeof(Order));
            Order model = null;

            using (TextReader textReader = new StringReader(popDebugMailData.Model))
            {
                model = (Order)xmlSerializer.Deserialize(textReader);
            }

            if (model == null)
            {
                return Content(string.Empty);
            }

            return View("Index", model);
        }
    }
}
