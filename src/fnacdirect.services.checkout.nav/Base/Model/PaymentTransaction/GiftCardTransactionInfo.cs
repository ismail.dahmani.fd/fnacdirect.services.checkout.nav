namespace FnacDirect.OrderPipe.Base.Model.FDV
{
    public class GiftCardTransactionInfo
    {
        /// <summary>
        /// Numéro de la carte
        /// </summary>
        public string CardNumber { get; set; }

        /// <summary>
        /// Emetteur
        /// </summary>
        public string IssuerRef { get; set; }

        /// <summary>
        /// Identifiant de l'authorisation
        /// </summary>
        public int AuthorizationId { get; set; }
    }
}
