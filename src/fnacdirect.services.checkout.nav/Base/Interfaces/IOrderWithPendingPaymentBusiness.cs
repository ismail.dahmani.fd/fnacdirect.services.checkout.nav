using FnacDirect.OrderPipe.Base.Model;

namespace FnacDirect.OrderPipe.Base.Business.Quote
{
    public interface IOrderWithPendingPaymentBusiness
    {
        PopOrderForm GetOrderByOrderReference(string orderReference, PopOrderForm popOrderForm);
    }
}
