using System;
using System.Collections.Generic;
using System.Linq;

namespace FnacDirect.OrderPipe.Base.Model.Solex
{
    public class AggregatedShippingChoice : ShippingChoice
    {
        public List<ShippingChoice> ShippingChoices { get; set; } = new List<ShippingChoice>();
        public int? SelectedChoiceMainShippingMethodId { get; set; }

        public AggregatedShippingChoice(int mainShippingMethodId)
        {
            MainShippingMethodId = mainShippingMethodId;
        }

        public AggregatedShippingChoice(AggregatedShippingChoice aggregatedShippingChoice)
        {
            MainShippingMethodId = aggregatedShippingChoice.MainShippingMethodId;
            SelectedChoiceMainShippingMethodId = aggregatedShippingChoice.SelectedChoiceMainShippingMethodId;
            ShippingChoices.AddRange(aggregatedShippingChoice.ShippingChoices.Select(shippingChoice => new ShippingChoice(shippingChoice)));
        }

        public override IEnumerable<ShippingChoice> GetInnerChoices()
        {
            return ShippingChoices;
        }

        public override IDictionary<int, Parcel> Parcels
        {
            get
            {
                return SelectedChoice?.Parcels;
            }
        }

        public override ShippingChoice SelectedChoice
        {
            get
            {
                return ShippingChoices.FirstOrDefault(sc => sc.MainShippingMethodId == SelectedChoiceMainShippingMethodId);
            }
        }

        //Default is the earliest choice, meaning the choice with
        //the min deliverydate in the parcel with shipping method as in its choice mainshipping method
        public override ShippingChoice DefaultChoice
        {
            get
            {
                return ShippingChoices.SelectMany(
                                    choice => choice.Parcels.Where(parcel => parcel.Value.ShippingMethodId == choice.MainShippingMethodId),
                                    (choice, parcel) => new { Choice = choice, Parcel = parcel }).OrderBy(item => item.Parcel.Value.Source.DeliveryDateMin)
                                .Select(item => item.Choice).First();
            }
        }
        public override IEnumerable<Promotion> AppliedPromotions
        {
            get
            {
                return SelectedChoice?.AppliedPromotions;
            }
        }

        public override IEnumerable<Group> Groups
        {
            get
            {
                return SelectedChoice?.Groups ?? Enumerable.Empty<Group>();
            }
        }

        public override GenericPrice TotalPrice
        {
            get
            {
                if (SelectedChoiceMainShippingMethodId.HasValue)
                {
                    return SelectedChoice.TotalPrice;
                }
                return ShippingChoices.Select(choice => choice.TotalPrice).OrderBy(price => price.Cost).First();
            }
        }

        public override Dates GetDates(BaseArticle article)
        {
            if (Groups != null && Parcels != null)
            {
                return base.GetDates(article);
            }
            //calcul de cas minimal
            var deliveryDateMax = DateTime.MaxValue;
            var deliveryDateMin = DateTime.MaxValue;
            var shippingDate = DateTime.MaxValue;

            foreach (var choice in ShippingChoices)
            {
                var parcelId = choice.Groups.SelectMany(g => g.Items).FirstOrDefault(i => i.Identifier.Prid == article.ProductID)?.ParcelId;

                if (!parcelId.HasValue)
                {
                    return null;
                }

                choice.Parcels.TryGetValue(parcelId.Value, out var parcel);

                if (parcel == null)
                {
                    break;
                }

                deliveryDateMax = new DateTime(Math.Min(deliveryDateMax.Ticks, parcel.Source.DeliveryDateMax.Ticks));
                deliveryDateMin = new DateTime(Math.Min(deliveryDateMin.Ticks, parcel.Source.DeliveryDateMin.Ticks));
                shippingDate = new DateTime(Math.Min(shippingDate.Ticks, parcel.Source.ShippingDate.Ticks));
            }

            return new Dates
            {
                DeliveryDateMax = deliveryDateMax,
                DeliveryDateMin = deliveryDateMin,
                ShippingDate = shippingDate
            };
        }

        public override bool IsAvailable
        {
            get
            {
                if (ShippingChoices.Any())
                {
                    if (SelectedChoiceMainShippingMethodId.HasValue)
                    {
                        return SelectedChoice.IsAvailable;
                    }
                    return ShippingChoices.Any(choice => choice.IsAvailable);
                }
                return false;
            }
        }

        public override bool ContainsShippingMethodId(int shippingMethodId)
        {
            return MainShippingMethodId == shippingMethodId || ShippingChoices.Any(choice => choice.ContainsShippingMethodId(shippingMethodId));
        }

        public override ShippingChoice GetChoice(int shippingMethodId)
        {
            return ShippingChoices.FirstOrDefault(choice => choice.MainShippingMethodId == shippingMethodId);
        }

        public override ShippingChoice GetChoice()
        {
            return ShippingChoices.First();
        }

        public override ShippingChoice Copy()
        {
            return new AggregatedShippingChoice(this);
        }
    }
}
