using FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Membership;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Common.Basket
{
    public class FnacPlusViewModel : IGotFnacPlusData
    {
        public bool ShowPush { get; set; }

        public bool ShowPopin { get; set; }

        public bool IsFullMP { get; set; }

        public bool IsFnacSeller {get ;set;}

        public bool CanAddCardFromPopin { get; set; }

        public bool IsFnacPlusEnabled { get; set; }

        public bool IsFnacPlusCanalPlayEnabled { get; set; }

        public bool IsEligibleEssaiFnacPlus { get; set; }

        public bool IsEligiblePushRecruit { get; set; }

        public bool IsEligiblePushRenouvAdh { get; set; }

        public bool IsEligiblePushRenouvFnacPlus { get; set; }

        public bool IsEligiblePushConfirm { get; set; }

        public bool HasAdherentCardInBasket { get; set; }

        public bool HasFnacPlusCardInBasket { get; set; }

        public MembershipPushCardViewModel CardPlusInfos { get; set; }

        public string FnacPlusImmediateDiscount { get; set; }

        public string FnacPlusDifferedDiscount { get; set; }

        public bool HasFnacPlusDiscount { get; set; }

        public bool HasFnacPlusImmediateDiscount { get; set; }

        public bool HasFnacPlusDifferedDiscount { get; set; }

        public bool IsSteedFromStoreAvailable { get; set; }

    }
}
