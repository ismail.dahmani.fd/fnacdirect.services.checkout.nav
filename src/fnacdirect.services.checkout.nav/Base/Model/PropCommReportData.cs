using System;
using System.Collections.Generic;

namespace FnacDirect.OrderPipe.FDV.Model
{
    /// <summary>
    /// Renferme la totalité des données à imprimer sur la proposition commerciale
    /// </summary>
    [Serializable]
    public class PropCommReportData
    {
        /// <summary>
        /// Formulaire principal
        /// </summary>
        public PropCommForm ReportForm
        {
            get;
            set;
        }

        private List<PropCommArticle> _propCommArticles = null;
        /// <summary>
        /// Lignes des articles 
        /// </summary>
        public List<PropCommArticle> PropCommArticles
        {
            get
            {
                if (_propCommArticles == null)
                {
                    _propCommArticles = new List<PropCommArticle>();
                }

                return _propCommArticles;
            }
            set
            {
                _propCommArticles = value;
            }
        }
    }
}
