using System;

namespace FnacDirect.OrderPipe.Base.Model
{
    public class SipsBillingMethod : BillingMethod
    {
        public const int IdBillingMethod = 1;

        public override bool IsPrimary
        {
            get
            {
                return true;
            }
        }

        public DateTime TransactionDate { get; set; }

        public int TransactionId { get; set; }

        public string PaymentMean { get; set; }

        public SipsBillingMethod()
        {
            BillingMethodId = IdBillingMethod;
            CanModifyAmountUp = true;
            CanModifyAmountDown = true;
            ImplyValidateOrder = true;
        }
    }
}
