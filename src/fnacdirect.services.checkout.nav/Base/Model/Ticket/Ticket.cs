﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace FnacDirect.OrderPipe.FDV.Model
{
    [Serializable]
    [XmlInclude(typeof(TGVTicket))]
    public abstract class Ticket
    {
        public Ticket() { }

        private string _TicketKey;

        public string TicketKey
        {
            get { return _TicketKey; }
            set { _TicketKey = value; }
        }
    }
}
