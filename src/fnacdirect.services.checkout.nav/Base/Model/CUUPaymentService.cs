using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.OrderPipe.Base.Model.Facets;
using System.Linq;

namespace FnacDirect.OrderPipe.Base.Business.Payment
{
    public class CUUPaymentService : ICUUPaymentService
    {
        private readonly IPaymentEligibilityService _paymentEligibilityService;
        private readonly ICheckArticleService _checkArticleService;
        private readonly ISwitchProvider _switchProvider;

        public CUUPaymentService(ICheckArticleService checkArticleService,
            ISwitchProvider switchProvider,
            IPaymentEligibilityService paymentEligibilityService)
        {
            _checkArticleService = checkArticleService;
            _switchProvider = switchProvider;
            _paymentEligibilityService = paymentEligibilityService;
        }

        public bool IsAllowed(IGotLineGroups iGotLineGroups, bool disableClickAndCollectCheck = false, bool disableEccvCheck = false)
        {
            var rules = _paymentEligibilityService.GetRules("orderpipe.pop.payment.cuu.eligibilityrules",
                disableClickAndCollectCheck: disableClickAndCollectCheck,
                disableEccvCheck: disableEccvCheck);

            if (!_paymentEligibilityService.IsAllowed(iGotLineGroups, rules))
            {
                return false;
            }

            if (iGotLineGroups.LineGroups.GetArticlesOfType<MarketPlaceArticle>().Any()
                && _switchProvider.IsEnabled("orderpipe.pop.payment.filtersecondaryformarketplace"))
            {
                return false;
            }

            if (_checkArticleService.HasSensibleSupportId(iGotLineGroups.Articles))
            {
                return false;
            }

            if (iGotLineGroups.HasNumericalArticle())
            {
                return true;
            }

            if (iGotLineGroups.HasFnacComArticle() || disableClickAndCollectCheck)
            {
                return true;
            }

            return false;
        }
    }
}
