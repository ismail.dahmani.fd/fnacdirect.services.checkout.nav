﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Common
{
    public class FooterViewModel
    {
        public bool HasFnac { get; set; }
        public bool HasMarketPlace { get; set; }
    }
}
