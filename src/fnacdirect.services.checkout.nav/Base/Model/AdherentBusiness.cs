using System;
using FnacDirect.OrderPipe.Base.BaseDAL;

namespace FnacDirect.OrderPipe.Base.Business
{
    public class AdherentBusiness : IAdherentBusiness
    {
        private readonly IAdherentDAL _adherentDAL;

        public AdherentBusiness(IAdherentDAL adherentDAL)
        {
            _adherentDAL = adherentDAL;
        }

        public bool IsAdherentHolderWithFnacCard(string adherentNumber, DateTime adherentBirthdate)
        {
            return _adherentDAL.IsAdherentHolderWithFnacCard(adherentNumber, adherentBirthdate);
        }
    }
}
