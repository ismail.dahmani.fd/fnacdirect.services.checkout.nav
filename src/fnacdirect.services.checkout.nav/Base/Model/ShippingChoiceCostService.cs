using FnacDirect.OrderPipe.Base.Model.Solex;
using FnacDirect.OrderPipe.Core.Services;
using System.Collections.Generic;
using System.Linq;

namespace FnacDirect.OrderPipe.Base.Business.Shipping
{
    public class ShippingChoiceCostService : IShippingChoiceCostService
    {
        private readonly ISwitchProvider _switchProvider;
        private readonly IPipeParametersProvider _pipeParametersProvider;


        public ShippingChoiceCostService(ISwitchProvider switchProvider, IPipeParametersProvider pipeParametersProvider)
        {
            _switchProvider = switchProvider;
            _pipeParametersProvider = pipeParametersProvider;
        }

        public decimal CalculateShippingCost(IEnumerable<ShippingChoice> shippingChoices, int? selectedShippingMethodId)
        {
            var taxCalculationEnabled = !_pipeParametersProvider.Get("taxcalculationservice.forcelegacy", false)
                && _switchProvider.IsEnabled("orderpipe.pop.taxcalculationservice.activation");

            var costToApply = 0m;

            if (!shippingChoices.Any())
            {
                return costToApply;
            }

            if (selectedShippingMethodId.HasValue && shippingChoices.Any(c => c.MainShippingMethodId == selectedShippingMethodId.Value))
            {
                var selectedChoice = shippingChoices.First(c => c.MainShippingMethodId == selectedShippingMethodId.Value);
                costToApply = taxCalculationEnabled ? selectedChoice.TotalPrice.CostToApply.GetValueOrDefault()
                                                    : selectedChoice.TotalPrice.Cost;
            }
            else
            {
                var firstChoicePrice = shippingChoices.First().TotalPrice;
                costToApply = taxCalculationEnabled ? firstChoicePrice.CostToApply.GetValueOrDefault()
                                                    : firstChoicePrice.Cost;
            }

            return costToApply;
        }
    }
}
