using FnacDirect.Contracts.Online.Model;
using FnacDirect.OrderPipe.Base.Business.OrderDetail;
using System.Collections.Generic;

namespace FnacDirect.OrderPipe.Base.Business.Mailing
{
    public class OrderDetail
    {        
        public string OrderReference { get; set; }

        public int OrderId { get; set; }

        public string SellerName { get; set; }

        public bool SellerIsFnac { get; set; }

        public bool IsMarketPlace { get; set; }

        public string ShippingCost { get; set; }          

        public List<Business.OrderDetail.Article> Articles { get; set; }                        
    }   
}
