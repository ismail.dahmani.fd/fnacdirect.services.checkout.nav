using System;

namespace FnacDirect.OrderPipe
{
    /// <summary>
    /// Stockage des donn�es s�rialis�e de l'OrderForm
    /// </summary>
    public interface IOFContainer
    {
        /// <summary>
        /// Lecture de l'orderform
        /// </summary>
        /// <param name="sid">Identifiant de l'OrderForm</param>
        /// <param name="pipeName">Nom du pipe</param>
        /// <returns>L'orderform s�rialis�</returns>
        string ReadOF(string sid, string pipeName);

        /// <summary>
        /// Ecriture de l'orderform
        /// </summary>
        /// <param name="sid">Identifiant de l'orderform</param>
        /// <param name="xmlOF">L'orderform s�rialis�</param>
        /// <param name="pipeName">Nom du pipe</param>
        void WriteOF(string sid, string xmlOF, string pipeName);
    }
}
