using System;
using System.Linq;
using FnacDirect.Technical.Framework.Configuration;
using System.Text.RegularExpressions;

namespace FnacDirect.OrderPipe.Base.Business.Configuration
{
    public class SolexConfigurationProvider : ISolexConfigurationProvider
    {
        private readonly SolexConfiguration _solexConfiguration;
        public SolexConfigurationProvider(IConfigurationProvider configurationProvider, ISwitchProvider switchProvider)
        {
            _solexConfiguration = configurationProvider.GetAs<SolexConfiguration>("solex");
            _solexConfiguration.ShippingMethodsAccountFiltersAsRegex = _solexConfiguration.ShippingMethodsAccountFilters.ToDictionary(x => int.Parse(x.Key), x => new Regex(x.Value, RegexOptions.Compiled));
        }

        public SolexConfiguration Get()
        {
            return _solexConfiguration;
        }
    }
}
