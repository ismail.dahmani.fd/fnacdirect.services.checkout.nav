using FnacDirect.OrderPipe.Base.Business;
using FnacDirect.OrderPipe.Base.Business.Operations;
using FnacDirect.OrderPipe.Base.LineGroups;
using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.OrderPipe.Base.Model.OptinWarranty;
using FnacDirect.OrderPipe.Base.Model.Pop;
using FnacDirect.OrderPipe.Base.Proxy.Membership;
using FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Common.Basket;
using FnacDirect.OrderPipe.BaseMvc.Web.Pop.Models.Common;
using FnacDirect.Technical.Framework.Web.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Basket
{
    public class BasketViewModelBuilder : IBasketViewModelBuilder
    {
        private readonly IMembershipDateBusiness _dateBusiness;
        private readonly IApplicationContext _applicationContext;
        private readonly IMembershipConfigurationService _membershipConfigurationService;
        private readonly IOrderExternalReferenceViewModelBuilder _orderExternalReferenceViewModelBuilder;
        private readonly int _tryCardPrid;
        private readonly IDeliveredForChristmasService _deliveredForChristmasService;
        private readonly IOptinWarrantyViewModelBuilder _optinWarrantyViewModelBuilder;
        private readonly IBasketSellerViewModelBuilder _basketSellerViewModelBuilder;
        private readonly IRemindersViewModelBuilder _remindersViewModelBuilder;

        public BasketViewModelBuilder(IApplicationContext applicationContext,
                                          IMembershipConfigurationService membershipConfigurationService,
                                          IMembershipDateBusiness dateBusiness,
                                          IOrderExternalReferenceViewModelBuilder orderExternalReferenceViewModelBuilder,
                                          IDeliveredForChristmasService deliveredForChristmasService,
                                          IOptinWarrantyViewModelBuilder optinWarrantyViewModelBuilder,
                                          IBasketSellerViewModelBuilder basketSellerViewModelBuilder,
                                          IRemindersViewModelBuilder remindersViewModelBuilder)
        {
            _applicationContext = applicationContext;
            _membershipConfigurationService = membershipConfigurationService;
            _tryCardPrid = membershipConfigurationService.GetTryCardPrid();
            _dateBusiness = dateBusiness;
            _orderExternalReferenceViewModelBuilder = orderExternalReferenceViewModelBuilder;
            _deliveredForChristmasService = deliveredForChristmasService;
            _optinWarrantyViewModelBuilder = optinWarrantyViewModelBuilder;
            _basketSellerViewModelBuilder = basketSellerViewModelBuilder;
            _remindersViewModelBuilder = remindersViewModelBuilder;
        }

        protected virtual BasketViewModel BuildViewModel()
        {
            return new BasketViewModel();
        }

        public virtual Maybe<BasketViewModel> Build(PopOrderForm popOrderForm)
        {
            var basketViewModel = BuildViewModel();
            var maxAccountData = popOrderForm.GetPrivateData<PopAccountMaximumData>(create: false);

            var totalArticleCount = popOrderForm.LineGroups.SelectMany(l => l.Articles).Count();
            var deletedArticlesCount = popOrderForm.DeletedArticles.Count;

            if (totalArticleCount == 0 && deletedArticlesCount == 0 || (maxAccountData != null && maxAccountData.HasNoArticles))
            {
                return Maybe<BasketViewModel>.Empty();
            }

            var sellers = popOrderForm.LineGroups.Sellers().ToList();
            if (deletedArticlesCount > 0)
            {
                //Dans le cas des articles indispo on ajoute s'ils n'y sont pas déjà,
                //l'id des vendeurs pour générer correctement les vues modèles.
                var stdArticles = popOrderForm.DeletedArticles.OfType<StandardArticle>();
                if (stdArticles.Any() && !sellers.Exists(sel => sel == SellerIds.GetId(SellerType.Fnac)))
                {
                    sellers.Add(SellerIds.GetId(SellerType.Fnac));
                }

                var mpArticles = popOrderForm.DeletedArticles.OfType<MarketPlaceArticle>();
                foreach (var mpArt in mpArticles)
                {
                    if (mpArt.Offer != null
                        && mpArt.Offer.Seller != null
                        && !sellers.Contains(mpArt.Offer.Seller.SellerId))
                    {
                        sellers.Add(mpArt.Offer.Seller.SellerId);
                    }
                }
            }

            var shouldDisplaySellerInfo = false;
            var shouldDisplayIndex = false;

            //Maybe we can extract the if inside and use 'Any'
            foreach (var seller in sellers)
            {
                var lineGroups = popOrderForm.LineGroups.Seller(seller);
                var articles = lineGroups.Seller(seller).SelectMany(l => l.Articles).OfType<BaseArticle>().ToList();
                var maxAccountPrids = new List<int>();
                if (maxAccountData != null
                    && maxAccountData.MaxAccountDeletedArticles != null
                    && maxAccountData.MaxAccountDeletedArticles.Count > 0)
                {
                    maxAccountPrids = maxAccountData.MaxAccountDeletedArticles;
                }

                if (seller == SellerIds.GetId(SellerType.Fnac))
                {
                    var stdArticles = popOrderForm.DeletedArticles.OfType<StandardArticle>().ToList();
                    articles.AddRange(stdArticles);
                }
                else
                {
                    var mpArticles = popOrderForm.DeletedArticles.OfType<MarketPlaceArticle>().Where(art => art.Offer.Seller.SellerId == seller).ToList();
                    articles.AddRange(mpArticles);
                }

                var filteredArticles = articles.Where(a => !maxAccountPrids.Contains(a.ProductID.Value)).ToList();

                var basketSellerViewModel = _basketSellerViewModelBuilder.Build(popOrderForm, seller, filteredArticles);

                if (basketSellerViewModel.Informations is MarketPlaceSellerInformationsViewModel
                    && !shouldDisplaySellerInfo)
                {
                    shouldDisplaySellerInfo = true;
                }

                basketViewModel.WithSeller(basketSellerViewModel);
            }

            var isOnlyOneSellerWithAvailableArticles = basketViewModel.Sellers.Count(s => s.Items.Any(art => art.IsAvailable)) > 1;
            if (isOnlyOneSellerWithAvailableArticles)
            {
                shouldDisplaySellerInfo = true;
                shouldDisplayIndex = true;
            }
            else //Several sellers
            {
                shouldDisplayIndex = false;

                var isAnySellerFnacWithAvailableArticles = basketViewModel.Sellers.Any(s => s.Items.Any(art => art.IsAvailable) && s.Informations.IsFnac);
                if (isAnySellerFnacWithAvailableArticles)
                {
                    shouldDisplaySellerInfo = false;
                }
                else
                {
                    shouldDisplaySellerInfo = true;
                }
            }

            foreach (var basketSellerViewModel in basketViewModel.Sellers)
            {
                var isSellerWithoutAvailableArticle = !basketSellerViewModel.Items.Any(art => art.IsAvailable);
                if (isSellerWithoutAvailableArticle)
                {
                    basketSellerViewModel.ShouldDisplaySellerInfo = false;
                    basketSellerViewModel.Informations.ShouldDisplayIndex = false;
                }
                else //use what is set above
                {
                    basketSellerViewModel.ShouldDisplaySellerInfo = shouldDisplaySellerInfo;
                    basketSellerViewModel.Informations.ShouldDisplayIndex = shouldDisplayIndex;
                }
            }

            var popData = popOrderForm.GetPrivateData<PopData>(create: false);
            var hasUnavailableArticles = basketViewModel.Sellers.SelectMany(s => s.Items).Any(i => !i.IsAvailable);

            basketViewModel.DispayUnavailableOrLimitedQuantityWarning = (popData != null && popData.HasLimitedQuantity) || hasUnavailableArticles;

            basketViewModel.HasAdherentCard = popOrderForm.LineGroups.GetArticles().OfType<StandardArticle>().Any(art => art.IsAdhesionCard);

            basketViewModel.HasTryAdherentCard = popOrderForm.LineGroups.GetArticles().OfType<StandardArticle>().Any(art => art.ProductID == _tryCardPrid);

            basketViewModel.HasFnacPlusCard = popOrderForm.LineGroups.GetArticles().OfType<StandardArticle>().Any(art =>
                _membershipConfigurationService.FnacPlusCardTypes.Contains(art.TypeId.GetValueOrDefault()));

            basketViewModel.HasFnacPlusTryCard = popOrderForm.LineGroups.GetArticles().OfType<StandardArticle>().Any(art =>
                art.TypeId.GetValueOrDefault() == _membershipConfigurationService.FnacPlusTryCardType);

            if (basketViewModel.HasFnacPlusTryCard)
            {
                basketViewModel.FnacPlusTryCardDuration = HtmlHelpers.Pop.Membership.AdherentCardHelper.GetFnacPlusTryCardDuration(null);
            }

            basketViewModel.DisplayBirthDatePopin = _dateBusiness.DisplayBirthDatePopin(popOrderForm);
            basketViewModel.OrderExternalReference = _orderExternalReferenceViewModelBuilder.Build(popOrderForm);

            var standardArticles = popOrderForm.LineGroups.GetArticlesOfType<StandardArticle>();

            BuildOptinWarranty(basketViewModel, popOrderForm);

            basketViewModel.HasInsurance = standardArticles.Any(a => a.Services.Any(s => s.TypeId.HasValue && s.TypeId.Value == Constants.InsuranceType));

            var christmasWordingKey = _deliveredForChristmasService.GetWordingWithAvailibility(standardArticles, TypeWording.Basket);
            basketViewModel.ChristmasWording = _applicationContext.GetMessage(christmasWordingKey);

            basketViewModel.Reminders = _remindersViewModelBuilder.Build(popOrderForm);
            return basketViewModel;
        }

        /// <summary>
        /// Remplir l'objet OptinWarranty.
        /// </summary>
        internal void BuildOptinWarranty(BasketViewModel basketViewModel, PopOrderForm popOrderForm)
        {
            var optinWarrantyData = popOrderForm.GetPrivateData<OptinWarrantyData>(create: false);
            if (optinWarrantyData == null)
            {
                return;
            }

            var hasServiceInBasket = basketViewModel.Sellers.SelectMany(seller => seller.Items).Any(item => item.Services.Any());
            if (hasServiceInBasket)
            {
                return;
            }

            var articleswithWarrantyLinked = basketViewModel.Sellers.SelectMany(seller => seller.Items).Where(item => item.Children.Any());
            if (articleswithWarrantyLinked.Any())
            {
                foreach (var item in basketViewModel.Sellers.SelectMany(seller => seller.Items))
                {
                    if (articleswithWarrantyLinked.Any(a => a.ProductId == item.ProductId))
                    {
                        item.OptinWarranty = _optinWarrantyViewModelBuilder.Build(popOrderForm, item.ProductId);
                    }
                }
            }
        }

    }
}
