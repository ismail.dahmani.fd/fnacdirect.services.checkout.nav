using FnacDirect.OrderPipe.Core.Model.Config;
using System;
using System.Xml.Serialization;

namespace FnacDirect.OrderPipe.Config
{
	public abstract class UIDescriptor : IUiDescriptor
	{
		public virtual void Init(IInteractiveStep step, IOP pipe, InteractiveStep cs, Pipe cp)
		{
            if (ForceRedirectOnContinue && step != null)
            {
                step.ExitOnContinue = true;                
            }
		}

        public virtual T GetRedirect<T>()
            where T : class
        {
            throw new NotImplementedException();
        }

	    public string GetRedirect()
	    {
            return GetRedirect<string>();
	    }

        public string Label { get; set; }

        public string LabelId { get; set; }

        [XmlAttribute("forceredirectoncontinue")]
        public bool ForceRedirectOnContinue { get; set; }

        [XmlAttribute("redirectmode")]
        public RedirectMode RedirectMode { get; set; }
    }
}