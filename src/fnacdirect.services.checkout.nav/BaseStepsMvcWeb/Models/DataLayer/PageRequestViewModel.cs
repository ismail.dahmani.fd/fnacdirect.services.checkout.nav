namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.DataLayer
{
    public class PageRequestViewModel
    {
        public string PipeName { get; set; }
        public bool IsMobile { get; set; }
        public string CallingPlatform { get; set; }
        public PageTypeEnum PageType { get; set; }
    }
}
