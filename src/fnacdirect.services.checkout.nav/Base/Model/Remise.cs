using System;
using System.Xml.Serialization;

namespace FnacDirect.OrderPipe.Base.Model
{
    [Serializable]
    public class Remise : Promotion
    {
        [XmlIgnore]
        private int? _DiscountReason;

        public int? DiscountReason
        {
            get
            {
                return _DiscountReason;
            }
            set
            {
                _DiscountReason = value;
            }
        }

        [XmlIgnore]
        private decimal? _PriceDiscount;

        public decimal? PriceDiscount
        {
            get
            {
                return _PriceDiscount;
            }
            set
            {
                _PriceDiscount = value;
            }
        }
    }
}
