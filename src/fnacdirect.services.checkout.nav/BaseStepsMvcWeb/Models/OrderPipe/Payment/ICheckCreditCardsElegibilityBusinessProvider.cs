﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FnacDirect.OrderPipe.Base.Business.Payment
{
    public interface ICheckCreditCardsEligibilityBusinessProvider
    {
        ICheckCreditCardsEligibilityBusiness GetCheckCreditCardsEligibilityBusinessInstance(OPContext opContext);
    }
}
