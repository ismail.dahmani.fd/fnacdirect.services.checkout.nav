using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.OrderPipe.Base.DAL;
using FnacDirect.Technical.Framework.Caching2;

namespace FnacDirect.OrderPipe.Base.Business
{
    public interface IItemService
    {
        void GetItemInfos(StandardArticle standardArticle);
        void GetItemInfos(Model.VirtualGiftCheck virtualGiftCheck);
    }

    public class ItemBusiness : IItemService
    {
        private readonly ICacheService _cacheService;
        private readonly ICatalogDAL _catalogDal;

        public ItemBusiness(ICatalogDAL catalogDal, ICacheService cacheService)
        {
            _catalogDal = catalogDal;
            _cacheService = cacheService;
        }
        public void GetItemInfos(StandardArticle art)
        {
            BaseGetItemInfos(art);
        }

        public void GetItemInfos(Model.VirtualGiftCheck art)
        {
            BaseGetItemInfos(art);
        }

        private void BaseGetItemInfos(Article art)
        {
            var productId = art is StandardArticle ? art.ProductID.Value : art.ProductID.GetValueOrDefault(-1);
            var itemInfoResult = _cacheService.Get(new CacheKey("ArticleService.Article.ItemInfos", productId), () =>
            {
                return _catalogDal.GetItemInfos(productId);
            });

            art.UniverseInfo = new UniverseInfo { UniverseId = itemInfoResult.AccountUniverseId };
            art.DistributorAvailabilityTypeId = itemInfoResult.DistributorAvailabilityTypeId;
        }
    }
}
