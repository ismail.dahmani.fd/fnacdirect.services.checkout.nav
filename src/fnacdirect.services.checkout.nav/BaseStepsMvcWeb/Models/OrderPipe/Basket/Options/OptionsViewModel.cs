﻿
using FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Common.Basket;
namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Basket
{
    public class OptionsViewModel
    {
        public ExpressPlusViewModel ExpressPlusInfo { get; set; }

        public SplitViewModel Split { get; set; }

        public WrapViewModel Wrap { get; set; }

        public Common.Payment.PromotionViewModel Promotion { get; set; }

        public HideBillingInfoViewModel HideBillingInfo { get; set; }
    }
}
