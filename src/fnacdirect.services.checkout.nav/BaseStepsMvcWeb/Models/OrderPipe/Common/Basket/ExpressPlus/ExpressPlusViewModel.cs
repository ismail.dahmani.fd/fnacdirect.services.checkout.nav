﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Common.Basket
{
    public class ExpressPlusViewModel
    {
        public bool IsTrial { get; set; }
        public bool IsAdh { get; set; }
        public bool IsAdhOne { get; set; }
        public string UrlCGV { get; set; }
        public string PeriodTitle { get; set; }
        public string PriceSubtitle { get; set; }
        public string AdherentSubtitle { get; set; }
        public bool FromShipping { get; set; }
        public bool ShowInShipping { get; set; }
        public bool HasExpressPlus { get; set; }
        public bool ShowPopinButton { get; set; }
        public string YearlyPrice { get; set; }
    }
}
