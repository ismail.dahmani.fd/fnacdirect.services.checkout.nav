﻿using FnacDirect.OrderPipe.Base.Business.LogisticLineGroups.Handlers;
using StructureMap;
using System;

namespace FnacDirect.OrderPipe.Base.Business.LogisticLineGroups.Internals.Rules
{
    public class StructureMapLogisticLineGroupGenerationRuleFactory : ILogisticLineGroupGenerationRuleFactory
    {
        private readonly Func<StructureMapLogisticLineGroupGenerationRule> _structureMapLogisticLineGroupGenerationRuleFactory;
        private readonly IContainer _container;

        public StructureMapLogisticLineGroupGenerationRuleFactory(Func<StructureMapLogisticLineGroupGenerationRule> structureMapLogisticLineGroupGenerationRuleFactory, IContainer container)
        {
            _structureMapLogisticLineGroupGenerationRuleFactory = structureMapLogisticLineGroupGenerationRuleFactory;
            _container = container;
        }

        public ILogisticLineGroupGenerationRule Get(ILogisticLineGroupGenerationRuleDescriptor logisticLineGroupGenerationRuleDescriptor)
        {
            var structureMapLogisticLineGroupGenerationRule = _structureMapLogisticLineGroupGenerationRuleFactory();

            structureMapLogisticLineGroupGenerationRule.Container = _container;
            structureMapLogisticLineGroupGenerationRule.LogisticLineGroupGenerationRuleDescriptor = logisticLineGroupGenerationRuleDescriptor;

            return structureMapLogisticLineGroupGenerationRule;
        }
    }
}
