using FnacDirect.OfferService.IBusiness;
using FnacDirect.Technical.Framework.CoreServices;
using FnacDirect.Technical.Framework.ServiceLocation;
using FnacDirect.Technical.Framework.Web.Mvc;

namespace FnacDirect.OrderPipe.Base.Business.OneClick
{
    public class OfferBusinessProvider : IOfferBusinessProvider
    {
        public OfferBusinessProvider(IOfferBusiness offerBusiness)
        {
            _offerBusiness = offerBusiness; 
        }
        public IOfferBusiness _offerBusiness;

        public IOfferBusiness GetOfferBusiness(IApplicationContext applicationContext)
        {
            _offerBusiness.SetSiteContext(applicationContext.GetSiteContext());
            _offerBusiness.SetPath(System.Reflection.Assembly.GetExecutingAssembly().Location);
            return _offerBusiness;
        }
    }
}
