using FnacDirect.OrderPipe.Base.Business;
using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.OrderPipe.Base.Model.ThankYou;
using FnacDirect.OrderPipe.Base.Model.Tracking;
using FnacDirect.OrderPipe.BaseMvc.Web.Controllers.Pop;
using FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.DataLayer;
using FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Omniture;
using FnacDirect.OrderPipe.BaseMvc.Web.Models.Tracking;
using FnacDirect.OrderPipe.BaseMvc.Web.Pop.Models.SelfCheckout;
using FnacDirect.OrderPipe.BaseMvc.Web.Services.Pop.DataLayer;
using FnacDirect.OrderPipe.BaseMvc.Web.Services.Tracking;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Pop.Controllers.SelfCheckout
{
    public class SelfCheckoutOrderThankYouController : PopBaseController
    {
        private readonly ISelfCheckoutOrderThankYouModelBuilder _selfCheckoutOrderThankYouModelBuilder;
        private readonly IOmnitureViewModelBuilder _omnitureViewModelBuilder;
        private readonly ISelfCheckoutService _selfCheckoutService;
        private readonly IDataLayerService _dataLayerService;
        private readonly ITrackingService _trackingService;

        public SelfCheckoutOrderThankYouController(
                                        ISelfCheckoutOrderThankYouModelBuilder selfCheckoutOrderThankYouModelBuilder,
                                        IOmnitureViewModelBuilder omnitureViewModelBuilder,
                                        ISelfCheckoutService selfCheckoutService,
                                        IDataLayerService dataLayerService,
                                        ITrackingService trackingService)
        {
            _selfCheckoutOrderThankYouModelBuilder = selfCheckoutOrderThankYouModelBuilder;
            _omnitureViewModelBuilder = omnitureViewModelBuilder;
            _selfCheckoutService = selfCheckoutService;
            _dataLayerService = dataLayerService;
            _trackingService = trackingService;

        }

        [HttpGet]
        public ViewResult Index()
        {
            var opContext = DependencyResolver.Current.GetService<OPContext>();
            var popOrderForm = opContext.OF as PopOrderForm;

            var model = new RootViewModel { SelfCheckoutOrderThankYouViewModel = _selfCheckoutOrderThankYouModelBuilder.Build(popOrderForm) };

            // TODO remove it after DataLayer is definitively used:
            model.Tracking = _omnitureViewModelBuilder.BuildTracking(popOrderForm, TrackedPage.scoOrderThankYou, model.SelfCheckoutOrderThankYouViewModel.Total);

            var datalayerViewModel = new DatalayerViewModel()
            {
                PageType = PageTypeEnum.ScoOrderThankYou,
                PopOrderForm = popOrderForm,
            };

            var viewResult = View(model);
            _dataLayerService.ComputeTransaction(datalayerViewModel, viewResult as ViewResultBase, ControllerContext);

            var trackingModels = new List<TrackingArticleModel>();
            var thankYouOrderDetailData = popOrderForm.GetPrivateData<ThankYouOrderDetailData>(false);
            foreach (var orderDetail in thankYouOrderDetailData.ThankYouOrdersDetails)
            {
                trackingModels.AddRange(orderDetail.Articles.Select(a => new TrackingArticleModel()
                {
                    Offer = a.OfferRef.ToStringOrDefault(),
                    Prid = a.Prid,
                    Quantity = a.Quantity
                }).ToList());
            }

            _trackingService.TrackArticles(popOrderForm, trackingModels, EventTracking.Order);

            return viewResult;
        }
        
        public ActionResult BuildTicketTemplate()
        {
            // Utilisation du GetOPContext() car cette action est utilisé par un render qui n'accepte pas les paramêtres. 
            var popOrderForm = GetOpContext().OF as PopOrderForm;

            var model = _selfCheckoutService.CreateShopTicketFromSelfCheckoutOrderForm(popOrderForm);

            return View("ShopTicket", model);
        }
    }
}
