﻿using FnacDirect.OrderPipe.Config;
using FnacDirect.OrderPipe.Core.Services;

namespace FnacDirect.OrderPipe.Base.Business.Shipping
{
    public interface IShippingMethodBasketServiceProvider
    {
        IShippingMethodBasketService Get(IPipeParametersProvider pipeParametersProvider);
    }
}
