﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Globalization;
using System.Text.RegularExpressions;

namespace FnacDirect.OrderPipe
{
    public static class StringUtils
    {
        public static StringBuilder ToStringBuilder(this string str)
        {
            var stringBuilder = new StringBuilder();

            if(!string.IsNullOrEmpty(str))
            {
                stringBuilder.Append(str);
            }

            return stringBuilder;
        }

        public static string Truncate(string source, int length)
        {
            if (source == null)
                return null;

            if (source.Length > length)
            {
                source = source.Substring(0, length);
            }
            return source;
        }

        public static string RemoveDiacritics(string inputString, bool fullReplace = true)
        {
            inputString = ReplaceBadChar(inputString, fullReplace);

            if (string.IsNullOrEmpty(inputString))
                return inputString;

            var normalizedString = inputString.Normalize(NormalizationForm.FormD);
            var stringBuilder = new StringBuilder();
            for (var i = 0; i < normalizedString.Length; i++)
            {
                var c = normalizedString[i];
                if (CharUnicodeInfo.GetUnicodeCategory(c) != UnicodeCategory.NonSpacingMark)
                    stringBuilder.Append(c);
            }
            return stringBuilder.ToString();
        }

        public static string RemoveDiaCriticsAndNormalize(string inputString, string replacementChar="")
        {
            if(string.IsNullOrEmpty(inputString))
            {
                return string.Empty;
            }

            var withoutDiacritics = RemoveDiacritics(inputString);

            var reg = new Regex(@"[^a-zA-Z0-9' ()-]", RegexOptions.Compiled);

            return reg.Replace(withoutDiacritics, replacementChar);
        }

        /// <summary>
        /// Convertit le paramètre en une liste.
        /// </summary>
        /// <typeparam name="T">Type valeur vers lequel convertir les éléments contenus dans le paramètre.</typeparam>
        /// <param name="parameter">Représente une ou plusieurs valeurs généralement stockée dans la configuration.
        /// Exemple : 10-95-65-253-1-87
        /// </param>
        /// <returns>Retourne une liste des valeurs du paramètre.</returns>
        public static List<T> ConvertParameterToList<T>(string parameter)
        {
            var l_Values = new List<T>();

            Func<string, T> l_Conversion = delegate(string p_ValueToConvert)
                {
                    T l_Value;
                    try
                    {
                        l_Value = (T)Convert.ChangeType(p_ValueToConvert, typeof(T), CultureInfo.InvariantCulture);
                    }
                    catch
                    {
                        l_Value = default(T);
                    }
                    return l_Value;
                };

            if (!string.IsNullOrEmpty(parameter))
                l_Values = parameter.Split(new char[] { ' ', '-', ',', ';' }, StringSplitOptions.RemoveEmptyEntries)
                            .Select(l_Conversion).ToList();

            return l_Values;
        }


        public static string ReplaceBadChar(string value, bool fullReplace = true)
        {
            if (value != null)
            {
                value = value.Trim();
                if (value != string.Empty)
                {
                    value = Regex.Replace(value, @"[ÉÊÈË]", "E");
                    value = Regex.Replace(value, @"[éêèë]", "e");
                    value = Regex.Replace(value, @"[ð]", "e");
                    value = Regex.Replace(value, @"[ÁÂÀÅÃÄ]", "A");
                    value = Regex.Replace(value, @"[ãåäâ]", "a");
                    value = Regex.Replace(value, @"[ÚÛÙÜ]", "U");
                    value = Regex.Replace(value, @"[úùüû]", "u");
                    value = Regex.Replace(value, @"[ÓÔÒØÕÖ]", "O");
                    value = Regex.Replace(value, @"[óòøõôö]", "o");
                    value = Regex.Replace(value, @"[ÍÎÌÏ]", "I");
                    value = Regex.Replace(value, @"[¡ìíîï]", "i");
                    value = Regex.Replace(value, @"[¢ç]", "c");
                    value = Regex.Replace(value, @"[Ç]", "C");
                    value = Regex.Replace(value, @"[Ð]", "D");
                    value = Regex.Replace(value, @"[Ñ]", "N");
                    value = Regex.Replace(value, @"[ñ]", "n");
                    value = Regex.Replace(value, @"[Ý]", "Y");
                    value = Regex.Replace(value, @"[ýÿ]", "y");
                    value = Regex.Replace(value, @"[ß]", "SS");
                    if (fullReplace)
                    {
                        value = Regex.Replace(value, @"[^a-z^A-Z0-9^é^è^à^ê^'^ë^,^\.^\-^\*^\+^ ^\n^:^?^!^@]", "_");
                    }
                }
            }
            else
            {
                value = string.Empty;
            }

            return value;
        }
        
        // Nettoyage des char pour avoir des htmls entities (pour la page de template Ogone)
        public static string LightHtmlEncode(string htmlInput)
        {
            if (string.IsNullOrEmpty(htmlInput))
                return htmlInput;

            var buf = new StringBuilder();
            for (var i = 0; i < htmlInput.Length; i++)
            {
                var c = htmlInput[i];
                if (c > 127)
                    buf.Append(string.Format("&#{0};", (int)c));
                else
                    buf.Append(c);
            }

            return buf.ToString();
        }

        public static string ToSeoUrl(this string url)
        {
            var encodedUrl = (url ?? "").ToLowerInvariant();

            encodedUrl = Regex.Replace(encodedUrl, @"\&+", "and");

            encodedUrl = encodedUrl.Replace("'", "");

            encodedUrl = encodedUrl.Replace("é", "e");

            encodedUrl = encodedUrl.Replace("è", "e");

            encodedUrl = encodedUrl.Replace("ê", "e");

            encodedUrl = encodedUrl.Replace("à", "e");

            encodedUrl = Regex.Replace(encodedUrl, @"[^a-z0-9]", "-");

            encodedUrl = Regex.Replace(encodedUrl, @"-+", "-");

            encodedUrl = encodedUrl.Trim('-');

            return encodedUrl;
        }

        public static string ApplyMask(this string str, string pattern, char mask)
        {            
            var sb2 = new StringBuilder();

            var idx = 0;

            foreach (var c in pattern.ToCharArray())
            {
                if (c == mask)
                {
                    sb2.Append(str[idx]);
                    idx++;
                }
                else
                {
                    sb2.Append(c);
                }
                if (idx >= str.Length)
                {
                    break;
                }
            }
            return sb2.ToString();
        }
    }
}
