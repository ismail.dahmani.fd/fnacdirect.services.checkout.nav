using FnacDirect.Contracts.Online.Model;
using FnacDirect.Front.Meteor.Business.Models;
using FnacDirect.OrderPipe.Base.BaseModel;
using FnacDirect.OrderPipe.Base.BaseModel.Model;
using FnacDirect.OrderPipe.Base.LineGroups;
using FnacDirect.OrderPipe.Base.Model.Facets;
using FnacDirect.OrderPipe.Base.Model.LineGroups;
using FnacDirect.OrderPipe.Base.Model.OrderInformations;
using FnacDirect.OrderPipe.Base.Model.Pop;
using FnacDirect.OrderPipe.Base.Model.Solex;
using FnacDirect.OrderPipe.Base.Model.UserInformations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace FnacDirect.OrderPipe.Base.Model
{
    /// <summary>
    /// Container used by "pop" pipe definition.
    /// </summary>

    // TODO solution temporaire, à voir comment changer la configuration du serialiseur xml
    [XmlInclude(typeof(SubscriptionsData))]
    [XmlInclude(typeof(AboIlliData))]
    [XmlInclude(typeof(LastCUUMemoryData))]
    [XmlInclude(typeof(QasBillingData))]
    public class PopOrderForm : OF,
                                IGotContextInformations,
                                IGotMobileInformations,
                                IGotUserContextInformations,
                                IGotLineGroups,
                                IGotShippingMethodEvaluation,
                                IGotBillingInformations,
                                IGotLogisticLineGroups,
                                IGotQuoteInformations
    {
        private readonly List<Promotion> _globalPromotions = new List<Promotion>();
        private readonly ArticleList _deletedArticles = new ArticleList();
        private readonly ArticleList _removedArticles = new ArticleList();
        private readonly List<Credit> _availableCredits = new List<Credit>();

        public PopOrderForm()
        {
            OrderContextInformations = new OrderContextInformations();
            OrderMobileInformations = new OrderMobileInformations();
            UserContextInformations = new UserContextInformations();
            OrderGlobalInformations = new OrderGlobalInformations();

            LineGroups = new List<PopLineGroup>();
            LogisticLineGroups = new List<LogisticLineGroup>();
            ShippingMethodEvaluation = new ShippingMethodEvaluation();
        }

        public List<PopLineGroup> LineGroups { get; set; }

        IEnumerable<Article> IGotLineGroups.Articles
        {
            get
            {
                return LineGroups.GetArticles();
            }
        }

        public ArticleList DeletedArticles
        {
            get
            {
                return _deletedArticles;
            }
        }

        public ArticleList RemovedArticles
        {
            get
            {
                return _removedArticles;
            }
        }

        public bool ArticlesModified { get; set; }

        public bool DisableSplit { get; set; }

        public void AddLineGroup(ILineGroup lineGroup)
        {
            if (lineGroup is PopLineGroup)
            {
                LineGroups.Add(lineGroup as PopLineGroup);
            }
        }

        public void InsertLineGroup(ILineGroup lineGroup, int index)
        {
            if (lineGroup is PopLineGroup)
            {
                LineGroups.Insert(index, lineGroup as PopLineGroup);
            }
        }

        public void RemoveLineGroup(ILineGroup lineGroup)
        {
            if (lineGroup is PopLineGroup)
            {
                LineGroups.Remove(lineGroup as PopLineGroup);
            }
        }

        public void RemoveArticles(ArticleList articlesToRemove)
        {
            AddToRemovedArticles(articlesToRemove);

            var articlesIds = articlesToRemove.Select(a => a.ProductID);
            RemoveArticlesByIds(articlesIds);
        }

        private void AddToRemovedArticles(ArticleList articlesToRemove)
        {
            foreach (var articleToRemove in articlesToRemove)
            {
                var notExist = !RemovedArticles.Select(ra => ra.ProductID).Contains(articleToRemove.ProductID);
                if (notExist)
                    RemovedArticles.Add(articleToRemove);
            }
        }

        private void RemoveArticlesByIds(IEnumerable<int?> articlesToRemove)
        {
            foreach (var lg in LineGroups)
            {
                lg.RemoveArticlesById(articlesToRemove);
            }
            ArticlesModified = true;
        }

        public void ClearLineGroups()
        {
            LineGroups.Clear();
        }

        public ILineGroup BuildLineGroup(ArticleList articleList)
        {
            return new PopLineGroup()
            {
                Articles = articleList
            };
        }

        public ILineGroup BuildTemporaryLineGroup(ArticleList articleList)
        {
            return new TemporaryPopLineGroup()
            {
                Articles = articleList
            };
        }

        [XmlIgnore]
        public XmlOrderForm XmlOrder { get; set; }

        [XmlIgnore]
        public IOrderContextInformations OrderContextInformations { get; set; }

        public OrderMobileInformations OrderMobileInformations { get; set; }

        public AppliablePath? Path { get; set; }

        public UserContextInformations UserContextInformations { get; set; }

        public OrderGlobalInformations OrderGlobalInformations { get; set; }

        [XmlIgnore]
        IOrderMobileInformations IGotMobileInformations.OrderMobileInformations
        {
            get
            {
                return OrderMobileInformations;
            }
        }

        [XmlIgnore]
        IUserContextInformations IGotUserContextInformations.UserContextInformations
        {
            get
            {
                return UserContextInformations;
            }
        }

        [XmlIgnore]
        IEnumerable<ILineGroup> IGotLineGroups.LineGroups
        {
            get
            {
                return LineGroups;
            }
        }

        public IList<Credit> AvailableCredits
        {
            get
            {
                return _availableCredits;
            }
        }

        public ShippingMethodEvaluation ShippingMethodEvaluation { get; set; }

        public OrderTypeEnum OrderType { get; set; }

        private readonly BillingMethodList _orderBillingMethods = new BillingMethodList();

        public BillingMethodList OrderBillingMethods
        {
            get
            {
                return _orderBillingMethods;
            }
        }

        IEnumerable<BillingMethod> IGotBillingInformations.OrderBillingMethods
        {
            get
            {
                return OrderBillingMethods;
            }
        }

        public void AddBillingMethod(BillingMethod billingMethod)
        {
            _orderBillingMethods.Add(billingMethod);
        }

        public void RemoveBillingMethod(int billingMethodId)
        {
            _orderBillingMethods.RemoveAll(m => m.BillingMethodId == billingMethodId);
        }

        public void RemoveBillingMethod(Predicate<BillingMethod> predicate)
        {
            _orderBillingMethods.RemoveAll(predicate);
        }

        public void SortOrderBillingMethods()
        {
            _orderBillingMethods.Sort();
        }

        public void ClearBillingMethods()
        {
            _orderBillingMethods.Clear();
        }

        public void ClearShippingMethodEvaluation()
        {
            ShippingMethodEvaluation.Clear();
        }

        public BillingMethod MainBillingMethod { get; set; }

        [XmlIgnore]
        public decimal RemainingTotal { get; set; }

        [XmlIgnore]
        public int BillingMethodMask { get; set; }

        [XmlIgnore]
        public Dictionary<int, decimal> BillingMethodConsolidation { get; set; }

        [XmlIgnore]
        public bool EnableOgoneCreditCard { get; set; }

        [XmlIgnore]
        public IEnumerable<Promotion> GlobalPromotions
        {
            get
            {
                return _globalPromotions;
            }
        }

        public int GetTotalArticleQuantities()
        {
            var quantities = 0;
            var articles = LineGroups.GetArticles();
            foreach (var item in articles)
            {
                quantities += item.Quantity.GetValueOrDefault(1);
                if (item is StandardArticle)
                {
                    var standardArticle = item as StandardArticle;
                    if (standardArticle.Services != null
                        && standardArticle.Services.Any())
                    {
                        quantities += standardArticle.Services.Sum(s => s.Quantity.GetValueOrDefault(1));
                    }
                }
            }
            return quantities;
        }

        [XmlIgnore]
        IOrderGlobalInformations IGotBillingInformations.OrderGlobalInformations
        {
            get
            {
                return OrderGlobalInformations;
            }
        }

        public void AddGlobalPromotion(Promotion promotion)
        {
            _globalPromotions.Add(promotion);
        }

        public void AddLogisticLineGroup(ILogisticLineGroup logisticLineGroup)
        {
            if (logisticLineGroup is LogisticLineGroup)
            {
                LogisticLineGroups.Add(logisticLineGroup as LogisticLineGroup);
            }
        }

        public void RemoveLogisticLineGroup(ILogisticLineGroup logisticLineGroup)
        {
            if (logisticLineGroup is LogisticLineGroup)
            {
                LogisticLineGroups.Remove(logisticLineGroup as LogisticLineGroup);
            }
        }

        private readonly GlobalPrices _globalPrices = new GlobalPrices();

        public GlobalPrices GlobalPrices
        {
            get { return _globalPrices; }
        }

        public BasketType BasketType { get; set; }

        public BillingAddress BillingAddress { get; set; }

        public string RelayCellphone { get; set; }

        private readonly List<int> _hiddenCreditCards = new List<int>();
        public PipePath EligiblePath { get; set; } = PipePath.All;

        public List<int> HiddenCreditCards
        {
            get { return _hiddenCreditCards; }
        }

        public List<LogisticLineGroup> LogisticLineGroups { get; set; }

        IEnumerable<ILogisticLineGroup> IGotLogisticLineGroups.LogisticLineGroups
        {
            get
            {
                return LogisticLineGroups as IEnumerable<ILogisticLineGroup>;
            }
        }

        public bool PayOnDeliverySelected()
        {
            if (ShippingMethodEvaluation.FnacCom.HasValue)
            {
                var fnaccom = ShippingMethodEvaluation.FnacCom.Value;
                if (ShippingMethodEvaluation.FnacCom.Value.SelectedShippingMethodEvaluationType == ShippingMethodEvaluationType.Postal &&
                    fnaccom.SelectedShippingMethod.HasValue && fnaccom.SelectedShippingMethod.Value == PayOnDeliveryBillingMethod.GetShippingMethodId())
                {
                    return true;
                }
            }
            return false;
        }

        public bool HasProfessionalAddressForSeller(int sellerId)
        {
            var shippingMethodEvaluationGroupForSeller = ShippingMethodEvaluation.GetShippingMethodEvaluationGroup(sellerId);

            if (shippingMethodEvaluationGroupForSeller.HasValue &&
                shippingMethodEvaluationGroupForSeller.Value.SelectedShippingMethodEvaluationType == ShippingMethodEvaluationType.Postal)
            {
                var selectedShippingMethodEvaluation = shippingMethodEvaluationGroupForSeller.Value.SelectedShippingMethodEvaluation;
                var shippingAddressLegalStatus = selectedShippingMethodEvaluation.ShippingAddress?.LegalStatus;

                return IsProfessionalLegalStatus(shippingAddressLegalStatus);
            }

            var billingAddressLegalStatus = BillingAddress?.LegalStatus;

            return IsProfessionalLegalStatus(billingAddressLegalStatus);
        }

        private static bool IsProfessionalLegalStatus(int? legalStatus)
        {
            return legalStatus.HasValue && legalStatus.Value == (int)LegalStatus.Business;
        }

        public QuoteInfos QuoteInfos { get; set; }

        public override string ToString()
        {
            var lineGroupsBuilder = new StringBuilder();
            LineGroups.ForEach(l => lineGroupsBuilder.AppendLine(l.ToString()));

            var pop = new StringBuilder();
            pop.AppendLine($"IsCurrentlyRunningCriticalSection {IsCurrentlyRunningCriticalSection}");
            pop.AppendLine($"deletedArticles");
            pop.AppendLine($"ShippingMethod : {ShippingMethodEvaluation.ToString()}");
            _deletedArticles.ForEach(a => pop.AppendLine(a.ToString()));
            pop.AppendLine($"removedArticles");
            _removedArticles.ForEach(a => pop.AppendLine(a.ToString()));
            pop.AppendLine(UserContextInformations.ToString());
            pop.AppendLine(lineGroupsBuilder.ToString());

            return pop.ToString();
        }

        public void AddArticle(Article article)
        {
            // Nous tentons de récupérer le groupe d'articles correspondant au vendeur de l'article en cours de lecture.
            var lineGroup = LineGroups.FirstOrDefault(l => l.Seller.SubSellerId == article.SellerID.GetValueOrDefault());

            if (lineGroup == null)
            {
                // Initialisation du nouveau groupe d'article à partir des informations de L'OrderForm et de l'article en cours de lecture.
                lineGroup = new PopLineGroup
                {
                    Seller =
                        {
                            SellerId = article.SellerID.GetValueOrDefault()
                        }
                };

                if (article is MarketPlaceArticle)
                {
                    var marketPlaceArticle = article as MarketPlaceArticle;
                    var marketPlaceSeller = marketPlaceArticle.Offer.Seller;
                    lineGroup.Seller.DisplayName = marketPlaceSeller.Company ?? string.Format("{0} {1}", marketPlaceSeller.FirstName, marketPlaceSeller.LastName);
                }
                else if (article is Model.VirtualGiftCheck)
                {
                    lineGroup.Seller.SellerId = SellerIds.GetId(Base.LineGroups.SellerType.VirtualGiftCheck); // CCV
                }

                LineGroups.Add(lineGroup);

                LineGroups = LineGroups.OrderBy(l => l.Seller.SellerId).ToList();
            }

            lineGroup.Articles.Add(article);
        }

        public override OFRoute GetRoute()
        {
            if (this.IsOneClick())
            {
                return OFRoute.OneClick;
            }

            switch (Path)
            {
                case AppliablePath.OnePage:
                    return OFRoute.OnePage;
                case AppliablePath.Full:
                    return OFRoute.Full;
                default:
                    return OFRoute.Unknown;
            }
        }

        public decimal? RemainingGlobalPricesForPrimaryMethods()
        {
            var globalPrice = GlobalPrices?.TotalPriceEur.GetValueOrDefault();
            foreach (var billingMethod in OrderBillingMethods.Where(m => !m.IsPrimary && !(m is FreeBillingMethod)))
            {
                globalPrice -= billingMethod.Amount.GetValueOrDefault(0);
            }
            return globalPrice;
        }

        public ShippingChoice GetMainShippingChoice()
        {
            var mainShippingMethodEvaluationGroup = ShippingMethodEvaluation?.GetMainShippingMethodEvaluationGroup();
            return mainShippingMethodEvaluationGroup?.GetSelectedChoice();
        }
    }

    [Flags]
    public enum PipePath
    {
        OnePage = 1,
        ParcoursLong = 2,
        Oneclick = 4,
        All = OnePage | ParcoursLong | Oneclick
    }
}
