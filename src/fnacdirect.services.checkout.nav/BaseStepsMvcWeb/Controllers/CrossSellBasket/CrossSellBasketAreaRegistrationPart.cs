using FnacDirect;
using FnacDirect.Technical.Framework.Web.Routing;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Web.Mvc;

namespace FnacDirect.OrderPipe.BaseMvc.Web.CrossSellBasket
{
    public class CrossSellBasketAreaRegistrationPart
    {
        private readonly ISwitchProvider _switchProvider;

        public CrossSellBasketAreaRegistrationPart(ISwitchProvider switchProvider)
        {
            _switchProvider = switchProvider;
        }

        public void RegisterArea(AreaRegistrationContext areaRegistrationContext)
        {
            if (_switchProvider.IsEnabled("orderpipe.crosssell.redirect.enabled"))
            {
                areaRegistrationContext.MapRoute(
                    name: "Cross Sell Basket - IntermediaryBasketLegacy",
                    url: "Account/Basket/IntermediaryShoppingCartRecalculate.aspx",
                    defaults: new { controller = "CrossSellBasket", action = "IntermediaryBasketLegacy", pipe = "basket" },
                    constraints: new
                    {
                        doNotCaptureIfDynamicBundleIsAdded = new QueryStringRulesRouteConstraint().Parameter("Action").IsNot("13")
                    }
                );

                var intermediaryBasketPagesToRedirect = new[]{
                    "IntermediaryBasket.aspx",
                    "IntermediaryBasket_RIA.aspx",
                    "IntermediaryBasketDynamicBundle.aspx",
                    "IntermediaryBasketHP.aspx",
                    "IntermediaryBasketJavascriptCheck.aspx",
                    "IntermediaryBasketWithForm.aspx",
                    "IntermediaryNoelSelection.aspx"
                };

                foreach (var pageToRedirect in intermediaryBasketPagesToRedirect)
                {
                    areaRegistrationContext.MapRoute(
                         name: "Cross Sell Basket - Redirect -" + pageToRedirect,
                         url: "Account/Basket/" + pageToRedirect,
                         defaults: new { controller = "CrossSellBasket", action = "IntermediaryBasketOnlyRedirectToOrderPipe", pipe = "basket" }
                    );
                }
            }

            areaRegistrationContext.MapRoute(
                name: "Croll Sell Basket - Add",
                url: "basket/add",
                defaults: new { controller = "CrossSellBasket", action = "Add", pipe = "basket" }
            );

            areaRegistrationContext.MapRoute(
                name: "Croll Sell Basket - OneClick",
                url: "basket/oneclick",
                defaults: new { controller = "OneClickBasket", action = "Add", pipe = "basket" }
            );

            areaRegistrationContext.MapRoute(
                name: "Croll Sell Basket - ClickAndCollectExpress",
                url: "basket/clickandcollect-express",
                defaults: new { controller = "ClickAndCollectExpressBasket", action = "Add", pipe = "basket" }
            );

            areaRegistrationContext.MapRoute(
                name: "Croll Sell Basket - ClickInStore",
                url: "basket/click-in-store",
                defaults: new { controller = "CrossSellBasket", action = "ClickInStore", pipe = "basket" }
            );

            areaRegistrationContext.MapRoute(
                name: "Croll Sell Basket - Delete",
                url: "basket/delete",
                defaults: new { controller = "CrossSellBasket", action = "Delete", pipe = "basket" }
            );

            areaRegistrationContext.MapRoute(
               name: "Croll Sell Basket - Update",
               url: "basket/update",
               defaults: new { controller = "CrossSellBasket", action = "Update", pipe = "basket" }
           );

            areaRegistrationContext.MapRoute(
               name: "Basket Management - Clean",
               url: "basket/clean",
               defaults: new { controller = "BasketManagement", action = "Clean" }
           );
        }
    }
}
