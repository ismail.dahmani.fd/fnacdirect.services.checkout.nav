using System.Collections.Generic;
using FnacDirect.Subscriptions.Models;

namespace FnacDirect.OrderPipe.Base.Business
{
    public interface ISubscriptionService
    {
        Subscription GetSubscriptionByTypeId(int accountId, int typeId);
        List<Subscription> GetSubscriptions(int accountId);
    }
}
