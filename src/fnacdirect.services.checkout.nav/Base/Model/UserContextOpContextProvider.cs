﻿using System.Web;
using FnacDirect.Context;
using FnacDirect.Contracts.Online.Model;
using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.OrderPipe.Core;

namespace FnacDirect.OrderPipe.CoreRouting
{
    public class UserContextOpContextProvider : IOPContextProvider
    {
        private readonly IUserContextProvider _userContextProvider;
        private readonly SiteContext _siteContext;

        protected SiteContext SiteContext
        {
            get
            {
                return _siteContext;
            }
        }

        public UserContextOpContextProvider(IUserContextProvider userContextProvider, SiteContext siteContext)
        {
            _userContextProvider = userContextProvider;
            _siteContext = siteContext;
        }

        public IUserContext UserContext
        {
            get
            {
                return _userContextProvider.GetCurrentUserContext();
            }
        }

        protected void RegisterOpContextToStructureMap(string pipeName)
        {
            HttpContext.Current.Items["pipe"] = pipeName;
        }

        public virtual OPContext GetOrCreateCurrentContext(string sid, string uid, IOP pipe)
        {
            sid = (pipe.PipeName == "selection") ? uid : sid;

            var currentContext = OPContextManager.GetCurrentContext(pipe.PipeName);

            if (currentContext != null
                && currentContext.SID == sid
                && currentContext.Pipe == pipe)
            {
                RegisterOpContextToStructureMap(pipe.PipeName);

                return currentContext;
            }

            currentContext = OPContextManager.Create(sid, _siteContext, pipe.PipeName);

            currentContext.Pipe = pipe;
            currentContext.OF = new BaseOF();

            RegisterOpContextToStructureMap(pipe.PipeName);

            return currentContext;
        }

        public OPContext GetCurrentContext(string pipeName)
        {
            return OPContextManager.GetCurrentContext(pipeName);
        }
    }
}