﻿using System;
using System.Collections.Generic;

namespace FnacDirect.OrderPipe.Core.Steps
{
    public abstract class MonadicEnumerableStep<TOrderForm, TInput, TOutput> : Step
        where TOrderForm : OF
    {
        protected internal abstract IEnumerable<TInput> Bind(TOrderForm orderForm);

        protected internal abstract TOrderForm Returns(TOrderForm orderForm, IEnumerable<TOutput> output);

        public abstract TOutput Process(TInput input);

        public sealed override void PreCalc(OPContext opContext)
        {
            base.PreCalc(opContext);
        }

        public sealed override StepReturn Check(OPContext opContext)
        {
            var orderForm =  opContext.OF as TOrderForm;

            var input = Bind(orderForm);

            var checking = Check(input);

            if (checking.HasValue)
                return checking.Value;

            return StepReturn.Process;
        }

        protected internal virtual Maybe<StepReturn> Check(IEnumerable<TInput> input)
        {
            return Maybe<StepReturn>.Empty();
        }

        public sealed override StepReturn Process(OPContext opContext)
        {
            var orderForm = opContext.OF as TOrderForm;

            var inputs = Bind(orderForm);

            var outputs = new List<TOutput>();

            foreach (var input in inputs)
            {
                var output = Process(input);
                outputs.Add(output);
            }

            opContext.OF = Returns(orderForm, outputs);

            return StepReturn.Next;
        }
    }
}
