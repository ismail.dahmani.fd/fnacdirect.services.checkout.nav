﻿using System;
using System.Collections.Generic;
using FnacDirect.Technical.Framework.Utils;

namespace FnacDirect.OrderPipe.Core.Services
{
    public class PipeParametersProvider : IPipeParametersProvider
    {
        private readonly IOP _pipe;

        public PipeParametersProvider(IOP pipe)
        {
            _pipe = pipe;
        }

        public T Get<T>(string key)
        {
            var parameterList = _pipe.GlobalParameters;
            return parameterList.Get<T>(key);
        }

        public T Get<T>(string key, T defaultValue)
        {
            var parameterList = _pipe.GlobalParameters;
            return parameterList.Get(key, defaultValue);
        }

        public IEnumerable<T> GetAsEnumerable<T>(string key) where T : IComparable<T>
        {
            var parameterList = _pipe.GlobalParameters;
            return parameterList.GetAsEnumerable<T>(key);
        }

        public IDictionary<string,T> GetAsDictionary<T>(string key) where T : IComparable<T>
        {
            var parameterList = _pipe.GlobalParameters;
            return parameterList.GetAsDictionary<T>(key);
        }

        public RangeSet<T> GetAsRangeSet<T>(string key) where T : IComparable<T>
        {
            var parameterList = _pipe.GlobalParameters;
            return parameterList.GetAsRangeSet<T>(key);
        }
    }
}
