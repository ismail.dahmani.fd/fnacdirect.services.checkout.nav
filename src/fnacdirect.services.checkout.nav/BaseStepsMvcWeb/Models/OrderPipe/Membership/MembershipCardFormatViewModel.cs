﻿
using System.Collections.Generic;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Basket
{
    public class MembershipCardFormatViewModel
    {
        public IEnumerable<string> CardsPrefix { get; set; }

        public bool DisplayCardPrefix { get; set; }

        public string Pattern { get; set; }

        public int MaxLength { get; set; }

        public bool IsTryCard { get; set; }

        public int TryCardDuration { get; set; }
    }
}
