﻿namespace FnacDirect.OrderPipe
{
    /// <summary>
    /// Step de base pour les étapes interactives les plus simples.
    /// Utilisable par les steps n'ayant pas a manipuler l'order form par exemple.
    /// </summary>
    public abstract class BasicInteractiveStep : InteractiveStep<OF, NullStepData, NullGroupData>
    {

    }
}
