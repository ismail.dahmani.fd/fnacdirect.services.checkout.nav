﻿namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.OrderThankYou
{
    public class SteedFromStoreViewModel
    {
        public string DeliveryDate { get; set; }
        public string PhoneNumber { get; set; }
    }
}
