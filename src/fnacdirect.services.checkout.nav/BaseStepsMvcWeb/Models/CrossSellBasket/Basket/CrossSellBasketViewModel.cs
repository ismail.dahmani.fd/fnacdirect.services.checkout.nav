﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FnacDirect.OrderPipe.BaseMvc.Web.CrossSellBasket
{
    public class CrossSellBasketViewModel : EmptyCrossSellBasketViewModel
    {
        private readonly BasketItemReferenceViewModel _mainProduct;
        private readonly List<CrossSellBasketItemViewModel> _currentProducts;
        private readonly List<BasketItemReferenceViewModel> _allProducts;
        private readonly List<CrossSellBasketItemViewModel> _deletedProducts;

        public CrossSellBasketViewModel(string orderPipeUrl, BasketItemReferenceViewModel mainProduct, IEnumerable<CrossSellBasketItemViewModel> currentProducts, IEnumerable<BasketItemReferenceViewModel> allProducts, IEnumerable<CrossSellBasketItemViewModel> deletedProducts)
            : base(orderPipeUrl)
        {
            _mainProduct = mainProduct;
            _currentProducts = new List<CrossSellBasketItemViewModel>(currentProducts);
            _allProducts = new List<BasketItemReferenceViewModel>(allProducts);
            _deletedProducts = new List<CrossSellBasketItemViewModel>(deletedProducts);
        }

        public BasketItemReferenceViewModel MainProduct
        {
            get
            {
                return _mainProduct;
            }
        }

        public IEnumerable<CrossSellBasketItemViewModel> CurrentProducts
        {
            get
            {
                return _currentProducts;
            }
        }

        public IEnumerable<BasketItemReferenceViewModel> AllProducts
        {
            get
            {
                return _allProducts;
            }
        }

        public IEnumerable<CrossSellBasketItemViewModel> DeletedProducts
        {
            get
            {
                return _deletedProducts;
            }
        }

        public bool HasMaxItemsInBasketMessage { get; set; }
    }
}
