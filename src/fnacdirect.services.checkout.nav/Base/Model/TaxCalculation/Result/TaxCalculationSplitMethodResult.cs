namespace FnacDirect.OrderPipe.Base.Model.TaxCalculation
{
    public class TaxCalculationSplitMethodResult
    {
        public GenericPrice GenericGlobalPrice { get; set; }
        public GenericPrice GenericUnitPrice { get; set; }
    }
}
