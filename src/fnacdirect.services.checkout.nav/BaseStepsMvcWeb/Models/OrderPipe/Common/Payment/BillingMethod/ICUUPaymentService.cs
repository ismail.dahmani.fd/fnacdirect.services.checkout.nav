using FnacDirect.OrderPipe.Base.Model.Facets;

namespace FnacDirect.OrderPipe.Base.Business.Payment
{
    public interface ICUUPaymentService
    {
        bool IsAllowed(IGotLineGroups iGotLineGroups, bool disableClickAndCollectCheck = false, bool disableECCVCheck = false);
    }
}
