using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;

namespace FnacDirect.OrderPipe.Base.BaseModel.Model.Quote
{
    public class Choice
    {
        public int MainShippingMethodId { get; set; }
        public int Weight { get; set; }
        [JsonProperty(DefaultValueHandling = DefaultValueHandling.Populate)]
        public decimal TotalPrice { get; set; }
        public List<Parcel> Parcels { get; set; } = new List<Parcel>();
        public List<Group> Groups { get; set; } = new List<Group>();
        public List<AppliedPromotion> AppliedPromotions { get; set; } = new List<AppliedPromotion>();

        public string Label { get; set; }
        public string AltLabel { get; set; }
        public string Description { get; set; }

        public Choice()
        {

        }

        public Choice(Solex.Shipping.Client.Contract.Choice choice)
        {
            MainShippingMethodId = choice.MainShippingMethodId;
            Weight = choice.Weight;
            TotalPrice = choice.TotalPrice;
            Parcels = choice.Parcels.Select(x => new Parcel(x.Key, x.Value)).ToList();
            Groups = choice.Groups.Select(x => new Group(x)).ToList();
            AppliedPromotions = choice.AppliedPromotions.Select(x => new AppliedPromotion(x)).ToList();
            Label = choice.Label;
            AltLabel = choice.AltLabel;
            Description = choice.Description;
        }
    }
}
