﻿using FnacDirect.SalesOffer.Catalog.Data.Core.Repositories;
using FnacDirect.SalesOffer.Catalog.Domain.Entities;
using FnacDirect.Technical.Framework.Web.Configuration;
using FnacDirect.Technical.Framework.Web.Mvc;
using System.Collections.Generic;
using System.Linq;
namespace FnacDirect.OrderPipe.Base.Business.Configuration
{
    public class PromotionTemplateDisplayProvider : IPromotionTemplateDisplayProvider
    {
        private readonly IApplicationContext applicationContext;
        public PromotionTemplateDisplayProvider(IApplicationContext applicationContext)
        {
            this.applicationContext = applicationContext;
        }
        public IEnumerable<PromotionTemplateDisplay> GetPromotionTemplateDisplays()
        {
            return applicationContext.ConfigurationOpcPushMvc.Select(o => ToPromotionTemplateDisplay(o.Value.ToMarketPlacePromotionPush())).ToList();
        }
        private PromotionTemplateDisplay ToPromotionTemplateDisplay(FnacDirect.Contracts.Online.Model.MarketPlacePromotionPush push) => new PromotionTemplateDisplay
        {
            CssClass = push.CssClass,
            CssClassLong = push.CssClassLong,
            TemplateDisplay = (PromotionTemplate)push.DisplayTemplate,
            ListUseLongLabel = push.ListUseLongLabel,
            LongLabel = push.LongLabel,
            ShortLabel = push.ShortLabel,
            ShowEconomy = push.ShowEconomy,
            ThumbnailUseLongLabel = push.ThumbnailUseLongLabel,
            Visibility = push.Visibility.Cast<PromotionVisibilityPlace>().ToList()
        };
    }
}
