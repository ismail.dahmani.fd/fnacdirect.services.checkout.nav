using FnacDirect.OrderPipe.Base.BaseModel;
using FnacDirect.OrderPipe.Base.Business;
using FnacDirect.OrderPipe.Base.Business.Availability;
using FnacDirect.OrderPipe.Base.Business.Shipping;
using FnacDirect.OrderPipe.Base.Business.Shipping.Shop;
using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.OrderPipe.Base.Model.LineGroups;
using FnacDirect.OrderPipe.Base.Model.Pop;
using FnacDirect.OrderPipe.Base.Proxy.Articles;
using FnacDirect.OrderPipe.Base.Proxy.Store;
using FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Basket;
using FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Common.Basket;
using FnacDirect.Technical.Framework.Web.Mvc;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.OnePage.Basket
{
    public class BasketViewModelBuilder : IBasketViewModelBuilder
    {
        private readonly IOrderPipeUrlBuilder _orderPipeUrlBuilder;
        private readonly IPopAvailabilityProcessor _popAvailabilityProcessor;
        private readonly IW3ArticleService _w3ArticleService;
        private readonly IApplicationContext _applicationContext;
        private readonly IShipToStoreAvailabilityService _shipToStoreAvailabilityService;
        private readonly IFnacStoreService _fnacStoreService;
        private readonly IShippingStateService _shippingStateService;
        private readonly IOrderExternalReferenceViewModelBuilder _orderExternalReferenceViewModelBuilder;
        private readonly IMembershipDateBusiness _dateBusiness;
        private readonly ISwitchProvider _switchProvider;
        private readonly IBasketSellerViewModelBuilder _basketSellerViewModelBuilder;

        public BasketViewModelBuilder(IOrderPipeUrlBuilder orderPipeUrlBuilder,
                                      IPopAvailabilityProcessor popAvailabilityProcessor,
                                      IW3ArticleService w3ArticleService,
                                      IApplicationContext applicationContext,
                                      IShipToStoreAvailabilityService shipToStoreAvailabilityService,
                                      IFnacStoreService fnacStoreService,
                                      IShippingStateService shippingStateService,
                                      IOrderExternalReferenceViewModelBuilder orderExternalReferenceViewModelBuilder,
                                      IMembershipDateBusiness dateBusiness,
                                      ISwitchProvider switchProvider,
                                      IBasketSellerViewModelBuilder basketSellerViewModelBuilder)
        {
            _orderPipeUrlBuilder = orderPipeUrlBuilder;
            _popAvailabilityProcessor = popAvailabilityProcessor;
            _w3ArticleService = w3ArticleService;
            _applicationContext = applicationContext;
            _shipToStoreAvailabilityService = shipToStoreAvailabilityService;
            _fnacStoreService = fnacStoreService;
            _shippingStateService = shippingStateService;
            _orderExternalReferenceViewModelBuilder = orderExternalReferenceViewModelBuilder;
            _dateBusiness = dateBusiness;
            _switchProvider = switchProvider;
            _basketSellerViewModelBuilder = basketSellerViewModelBuilder;
        }

        public BasketViewModel Build(PopOrderForm popOrderForm)
        {
            var basketViewModel = new BasketViewModel();
            if (popOrderForm.Path != AppliablePath.Payment)
            {
                basketViewModel.BackLinkUrl = _orderPipeUrlBuilder.GetUrl("DisplayBasket");
            }

            var sellers = popOrderForm.LineGroups.Sellers().ToList();

            var shouldDisplaySellerInfo = false;

            basketViewModel.ProductCount = popOrderForm.LineGroups.GetArticles().Sum(a => a.GetQuantity());

            foreach (var seller in sellers)
            {
                //Seller's Informations
                var lineGroups = popOrderForm.LineGroups.Seller(seller);

                //Article's Part
                var articles = from lineGroup in lineGroups
                               from article in lineGroup.Articles.OfType<BaseArticle>()
                               select article;

                var filteredArticles = articles
                    .Where(a =>
                        (a is StandardArticle standardArticle && !standardArticle.Hidden)
                        || (a is MarketPlaceArticle))
                    .ToList();

                var basketSellerViewModel = _basketSellerViewModelBuilder.Build(popOrderForm, seller, filteredArticles);

                if (basketSellerViewModel.Informations is MarketPlaceSellerInformationsViewModel)
                {
                    shouldDisplaySellerInfo = true;
                }

                basketViewModel.With(basketSellerViewModel);
            }

            if (basketViewModel.Sellers.Count > 1)
            {
                shouldDisplaySellerInfo = true;
            }

            foreach (var basketSellerViewModel in basketViewModel.Sellers)
            {
                basketSellerViewModel.ShouldDisplaySellerInfo = shouldDisplaySellerInfo;
            }

            basketViewModel.OrderExternalReference = _orderExternalReferenceViewModelBuilder.Build(popOrderForm);

            basketViewModel.DisplayBirthDatePopin = _dateBusiness.DisplayBirthDatePopin(popOrderForm);

            return basketViewModel;
        }

        public string GetAvailability(PopOrderForm popOrderForm, StandardArticle standardArticle, ShippingMethodEvaluationGroup shippingMethodEvaluationGroup, IEnumerable<ILineGroup> lineGroups, IEnumerable<int> excludedLogisticTypes)
        {
            if (standardArticle.AvailabilityId == (int)AvailabilityEnum.PreOrder || standardArticle.AvailabilityId == (int)AvailabilityEnum.PreOrderBis)
            {
                var w3Article = _w3ArticleService.GetArticle(standardArticle.ToArticleReference());
                var dtoArticle = w3Article.DTO;

                var locale = _applicationContext.GetLocale();

                var preOrder = _applicationContext.GetMessage("orderpipe.pop.basket.availability.preorder");

                if (standardArticle.IsNumerical)
                {
                    preOrder = _applicationContext.GetMessage("orderpipe.pop.basket.availability.numerical.preorder");
                }

                if (dtoArticle.ExtendedProperties.DisplayAnnouncementDay.HasValue && dtoArticle.ExtendedProperties.DisplayAnnouncementDay.Value)
                {
                    var preOrderFullDate = _applicationContext.GetMessage("orderpipe.pop.basket.availability.preorder.fulldate");
                    var date = standardArticle.AnnouncementDate.ToString("dd MMMM yyyy", CultureInfo.GetCultureInfo(locale));
                    return string.Format(preOrder, string.Format(preOrderFullDate, date));
                }

                var preOrderMonth = _applicationContext.GetMessage("orderpipe.pop.basket.availability.preorder.month");
                var month = standardArticle.AnnouncementDate.ToString("MMMM yyyy", CultureInfo.GetCultureInfo(locale));
                return string.Format(preOrder, string.Format(preOrderMonth, month));

            }

            if (shippingMethodEvaluationGroup.SelectedShippingMethodEvaluationType == ShippingMethodEvaluationType.Shop)
            {
                var shopShippingMethodEvaluationItem = shippingMethodEvaluationGroup.ShopShippingMethodEvaluationItem.Value;

                var shopAddress = shopShippingMethodEvaluationItem.ShippingAddress;

                var fnacStore = _fnacStoreService.GetStore(shopAddress.Identity.GetValueOrDefault());

                if (fnacStore == null)
                    return string.Empty;

                var shopAvailabilities = _shipToStoreAvailabilityService.GetAvailabilities(fnacStore,
                                                                                           lineGroups.SelectMany(l => l.Articles),
                                                                                           excludedLogisticTypes,
                                                                                           popOrderForm.UserContextInformations.IdentityImpersonator);

                if (standardArticle.AvailabilityId == (int)AvailabilityEnum.PreOrderRestockingPE || standardArticle.AvailabilityId == (int)AvailabilityEnum.PreOrderRestockingPT)
                {
                    var availability = shopAvailabilities.FirstOrDefault(x => string.Equals(x.ReferenceGu, standardArticle.ReferenceGU, StringComparison.InvariantCultureIgnoreCase));
                    if (availability != null && availability is ShippingShipToStoreAvailability)
                    {
                        var locale = CultureInfo.GetCultureInfo(_applicationContext.GetLocale());
                        var date = standardArticle.AnnouncementDate.ToString(locale.DateTimeFormat.ShortDatePattern);
                        return string.Format("{0} {1}", standardArticle.AvailabilityLabel, date);
                    }
                }

                return _popAvailabilityProcessor.GetShortGlobalEstimatedDeliveryDateForStore(fnacStore, standardArticle, shopAvailabilities);
            }
            else
            {
                var availabilities = new List<PopAvailabilityProcessorInput>();

                var selectedShippingMethodEvaluation = shippingMethodEvaluationGroup.SelectedShippingMethodEvaluation;

                if (selectedShippingMethodEvaluation == null)
                {
                    selectedShippingMethodEvaluation = shippingMethodEvaluationGroup.PostalShippingMethodEvaluationItem.Value;
                }

                var shippingMethod = selectedShippingMethodEvaluation.GetDefaultShippingMethodId().FirstOrDefault();

                if (selectedShippingMethodEvaluation.SelectedShippingMethodId.HasValue)
                {
                    var shippingChoices = selectedShippingMethodEvaluation.Choices.Where(c => c.MainShippingMethodId == selectedShippingMethodEvaluation.SelectedShippingMethodId);
                    shippingMethod = shippingChoices.FirstOrDefault().MainShippingMethodId;

                    foreach (var choice in shippingChoices)
                    {
                        foreach (var choiceGroup in choice.Groups.Where(g => g.Id == standardArticle.LogType.GetValueOrDefault()))
                        {
                            if (!choice.ContainsShippingMethodId(Constants.ShippingMethods.SteedFromStore)
                                && (standardArticle.AvailabilityId == (int)AvailabilityEnum.PreOrderRestockingPE || standardArticle.AvailabilityId == (int)AvailabilityEnum.PreOrderRestockingPT))
                            {
                                var locale = CultureInfo.GetCultureInfo(_applicationContext.GetLocale());
                                var date = standardArticle.AnnouncementDate.ToString(locale.DateTimeFormat.ShortDatePattern);
                                return string.Format("{0} {1}", standardArticle.AvailabilityLabel, date);
                            }

                            var articles = lineGroups.GetArticles();
                            if (!_switchProvider.IsEnabled("orderpipe.pop.shipping.dlemp.enable"))
                            {
                                articles = articles.Where(a => a is StandardArticle);
                            }
                            var availabilityList = new PopAvailabilityProcessorInputListBuilder(articles, choice.MainShippingMethodId)
                                .ExcludeLogisticTypes(excludedLogisticTypes)
                                .FilterArticles((article) => article.LogType == choiceGroup.Id)
                                .BuildList();

                            availabilities.AddRange(availabilityList);
                        }
                    }
                }

                if (shippingMethodEvaluationGroup.SelectedShippingMethodEvaluationType == ShippingMethodEvaluationType.Relay
                    && selectedShippingMethodEvaluation.ShippingAddress is RelayAddress relayAddress
                    && !CheckDleVisibility(relayAddress.ZipCode))
                {
                    return string.Empty;
                }

                var artAvailabilities = availabilities.Where(art => art.Article.ProductID == standardArticle.ProductID);
                if (artAvailabilities.Any())
                {
                    return _popAvailabilityProcessor.GetShortGlobalEstimatedDeliveryDate(shippingMethod, artAvailabilities, null, _applicationContext.GetMessage("orderpipe.pop.onepage.basket.availability.prefix"));
                }
            }

            return string.Empty;
        }

        private bool CheckDleVisibility(string zipCode)
        {
            return string.IsNullOrEmpty(_shippingStateService.GetStateByZipCode(zipCode));
        }

    }
}
