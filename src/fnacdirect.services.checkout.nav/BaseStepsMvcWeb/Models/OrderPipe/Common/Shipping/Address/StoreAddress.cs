using System.Xml.Serialization;

namespace FnacDirect.OrderPipe.Base.Model
{
    public class StoreAddress : ShippingAddress
    {
        public override int Type
        {
            get { return 4; }
        }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public int GenderId { get; set; }

        public string Name { get; set; }

        [XmlIgnore]
        public string AddressLine1 { get; set; }

        [XmlIgnore]
        public string AddressLine2 { get; set; }

        [XmlIgnore]
        public string AddressLine3 { get; set; }

        [XmlIgnore]
        public string AddressLine4 { get; set; }

        [XmlIgnore]
        public string ZipCode { get; set; }

        [XmlIgnore]
        public string City { get; set; }

        [XmlIgnore]
        public string CellPhone { get; set; }

        [XmlIgnore]
        public string Fax { get; set; }

        [XmlIgnore]
        public string GU { get; set; }

        public override bool IsDummy()
        {
            return false;
        }
    }
}
