using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.TagCommander;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Controllers.Pop
{
    public class PopTagCommanderController : PopBaseController
    {
        private readonly OPContext _opContext;
        private readonly ITagCommanderViewModelBuilder _tagCommanderViewModelBuilder;


        public PopTagCommanderController(OPContext opContext, ITagCommanderViewModelBuilder tagCommanderViewModelBuilder)
        {
            _opContext = opContext;
            _tagCommanderViewModelBuilder = tagCommanderViewModelBuilder;
        }

        [ChildActionOnly]
        public ActionResult Common()
        {
            var popOrderForm = _opContext.OF as PopOrderForm;

            try
            {
                var viewModel = _tagCommanderViewModelBuilder.Build(popOrderForm, _opContext);

                return PartialView("_Common", viewModel);
            }
            catch
            {
                return new EmptyResult();
            }
        }

        [ChildActionOnly]
        public ActionResult OrderThankYou()
        {
            var popOrderForm = _opContext.OF as PopOrderForm;

            try
            {
                var viewModel = _tagCommanderViewModelBuilder.BuildThankYou(popOrderForm, _opContext);

                return PartialView("_ThankYou", viewModel);
            }
            catch
            {
                return new EmptyResult();
            }
        }
    }
}
