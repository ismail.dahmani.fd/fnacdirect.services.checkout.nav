using System;
using System.Linq;
using System.Collections.Generic;
using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.Membership.Business;
using FnacDirect.Technical.Framework.CoreServices.Localization;
using FnacDirect.Technical.Framework.CoreServices;
using FnacDirect.Membership.Model.Constants;
using System.Text.RegularExpressions;
using FnacDirect.Contracts.Online.Model;
using FnacDirect.Front.WebBusiness;
using Article = FnacDirect.OrderPipe.Base.Model.Article;
using FnacDirect.Technical.Framework.Web.Mvc;
using FnacDirect.Technical.Framework.Switch;

namespace FnacDirect.OrderPipe.Base.Business
{
    public class NewAdhesionCMException : Exception
    {
        public NewAdhesionCMException(string message) : base(message) { }
    }

    public class NewAdhesionCMBusiness : INewAdhesionCMBusiness
    {
        private readonly IMembershipDateBusiness _dateBusiness;
        private readonly IApplicationContext _appContext;
        private readonly ISwitchProvider _switchProvider;
        private readonly int _minAge;
        private readonly int _youngPriceMaxAge;
        private readonly int? _fnacCardThreeYears;
        private readonly int _fnacCardYoungThreeYears;
        private readonly int _fnacPlusCard;

        public NewAdhesionCMBusiness(IApplicationContext applicationContext, IMembershipDateBusiness dateBusiness, ISwitchProvider switchProvider)
        {
            _appContext = applicationContext;
            _dateBusiness = dateBusiness;
            _switchProvider = switchProvider;
            var setting = _appContext.GetAppSetting("adherent.age.min");
            if (!int.TryParse(setting, out _minAge))
                _minAge = 14;
            setting = _appContext.GetAppSetting("adherent.age.max.youngprice");
            if (!int.TryParse(setting, out _youngPriceMaxAge))
                _youngPriceMaxAge = 26;
            
            var a = MembershipConfigurationBusiness.GetAdherentCardByKey("siebel.adherent.carte.normal.newadh.3ans");
            _fnacCardThreeYears = a?.Prid;

            if (Switch.IsEnabled("op.accnt.enable.AdhesionYoung", false))
            {
                a = MembershipConfigurationBusiness.GetAdherentCardByKey("siebel.adherent.carte.jeune.newadh.3ans");
                _fnacCardYoungThreeYears = a.Prid;
            }

            if (Switch.IsEnabled("front.adherents.fnacPlusEnabled", false))
            {
                a = MembershipConfigurationBusiness.GetAdherentCardByKey("siebel.adherent.carte.adhfnacplus.conversion.1ans.fidunique");
                _fnacPlusCard = a.Prid;
            }
        }

        /// <summary>
        /// Détermine si l'article est une demande d'adhésion ou pas
        /// </summary>
        /// <param name="articleId"></param>
        /// <returns>Vrai si l'article est une demande d'adhésion et faux sinon</returns>
        public bool IsNewAdhesion(int articleId)
        {
            return articleId == _fnacCardThreeYears || articleId == _fnacCardYoungThreeYears;
        }

        public bool IsAdh3Years(int articleId)
        {
            return articleId == _fnacCardThreeYears || articleId == _fnacCardYoungThreeYears;
        }        

        /// <summary>
        /// Détermine si le panier contient une demande d'adhésion
        /// </summary>
        /// <param name="orderForm">OF à tester</param>
        /// <returns>Vrai si le panier contient une demande d'adhésion et faux sinon</returns>
        public bool IsContainsNewAdhesion(IEnumerable<Article> articles)
        {
            return GetNewAdhesionArticle(articles) != null;
        }
        
        /// <summary>
        /// Détermine si le panier contient une demande d'adhésion
        /// </summary>
        /// <param name="orderForm">OF à tester</param>
        /// <returns>Vrai si le panier contient une demande d'adhésion et faux sinon</returns>
        public bool IsContainsNewFnacPlus(IEnumerable<Article> articles)
        {
            foreach (var a in articles)
            {
                if (a.ProductID.HasValue)
                {
                    var pAdherentCardArticle = MembershipConfigurationBusiness.AdherentCardArticle(a.ProductID.Value);
                    if (pAdherentCardArticle?.CategoryCardType == CategoryCardType.ConversionFnacPlus)
                        return true;
                }
            }
            return false;
        }

        /// <summary>
        /// Détermine si le panier contient une adhésion ou assimilé
        /// </summary>
        /// <param name="orderForm">OF à tester</param>
        /// <returns>Vrai si le panier contient une demande d'adhésion et faux sinon</returns>
        public bool IsContainsAdhesion(IEnumerable<Article> articles)
        {
            foreach (var a in articles)
            {
                if (a.ProductID.HasValue)
                {
                    var pAdherentCardArticle = MembershipConfigurationBusiness.AdherentCardArticle(a.ProductID.Value);
                    if (pAdherentCardArticle != null)
                    {
                        if (    pAdherentCardArticle.CategoryCardType == CategoryCardType.Recruit ||
                                pAdherentCardArticle.CategoryCardType == CategoryCardType.Renew ||
                                pAdherentCardArticle.CategoryCardType == CategoryCardType.RenouvellementFnacPlus ||
                                pAdherentCardArticle.CategoryCardType == CategoryCardType.ConversionFnacPlus ||
                                pAdherentCardArticle.CategoryCardType == CategoryCardType.SouscriptionFnacPlusTrial ||
                                pAdherentCardArticle.CategoryCardType == CategoryCardType.SouscriptionFnacPlusTrialDecouverte ||
                                pAdherentCardArticle.CategoryCardType == CategoryCardType.TRFnacPlus)
                            return true;
                    }
                }
            }
            return false;
        }

        /// <summary>
        /// Détermine si le panier contient une adhésion 3ans ou assimilé
        /// </summary>
        /// <param name="orderForm">OF à tester</param>
        /// <returns>Vrai si le panier contient une demande d'adhésion et faux sinon</returns>
        public bool IsContainsAdhesion3Years(IEnumerable<Article> articles)
        {
            foreach (var a in articles)
            {
                if (a.ProductID.HasValue)
                {
                    var pAdherentCardArticle = MembershipConfigurationBusiness.AdherentCardArticle(a.ProductID.Value);
                    if (pAdherentCardArticle != null)
                    {
                        // Recruit ou renouvellement 3 ans
                        if (    (pAdherentCardArticle.CategoryCardType == CategoryCardType.Recruit || pAdherentCardArticle.CategoryCardType == CategoryCardType.Renew) 
                                && pAdherentCardArticle.CardDuration == "ThreeYears")
                        {
                            return true;
                        }
                        // Fnac+
                        else if (pAdherentCardArticle.CategoryCardType == CategoryCardType.RenouvellementFnacPlus ||
                                pAdherentCardArticle.CategoryCardType == CategoryCardType.ConversionFnacPlus ||
                                pAdherentCardArticle.CategoryCardType == CategoryCardType.SouscriptionFnacPlusTrial ||
                                pAdherentCardArticle.CategoryCardType == CategoryCardType.SouscriptionFnacPlusTrialDecouverte ||
                                pAdherentCardArticle.CategoryCardType == CategoryCardType.TRFnacPlus)
                            return true;
                    }
                }
            }
            return false;
        }


        /// <summary>
        /// Extrait du panier une demande d'adhésion.
        /// </summary>
        /// <param name="orderForm">OF à tester</param>
        /// <returns>Vrai si le panier contient une demande d'adhésion et faux sinon</returns>
        private Article GetNewAdhesionArticle(IEnumerable<Article> articles)
        {
            var article = articles.FirstOrDefault(art => IsNewAdhesion(art.ProductID.GetValueOrDefault()));
            return article;
        }


        /// <summary>
        /// Retourne la liste des cartes Fnac d'adhésion
        /// </summary>
        /// <returns>Dictionnaire avec la liste des cartes d'adhésion
        /// clef = id produit / valeur = libellé
        /// </returns>
        private Dictionary<int, string> NewAdhesionCards(string culture)
        {
            var loc = Service<IUIResourceService>.Instance;
            var myFnacCards = new Dictionary<int, string>();

            if (Switch.IsEnabled("front.adherents.fnacPlusEnabled", false))
            {
                myFnacCards.Add(_fnacPlusCard, loc.GetUIResourceValue("OP.ModifyAccount.NewAdhesion.FnacPlusCard", culture));
            }

            if (_fnacCardThreeYears.HasValue)
            {
                myFnacCards.Add(_fnacCardThreeYears.Value, loc.GetUIResourceValue("OP.ModifyAccount.NewAdhesion.Adhesion3YearsCard", culture));
            }

            if (Switch.IsEnabled("op.accnt.enable.AdhesionYoung", false))
            {                
                myFnacCards.Add(_fnacCardYoungThreeYears, loc.GetUIResourceValue("OP.ModifyAccount.NewAdhesion.AdhesionYound3YearsCard", culture));
            }

            return myFnacCards;
        }

        public bool IsLowerThanAdherenAgeMin(DateTime pBirthDate)
        {
            return _dateBusiness.IsLowerThanAdherenAgeMin(pBirthDate);
        }

        public IDictionary<int, string> NewAdhesionCards(string culture, DateTime pBirthDate)
        {
            var myFnacCards = new Dictionary<int, string>();

            // Age 
            var age = _dateBusiness.GetAge(pBirthDate);

            foreach (var card in NewAdhesionCards(culture))
            {
                var article = ArticleFactoryNew.GetArticle(ArticleReference.FromPrid(card.Key));

                var resultStatus = AgeValidationStatus.OK;

                if (Switch.IsEnabled("op.accnt.enable.AdhesionYoung", false))
                {
                    resultStatus = IsValidAge(article.ArticleReference.PRID.Value, age);
                }

                if (age < _minAge)
                {
                    resultStatus = AgeValidationStatus.TooYoung;
                }
                if (resultStatus == AgeValidationStatus.OK)
                    myFnacCards.Add(
                        card.Key,
                        card.Value);
                else if (resultStatus == AgeValidationStatus.TooYoung)
                    throw new NewAdhesionCMException(MessageHelper.Get(Technical.Framework.Web.FnacContext.Current.SiteContext, "OP.ModifyAccount.NewAdhesion.TooYoung.CM"));
            }

            return myFnacCards;
        }

        public enum AgeValidationStatus
        {
            OK = 0,     // Bon choix !!
            TooYoung,   // Moins de 13 ans, tu peux pas devenir adhérent !
            TooRich,    // Moins de 26 ans, profite des tarifs de djeunes !
            TooOld,     // Tarif jeune pour les moins de 26 ans
        }

        /// <summary>
        /// Détermine si l'age et le type de carte sont en adéquation
        /// </summary>
        /// <returns>Vrai si l'age permet d'avoir accès à cette carte et faux sinon</returns>
        private AgeValidationStatus IsValidAge(int pridCard, int age)
        {
            var ageValidation = AgeValidationStatus.OK;
            if (age < _minAge)
            {
                // Moins de 13 ans, tu peux pas devenir adhérent !
                ageValidation = AgeValidationStatus.TooYoung;
            }
            else if (pridCard == _fnacCardYoungThreeYears)
            {
                // Tarif jeune pour les moins de 26 ans
                ageValidation = (age < _youngPriceMaxAge) ? AgeValidationStatus.OK : AgeValidationStatus.TooOld;
            }
            else if (pridCard == _fnacCardThreeYears)
            {
                // Moins de 26 ans, profite des tarifs de djeunes !
                ageValidation = (age >= _youngPriceMaxAge) ? AgeValidationStatus.OK : AgeValidationStatus.TooRich;
            }

            return ageValidation;
        }

        /// <summary>
        /// Vérifie la formule de Luhn de la carte du numéro de carte
        /// cf. http://blogs.media-tips.com/bernard.opic/2006/07/27/algorithme-de-luhn-implementation-c-sharp/
        /// </summary>
        /// <param name="l"></param>
        /// <returns>Vrai si la formule est vérifié et faux sinon</returns>
        private bool Luhn(string l)
        {
            int s = 0, i = 16;

            while (i-- > 0)
            {
                var v = l[15 - i] - '0' << i % 2;
                s += (v > 9) ? v - 9 : v;
            }

            return s % 10 < 1;
        }

        /// <summary>
        /// Vérifie la validité du numéro de carte
        /// </summary>
        /// <param name="cardNumber"></param>
        /// <returns>Vrai si le numéro de carte est valide et faux sinon</returns>
        public bool IsValidCardNumber(string cardNumber)
        {
            if (cardNumber.Length != 16)
            {
                return false;
            }
            else
            {
                return Luhn(cardNumber);
            }
        }

        /// <summary>
        /// Retourne le login à utiliser
        /// </summary>
        /// <returns></returns>
        public string GenerateLogin(Seller seller)
        {
            return seller?.Number.ToString("D7") ?? 0.ToString("D7"); // On zéro-complète pour faire plaisir à Siebel
        }

        /// <summary>
        /// Retourne le magasin à utiliser
        /// </summary>
        /// <returns></returns>
        public string GenerateMagasin(FnacStore.Model.FnacStore store)
        {
            return store.RefUG.ToString("D4"); // On zéro-complète pour faire plaisir à Siebel
        }

        /// <summary>
        /// Renvoie vrai ssi le système d'adhésion simplifiée est en maintenance
        /// </summary>
        /// <returns></returns>
        public static bool IsMaintenance()
        {
            return ConfigurationManager.Current.Maintenance["CM.AdhesionSimplifiee"];
        }

        /// <summary>
        /// Retire les cartes d'adhésion du panier 
        /// </summary>
        /// <param name="orderForm"></param>
        /// <param name="culture"></param>
        public void RemoveCards(ArticleList articles, string culture)
        {
            var cards = NewAdhesionCards(culture);
            CollectionUtils.RemoveItems<Article>(articles,
                a => a.ProductID.HasValue && cards.ContainsKey(a.ProductID.Value));
        }


        #region pop

        /// <summary>
        /// Retourne la liste des cartes pour un recrutement
        /// </summary>
        /// <returns></returns>
        public IDictionary<int, string> GetAdhesionCards()
        {
            var cards = new Dictionary<int, string>();

            if (_switchProvider.IsEnabled("front.adherents.fnacPlusEnabled"))
            {
                cards.Add(_fnacPlusCard, _appContext.TryGetMessage("orderpipe.pop.membershipcard.fnac.plus"));
            }

            if (_fnacCardThreeYears.HasValue)
            {
                cards.Add(_fnacCardThreeYears.Value, _appContext.TryGetMessage("orderpipe.pop.membershipcard.three.year"));
            }

            return cards;
        }

        #endregion  

    }
}
