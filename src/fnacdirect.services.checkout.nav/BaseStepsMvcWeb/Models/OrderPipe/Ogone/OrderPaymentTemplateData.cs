namespace FnacDirect.OrderPipe.Base.Model
{
    public class OrderPaymentTemplateData : GroupData
    {
        public string Reference { get; set; }

        public string OrderPaymentTemplateContent { get; set; }
    }
}
