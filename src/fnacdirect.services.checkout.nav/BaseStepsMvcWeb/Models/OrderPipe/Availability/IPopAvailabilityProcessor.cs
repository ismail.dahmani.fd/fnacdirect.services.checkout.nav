using System.Collections.Generic;
using FnacDirect.OrderPipe.Base.Business.Shipping.Shop;
using FnacDirect.OrderPipe.Base.Model;

namespace FnacDirect.OrderPipe.Base.Business.Availability
{
    public interface IPopAvailabilityProcessor
    {
        string GetShortGlobalEstimatedDeliveryDate(int shippingMethodId, IEnumerable<PopAvailabilityProcessorInput> availabilities, bool? hasManyDeliveryDates);
        string GetShortGlobalEstimatedDeliveryDate(int shippingMethodId, IEnumerable<PopAvailabilityProcessorInput> availabilities);
        IEnumerable<PopAvailabilityProcessorOutput> GetLongGlobalEstimatedDeliveryDate(IEnumerable<PopAvailabilityProcessorInput> availabilities, bool excludeIfSingle = true);
        IEnumerable<PopAvailabilityProcessorOutput> GetLongGlobalEstimatedDeliveryDateForStore(IEnumerable<Article> articles, IEnumerable<ShipToStoreAvailability> availabilities, int shippingMethodId, int storeId, bool excludeIfSingle = true);
        string GetShortGlobalEstimatedDeliveryDateForStore(StandardArticle standardArticle, IEnumerable<ShipToStoreAvailability> availabilities);
        string GetShortGlobalEstimatedDeliveryDateForStore(FnacStore.Model.FnacStore fnacStore, StandardArticle standardArticle, IEnumerable<ShipToStoreAvailability> availabilities);
        IEnumerable<string> GetShortGlobalEstimatedDeliveryDateForStore(IEnumerable<Article> articles, IEnumerable<ShipToStoreAvailability> availabilities, int shippingMethodId, int storeId);
        IEnumerable<PopAvailabilityProcessorOutput<PopAvailabilityProcessorOutput>> SortPostalLongGlobalEstimatedDeliveryDate(List<PopAvailabilityProcessorOutput<PopAvailabilityProcessorOutput>> longEstimatedDeliveryDate);
        string GetShortGlobalEstimatedDeliveryDate(int shippingMethodId, IEnumerable<PopAvailabilityProcessorInput> availabilities, bool? hasManyDeliveryDates, string prefix);
    }
}
