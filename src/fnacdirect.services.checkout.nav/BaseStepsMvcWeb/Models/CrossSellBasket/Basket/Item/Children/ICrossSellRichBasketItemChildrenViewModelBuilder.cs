using FnacDirect.OrderPipe.Base.Model;
using System.Collections.Generic;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Basket.Children
{
    public interface ICrossSellRichBasketItemChildrenViewModelBuilder : IRichBasketItemChildrenViewModelBuilder
    {
        IEnumerable<CrossSellRichItemChildrenViewModel> Build(StandardArticle standardArticle);
    }
}
