﻿using FnacDirect.OrderPipe.BaseMvc.Web.Validations;
using System.Collections.Generic;
using System.Linq;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Common.Payment
{
    public class FnacCardMethodsViewModel : BillingMethodViewModel
    {
        private readonly IList<CreditViewModel> _credits
              = new List<CreditViewModel>();

        public IList<CreditViewModel> Credits
        {
            get { return _credits; }
        }

        public int SelectedCreditCardId { get; set; }

        [CreditCardHolderInformationsValidation]
        public string Holder { get; set; }

        [CreditCardNumberValidation("")]
        public string CardNo { get; set; }

        public int CardNoLength { get; set; }

        public string ValidityMonth { get; set; }

        [CreditCardValidityDateValidation("ValidityMonth", "", "")]
        public string ValidityYear { get; set; }

        [CreditCardCryptoValidation("")]
        public string Cvv { get; set; }

        public string CreditId { get; set; }

        public bool NumByPhone { get; set; }
    }
}
