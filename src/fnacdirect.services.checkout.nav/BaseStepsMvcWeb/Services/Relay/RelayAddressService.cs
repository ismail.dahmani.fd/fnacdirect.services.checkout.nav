﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.OrderPipe;
using FnacDirect.OrderPipe.Config;

namespace FnacDirect.OrderPipe.Base.Business.Shipping.Relay
{
    public class RelayAddressService : IRelayAddressService
    {
        public void TransformCountryWithZipCode(RelayAddress relayAddress, OPContext opContext)
        {
            var parameters = opContext.Pipe.GlobalParameters;

            if (!int.TryParse(relayAddress.ZipCode, out var zipcode))
            {
                return;
            }

            TransformCountryWithZipCode(relayAddress, parameters, zipcode, "balearicislands");
            TransformCountryWithZipCode(relayAddress, parameters, zipcode, "canaryislands");
        }

        private void TransformCountryWithZipCode(RelayAddress relayAddress, ParameterList parameters, int zipCode, string configName)
        {
            var zipCodes = parameters.GetAsRangeSet<int>($"{configName}.zipcode");
            if (zipCodes.Contains(zipCode))
            {
                relayAddress.CountryId = parameters.GetAsInt($"{configName}.countryid");
                relayAddress.ShippingZone = parameters.GetAsInt($"{configName}.shippingzone");
            }
        }

        public void AdjustAddressFields(RelayAddress relayAddress)
        {
            var address1 = relayAddress.AddressLine1;
            var address2 = relayAddress.AddressLine2;
            var address3 = relayAddress.AddressLine3;

            relayAddress.AddressLine1 = address2;
            relayAddress.AddressLine2 = address3;
            relayAddress.AddressLine3 = address1;
        }
    }
}
