using FnacDirect.Basket.Business;
using FnacDirect.Contracts.Online.IModel;
using FnacDirect.OrderPipe.Base.Business.ArticleConversions.ShoppingCartToOrderForm;
using FnacDirect.OrderPipe.Base.DAL;
using FnacDirect.OrderPipe.Base.LineGroups;
using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.OrderPipe.Base.Model.Facets;
using FnacDirect.OrderPipe.Base.Model.LineGroups;
using FnacDirect.Technical.Framework.ServiceLocation;
using FnacDirect.Technical.Framework.Web.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using BM = FnacDirect.OrderPipe.Base.Model;
using DTO = FnacDirect.Contracts.Online.Model;
using Orderform = FnacDirect.ShoppingCartEntities.Orderform;

namespace FnacDirect.OrderPipe.Base.Business.ShoppingCart
{
    public class TransfertShoppingCartService : ITransfertShoppingCartService
    {
        private readonly IShoppingCartPersistantConfigurationService _shoppingCartPersistantConfigurationService;
        private readonly IShoppingCartService _shoppingCartService;
        private readonly IShoppingCartToOrderFormConverter _shoppingCartToOrderFormArticleConverter;
        private readonly IOrderDetailsDAL _orderDetailsDAL;
        private readonly IApplicationContext _applicationContext;
        private readonly IArticleBusiness3 _articleBusiness;

        public TransfertShoppingCartService(IShoppingCartPersistantConfigurationService shoppingCartPersistantConfigurationService,
                                           IShoppingCartService shoppingCartService, IOrderDetailsDAL orderDetailsDAL, IApplicationContext applicationContext, IArticleBusiness3 articleBusiness)
        {
            _shoppingCartPersistantConfigurationService = shoppingCartPersistantConfigurationService;
            _shoppingCartService = shoppingCartService;
            _orderDetailsDAL = orderDetailsDAL;
            _applicationContext = applicationContext;
            _articleBusiness = articleBusiness;
            _shoppingCartToOrderFormArticleConverter = ServiceLocator.Current.GetInstance<IShoppingCartToOrderFormConverter>();
        }

        public TransferShoppingCartResult TransfertShoppingCartToOrderForm(string uid, string sid, ShoppingCartEntities.ShoppingCartType shoppingCartType, ShoppingCartEntities.FusionType fusionType)
        {
            var articleList = new ArticleList();

            var anyCollectInStore = false;

            var transferShoppingCartResult = new TransferShoppingCartResult();


            _shoppingCartService.RetrieveShoppingCartOrderforms(uid,
                sid,
                shoppingCartType,
                out var shoppingCartSid,
                out var shoppingCartUid,
                out var orderform,
                out var shoppingCartUidExists,
                fusionType);

            if (!_shoppingCartPersistantConfigurationService.IsActive)
            {
                orderform = BasketBusiness.Load(shoppingCartType, sid);

                articleList = _shoppingCartToOrderFormArticleConverter.ConvertArticles(orderform.Articles);

                foreach (ShoppingCartEntities.Article shoppingCartArticle in orderform.Articles)
                {
                    if (shoppingCartArticle.CollectState > 0)
                    {
                        var articleSid = FindArticle(shoppingCartSid, shoppingCartArticle);

                        if (orderform == shoppingCartSid || articleSid != null)
                            anyCollectInStore = true;
                    }
                }
            }
            else
            {
                // On marque chaque article selon sa provenance
                foreach (ShoppingCartEntities.Article shoppingCartArticle in orderform.Articles)
                {
                    var article = _shoppingCartToOrderFormArticleConverter.Convert(shoppingCartArticle);

                    if (article != null)
                    {
                        // on cherche l'origine de l'article
                        if (orderform == shoppingCartSid)
                        {
                            article.ShoppingCartOriginId = shoppingCartSid.SID;

                            if (shoppingCartArticle.CollectState > 0)
                            {
                                anyCollectInStore = true;
                            }
                        }
                        else
                        {
                            var articleSid = FindArticle(shoppingCartSid, shoppingCartArticle);
                            var articleUid = FindArticle(shoppingCartUid, shoppingCartArticle);
                            var productId = shoppingCartArticle.ProductID;
                            if (articleSid != null && articleSid.CollectState > 0)
                            {
                                anyCollectInStore = true;
                            }

                            if (articleUid != null && articleUid.CollectState > 0)
                            {
                                anyCollectInStore = true;
                            }

                            if (articleSid != null && productId == articleSid.ProductID)
                            {
                                article.ShoppingCartOriginId = shoppingCartSid.SID;
                            }

                            if (articleUid != null && productId == articleUid.ProductID)
                            {
                                article.ShoppingCartOriginId = shoppingCartUid.SID;
                            }
                        }

                        articleList.Add(article);
                    }
                }
            }

            // s'il y a qu'un seul article il se peut qu'il soit à retirer en magasin donc il faut prendre l'id du mag en question
            if (articleList.OfType<StandardArticle>().Any()
                && anyCollectInStore
                && orderform.ShippingAddresses.Cast<ShoppingCartEntities.ShopAddress>().Any())
            {
                var shoppingCartShopAddress = orderform.ShippingAddresses.Cast<ShoppingCartEntities.ShopAddress>().FirstOrDefault();

                if (shoppingCartShopAddress != null)
                {
                    transferShoppingCartResult.ShopAddress = new ShopAddress
                    {
                        Identity = shoppingCartShopAddress.Identity,
                        InternalId = shoppingCartShopAddress.InternalId
                    };
                }
            }

            transferShoppingCartResult.Articles = articleList;

            return transferShoppingCartResult;
        }

        public void TransfertOrderToOrderForm(BaseOF of, Guid g, int referenceType, OPContext opContext)
        {
            var previouslinegroups = _orderDetailsDAL.GetOrderDetails(g.ToString(), referenceType);

            of.Articles.Clear();
            of.LineGroups.Clear();

            if (previouslinegroups != null && previouslinegroups.Count > 0)
            {
                foreach (var odi in previouslinegroups)
                {
                    foreach (var a in odi.Articles)
                    {
                        if (a is MarketPlaceArticle)
                        {
                            AddMarketPlaceArticleToBasket(of.Articles, a.ProductID.GetValueOrDefault(0), (a as MarketPlaceArticle).OfferId, a.Quantity.GetValueOrDefault(0));
                            of.Articles.Modified = true;
                        }
                        else if ((StandardArticle)a != null)
                        {
                            AddArticleToBasket(of.Articles, of, a.ProductID.GetValueOrDefault(0), a.Quantity.GetValueOrDefault(0));
                            of.Articles.Modified = true;

                            if ((a as StandardArticle).Services != null && (a as StandardArticle).Services.Count > 0)
                            {
                                foreach (var serv in (a as StandardArticle).Services)
                                {
                                    var std = of.Articles.Find(currentstd => currentstd.ProductID.GetValueOrDefault(0) == a.ProductID.GetValueOrDefault(0));
                                    if (std != null)
                                    {
                                        AddServiceToBasket(of, std, serv.ProductID.GetValueOrDefault(0));
                                        of.Articles.Modified = true;
                                    }
                                }
                            }
                        }
                    }
                }
                of.PreviouslineGroups.Clear();
                of.PreviouslineGroups.AddRange(TransformToILineGroups(previouslinegroups));
                // Get Advantage code and set it if exist
                of.OrderInfo.AdvantageCode = _orderDetailsDAL.GetAdvantageCode(of.PreviouslineGroups[0].OrderInfo.OrderPk.GetValueOrDefault(0));
            }
        }

        public void TransfertOrderToPopOrderForm(PopOrderForm popOrderForm, Guid g, int referenceType)
        {
            var previouslinegroups = _orderDetailsDAL.GetOrderDetails(g.ToString(), referenceType);

            popOrderForm.LineGroups.Clear();

            var articleList = new ArticleList();

            if (previouslinegroups != null && previouslinegroups.Count > 0)
            {
                foreach (var odi in previouslinegroups)
                {
                    foreach (var a in odi.Articles)
                    {
                        if (a is MarketPlaceArticle)
                        {
                            AddMarketPlaceArticleToBasket(articleList, a.ProductID.GetValueOrDefault(0), (a as MarketPlaceArticle).OfferId, a.Quantity.GetValueOrDefault(0));
                        }
                        else if ((StandardArticle)a != null)
                        {
                            AddArticleToBasket(articleList, popOrderForm, a.ProductID.GetValueOrDefault(0), a.Quantity.GetValueOrDefault(0));
                            if ((a as StandardArticle).Services != null && (a as StandardArticle).Services.Count > 0)
                            {
                                foreach (var serv in (a as StandardArticle).Services)
                                {
                                    var std = popOrderForm.LineGroups.GetArticles().ToList().Find(currentstd => currentstd.ProductID.GetValueOrDefault(0) == a.ProductID.GetValueOrDefault(0));
                                    if (std != null)
                                    {
                                        AddServiceToBasket(popOrderForm, std, serv.ProductID.GetValueOrDefault(0));
                                    }
                                }
                            }
                        }
                    }
                }

                if (articleList.Count >= 1)
                {
                    popOrderForm.ArticlesModified = true;
                }

                popOrderForm.LineGroups.Add(popOrderForm.BuildTemporaryLineGroup(articleList) as PopLineGroup);

                // Get Advantage code and set it if exist
                popOrderForm.OrderGlobalInformations.AdvantageCode = _orderDetailsDAL.GetAdvantageCode(previouslinegroups[0].OrderPk.GetValueOrDefault(0));
            }
        }

        private ShoppingCartEntities.Article FindArticle(Orderform shoppingCart, ShoppingCartEntities.Article articleToFind)
        {
            if (shoppingCart == null)
            {
                return null;
            }

            if (shoppingCart.Articles == null)
            {
                return null;
            }

            foreach (ShoppingCartEntities.Article article in shoppingCart.Articles)
            {
                if (article.GetType() == articleToFind.GetType())
                {
                    if (article.ProductID.GetValueOrDefault() == articleToFind.ProductID.GetValueOrDefault())
                    {
                        return article;
                    }
                }
            }

            return null;
        }

        private List<LineGroup> TransformToILineGroups(List<PreviousLineGroup> previouslinegroups)
        {
            var lineGroups = new List<LineGroup>();

            foreach (var previouslinegroup in previouslinegroups)
            {
                var lineGroup = new LineGroup();

                lineGroup.OrderInfo.OrderPk = previouslinegroup.OrderPk;
                lineGroup.OrderInfo.OrderMessage = previouslinegroup.OrderMessage;
                lineGroup.OrderInfo.OrderReference = previouslinegroup.OrderReference;

                lineGroup.BillingAddress = previouslinegroup.BillingAddress;
                lineGroup.ShippingAddress = previouslinegroup.ShippingAddress;

                lineGroup.Articles = previouslinegroup.Articles;

                lineGroups.Add(lineGroup);
            }

            return lineGroups;
        }

        public void AddServiceToBasket(IGotLineGroups orderForm, Article masterArticle, int servId)
        {
            AddServiceToBasket<Service>(orderForm, masterArticle, servId);
        }

        public void AddServiceToBasket<T>(IGotLineGroups orderForm, Article masterArticle, int servId) where T : Service, new()
        {
            var service = new T
            {
                ProductID = servId,
                Type = ArticleType.Saleable,
                Quantity = masterArticle.Quantity,
                OriginCode = "FnacAff",
                InsertDate = DateTime.Now,
                IsLastAddedService = true
            };

            if (service is FnacDirect.OrderPipe.FDV.Model.ShopService)
            {
                service.Quantity = 1;
                var ServiceTemp = Front.WebBusiness.ArticleFactoryNew.GetArticle(DTO.ArticleReference.FromPrid(servId));
                if (ServiceTemp.Stocks != null)
                    service.ShopStocks = ServiceTemp.Stocks;
            }

            orderForm.Articles.Where(a => a is StandardArticle).ToList().ForEach(a => ((StandardArticle)a).Services.ForEach(s => s.IsLastAddedService = false));
            //orderForm.LineGroups.ForEach(lg => lg.Articles.Where(a => a is StandardArticle).ToList().ForEach(a => ((StandardArticle)a).Services.ForEach(s => s.IsLastAddedService = false)));

            foreach (var a in orderForm.Articles)
            {

                if (!(a is StandardArticle std))
                    continue;

                if (std.ID != masterArticle.ID || std.SellerID != masterArticle.SellerID)
                    continue;

                std.Services.Add(service);
                break;
            }

            // mise a jour du line group 
            foreach (var lineGroup in orderForm.LineGroups)
            {
                foreach (var a in lineGroup.Articles)
                {

                    if (!(a is StandardArticle std))
                        continue;

                    if (std.ID != masterArticle.ID || std.SellerID != masterArticle.SellerID)
                        continue;

                    std.Services.Add(service);

                    break;
                }
            }
        }

        /// <summary>
        /// Renvoie un article de la liste BaseOF.Articles
        /// </summary>
        /// <remarks>
        /// L'article en question peut provenir d'un LineGroup déconnecté après sérialisation
        /// </remarks>
        private static MarketPlaceArticle FindArticleInBaseOF(MarketPlaceArticle mparticle, IGotLineGroups of)
        {
            return of.Articles.OfType<MarketPlaceArticle>()
                              .FirstOrDefault(mpa => mpa.ProductID == mparticle.ProductID && mpa.OfferId == mparticle.OfferId);
        }

        /// <summary>
        /// Cherche et renvoie les articles correspondants dans les linegroups
        /// </summary>
        /// <param name="article"></param>
        /// <param name="of"></param>
        /// <returns></returns>
        private static ArticleList FindMatchingArticlesFromLineGroups(Article article, BasketOF of)
        {
            var articles = new ArticleList();

            foreach (var lg in of.LineGroups)
            {
                // l'article est standard
                if (article is StandardArticle standardArticle)
                {
                    foreach (var stdArt in CollectionUtils.FilterOnType<StandardArticle>(lg.Articles))
                    {
                        if (standardArticle.ProductID == stdArt.ProductID)
                            // l'article a été trouvé
                            articles.Add(stdArt);
                    }
                }

                // l'article est un ccv
                if (article is BM.VirtualGiftCheck virtualGiftCheck)
                {
                    foreach (var ccv in CollectionUtils.FilterOnType<BM.VirtualGiftCheck>(lg.Articles))
                    {
                        if (virtualGiftCheck.Amount == ccv.Amount)
                            // l'article a été trouvé
                            articles.Add(ccv);
                    }
                }

                // l'article est un MP
                if (article is MarketPlaceArticle marketPlaceArticle)
                {
                    foreach (var mpart in CollectionUtils.FilterOnType<MarketPlaceArticle>(lg.Articles))
                    {
                        if ((marketPlaceArticle.ID == mpart.ID) && (marketPlaceArticle.SellerID == mpart.SellerID))
                            // l'article a été trouvé
                            articles.Add(mpart);
                    }
                }
            }
            return articles;
        }

        /// <summary>
        /// Renvoie un article de la liste BaseOF.Articles
        /// </summary>
        /// <remarks>
        /// L'article en question peut provenir d'un LineGroup déconnecté après sérialisation
        /// </remarks>
        private static Article FindArticleInBaseOF(BM.VirtualGiftCheck ccv, IGotLineGroups of)
        {
            return of.Articles.OfType<BM.VirtualGiftCheck>().FirstOrDefault(a => a.Amount == ccv.Amount);
        }

        /// <summary>
        /// Renvoie un article de la liste BaseOF.Articles
        /// </summary>
        /// <remarks>
        /// L'article en question peut provenir d'un LineGroup déconnecté après sérialisation
        /// </remarks>
        public static Article FindArticleInBaseOF(Article article, IGotLineGroups of)
        {
            if (article != null)
            {
                if (article is BM.VirtualGiftCheck)
                    return FindArticleInBaseOF(article as BM.VirtualGiftCheck, of);
                if (article is StandardArticle)
                    return FindArticleInBaseOF(article as StandardArticle, of);
                if (article is MarketPlaceArticle)
                    return FindArticleInBaseOF(article as MarketPlaceArticle, of);
            }

            return null;
        }

        /// <summary>
        /// Renvoie un article de la liste BaseOF.Articles
        /// </summary>
        /// <remarks>
        /// L'article en question peut provenir d'un LineGroup déconnecté après sérialisation
        /// </remarks>
        private static Article FindArticleInBaseOF(StandardArticle standard, IGotLineGroups of)
        {
            foreach (var art in CollectionUtils.FilterOnType<StandardArticle>(of.Articles))
            {
                if (art.ProductID == standard.ProductID)
                    return art;
            }
            return null;
        }

        /// <summary>
        /// Effectue un merge de la quantité des articles du BaseOF.LineGroups vers BaseOF.Articles
        /// </summary>
        /// <param name="of"></param>
        public static void MergeArticlesAndLineGroups(BaseOF of)
        {
            var articlesNotFound = new ArticleList(); // articles orphelins
            var hasNewArticles = false;

            foreach (var of_article in of.Articles)
            {
                // 1 - trouve la correspondance des articles de l'OF et des linegroups
                var lg_articles = FindMatchingArticlesFromLineGroups(of_article, of);
                if (lg_articles == null || lg_articles.Count <= 0)
                    articlesNotFound.Add(of_article);
                else
                {
                    // 2 - met à jour la quantité
                    var quantity = 0;
                    foreach (var art in lg_articles)
                        quantity += art.Quantity.GetValueOrDefault(0);
                    of_article.Quantity = quantity;
                    // 2 - a - met à jour la quantité des services
                    if (of_article is StandardArticle stdArt)
                        foreach (var service in stdArt.Services)
                            service.Quantity = stdArt.Quantity;
                }
            }

            // 3 - trouve les nouveaux articles dans les linegroups
            // (a - ce sont surtout des ccv créés dans la page de livraison)
            // (b - ce sont des articles qui n'ont pas de correspondance avec ceux du BaseOF.Articles)
            foreach (var lg in of.LineGroups)
            {
                foreach (var art in lg.Articles)
                {
                    var artof = FindArticleInBaseOF(art, of);
                    if (artof == null)
                    {
                        of.Articles.Add(art);
                        hasNewArticles = true;
                    }
                }
            }

            // 4 - nettoie les articles orphelins
            foreach (var of_article in articlesNotFound)
            {
                of.Articles.Remove(of_article);
            }

            if (hasNewArticles)
            {
                // 3 - c - merge les nouveaux articles
                MergeArticlesAndLineGroups(of);
            }
        }

        public void AddArticleToBasket(OPContext opContext, int productId, int quantity)
        {
            var baseOF = opContext.OF as BaseOF;
            AddArticleToBasket(baseOF.Articles, baseOF, productId, quantity);
        }

        public void AddArticleToBasket(ArticleList articleList, IGotLineGroups of, int productId, int quantity)
        {

            var ar = new DTO.ArticleReference(productId, null, string.Empty, string.Empty);
            var siteContext = _applicationContext.GetSiteContext();
            var article = _articleBusiness.GetArticle(siteContext, ar);
            if (article != null)
            {
                StandardArticle art;
                if (article.Core.IsBundle)
                    art = new Bundle();
                else
                    art = new StandardArticle();

                art.ProductID = productId;
                art.Type = ArticleType.Saleable;
                art.Quantity = quantity;
                var articleFound = FindArticleInBaseOF(art, of);
                if (articleFound == null)
                {
                    articleList.Add(art);
                }
            }
        }

        private void AddMarketPlaceArticleToBasket(ArticleList articleList, int productId, int offerId, int quantity)
        {
            if (productId != 0 && offerId != 0)
            {
                var mart = new MarketPlaceArticle
                {
                    ProductID = productId,
                    Type = ArticleType.Saleable,
                    OfferId = offerId,
                    Quantity = quantity
                };
                articleList.Add(mart);
            }
        }

        public void AddMarketPlaceArticleToBasket(ArticleList articleList, int productId, string offerReference, int quantity, OPContext context)
        {

            if (productId != 0 && !string.IsNullOrEmpty(offerReference))
            {
                try
                {
                    var gOfferRef = new Guid(offerReference);
                    var mpb = new MarketPlaceBusiness(context.SiteContext);
                    var offerId = mpb.GetOfferId(gOfferRef);
                    AddMarketPlaceArticleToBasket(articleList, productId, offerId, quantity);
                }
                catch (Exception)
                {
                    // Because we don't care ?
                }
            }

        }

        public void AddMarketPlaceArticleToBasket(BaseOF orderForm, int productId, string offerReference, int quantity, OPContext context)
        {
            AddMarketPlaceArticleToBasket(orderForm.Articles, productId, offerReference, quantity, context);
        }
    }
}
