using System;
using System.Collections.Generic;
using System.Linq;

namespace FnacDirect.OrderPipe.Base.Model.Solex
{
    public class ShippingChoice
    {
        public ShippingChoice()
        {
        }

        public ShippingChoice(ShippingChoice source)
        {
            MainShippingMethodId = source.MainShippingMethodId;
            Weight = source.Weight;
            TotalPrice = source.TotalPrice;
            Parcels = source.Parcels.ToDictionary(parcel => parcel.Key, parcel => new Parcel(parcel.Value));
            Groups = source.Groups.Select(group => new Group(group)).ToList();
            AppliedPromotions = source.AppliedPromotions?.Select(promotion => new Promotion(promotion)).ToList();
            Label = source.Label;
            AltLabel = source.AltLabel;
            Description = source.Description;
            IsAvailable = source.IsAvailable;
            CarbonFootprint = source.CarbonFootprint;
        }

        public virtual bool IsAvailable { get; set; } = true;

        public int MainShippingMethodId { get; set; }
        public int Weight { get; set; }
        public decimal? CarbonFootprint { get; set; }
        public virtual IDictionary<int, Parcel> Parcels { get; set; }
        public virtual IEnumerable<Group> Groups { get; set; }
        public virtual IEnumerable<Promotion> AppliedPromotions { get; set; }
        public virtual GenericPrice TotalPrice { get; set; } = new GenericPrice();
        public virtual IEnumerable<ShippingChoice> GetInnerChoices() { return Enumerable.Empty<ShippingChoice>(); }

        public string Label { get; set; }
        public string AltLabel { get; set; }
        public string Description { get; set; }

        public virtual bool ContainsShippingMethodId(int shippingMethodId)
        {
            return shippingMethodId == MainShippingMethodId;
        }
        public virtual Dates GetDates(BaseArticle article)
        {
            var parcel = GetParcel(article);

            if (parcel == null)
            {
                return null;
            }

            return new Dates
            {
                DeliveryDateMax = parcel.Source.DeliveryDateMax,
                DeliveryDateMin = parcel.Source.DeliveryDateMin,
                ShippingDate = parcel.Source.ShippingDate,
            };

        }

        public Parcel GetParcel(Article article)
        {
            if (article?.ProductID == null)
            {
                return null;
            }

            var parcelId = Groups
                .SelectMany(g => g.Items)
                .FirstOrDefault(i => i.Identifier.Prid == article.ProductID)?
                .ParcelId;

            if (!parcelId.HasValue)
            {
                return null;
            }

            Parcels.TryGetValue(parcelId.Value, out var parcel);
            return parcel;
        }

        public virtual ShippingChoice SelectedChoice
        {
            get { return this; }
        }
        public virtual ShippingChoice DefaultChoice
        {
            get { return this; }
        }

        public virtual ShippingChoice GetChoice(int shippingMethodId)
        {
            return MainShippingMethodId == shippingMethodId ? this : null;
        }

        public virtual ShippingChoice GetChoice()
        {
            return this;
        }

        public virtual ShippingChoice Copy()
        {
            return new ShippingChoice(this);
        }
    }
    public class Dates
    {
        public DateTime ShippingDate { get; set; }
        public DateTime DeliveryDateMin { get; set; }
        public DateTime DeliveryDateMax { get; set; }
    }
    public class Group
    {
        public int Id { get; set; }
        public IEnumerable<ItemInGroup> Items { get; set; }
        public Group(Group source)
        {
            Id = source.Id;
            Items = source.Items.Select(item => new ItemInGroup(item)).ToList();
            GlobalPrice = new GenericPrice(source.GlobalPrice);
        }
        public Group()
        {
        }
        public GenericPrice GlobalPrice { get; set; } = new GenericPrice();
    }

    public class ItemInGroup
    {
        public Identifier Identifier { get; set; }
        public int Quantity { get; set; }
        public GenericPrice UnitPrice { get; set; } = new GenericPrice();
        public int ParcelId { get; set; }
        public ItemInGroup(ItemInGroup source)
        {
            Identifier = new Identifier(source.Identifier);
            Quantity = source.Quantity;
            UnitPrice = source.UnitPrice;
            ParcelId = source.ParcelId;
        }
        public ItemInGroup()
        {
        }
    }

    public class Identifier
    {
        public int? Prid { get; set; }
        public Guid? OfferReference { get; set; }
        public int? PurId { get; set; }
        public Identifier(Identifier source)
        {
            Prid = source.Prid;
            OfferReference = source.OfferReference;
            PurId = source.PurId;
        }
        public Identifier()
        {
        }
    }

    public class Parcel
    {
        public int ShippingMethodId { get; set; }
        public int ShippingServiceId { get; set; }
        public int DelayInHours { get; set; }
        public Source Source { get; set; }
        public Source SelectedSlotSource { get; set; }
        public int ParcelId { get; set; }

        public Parcel(Parcel parcel)
        {
            ShippingMethodId = parcel.ShippingMethodId;
            ShippingServiceId = parcel.ShippingServiceId;
            DelayInHours = parcel.DelayInHours;
            Source = new Source(parcel.Source);
            if (parcel.SelectedSlotSource != null)
            {
                SelectedSlotSource = new Source(parcel.SelectedSlotSource);
            }
        }
        public Parcel()
        {
        }
    }

    public class Source
    {
        public StockLocation StockLocation { get; set; }
        public ShippingLocation ShippingLocation { get; set; }
        public DateTime DeliveryDateMin { get; set; }
        public DateTime DeliveryDateMax { get; set; }
        public DateTime ShippingDate { get; set; }
        public DateTime OrderDateMax { get; set; }
        public Source(Source source)
        {
            StockLocation = new StockLocation(source.StockLocation);
            ShippingLocation = new ShippingLocation(source.ShippingLocation);
            DeliveryDateMax = source.DeliveryDateMax;
            DeliveryDateMin = source.DeliveryDateMin;
            ShippingDate = source.ShippingDate;
            OrderDateMax = source.OrderDateMax;
        }
        public Source()
        {
        }

    }

    public class ShippingLocation
    {
        public string Type { get; set; }
        public string Identifier { get; set; }
        public ShippingLocation(ShippingLocation source)
        {
            Type = source.Type;
            Identifier = source.Identifier;
        }
        public ShippingLocation()
        {
        }
    }

    public class StockLocation
    {
        public string Type { get; set; }
        public string Identifier { get; set; }
        public StockLocation(StockLocation source)
        {
            Type = source.Type;
            Identifier = source.Identifier;
        }
        public StockLocation()
        {
        }
    }

    public class Promotion
    {
        public string TriggerKey { get; set; }
        public int Quantity { get; set; }
        public string PromotionCode { get; set; }
        public List<int> ApplyToLogisticTypes { get; set; }
        public string AdditionalRuleParam1 { get; set; }
        public string AdditionalRuleParam2 { get; set; }
        public Promotion(Promotion source)
        {
            TriggerKey = source.TriggerKey;
            Quantity = source.Quantity;
            PromotionCode = source.PromotionCode;
            ApplyToLogisticTypes = source.ApplyToLogisticTypes;
            AdditionalRuleParam1 = source.AdditionalRuleParam1;
            AdditionalRuleParam2 = source.AdditionalRuleParam2;
        }
        public Promotion()
        {
        }
    }
}
