using FnacDirect.OrderPipe.Base.DAL;
using FnacDirect.OrderPipe.Contracts.Model.Tickets;
using FnacDirect.OrderPipe.Contracts.Services.Tickets;
using System;
using System.Collections.Generic;
using System.Linq;

namespace FnacDirect.OrderPipe.Base.Business.Tickets
{
    public class TicketService : ITicketService
    {
        private readonly ITicketDAL _ticketDAL;

        public TicketService(ITicketDAL ticketDal)
        {
            _ticketDAL = ticketDal;
        }

        public void AddTicket(int ticketType, int ticketSequenceNumber, int mainOrderId, int associatedTypeId, string refUg, string countryCode, string appScope, DateTime? groupDate, string checkoutNumber)
        {
            _ticketDAL.AddTicket(ticketType,
                1, //SourceProcessID : 1 = Front-FnacShop
                ticketSequenceNumber,
                1, //TicketStatus : 1 = Initialized
                mainOrderId,
                associatedTypeId,
                refUg,
                countryCode,
                appScope,
                groupDate,
                checkoutNumber);
        }

        public void AddMainOrderDocuments(IEnumerable<Document> tickets, int mainOrderId)
        {
            if (tickets != null)
            {
                foreach (var document in tickets)
                {
                    var toSave = document.Content;
                    if (toSave.Contains('\0'))
                    {
                        for (int i = 0; i < toSave.Length; i++)
                        {
                            if (toSave[i] == '\0')
                                toSave[i] = '\n';
                        }
                    }

                    _ticketDAL.AddMainOrderDocument((int)document.TicketType, mainOrderId, toSave, document.TicketData);
                }
            }
        }

        public void InsertGuptTicketNumber(int mainOrderId, string guptTicketId)
        {
            if (mainOrderId != 0 && !string.IsNullOrEmpty(guptTicketId))
            {
                _ticketDAL.InsertGuptTicketNumber(mainOrderId, guptTicketId);
            }
        }

        /// <summary>
        /// Returne true si un ticket ayant le meme associate Id est deja present en bdd
        /// </summary>
        /// <param name="ticketType"></param>
        /// <param name="mainOrderId"></param>
        /// <param name="associatedTypeId"></param>
        /// <returns></returns>
        public bool HasExistingTicketFlowInserted(int ticketType, int mainOrderId, int associatedTypeId)
        {
            return _ticketDAL.HasExistingTicketInserted(ticketType, mainOrderId, associatedTypeId);
        }

        public string GetMainOrderDocumentByUserRef(string userRef)
        {
            return _ticketDAL.GetMainOrderDocumentByUserRef(userRef);
        }
    }
}
