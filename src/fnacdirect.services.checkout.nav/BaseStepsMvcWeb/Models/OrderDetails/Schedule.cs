﻿using System.Collections.Generic;

namespace FnacDirect.OrderPipe.Base.Business.OrderDetail
{
    public class Schedule
    {
        public List<TimeTable> TimeTables { get; set; }

        public ExceptionnalTimeTable ExceptionnalTimeTables { get; set; }
    }
}
