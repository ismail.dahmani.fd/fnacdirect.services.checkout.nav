﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using System.Linq;
using StructureMap;
using FnacDirect.Technical.Framework.Web.Mvc;
using FnacDirect.Context;
using FnacDirect.Contracts.Online.Model;
using FnacDirect.Customer.Model;
using FnacDirect.OrderPipe.Base.Business;
using FnacDirect.OrderPipe.BaseMvc.Web.Services.Pop;
using FnacDirect.OrderPipe.Base.Business.Payment;
using FnacDirect.Technical.Framework.ServiceLocation;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Validations
{
    public class CreditCardHolderInformationsValidationAttribute : ValidationAttribute, IClientValidatable
    {
        private readonly IApplicationContext _applicationContext;
        private readonly IPaymentSelectionBusiness _paymentSelectionBusiness;
        private readonly SiteContext _siteContext;
        private readonly string _culture;
        private readonly string _configShippingMethodSelection;

        public CreditCardHolderInformationsValidationAttribute()
        {
            _applicationContext = ServiceLocator.Current.GetInstance<IApplicationContext>();
            _siteContext = _applicationContext.GetSiteContext();
            _culture = _siteContext != null ? _siteContext.Culture : "";
            _configShippingMethodSelection = _applicationContext.GetAppSetting("config.shippingMethodSelection");

            _paymentSelectionBusiness = ServiceLocator.Current.GetInstance<IPaymentSelectionBusiness>();
        }

        public IEnumerable<ModelClientValidationRule> GetClientValidationRules(ModelMetadata metadata, ControllerContext context)
        {
            var modelClientValidationRule = new ModelClientValidationRule
            {
                ErrorMessage = _paymentSelectionBusiness.GetErrorMessage("OP.CARD.ErrMsg.holder"),
                ValidationType = "creditcardholderinformationsvalidation"
            };

            yield return modelClientValidationRule;
        }

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            //Value : Field (Firstname or Lastname)
            if (!(value is string))
                return ValidationResult.Success;

            var holderInfo = value.ToString();

            return CheckCreditCardHolderName(holderInfo);
        }

        private ValidationResult CheckCreditCardHolderName(string name)
        {
            var validationResult = _paymentSelectionBusiness.CheckSipsCreditCardHolderName(name);

            return validationResult;
        }
    }
}
