namespace FnacDirect.OrderPipe.Base.Business.Configuration
{
    public interface IMBWayConfigurationProvider
    {
        MBWayConfiguration Get();
    }
}
