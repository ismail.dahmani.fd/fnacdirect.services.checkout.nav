
using System;
using System.Linq;
using FnacDirect.Contracts.Online.Model;
using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.OrderPipe.Base.Model.Pop;
using FnacDirect.Technical.Framework.Web.Mvc;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Basket
{
    public class SplitViewModelBuilder : ISplitViewModelBuilder
    {
        private readonly IPriceHelper _priceHelper;
        private readonly bool _isFnacPro;

        public SplitViewModelBuilder(IApplicationContext applicationContext)
        {
            _priceHelper = applicationContext.GetPriceHelper();
            _isFnacPro = applicationContext.GetSiteId() == (int)FnacSites.FNACPRO;
        }
        public SplitViewModel Build(PopOrderForm popOrderForm)
        {
            var splitData = popOrderForm.GetPrivateData<SplitData>(false);            

            var isApplied = IsApplied(popOrderForm);
            if (splitData != null)
            {
                var price = isApplied ? SplitCost(popOrderForm) : splitData.Price;
                var priceNoVAT = isApplied ? SplitCostNoVAT(popOrderForm) : splitData.PriceNoVAT;
                return new SplitViewModel
                {
                    IsApplied = isApplied,
                    IsAvailable = splitData.IsAvailable,
                    IsFree = price == decimal.Zero,
                    Price = _priceHelper.Format(price, PriceFormat.CurrencyAsDecimalSeparator),
                    PriceModel = _priceHelper.GetDetails(price),
                    PriceNoVAT = _priceHelper.Format(priceNoVAT, PriceFormat.CurrencyAsDecimalSeparator),
                    PriceNoVATModel = _priceHelper.GetDetails(priceNoVAT),
                    PriceToDisplay = _isFnacPro ? _priceHelper.Format(priceNoVAT, PriceFormat.CurrencyAsDecimalSeparator) :
                                                  _priceHelper.Format(price, PriceFormat.CurrencyAsDecimalSeparator),
                    PriceModelToDisplay = _isFnacPro ? _priceHelper.GetDetails(priceNoVAT) :
                                                       _priceHelper.GetDetails(price)
                };
            }
            return null;
        }

        public decimal SplitCost(PopOrderForm popOrderForm)
        {
            return popOrderForm.LineGroups.Fnac().Sum(lg => lg.GlobalPrices.TotalSplitPriceDBEur.GetValueOrDefault());
        }

        public decimal SplitCostNoVAT(PopOrderForm popOrderForm)
        {
            return popOrderForm.LineGroups.Fnac().Sum(lg => lg.GlobalPrices.TotalSplitPriceDBNoVatEur.GetValueOrDefault());
        }

        public bool IsApplied(PopOrderForm popOrderForm)
        {
            return popOrderForm.LineGroups.Fnac().SelectMany(lg => lg.LogisticTypes).Any(lg => lg.SplitLevel == 1);
        }
    }
}
