using System.Collections.Generic;
using FnacDirect.OrderPipe.Base.Model;
using Contract = FnacDirect.Solex.AppointmentDelivery.Client.Contract;

namespace FnacDirect.OrderPipe.Base.Business.Shipping
{
    public class ItemsForAppointmentDeliveryRequest
    {
        public ItemsForAppointmentDeliveryRequest(List<Contract.Item> items, List<Contract.Promotion> promotions)
        {
            Promotions = promotions;
            Items = items;
        }
        public List<Contract.Promotion> Promotions { get; }
        public List<Contract.Item> Items { get; }
    }

    internal class ItemsForAppointmentDeliveryRequestWitEligibleArticles : ItemsForAppointmentDeliveryRequest
    {
        public ItemsForAppointmentDeliveryRequestWitEligibleArticles(List<Contract.Promotion> promotions, List<Contract.Item> items,List<Article> eligibleArticles) : base(items, promotions)
        {
            EligibleArticles = eligibleArticles;
        }
        public List<Article> EligibleArticles { get; }
    }
}
