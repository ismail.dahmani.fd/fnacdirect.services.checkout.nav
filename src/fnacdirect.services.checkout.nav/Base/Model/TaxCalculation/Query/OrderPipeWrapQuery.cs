using FnacDirect.OrderPipe.TaxCalculation.Front.Models;
using System.Collections.Generic;
using System.Linq;

namespace FnacDirect.OrderPipe.Base.Model.TaxCalculation
{
    public class OrderPipeWrapQuery : Query
    {
        public List<WrapPriceQuery> WrapPrices { get; set; }
        public int TaxType { get; set; }

        public WrapsQuery ToWrapsQuery()
        {
            var wrapQuery = new WrapsQuery
            {
                OrderContext = OrderContext.ToOrderContext(),
                TaxType = TaxType,
                WrapPrices = WrapPrices.Select(w => new WrapPrice(w.Id, w.PriceGlobal, w.PriceDetail)).ToList()
            };

            return wrapQuery;
        }
    }

    public class WrapPriceQuery
    {
        public int Id { get; set; }
        public decimal PriceGlobal { get; set; }
        public decimal PriceDetail { get; set; }

        public WrapPriceQuery(int id, decimal priceGlobal, decimal priceDetail)
        {
            Id = id;
            PriceGlobal = priceGlobal;
            PriceDetail = priceDetail;
        }
    }   
}
