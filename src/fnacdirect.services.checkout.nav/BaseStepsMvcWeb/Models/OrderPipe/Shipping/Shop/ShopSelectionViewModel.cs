﻿namespace FnacDirect.OrderPipe.BaseMvc.Web.Pop.ApiControllers.Models
{
    public class ShopSelectionViewModel
    {
        public int ShopId { get; set; }
    }
}
