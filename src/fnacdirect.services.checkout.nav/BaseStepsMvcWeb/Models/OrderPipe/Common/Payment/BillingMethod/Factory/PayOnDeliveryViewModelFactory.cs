﻿using FnacDirect.OrderPipe.Base.Model;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Common.Payment.BillingMethodFactories
{
    public class PayOnDeliveryViewModelFactory : BasePaymentViewModelFactory<PayOnDeliveryBillingMethod>
    {
        public override BillingMethodViewModel Get(string tabName, PaymentMethod paymentMethod, OPContext opContext, PopOrderForm popOrderForm)
        {
            var viewModel = new BillingMethodViewModel()
            {
                Display = popOrderForm.PayOnDeliverySelected()
            };

            viewModel.DisplayAlone = viewModel.Display;

            return viewModel;
        }
    }
}
