﻿using FnacDirect.OrderPipe.Base.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.TagCommander
{
    public interface ITagCommanderViewModelBuilder
    {
        TagCommanderThankYouViewModel BuildThankYou(PopOrderForm popOrderForm, OPContext opContext);
        TagCommanderOrderViewModel Build(PopOrderForm popOrderForm, OPContext opContext);
    }
}
