﻿using FnacDirect.OrderPipe.Base.Model;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Basket
{
    public interface IHideBillingInfoViewModelBuilder
    {
        HideBillingInfoViewModel Build(PopOrderForm popOrderForm);
    }
}
