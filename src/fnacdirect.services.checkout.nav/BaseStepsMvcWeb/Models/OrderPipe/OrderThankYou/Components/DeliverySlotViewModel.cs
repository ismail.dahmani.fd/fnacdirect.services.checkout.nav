using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Solex.Appointment
{
    public class DeliverySlotViewModel
    {
        public string SlotStartDate { get; set; }
        public string SlotEndDate { get; set; }

        public bool IsSameDeliveryDateForAllArticles { get; set; }
    }
}
