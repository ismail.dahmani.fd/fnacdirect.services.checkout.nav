namespace FnacDirect.OrderPipe.Base.Business
{
    public interface IOrderDiscountBusiness
    {
        void AddOrderDiscount(int orderId, int orderDetailId, int discountTypeId, int discountReasonId, decimal discountAmount, decimal? s18DiscountAmount = null, string s18DiscountCode = null, string s18DiscountType = null, int? typeId = null, string code = null, int? templateId = null, string respComment = null, string sellerComment = null);
    }
}
