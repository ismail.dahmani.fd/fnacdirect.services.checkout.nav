using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FnacDirect.OrderPipe.BaseMvc.Web.CrossSellBasket
{
    public class EmptyCrossSellBasketViewModel
    {
        private readonly string _orderPipeUrl;

        public EmptyCrossSellBasketViewModel(string orderPipeUrl)
        {
            _orderPipeUrl = orderPipeUrl;            
        }

        public string OrderPipeUrl
        {
            get
            {
                return _orderPipeUrl;
            }
        }

        public bool IsFnacPro { get; set; }

        private List<string> _errorMessages
     = new List<string>();

        public List<string> ErrorMessages
        {
            get
            {
                return _errorMessages;
            }
        }
    }
}
