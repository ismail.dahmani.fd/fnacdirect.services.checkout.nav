using FnacDirect.OrderPipe.Base.BaseModel.Model;
using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.OrderPipe.Base.Proxy.Appointment;
using FnacDirect.OrderPipe.Base.Proxy.Membership;
using FnacDirect.OrderPipe.Core.Services;
using FnacDirect.Technical.Framework.Web.Mvc;
using System.Linq;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Common.Shipping
{
    public class BillingOnlyViewModelBuilder : IBillingOnlyViewModelBuilder
    {
        private readonly IAdherentCardArticleService _adherentCardArticleService;
        private readonly IAppointmentService _appointmentService;
        private readonly IPipeParametersProvider _pipeParametersProvider;
        private readonly IApplicationContext _applicationContext;


        public BillingOnlyViewModelBuilder(IAdherentCardArticleService adherentCardArticleService,
                                           IAppointmentService appointmentService,
                                           IPipeParametersProvider pipeParametersProvider,
                                           IApplicationContext applicationContext)
        {
            _adherentCardArticleService = adherentCardArticleService;
            _appointmentService = appointmentService;
            _pipeParametersProvider = pipeParametersProvider;
            _applicationContext = applicationContext;
        }

        public BillingOnlyViewModel Build(PopOrderForm popOrderForm, int sellerId)
        {
            var billingOnlyViewModel = new BillingOnlyViewModel();
            var aboIlliData = popOrderForm.GetPrivateData<AboIlliData>();
            var lineGroups = popOrderForm.LineGroups.Seller(sellerId);

            if (lineGroups.Any(l => l.HasNumericalArticles))
            {
                billingOnlyViewModel.HasEbook = true;
            }

            if (lineGroups.Any(l => l.HasDematSoftArticles))
            {
                billingOnlyViewModel.HasDematSoft = true;
            }

            if (lineGroups.SelectMany(l => l.Articles).Any(a => a.ProductID.HasValue && _adherentCardArticleService.IsAdherentCard(a.ProductID.Value)))
            {
                billingOnlyViewModel.HasAdherentCard = true;
            }

            if (lineGroups.SelectMany(l => l.Articles).OfType<Service>().Any())
            {
                billingOnlyViewModel.HasService = true;
            }

            if (aboIlliData != null && aboIlliData.HasAboIlliInBasket)
            {
                billingOnlyViewModel.HasExpressPlus = true;
            }

            var appointmentTypeIds = _appointmentService.GetAppointmentArticleTypeIds();

            if (lineGroups.SelectMany(l => l.Articles).Any(a => a.TypeId.HasValue && appointmentTypeIds.Contains(a.TypeId.Value)))
            {
                billingOnlyViewModel.HasAppointment = true;
            }

            var eccvTypeIds = _pipeParametersProvider.GetAsRangeSet<int>("eccv.typeid");
            var hasECCVInBasket = lineGroups.GetArticles().Any(a => a.TypeId.HasValue && eccvTypeIds.Contains(a.TypeId.Value));

            if (hasECCVInBasket)
            {
                billingOnlyViewModel.HasECCV = true;
            }

            if (int.TryParse(_applicationContext.GetAppSetting("DonationTypeId"), out var donationTypeId))
            {
                if (lineGroups.SelectMany(l => l.Articles).Any(a => a.TypeId.HasValue && donationTypeId == a.TypeId.Value))
                {
                    billingOnlyViewModel.HasDonation = true;
                }
            }

            if (lineGroups.Any(l => l.Articles.ContainsSupport(3848)))
            {
                billingOnlyViewModel.HasGamingSubscription = true;
            }

            return billingOnlyViewModel;
        }
    }
}
