using System;

namespace FnacDirect.OrderPipe.Base.BaseModel.Model
{
    public class AccountScoring
    {
        public int UserConfidence { get; set; }

        public DateTime? LastOrderDate { get; set; }

        public AccountScoring()
        {
            AccountLastScoringStatus = null;
        }

        public string AccountLastScoringStatus { get; set; }
    }
}
