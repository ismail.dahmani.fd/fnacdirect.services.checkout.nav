using FnacDirect.OrderPipe.Base.Model.Pop;
using FnacDirect.OrderPipe.BaseMvc.Web.Models.Common;
using FnacDirect.OrderPipe.BaseMvc.Web.Pop.Models.Common;
using System;
using System.Collections.Generic;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Models
{
    public abstract class BaseRootViewModel
    {
        public Guid OrderReference { get; set; } 
        public bool IsFnacPro { get; set; }
        public bool IsGuest { get; set; }
        public string LocalCountry { get; set; }
        public string DomainId { get; set; }
        public string PipeRoute { get; set; }
        public NavigationViewModel Navigation { get; set; }
        public BreadCrumbViewModel BreadCrumb { get; set; }
        public List<PipeMessageViewModel> PipeMessages { get; set; }
        public SortedDictionary<string, bool> Configuration { get; set; }
    }
}
