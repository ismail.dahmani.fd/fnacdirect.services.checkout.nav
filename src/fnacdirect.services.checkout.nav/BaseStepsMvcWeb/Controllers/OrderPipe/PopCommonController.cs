﻿using System.Web.Mvc;
using FnacDirect.OrderPipe.BaseMvc.Web.Pop.Models.Common;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Controllers.Pop
{
    public class PopCommonController : PopBaseController
    {
        private readonly OPContext _opContext;
        private readonly IBreadCrumbViewModelBuilder _breadCrumbViewModelBuilder;

        public PopCommonController(IBreadCrumbViewModelBuilder breadCrumbViewModelBuilder,
                                   OPContext opContext)
        {
            _opContext = opContext;
            _breadCrumbViewModelBuilder = breadCrumbViewModelBuilder;
        }

        [ChildActionOnly]
        public ActionResult BreadCrumb()
        {
            var viewModel = _breadCrumbViewModelBuilder.Build(_opContext,(a, b) => Url.RouteUrl(a, b));

            return PartialView("_BreadCrumb", viewModel);
        }
    }
}
