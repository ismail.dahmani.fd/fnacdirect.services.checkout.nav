﻿using FnacDirect.OrderPipe.Base.Model;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Basket
{
    public interface IRichBasketItemViewModelBuilder
    {
        RichBasketItemViewModel Build(BaseArticle baseArticle, IOF orderForm);
        RichBasketItemViewModel Build(BaseArticle baseArticle, IRichBasketItemViewModelBuilderCallbacks callbacks, IOF orderForm);
    }

    public interface IRichBasketItemViewModelBuilderCallbacks
    {
        RichBasketItemViewModel BuildingStandardViewModel(RichBasketItemViewModel current, StandardArticle standardArticle);
        RichBasketItemViewModel BuildingMarketPlaceViewModel(RichBasketItemViewModel current, MarketPlaceArticle marketPlaceArticle);

        AvailabilityViewModel OnComputingAvailability(StandardArticle standardArticle);
    }
}
