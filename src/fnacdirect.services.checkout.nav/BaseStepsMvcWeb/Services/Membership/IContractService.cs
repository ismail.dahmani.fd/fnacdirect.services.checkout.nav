﻿using System;
using System.Collections.Generic;
using FnacDirect.Membership.Model.Entities;

namespace FnacDirect.OrderPipe.Base.Proxy.Membership
{
    public interface IContractService
    {
        IEnumerable<Contract> GetContractsByAdherentNumber(string adherentNumber, DateTime birthDate);
        Contract GetGamingContract(IEnumerable<Contract> contracts);
        Contract GetMembershipContract(IEnumerable<Contract> contracts);
    }
}
