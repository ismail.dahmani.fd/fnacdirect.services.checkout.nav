using FnacDirect.OrderPipe.Base.Model;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Common.Shipping
{
    public static class BillingAddressExtensions
    {
        private const string UNKNOW_TAXE_ID_NUMBER_TYPE = "-1";

        public static AddressViewModel ConvertToAddressViewModel(this BillingAddress billingAddress, int countryId, bool hasMissingInformations = false)
        {
            var addressViewModel = new AddressViewModel
            {
                Id = billingAddress.Identity,
                Reference = billingAddress.Reference
            };

            if (string.IsNullOrEmpty(billingAddress.Alias))
            {
                addressViewModel.Alias = string.Format("{0} {1} {2}",
                    billingAddress.Firstname,
                    billingAddress.Lastname,
                    billingAddress.City);
            }
            else
            {
                addressViewModel.Alias = billingAddress.Alias;
            }

            addressViewModel.ShouldDisplayCompany = !string.IsNullOrEmpty(billingAddress.Company);
            addressViewModel.CompanyName = billingAddress.Company;

            addressViewModel.AddressLine1 = billingAddress.AddressLine1;

            addressViewModel.ShouldDisplayAddressLine2 = !string.IsNullOrEmpty(billingAddress.AddressLine2);
            addressViewModel.AddressLine2 = billingAddress.AddressLine2;
            addressViewModel.ShouldDisplayAddressLine3 = !string.IsNullOrEmpty(billingAddress.AddressLine3);
            addressViewModel.AddressLine3 = billingAddress.AddressLine3;

            addressViewModel.City = billingAddress.City;
            addressViewModel.ZipCode = billingAddress.ZipCode;
            addressViewModel.Lastname = billingAddress.Lastname;

            addressViewModel.City = billingAddress.City;
            addressViewModel.ZipCode = billingAddress.ZipCode;
            addressViewModel.Lastname = billingAddress.Lastname;
            addressViewModel.Firstname = billingAddress.Firstname;
            addressViewModel.CountryId = countryId;
            addressViewModel.ShouldDisplayCountry = countryId != billingAddress.CountryId;
            if (addressViewModel.ShouldDisplayCountry)
            {
                addressViewModel.Country = billingAddress.CountryLabel;
            }

            addressViewModel.State = billingAddress.State;

            addressViewModel.HasMissingInformations = hasMissingInformations;
            addressViewModel.Phone = billingAddress.Phone ?? billingAddress.CellPhone;
            addressViewModel.CellPhone = billingAddress.CellPhone;

            addressViewModel.GenderId = billingAddress.GenderId;
            addressViewModel.CountryId = billingAddress.CountryId ?? -1;

            addressViewModel.Email = billingAddress.Email;
            addressViewModel.ConfirmEmail = billingAddress.ConfirmEmail;

            addressViewModel.DocumentCountryId = billingAddress.DocumentCountryId;

            addressViewModel.TaxNumberType = string.IsNullOrEmpty(billingAddress.TaxIdNumberType) ? UNKNOW_TAXE_ID_NUMBER_TYPE : billingAddress.TaxIdNumberType;
            addressViewModel.TaxIdNumber = billingAddress.TaxIdNumber;
            addressViewModel.LegalStatus = billingAddress.LegalStatus;

            return addressViewModel;
        }
    }
}
