﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FnacDirect.OrderPipe.FDV.Model
{
    public class OrderTransactionIngenico
    {
        public int OrderTransactionIngenicoId { get; set; }
        public Guid Reference { get; set; }
        public int? OrderId { get; set; }

        public string Amount { get; set; }
        public string CurrencyCode { get; set; }
        public string Amount2 { get; set; }
        public string CurrencyCode2 { get; set; }
        public string ResponseCode { get; set; }
        public string C3Error { get; set; }
        public string TicketAvailable { get; set; }
        public string Pan { get; set; }
        public string NumAuto { get; set; }
        public string SignatureDemande { get; set; }
        public string TermNum { get; set; }
        public string TypeMedia { get; set; }
        public string Iso2 { get; set; }
        public string Cmc7 { get; set; }
        public string CardType { get; set; }
        public string CharityAmount { get; set; }
        public string SSCarte { get; set; }
        public string TimeLoc { get; set; }
        public string DateFinValidite { get; set; }
        public string CodeService { get; set; }
        public string TypeForcage { get; set; }
        public string TenderIdent { get; set; }
        public string CertificatVA { get; set; }
        public string DateTrns { get; set; }
        public string HeureTrns { get; set; }
        public string PisteK { get; set; }
        public string DepassPuce { get; set; }
        public string IncidentCam { get; set; }
        public string CondSaisie { get; set; }
        public string OptionChoisie { get; set; }
        public string OptionLibelle { get; set; }
        public string NumContexte { get; set; }
        public string CodeRejet { get; set; }
        public string CAI_Emetteur { get; set; }
        public string CAI_Auto { get; set; }
        public string TrnsNum { get; set; }
        public string NumFile { get; set; }
        public string UserData1 { get; set; }
        public string UserData2 { get; set; }
        public string NumDossier { get; set; }
        public string TypeFacture { get; set; }
        public string Axis { get; set; }
        public string NumFileV5 { get; set; }
        public string FFU { get; set; }
        public string PaymentId { get; set; }
    }
}
