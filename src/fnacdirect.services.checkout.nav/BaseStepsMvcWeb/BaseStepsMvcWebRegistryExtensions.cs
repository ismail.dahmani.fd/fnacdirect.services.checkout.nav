using FnacDarty.FnacCom.DataLayer;
using FnacDirect;
using FnacDirect.Front.WebBusiness.NewCustomer;
using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.OrderPipe.Base.Model.BillingMethods;
using FnacDirect.OrderPipe.BaseMvc.Web.CrossSellBasket;
using FnacDirect.OrderPipe.BaseMvc.Web.LayerBasket;
using FnacDirect.OrderPipe.BaseMvc.Web.Models;
using FnacDirect.OrderPipe.BaseMvc.Web.Models.Common;
using FnacDirect.OrderPipe.BaseMvc.Web.Models.Common.Basket.PipeArticle;
using FnacDirect.OrderPipe.BaseMvc.Web.Models.Orderpipe.Common.Shipping.Network.Postal.Appointment.Fly;
using FnacDirect.OrderPipe.BaseMvc.Web.Models.OrderPipe.Basket;
using FnacDirect.OrderPipe.BaseMvc.Web.Models.OrderPipe.Common.Scoring;
using FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop;
using FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Basket;
using FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Basket.Children;
using FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Common;
using FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Common.Basket;
using FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Common.Payment;
using FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Common.Payment.BillingMethodFactories;
using FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Common.Shipping;
using FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Common.Shipping.Postal;
using FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Common.Shipping.Relay;
using FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Common.Shipping.Shop;
using FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.DataLayer;
using FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.InvoicerEbook;
using FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Kobo;
using FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Membership;
using FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Omniture;
using FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.OnePage.Address;
using FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.OrderThankYou;
using FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Solex.Appointment;
using FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Sorry;
using FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.TagCommander;
using FnacDirect.OrderPipe.BaseMvc.Web.Pop.Models.Common;
using FnacDirect.OrderPipe.BaseMvc.Web.Pop.Models.OnePage;
using FnacDirect.OrderPipe.BaseMvc.Web.Pop.Models.Payment;
using FnacDirect.OrderPipe.BaseMvc.Web.Pop.Models.SelfCheckout;
using FnacDirect.OrderPipe.BaseMvc.Web.Pop.Models.Shipping;
using FnacDirect.OrderPipe.BaseMvc.Web.Providers.ShipToStore;
using FnacDirect.OrderPipe.BaseMvc.Web.Services.Article.Availability;
using FnacDirect.OrderPipe.BaseMvc.Web.Services.Article.Pipe;
using FnacDirect.OrderPipe.BaseMvc.Web.Services.Pop;
using FnacDirect.OrderPipe.BaseMvc.Web.Services.Pop.DataLayer;
using FnacDirect.OrderPipe.BaseMvc.Web.Services.Pop.Payment;
using FnacDirect.OrderPipe.BaseMvc.Web.Services.Pop.Shipping;
using FnacDirect.OrderPipe.BaseMvc.Web.Services.ShipToStore;
using FnacDirect.OrderPipe.BaseMvc.Web.Services.Tracking;
using FnacDirect.Technical.Framework.Configuration;
using FnacDirect.Technical.Framework.Web.Mvc;
using FnacDirect.Technical.Framework.Web.Mvc.Implementation;

namespace StructureMap.Configuration.DSL
{
    public static class BaseStepsMvcWebRegistryExtensions
    {
        public static void AddOrderPipe(this IRegistry registry, ISwitchProvider switchProvider, IConfigurationProvider configurationProvider)
        {
            registry.AddOrderPipeCoreMvc();

            registry.For<IStoreSearchService>().Use<StoreSearchService>();

            registry.For<ICustomerFavoriteStoreService>().Use<CustomerFavoriteStoreService>();

            registry.For<IStoreViewModelProvider>().Use<StoreViewModelProvider>();

            registry.For<ICrossSellBasketViewModelBuilder>().Use<CrossSellBasketViewModelBuilder>();
            registry.For<ILayerBasketViewModelBuilder>().Use<LayerBasketViewModelBuilder>();

            registry.For<IAddressSelectorService>().Use<AddressSelectorService>();

            registry.For<IUserExtendedContext>().Use<UserExtendedContext>();

            registry.For<INewCustomerBusiness>().Use<NewCustomerBusiness>();
            registry.For<IFnacPlusViewModelBuilder>().Use<FnacPlusViewModelBuilder>();
            registry.For<IStoreEligibilityLineGroupsService>().Use<StoreEligibilityLineGroupsService>();

            registry.RegisterPop(switchProvider);
        }

        private static void RegisterPop(this IRegistry registry, ISwitchProvider switchProvider)
        {
            registry.For<IBreadCrumbViewModelBuilder>().Use<BreadCrumbViewModelBuilder>();

            registry.For<IPaymentViewModelFactory<AvoirOccazBillingMethod>>().Use<AvoirOccazViewModelFactory>();
            registry.For<IPaymentViewModelFactory<BankTransferBillingMethod>>().Use<BankTransferViewModelFactory>();
            registry.For<IPaymentViewModelFactory<CheckBillingMethod>>().Use<CheckViewModelFactory>();
            registry.For<IPaymentViewModelFactory<CreditCardBillingMethod>>().Use<SipsViewModelFactory>();
            registry.For<IPaymentViewModelFactory<DefferedPaymentBillingMethod>>().Use<DeferredViewModelFactory>();
            registry.For<IPaymentViewModelFactory<FibBillingMethod>>().Use<FibViewModelFactory>();
            registry.For<IPaymentViewModelFactory<FidelityEurosBillingMethod>>().Use<FidelityEurosViewModelFactory>();
            registry.For<IPaymentViewModelFactory<FidelityPointsOccazBillingMethod>>().Use<FidelityPointsOccazViewModelFactory>();
            registry.For<IPaymentViewModelFactory<FnacCardBillingMethod>>().Use<FnacCardMethodsViewModelFactory>();
            registry.For<IPaymentViewModelFactory<FnacCardCetelemBillingMethod>>().Use<CreditCardCetelemViewModelFactory>();
            registry.For<IPaymentViewModelFactory<FreeBillingMethod>>().Use<FreeViewModelFactory>();
            registry.For<IPaymentViewModelFactory<GiftCardBillingMethod>>().Use<GiftCardViewModelFactory>();
            registry.For<IPaymentViewModelFactory<LimonetikBillingMethod>>().Use<LimonetikViewModelFactory>();
            registry.For<IPaymentViewModelFactory<OgoneCreditCardBillingMethod>>().Use<OgoneViewModelFactory>();
            registry.For<IPaymentViewModelFactory<PayByRefBillingMethod>>().Use<PayByRefViewModelFactory>();
            registry.For<IPaymentViewModelFactory<PayOnDeliveryBillingMethod>>().Use<PayOnDeliveryViewModelFactory>();
            registry.For<IPaymentViewModelFactory<UnicreCreditCardBillingMethod>>().Use<UnicreViewModelFactory>();
            registry.For<IPaymentViewModelFactory<VirtualGiftCheckBillingMethod>>().Use<VirtualGiftCheckViewModelFactory>();
            registry.For<IPaymentViewModelFactory<OrangeBillingMethod>>().Use<OrangeMethodViewModelFactory>();
            registry.For<IPaymentViewModelFactory<MBWayBillingMethod>>().Use<MBWayViewModelFactory>();

            registry.For<ITagCommanderViewModelBuilder>().Use<TagCommanderViewModelBuilder>();
            registry.For<IOmnitureViewModelBuilder>().Use<OmnitureViewModelBuilder>();

            registry.For<IFooterViewModelBuilder>().Use<FooterViewModelBuilder>();

            registry.For<IArticleImagePathService>().Use<ArticleImagePathService>();
            registry.For<ITermsAndConditionsBuilder>().Use<TermsAndConditionsBuilder>();

            registry.For<IPriceViewModelBuilder>().Use<PriceViewModelBuilder>();
            registry.For<IBasketItemViewModelBuilder>().Use<BasketItemViewModelBuilder>();
            registry.For<ISellerInformationsViewModelBuilder>().Use<SellerInformationsViewModelBuilder>();
            registry.For<IRichBasketItemViewModelBuilderProvider>().Use<RichBasketItemViewModelBuilderProvider>();
            registry.For<IBasketViewModelBuilder>().Use<BasketViewModelBuilder>();

            registry.For<FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Payment.IBasketViewModelBuilder>().Use<FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Payment.BasketViewModelBuilder>();
            registry.For<FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Shipping.IBasketViewModelBuilder>().Use<FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Shipping.BasketViewModelBuilder>();
            registry.For<FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.OnePage.Basket.IBasketViewModelBuilder>().Use<FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.OnePage.Basket.BasketViewModelBuilder>();

            registry.For<IShippingPageRootViewModelBuilder>().Use<ShippingPageRootViewModelBuilder>();
            registry.For<IOnePageRootViewModelBuilder>().Use<OnePageRootViewModelBuilder>();
            registry.For<IPaymentPageRootViewModelBuilder>().Use<PaymentPageRootViewModelBuilder>();

            registry.For<IPopBaseRootViewModelBuilder<FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Payment.RootViewModel>>().Use<PopBaseRootViewModelBuilder<FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Payment.RootViewModel>>();
            registry.For<IPopBaseRootViewModelBuilder<FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.OnePage.RootViewModel>>().Use<PopBaseRootViewModelBuilder<FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.OnePage.RootViewModel>>();
            registry.For<IPopBaseRootViewModelBuilder<FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Basket.RootViewModel>>().Use<PopBaseRootViewModelBuilder<FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Basket.RootViewModel>>();
            registry.For<IPopBaseRootViewModelBuilder<FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Shipping.RootViewModel>>().Use<PopBaseRootViewModelBuilder<FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Shipping.RootViewModel>>();

            registry.For<IBillingOnlyViewModelBuilder>().Use<BillingOnlyViewModelBuilder>();
            registry.For<IShopShippingNetworkViewModelBuilder>().Use<ShopShippingNetworkViewModelBuilder>();
            registry.For<IRelayShippingNetworkViewModelBuilder>().Use<RelayShippingNetworkViewModelBuilder>();
            registry.For<IPostalShippingNetworkViewModelBuilder>().Use<PostalShippingNetworkViewModelBuilder>();
            registry.For<IPostalShippingChoiceViewModelBuilder>().Use<PostalShippingChoiceViewModelBuilder>();

            registry.For<IAddressViewModelBuilder>().Use<AddressViewModelBuilder>();
            registry.For<IShippingViewModelBuilder>().Use<ShippingViewModelBuilder>();

            registry.For<IPaymentViewModelBuilder>().Use<PaymentViewModelBuilder>();
            registry.For<FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Payment.IPaymentHtmlTemplateViewModelBuilder>().Use<FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Payment.PaymentHtmlTemplateViewModelBuilder>();
            registry.For<IExpressPlusViewModelBuilder>().Use<ExpressPlusViewModelBuilder>();

            registry.For<ITvRoyaltyViewModelBuilder>().Use<TvRoyaltyViewModelBuilder>();

            registry.For<IOrderThankYouViewModelBuilder>().Use<OrderThankYouViewModelBuilder>();

            registry.For<IKoboMergeAccountBuilder>().Use<KoboMergeAccountBuilder>();

            registry.For<IInvoicerEbookViewModelBuilder>().Use<InvoicerEbookViewModelBuilder>();

            registry.For<IMembershipViewModelBuilder>().Use<MembershipViewModelBuilder>();

            registry.For<IPaymentMethodService>().Use<PaymentMethodService>();

            registry.For<IPaymentMethodViewModelFactory>().Use<StructureMapPaymentMethodViewModelFactory>();

            registry.For<IOptionsViewModelBuilder>().Use<OptionsViewModelBuilder>();

            registry.For<ISplitViewModelBuilder>().Use<SplitViewModelBuilder>();

            registry.For<IWrapViewModelBuilder>().Singleton().Use<WrapViewModelBuilder>();

            registry.For<IConfigurationViewModelBuilder>().Use<ConfigurationViewModelBuilder>();

            registry.For<IHideBillingInfoViewModelBuilder>().Use<HideBillingInfoViewModelBuilder>();
            registry.For<IOrderExternalReferenceViewModelBuilder>().Use<OrderExternalReferenceViewModelBuilder>(); 


            registry.For<IRichBasketItemChildrenViewModelBuilder>().Use<DigiCopyRichBasketItemChildrenViewModelBuilder>();
            registry.For<IRichBasketItemChildrenViewModelBuilder>().Use<ServicesRichBasketItemChildrenViewModelBuilder>();
            registry.For<IRichBasketItemChildrenViewModelBuilder>().Use<ClickAndCollectBasketItemChildrenViewModelBuilder>();
            registry.For<IRichBasketItemChildrenViewModelBuilder>().Use<CrossSellRichBasketItemChildrenViewModelBuilder>();
            registry.For<ICompositeRichBasketItemChildrenViewModelBuilder>().Use<DefaultCompositeRichBasketItemChildrenViewModelBuilder>();

            registry.For<ICrossSellRichBasketItemChildrenViewModelBuilder>().Use<ClickAndCollectBasketItemChildrenViewModelBuilder>();
            registry.For<ICrossSellRichBasketItemChildrenViewModelBuilder>().Use<CrossSellRichBasketItemChildrenViewModelBuilder>();
            registry.For<ICompositeCrossSellRichBasketItemChildrenViewModelBuilder>().Use<DefaultCrossSellRichBasketItemChildrenViewModelBuilder>();

            registry.For<IDonationViewModelBuilder>().Use<DonationViewModelBuilder>();

            registry.For<IFnacPlusPushBuilder>().Use<FnacPlusPushBuilder>();

            registry.For<IPromotionTemplateViewModelBuilder>().Use<PromotionTemplateViewModelBuilder>();

            registry.For<ISummaryViewModelBuilder>().Use<SummaryViewModelBuilder>();
            registry.For<ISummaryPricesViewModelBuilder>().Use<SummaryPricesViewModelBuilder>();

            registry.For<IAppointmentDeliveryCalendarViewModelBuilder>().Use<AppointmentDeliveryCalendarViewModelBuilder>();

            registry.For<IPipeMessageViewModelBuilder>().Use<PipeMessageViewModelBuilder>();

            registry.For<INavigationViewModelBuilder>().Use<NavigationViewModelBuilder>();

            registry.For<IRemindersViewModelBuilder>().Use<RemindersViewModelBuilder>();

            registry.For<ITermsAndConditionsViewModelBuilder>().Use<TermsAndConditionsViewModelBuilder>();

            registry.For<ICustomerCreditCardBaseViewModelBuilder>().Use<CustomerCreditCardBaseViewModelBuilder>();

            registry.For<ISelfCheckoutSummaryViewModelBuilder>().Use<SelfCheckoutSummaryViewModelBuilder>();
            registry.For<ISelfCheckoutCryptoViewModelBuilder>().Use<SelfCheckoutCryptoViewModelBuilder>();
            registry.For<ISelfCheckoutOrderThankYouModelBuilder>().Use<SelfCheckoutOrderThankYouModelBuilder>();

            registry.For<IOptinWarrantyViewModelBuilder>().Use<OptinWarrantyViewModelBuilder>();
            
            registry.For<IPipeArticleViewModelBuilder>().ClearAll().Use<PipeArticleViewModelBuilder>();
            registry.For<IFlyViewModelBuilder>().Singleton().Use<FlyViewModelBuilder>();
            registry.For<IPipeArticleService>().Use<PipeArticleService>();

            registry.For<IArticleAvailabilityService>().Use<ArticleAvailabilityService>();

            registry.For<IScoringViewModelBuilder>().Use<ScoringViewModelBuilder>();

            registry.For<IOrderSorryViewModelBuilder>().Use<OrderSorryViewModelBuilder>();

            registry.For<IBasketSellerViewModelBuilder>().Use<BasketSellerViewModelBuilder>();
            
            registry.For<IShopAvailabilityViewModelBuilder>().Use<ShopAvailabilityViewModelBuilder>();

            #region Datalayer
            DataLayerRegistry.AddTo(registry);
            registry.For<IDataLayerService>().Use<DataLayerService>();
            registry.For<IDataLayerBuilder>().Use<DataLayerBuilder>();
            #endregion

            if (switchProvider.IsEnabled("Orderpipe.Tracking.Producer.Enabled", false))
                registry.For<ITrackingService>().Singleton().Use<TrackingService>();
            else
                registry.For<ITrackingService>().Singleton().Use<DummyTrackingService>();
        }
    }
}
