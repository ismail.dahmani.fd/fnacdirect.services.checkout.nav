﻿using FnacDirect.OrderPipe.Base.Model;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Common.Shipping
{
    public interface IBillingOnlyViewModelBuilder
    {
        BillingOnlyViewModel Build(PopOrderForm popOrderForm, int sellerId);
    }
}
