using FnacDirect.OrderPipe.Base.BaseModel.Model;
using FnacDirect.OrderPipe.Base.DAL;
using FnacDirect.OrderPipe.Base.Model.Facets;
using FnacDirect.OrderPipe.Base.Proxy;
using FnacDirect.Technical.Framework.Caching2;
using FnacDirect.Technical.Framework.Web.Mvc;
using System;
using System.Collections.Generic;

namespace FnacDirect.OrderPipe.Base.Business
{
    public class CustomerContext : ICustomerContext
    {
        private readonly OPContext _opContext;
        private readonly ICustomerDAL _customerDal;
        private readonly IUserExtendedContext _userExtendedContext;

        public CustomerContext(ICustomerDAL customerDal,
                               IUserExtendedContextProvider userExtendedContextProvider,
                               OPContext opContext)
        {
            _customerDal = customerDal;
            _userExtendedContext = userExtendedContextProvider.GetCurrentUserExtendedContext();
            _opContext = opContext;
        }

        public string GetUserReference()
        {
            return _opContext.UID;
        }

        public int GetUserId()
        {
            if (!(_opContext.OF is IGotUserContextInformations iContainsUserContextInformations))
            {
                throw new InvalidOperationException();
            }

            var userContextInformations = iContainsUserContextInformations.UserContextInformations;

            if (userContextInformations.CustomerEntity != null &&
                userContextInformations.CustomerEntity.Reference == userContextInformations.UID)
            {
                return userContextInformations.AccountId;
            }

            var accountId = _customerDal.GetAccountId(GetUserReference());

            if (accountId != null && accountId.AccountPk.HasValue)
            {
                var currentAccountId = accountId.AccountPk.Value;
                if (currentAccountId != -1)
                {
                    userContextInformations.AccountId = currentAccountId;
                }
            }

            return userContextInformations.AccountId;
        }

        public FnacDirect.Contracts.Online.Model.Customer GetContractDTO()
        {
            //Info obligatoire pour activer la promo remiseperso (correspond au montant max alloué par personne)
            if ((_userExtendedContext?.ContractDto?.Info?.BalanceAmount).HasValue)
            {
                _userExtendedContext.ContractDto.Info.BalanceAmount = 100000;
            }

            return _userExtendedContext.ContractDto;
        }

        public List<State> GetState()
        {
            return CacheManager.Get(
                new CacheKey("RefOrderPipe", "State"),
                delegate ()
                {
                    return _customerDal.GetState();
                });
        }
    }
}
