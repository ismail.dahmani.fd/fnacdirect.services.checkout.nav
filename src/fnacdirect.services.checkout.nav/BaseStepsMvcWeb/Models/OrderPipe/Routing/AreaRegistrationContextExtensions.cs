﻿using System.Linq;
using System.Web.Mvc;
using System.Web.Routing;

namespace FnacDirect.OrderPipe.CoreMvcWeb.Routing
{
    public static class AreaRegistrationContextExtensions
    {
        public static Route MapOrderPipeRoute(this AreaRegistrationContext areaRegistrationContext, string name, string url)
        {
            return MapOrderPipeRoute(areaRegistrationContext, name, url, null);
        }

        public static Route MapOrderPipeRoute(this AreaRegistrationContext areaRegistrationContext, string name, string url, object defaults)
        {
            return MapOrderPipeRoute(areaRegistrationContext, name, url, defaults, null);
        }

        public static Route MapOrderPipeRoute(this AreaRegistrationContext areaRegistrationContext, string name, string url, string[] namespaces)
        {
            return MapOrderPipeRoute(areaRegistrationContext, name, url, null, namespaces);
        }

        public static Route MapOrderPipeRoute(this AreaRegistrationContext areaRegistrationContext, string name, string url, object defaults, object constraints)
        {
            return MapOrderPipeRoute(areaRegistrationContext, name, url, defaults, constraints, null);
        }

        public static Route MapOrderPipeRoute(this AreaRegistrationContext areaRegistrationContext, string name, string url, object defaults, string[] namespaces)
        {
            return MapOrderPipeRoute(areaRegistrationContext, name, url, defaults, null, namespaces);
        }

        public static Route MapOrderPipeRoute(this AreaRegistrationContext areaRegistrationContext, string name, string url, object defaults, object constraints, string[] namespaces)
        {
            if ((namespaces == null) && (areaRegistrationContext.Namespaces != null))
            {
                namespaces = areaRegistrationContext.Namespaces.ToArray();
            }

            var route = areaRegistrationContext.Routes.MapOrderPipeRoute(name, url, defaults, constraints, namespaces);

            route.DataTokens["area"] = areaRegistrationContext.AreaName;

            route.DataTokens["UseNamespaceFallback"] = (namespaces == null) || (namespaces.Length == 0);
            return route;
        }


        public static Route MapMvcOrderPipeRoute(this AreaRegistrationContext areaRegistrationContext, string name, string url)
        {
            return MapMvcOrderPipeRoute(areaRegistrationContext, name, url, null);
        }

        public static Route MapMvcOrderPipeRoute(this AreaRegistrationContext areaRegistrationContext, string name, string url, object defaults)
        {
            return MapMvcOrderPipeRoute(areaRegistrationContext, name, url, defaults, null);
        }

        public static Route MapMvcOrderPipeRoute(this AreaRegistrationContext areaRegistrationContext, string name, string url, string[] namespaces)
        {
            return MapMvcOrderPipeRoute(areaRegistrationContext, name, url, null, namespaces);
        }

        public static Route MapMvcOrderPipeRoute(this AreaRegistrationContext areaRegistrationContext, string name, string url, object defaults, object constraints)
        {
            return MapMvcOrderPipeRoute(areaRegistrationContext, name, url, defaults, constraints, null);
        }

        public static Route MapMvcOrderPipeRoute(this AreaRegistrationContext areaRegistrationContext, string name, string url, object defaults, string[] namespaces)
        {
            return MapMvcOrderPipeRoute(areaRegistrationContext, name, url, defaults, null, namespaces);
        }

        public static Route MapMvcOrderPipeRoute(this AreaRegistrationContext areaRegistrationContext, string name, string url, object defaults, object constraints, string[] namespaces)
        {
            if ((namespaces == null) && (areaRegistrationContext.Namespaces != null))
            {
                namespaces = areaRegistrationContext.Namespaces.ToArray();
            }

            var route = areaRegistrationContext.Routes.MapMvcOrderPipeRoute(name, url, defaults, constraints, namespaces);

            route.DataTokens["area"] = areaRegistrationContext.AreaName;

            route.DataTokens["UseNamespaceFallback"] = (namespaces == null) || (namespaces.Length == 0);
            return route;
        }
    }
}