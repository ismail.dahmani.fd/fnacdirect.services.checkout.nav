using System.Collections.Generic;

namespace FnacDirect.OrderPipe
{
    public static class Constants
    {
        public static class ShippingMethods
        {
            public const int VirtualExpressFnacPlus = -3;
            public const int VirtualFly = -2;
            public const int VirtualAppointment = -1;
            public const int Standard = 1;
            public const int ServiceEnLigneNonExpedie = 5;
            public const int RetraitMagasin = 9;
            public const int ExpressOneDay = 11;
            // Il s'agit d'une méthode de livraison spécifique ES qui réutilise la même id que le ExpressOneDay
            public const int PayOnDelivery = 11;
            public const int Download = 23;
            public const int HandDelivery = 28;
            public const int LivraisonMarketPlaceGenerique1 = 51;
            public const int LivraisonMarketPlaceGenerique2 = 52;
            public const int LivraisonMarketPlaceGenerique3 = 53;
            public const int LivraisonMarketPlaceGenerique4 = 54;
            public const int SteedFromStore = 57;
            public const int ExpressIDF = 71;
            public const int Fly5Hours = 72;
            public const int Fly2Hours = 83;
            public const int ChronopostDelivery = 75;
            public const int ClickAndCollect = 76;
            public const int LivraisonDeuxHeuresChrono = 57;
        }

        public static class ShippingNetworks
        {
            public const int Postal = 1;
            public const int Relay = 2;
            public const int Store = 3;
        }

        public static class ContextKeys
        {
            public const string OneClick = "oneclick";
            public const string Operation = "operation";
            public const string SpreadRegistration = "spreadregistration";
            public const string ClickAndCollectExpress = "clickandcollectexpress";
            public const string Guest = "guest";
        }

        public static class Promotions
        {
            public static class Triggers
            {
                public const string MpFreeShippingGlobal = "MP_FREE_SHIPPING_GLOBAL";
            }
        }

        public static readonly string ShoppingCardDeezer = "Deezer";

        public static readonly List<int> WarrantyTypes = new List<int>
            {
                19001, 19002, 19005, 19006, 19007, 19008, 19009, 19010, 19011
            };
        public static readonly int InsuranceType = 19008;

        public static class PipeName
        {
            public const string Pop = "pop";
            public const string Quote = "quote";
            public const string Payment = "payment";
            public const string SelfCheckout = "selfcheckout";
        }

        public static class Error
        {
            public enum ErrorType : int
            {
                Undefined = 0,
                Authentication = 1,
                SelfCheckout = 2,
                Customer = 3,
            }
        }

        public static class Countries
        {
            public const int Belgium = 21;
            public const int France = 74;
            public const int Luxembourg = 126;
            public const int Portugal = 173;
            public const int Spain = 197;
            public const int Swiss = 206;

            public const int EspañaIslasBaleares = 246;
            public const int EspañaIslasCanarias = 247;
            public const int EspañaCeutaYMelilla = 248;
        }

        public static readonly List<int> SpainCountries = new List<int>
        {
            Countries.Spain,
            Countries.EspañaCeutaYMelilla,
            Countries.EspañaIslasBaleares,
            Countries.EspañaIslasCanarias
        };

        public static class LocalCountry
        {
            public const string FRANCE = "FR";
            public const string ESPAGNE = "ES";
            public const string PORTUGAL = "PT";
            public const string SUISSE = "CH";
            public const string BELGIQUE = "BE";
        }

        public static class OrderOrigin
        {
            public const int ClickInStore = 1001;
        }

        public static class OrderInfoTypes
        {
            public const int UrlOrderGuest = 8;
            public const int InternalCodeSelfCheckout = 10;
            public const int ShouldCheckCreditCard = 11;
            public const int PersonnelNumber = 12;
            public const int FlyQualifyingSurvey = 13;
            public const int FlyVersion = 14;
        }

        public static class CreditCardIds
        {
            public const int Paypal = 30;
        }
        public static class Environement
        {
            public const string Recette = "REC";
            public const string Production = "PRD";
        }
        public static class DevicesUIRessource
        {
            public const string Desktop = "desktop";
            public const string Webmobile = "webmobile";
            public const string AndroidTablet = "android.tablet";
            public const string Android = "android";
            public const string Ipad = "ipad";
            public const string Iphone = "iphone";
        }
        public static class PageType
        {
            public const string Cart = "Cart";
            public const string Order = "Order";
        }

        public static class ChannelType
        {
            public const string MOBILE = "mobile";
            public const string MOBILEAPP = "mobile application";
            public const string DESKTOP = "desktop";

        }
        public static class PipeRoute
        {
            public const string ONECLICK = "oneclick";
            public const string ONEPAGE = "onepage";
            public const string FULL = "full";
            public const string UNKNOWN = "unknown";

        }
        public static class AggregatedShippingMethods
        {
            public const int FLY = -2;
        }
        public static class VirtualBillingMethod
        {
            public const int Caixa = -1;
        }
    }
}
