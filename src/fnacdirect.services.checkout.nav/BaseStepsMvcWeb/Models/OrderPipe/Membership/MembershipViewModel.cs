﻿namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Membership
{

    public class MembershipViewModel
    {
        public MembershipViewModel()
        {
            Profil = new MembershipProfilViewModel();
            Push = new MembershipPushViewModel();           
        }
        public MembershipProfilViewModel Profil { get; set; }

        public MembershipPushViewModel Push { get; set; }

        public MembershipFidelityAccountViewModel Fnac { get; set; }

        public MembershipFidelityAccountViewModel Gaming { get; set; }

        public MembershipFidelityAccountViewModel AvoirGaming { get; set; }
    }
}
