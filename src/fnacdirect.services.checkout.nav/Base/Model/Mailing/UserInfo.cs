namespace FnacDirect.OrderPipe.Base.Business.Mailing
{
    public class UserInfo
    {
        public string UserName { get; set; }

        public string EmailMD5 { get; set; }

        public int ScoringLevel { get; set; }
        public bool IsDeezerAccountSynchronized { get;set; }
        public bool IsOptIn  { get;set; }
    }
}
