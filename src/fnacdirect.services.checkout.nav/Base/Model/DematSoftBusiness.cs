﻿using System;
using System.Collections.Generic;
using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.Technical.Framework.CoreServices;
using Fnac.DEMAT.Delivery.Contracts;

namespace FnacDirect.OrderPipe.Base.Business
{
    public class DematSoftBusiness : IDematSoftBusiness
    {
        private static IDeliveryService _deliveryService = Service<IDeliveryService>.Instance;

        public DematSoftBusiness()
        {
            _deliveryService = Service<IDeliveryService>.Instance;
        }

        public IDictionary<long, bool> GetDispoArticles(ArticleList standardArticles)
        {
            var dispoReturn = new Dictionary<long, bool>();  
                      
            foreach (StandardArticle article in standardArticles)
            {
                var lstPrdct = new List<GetCrossUpSellProduct>();

                if ((long.TryParse(article.NumericalAddOn.ExternalRef, out long artExtRef)) && article.Quantity.HasValue)
                    lstPrdct.Add(new GetCrossUpSellProduct() { productRef = artExtRef, quantity = article.Quantity.Value });

                var request = new GetCrossUpSellRequest()
                {
                    products = lstPrdct,
                    language = "FR"
                };

                try
                {
                    var articleDispo = _deliveryService.GetCrossUpSell(request).responseCode;
                    dispoReturn.Add(artExtRef, articleDispo == 0);
                }
                catch (TimeoutException e)
                {
                    Logging.Current.WriteError("Timeout dans l'appel au service Delivery", e);
                }
                catch (Exception e)
                {
                    Logging.Current.WriteError("Erreur dans l'appel au service Delivery", e);
                }
            }               

            return dispoReturn;
        }

        public IDictionary<long, bool> IsAvailable(ArticleList standardArticles)
        {
            var articlesAvailability = new Dictionary<long, bool>();
            try
            {
                var artExtRefList = new List<long>();

                foreach (StandardArticle article in standardArticles)
                {
                    if (long.TryParse(article.NumericalAddOn.ExternalRef, out long artExtRef))
                        artExtRefList.Add(artExtRef);
                }
                
                var stockStatusList = _deliveryService.GetStockStatus(artExtRefList);

                foreach (var stockStatus in stockStatusList)
                {
                    switch (stockStatus.Status)
                    {
                        case StockStatus.Almost_No:
                        case StockStatus.Enough_Serial:
                            articlesAvailability.Add(stockStatus.ProductRef, true);
                            break;
                        case StockStatus.Out_Of_Stock:
                        case StockStatus.Undefined:
                        default:
                            articlesAvailability.Add(stockStatus.ProductRef, false);
                            break;
                    }
                }

                return articlesAvailability;
            }
            catch (TimeoutException e)
            {
                Logging.Current.WriteError("Timeout dans l'appel au service Delivery", e);
                return articlesAvailability;
            }
            catch (Exception e)
            {
                Logging.Current.WriteError("Erreur dans l'appel au service Delivery", e);
                return articlesAvailability;
            }
        }
    }
}
