using FnacDirect.Contracts.Online.Model;
using FnacDirect.Customer.Model;
using FnacDirect.OrderPipe.Base.Business.Billing;
using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.OrderPipe.Base.Model.Facets;
using FnacDirect.OrderPipe.Base.Model.LineGroups;
using FnacDirect.OrderPipe.Base.Model.UserInformations;
using FnacDirect.OrderPipe.Base.Proxy.Customer;
using FnacDirect.OrderPipe.Base.Proxy.Ebook;
using FnacDirect.Technical.Framework.Web.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace FnacDirect.OrderPipe.Base.Business.Shipping.Postal
{
    public class AddressService : IAddressService
    {
        private readonly IAddressBookService _addressBookService;
        private readonly IShippingAddressElegibilityServiceProvider _shippingAddressElegibilityServiceProvider;
        private readonly IBillingAddressElegibilityServiceProvider _billingAddressElegibilityServiceProvider;
        private readonly ICapencyService _capencyService;
        private readonly IEbookService _ebookService;
        private readonly IApplicationContext _context;
        protected readonly IAddressValidationBusiness _addressValidationBusiness;

        public AddressService(IAddressBookService addressBookService,
            IShippingAddressElegibilityServiceProvider shippingAddressElegibilityServiceProvider,
            IBillingAddressElegibilityServiceProvider billingAddressElegibilityServiceProvider,
            ICapencyService capencyService,
            IApplicationContext context,
            IEbookService ebookService,
            IAddressValidationBusiness addressValidationBusiness)
        {
            _addressBookService = addressBookService;
            _shippingAddressElegibilityServiceProvider = shippingAddressElegibilityServiceProvider;
            _billingAddressElegibilityServiceProvider = billingAddressElegibilityServiceProvider;
            _capencyService = capencyService;
            _context = context;
            _ebookService = ebookService;
            _addressValidationBusiness = addressValidationBusiness;
        }

        public AddressEntity GetAddressByAddressBookReference(string addressReference)
        {
            return _addressBookService.GetAddressByAddressBookReference(addressReference);
        }

        public bool IsAddressEligible(OPContext opContext,
                                      IGotShippingMethodEvaluation iGotShippingMethodEvaluation,
                                      IEnumerable<ILineGroup> lineGroups,
                                      AddressEntity address)
        {
            var shippingAddressElegibilityService = _shippingAddressElegibilityServiceProvider.GetShippingAddressElegibilityServiceFor(opContext);

            return shippingAddressElegibilityService.GetEligibleAddresses(iGotShippingMethodEvaluation, lineGroups, address.Yield()).Any();
        }

        //Used in pop only
        public bool IsBillingAddressEligible(OPContext opContext, IUserContextInformations userContext, IEnumerable<ILineGroup> lineGroups, AddressEntity addressEntity)
        {
            var billingAddressElegibilityServiceProvider = _billingAddressElegibilityServiceProvider.GetBillingAddressElegibilityServiceFor(opContext);

            return billingAddressElegibilityServiceProvider.GetElegibleAddress(lineGroups, addressEntity.Yield().ToList()).Any() && _ebookService.IsAllowedToBeSoldInGeo(new BillingAddress(addressEntity), userContext, lineGroups, opContext.SiteContext.MarketId);
        }

        //Used in getBillingAddressV2 (pop) + method in this class
        public virtual AddressElegibilityList GetBillingAddresses(OPContext opContext, IUserContextInformations userContext, IEnumerable<ILineGroup> lineGroups, bool withKoboEligibility)
        {
            var addresses = _addressBookService.GetBillingAddresses().ToList();

            var billingAddressElegibilityService = _billingAddressElegibilityServiceProvider.GetBillingAddressElegibilityServiceFor(opContext);

            var elegibleAddresses = billingAddressElegibilityService.GetElegibleAddress(lineGroups, addresses);

            var addressList = (from address in addresses
                    select new AddressElegibility()
                    {
                        AddressEntity = address,
                        IsElegible = elegibleAddresses.Contains(address) && (!withKoboEligibility || _ebookService.IsAllowedToBeSoldInGeo(new BillingAddress(address), userContext, lineGroups, opContext.SiteContext.MarketId)),
                        HasMissingInformations = !_addressValidationBusiness.IsAddressValid(address)
                    }).ToList();
            return new AddressElegibilityList(addressList);
        }

        //Only in pop
        public AddressElegibilityList GetBillingAddresses(OPContext opContext, IUserContextInformations userContext,
            IEnumerable<ILineGroup> lineGroups,
            int? pageSize,
            int? pageIndex = null)
        {
            var addresses = GetAddressesEntity(opContext, userContext);

            if (pageSize.HasValue)
            {
                addresses = pageIndex.HasValue ? addresses.Skip(pageSize.Value * (pageIndex.Value - 1)).Take(pageSize.Value).ToList() : addresses.Take(pageSize.Value).ToList();
            }

            var billingAddressElegibilityService = _billingAddressElegibilityServiceProvider.GetBillingAddressElegibilityServiceFor(opContext);

            var elegibleAddresses = billingAddressElegibilityService.GetElegibleAddress(lineGroups, addresses);

            var addressList = (from address in addresses
                    select new AddressElegibility
                    {
                        AddressEntity = address,
                        IsElegible = elegibleAddresses.Contains(address) && _ebookService.IsAllowedToBeSoldInGeo(new BillingAddress(address), userContext, lineGroups, opContext.SiteContext.MarketId),
                        HasMissingInformations = !_addressValidationBusiness.IsAddressValid(address)
                    }).ToList();
            return new AddressElegibilityList(addressList);
        }

        private List<AddressEntity> GetAddressesEntity(OPContext opContext, IUserContextInformations userContext)
        {
            var addresses = new List<AddressEntity>();
            if (userContext.IsGuest)
            {
                var popOrderForm = opContext.OF as PopOrderForm;
                var guestData = popOrderForm.GetPrivateData<GuestData>(true);
                var addressEntity = new AddressEntity();

                if (guestData.BillingAddress != null)
                {
                    addressEntity = guestData.BillingAddress.AddressEntity;
                }

                addresses.Add(addressEntity);
            }
            else
            {
                addresses = _addressBookService.GetShippingAddresses().OrderByDescending(a => a.LastUpdateDate).ToList();
            }

            return addresses;
        }

        public AddressElegibilityList GetShippingAddresses(OPContext opContext, IGotShippingMethodEvaluation iContainsShippingMethodEvaluation, IEnumerable<ILineGroup> lineGroups)
        {
            var addresses = _addressBookService.GetShippingAddresses().ToList();

            var shippingAddressElegibilityService =
                _shippingAddressElegibilityServiceProvider.GetShippingAddressElegibilityServiceFor(opContext);

            var elegibleAddresses = shippingAddressElegibilityService.GetEligibleAddresses(iContainsShippingMethodEvaluation, lineGroups, addresses);

            var addressList = (from address in addresses
                    select new AddressElegibility()
                    {
                        AddressEntity = address,
                        IsElegible = elegibleAddresses.Contains(address),
                        HasMissingInformations = !_addressValidationBusiness.IsAddressValid(address)
                    }).ToList();
            return new AddressElegibilityList(addressList);
        }

        public AddressElegibilityList GetShippingAddresses(OPContext opContext,
            IGotShippingMethodEvaluation iContainsShippingMethodEvaluation,
            IEnumerable<ILineGroup> lineGroups,
            int? pageSize = null,
            int? lastId = null)
        {
            var addresses = _addressBookService.GetShippingAddresses().OrderByDescending(a => a.LastUpdateDate).ToList();

            if (pageSize.HasValue)
            {
                addresses = lastId.HasValue ? addresses.Skip(pageSize.Value * (lastId.Value - 1)).Take(pageSize.Value).ToList() : addresses.Take(pageSize.Value).ToList();
            }

            var shippingAddressElegibilityService =
                _shippingAddressElegibilityServiceProvider.GetShippingAddressElegibilityServiceFor(opContext);

            var elegibleAddresses = shippingAddressElegibilityService.GetEligibleAddresses(iContainsShippingMethodEvaluation, lineGroups, addresses);

            var addressList = (from address in addresses
                    select new AddressElegibility()
                    {
                        AddressEntity = address,
                        IsElegible = elegibleAddresses.Contains(address),
                        HasMissingInformations = !_addressValidationBusiness.IsAddressValid(address)
                    }).ToList();
            return new AddressElegibilityList(addressList);
        }

        public async Task<AddressValidityResult> GetAddressValidityResultAsync(IAddress address)
        {
            var isCapencyEnabled = _context.GetSwitch("orderpipe.pop.capency");
            if (isCapencyEnabled)
            {
                var capencyResult = await _capencyService.VerifyAsync(address);
                if (capencyResult != null)
                {
                    return capencyResult.GetAddressValidityResult();
                }
            }
            return new AddressValidityResult(AddressValidityResultType.Valid);
        }

        public IEnumerable<ValidationResult> Add(OPContext opContext, IGotShippingMethodEvaluation orderForm, AddressEntity addressEntity, IEnumerable<ILineGroup> lineGroups, bool checkElegibility)
        {
            var shippingAddressElegibilityService =
                _shippingAddressElegibilityServiceProvider.GetShippingAddressElegibilityServiceFor(opContext);

            var hasAnyShippingElegibleAddress = shippingAddressElegibilityService.GetEligibleAddresses(orderForm, lineGroups, addressEntity.Yield()).Any();

            if (checkElegibility && !hasAnyShippingElegibleAddress)
            {
                yield return new ValidationResult(_context.GetMessage("orderpipe.index.shiptoaddress.ineligibleaddress", null));
            }
            else
            {
                _addressBookService.AddAddress(addressEntity);
            }
        }

        public IEnumerable<ValidationResult> AddWithBillingElegibility(OPContext opContext, AddressEntity addressEntity, IEnumerable<ILineGroup> lineGroups)
        {
            var billingAddressElegibilityService =
                _billingAddressElegibilityServiceProvider.GetBillingAddressElegibilityServiceFor(opContext);

            if (!billingAddressElegibilityService.GetElegibleAddress(lineGroups, addressEntity.Yield().ToList()).Any())
            {

                yield return new ValidationResult(_context.GetMessage("orderpipe.index.billingaddress.ineligibleaddress", null));
            }
            else
            {
                _addressBookService.AddAddress(addressEntity);
            }
        }

        public IEnumerable<ValidationResult> Update(OPContext opContext, IGotShippingMethodEvaluation orderForm, AddressEntity addressEntity, IEnumerable<ILineGroup> lineGroups, bool checkElegibility)
        {
            var shippingAddressElegibilityService =
                _shippingAddressElegibilityServiceProvider.GetShippingAddressElegibilityServiceFor(opContext);

            if (checkElegibility && !shippingAddressElegibilityService.GetEligibleAddresses(orderForm, lineGroups, addressEntity.Yield()).Any())
            {
                yield return new ValidationResult(_context.GetMessage("orderpipe.index.shiptoaddress.ineligiblemodifiedaddress", null));
            }
            else
            {
                _addressBookService.UpdateAddress(addressEntity);
            }
        }

        public IEnumerable<ValidationResult> UpdateWithBillingElegibility(OPContext opContext, AddressEntity addressEntity, IEnumerable<ILineGroup> lineGroups)
        {
            var billingAddressElegibilityService =
                _billingAddressElegibilityServiceProvider.GetBillingAddressElegibilityServiceFor(opContext);

            if (!billingAddressElegibilityService.GetElegibleAddress(lineGroups, addressEntity.Yield().ToList()).Any())
            {
                yield return new ValidationResult(_context.GetMessage("orderpipe.index.billingaddress.ineligiblemodifiedaddress", null));
            }
            else
            {
                _addressBookService.UpdateAddress(addressEntity);
            }
        }

    }
}
