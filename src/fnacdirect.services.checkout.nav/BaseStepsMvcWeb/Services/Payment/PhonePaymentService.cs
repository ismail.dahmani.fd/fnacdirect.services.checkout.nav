using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.OrderPipe.Core.Services;
using System;
using System.Linq;

namespace FnacDirect.OrderPipe.Base.Business.Payment
{
    public class PhonePaymentService : IPhonePaymentService
    {
        private readonly ICheckArticleService _checkArticleService;
        private readonly IPaymentEligibilityService _paymentEligibilityService;

        private readonly decimal _creditCardByPhoneMinAmount;
        private readonly decimal _creditCardByPhoneMaxAmount;


        public PhonePaymentService(IPipeParametersProvider pipeParametersProvider,
            ICheckArticleService checkArticleService,
            IPaymentEligibilityService paymentEligibilityService)
        {
            _checkArticleService = checkArticleService;
            _paymentEligibilityService = paymentEligibilityService;

            _creditCardByPhoneMinAmount = pipeParametersProvider.Get("billing.creditcardbyphone.limit.low", 0m);
            _creditCardByPhoneMaxAmount = pipeParametersProvider.Get("billing.creditcardbyphone.limit.high", 1000.0m);
        }

        public bool IsAllowed(OPContext opContext, PopOrderForm popOrderForm, bool disableEccvCheck = false)
        {
            var rules = _paymentEligibilityService.GetRules("orderpipe.pop.payment.phone.eligibilityrules",
                disableEccvCheck: disableEccvCheck);

            if(!_paymentEligibilityService.IsAllowed(popOrderForm, rules))
            {
                return false;
            }

            var isGuest = popOrderForm.UserContextInformations.IsGuest;
            if (isGuest)
            {
                return false;
            }

            var hasSensibleProduct = _checkArticleService.HasSensibleSupportId(popOrderForm.LineGroups.GetArticles());
            if (hasSensibleProduct)
            {
                return false;
            }

            var tabsConfig = opContext.Pipe.GlobalParameters.GetAs<TabsGroupPaymentMethods>("tabs.group.payment.methods");
            var phonePaymentMethod = tabsConfig.TabGroup.FirstOrDefault(bm => bm.Name.Equals("Phone", StringComparison.CurrentCultureIgnoreCase))?.PaymentMethod.FirstOrDefault();
            if (phonePaymentMethod == null)
            {
                return false;
            }

            var paymentSelectionData = popOrderForm.GetPrivateData<PaymentSelectionData>("payment");

            // Test ECCV propre à ce moyen de paiement
            var hasEccvInBasket = _checkArticleService.HasECCV(popOrderForm.LineGroups.GetArticles());
            var hasPhoneSpecificRules = phonePaymentMethod.SpecificRules.Contains(SpecificRules.Phone);

            var isAllowed = !(hasPhoneSpecificRules && (paymentSelectionData.RemainingAmount < _creditCardByPhoneMinAmount
                                                       || paymentSelectionData.RemainingAmount > _creditCardByPhoneMaxAmount
                                                       || (popOrderForm.Constraints != null && popOrderForm.Constraints.Contains("billingmethod.telephone.hide")))
                            || (disableEccvCheck || hasEccvInBasket));

            return isAllowed;
        }
    }
}
