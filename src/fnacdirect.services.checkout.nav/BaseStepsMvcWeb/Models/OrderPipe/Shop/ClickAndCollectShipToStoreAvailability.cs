namespace FnacDirect.OrderPipe.Base.Business.Shipping.Shop
{
    public class ClickAndCollectShipToStoreAvailability : ShipToStoreAvailability
    {
        public ClickAndCollectShipToStoreAvailability(string referenceGu)
            : base(referenceGu)
        {
        }

        public ClickAndCollectShipToStoreAvailability(string referenceGu, int shippingMethodId)
            : base(referenceGu, shippingMethodId)
        {
        }
    }
}
