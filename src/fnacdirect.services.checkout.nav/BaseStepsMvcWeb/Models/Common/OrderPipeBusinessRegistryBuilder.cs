﻿using StructureMap;

namespace FnacDirect.OrderPipe.Base.Business.Registries
{
    public class OrderPipeBusinessRegistryBuilder
    {
        private readonly IRegistry _registry;

        public OrderPipeBusinessRegistryBuilder(IRegistry registry)
        {
            _registry = registry;
        }

        public IRegistry Registry
        {
            get
            {
                return _registry;
            }
        }
    }
}
