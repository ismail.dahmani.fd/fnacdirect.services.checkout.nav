﻿using FnacDirect.OrderPipe.Base.BaseModel.Model;
using FnacDirect.OrderPipe.Base.Business.Shipping.Shop;
using FnacDirect.OrderPipe.Base.LineGroups;
using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.OrderPipe.Base.Model.Facets;
using FnacDirect.OrderPipe.Base.Model.LineGroups;
using FnacDirect.OrderPipe.Base.Proxy.Store;
using System.Linq;

namespace FnacDirect.OrderPipe.Base.Business.Stock
{
    public class ClickAndCollectOnlyUnavailableArticleHandler : IUnavailableArticleHandler
    {
        private readonly IShipToStoreAvailabilityService _shipToStoreAvailabilityService;
        private readonly IFnacStoreService _fnacStoreService;
        private readonly ISwitchProvider _switchProvider;
        private readonly IGotLineGroups _iGotLineGroups;

        public ClickAndCollectOnlyUnavailableArticleHandler(IShipToStoreAvailabilityService shipToStoreAvailabilityService,
                                                        IFnacStoreService fnacStoreService,
                                                        ISwitchProvider switchProvider,
                                                        IGotLineGroups iGotLineGroups)
        {
            _shipToStoreAvailabilityService = shipToStoreAvailabilityService;
            _fnacStoreService = fnacStoreService;
            _switchProvider = switchProvider;
            _iGotLineGroups = iGotLineGroups;
        }

        public bool Handle(ILineGroup parentLineGroup, StandardArticle standardArticle)
        {
            if (!_switchProvider.IsEnabled("orderpipe.pop.clickandcollectonly"))
            {
                return false;
            }           

            if (standardArticle.Availability == StockAvailability.Central)
            {
                return false;
            }

            var shopData = _iGotLineGroups.GetPrivateData<ShopData>(create: false);

            if (shopData != null)
            {
                var shopAddress = shopData.ShopAddress;

                if (shopAddress != null
                    && shopAddress.Identity.HasValue)
                {
                    var fnacStore = _fnacStoreService.GetStore(shopAddress.Identity.Value);

                    if (fnacStore != null)
                    {
                        var clickAndCollectAvailabilities = _shipToStoreAvailabilityService.GetClickAndCollectAvailabilities(fnacStore, standardArticle.Yield());

                        if (clickAndCollectAvailabilities.Count() == 1)
                        {
                            var clickAndCollectAvailability = clickAndCollectAvailabilities.First();

                            if (clickAndCollectAvailability.ReferenceGu == standardArticle.ReferenceGU)
                            {
                                if (parentLineGroup.Seller.Type != SellerType.ClickAndCollectOnly)
                                {
                                    parentLineGroup.Articles.Remove(standardArticle);

                                    var reservedSellerIds = _iGotLineGroups.LineGroups.Where(lg => lg.Seller.Type == SellerType.ClickAndCollectOnly).Select(s => s.Seller.SellerId);
                                    var newSellerId = SellerIds.GetId(SellerType.ClickAndCollectOnly, reservedSellerIds);

                                    var lineGroup = _iGotLineGroups.BuildLineGroup(new ArticleList() { standardArticle });
                                    lineGroup.OrderType = OrderTypeEnum.FnacCom;
                                    lineGroup.Seller = new SellerInformation
                                    {
                                        SellerId = newSellerId
                                    };

                                    _iGotLineGroups.AddLineGroup(lineGroup);
                                }

                                standardArticle.Availability = StockAvailability.ClickAndCollectOnly;

                                return true;
                            }
                        }
                    }
                }
            }

            if (parentLineGroup.Seller.Type == SellerType.ClickAndCollectOnly)
            {
                // En cas de données non alimentées liées au magasin,
                // s'assurer que le handler gère quand même cet article
                standardArticle.Availability = StockAvailability.ClickAndCollectOnly;
                return true;
            }

            return false;
        }
    }
}
