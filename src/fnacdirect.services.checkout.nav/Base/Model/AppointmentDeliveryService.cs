using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.OrderPipe.Base.Model.Solex;
using FnacDirect.OrderPipe.Core.Services;
using FnacDirect.Technical.Framework.Utils;
using StructureMap;
using System;
using System.Collections.Generic;
using System.Linq;

namespace FnacDirect.OrderPipe.Base.Business.Shipping
{
    public class AppointmentDeliveryService : IAppointmentDeliveryService
    {
        private readonly IShippingEvaluationBusiness _shippingEvaluationBusiness;
        private readonly IContainer _container;
        private readonly IDictionary<string, string> _defaultDeliverySlotFactories;

        public AppointmentDeliveryService(IPipeParametersProvider pipeParametersProvider,
                                          IContainer container,
                                          ISwitchProvider switchProvider,
                                          IShippingEvaluationBusiness shippingEvaluationBusiness)
        {
            _container = container;
            _shippingEvaluationBusiness = shippingEvaluationBusiness;

            AggregatedShippingMethodIds = new HashSet<int>(pipeParametersProvider.GetAsEnumerable<int>("appointmentdelivery.AggregatedMethodId"));
            ShippingMethodsPriorityOrder = new HashSet<int>(pipeParametersProvider.GetAsEnumerable<int>("appointmentdelivery.ShippingMethodsPriorityOrder"));

            ShippingMethodsCourier = new HashSet<int>(pipeParametersProvider.GetAsEnumerable<int>("appointmentdelivery.ShippingMethods.Courier"));

            if (switchProvider.IsEnabled("orderpipe.fly.enabled"))
            {
                ShippingMethodsFly = new HashSet<int>(pipeParametersProvider.GetAsEnumerable<int>("appointmentdelivery.ShippingMethods.Fly"));
            }
            else
            {
                ShippingMethodsFly = new HashSet<int>();
            }

            ShippingMethods = ShippingMethodsCourier.Concat(ShippingMethodsFly).ToHashSet();

            ShippingMethodsThatOpensCalendar = new HashSet<int>(pipeParametersProvider.GetAsEnumerable<int>("appointmentdelivery.ShippingMethodsThatOpensCalendar"));
            _defaultDeliverySlotFactories = pipeParametersProvider.GetAsDictionary<string>("appointmentdelivery.DefaultDeliverySlotFactories");
        }

        public HashSet<int> ShippingMethodsPriorityOrder { get; }
        public HashSet<int> ShippingMethodsFly { get; }
        public HashSet<int> ShippingMethodsCourier { get; }
        public HashSet<int> ShippingMethods { get; }
        public HashSet<int> ShippingMethodsThatOpensCalendar { get; }

        public HashSet<int> AggregatedShippingMethodIds { get; }

        public void SynchronizeSelectedChoiceAccordingToSelectedDeliverySlot(IEnumerable<ShippingChoice> choices, DeliverySlotData deliverySlotData)
        {
            if (deliverySlotData == null || deliverySlotData.DeliverySlotSelected == null)
            {
                return;
            }

            var shippingChoice = choices.FirstOrDefault(choice => AggregatedShippingMethodIds.Contains(choice.MainShippingMethodId));

            if (shippingChoice != null
                && shippingChoice is AggregatedShippingChoice aggregatedShippingChoice
                && aggregatedShippingChoice.ContainsShippingMethodId(deliverySlotData.SlotShippingMethodId))
            {
                aggregatedShippingChoice.SelectedChoiceMainShippingMethodId = deliverySlotData.SlotShippingMethodId;

                foreach (var choice in aggregatedShippingChoice.ShippingChoices)
                {
                    foreach (var parcel in choice.Parcels.Values)
                    {
                        if (parcel.SelectedSlotSource != null)
                        {
                            parcel.Source = parcel.SelectedSlotSource;
                        }
                    }
                }
            }
            else
            {
                var deliverySlotChoice = choices.FirstOrDefault(choice => choice.MainShippingMethodId == deliverySlotData.DeliverySlotSelected.ShippingMethodId);

                if (deliverySlotChoice != null)
                {
                    foreach (var parcel in deliverySlotChoice.Parcels.Values)
                    {
                        if (parcel.SelectedSlotSource != null)
                        {
                            parcel.Source = parcel.SelectedSlotSource;
                        }
                    }
                }
            }
        }

        public DeliverySlot GetDefaultSlotForShippingMethod(int mainShippingMethodId)
        {
            if (_defaultDeliverySlotFactories.TryGetValue(mainShippingMethodId.ToString(), out var defaultDeliverySlotTypeName))
            {
                var defaultDeliverySlotFactoryType = Type.GetType(defaultDeliverySlotTypeName);

                if (_container.GetInstance(defaultDeliverySlotFactoryType) is IDefaultDeliverySlotFactory defaultDeliverySlotFactory)
                {
                    return defaultDeliverySlotFactory.Get();
                }
            }

            throw new InvalidOperationException($"There is no delivery slot configuration for the shipping method id {mainShippingMethodId}");
        }

        public interface IDefaultDeliverySlotFactory
        {
            DeliverySlot Get();
        }

        public class StuartDeliverySlotFactory : IDefaultDeliverySlotFactory
        {
            private readonly DateTimeProvider _dateTimeProvider;

            public StuartDeliverySlotFactory(DateTimeProvider dateTimeProvider)
            {
                _dateTimeProvider = dateTimeProvider;
            }

            public DeliverySlot Get()
            {
                var now = _dateTimeProvider.Now();
                var startDate = now.AddHours(1);
                var endDate = now.AddHours(2);

                return new DeliverySlot
                {
                    PartnerId = "2",
                    PartnerUniqueIdentifier = $"Stuart_{startDate.ToString("o")}",
                    StartDate = startDate,
                    EndDate = endDate,
                    SlotType = "Stuart",
                    ShippingMethodId = 57
                };
            }
        }

        public DeliverySlot GetDeliverySlot(PopOrderForm popOrderForm, string partnerId, string partnerUniqueIdentifier)
        {
            var deliverySlotData = popOrderForm.GetPrivateData<DeliverySlotData>(create: false);
            if (deliverySlotData == null || deliverySlotData.DeliverySlotChoices == null || !deliverySlotData.DeliverySlotChoices.Any())
            {
                return null;
            }

            var deliverySlot = deliverySlotData?.DeliverySlotChoices?.FirstOrDefault(d => (d.PartnerId == partnerId && d.PartnerUniqueIdentifier == partnerUniqueIdentifier));
            return deliverySlot;
        }

        public void SetSelectedDeliverySlot(PopOrderForm popOrderForm, DeliverySlot selectedDeliverySlot, int sellerId, int shippingMethodId)
        {
            var deliverySlotData = popOrderForm.GetPrivateData<DeliverySlotData>(create: false);

            selectedDeliverySlot.IsSelected = true;
            deliverySlotData.DeliverySlotSelected = selectedDeliverySlot;
            deliverySlotData.ChoiceShippingMethodId = shippingMethodId;
            deliverySlotData.SlotShippingMethodId = selectedDeliverySlot.ShippingMethodId;

            _shippingEvaluationBusiness.UpdateSelectedShippingMethodEvaluationItem(shippingMethodId, sellerId, popOrderForm.ShippingMethodEvaluation);
        }
    }
}
