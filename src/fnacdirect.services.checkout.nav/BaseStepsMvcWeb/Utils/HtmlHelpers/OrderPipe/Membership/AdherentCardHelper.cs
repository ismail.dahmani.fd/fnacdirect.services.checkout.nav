using FnacDirect.Contracts.Online.IModel;
using FnacDirect.Contracts.Online.Model;
using FnacDirect.Membership.Business.Common;
using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.OrderPipe.Base.Proxy.Membership;
using FnacDirect.Technical.Framework.Web.Mvc;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace FnacDirect.OrderPipe.BaseMvc.Web.HtmlHelpers.Pop.Membership
{
    public static class AdherentCardHelper
    {
        public static IEnumerable<string> GetAdherentCardPrefix(this HtmlHelper htmlHelper)
        {
            var _membershipCardBindingConfigurationBusiness = DependencyResolver.Current.GetService<IMembershipCardBindingConfigurationBusiness>();
            return _membershipCardBindingConfigurationBusiness != null ? _membershipCardBindingConfigurationBusiness.GetPrefixes() : new List<string>();
        }

        public static string GetPattern(this HtmlHelper htmlHelper)
        {
            var _membershipCardBindingConfigurationBusiness = DependencyResolver.Current.GetService<IMembershipCardBindingConfigurationBusiness>();
            return _membershipCardBindingConfigurationBusiness != null ? _membershipCardBindingConfigurationBusiness.GetPattern() : string.Empty;
        }

        public static int GetMaxLength(this HtmlHelper htmlHelper)
        {
            var _membershipCardBindingConfigurationBusiness = DependencyResolver.Current.GetService<IMembershipCardBindingConfigurationBusiness>();
            return _membershipCardBindingConfigurationBusiness != null ? _membershipCardBindingConfigurationBusiness.GetMaxLength() : 0;
        }

        public static bool IsTryCard(this HtmlHelper htmlHelper)
        {
            try
            {
                var opContext = DependencyResolver.Current.GetService<OPContext>();
                if (opContext != null && opContext.OF is PopOrderForm popOf)
                {
                    var _membershipConfigurationService = DependencyResolver.Current.GetService<IMembershipConfigurationService>();
                    return popOf.LineGroups.GetArticles().Any(a => a.ProductID == _membershipConfigurationService.GetTryCardPrid());
                }
            }
            catch
            {
            }

            return false;
        }

        public static bool HasAdherentCardInBasket(this HtmlHelper htmlHelper)
        {
            var opContext = DependencyResolver.Current.GetService<OPContext>();
            if (opContext != null && opContext.OF is PopOrderForm popOf)
            {
                var _membershipConfigurationService = DependencyResolver.Current.GetService<IMembershipConfigurationService>();
                return popOf.LineGroups.GetArticles().Any(a => _membershipConfigurationService.GetAdherentCardArticleByPrid(a.ProductID.Value) != null);
            }

            return false;
        }

        public static bool HasFnacPlusCardInBasket(this HtmlHelper htmlHelper)
        {
            var opContext = DependencyResolver.Current.GetService<OPContext>();
            var membershipConfigurationService = DependencyResolver.Current.GetService<IMembershipConfigurationService>();

            if (opContext != null && opContext.OF is PopOrderForm popOf)
            {
                return popOf.LineGroups.GetArticles().Any(a => membershipConfigurationService.FnacPlusCardTypes.Contains(a.TypeId.GetValueOrDefault()));
            }

            return false;
        }

        public static bool HasFnacPlusTryCardInBasket(this HtmlHelper htmlHelper)
        {
            try
            {
                var opContext = DependencyResolver.Current.GetService<OPContext>();
                var membershipConfigurationService = DependencyResolver.Current.GetService<IMembershipConfigurationService>();

                if (opContext != null && opContext.OF is PopOrderForm popOf)
                {
                    return popOf.LineGroups.GetArticles().Any(a => a.TypeId.GetValueOrDefault() == membershipConfigurationService.FnacPlusTryCardType);
                }
            }
            catch
            {
            }
            return false;
        }

        public static string GetFnacPlusTryCardDuration(this HtmlHelper htmlHelper)
        {
            var validityDuration = string.Empty;
            var opContext = DependencyResolver.Current.GetService<OPContext>();
            var applicationContext = DependencyResolver.Current.GetService<IApplicationContext>();
            var siteManagerBusiness2 = DependencyResolver.Current.GetService<ISiteManagerBusiness2>();
            var membershipConfigurationService = DependencyResolver.Current.GetService<IMembershipConfigurationService>();

            if (opContext != null && opContext.OF is PopOrderForm popOf)
            {
                var card = popOf.LineGroups.GetArticles().FirstOrDefault(a => a.TypeId.GetValueOrDefault() == membershipConfigurationService.FnacPlusTryCardType);

                if (card != null)
                {
                    // Get duration and typeId from New Ref
                    var context = new FnacDirect.Contracts.Online.Model.Context()
                    {
                        SiteContext = applicationContext.GetSiteContext()
                    };

                    var articleReference = new ArticleReference(card.ProductID, null, null, null, ArticleCatalog.FnacDirect);

                    var item = siteManagerBusiness2.GetArticle(context, articleReference);
                    if (item != null)
                    {
                        validityDuration = GetValidityString(item.ContractValidity.Type, item.ContractValidity.Value);
                    }
                }

            }

            return validityDuration;
        }

        private static string GetValidityString(ContractValidityType type, int duration)
        {
            var suffix = duration > 1 ? "s" : string.Empty;
            var id = "orderpipe.pop.membership.contractvaliditytype." + type.ToString().ToLowerInvariant() + suffix;
            return duration + " " + MessageHelper.Get(Technical.Framework.Web.FnacContext.Current.SiteContext, id, null);
        }

        public static int GetTryCardDuration(this HtmlHelper htmlHelper)
        {
            var opContext = DependencyResolver.Current.GetService<OPContext>();
            if (opContext != null)
            {
                return opContext.Pipe.GlobalParameters.GetAsInt("membership.try.duration");
            }

            return 0;
        }
    }
}
