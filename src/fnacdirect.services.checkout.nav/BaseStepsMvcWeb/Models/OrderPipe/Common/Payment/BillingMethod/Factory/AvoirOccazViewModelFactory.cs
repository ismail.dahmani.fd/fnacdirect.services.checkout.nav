using FnacDirect.Membership.Model.Entities;
using FnacDirect.OrderPipe.Base.Business;
using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.OrderPipe.Core.Services;
using FnacDirect.Technical.Framework.Web;
using FnacDirect.Technical.Framework.Web.Mvc;
using System;
using System.Globalization;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Common.Payment.BillingMethodFactories
{
    public class AvoirOccazViewModelFactory : BasePaymentViewModelFactory<AvoirOccazBillingMethod>
    {
        private readonly IApplicationContext _applicationContext;
        private readonly IPipeParametersProvider _pipeParametersProvider;
        private readonly IPriceHelper _priceHelper;
        private readonly IUserExtendedContextProvider _userExtendedContextProvider;

        public AvoirOccazViewModelFactory(IPipeParametersProvider pipeParametersProvider,
                                          IApplicationContext applicationContext,
                                          IUserExtendedContextProvider userExtendedContextProvider)
        {
            _applicationContext = applicationContext;
            _pipeParametersProvider = pipeParametersProvider;
            _priceHelper = applicationContext.GetPriceHelper();
            _userExtendedContextProvider = userExtendedContextProvider;
        }

        public override BillingMethodViewModel Get(string tabName, PaymentMethod paymentMethod, OPContext opContext, PopOrderForm popOrderForm)
        {
            var userExtendedContext = _userExtendedContextProvider.GetCurrentUserExtendedContext();

            var fidelityAllowedDelayAfterExpiration = _pipeParametersProvider.Get<int>("fidelity.advantages.alloweddelay.after.expiration");
            var viewModel = new FidelityAvoirOccazViewModel();
            if ((userExtendedContext.Customer.IsValidAdherent && (userExtendedContext.Customer.MembershipContract != null && userExtendedContext.Customer.MembershipContract.AdhesionStatus == AdhesionStatus.OutDated
                && userExtendedContext.Customer.MembershipContract.AdhesionEndDate.AddMonths(fidelityAllowedDelayAfterExpiration) < DateTime.Now)) || userExtendedContext.Customer.IsValidGamer)
            {
                var fidelityEuroBusiness = new FidelityBusiness<AvoirOccazBillingMethod>();
                var amount = fidelityEuroBusiness.GetAvailableAmount(userExtendedContext.Customer);

                var maxUsableAmount = fidelityEuroBusiness.GetMaxUsableAmount(VisitorAdapter.Current.GetVisitor().Customer, popOrderForm, 10);
                viewModel.Amount = _priceHelper.Format(amount, PriceFormat.CurrencyAsDecimalSeparator);
                var firstExpiry = fidelityEuroBusiness.GetFirstExpiry(VisitorAdapter.Current.GetVisitor().Customer);
                if (firstExpiry != null)
                {
                    viewModel.AmountToExpire = _priceHelper.Format(firstExpiry.Amount, PriceFormat.CurrencyAsDecimalSeparator);
                    viewModel.HasUniqueExpiration = maxUsableAmount <= firstExpiry.Amount;

                    if (firstExpiry.ExpirationDate <= DateTime.Now.AddDays(30))
                    {
                        viewModel.ExpireInLessThanOneMonth = true;
                    }
                    var culture = CultureInfo.GetCultureInfo(_applicationContext.GetLocale());
                    viewModel.Expiration = firstExpiry.ExpirationDate.ToString(culture.DateTimeFormat.ShortDatePattern);
                }
                var currentBillingMethod = fidelityEuroBusiness.GetCurrentBillingMethod(popOrderForm);
                if (currentBillingMethod != null)
                {
                    var usedAmount = fidelityEuroBusiness.GetCurrentBillingMethod(popOrderForm).Amount;
                    if (usedAmount.HasValue)
                    {
                        viewModel.Used = _priceHelper.Format(usedAmount.Value, PriceFormat.CurrencyAsDecimalSeparator);
                        viewModel.Active = true;
                        var remainingAmount = amount - usedAmount.Value;
                        if (remainingAmount > 0)
                        {
                            viewModel.Remaining = _priceHelper.Format(remainingAmount, PriceFormat.CurrencyAsDecimalSeparator);
                        }
                    }
                }
                viewModel.MaxUsableAmount = Helper.LiteralDecimal(maxUsableAmount);
            }

            viewModel.Display = true;
            return viewModel;
        }
    }
}
