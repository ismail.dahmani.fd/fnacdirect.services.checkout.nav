using FnacDirect.Customer.BLL;
using FnacDirect.Customer.Model;
using FnacDirect.OrderPipe;
using FnacDirect.OrderPipe.Base.Business;
using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.OrderPipe.Base.Steps;
using FnacDirect.Quote.DAL;
using FnacDirect.Quote.Model;
using FnacDirect.Quote.Model.Enums;
using FnacDirect.Technical.Framework.Web;
using System;
using System.IO;
using System.Threading;
using System.Xml.Serialization;

namespace FnacDirect.Quote.Business
{
    public class QuoteBusiness : BaseBusiness
    {
        public string Culture { get; }

        public QuoteBusiness()
            : base()
        {
            Culture = Thread.CurrentThread.CurrentUICulture.Name;
        }

        public SalesMan GetSalesMan(int accountId)
        {
            var biz = new OrderPipe.Reporting.SalesManBusiness();

            var l_salesMan = biz.GetSalesMan(accountId);
            if (l_salesMan == null)
            {
                // pour garder une rétro-compatibilité
                l_salesMan = new SalesMan
                {
                    IsGeneric = true
                };
            }
            return l_salesMan;

        }

        public void Save(QuoteType prmiType, QuoteForm quoteForm)
        {
            quoteForm.OrderInfo.OrderType = (int)prmiType;
            quoteForm.QuoteXmlContent = SerializeQuoteForm(quoteForm);
            if (0 == quoteForm.QuoteId)
            {
                quoteForm.QuoteId = QuoteDAL.Add(quoteForm);

                quoteForm.QuoteXmlContent = SerializeQuoteForm(quoteForm);
                QuoteDAL.Write(quoteForm);
            }
            else
            {
                QuoteDAL.Update(quoteForm);
            }
        }

        /// <summary>
        /// Retrieves serialized QuoteForm
        /// </summary>
        /// <param name="quoteForm">QuoteForm to serialize</param>
        /// <returns></returns>
        private string SerializeQuoteForm(QuoteForm quoteForm)
        {
            quoteForm.QuoteXmlContent = string.Empty;
            var l_result = string.Empty;
            using (var sw = new StringWriter())
            {
                var ser = GetQuoteFormSerializer();

                ser.Serialize(sw, quoteForm);
                l_result = sw.ToString();
            }

            return l_result;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="quoteEntity"></param>
        /// <param name="culture"></param>
        private void InitializeAllProperties(QuoteEntity quoteEntity)
        {
            if (null != quoteEntity && !string.IsNullOrEmpty(quoteEntity.QuoteXmlContent))
            {
                var l_serializer = GetQuoteFormSerializer();
                var l_customerBusiness = new CustomerBusiness();
                var l_quoteForm = l_serializer.Deserialize(new StringReader(quoteEntity.QuoteXmlContent)) as QuoteForm;
                //On synchronise les champs de l'enveloppe avec l'objet QuoteForm
                l_quoteForm.Status = quoteEntity.Status;
                l_quoteForm.ExpirationDate = quoteEntity.ExpirationDate;
                l_quoteForm.LastModificationDate = quoteEntity.LastModificationDate;
                l_quoteForm.QuoteDemandRaison = quoteEntity.QuoteDemandRaison;

                quoteEntity.QuoteForm = l_quoteForm;
                quoteEntity.QuoteForm.QuoteLineDetailCollection = l_quoteForm.QuoteLineDetailCollection;

                quoteEntity.QuoteForm.Company = l_customerBusiness.GetCustomerCompanyDetail(l_quoteForm.Company.ID, Thread.CurrentThread.CurrentUICulture.Name);
            }
        }

        public QuoteEntity GetQuoteDetailBySID(string quoteSid)
        {
            var quoteEntity = QuoteDAL.GetQuoteDetailBySID(quoteSid);
            InitializeAllProperties(quoteEntity);
            return quoteEntity;
        }

        private XmlSerializer GetQuoteFormSerializer()
        {
            return new XmlSerializer(typeof(QuoteForm),
                new Type[]{
                typeof(DisplayData),
                typeof(PhoneValidData),
                typeof(ShippingAddressData),
                typeof(BillingAddressData),
                typeof(DisplayData),
                // billingMethod
                typeof(BankTransferBillingMethod),
                typeof(BankCreditCardBillingMethod),
                typeof(CheckBillingMethod),
                typeof(DefferedPaymentBillingMethod),
                typeof(DefferedPaymentData)
                });
        }

        /// <summary>
        /// Retourne un label localisé sur l'état d'un devis
        /// RG-DE-0030
        /// </summary>
        /// <param name="quoteStatus"></param>
        /// <returns></returns>
        public static string GetQuoteStatusClientLabel(QuoteStatusEnum quoteStatus)
        {
            switch (quoteStatus)
            {
                case QuoteStatusEnum.Validated:
                    return MessageHelper.Get(FnacContext.Current.SiteContext, "B2B.QUOTE.STATUS.Validated");

                case QuoteStatusEnum.Ordred:
                    return MessageHelper.Get(FnacContext.Current.SiteContext, "B2B.QUOTE.STATUS.Ordred");

                case QuoteStatusEnum.TimeExpired:
                    return MessageHelper.Get(FnacContext.Current.SiteContext, "B2B.QUOTE.STATUS.TimeExpired");

                case QuoteStatusEnum.Refused:
                    return MessageHelper.Get(FnacContext.Current.SiteContext, "B2B.QUOTE.STATUS.Refused");

                case QuoteStatusEnum.Aborted:
                    return MessageHelper.Get(FnacContext.Current.SiteContext, "B2B.QUOTE.STATUS.Refused");

                case QuoteStatusEnum.Created:
                    return MessageHelper.Get(FnacContext.Current.SiteContext, "B2B.QUOTE.STATUS.ToValidate");

                case QuoteStatusEnum.Modified:
                    return MessageHelper.Get(FnacContext.Current.SiteContext, "B2B.QUOTE.STATUS.ToValidate");

                case QuoteStatusEnum.ToValidate:
                    return MessageHelper.Get(FnacContext.Current.SiteContext, "B2B.QUOTE.STATUS.ToValidate");

                case QuoteStatusEnum.CancelByUser:
                    return MessageHelper.Get(FnacContext.Current.SiteContext, "B2B.QUOTE.STATUS.CancelByUser");

                default:
                    return quoteStatus.ToString();
            }
        }
    }
}
