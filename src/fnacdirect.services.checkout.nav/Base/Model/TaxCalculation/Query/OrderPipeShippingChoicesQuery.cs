using FnacDirect.OrderPipe.Base.Model.Solex;
using FnacDirect.OrderPipe.TaxCalculation.Front.Models;
using System.Collections.Generic;
using System.Linq;

namespace FnacDirect.OrderPipe.Base.Model.TaxCalculation
{
    public class OrderPipeShippingChoicesQuery : Query
    {
        public int TaxType { get; set; }
        public List<TaxCalcultationQueryShippingChoice> Choices { get; set; }

        public ShippingChoiceQuery ToShippingChoiceQuery()
        {
            var shippingChoiceQuery = new ShippingChoiceQuery
            {
                OrderContext = OrderContext.ToOrderContext(),
                TaxType = TaxType,
                ShippingChoices = Choices.Select(c => c.ToShippingChoiceRequest()).ToList() 
            };

            return shippingChoiceQuery;
        }
    }

    public class TaxCalcultationQueryShippingChoice
    {
        public int MainShippingMethodId { get; set; }
        public decimal TotalPrice { get; set;  }
        public IEnumerable<TaxCalculationQueryGroup> Groups{ get; set; }

        public TaxCalcultationQueryShippingChoice(){}

        public TaxCalcultationQueryShippingChoice(ShippingChoice choice)
        {
            MainShippingMethodId = choice.MainShippingMethodId;
            TotalPrice = choice.TotalPrice.Cost;
            Groups = choice.Groups != null  ? choice.Groups.Select(group => new TaxCalculationQueryGroup(group))
                                            : Enumerable.Empty<TaxCalculationQueryGroup>();
        }

        internal ShippingChoiceRequest ToShippingChoiceRequest()
        {
            var shippingChoiceRequest = new ShippingChoiceRequest
            {
                MainShippingMethodId = MainShippingMethodId,
                TotalPrice = TotalPrice,
                Groups = Groups.Select(g => g.ToGroupRequest())
            };

            return shippingChoiceRequest;
        }
    }

    public class TaxCalculationQueryGroup
    {
        public int Id { get; set; }
        public decimal GlobalPrice { get; set; }
        public IEnumerable<TaxCalcuationQueryItemInGroup> Items { get; set; }

        public TaxCalculationQueryGroup(){ }
        public TaxCalculationQueryGroup(Group group)
        {
            Id = group.Id;
            GlobalPrice = group.GlobalPrice.Cost;
            Items = group.Items.Select(item => new TaxCalcuationQueryItemInGroup(item));
        }

        internal GroupRequest ToGroupRequest()
        {
            var groupRequest = new GroupRequest
            {
                Id = Id,
                GlobalPrice = GlobalPrice,
                Items = Items.Select(i => i.ToItemInGroupRequest())
            };

            return groupRequest;
        }
    }

    public class TaxCalcuationQueryItemInGroup
    {
        public Identifier Identifier { get; set; }
        public decimal UnitPrice { get; set; }

        public TaxCalcuationQueryItemInGroup(){ }

        public TaxCalcuationQueryItemInGroup(ItemInGroup item)
        {
            Identifier = new Identifier
            {
                Prid = item.Identifier.Prid.GetValueOrDefault(),
                OfferReference = item.Identifier.OfferReference
            };
            UnitPrice = item.UnitPrice.Cost;
        }

        internal ItemInGroupRequest ToItemInGroupRequest()
        {
            var itemInGroupRequest = new ItemInGroupRequest
            {
                Identifier = new OrderPipe.TaxCalculation.Front.Models.Identifier
                {
                    Prid = Identifier.Prid,
                    OfferReference = Identifier.OfferReference
                },
                UnitPrice = UnitPrice
            };

            return itemInGroupRequest;
        }
    }
}
