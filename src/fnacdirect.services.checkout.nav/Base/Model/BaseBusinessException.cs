using System;
using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.Technical.Framework.CoreServices;
using System.Diagnostics;

namespace FnacDirect.OrderPipe.Base.Business
{
    public enum BusinessRange:ushort
    {
        Purchase = 55500,
        ShippingService = 55590,
        Init = 55600,
        MissingParameter = 55700
    }

    public class BaseBusinessException: Exception
    {
        private readonly ushort _code;

        private BaseBusinessException(string message, Exception e, ushort code)
            : base(message, e)
        {
            _code = code;
        }

        public void Log()
        {
            Logging.Current.WriteError(this, _code);
        }

        public BaseBusinessException WithData(string key, object value)
        {
            Data.Add(key, value);
            return this;
        }

        public static BaseBusinessException BuildException(string message, Exception e, BusinessRange range, ushort subcode, params object[] prm)
        {
            var trace=new StackTrace(1);
            var biz = trace.GetFrame(0).GetMethod().ReflectedType.Name;
            var method = trace.GetFrame(0).GetMethod().Name;
            var code = (ushort)(range + subcode);
            var d = new BaseBusinessException(
                string.Format("[{0:00000}] {1}.{2} : {3}{4}", code, biz, method, message, e==null?"":" ("+e.Message+")"), e, code);
            d.Data["MESSAGE"] = message;
            d.Data["BUSINESS"] = biz;
            d.Data["METHOD"] = method;
            d.Data["CODE"] = code;
            if (prm != null)
                for (var i = 0; i < prm.Length / 2; i++)
                    d.Data["DATA." + prm[i]] = prm[i + 1];
            d.Log();
            return d;
        }

        public static BaseBusinessException BuildException(string message, OPContext context, ErrorInfo info, BaseOF orderForm, BusinessRange range, ushort subcode, params object[] prm)
        {
            var code = (ushort)(range + subcode);
            var d = new BaseBusinessException(
                string.Format("[{0:00000}] {1}{2}", 
                    code,
                    message, 
                    info==null || info.Exception == null ? "" : " (" + info.Exception.Message + ")"
                    ), info?.Exception, code);
            d.Data["MESSAGE"] = message;
            d.Data["CODE"] = code;
            if (context!=null)
                d.Data["PIPE"] = context.Pipe.PipeName;
            if (info != null)
            {
                if (info.FailedStep!=null)
                    d.Data["ERROR.STEP"] = info.FailedStep.Name;
                if (info.Error != null)
                {
                    d.Data["ERROR.CODE"] = info.Error.Code;
                    d.Data["ERROR.DESCRIPTION"] = info.Error.Description;
                    d.Data["ERROR.TYPE"] = info.Error.Type;
                }
            }
            if (orderForm != null)
            {
                d.Data["SID"] = orderForm.SID;
                if (orderForm.UserInfo != null)
                {
                    d.Data["USER.UID"] = orderForm.UserInfo.UID;
                    d.Data["USER.E-MAIL"] = orderForm.UserInfo.ContactEmail;
                }
                if (orderForm.OrderInfo != null)
                {
                    d.Data["ORDER.PK"] = orderForm.OrderInfo.MainOrderPk;
                    d.Data["ORDER.USERREFERENCE"] = orderForm.OrderInfo.MainOrderUserReference;
                }
            }
            if (prm != null)
                for (var i = 0; i < prm.Length / 2; i++)
                    d.Data["DATA." + prm[i]] = prm[i + 1];
            return d;
        }
    }
}
