using System.Collections.Generic;
using FnacDirect.OrderPipe.Base.Model;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Common.Shipping.Postal
{
    public interface IPostalShippingNetworkViewModelBuilder : IShippingNetworkViewModelBuilder<PostalShippingNetworkViewModel>
    {
        PostalDetailsViewModel BuildDetail(PostalAddress address, bool hasMissingInfo = false, bool hasManyEligibleBillingAddresses = false);

        IEnumerable<PostalShippingChoiceViewModel> BuildPostalChoiceViewModel(PopOrderForm popOrderForm, int sellerId);
    }
}
