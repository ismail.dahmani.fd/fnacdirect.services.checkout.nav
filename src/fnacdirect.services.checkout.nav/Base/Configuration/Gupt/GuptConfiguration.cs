using System.Collections.Generic;

namespace FnacDirect.OrderPipe.Base.Business
{
    public class GuptConfiguration
    {
        public Dictionary<string, GuptPromotionCodeConfiguration> Promotions
        {
            get;
            set;
        }

        public List<string> RebateGroups
        {
            get;
            set;
        }

        public decimal DefaultRebateValidationThreshold { get; set; }

        public Dictionary<string, RebateValidationThresholdConfiguration> RebateValidationThresholds
        {
            get;
            set;
        }
    }
}
