using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.OrderPipe.Common.Payment
{
    public class UnavailablePaymentViewModel
    {
        public string Type { get; set; }
        public string Name { get; set; }
    }
}
