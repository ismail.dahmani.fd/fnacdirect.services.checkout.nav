
using FnacDirect.Technical.Framework.Web.Mvc;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Basket
{
    public class WrapViewModel
    {
        public string Price { get; set; }

        public PriceDetailsViewModel PriceModel { get; set; }

        public string PriceNoVAT { get; set; }

        public PriceDetailsViewModel PriceNoVATModel { get; set; }

        public string PriceToDisplay { get; set; }

        public PriceDetailsViewModel PriceModelToDisplay { get; set; }

        public bool IsFree { get; set; }

        public string Dedication { get; set; }

        public bool ForceDedicationDisplay { get; set; }

        public bool HasDedication
        {
            get
            {
                return !string.IsNullOrWhiteSpace(Dedication);
            }
        }

        public bool IsApplied { get; set; }

        public bool IsAvailable { get; set; }

        public bool DisplayNotWrapedArticlesMessage { get; set; }
        public string WrapDetailText { get; set; }
    }
}
