﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FnacDirect.OrderPipe.Base.Model;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Basket.Children
{
    public class DefaultCrossSellRichBasketItemChildrenViewModelBuilder : ICompositeCrossSellRichBasketItemChildrenViewModelBuilder
    {

        private readonly IEnumerable<ICrossSellRichBasketItemChildrenViewModelBuilder> _richBasketItemChildrenViewModelBuilders;

        public DefaultCrossSellRichBasketItemChildrenViewModelBuilder(IEnumerable<ICrossSellRichBasketItemChildrenViewModelBuilder> richBasketItemChildrenViewModelBuilders)
        {
            _richBasketItemChildrenViewModelBuilders = richBasketItemChildrenViewModelBuilders;
        }

        public IEnumerable<CrossSellRichItemChildrenViewModel> Build(StandardArticle standardArticle)
        {
            foreach (var item in _richBasketItemChildrenViewModelBuilders)
            {
                var results = item.Build(standardArticle).ToList();

                if (results.Any())
                {
                    return results.Where(c => !c.IsDummy);
                }
            }
            return Enumerable.Empty<CrossSellRichItemChildrenViewModel>();
        }

        public IEnumerable<ChildBasketItemViewModel> Build(StandardArticle standardArticle, IEnumerable<Promotion> globalPromotions, bool applyAdherentPromotion)
        {
            throw new NotImplementedException();
        }
    }
}
