using FnacDirect.OrderPipe.Base.Business.Wrap;
using FnacDirect.OrderPipe.Base.Model;
using System.Collections.Generic;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Common
{
    public class ConfigurationViewModelBuilder : IConfigurationViewModelBuilder
    {
        private readonly IWrapBusiness _wrapBusiness;
        private readonly ISwitchProvider _switchProvider;

        public ConfigurationViewModelBuilder(IWrapBusiness wrapBusiness, ISwitchProvider switchProvider)
        {
            _wrapBusiness = wrapBusiness;
            _switchProvider = switchProvider;
        }

        public SortedDictionary<string, bool> Build(PopOrderForm popOrderForm)
        {
            var configuration = new SortedDictionary<string, bool>();
            InitializeDefaultConfiguration(configuration);

            //Wrap
            if(_switchProvider.IsEnabled("orderpipe.pop.enable.wrap.v2"))
                configuration["wrapV2"] = true;
            else
                configuration["wrapV1"] = true;

            configuration["cobadge"] = _switchProvider.IsEnabled("orderpipe.pop.enable.cobadge");

            configuration["pushEngine"] = _switchProvider.IsEnabled("orderpipe.pop.pushengine.enable");

            return configuration;
        }

        private void InitializeDefaultConfiguration(SortedDictionary<string, bool> configuration)
        {
            configuration.Add("wrapV1", false);
            configuration.Add("wrapV2", false);
            configuration.Add("pushEngine", false);
            configuration.Add("cobadge", false);
        }
    }
}
