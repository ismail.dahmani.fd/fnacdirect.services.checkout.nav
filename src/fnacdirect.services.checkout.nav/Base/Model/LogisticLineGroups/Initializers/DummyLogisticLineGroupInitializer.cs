﻿using System.Collections.Generic;
using FnacDirect.OrderPipe.Base.LineGroups;
using FnacDirect.OrderPipe.Base.Model;
using System.Linq;
using FnacDirect.Technical.Framework.Velocity;
using FnacDirect.OrderPipe.Base.Business.LogisticLineGroups.Internals.Initializers;

namespace FnacDirect.OrderPipe.Base.Business.LogisticLineGroups.Initializers
{
    public class DummyLogisticLineGroupInitializer : BaseLogisticLineGroupInitializer
    {
        private readonly ILogisticLineGroupGenerator _logisticLineGroupGenerator;

        public DummyLogisticLineGroupInitializer(ILogisticLineGroupGenerator logisticLineGroupGenerator)
        {
            _logisticLineGroupGenerator = logisticLineGroupGenerator;
        }

        public override IEnumerable<ILogisticLineGroup> Initialize(IDictionary<ILineGroupHeader, IEnumerable<Article>> selectedLineGroups)
        {
            var shippingMethodEvaluation = Context.IGotShippingMethodEvaluation.ShippingMethodEvaluation;

            var output = new List<ILogisticLineGroup>();

            foreach (var lineGroup in selectedLineGroups.Keys)
            {
                var logisticLineGroup = _logisticLineGroupGenerator.Generate(lineGroup);

                logisticLineGroup.Articles = new List<Article>(selectedLineGroups[lineGroup]);

                Helper.CopyShippingMethodEvaluation(lineGroup, logisticLineGroup, shippingMethodEvaluation);

                output.Add(logisticLineGroup);
            }

            return output;
        }
    }
}
