using System.Collections.Generic;

namespace FnacDirect.OrderPipe.Base.Business.Limonetik
{
	public interface ILimonetikBusiness
	{
        bool IsEnabled(OPContext opContext);

        List<int> GetValidLimonetikCardTypes(OPContext opContext);
    }
}
