using System;
using FnacDirect.OrderPipe.Base.BaseModel.Model.MarketPlace;
using FnacDirect.Technical.Framework.CoreServices;
using System.Data.SqlClient;
using FnacDirect.OrderPipe.Base.DAL;
using System.Data;

namespace FnacDirect.OrderPipe.Base.BaseDAL
{
    public class MarketPlaceOfferRefDAL
    {
        private const string DATABASE_SqlOfferRef = "SqlMarketPlaceOfferRef";

        public MarketPlaceReservation AddReservation(int offerId, int quantity, int timeoutInSecond)
        {
            MarketPlaceReservation returnValue = null;
            try
            {
                using (var database = Database.CreateInstance(DATABASE_SqlOfferRef))
                {
                    using (var command = database.GetStoredProcCommand("spMP_AddReservation"))
                    {
                        database.AddInParameter(command, "OfferId", SqlDbType.Int, offerId);
                        database.AddInParameter(command, "Quantity", SqlDbType.Int, quantity);
                        database.AddInParameter(command, "TimeoutInSecond", SqlDbType.Int, timeoutInSecond);

                        using (var reader = command.ExecuteReader())
                        {
                            returnValue = BuildMarketPlaceReservation(reader);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw DALException.BuildException(ex, DALRange.MarketPlaceRef, 1);
            }

            return returnValue;
        }

        public MarketPlaceReservation RefreshReservationDate(int reservationId, int quantity, int timeoutInSecond)
        {
            MarketPlaceReservation returnValue = null;
            try
            {
                using (var database = Database.CreateInstance(DATABASE_SqlOfferRef))
                {
                    using (var command = database.GetStoredProcCommand("spMP_RefreshReservation"))
                    {
                        database.AddInParameter(command, "ReservationId", SqlDbType.Int, reservationId);
                        database.AddInParameter(command, "Quantity", SqlDbType.Int, quantity);
                        database.AddInParameter(command, "TimeoutInSecond", SqlDbType.Int, timeoutInSecond);

                        using (var reader = command.ExecuteReader())
                        {
                            returnValue = BuildMarketPlaceReservation(reader);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw DALException.BuildException(ex, DALRange.MarketPlaceRef, 2);
            }

            return returnValue;
        }

        private MarketPlaceReservation BuildMarketPlaceReservation(SqlDataReader reader)
        {
            MarketPlaceReservation result = null;

            if (reader.Read())
            {
                result = new MarketPlaceReservation
                {
                    Id = DataReaderHelper.GetField<int>(reader, "RESERVATIONID_PK"),
                    Quantity = DataReaderHelper.GetField<int>(reader, "QUANTITY")
                };
            }

            return result;
        }

        public void ValidateReservation(int reservationId, int orderDetailId)
        {
            try
            {
                using (var database = Database.CreateInstance(DATABASE_SqlOfferRef))
                {
                    using (var command = database.GetStoredProcCommand("spMP_ValidateReservation"))
                    {
                        database.AddInParameter(command, "ReservationId", SqlDbType.Int, reservationId);
                        database.AddInParameter(command, "OrderDetailId", SqlDbType.Int, orderDetailId);
                        command.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception ex)
            {
                throw DALException.BuildException(ex, DALRange.MarketPlaceRef, 3);
            }
        }

        public void DeleteReservation(int reservationId)
        {
            try
            {
                using (var database = Database.CreateInstance(DATABASE_SqlOfferRef))
                {
                    using (var command = database.GetStoredProcCommand("spMP_DeleteReservation"))
                    {
                        database.AddInParameter(command, "ReservationId", SqlDbType.Int, reservationId);
                        command.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception ex)
            {
                throw DALException.BuildException(ex, DALRange.MarketPlaceRef, 3);
            }
        }
    }
}
