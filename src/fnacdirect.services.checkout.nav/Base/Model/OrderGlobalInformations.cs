using System;
using System.Xml.Serialization;

namespace FnacDirect.OrderPipe.Base.Model.OrderInformations
{
    public class OrderGlobalInformations : IOrderGlobalInformations
    {
        public string AdvantageCode { get; set; }
        
        public int MessageCodes { get; set; }

        [XmlIgnore]
        public bool ModeBatch { get; set; }

        public bool OrderInserted { get; set; }

        public bool OrderCancelled { get; set; }

        public bool OgoneDirectLinkUnfinished { get; set; }

        public string PaymentReference { get; set; }

        public string MerchantId { get; set; }

        public string BankAccountNumber { get; set; }

        public DateTime OrderDate { get; set; }

        public int MainOrderPk { get; set; }

        public Guid MainOrderReference { get; set; }

        public string MainOrderUserReference { get; set; }

        public OrderTransactionInfoList MainOrderTransactionInfos { get; set; }

        public bool HasOgoneBeenCalled { get; set; }

        [XmlIgnore]
        public bool OrderValidated { get; set; }

        public string OrderExternalReference { get; set; }

        public string SenderEmail { get; set; }
    }
}
