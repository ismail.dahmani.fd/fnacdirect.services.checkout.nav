namespace FnacDirect.OrderPipe.Base.Model.SelfCheckout
{
    public class SelfCheckoutData : GroupData
    {
        public bool DisplaySelfCheckoutValidated { get; set; }
        public string InternalCode { get; set; }
        public bool CCVRequirementDone { get; set; }
    }
}
