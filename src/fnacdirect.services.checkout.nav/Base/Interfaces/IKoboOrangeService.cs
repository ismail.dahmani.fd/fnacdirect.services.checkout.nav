namespace FnacDirect.OrderPipe.Base.Proxy.Nav
{
    public interface IKoboOrangeService
    {
        bool IsValidOrangeRequest();
    }
}
