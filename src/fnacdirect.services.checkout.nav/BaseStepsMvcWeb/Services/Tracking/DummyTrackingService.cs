using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.OrderPipe.Base.Model.Tracking;
using FnacDirect.OrderPipe.BaseMvc.Web.Models.Tracking;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Services.Tracking
{
    class DummyTrackingService : ITrackingService
    {
        public void TrackArticle(PopOrderForm popOrderForm, TrackingArticleModel trackingArticleModel, EventTracking eventTracking)
        {
            
        }

        public void TrackArticles(PopOrderForm popOrderForm, IEnumerable<TrackingArticleModel> trackingArticleModels, EventTracking eventTracking)
        {
            
        }
    }
}
