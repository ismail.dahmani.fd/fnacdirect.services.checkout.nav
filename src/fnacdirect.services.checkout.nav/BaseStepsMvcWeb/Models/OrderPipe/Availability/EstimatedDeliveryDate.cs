using FnacDirect.OrderPipe.Base.Model;
using System;

namespace FnacDirect.OrderPipe.Base.Business.Availability
{
    public class EstimatedDeliveryDate
    {
        private readonly string _label;
        private readonly long _priority;
        private readonly string _articleTitle;
        private readonly int _prid;
        private readonly Guid? _offerRef;

        public string Label
        {
            get
            {
                return _label;
            }
        }

        public long Priority
        {
            get
            {
                return _priority;
            }
        }

        public string ArticleTitle
        {
            get
            {
                return _articleTitle;
            }
        }

        public int Prid
        {
            get { return _prid; }
        }

        public Guid? OfferRef
        {
            get { return _offerRef; }
        }

        public int MethodId { get; set; }

        public bool BeforeXmas { get; set; }

        public DateTime? StartDate { get; private set; }
        public DateTime? EndDate { get; private set; }

        public EstimatedDeliveryDate(string label, long priority, PopAvailabilityProcessorInput input)
        {
            _label = label;
            _priority = priority;

            _articleTitle = input.Article.Title;
            _prid = input.Article.ProductID.GetValueOrDefault();

            StartDate = input.DateMin;
            EndDate = input.DateMax;

            if (input.Article is MarketPlaceArticle mp)
            {
                _offerRef = mp.Offer.Reference;
            }
        }
    }
}
