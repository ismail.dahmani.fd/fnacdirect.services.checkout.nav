using FnacDirect.Technical.Framework.Web;
using FnacDirect.Technical.Framework.Web.Mvc;
using FnacDirect.Technical.Framework.Web.UrlRedirect;
using System.Collections.Generic;
using System.Web.Mvc;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Controllers.Pop
{
    public class PopLoginGatewayController : Controller
    {
        private readonly IUrlWriterHelper _urlWriterHelper;
        private readonly IVisitorAdapter _visitorAdapter;

        public PopLoginGatewayController(IVisitorAdapter visitorAdapter, IUrlWriterHelper urlWriterHelper)
        {
            _urlWriterHelper = urlWriterHelper;
            _visitorAdapter = visitorAdapter;
        }

        [HttpGet]
        public ActionResult Challenge(string redirectUrl, string logontype, string logonModes)
        {
            if(_visitorAdapter.GetVisitor().Session.IsGranted(Customer.Model.Constants.SessionLevel.Full))
            {
                return Redirect(_urlWriterHelper.WrapUrlWithBouncePage(redirectUrl, null));
            }

            var acrProperties = new Dictionary<string, string>
            {
                { "logonType", logontype },
                { "logonModes", logonModes }
            };

            return new ChallengeResult(redirectUrl, "oidc", acrProperties);
        }
    }
}
