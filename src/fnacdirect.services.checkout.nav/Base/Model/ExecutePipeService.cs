using FnacDirect.Context;
using FnacDirect.OrderPipe.Core;
using FnacDirect.Technical.Framework.ServiceLocation;
using System;

namespace FnacDirect.OrderPipe.CoreRouting
{
    public class ExecutePipeService : IExecutePipeService
    {
        private readonly Func<IOPManager> _getOpManager;
        private readonly Func<IOPContextProvider> _getOpContextProvider;
        private readonly Func<IUserContextProvider> _getUserContextProvider;
        private readonly Func<IOrderPipeResetStrategyLocator> _getOrderPipeResetStrategyLocator;
        private readonly Func<ISwitchProvider> _switchProvider;

        public ExecutePipeService()
        {
            _getOpManager = ServiceLocator.Current.GetInstance<IOPManager>;
            _getOpContextProvider = ServiceLocator.Current.GetInstance<IOPContextProvider>;
            _getUserContextProvider = ServiceLocator.Current.GetInstance<IUserContextProvider>;
            _getOrderPipeResetStrategyLocator = ServiceLocator.Current.GetInstance<IOrderPipeResetStrategyLocator>;
            _switchProvider = ServiceLocator.Current.GetInstance<ISwitchProvider>;
        }

        public PipeExecutionResult ExecuteOrderPipe(string pipeName)
        {
            return ExecuteOrderPipe(pipeName, false, pipe => null);
        }

        public PipeExecutionResult ExecuteOrderPipe(string pipeName, Action<OPContext> opContextSetup)
        {
            return ExecuteOrderPipe(pipeName, false, pipe => null, opContextSetup);
        }

        public PipeExecutionResult ExecuteOrderPipe(string pipeName, bool pipeReset, Func<IOP, string> getForceStep)
        {
            return ExecuteOrderPipe(pipeName, pipeReset, getForceStep ?? (pipe => null), null);
        }

        public PipeExecutionResult ExecuteOrderPipe(string pipeName, bool pipeReset, Func<IOP, string> getForceStep, Action<OPContext> opContextSetup)
        {
            return ExecuteOrderPipe(pipeName, pipeReset, getForceStep ?? (pipe => null), null, true);
        }

        public PipeExecutionResult ExecuteOrderPipe(string pipeName, 
                                                    bool pipeReset, 
                                                    Func<IOP, string> getForceStep, 
                                                    Action<OPContext> opContextSetup,
                                                    bool applyRedirect)
        {
            var opManager = _getOpManager();

            var pipe = opManager[pipeName];

            var opContextProvider = _getOpContextProvider();

            var userContext = _getUserContextProvider().GetCurrentUserContext();

            var opContext = opContextProvider.GetCurrentContext(pipeName);

            if (opContext == null)
            {
                opContext = opContextProvider.GetOrCreateCurrentContext(userContext.GetSessionId().ToString(), userContext.GetUserGuid().ToString(), pipe);

                if (pipeReset)
                {
                    HandlePipeResetRequest(opContext, applyRedirect);
                }

                opContextSetup?.Invoke(opContext);

                var pipeResult = Execute(opContext, getForceStep?.Invoke(pipe), pipe);

                return new PipeExecutionResult(opContext, pipeResult.UIDescriptor);
            }

            return new PipeExecutionResult(opContext, opContext.CurrentStep.UIDescriptor);
        }

        private PipeResult Execute(OPContext opContext, string forceStep, IOP pipe)
        {
            var userContextProvider = _getUserContextProvider();

            var userContext = userContextProvider.GetCurrentUserContext();

            opContext.UID = userContext.GetUserGuid().ToString();

            var pipeResult = pipe.Run(opContext, forceStep, false);

            opContext.CurrentStep = pipeResult.CurrentStep;

            return pipeResult;
        }

        private void HandlePipeResetRequest(OPContext opContext, bool applyRedirect)
        {
            var orderPipeResetStrategyLocator = _getOrderPipeResetStrategyLocator();

            var orderPipeResetStrategy = orderPipeResetStrategyLocator.GetOrderPipePipeRedirectionStrategyFor(opContext);

            orderPipeResetStrategy.DoResetFor(opContext, applyRedirect);
        }

        public bool IsKnownOrderPipe(string pipeName)
        {
            var opManager = _getOpManager();
            return opManager.IsKnown(pipeName);
        }

        public bool CanExecute(string pipeName)
        {
            var opManager = _getOpManager();
            var pipeConf = opManager.LoadConfigurationOnly(pipeName);

            var switchPipeId = pipeConf.SwitchId;
            if (switchPipeId != null)
            {
                var switchProvider = _switchProvider();
                return switchProvider.IsEnabled(switchPipeId, true);
            }
            return true;
        }
    }
}
