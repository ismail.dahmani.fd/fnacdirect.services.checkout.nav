﻿using System.Collections.Generic;
using FnacDirect.OrderPipe.Base.LineGroups;
using FnacDirect.OrderPipe.Base.Model;
using System.Linq;
using FnacDirect.Technical.Framework.Velocity;
using FnacDirect.OrderPipe.Base.Business.LogisticLineGroups.Internals.Initializers;

namespace FnacDirect.OrderPipe.Base.Business.LogisticLineGroups.Initializers
{
    public class DematSoftLogisticLineGroupInitializer : BaseLogisticLineGroupInitializer
    {
        private readonly ILogisticLineGroupGenerator _logisticLineGroupGenerator;

        public DematSoftLogisticLineGroupInitializer(ILogisticLineGroupGenerator logisticLineGroupGenerator)
        {
            _logisticLineGroupGenerator = logisticLineGroupGenerator;
        }

        public override IEnumerable<ILogisticLineGroup> Initialize(IDictionary<ILineGroupHeader, IEnumerable<Article>> selectedLineGroups)
        {
            var shippingMethodEvaluation = Context.IGotShippingMethodEvaluation.ShippingMethodEvaluation;

            var output = new List<ILogisticLineGroup>();

            foreach (var lineGroup in selectedLineGroups.Keys)
            {
                var logisticLineGroup = _logisticLineGroupGenerator.Generate(lineGroup);

                logisticLineGroup.OrderType = OrderTypeEnum.DematSoft;
                logisticLineGroup.Informations.OrderType = (int)OrderInfoOrderTypeEnum.DematSoft;

                logisticLineGroup.Articles = new List<Article>(selectedLineGroups[lineGroup]);

                Helper.CopyShippingMethodEvaluation(lineGroup, logisticLineGroup, shippingMethodEvaluation);

                output.Add(logisticLineGroup);
            }

            return output;
        }
    }
}
