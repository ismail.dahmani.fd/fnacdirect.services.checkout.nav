namespace FnacDirect.OrderPipe.Base.DAL
{
    public interface IBillingAddressCommerceDAL
    {
        void AddBillingAddress(int orderId, int accountId, string alias, string company, string vatNumber, string firstName, string lastName, string addressLine1, string addressLine2, string addressLine3, string addressLine4, string zipCode, string city, string state, string countryId, string tel, string cellphone, string fax, int orderBillingAddressStatusId, string taxIdNumber, int nationalityCountryId, int legalStatus, int gender, string siretNumber, int? docCountry = null, int? docType = null);
    }
}
