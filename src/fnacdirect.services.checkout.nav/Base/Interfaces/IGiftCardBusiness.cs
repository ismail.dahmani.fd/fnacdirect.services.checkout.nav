using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.OrderPipe.Base.Model.Facets;
using System.Collections.Generic;

namespace FnacDirect.OrderPipe.Base.Business
{
    public interface IGiftCardBusiness
    {
        bool AddGiftCard(IOF iOF, string cardNumber, string pin);

        bool RemoveGiftCard(IGotBillingInformations gotBillingInformations, string number);

        bool ModifyGiftCard(IGotBillingInformations gotBillingInformations, string p, decimal val);

        bool TransformGiftCard(IGotUserContextInformations gotUserContextInformations, IGotContextInformations gotContextInformations, GiftCardBillingMethod m, ref VirtualGiftCheckBillingMethod vgcbm);

        bool GreaterThanMaximumAllowedAmount(IOF iOF, string cardNumber, decimal newAmount);
        decimal? GetUsabledAmount(IOF iOF, decimal remainingAmount, decimal actualChoosenAmount);

        List<GiftCardTypeValue> GetGiftCards();
    }
}
