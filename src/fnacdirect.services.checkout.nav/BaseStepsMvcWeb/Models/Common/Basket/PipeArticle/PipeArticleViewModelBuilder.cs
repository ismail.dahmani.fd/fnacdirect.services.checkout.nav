using Common.Logging;
using FnacDirect.Front.WebBusiness;
using FnacDirect.Membership.Model.Constants;
using FnacDirect.OrderPipe.Base.Business;
using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.OrderPipe.Base.Proxy.Membership;
using FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Basket;
using FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Basket.Children;
using FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Common.Basket;
using FnacDirect.OrderPipe.BaseMvc.Web.Services.Article.Availability;
using FnacDirect.Technical.Framework.CoreServices;
using FnacDirect.Technical.Framework.Web.Mvc;
using FnacDirect.Technical.Framework.Web.UrlMgr;
using System;
using System.Collections.Generic;
using System.Linq;
using DTO = FnacDirect.Contracts.Online.Model;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Common.Basket.PipeArticle
{
    public class PipeArticleViewModelBuilder : IPipeArticleViewModelBuilder
    {
        private readonly IBasketItemViewModelBuilder _basketItemViewModelBuilder;
        private readonly IPriceViewModelBuilder _priceViewModelBuilder;
        private readonly IApplicationContext _applicationContext;
        private readonly ICompositeRichBasketItemChildrenViewModelBuilder _compositeRichBasketItemChildrenViewModelBuilder;
        private readonly IArticleSubTitleService _articleSubTitleService;
        private readonly IArticleAvailabilityService _articleAvailabilityService;
        private readonly IAdherentCardArticleService _adherentCardArticleService;
        private readonly ILog _logger;

        private const int DefaultQuantityMax = 999;
        private readonly int _donationTypeId;

        private readonly int _tryCardPrid;
        private readonly List<int> _fnacPlusCardTypes;
        private readonly int _fnacPlusTryCardType;

        public PipeArticleViewModelBuilder(IBasketItemViewModelBuilder basketItemViewModelBuilder,
            IArticleSubTitleService articleSubTitleService,
            IApplicationContext applicationContext,
            IPriceViewModelBuilder priceViewModelBuilder,
            IMembershipConfigurationService membershipConfigurationService,
            ICompositeRichBasketItemChildrenViewModelBuilder compositeRichBasketItemChildrenViewModelBuilder,
            IArticleAvailabilityService articleAvailabilityService,
            IAdherentCardArticleService adherentCardArticleService,
            ILog logger)
        {
            _basketItemViewModelBuilder = basketItemViewModelBuilder;
            _articleSubTitleService = articleSubTitleService;
            _applicationContext = applicationContext;
            _priceViewModelBuilder = priceViewModelBuilder;
            _compositeRichBasketItemChildrenViewModelBuilder = compositeRichBasketItemChildrenViewModelBuilder;
            _articleAvailabilityService = articleAvailabilityService;
            _adherentCardArticleService = adherentCardArticleService;
            _logger = logger;

            _tryCardPrid = membershipConfigurationService.GetTryCardPrid();
            _fnacPlusCardTypes = membershipConfigurationService.FnacPlusCardTypes;
            _fnacPlusTryCardType = membershipConfigurationService.FnacPlusTryCardType;

            _donationTypeId = int.Parse(_applicationContext.GetAppSetting("DonationTypeId"));
        }

        public virtual PipeArticleViewModel Build(BaseArticle baseArticle)
        {
            if (!CheckModel(baseArticle))
                return null;

            var viewModel = BuildViewModel(baseArticle);
            SetQuantity(viewModel);
            SetChristmas(viewModel);
            SetAvailability(viewModel, baseArticle);

            if (IsSaleable(baseArticle))
            {
                SetFieldsForSaleableArticle(viewModel, baseArticle);
                SetFieldsForMarketplaceArticle(viewModel, baseArticle);

                SetDonation(viewModel, baseArticle);
                SetProductUrl(viewModel, baseArticle);
                SetSubtitle(viewModel, baseArticle);

                SetFieldsForAdhesionCard(viewModel, baseArticle);
                SetChildrensForStandardArticle(viewModel, baseArticle);
            }

            if (viewModel.IsAvailable)
            {
                SetPrice(viewModel, baseArticle);
            }

            return viewModel;
        }

        #region privates

        private bool CheckModel(BaseArticle baseArticle) => (baseArticle is StandardArticle standardArticle && !standardArticle.Hidden) || baseArticle is MarketPlaceArticle;

        private bool IsSaleable(BaseArticle baseArticle) => baseArticle.Type == ArticleType.Saleable;

        private PipeArticleViewModel BuildViewModel(BaseArticle baseArticle)
        {
            BasketItemWithPictureViewModel basketItem = null;
            if(baseArticle is StandardArticle standardArticle)
            {
                basketItem = _basketItemViewModelBuilder.BuildItemStandardArticleWithPicture(standardArticle, new List<Promotion>());
            }
            else if(baseArticle is MarketPlaceArticle marketPlaceArticle)
            {
                basketItem = _basketItemViewModelBuilder.BuildItemMpArticleWithPicture(marketPlaceArticle);
            }
            return new PipeArticleViewModel(baseArticle, basketItem);
        }

        private void SetQuantity(PipeArticleViewModel viewModel)
        {
            viewModel.Quantity.DefaultMax = DefaultQuantityMax;
            viewModel.Quantity.Max = DefaultQuantityMax;
            viewModel.Quantity.Count = 1;
        }

        private void SetAvailability(PipeArticleViewModel viewModel, BaseArticle baseArticle)
        {
            if (baseArticle is StandardArticle standardArticle)
            {
                viewModel.IsAvailable = standardArticle.Availability != StockAvailability.None;

                if (!viewModel.IsAvailable)
                {
                    viewModel.Availability = new AvailabilityViewModel()
                    {
                        Availability = _applicationContext.GetMessage("orderpipe.pop.basket.unavailable.article")
                    };
                    return;
                }
                
                if (!_adherentCardArticleService.IsAdherentCard(viewModel.ProductId)
                    && baseArticle.TypeId != _donationTypeId)
                {
                    viewModel.Availability = _articleAvailabilityService.GetAvailability(standardArticle);
                }
            }
            else if (baseArticle is MarketPlaceArticle marketPlaceArticle)
            {
                viewModel.IsAvailable = marketPlaceArticle.Quantity > 0 && marketPlaceArticle.AvailableQuantity > 0;
                viewModel.AvailabilityFnacId = (int)AvailabilityEnum.Indisponible;

                viewModel.Availability = new AvailabilityViewModel()
                {
                    Availability = viewModel.IsAvailable ? baseArticle.AvailabilityLabel : _applicationContext.GetMessage("orderpipe.pop.basket.unavailable.article")
                };
            }
        }

        private void SetProductUrl(PipeArticleViewModel viewModel, BaseArticle baseArticle)
        {
            if (baseArticle is Service
                || viewModel.IsDonation
                || (baseArticle is StandardArticle && _adherentCardArticleService.IsAdherentCard(viewModel.ProductId)))
            {
                return;
            }

            var w3Article = ArticleFactoryNew.GetW3Article(baseArticle.ToArticleReference(), true, false);
            if (w3Article == null)
            {
                var ex = new Exception();
                ex.Data.Add("Product Id", viewModel.ProductId);
                ex.Data.Add("Seller Name", viewModel.SellerName);
                _logger.Warn("w3Article is null", ex);
                return;
            }
            var urlWriter = w3Article.GetPageUrl();
            viewModel.ProductUrl = !UrlWriter.IsNullOrEmpty(urlWriter) ? urlWriter.ToString() : null;
        }

        private void SetSubtitle(PipeArticleViewModel viewModel, BaseArticle baseArticle)
        {
            FnacDirect.Contracts.Online.Model.Article dtoArticle = null;
            var w3Article = ArticleFactoryNew.GetW3Article(baseArticle.ToArticleReference(), true, false);
            if (w3Article == null)
            {
                var ex = new Exception();
                ex.Data.Add("Product Id", viewModel.ProductId);
                ex.Data.Add("Seller Name", viewModel.SellerName);
                _logger.Warn("w3Article is null", ex);
            }
            else
            {
                dtoArticle = w3Article.DTO;
            }
            viewModel.SubTitle = _articleSubTitleService.BuildSubTitle(baseArticle, dtoArticle);
        }

        private void SetFieldsForSaleableArticle(PipeArticleViewModel viewModel, BaseArticle baseArticle)
        {
            if (IsSaleable(baseArticle))
            {
                return;
            }

            viewModel.CanUpdate = true;
            viewModel.CanDelete = true;
            viewModel.ShowIfArticleIsFreePruduct = true;
        }

        private void SetPrice(PipeArticleViewModel viewModel, BaseArticle baseArticle)
        {
            viewModel.Price = _priceViewModelBuilder.Build(baseArticle, false);
        }

        private void SetDonation(PipeArticleViewModel viewModel, BaseArticle baseArticle)
        {
            viewModel.IsDonation = _donationTypeId == baseArticle.TypeId.GetValueOrDefault();
        }

        private void SetFieldsForAdhesionCard(PipeArticleViewModel viewModel, BaseArticle baseArticle)
        {
            if(!(baseArticle is StandardArticle)
                || !_adherentCardArticleService.IsAdherentCard(viewModel.ProductId))
            {
                return;
            }

            viewModel.IsMembershipRenewCard = baseArticle.TypeId.GetValueOrDefault() == (int)CategoryCardType.Renew
                || baseArticle.TypeId.GetValueOrDefault() == (int)CategoryCardType.RenouvellementFnacPlus
                || baseArticle.TypeId.GetValueOrDefault() == (int)CategoryCardType.ConversionFnacPlus;
            viewModel.IsMembershipRecruitCard = baseArticle.TypeId.GetValueOrDefault() == (int)CategoryCardType.Recruit
                || baseArticle.TypeId.GetValueOrDefault() == (int)CategoryCardType.SouscriptionFnacPlusTrial
                || baseArticle.TypeId.GetValueOrDefault() == (int)CategoryCardType.SouscriptionFnacPlusTrialDecouverte;
            viewModel.IsTryCard = baseArticle.ProductID == _tryCardPrid;
            viewModel.IsFnacPlusTryCard = baseArticle.TypeId.GetValueOrDefault() == _fnacPlusTryCardType;
            viewModel.IsFnacPlusCard = _fnacPlusCardTypes.Contains(baseArticle.TypeId.GetValueOrDefault());
        }

        private void SetChristmas(PipeArticleViewModel viewModel)
        {
            viewModel.CanBeDelivredBeforChristmas = false;
            viewModel.CanBeDelivredBeforChristmasPicto = string.Empty;
            viewModel.CanBeDelivredBeforChristmasWording = string.Empty;
        }

        private void SetChildrensForStandardArticle(PipeArticleViewModel viewModel, BaseArticle baseArticle)
        {
            if (!(baseArticle is StandardArticle standardArticle))
            {
                return;
            }

            var childrens = _compositeRichBasketItemChildrenViewModelBuilder.Build(standardArticle, new List<Promotion>(), false).ToArray();
            var hasAnyCrossChildItem = childrens.Any(c => c is CrossSellRichItemChildrenViewModel);
            viewModel.HasCrossSellChildItems = hasAnyCrossChildItem;
            viewModel.WithChildren(childrens);
        }

        private void SetFieldsForMarketplaceArticle(PipeArticleViewModel viewModel, BaseArticle baseArticle)
        {
            if (!(baseArticle is MarketPlaceArticle mpArticle))
            {
                return;
            }

            viewModel.IsPureMarketPlace = mpArticle.Offer.ArticleReference.Catalog == DTO.ArticleCatalog.MarketPlace;
            viewModel.IsNumerical = false;

            viewModel.OfferId = mpArticle.Offer.Reference;
            viewModel.Comments = mpArticle.Offer.Comments;
            viewModel.Status = mpArticle.Offer.ProductSubStatusLabel;
            viewModel.TotalOfferCount = mpArticle.AvailableQuantity;
        }
        
        #endregion
    }
}
