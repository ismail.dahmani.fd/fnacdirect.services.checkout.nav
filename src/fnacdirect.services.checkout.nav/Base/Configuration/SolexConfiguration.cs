using FnacDirect.Solex.Shipping.Client.Contract;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace FnacDirect.OrderPipe.Base.Business.Configuration
{
    public class SolexConfiguration
    {
        public string Channel { get; set; }
        public string Market { get; set; }
        public int SiteId { get; set; }
        public int StartDateValue { get; set; }
        public int EndDateValue { get; set; }
        public string StoresUri { get; set; }
        public IDictionary<string, string> AvailabilityCodeToDelay { get; set; } = new Dictionary<string, string>();

        public SolexAppointmentDeliveryConfiguration AppointmentDelivery { get; set; } = new SolexAppointmentDeliveryConfiguration();
        public SolexDefaultPostalAddressConfiguration DefaultPostalAddress { get; set; } = new SolexDefaultPostalAddressConfiguration();
        public SolexShippingAndDeliveryDatesConfiguration ShippingAndDeliveryDates { get; set; } = new SolexShippingAndDeliveryDatesConfiguration();
        public SolexShippingAndStockLocationsConfiguration ShippingAndStockLocations { get; set; } = new SolexShippingAndStockLocationsConfiguration();
        public SolexDatabasesConfiguration Databases { get; set; } = new SolexDatabasesConfiguration();
        public SolexShippingNetworksConfiguration ShippingNetworks { get; set; } = new SolexShippingNetworksConfiguration();
        public Dictionary<string, string> ShippingMethodsAccountFilters { get; set; } = new Dictionary<string, string>();
        public Dictionary<int, Regex> ShippingMethodsAccountFiltersAsRegex { get; set; } = new Dictionary<int, Regex>();
        public List<int> ShippingMethodNotToBeLogged { get; set; } = new List<int>();
        public List<int> EvaluateProductIncludeSourcesWhitelist { get; set; } = new List<int>();

        public bool IsLoggingEvaluateShoppingCartQuery { get; set; }
        public bool IsLoggingEvaluateProductQuery { get; set; }
        public bool IsLoggingEvaluateProductsQuery { get; set; }
    }

    public class SolexDefaultPostalAddressConfiguration
    {
        public string AddressLine { get; set; }
        public string ZipCode { get; set; }
        public string City { get; set; }
        public string Country { get; set; }
    }

    public class SolexAppointmentDeliveryConfiguration
    {
        public string EndpointAddress { get; set; }
    }

    public class SolexShippingAndDeliveryDatesConfiguration
    {
        public string Culture { get; set; }
        public string CountryCode { get; set; }
        public string AppScope { get; set; }
        public int SACountryId { get; set; }
        public List<int> ShippingMethodNotToBeLogged { get; set; } = new List<int>();
    }

    public class SolexShippingNetworksConfiguration
    {
        public int PostalNetworkId { get; set; }
        public int RelayNetworkId { get; set; }
        public int ShopNetworkId { get; set; }
    }

    public class SolexDatabasesConfiguration
    {
        public string SqlCatalog { get; set; }
        public string SqlCommerce { get; set; }
        public string SqlCustomer { get; set; }
        public string SqlMetaCatalog { get; set; }
        public string SqlOffer { get; set; }
        public string SqlStom { get; set; }
        public string SqlAdminSolex { get; set; }
    }
    public class SolexShippingAndStockLocationsConfiguration
    {
        public List<Location> Locations { get; set; }
        public string ComputeLocationMethod { get; set; }
        public IDictionary<string, int> WarehousesBySubFamily { get; set; }
            = new Dictionary<string, int>();
    }
}
