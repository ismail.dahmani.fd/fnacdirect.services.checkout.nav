﻿namespace FnacDirect.OrderPipe.Base.Model
{
    public interface IBundle
    {
        ArticleList BundleComponents { get; set; }
    } 
}
