﻿namespace FnacDirect.OrderPipe.Base.Model
{
    /// <summary>
    /// Ensemble d'informations liées à l'éligibilité d'un client donné (contrat).
    /// </summary>
    public class DeferredPaymentEligibility
    {
        /// <summary>
        /// Obtient ou définit l'identifiant du compte client.
        /// </summary>
        public int AccountId { get; set; }

        /// <summary>
        /// Obtient ou définit si le paiement différé est activé pour le client.
        /// </summary>
        public bool IsEligible { get; set; }

        /// <summary>
        /// Obtient ou définit le type de l'autorisation.
        /// </summary>
        public DeferredPaymentAuthorizationType AuthorizationType { get; set; }

        /// <summary>
        /// Obtient ou définit la raison pour laquelle le client n'est pas éligible.
        /// </summary>
        public DeferredPaymentInegibilityReason InegibilityReason { get; set; }

        /// <summary>
        /// Obtient ou définit le cumul des commandes de la journée du client.
        /// </summary>
        public decimal CumulativeOrdersAmount { get; set; }

        /// <summary>
        /// Obtient ou définit la somme maximum quotidienne moins le cumul de la journée.
        /// </summary>
        public decimal CumulativeOrdersAmountDelta { get; set; }

        /// <summary>
        /// Obtient ou définit le délai du paiement différé en nombre de jours.
        /// </summary>
        public int PaymentDelay { get; set; }

        /// <summary>
        /// Obtient ou définit le N° de paiement différé du client.
        /// </summary>
        public string DeferredAccountNumber { get; set; }
    }
}
