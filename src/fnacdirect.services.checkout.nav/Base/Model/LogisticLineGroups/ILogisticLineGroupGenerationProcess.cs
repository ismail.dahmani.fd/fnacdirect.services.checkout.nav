using FnacDirect.OrderPipe.Base.Business.LogisticLineGroups.Internals.Rules;
using FnacDirect.OrderPipe.Base.LineGroups;
using FnacDirect.OrderPipe.Base.Model.Facets;
using System.Collections.Generic;

namespace FnacDirect.OrderPipe.Base.Business.LogisticLineGroups
{
    public interface ILogisticLineGroupGenerationProcess
    {
        IEnumerable<ILogisticLineGroup> Apply(MultiFacet<IGotLineGroups, IGotBillingInformations, IGotShippingMethodEvaluation> multiFacet);

        IEnumerable<ILogisticLineGroup> SimulateInitialize(MultiFacet<IGotLineGroups, IGotBillingInformations, IGotShippingMethodEvaluation> multiFacet);
    }
}
