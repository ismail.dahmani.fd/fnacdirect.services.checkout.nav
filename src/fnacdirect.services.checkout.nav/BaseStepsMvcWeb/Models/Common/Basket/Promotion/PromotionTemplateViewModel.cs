﻿namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Basket
{
    public class PromotionTemplateViewModel
    {
        public string CssClass { get; set; }

        public string Label { get; set; }

        public bool IsLongLabel { get; set; }

        public bool IsFirst { get; set; }

        public string Comment { get; set; }

        public string ShoppingCartText { get; set; }

        public bool IsHidden { get; set; }
    }
}
