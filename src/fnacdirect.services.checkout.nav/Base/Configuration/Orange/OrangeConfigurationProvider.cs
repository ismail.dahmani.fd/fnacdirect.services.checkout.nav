using FnacDirect.Technical.Framework.Configuration;

namespace FnacDirect.OrderPipe.Base.Business.Configuration
{
    public class OrangeConfigurationProvider : IOrangeConfigurationProvider
    {
        private readonly OrangeConfiguration _orangeConfiguration;

        public OrangeConfigurationProvider(IConfigurationProvider configurationProvider)
        {
            _orangeConfiguration = configurationProvider.GetAs<OrangeConfiguration>("orange");
        }

        public OrangeConfiguration Get()
        {
            return _orangeConfiguration;
        }
    }
}
