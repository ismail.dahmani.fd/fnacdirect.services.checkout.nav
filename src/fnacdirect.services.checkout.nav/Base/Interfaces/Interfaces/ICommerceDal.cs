using FnacDirect.OrderPipe.Base.BaseModel.Model;
using FnacDirect.OrderPipe.Base.Model;

namespace FnacDirect.OrderPipe.Base.DAL
{
    public interface ICommerceDal
    {
        AccountScoring SearchOrderAccountScoring(int accountID);
        WrapMethodList GetListOfAvailableWrapMethods(int logisticType);
    }
}
