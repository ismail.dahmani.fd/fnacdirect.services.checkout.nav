﻿using System.Collections.Generic;

namespace FnacDirect.OrderPipe.Core.Services
{
    public interface IExecutionContextKeysProvider
    {
        IDictionary<string, string> Get();
    }
}
