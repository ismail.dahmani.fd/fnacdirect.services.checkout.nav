using FnacDirect.OrderPipe.Base.Model.Facets;
using FnacDirect.OrderPipe.Base.Model.UserInformations;

namespace FnacDirect.OrderPipe.Base.Business.Payment
{
    public interface IFidelityPointsOccazPaymentService
    {
        bool IsAllowed(IGotBillingInformations iGotBillingInformations, IGotLineGroups iGotLineGroups, IUserContextInformations userContext, bool disableClickAndCollectCheck = false, bool disableECCVCheck = false);
    }
}
