using X=System.Xml;

namespace FnacDirect.OrderPipe
{
    public class SetErrorForwardStep : Step
    {
        private string _stepName;

        protected override void Init()
        {
            base.Init();
            _stepName = Parameters["forward.step"];
        }

        public override bool Run(OPContext context, PipeResult res, bool force)
        {
            context.ErrorStep = _stepName;
            res.LastStepReturn = StepReturn.Next;

            return false;
        }
    }
}
