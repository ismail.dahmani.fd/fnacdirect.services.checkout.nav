using System;

namespace FnacDirect.OrderPipe.Base.Business.Shipping.Shop
{
    public class ShippingShipToStoreAvailability : ShipToStoreAvailability
    {
        public DateTime? DeliveryDayMin { get; set; }
        public DateTime? DeliveryDayMax { get; set; }        

        public ShippingShipToStoreAvailability(string referenceGu, DateTime? deliveryDayMin, DateTime? deliveryDayMax, int shippingMethodId)
            : base(referenceGu)
        {
            DeliveryDayMin = deliveryDayMin;
            DeliveryDayMax = deliveryDayMax;
            ShippingMethodId = shippingMethodId;
        }
    }
}
