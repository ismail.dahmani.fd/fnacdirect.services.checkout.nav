namespace FnacDirect.OrderPipe
{
    using System;
    using FnacDirect.Contracts.Online.Model;
    using FnacDirect.Technical.Framework.CoreServices;
    using FnacDirect.Technical.Framework.CoreServices.Localization;
    using System.Globalization;

    public class MessageHelper
    {
        public static string Get(SiteContext context, string id, params object[] prms)
        {
            if (context == null)
                throw new ArgumentNullException("context");

            var loc = Service<IUIResourceService>.Instance;

            // CultureInfo.InvariantCulture should be replaced one day
            if (prms != null && prms.Length != 0)
                return string.Format(CultureInfo.InvariantCulture, loc.GetUIResourceValue(id, context.Culture), prms);
            else
                return loc.GetUIResourceValue(id, context.Culture);
        }
    }
}
