﻿namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop
{
    public interface IRootViewModel
    {
        string PageName { get; }
    }
}
