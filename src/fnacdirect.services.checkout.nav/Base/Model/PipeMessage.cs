﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FnacDirect.OrderPipe
{
    public class PipeMessage
    {
        public string StepName { get; private set; }
        public string Category { get; private set; }
        public string RessourceId { get; private set; }
        public IReadOnlyCollection<string> Parameters { get; private set; }
        public PipeMessageLevel Level { get; private set; }
        public PipeMessageBehavior Behavior { get; set; }

        public PipeMessage(string stepName, string category, string ressourceId, PipeMessageLevel level, params object[] parameters)
        {
            StepName = stepName;
            Behavior = PipeMessageBehavior.AutoClear;
            Category = category;
            RessourceId = ressourceId;
            Level = level;
            var result = new List<string>();
            foreach (var value in parameters)
            {
                if (value is List<string> && ((List<string>)value).Count > 0)
                    result.AddRange((List<string>)value);
                if (value is string)
                    result.Add(value.ToString());
            }

            Parameters = result;
        }

        public override int GetHashCode()
        {
            return ToString().GetHashCode();
        }

        public override bool Equals(object obj)
        {
            return GetHashCode().Equals(obj.GetHashCode());
        }

        public override string ToString()
        {
            return string.Format("{0}_{1}_{2}_{3}_{4}", Level, StepName, Category, RessourceId, Parameters.GetHashCode());
        }
    }

    public enum PipeMessageLevel
    {
        Warning,
        Error,
        Info
    }
}
