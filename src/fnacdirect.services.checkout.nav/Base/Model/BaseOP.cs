using FnacDirect.Context;
using FnacDirect.OrderPipe.Base.DAL;
using FnacDirect.Technical.Framework.ServiceLocation;

namespace FnacDirect.OrderPipe.Base.Business
{
    public class BaseOP : OP
    {
        private readonly BillingMethodsManager _billingMethodsManager;

        public BaseOP()
        {
            _billingMethodsManager = new BillingMethodsManager(ServiceLocator.Current.GetInstance<IMaintenanceConfigurationService>(),
                                                               ServiceLocator.Current.GetInstance<IPaymentMethodDal>(),
                                                               ServiceLocator.Current.GetInstance<IApplicationContextLight>());
        }

        public BaseOP(BillingMethodsManager billingMethodsManager)
        {
            _billingMethodsManager = billingMethodsManager;
        }

        public BillingMethodsManager BillingMethodsManager
        {
            get
            {
                return _billingMethodsManager;
            }
        }
    }
}

