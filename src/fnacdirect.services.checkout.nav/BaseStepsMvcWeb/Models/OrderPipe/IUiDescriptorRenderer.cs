﻿using FnacDirect.OrderPipe.Config;
using System.IO;

namespace FnacDirect.OrderPipe.Context
{
    public interface IUiDescriptorRenderer
    {
        string Render(IUiDescriptor uiDescriptor);
    }

    public interface IUiDescriptorRenderer<in TDescriptor> : IUiDescriptorRenderer
        where TDescriptor : IUiDescriptor
    {
    }
}
