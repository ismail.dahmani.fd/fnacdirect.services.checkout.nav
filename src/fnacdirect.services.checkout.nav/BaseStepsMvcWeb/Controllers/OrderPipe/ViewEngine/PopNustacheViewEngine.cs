﻿using FnacDirect.Technical.Framework.Web.Mvc.Nustache;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Controllers.Pop
{
    public class PopNustacheViewEngine : LocalizationNustacheViewEngine
    {
        public PopNustacheViewEngine()
            : base("OrderPipe/Pop")
        {
        }
    }
}
