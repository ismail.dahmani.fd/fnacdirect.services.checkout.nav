using System;
using System.Collections.Generic;
using System.Text;
using FnacDirect.Customer.Model;

namespace FnacDirect.Quote.Model
{
    public class CompanyInfoCollection : List<CompanyInfo>
    {
          private int _recordsCount;

        public CompanyInfo FindByReference(int ID)
        {

            CompanyInfo result = null;
            foreach (var company in this)
            {
                if (company.ID == ID)
                {
                    result = company;

                }
            }
            return result;
        }

        public int RecordsCount
        {
            get
            {
                return _recordsCount;
            }
            set
            {
                _recordsCount = value;

                if (_recordsCount > 0 && pageSize > 0)
                {
                    PageAnchorList =  new List<object>();
                    var pageNumber = ((_recordsCount - (_recordsCount % pageSize)) / pageSize) + (_recordsCount % pageSize == 0 ? 0 : 1);

                    for (var i = 1; i <= pageNumber; i++)
                    {
                        PageAnchorList.Add((object)new KeyValuePair<int, int>(i,i));
                    }
                }


            }
        }


        public int PageSize
        {
            get { return pageSize; }
            set { pageSize = value; }
        }

        private int pageSize;

        private List<object> pageAnchorList;

        public CompanyInfoCollection(int i)
        {
            PageSize = i;
        }

        public List<object> PageAnchorList
        {
            get { return  pageAnchorList; }
            set { pageAnchorList =  value; }
        }
    }
}