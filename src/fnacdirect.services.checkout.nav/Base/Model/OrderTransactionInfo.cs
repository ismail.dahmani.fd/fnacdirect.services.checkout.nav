using FnacDirect.Ogone.Model;
using FnacDirect.OrderPipe.FDV.Model;


namespace FnacDirect.OrderPipe.Base.Model
{
    /// <summary>
    /// Need to be used for multi logistic order
    /// </summary>
    public class OrderTransactionInfo
    {

        public OrderTransaction OrderTransaction { get; set; }
        public OrderTransactionIngenico OrderTransactionIngenico { get; set; }
        public OrderTransactionOperation DebitCancellationOrderTransactionOperation { get; set; }
        public OrderTransactionSiebel OrderTransactionSiebel { get; set; }
        public OrderTransactionGiftCard OrderTransactionGiftCard { get; set; }
        public bool IsSelectedForPsp { get; set; }
        public bool IsMainBillingMethod { get; set; }
    }
}
