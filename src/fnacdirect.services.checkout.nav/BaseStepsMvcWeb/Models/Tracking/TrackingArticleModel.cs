namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Tracking
{
    public class TrackingArticleModel
    {
        public int Prid { get; set; }
        public string Offer { get; set; }
        public int Quantity { get; set; }
    }
}
