﻿using System;
namespace FnacDirect.OrderPipe
{
    public interface IOFManager
    {
        Type Type { get; set; }
        IOFContainer OFContainer { get; }
        IOFSerializer OFSerializer { get; }

        IOF BuildOF(OPContext context, string sid);
        IOF Read(OPContext context, string sid);
        void Write(OPContext context, IOF of);
        void Initialize(IOP pipe);

    }
}
