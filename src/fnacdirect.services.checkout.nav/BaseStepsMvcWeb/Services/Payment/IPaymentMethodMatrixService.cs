﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FnacDirect.OrderPipe.Base.Business.Payment
{
    public interface IPaymentMethodMatrixService
    {
        int GetRemainingMethodMask(int currentMethodsMask, int allowedMethodsMask, string configuration);
    }
}
