using FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Common.Payment;
using FnacDirect.Technical.Framework.Web.Mvc;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Pop.Models.SelfCheckout
{
    public class SelfCheckoutSummaryViewModel
    {
        public int ProductsCount { get; set; }
        public string TotalGlobal { get; set; }
        public PriceDetailsViewModel TotalGlobalModel { get; set; }
        public CustomerCreditCardBaseViewModel CreditCard { get; set; }
        public string TermsAndConditions { get; set; }
    }
}
