using FnacDirect.OrderPipe.Base.BaseModel;
using FnacDirect.OrderPipe.Base.LineGroups;
using FnacDirect.Technical.Framework.Velocity;
using System.Linq;

namespace FnacDirect.OrderPipe.Base.Business.LogisticLineGroups.Internals.Initializers
{
    public class LogisticLineGroupInitializerHelper : ILogisticLineGroupInitializerHelper
    {
        private readonly ICloneService _cloneService;

        public LogisticLineGroupInitializerHelper(ICloneService cloneService,
                                                  ISwitchProvider switchProvider)
        {
            _cloneService = cloneService;
        }

        public T Clone<T>(T item)
        {
            return CloneUtil.DeepCopy(item);
        }

        /// <summary>
        /// En attendant la suppression de shippingmethodevaluation ...
        /// </summary>
        public void CopyShippingMethodEvaluation(ILineGroupHeader source, LogisticLineGroup destination, ShippingMethodEvaluation shippingMethodEvaluation)
        {
            if (source is Model.LineGroups.PopLineGroup)
            {
                var maybeShippingMethodEvaluationGroup = shippingMethodEvaluation.GetShippingMethodEvaluationGroup(source.Seller.SellerId);

                if (maybeShippingMethodEvaluationGroup.HasValue)
                {
                    var shippingMethodEvaluationGroup = maybeShippingMethodEvaluationGroup.Value;

                    if (shippingMethodEvaluationGroup.SelectedShippingMethodEvaluationType
                         != ShippingMethodEvaluationType.Unknown)
                    {
                        var shippingMethodEvaluationItem = shippingMethodEvaluationGroup.SelectedShippingMethodEvaluation;

                        var shippingAddress = shippingMethodEvaluationItem.ShippingAddress;

                        if (shippingAddress != null)
                        {
                            destination.ShippingAddress = _cloneService.Clone(shippingAddress);
                        }

                        var selectedChoice = shippingMethodEvaluationItem.Choices.FirstOrDefault(choice => choice.MainShippingMethodId == shippingMethodEvaluationItem.SelectedShippingMethodId);

                        if (selectedChoice != null)
                        {
                            destination.SelectedShippingChoice = selectedChoice.Copy();
                        }
                    }
                }
            }
            else if (source is Model.LineGroup lineGroup)
            {
                var shippingAddress = lineGroup.ShippingAddress;
                if (shippingAddress != null)
                {
                    destination.ShippingAddress = _cloneService.Clone(shippingAddress);
                }
                if (lineGroup.ShippingMethodChoice != null)
                {
                    destination.ShippingMethodChoice = _cloneService.Clone(lineGroup.ShippingMethodChoice);
                }
            }
        }
    }
}
