namespace FnacDirect.OrderPipe.Base.Model
{
    public enum LegalStatus
    {
        Undefined = 0,
        Particular = 1,
        Business = 2
    }
}
