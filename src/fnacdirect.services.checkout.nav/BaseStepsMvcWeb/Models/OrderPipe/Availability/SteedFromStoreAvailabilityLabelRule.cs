using FnacDirect.OrderPipe.Core.Services;
using FnacDirect.Technical.Framework.Web.Mvc;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;

namespace FnacDirect.OrderPipe.Base.Business.Availability
{
    public class SteedFromStoreAvailabilityLabelRule : DefaultAvailabilityLabelRule, IAvailabilityLabelRule
    {
        private readonly TimeSpan _x;
        private readonly TimeSpan _y;
        private readonly TimeSpan _z;

        public SteedFromStoreAvailabilityLabelRule(IApplicationContext applicationContext, ISwitchProvider switchProvider, DateTimeFormatInfo dateTimeFormatInfo, string customRangeDatesFormat, IPipeParametersProvider pipeParametersProvider) : base(applicationContext, switchProvider, pipeParametersProvider, dateTimeFormatInfo, customRangeDatesFormat)
        {
            _x = TimeSpan.FromMinutes(pipeParametersProvider.Get<int>("steedfromstore.availabilitylabel.x"));
            _y = TimeSpan.FromMinutes(pipeParametersProvider.Get<int>("steedfromstore.availabilitylabel.y"));
            _z = TimeSpan.FromMinutes(pipeParametersProvider.Get<int>("steedfromstore.availabilitylabel.z"));
        }

        public override EstimatedDeliveryDate ComputeSingleEstimatedDeliveryDate(PopAvailabilityProcessorInput availability)
        {
            if (availability.Article.IsPreOrder) return base.ComputeSingleEstimatedDeliveryDate(availability);

            var today = DateTime.Today;
            var now = DateTime.Now;

            var deliveryDate = availability.DateMax.GetValueOrDefault();

            if (deliveryDate.Date == today)
            {
                var y = today.Add(_y);

                if (now > y)
                {
                    return new EstimatedDeliveryDate(_applicationContext.GetMessage("orderpipe.steedfromstore.availabilitylabel.today.afterz", _x.Hours),
                                                     long.MinValue,
                                                     availability);
                }
                else
                {
                    var z = today.Add(_z);
                    var time = z.ToString("HH\\H", _dateTimeFormatInfo);

                    if (z.Minute != 0)
                    {
                        time += z.Minute.ToString();
                    }

                    return new EstimatedDeliveryDate(_applicationContext.GetMessage("orderpipe.steedfromstore.availabilitylabel.today.beforez", time),
                                                    0,
                                                    availability);
                }
            }
            else if (deliveryDate.Date > today)
            {
                var day = deliveryDate.Date.ToString("dd MMMM", _dateTimeFormatInfo);
                return new EstimatedDeliveryDate(_applicationContext.GetMessage("orderpipe.steedfromstore.availabilitylabel.nottoday", day),
                                                   long.MaxValue,
                                                   availability);
            }

            return null;
        }

        public override string Get(IEnumerable<PopAvailabilityProcessorInput> availabilities, bool homogenous, string prefix, bool? hasManyDeliveryDates)
        {
            var today = DateTime.Today;
            var now = DateTime.Now;

            var delivery = availabilities.Where(a => a.DateMax.HasValue && !a.Article.IsPreOrder)
                                         .OrderBy(a => a.DateMax.Value)
                                         .Select(a => new { shippingDate = a.DateMax.Value, shippingMethodId = a.ShippingMethodId })
                                         .FirstOrDefault();

            if (delivery != null)
            {
                if (delivery.shippingDate.Date == today || delivery.shippingDate == default(DateTime))
                {
                    var y = today.Add(_y);

                    if (now > y)
                    {
                        return _applicationContext.GetMessage(hasManyDeliveryDates.GetValueOrDefault() ? "orderpipe.steedfromstore.availabilitylabel.today.afterz.many.dle" : "orderpipe.steedfromstore.availabilitylabel.today.afterz", _x.Hours);
                    }
                    else
                    {
                        var z = today.Add(_z);
                        var time = z.ToString("HH\\H", _dateTimeFormatInfo);

                        if (z.Minute != 0)
                        {
                            time += z.Minute.ToString();
                        }

                        return _applicationContext.GetMessage("orderpipe.steedfromstore.availabilitylabel.today.beforez", time);
                    }
                }
                else if (delivery.shippingDate.Date > today)
                {
                    var day = delivery.shippingDate.Date.ToString("dd MMMM", _dateTimeFormatInfo);
                    return _applicationContext.GetMessage("orderpipe.steedfromstore.availabilitylabel.nottoday", day);
                }

            }
            return string.Empty;
        }
    }
}
