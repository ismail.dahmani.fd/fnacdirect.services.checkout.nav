﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FnacDirect.OrderPipe.Base.Model
{
 public class ExperianData : GroupData
    {
        public string Payload { get; set; }
        public string Headers { get; set; }
    }
}
