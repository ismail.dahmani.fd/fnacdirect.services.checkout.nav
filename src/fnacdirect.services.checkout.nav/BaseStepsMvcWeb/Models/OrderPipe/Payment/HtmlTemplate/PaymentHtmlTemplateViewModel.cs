using FnacDirect.Customer.Model;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Payment
{
    public class PaymentHtmlTemplateViewModel
    {
        public bool Is3ds { get; set; }

        public decimal DebitAmount { get; set; }

        public decimal RealAmount { get; set; }

        public RootViewModel Root { get; set; }

        public bool OnlyShippedProducts { get; set; }

        public DebitType? DebitType { get; set; }
    }
}
