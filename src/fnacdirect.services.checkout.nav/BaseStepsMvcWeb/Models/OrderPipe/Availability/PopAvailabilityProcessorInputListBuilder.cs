using FnacDirect.OrderPipe.Base.Model;
using System;
using System.Collections.Generic;
using System.Linq;

namespace FnacDirect.OrderPipe.Base.Business.Availability
{
    public class PopAvailabilityProcessorInputListBuilder
    {
        // Obligatoire
        private IEnumerable<Article> _articles = new List<Article>();
        private int _deliveryDateShippingMethodId;

        // Filtre
        private IEnumerable<int> _excludedLogisticTypes = new List<int>();
        private Func<BaseArticle, bool> _articleFilter = null;

        // Facultative
        private int? _shippingMethodId = null;
        private Dictionary<BaseArticle, int> _parcelShippingMethodIds = null;

        public PopAvailabilityProcessorInputListBuilder(IEnumerable<Article> articles, int shippingMethodId)
        {
            _articles = articles;
            _deliveryDateShippingMethodId = shippingMethodId;
        }

        public PopAvailabilityProcessorInputListBuilder ExcludeLogisticTypes(IEnumerable<int> excludedLogisticTypes)
        {
            _excludedLogisticTypes = excludedLogisticTypes;
            return this;
        }

        public PopAvailabilityProcessorInputListBuilder FilterArticles(Func<BaseArticle, bool> articleFilterDelegate)
        {
            _articleFilter = articleFilterDelegate;
            return this;
        }
        
        public PopAvailabilityProcessorInputListBuilder WithShippingMethodId()
        {
            _shippingMethodId = _deliveryDateShippingMethodId;
            return this;
        }

        public PopAvailabilityProcessorInputListBuilder WithParcelShippingMethodIds(Dictionary<BaseArticle, int> parcelShippingMethodIds)
        {
            _parcelShippingMethodIds = parcelShippingMethodIds;
            return this;
        }

        public IEnumerable<PopAvailabilityProcessorInput> BuildList()
        {
            return (
                from article in _articles
                    .OfType<BaseArticle>()
                    .Where(a =>
                        !_excludedLogisticTypes.Contains(a.LogType.GetValueOrDefault())
                        && (_articleFilter == null || _articleFilter(a))
                    )
                let delivery = article.DeliveryDates.FirstOrDefault(d => d.ShippingMethodId == _deliveryDateShippingMethodId)
                where delivery != null
                select new PopAvailabilityProcessorInput(
                    article,
                    delivery.DeliveryDateMin,
                    delivery.DeliveryDateMax,
                    shippingMethodId: _shippingMethodId,
                    parcelShippingMethodId: GetParcelShippingMethodId(article)
                )
            ).ToList();
        }

        #region privates

        private int? GetParcelShippingMethodId(BaseArticle article)
        {
            if(_parcelShippingMethodIds == null
                || !_parcelShippingMethodIds.ContainsKey(article))
            {
                return default(int?);
            }

            return _parcelShippingMethodIds[article];
        }

        #endregion
    }
}
