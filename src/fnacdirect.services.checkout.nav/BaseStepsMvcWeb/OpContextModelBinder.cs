﻿using FnacDirect.OrderPipe.CoreRouting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http.Controllers;
using System.Web.Http.ModelBinding;
using System.Web.Routing;

namespace FnacDirect.OrderPipe.CoreMvcWeb.Http
{
    public class OpContextModelBinder : IModelBinder
    {
        public bool BindModel(HttpActionContext actionContext, ModelBindingContext bindingContext)
        {
            var executePipeService = System.Web.Mvc.DependencyResolverExtensions.GetService<IExecutePipeService>(System.Web.Mvc.DependencyResolver.Current);

            var pipeExecutionResult = executePipeService.ExecuteOrderPipe(RouteTable.Routes.GetPipeName());

            var opContext = pipeExecutionResult.OpContext;

            if (opContext == null)
            {
                return false;
            }

            if (opContext.OF == null)
            {
                return false;
            }

            if(!bindingContext.ModelType.IsAssignableFrom(opContext.OF.GetType()))
            {
                return false;
            }

            bindingContext.Model = opContext.OF;

            return true;
        }
    }
}