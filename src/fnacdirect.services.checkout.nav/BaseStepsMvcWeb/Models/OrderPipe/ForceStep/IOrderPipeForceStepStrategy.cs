﻿using System.Web.Routing;
using FnacDirect.OrderPipe.Config;

namespace FnacDirect.OrderPipe.CoreRouting
{
    public interface IOrderPipeForceStepStrategy
    {
        string GetForceStepFor(IOP pipe, RequestContext requestContext);
    }

    public interface IOrderPipeForceStepStrategy<in TDescriptor> : IOrderPipeForceStepStrategy
        where TDescriptor : IUiPipeDescriptor
    {

    }
}
