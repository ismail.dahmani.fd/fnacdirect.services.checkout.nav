﻿using FnacDirect.Context;
using FnacDirect.OrderPipe.Config;
using FnacDirect.Technical.Framework.Web.UrlMgr;
using System.Web.Mvc;

namespace FnacDirect.OrderPipe.CoreMvcWeb.Routing
{
    public class AuthenticationActionFilter : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            var userContextProvider = DependencyResolver.Current.GetService<IUserContextProvider>();

            var userContext = userContextProvider.GetCurrentUserContext();

            if (!userContext.IsAuthenticated)
            {
                var opContext = DependencyResolver.Current.GetService<OPContext>();

                var directUrl = string.Empty;

                if (opContext != null)
                {
                    var pipe = opContext.Pipe;

                    var pipeName = pipe.PipeName;

                    var uIDescriptor = pipe.UIDescriptor as WebUIPipeDescriptor;

                    if (uIDescriptor != null)
                    {
                        var loginStepName = uIDescriptor.LoginStep;

                        var url = new UrlWriter(uIDescriptor.DefaultPageUrl).WithParam(
                                UrlWriterParameter.CreateParameter("step", loginStepName));
                        url.WithParam("pipe", pipeName).WithBaseUrlParameters();

                        directUrl = url.ToString();
                    }
                }
                else
                {
                    if (filterContext.RequestContext.HttpContext.Request.Url != null)
                        directUrl = filterContext.RequestContext.HttpContext.Request.Url.ToString();
                }

                if (filterContext.HttpContext.Request.IsAjaxRequest())
                {
                    filterContext.Result = new HttpUnauthorizedResult(directUrl);
                }
                else
                {
                    filterContext.Result = new RedirectResult(directUrl);
                }

            }

            base.OnActionExecuting(filterContext);
        }
    }
}