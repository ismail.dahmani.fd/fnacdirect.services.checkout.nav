using FnacDirect.Customer.Model;
using FnacDirect.OrderPipe.Base.Model.Facets;

namespace FnacDirect.OrderPipe.Base.Business.Ogone
{
    public interface IOgoneCvcStatusService
    {
        bool IsRequiredForCardType(int creditCardTypeId,
                                          IGotUserContextInformations gotUserContextInformations,
                                          IGotLineGroups gotLineGroups,
                                          IGotShippingMethodEvaluation gotShippingMethodEvaluation,
                                          IGotBillingInformations gotBillingInformations);

        bool IsRequiredForCard(CreditCardEntity customerCreditCard,
                               IGotUserContextInformations gotUserContextInformations,
                               IGotLineGroups gotLineGroups,
                               IGotShippingMethodEvaluation gotShippingMethodEvaluation,
                               IGotBillingInformations gotBillingInformations);
    }
}
