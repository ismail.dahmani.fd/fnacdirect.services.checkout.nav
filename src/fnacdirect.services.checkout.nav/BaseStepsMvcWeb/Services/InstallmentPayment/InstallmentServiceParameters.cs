using FnacDirect.InstallmentPayment.Models;
using FnacDirect.OrderPipe.Base.BaseModel;
using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.OrderPipe.Base.Model.Facets;
using FnacDirect.Technical.Framework.Web.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;

namespace FnacDirect.OrderPipe.Base.Business.InstallmentPayment
{
    public class InstallmentServiceParameters
    {
        [Flags]
        public enum CheckField
        {
            Articles = 0x1,
            PriceToPay = 0x2,
            ShippingMethodIds = 0x4,
            Parameters = 0x8,
            HasOrderBillingMethods = 0X10,
            TotalPriceToPay = 0x20,
            AccountId = 0x40,
            IsGuest = 0x80,
            IsVisitorAuthenticated = 0x100,
            BillingAddressCountryId = 0x200,
            ShippingMethodEvaluation = 0x400,
            CreditCardTypesConfig = 0x800,
            CreditCardTypeIds = 0x1000,
            Country = 0x2000
        }
        
        public IEnumerable<AvailabilityInput> Articles { get; }
        
        public decimal? PriceToPay { get; }

        public IEnumerable<int> ShippingMethodIds { get; }

        public IEnumerable<Parameters> Parameters { get; }
        
        public bool? HasOrderBillingMethods { get; }

        public decimal? TotalPriceEur { get; }

        public string AccountId { get; }

        public bool IsCallCenter { get; }

        public bool? IsGuest { get; }

        public bool? IsVisitorAuthenticated { get; }

        public int? BillingAddressCountryId { get; }

        public ShippingMethodEvaluation ShippingMethodEvaluation { get; }

        public IEnumerable<CreditCardTypeConfig> CreditCardTypesConfig { get; }

        public IEnumerable<int> CreditCardTypeIds { get; }
        
        public string Country { get; }

        public bool IsAdherent { get; }

        public InstallmentServiceParameters(
            MultiFacet<IGotLineGroups, IGotBillingInformations, IGotUserContextInformations, IGotShippingMethodEvaluation> multiFacet,
            IUserExtendedContextProvider userExtendedContextProvider,
            string localCountry)
        {
            var paymentSelectionData = multiFacet.GetPrivateData<PaymentSelectionData>(false);
            var remainingPriceToPay = paymentSelectionData.RemainingAmount;

            if (remainingPriceToPay == 0
                && !multiFacet.Facet2.OrderBillingMethods.Any())
            {
                remainingPriceToPay = multiFacet.Facet2.GlobalPrices.TotalPriceEur.GetValueOrDefault(0);
            }
            
            //ArticleList
            var lineGroups = multiFacet.Facet1.LineGroups;
            var availabilityInputs = new List<AvailabilityInput>();
            
            foreach (var lineGroup in lineGroups)
            {
                foreach (var article in lineGroup.Articles)
                {
                    var offer = new Guid();

                    if (article is MarketPlaceArticle marketPlaceArticle)
                    {
                        offer = marketPlaceArticle.Offer.Reference;
                    }
                    else if (article is StandardArticle standardArticle && standardArticle.Services != null)
                    {
                        foreach (var service in standardArticle.Services)
                        {
                            availabilityInputs.Add(new AvailabilityInput
                            {
                                ArticleReference = service.ToArticleReference(),
                                Quantity = service.Quantity.GetValueOrDefault(1),
                                AvailabilityId = service.AvailabilityId,
                                ItemTypeId = service.TypeId
                            });
                        }
                    }

                    availabilityInputs.Add(new AvailabilityInput
                    {
                        ArticleReference = article.ToArticleReference(),
                        Quantity = article.Quantity.GetValueOrDefault(1),
                        OfferReference = offer,
                        AvailabilityId = (article as BaseArticle)?.AvailabilityId,
                        ClickAndCollect = article.CollectInShop,
                        ItemTypeId = article.ArticleDTO?.Core?.Type?.ID
                    });
                }
            }

            var userContextInformations = multiFacet.Facet3.UserContextInformations;
            var userExtendedContext = userExtendedContextProvider.GetCurrentUserExtendedContext();

            var shippingMethodEvaluation = multiFacet.Facet4.ShippingMethodEvaluation;
            
            var hasOrderBillingMethods = multiFacet.Facet2.OrderBillingMethods.Any();
            var totalPriceEur = multiFacet.Facet2.GlobalPrices.TotalPriceEur.GetValueOrDefault(0);
            var isCallCenter = userContextInformations.IdentityImpersonator != null;

            PriceToPay = remainingPriceToPay;
            Articles = availabilityInputs;
            HasOrderBillingMethods = hasOrderBillingMethods;
            TotalPriceEur = totalPriceEur;
            AccountId = userContextInformations.AccountId.ToString();
            IsCallCenter = isCallCenter;
            IsGuest = userContextInformations.IsGuest;
            IsVisitorAuthenticated = userExtendedContext.Visitor.IsAuthenticated;
            BillingAddressCountryId = multiFacet.Facet4.BillingAddress.CountryId;
            ShippingMethodEvaluation = shippingMethodEvaluation;
            Country = localCountry;
            IsAdherent = userContextInformations.IsAdherent;
        }

        public bool IsValid(CheckField properties)
        {
            if (properties.HasFlag(CheckField.Articles)
                && Articles == null)
            {
                return false;
            }

            if (properties.HasFlag(CheckField.Country)
                && Country == null)
            {
                return false;
            }

            if (properties.HasFlag(CheckField.Parameters)
                && Parameters == null)
            {
                return false;
            }

            if (properties.HasFlag(CheckField.AccountId) 
                && AccountId == null)
            {
                return false;
            }

            if (properties.HasFlag(CheckField.IsGuest)
                && IsGuest == null)
            {
                return false;
            }

            if (properties.HasFlag(CheckField.IsVisitorAuthenticated)
                && IsVisitorAuthenticated == null)
            {
                return false;
            }

            if (properties.HasFlag(CheckField.PriceToPay)
                && PriceToPay == null)
            {
                return false;
            }

            if (properties.HasFlag(CheckField.ShippingMethodEvaluation)
                && ShippingMethodEvaluation == null)
            {
                return false;
            }

            if (properties.HasFlag(CheckField.ShippingMethodIds)
                && ShippingMethodIds == null)
            {
                return false;
            }

            if (properties.HasFlag(CheckField.BillingAddressCountryId)
                && BillingAddressCountryId == null)
            {
                return false;
            }

            if (properties.HasFlag(CheckField.CreditCardTypeIds)
                && CreditCardTypeIds == null)
            {
                return false;
            }

            if (properties.HasFlag(CheckField.CreditCardTypesConfig)
                && CreditCardTypesConfig == null)
            {
                return false;
            }

            if (properties.HasFlag(CheckField.HasOrderBillingMethods)
                && HasOrderBillingMethods == null)
            {
                return false;
            }

            if (properties.HasFlag(CheckField.TotalPriceToPay)
                && TotalPriceEur == null)
            {
                return false;
            }

            return true;
        }
    }
}