﻿using FnacDirect.Technical.Framework.CoreServices.Localization;
using FnacDirect.Technical.Framework.Web.Mvc;
using FnacDirect.Technical.Framework.ServiceLocation;
using System;
using System.ComponentModel.DataAnnotations;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Attributes
{
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false, Inherited = true)]
    public class MustBeTrueAttribute : ValidationAttribute
    {
        private readonly IApplicationContext _applicationContext;
        private readonly IUIResourceService _uiResourceService;

        public MustBeTrueAttribute()
        {
            _applicationContext = ServiceLocator.Current.GetInstance<IApplicationContext>();
            _uiResourceService = ServiceLocator.Current.GetInstance<IUIResourceService>();
        }
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            if (value is bool && (bool)value)
            {
                return ValidationResult.Success;
            }

            return new ValidationResult(GetErrorMessage(ErrorMessageResourceName));
        }
        private string GetErrorMessage(string messageId)
        {
            var siteContext = _applicationContext.GetSiteContext();

            if (siteContext != null)
                return _uiResourceService.GetUIResourceValue(messageId, siteContext.Culture);

            return string.Empty;
        }
    }
}
