using FnacDirect.OrderPipe.Base.Model.TaxCalculation;
using System.Collections.Generic;

namespace FnacDirect.OrderPipe.Base.Proxy.TaxCalculation
{
    public interface ITaxCalculationService
    {
        List<TaxCalculationResultItem> GetExtendedPriceForArticles(OrderPipeItemsQuery orderPipeItemsQuery);
        TaxCalcuationLineGroupResult SetLineGroupsAllPricesAndTaxInfos(OrderPipeLineGroupQuery orderPipeLineGroupQuery);
        TaxCalculationShippingChoicesResult SetShippingChoicesPrices(OrderPipeShippingChoicesQuery orderPipeShippingChoicesQuery);
        List<TaxCalculationAvailableWrapResult> SetAvailableWrapPriceAndTaxInfos(OrderPipeWrapQuery orderPipeWrapQuery);
        TaxCalculationSplitMethodResult SetSplitMethodPrices(OrderPipeSplitMethodQuery orderPipeSplitMethodQuery);
    }
}
