﻿using FnacDirect.OrderPipe.Config;
namespace FnacDirect.OrderPipe.CoreRouting
{
    public interface IOrderPipeStepRedirectionStrategy
    {
        string DoRedirectionFor(IUiDescriptor uiDescriptor, OPContext opContext);
    }

    public interface IOrderPipeStepRedirectionStrategy<in TDescriptor> : IOrderPipeStepRedirectionStrategy
        where TDescriptor : IUiDescriptor
    {
        
    }
}
