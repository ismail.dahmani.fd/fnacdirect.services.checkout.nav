﻿using System.Collections.Generic;
using FnacDirect.Customer.Model;

namespace FnacDirect.OrderPipe.Base.Proxy.Application
{
    public interface ICountryWithVatService
    {
        Dictionary<int, CountryVatIntraEntity> Get();
    }
}
