namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Orderpipe.Common.Shipping.Network.Postal.Appointment.Fly
{
    public class FlyViewModel
    {
        public FlyArticleViewModel[] Articles { get; set; }

        public FlyFormViewModel FlyForm { get; set; }
    }
}
