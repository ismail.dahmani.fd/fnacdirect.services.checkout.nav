﻿using FnacDirect;
using FnacDirect.Technical.Framework.Web;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Routing;

namespace FnacDirect.OrderPipe.CoreRouting.Rules
{
    public class RoutingRulesApplier
    {
        private readonly IList<IRoutingRule> _routingRules
            = new List<IRoutingRule>();

        public RoutingRulesApplier(ISwitchProvider switchProvider, Func<IUrlManager> urlManagerFactory)
        {
            _routingRules.Add(new SetAppCookieRule());
            _routingRules.Add(new RedirectDefaultToPopRoutingRule(switchProvider, urlManagerFactory));
        }

        public IHttpHandler Apply(RequestContext requestContext)
        {
            foreach (var routingRule in _routingRules)
            {
                var httpHandler = routingRule.Apply(requestContext);

                if (httpHandler != null)
                {
                    return httpHandler;
                }
            }

            return null;
        }
    }
}
