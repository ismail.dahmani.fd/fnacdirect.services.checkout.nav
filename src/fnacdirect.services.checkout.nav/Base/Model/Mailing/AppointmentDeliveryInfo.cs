using System;

namespace FnacDirect.OrderPipe.Base.Business.Mailing
{
    public class AppointmentDeliveryInfo
    {
        public DateTime DateMin { get; set; }
        public DateTime DateMax { get; set; }
    }
}
