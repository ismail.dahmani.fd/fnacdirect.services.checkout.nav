﻿using FnacDirect.OrderPipe.Base.Model.OrderInformations;

namespace FnacDirect.OrderPipe.Base.Model.Facets
{
    public interface IGotContextInformations : IOF
    {
        IOrderContextInformations OrderContextInformations { get; }
    }
}
