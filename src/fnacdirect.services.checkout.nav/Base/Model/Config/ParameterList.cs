using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Serialization;
using FnacDirect.Technical.Framework.Utils;

namespace FnacDirect.OrderPipe.Config
{
    public class ParameterList : KeyedCollection<string, Parameter>
    {
        protected override string GetKeyForItem(Parameter item)
        {
            if (item == null)
                throw new ArgumentNullException("item");

            return item.Key;
        }

        public new string this[string key]
        {
            get
            {
                if (Contains(key))
                    return base[key].Value;
                return null;
            }
            set
            {
                if (Contains(key))
                {
                    base[key].Value = value;
                }
                else
                {
                    Add(new Parameter(key, value));
                }
            }

        }
        public string Get(string key, string def)
        {
            if (Contains(key))
                return base[key].Value;
            return def;
        }

        public int Get(string key, int def)
        {
            if (Contains(key))
            {
                return int.Parse(base[key].Value, CultureInfo.InvariantCulture);
            }
            return def;
        }

        public bool Get(string key, bool def)
        {
            if (Contains(key))
            {
                return bool.Parse(base[key].Value);
            }
            return def;
        }

        public T Get<T>(string key)
        {
            return (T)Convert.ChangeType(base[key].Value, typeof(T), CultureInfo.InvariantCulture);
        }

        /// <summary>
        /// Récupère la valeur d'un paramètre.
        /// Si le paramètre est absent alors la valeur par défaut sera retournée.
        /// </summary>
        /// <typeparam name="T">Type de la valeur du paramètre.</typeparam>
        /// <param name="key">Clé du paramètre.</param>
        /// <param name="defaultValue">Valeur par défaut du paramètre.</param>
        /// <returns>Retourne la valeur du paramètre si ce dernier existe, sinon retourne la valeur par défaut spécifiée
        /// dans la signature de la méthode.</returns>
        public T Get<T>(string key, T defaultValue)
        {
            if (Contains(key))
            {
                return (T)Convert.ChangeType(base[key].Value, typeof(T), CultureInfo.InvariantCulture);
            }
            return defaultValue;
        }

        public IDictionary<T, U> GetAsDictionary<T, U>(string key)
        {
            if (Contains(key))
            {
                var root = XElement.Parse("<root>" + base[key].Value + "</root>");

                var items = root.Descendants("Item")
                                .Select(x => new
                                {
                                    Key = (T)Convert.ChangeType(x.Attribute("key").Value, typeof(T), CultureInfo.InvariantCulture),
                                    Value = (U)Convert.ChangeType(x.Value, typeof(U), CultureInfo.InvariantCulture)
                                })
                                .ToDictionary(x => x.Key, x => x.Value);

                return items;
            }

            return new Dictionary<T, U>();
        }

        public IDictionary<string, T> GetAsDictionary<T>(string key)
        {
            return GetAsDictionary<string, T>(key);
        }


        public int GetAsInt(string key)
        {
            if (!int.TryParse(base[key].Value, NumberStyles.Integer, CultureInfo.InvariantCulture, out var intKey))
            {
                throw new Exception($"Problème avec le paramètre : {key}");
            }
            return intKey;
        }
        /// <summary>
        /// Récupère la valeur du paramètre en tant que TimeSpan.
        /// </summary>
        /// <param name="key">Clé du paramètre.</param>
        /// <param name="defaultValueInMilliseconds">Valeur par défaut en millisecondes.</param>
        /// <returns></returns>
        public TimeSpan GetAsTimeSpan(string key, double defaultValueInMilliseconds)
        {
            return TimeSpan.FromMilliseconds(Get(key, defaultValueInMilliseconds));
        }

        public RangeSet<T> GetAsRangeSet<T>(string key) where T : IComparable<T>
        {
            if (Contains(key))
            {
                return RangeSet.Parse<T>(base[key].Value);
            }

            return new RangeSet<T>();
        }

        public IEnumerable<T> GetAsEnumerable<T>(string key) where T : IComparable<T>
        {
            if (Contains(key))
            {
                var value = base[key].Value;

                if (string.IsNullOrEmpty(value))
                {
                    return Enumerable.Empty<T>();
                }

                return value.Split(',').Select(item => (T)Convert.ChangeType(item, typeof(T)));
            }

            return Enumerable.Empty<T>();
        }

        public decimal Get(string key, decimal def)
        {
            if (Contains(key))
            {
                return decimal.Parse(base[key].Value, CultureInfo.InvariantCulture);
            }
            return def;
        }

        /// <summary>
        ///  Deserialise un objet Parameter qui a été rentré en XML. Le Converti en type T si possible. Retourne Null sinon.
        /// </summary>
        public T GetAs<T>(string key)
            where T : class
        {
            var serializer = new XmlSerializer(typeof(T));

            using (TextReader reader = new StringReader(base[key].Value))
            {
                return serializer.Deserialize(reader) as T;
            }
        }
    }

}
