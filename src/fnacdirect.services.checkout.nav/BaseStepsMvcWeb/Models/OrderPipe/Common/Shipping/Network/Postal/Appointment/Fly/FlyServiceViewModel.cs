using FnacDirect.Technical.Framework.Web.Mvc;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Orderpipe.Common.Shipping.Network.Postal.Appointment.Fly
{
    public class FlyServiceViewModel
    {
        public int? Prid { get; set; }

        public string Title { get; set; }

        public string SubTitle { get; set; }

        public decimal Price { get; set; }

        public PriceDetailsViewModel PriceDetails { get; set; }
    }
}
