using FnacDirect.OrderPipe.Base.Model;

namespace FnacDirect.OrderPipe.CM.Model
{
    public class CheckoutBillingMethod : BillingMethod
    {
        public const int IdBillingMethod = 16;

        public override bool ShouldBePersistent
        {
            get
            {
                return false;
            }
        }

        public CheckoutBillingMethod()
        {
            BillingMethodId = IdBillingMethod;
            Priority = -1;
            CanModifyAmountUp = true;
            CanModifyAmountDown = true;
            ImplyValidateOrder = true;
            Name = "CheckoutBillingMethod";
        }

        public override string ToString()
        {
            return "Paiement à la caisse";
        }
    }
}
