namespace FnacDirect.OrderPipe.Base.Model
{
    public class UniverseInfo
    {
        private int? _UniverseId;
        public int? UniverseId
        {
            get { return _UniverseId; }
            set { _UniverseId = value; }
        }
    }
}
