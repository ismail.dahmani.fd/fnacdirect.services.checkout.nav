using System.Collections.Generic;
using FnacDirect.OrderPipe.Base.BaseModel.Model;
using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.OrderPipe.Base.Model.LineGroups;
using FnacDirect.OrderPipe.Base.Model.Facets;
using FnacDirect.Contracts.Online.Model;
using FnacDirect.Membership.Configuration;
using FnacDirect.OrderPipe.Base.LineGroups;

namespace FnacDirect.OrderPipe.Base.Business
{
    public interface IFnacPlusCardService
    {
        CardInfosData GetRenewCardInfos(MultiFacet<IGotLineGroups, IGotUserContextInformations, IGotBillingInformations, IGotShippingMethodEvaluation, IGotMobileInformations> multiFacetOrderForm, int cardId);
        CardInfosData GetCardInfosFromPricer(MultiFacet<IGotLineGroups, IGotUserContextInformations, IGotBillingInformations, IGotShippingMethodEvaluation, IGotMobileInformations> multiFacetOrderForm, string cardKey);
        CardInfosData GetCardInfosFromPricer(MultiFacet<IGotLineGroups, IGotUserContextInformations, IGotBillingInformations, IGotShippingMethodEvaluation, IGotMobileInformations> multiFacetOrderForm, int cardPrid);
        CardInfosData GetCardInfosFromPricer(MultiFacet<IGotLineGroups, IGotUserContextInformations, IGotBillingInformations, IGotShippingMethodEvaluation, IGotMobileInformations> multiFacetOrderForm, AdherentCardArticle card);
        ShoppingCartLineItem GetCardFromPricer(MultiFacet<IGotLineGroups, IGotUserContextInformations, IGotBillingInformations, IGotShippingMethodEvaluation, IGotMobileInformations> multiFacetOrderForm, StandardArticle card, int? refGu = null);
        bool HasManyFnacPlusCardFromBasket(IEnumerable<ILineGroup> lineGroups);
        StandardArticle GetFnacPlusCardFromBasket(IEnumerable<ILineGroup> lineGroups);
        StandardArticle GetFnacPlusTryCardFromBasket(IEnumerable<ILineGroup> lineGroups);
        StandardArticle GetAdhCardFromBasket(IEnumerable<ILineGroup> lineGroups);
        StandardArticle GetAdhTryCardFromBasket(IEnumerable<ILineGroup> lineGroups);

        bool HasManyFnacPlusCardFromBasket(IEnumerable<ILogisticLineGroup> lineGroups);
        StandardArticle GetFnacPlusCardFromBasket(IEnumerable<ILogisticLineGroup> lineGroups);
        StandardArticle GetFnacPlusTryCardFromBasket(IEnumerable<ILogisticLineGroup> lineGroups);
        StandardArticle GetAdhCardFromBasket(IEnumerable<ILogisticLineGroup> lineGroups);
        StandardArticle GetAdhTryCardFromBasket(IEnumerable<ILogisticLineGroup> lineGroups);
        bool FnacPlusEnabled { get; }
    }
}
