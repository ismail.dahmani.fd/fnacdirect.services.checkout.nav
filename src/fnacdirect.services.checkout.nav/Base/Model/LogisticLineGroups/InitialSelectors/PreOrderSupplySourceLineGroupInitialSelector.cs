﻿using FnacDirect.OrderPipe.Base.Business.LogisticLineGroups.Internals.InitialSelectors;
using FnacDirect.OrderPipe.Base.LineGroups;
using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.OrderPipe.Base.Model.LineGroups;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FnacDirect.OrderPipe.Base.Business.LogisticLineGroups.InitialSelectors
{
    public class PreOrderSupplySourceLineGroupInitialSelector : BaseLineGroupInitialSelector
    {
        public override IDictionary<ILineGroupHeader, IEnumerable<Article>> Select(IEnumerable<ILineGroup> lineGroups)
        {
            var output = new Dictionary<ILineGroupHeader, IEnumerable<Article>>();

            foreach (var lineGroup in lineGroups.Where(l => l.HasFnacComArticle))
            {
                var articles = lineGroup.Articles.OfType<StandardArticle>().Where(a => a.SupplySource != null && (a.AvailabilityId == (int)AvailabilityEnum.PreOrder || a.AvailabilityId == (int)AvailabilityEnum.PreOrderBis));

                if (articles.Any())
                {
                    output.Add(lineGroup, articles);
                }
            }

            return output;
        }
    }
}
