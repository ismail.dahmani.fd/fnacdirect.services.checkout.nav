using FnacDirect.OrderPipe.Base.Model;
using System.Collections.Generic;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Pop.Models.Common
{
    public interface IPipeMessageViewModelBuilder
    {
        List<PipeMessageViewModel> Build(PopOrderForm popOrderForm);
        List<PipeMessageViewModel> BuildPipeMessageForSpecificCategory(PopOrderForm popOrderForm, string category);

        PipeMessageViewModel CreatePipeMessageViewModel(string category, string uiResourceId);
    }
}
