using FnacDirect.OrderPipe.Base.Model;
using System;
using System.Collections.Generic;

namespace FnacDirect.OrderPipe.Base.Business.Shipping
{
    public class AppointmentDeliverySlotsSortingResult
    {
        public AppointmentDeliverySlotsSortingResult(DateTime minStartDate, DateTime maxEndDate, List<Article> eligibleArticles, int? pageIndex, List<Model.Solex.DeliverySlot>[] sortedSlots, DateTime startDateReference, bool isServiceCallSuccess = true)
        {
            MinStartDate = minStartDate;
            MaxEndDate = maxEndDate;
            EligibleArticles = eligibleArticles;
            PageIndex = pageIndex;
            SortedSlots = sortedSlots;
            StartDateReference = startDateReference;
            IsServiceCallSuccess = isServiceCallSuccess;
        }

        private AppointmentDeliverySlotsSortingResult(bool hasError, string errorMessage)
        {
            HasError = hasError;
            ErrorMessage = errorMessage;
        }

        public DateTime MinStartDate { get; }
        public DateTime MaxEndDate { get; }
        public List<Article> EligibleArticles { get; } = new List<Article>();
        public int? PageIndex { get; }
        public List<Model.Solex.DeliverySlot>[] SortedSlots { get; } = new List<Model.Solex.DeliverySlot>[0];
        public DateTime StartDateReference { get; }
        public bool HasError { get; }
        public string ErrorMessage { get; }
        public bool IsServiceCallSuccess { get; }

        public static AppointmentDeliverySlotsSortingResult CreateInstanceWithError(string errorMessage)
        {
            return new AppointmentDeliverySlotsSortingResult(true, errorMessage);
        }
    }
}
