using System.Collections.ObjectModel;

namespace FnacDirect.OrderPipe.Base.Model
{
    public class VatInfo
    {
        public int CountryCode;
        public int VatCategory;
        public string CodeGU;
        public decimal Rate;

        public VatInfo()
        { }

        public VatInfo(VatInfo source)
        {
            CodeGU = source.CodeGU;
            CountryCode = source.CountryCode;
            Rate = source.Rate;
            VatCategory = source.VatCategory;
        }

        public VatInfo(int countryCode, int vatCategory)
        {
            CountryCode = countryCode;
            VatCategory = vatCategory;
        }
    }

    public class VatInfoList : KeyedCollection<string, VatInfo>
    {
        protected override string GetKeyForItem(VatInfo item)
        {
            return item.CountryCode + "-" + item.VatCategory;
        }

        public VatInfo this[int countryCode, int vatCategory]
        {
            get
            {
                var key = countryCode + "-" + vatCategory;
                if (Contains(key))
                    return base[key];
                else
                {
                    return new VatInfo(countryCode, vatCategory);
                }

            }
        }
    }

}

