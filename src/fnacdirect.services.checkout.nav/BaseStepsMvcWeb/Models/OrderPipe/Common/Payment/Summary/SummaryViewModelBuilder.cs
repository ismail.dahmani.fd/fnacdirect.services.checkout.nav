using System.Collections.Generic;
using System.Linq;
using FnacDirect.Contracts.Online.Model;
using FnacDirect.Membership.Model.Constants;
using FnacDirect.OrderPipe.Base.BaseModel;
using FnacDirect.OrderPipe.Base.BaseModel.Model;
using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.OrderPipe.Base.Model.Pop;
using FnacDirect.OrderPipe.Base.Proxy.Membership;
using FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Basket;
using FnacDirect.OrderPipe.BaseMvc.Web.Services.Pop.Payment;
using FnacDirect.Technical.Framework.Web.Mvc;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Common.Payment
{
    public class SummaryViewModelBuilder : ISummaryViewModelBuilder
    {
        private const int ExcludedNodeId = 9616962;
        private readonly IApplicationContext _applicationContext;

        private readonly string _defaultCountry;
        private readonly IMembershipConfigurationService _membershipConfigurationService;
        private readonly IPaymentMethodService _paymentMethodService;
        private readonly IPriceHelper _priceHelper;
        private readonly ISplitViewModelBuilder _splitViewModelBuilder;
        private readonly ISummaryPricesViewModelBuilder _summaryPricesViewModelBuilder;
        private readonly ISwitchProvider _switchProvider;
        private readonly IUserExtendedContext _userExtendedContext;
        private readonly IUserExtendedContextProvider _userExtendedContextProvider;
        private readonly ITermsAndConditionsViewModelBuilder _termsAndConditionsViewModelBuilder;

        public SummaryViewModelBuilder(IApplicationContext applicationContext,
            ISplitViewModelBuilder splitViewModelBuilder,
            ICountryService countryService,
            ISwitchProvider switchProvider,
            IPaymentMethodService paymentMethodService,
            IMembershipConfigurationService membershipConfigurationService,
            IUserExtendedContextProvider userExtendedContextProvider,
            ISummaryPricesViewModelBuilder summaryPricesViewModelBuilder,
            IUserExtendedContext userExtendedContext,
            ITermsAndConditionsViewModelBuilder termsAndConditionsViewModelBuilder
        )
        {
            _applicationContext = applicationContext;
            _priceHelper = applicationContext.GetPriceHelper();
            _splitViewModelBuilder = splitViewModelBuilder;
            _switchProvider = switchProvider;
            _paymentMethodService = paymentMethodService;
            _membershipConfigurationService = membershipConfigurationService;
            _userExtendedContextProvider = userExtendedContextProvider;
            _summaryPricesViewModelBuilder = summaryPricesViewModelBuilder;
            _userExtendedContext = userExtendedContext;
            _termsAndConditionsViewModelBuilder = termsAndConditionsViewModelBuilder;
            var defaultCountry = countryService.GetCountries()
                .FirstOrDefault(c => c.ID == _applicationContext.GetSiteContext().CountryId);
            _defaultCountry = defaultCountry != null ? defaultCountry.Label : string.Empty;
        }

        public PaymentSummaryViewModel Build(OPContext opContext, PopOrderForm popOrderForm)
        {
            var paymentSummaryViewModel = new PaymentSummaryViewModel();
            var oldTotalCost = 0m;
            var totalCost = 0m;
            var oldTotalCostNoVat = 0m;
            var totalCostNoVat = 0m;
            var siteContext = _applicationContext.GetSiteContext();
            var culture = siteContext != null ? siteContext.Culture : "";
            var config = _applicationContext.GetAppSetting("config.shippingMethodSelection");
            var aboIlliData = popOrderForm.GetPrivateData<AboIlliData>("AboIlliGroup");
            var wrapData = popOrderForm.GetPrivateData<WrapData>(create: false);

            var isShop = false;
            var shipMethod = popOrderForm.ShippingMethodEvaluation.ShippingMethodEvaluationGroups;

            var isFnacPro = _applicationContext.GetSiteId() == (int)FnacSites.FNACPRO;

            if (shipMethod.Any(s => s.SelectedShippingMethodEvaluationType == ShippingMethodEvaluationType.Shop))
                isShop = true;

            paymentSummaryViewModel.ProductsCount = popOrderForm.LineGroups.GetArticles().Sum(a => a.GetQuantity())
                                                    - popOrderForm.LineGroups.GetArticles().OfType<StandardArticle>()
                                                        .Where(a => a.Hidden).Sum(a => a.GetQuantity());

            if (paymentSummaryViewModel.ProductsCount > 1)
            {
                paymentSummaryViewModel.Basket =
                    _applicationContext.GetMessage("orderpipe.pop.payment.summary.products",
                        paymentSummaryViewModel.ProductsCount);
            }
            else
            {
                paymentSummaryViewModel.Basket = _applicationContext.GetMessage("orderpipe.pop.payment.summary.product",
                    paymentSummaryViewModel.ProductsCount);
            }
                
            var fnacLg = popOrderForm.LineGroups.Where(lg => lg.HasFnacComArticle).ToList();
            var hasOnlyExpressPlusInFnacCom = fnacLg.GetArticles().Any(a => a.TypeId.HasValue
                                                && aboIlliData.TypeArtIds.Contains(a.TypeId.Value))
                                                && fnacLg.GetArticles().Count() == 1;

            foreach (var item in popOrderForm.LineGroups.GetArticles())
            {
                var article = (BaseArticle)item;
                var ecoTax = 0m;
                var oldSvcTotalCost = 0m;
                var svcTotalCost = 0m;

                //HT
                var ecoTaxNoVat = 0m;
                var oldSvcTotalCostNoVat = 0m;
                var svcTotalCostNoVat = 0m;

                if (article is StandardArticle stdArticle)
                {
                    ecoTax = stdArticle.EcoTaxEur.GetValueOrDefault();
                    ecoTaxNoVat = stdArticle.EcoTaxEurNoVAT.GetValueOrDefault();

                    foreach (var service in stdArticle.Services)
                    {
                        oldSvcTotalCost += service.PriceDBEur.GetValueOrDefault() * service.Quantity.GetValueOrDefault(1);
                        svcTotalCost += service.PriceUserEur.GetValueOrDefault() * service.Quantity.GetValueOrDefault(1);

                        oldSvcTotalCostNoVat += service.SalesInfo.StandardPrice.HT * service.Quantity.GetValueOrDefault(1);
                        svcTotalCostNoVat += service.PriceNoVATEur.GetValueOrDefault() * service.Quantity.GetValueOrDefault(1);
                    }
                }
                //TTC
                var oldUnitPrice = article.PriceDBEur.GetValueOrDefault() + ecoTax;
                var unitPrice = article.PriceUserEur.GetValueOrDefault() + ecoTax;

                oldTotalCost += oldUnitPrice * article.Quantity.GetValueOrDefault(1) + oldSvcTotalCost;
                totalCost += unitPrice * article.Quantity.GetValueOrDefault(1) + +svcTotalCost;

                //HT
                var oldUnitPriceNoVat =
                    (article.SalesInfo?.StandardPrice?.HT ?? article.PriceNoVATEur.GetValueOrDefault()) + ecoTaxNoVat;
                var unitPriceNoVat = article.PriceNoVATEur.GetValueOrDefault() + ecoTaxNoVat;

                oldTotalCostNoVat += oldUnitPriceNoVat * article.Quantity.GetValueOrDefault(1) + oldSvcTotalCostNoVat;
                totalCostNoVat += unitPriceNoVat * article.Quantity.GetValueOrDefault(1) + +svcTotalCostNoVat;
            }

            //Split
            paymentSummaryViewModel.HasSplit = popOrderForm.LineGroups.Fnac().SelectMany(lg => lg.LogisticTypes)
                .Any(lg => lg.SplitLevel == 1);

            //prix TTC
            var splitCost = _splitViewModelBuilder.SplitCost(popOrderForm);
            paymentSummaryViewModel.SplitCost = _priceHelper.Format(splitCost, PriceFormat.CurrencyAsDecimalSeparator);
            paymentSummaryViewModel.SplitCostModel = _priceHelper.GetDetails(splitCost);

            //prix HT
            var splitCostNoVat = _splitViewModelBuilder.SplitCostNoVAT(popOrderForm);
            paymentSummaryViewModel.SplitCostNoVAT = _priceHelper.Format(splitCostNoVat, PriceFormat.CurrencyAsDecimalSeparator);
            paymentSummaryViewModel.SplitCostNoVATModel = _priceHelper.GetDetails(splitCostNoVat);

            paymentSummaryViewModel.SplitCostToDisplay = isFnacPro ? _priceHelper.Format(splitCostNoVat, PriceFormat.CurrencyAsDecimalSeparator)
                                                                   : _priceHelper.Format(splitCost, PriceFormat.CurrencyAsDecimalSeparator);

            paymentSummaryViewModel.SplitCostModelToDisplay = isFnacPro ? _priceHelper.GetDetails(splitCostNoVat)
                                                                        : _priceHelper.GetDetails(splitCost);

            paymentSummaryViewModel.IsFreeSplit = splitCost == decimal.Zero;

            //Wrap
            paymentSummaryViewModel.ShouldDisplayWrap = (wrapData != null && wrapData.HasBeenRemovedForShopShipping); 

            paymentSummaryViewModel.HasWrap =
                popOrderForm.LineGroups.Fnac().Any(lg => lg.Informations.ChosenWrapMethod != 0);

            //prix TTC
            var wrapCost = popOrderForm.LineGroups.Fnac()
                    .Sum(lg => lg.GlobalPrices.TotalShoppingCartBusinessWrapPriceUserEur.GetValueOrDefault());

            paymentSummaryViewModel.WrapCost = _priceHelper.Format(wrapCost, PriceFormat.CurrencyAsDecimalSeparator);

            paymentSummaryViewModel.WrapCostModel = _priceHelper.GetDetails(wrapCost);

            //prix HT
            var wrapCostNoVat = popOrderForm.LineGroups.Fnac().Sum(lg =>
                lg.GlobalPrices.TotalShoppingCartBusinessWrapPriceNoVatEur.GetValueOrDefault());

            paymentSummaryViewModel.WrapCostNoVAT = _priceHelper.Format(wrapCostNoVat, PriceFormat.CurrencyAsDecimalSeparator);

            paymentSummaryViewModel.WrapCostNoVATModel = _priceHelper.GetDetails(wrapCostNoVat);

            paymentSummaryViewModel.WrapCostToDisplay = isFnacPro ? _priceHelper.Format(wrapCostNoVat, PriceFormat.CurrencyAsDecimalSeparator)
                                                                  : _priceHelper.Format(wrapCost, PriceFormat.CurrencyAsDecimalSeparator);

            paymentSummaryViewModel.WrapCostModelToDisplay = isFnacPro ? _priceHelper.GetDetails(wrapCostNoVat)
                                                                       : _priceHelper.GetDetails(wrapCost);

            paymentSummaryViewModel.IsFreeWrap = wrapCost == decimal.Zero;

            //TTC
            paymentSummaryViewModel.TotalCost = _priceHelper.Format(totalCost, PriceFormat.CurrencyAsDecimalSeparator);

            paymentSummaryViewModel.TotalCostModel = _priceHelper.GetDetails(totalCost);

            if (totalCost != oldTotalCost)
            {
                paymentSummaryViewModel.OldTotalCost = _priceHelper.Format(oldTotalCost, PriceFormat.CurrencyAsDecimalSeparator);
                paymentSummaryViewModel.OldTotalCostModel = _priceHelper.GetDetails(oldTotalCost);
                if (!isFnacPro)
                {
                    paymentSummaryViewModel.OldTotalCostModelToDisplay = _priceHelper.GetDetails(oldTotalCost);
                    paymentSummaryViewModel.OldTotalCostToDisplay = _priceHelper.Format(oldTotalCost, PriceFormat.CurrencyAsDecimalSeparator);
                }
            }

            //HT
            paymentSummaryViewModel.TotalCostNoVAT = _priceHelper.Format(totalCostNoVat, PriceFormat.CurrencyAsDecimalSeparator);

            paymentSummaryViewModel.TotalCostNoVATModel = _priceHelper.GetDetails(totalCostNoVat);

            if (totalCostNoVat != oldTotalCostNoVat)
            {
                paymentSummaryViewModel.OldTotalCostNoVAT = _priceHelper.Format(oldTotalCostNoVat, PriceFormat.CurrencyAsDecimalSeparator);
                paymentSummaryViewModel.OldTotalCostNoVATModel = _priceHelper.GetDetails(oldTotalCostNoVat);
                if (isFnacPro)
                {
                    paymentSummaryViewModel.OldTotalCostModelToDisplay = _priceHelper.GetDetails(oldTotalCostNoVat);
                    paymentSummaryViewModel.OldTotalCostToDisplay = _priceHelper.Format(oldTotalCostNoVat, PriceFormat.CurrencyAsDecimalSeparator);
                }
            }

            paymentSummaryViewModel.TotalCostToDisplay = isFnacPro ? _priceHelper.Format(totalCostNoVat, PriceFormat.CurrencyAsDecimalSeparator)
                                                                         : _priceHelper.Format(totalCost, PriceFormat.CurrencyAsDecimalSeparator);

            paymentSummaryViewModel.TotalCostModelToDisplay =  isFnacPro ? _priceHelper.GetDetails(totalCostNoVat)
                                                                         : _priceHelper.GetDetails(totalCost);

            paymentSummaryViewModel.VatRates = BuildVATRates(popOrderForm);

            var totalShippingCost = 0m;
            var totalShippingCostNoVat = 0m;

            foreach (var lineGroup in popOrderForm.LineGroups)
            {
                totalShippingCost += lineGroup.GlobalPrices.TotalShipPriceUserEur.GetValueOrDefault();
                totalShippingCostNoVat += lineGroup.GlobalPrices.TotalShipPriceUserEurNoVAT.GetValueOrDefault();
            }

            //TTC
            paymentSummaryViewModel.TotalShippingCost = _priceHelper.Format(totalShippingCost, PriceFormat.CurrencyAsDecimalSeparator);
            paymentSummaryViewModel.TotalShippingCostModel = _priceHelper.GetDetails(totalShippingCost);

            //HT
            paymentSummaryViewModel.TotalShippingCostNoVAT = _priceHelper.Format(totalShippingCostNoVat, PriceFormat.CurrencyAsDecimalSeparator);
            paymentSummaryViewModel.TotalShippingCostNoVATModel = _priceHelper.GetDetails(totalShippingCostNoVat);

            paymentSummaryViewModel.TotalShippingCostToDisplay = isFnacPro ? _priceHelper.Format(totalShippingCostNoVat, PriceFormat.CurrencyAsDecimalSeparator)
                                                                           : _priceHelper.Format(totalShippingCost, PriceFormat.CurrencyAsDecimalSeparator);

            paymentSummaryViewModel.TotalShippingCostModelToDisplay = isFnacPro ? _priceHelper.GetDetails(totalShippingCostNoVat)
                                                                                : _priceHelper.GetDetails(totalShippingCost) ;

            var totalGlobal = totalShippingCost + totalCost + splitCost + wrapCost;

            var totalGlobalNoVat = totalShippingCostNoVat + totalCostNoVat + splitCostNoVat + wrapCostNoVat;

            foreach (var billingMethod in popOrderForm.OrderBillingMethods.Where(m => !m.IsPrimary && !(m is FreeBillingMethod)))
            {
                var resourceKey = "orderpipe.pop.payment.methods.secondary." + billingMethod.BillingMethodId;
                var message = _applicationContext.TryGetMessage(resourceKey);

                string priceString;
                if (_applicationContext.GetSwitch("front.adherents.card.literalAmountWithoutLiteralReplacement"))
                    priceString = _priceHelper.Format(billingMethod.Amount.GetValueOrDefault(0),
                        PriceFormat.CurrencyAsDecimalSeparator, PriceReplacement.NoLiteralReplacement);
                else
                    priceString = _priceHelper.Format(billingMethod.Amount.GetValueOrDefault(0),
                        PriceFormat.CurrencyAsDecimalSeparator);

                var item = new KeyValuePair<string, string>(message, priceString);

                paymentSummaryViewModel.SecondaryMethods.Add(item);

                totalGlobal -= billingMethod.Amount.GetValueOrDefault(0);
            }

            //TTC
            paymentSummaryViewModel.TotalGlobal = _priceHelper.Format(totalGlobal, PriceFormat.CurrencyAsDecimalSeparator);
            paymentSummaryViewModel.TotalGlobalModel = _priceHelper.GetDetails(totalGlobal);

            //HT
            paymentSummaryViewModel.TotalGlobalNoVAT = _priceHelper.Format(totalGlobalNoVat, PriceFormat.CurrencyAsDecimalSeparator);
            paymentSummaryViewModel.TotalGlobalNoVATModel = _priceHelper.GetDetails(totalGlobalNoVat);

            paymentSummaryViewModel.TotalGlobalToDisplay = _priceHelper.Format(totalGlobal, PriceFormat.CurrencyAsDecimalSeparator);

            paymentSummaryViewModel.TotalGlobalModelToDisplay = _priceHelper.GetDetails(totalGlobal); 

            paymentSummaryViewModel.IsFreeOrder = totalShippingCost + totalCost == 0;

            paymentSummaryViewModel.IsFreeShipping = totalShippingCost == 0;

            var hasOnlyMp = popOrderForm.LineGroups.All(lg => lg.HasMarketPlaceArticle);

            paymentSummaryViewModel.IsExpressPlus = (aboIlliData.IsAbonne || aboIlliData.HasAboIlliInBasket)
                                                    && !hasOnlyMp && !popOrderForm.HasClickAndCollectArticle()
                                                    && !isShop && !hasOnlyExpressPlusInFnacCom;

            paymentSummaryViewModel.IsFnacPlusOrHasFnacPlusCardInBasket = IsFnacPlusUser() || HasFnacPlusCardInBasket(opContext);

            paymentSummaryViewModel.ShippingPush = GetShippingPush(popOrderForm);

            paymentSummaryViewModel.AddPaymentCards(_paymentMethodService.CreditCards(opContext, popOrderForm, config, culture));

            paymentSummaryViewModel.AddPaymentMethods(_paymentMethodService.GetEligiblePaymentMethodesIds(opContext, config));

            GetShopShippingPush(popOrderForm, paymentSummaryViewModel.IsFreeShipping, paymentSummaryViewModel);

            if (IsFnacMember() || HasFnacMemberCardInBasket(opContext) ||
                paymentSummaryViewModel.IsFnacPlusOrHasFnacPlusCardInBasket)
            {
                paymentSummaryViewModel.DifferedSavedAmount =
                    _summaryPricesViewModelBuilder.BuildDifferedSavedAmount(popOrderForm);
            }

            paymentSummaryViewModel.TotalReduced = _summaryPricesViewModelBuilder.BuildTotalReduced(popOrderForm, oldTotalCost, totalCost);

            paymentSummaryViewModel.TotalReducedModel = _summaryPricesViewModelBuilder.BuildTotalReducedModel(popOrderForm, oldTotalCost, totalCost);

            paymentSummaryViewModel.TotalReducedNoVAT = _summaryPricesViewModelBuilder.BuildTotalReduced(popOrderForm, oldTotalCostNoVat, totalCostNoVat, false);

            paymentSummaryViewModel.TotalReducedNoVATModel = _summaryPricesViewModelBuilder.BuildTotalReducedModel(popOrderForm, oldTotalCostNoVat, totalCostNoVat, false);

            paymentSummaryViewModel.TotalReducedToDisplay = isFnacPro ? paymentSummaryViewModel.TotalReducedNoVAT : paymentSummaryViewModel.TotalReduced;

            paymentSummaryViewModel.TotalReducedModelToDisplay = isFnacPro ? paymentSummaryViewModel.TotalReducedNoVATModel : paymentSummaryViewModel.TotalReducedModel;

            paymentSummaryViewModel.ReducedPriceBook5pc = _summaryPricesViewModelBuilder.BuildReducedPriceBook5pc(popOrderForm, totalCost);

            if (popOrderForm.OrderBillingMethods.OfType<VoucherBillingMethod>().Any())
            {
                var cuu = popOrderForm.OrderBillingMethods.OfType<VoucherBillingMethod>().First();
                paymentSummaryViewModel.PromoCode = cuu.CheckNumber;
                paymentSummaryViewModel.PromoCodeAmount =
                    $"- {_priceHelper.Format(cuu.AmountEur.GetValueOrDefault(), PriceFormat.CurrencyAsDecimalSeparator)}";
            }
            paymentSummaryViewModel.HasHideBillingInfo = popOrderForm.LineGroups.Any(lg => lg.Informations.GiftOption);

            paymentSummaryViewModel.TermsAndConditions = _termsAndConditionsViewModelBuilder.Build(popOrderForm.LineGroups);

            return paymentSummaryViewModel;
        }

        private List<VatRate> BuildVATRates(PopOrderForm popOrderForm)
        {
            var model = new List<VatRate>();
            foreach (var vat in popOrderForm.GlobalPrices.VATList)
            {
                if (vat.Total != decimal.Zero)
                {
                    var item = new VatRate
                    {
                        Label = vat.RateLiteral,
                        Total = _priceHelper.Format(vat.Total, PriceFormat.CurrencyAsDecimalSeparator),
                        Rate = vat.Rate
                    };
                    model.Add(item);
                }
            }

            return model;
        }

        private bool IsFnacMember()
        {
            if (_userExtendedContext != null)
                return _userExtendedContext.Customer.IsValidOrTryAdherent;

            return false;
        }

        private bool HasFnacMemberCardInBasket(OPContext opContext)
        {
            if (opContext.OF is PopOrderForm popOf && _membershipConfigurationService != null)
                return popOf.LineGroups.GetArticles().OfType<StandardArticle>().Any(a =>
                    a.TypeId.GetValueOrDefault() == (int)CategoryCardType.Renew ||
                    a.TypeId.GetValueOrDefault() == (int)CategoryCardType.Recruit);

            return false;
        }

        private bool IsFnacPlusUser()
        {
            if (_userExtendedContextProvider != null)
            {
                var userExtendedContext = _userExtendedContextProvider.GetCurrentUserExtendedContext();
                var segments = userExtendedContext.Customer.ContractDTO.Segments;

                return segments.Contains(CustomerSegment.EssaiPlus.ToString())
                       || segments.Contains(CustomerSegment.ExpressPlus.ToString());
            }

            return false;
        }

        private bool HasFnacPlusCardInBasket(OPContext opContext)
        {
            if (opContext.OF is PopOrderForm popOf && _membershipConfigurationService != null)
                return popOf.LineGroups.GetArticles().Any(a =>
                    _membershipConfigurationService.FnacPlusCardTypes.Contains(a.TypeId.GetValueOrDefault()));

            return false;
        }

        private void GetShopShippingPush(PopOrderForm popOrderForm, bool isFreeShipping, PaymentSummaryViewModel paymentSummaryViewModel)
        {
            if (!popOrderForm.ShippingMethodEvaluation.FnacCom.HasValue || popOrderForm.IsMixed())
            {
                paymentSummaryViewModel.ShopShippingPush = string.Empty;
                return;
            }

            var isShopShipping =
                popOrderForm.ShippingMethodEvaluation.FnacCom.Value.SelectedShippingMethodEvaluationType ==
                ShippingMethodEvaluationType.Shop;

            var standardArticles = popOrderForm.LineGroups.SelectMany(lg => lg.Articles.OfType<StandardArticle>())
                .ToList();

            var hasArticlesToShip = popOrderForm.LineGroups.Fnac().GetArticles().Any(a =>
                !popOrderForm.ShippingMethodEvaluation.ExcludedLogisticTypes.Contains(a.LogType.GetHashCode()));

            var basketContainEditorialProduct = standardArticles.Any(art => art.IsPE && art.IsBook && !art.IsNumerical);

            if (!isShopShipping && basketContainEditorialProduct && !HasCustomizedBook(standardArticles)
                && _switchProvider.IsEnabled("orderpipe.pop.show5pctpush"))
            {
                paymentSummaryViewModel.ShowBookShopShippingPush = true;
                paymentSummaryViewModel.ShopShippingPush =
                    _applicationContext.GetMessage("orderpipe.pop.shipping.stickysummary.editorial.push");
                return;
            }

            if (!isShopShipping && !isFreeShipping && !HasCustomizedBook(standardArticles) && hasArticlesToShip)
            {
                paymentSummaryViewModel.ShowBookShopShippingPush = false;
                paymentSummaryViewModel.ShopShippingPush =
                    _applicationContext.GetMessage(
                        "orderpipe.pop.shipping.stickysummary.technical.push");
                return;
            }

            paymentSummaryViewModel.ShopShippingPush = string.Empty;
        }

        private static bool HasCustomizedBook(IEnumerable<StandardArticle> articles)
        {
            return articles.Where(tn => tn.TreeNodes != null).SelectMany(article => article.TreeNodes)
                .Any(treeNode => treeNode != null && treeNode.Id == ExcludedNodeId);
        }

        private string GetShippingPush(PopOrderForm popOrderForm)
        {
            var shippingMethodEvaluation = popOrderForm.ShippingMethodEvaluation;
            var shippingEvaluation = shippingMethodEvaluation.FnacCom;

            //create AboIlliData
            popOrderForm.GetPrivateData<AboIlliData>();

            if (popOrderForm.HasNoFnacComShippableArticle(_applicationContext) && !popOrderForm.HasMarketPlaceArticle())
                return string.Empty;

            if (shippingEvaluation.HasValue
                && shippingEvaluation.Value.SelectedShippingMethodEvaluationType == ShippingMethodEvaluationType.Shop)
            {
                return !popOrderForm.IsMixed()
                    ? _applicationContext.GetMessage("orderpipe.pop.shipping.stickysummary.shopdelivery")
                    : string.Empty;
            }

            var shippingIsRelay = shippingMethodEvaluation.ShippingMethodEvaluationGroups.All(smg =>
                smg.SelectedShippingMethodEvaluationType == ShippingMethodEvaluationType.Relay);

            if (shippingIsRelay)
                return _applicationContext.GetMessage("orderpipe.pop.shipping.stickysummary.relay");

            var shippingCountries = new List<string>();

            if (shippingMethodEvaluation.ShippingMethodEvaluationGroups
                .All(smg => smg.SelectedShippingMethodEvaluationType == ShippingMethodEvaluationType.Postal))
            {
                foreach (var shippingMethodEvaluationGroup in shippingMethodEvaluation.ShippingMethodEvaluationGroups)
                {
                    shippingCountries.Add(shippingMethodEvaluationGroup.PostalShippingMethodEvaluationItem.Value
                                              .ShippingAddress.CountryLabel ?? _defaultCountry);
                }
            }

            return shippingCountries.Distinct().Count() == 1
                ? _applicationContext.GetMessage("orderpipe.pop.shipping.stickysummary.shippinglocation",
                    shippingCountries.First())
                : string.Empty;
        }
    }
}
