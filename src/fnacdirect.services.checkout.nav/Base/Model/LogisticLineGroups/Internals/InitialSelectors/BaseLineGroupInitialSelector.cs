﻿using System.Collections.Generic;
using FnacDirect.OrderPipe.Base.LineGroups;
using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.OrderPipe.Base.Model.LineGroups;

namespace FnacDirect.OrderPipe.Base.Business.LogisticLineGroups.Internals.InitialSelectors
{
    public abstract class BaseLineGroupInitialSelector : ILineGroupInitialSelector
    {
        public ILogisticLineGroupGenerationProcessContext Context { get; set; }
        public abstract IDictionary<ILineGroupHeader, IEnumerable<Article>> Select(IEnumerable<ILineGroup> lineGroups);
    }
}
