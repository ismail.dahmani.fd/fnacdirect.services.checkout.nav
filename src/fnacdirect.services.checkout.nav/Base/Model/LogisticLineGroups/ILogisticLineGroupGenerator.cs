﻿using FnacDirect.OrderPipe.Base.LineGroups;
using FnacDirect.OrderPipe.Base.Model;
using System.Collections.Generic;

namespace FnacDirect.OrderPipe.Base.Business.LogisticLineGroups
{
    public interface ILogisticLineGroupGenerator
    {
        LogisticLineGroup Generate(ILineGroupHeader lineGroupHeader);
        LogisticLineGroup Generate(ILogisticLineGroup logisticLineGroupSource, IEnumerable<Article> articles);
    }
}
