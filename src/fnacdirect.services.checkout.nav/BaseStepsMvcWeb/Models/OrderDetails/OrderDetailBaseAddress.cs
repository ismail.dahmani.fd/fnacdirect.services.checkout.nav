﻿using System.Xml.Serialization;

namespace FnacDirect.OrderPipe.Base.BaseModel.Model.OrderDetails
{
    [XmlInclude(typeof(OrderDetailPostalAddress))]
    [XmlInclude(typeof(OrderDetailRelayAddress))]
    [XmlInclude(typeof(OrderDetailShopAddress))]
    public abstract class OrderDetailBaseAddress
    {
        public string Id { get; set; }

        public string Address { get; set; }

        public string City { get; set; }

        public string Country { get; set; }

        public string ZipCode { get; set; }

        public string Type { get; set; }
    }
}
