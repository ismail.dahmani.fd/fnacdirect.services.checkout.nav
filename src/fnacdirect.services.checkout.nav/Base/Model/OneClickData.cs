﻿namespace FnacDirect.OrderPipe.Base.OneClick
{
    public class OneClickData : GroupData
    {
        public bool? IsAvailable { get; set; }
    }
}
