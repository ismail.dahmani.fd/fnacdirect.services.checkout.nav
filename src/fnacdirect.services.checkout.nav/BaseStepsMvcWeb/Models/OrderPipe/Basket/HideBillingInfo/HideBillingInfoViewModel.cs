namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Basket
{
    public class HideBillingInfoViewModel
    {
        public bool IsApplied { get; set; }
    }
}
