﻿using System;
using System.Collections.Generic;
using FnacDirect.OrderPipe.Base.Business.LogisticLineGroups.Internals.Initializers;
using FnacDirect.OrderPipe.Base.Business.LogisticLineGroups.Internals.InitialSelectors;
using FnacDirect.OrderPipe.Base.Business.LogisticLineGroups.Internals.Unfolders;
using FnacDirect.OrderPipe.Base.Business.LogisticLineGroups.Handlers;
using FnacDirect.OrderPipe.Base.Business.LogisticLineGroups.Internals.Rules;

namespace FnacDirect.OrderPipe.Base.Business.LogisticLineGroups.Rules
{
    public abstract class BaseLogisticLineGroupGenerationRuleDescriptor<TSelector, TInitializer, TUnfolder> : ILogisticLineGroupGenerationRuleDescriptor
        where TSelector : ILineGroupInitialSelector
        where TInitializer : ILogisticLineGroupInitializer
        where TUnfolder : ILogisticLineGroupUnfolder
    {
        public Type Initializer
        {
            get
            {
                return typeof(TInitializer);
            }
        }

        public abstract IEnumerable<Type> LogisticLineGroupGeneratedHandlers { get; }

        public Type Selector
        {
            get
            {
                return typeof(TSelector);
            }
        }

        public abstract string SwitchId { get; }

        public Type Unfolder
        {
            get
            {
                return typeof(TUnfolder);
            }
        }
    }
}
