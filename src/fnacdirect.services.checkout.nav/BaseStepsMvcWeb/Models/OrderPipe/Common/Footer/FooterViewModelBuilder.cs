﻿using FnacDirect.OrderPipe.Base.LineGroups;
using FnacDirect.OrderPipe.Base.Model;
using System.Linq;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Common
{
    public class FooterViewModelBuilder : IFooterViewModelBuilder
    {
        public FooterViewModel Build(PopOrderForm popOrderForm)
        {
            var footerViewModel = new FooterViewModel
            {
                HasFnac = popOrderForm.LineGroups.Any(s => s.Seller.Type == SellerType.Fnac || s.Seller.Type == SellerType.ClickAndCollectOnly),
                HasMarketPlace = popOrderForm.LineGroups.Any(s => s.Seller.Type == SellerType.MarketPlace)
            };

            return footerViewModel;
        }
    }
}
