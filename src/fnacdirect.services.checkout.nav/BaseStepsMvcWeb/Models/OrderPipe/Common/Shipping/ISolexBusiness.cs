using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.OrderPipe.Base.BaseModel;
using FnacDirect.Solex.Shipping.Client.Contract;
using System.Collections.Generic;
using FnacDirect.OrderPipe.Base.Model.LineGroups;
using FnacDirect.OrderPipe.Base.Model.Facets;
using SolexModel = FnacDirect.OrderPipe.Base.Model.Solex;

namespace FnacDirect.OrderPipe.Base.Business.Shipping
{
    public interface ISolexBusiness
    {
        StoreStockInformations GetStoreStockInformations(string refUg, Article article);
        SolexModel.ItemInGroup FindItemInChoice(SolexModel.ShippingChoice choice, Article article);
        IEnumerable<SolexModel.Identifier> GetIdentifiers(ShippingMethodEvaluationItem item, int shippingMethodId);
        IEnumerable<SolexModel.Identifier> GetIdentifiers(IEnumerable<SolexModel.ShippingChoice> choices, int shippingMethodId);
        EvaluateShoppingCartResult EvaluateShoppingCart(IEnumerable<Article> articles, IEnumerable<FnacDirect.Contracts.Online.Model.FreeShipping> freeShippings, ShippingNetworks shippingNetworks, string usageKey);
        decimal SimultateTotalsShippingCosts(IEnumerable<ILineGroup> lineGroups, IGotShippingMethodEvaluation iGotShippingMethodEvaluation, List<Solex.Shipping.Client.Contract.Promotion> shippingPromotionsTriggers, ShippingAddress address, bool excludeAdherentPromotion = true);
    }
}
