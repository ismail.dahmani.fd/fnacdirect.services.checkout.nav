using FnacDirect.OrderPipe.Base.Model.Facets;

namespace FnacDirect.OrderPipe.Base.Business.Payment
{
    public interface IBankPaymentService
    {
        bool IsBankAllowed(IGotLineGroups iGotLineGroups, IGotUserContextInformations iGotUserContextInformations, IGotShippingMethodEvaluation iGotShippingMethodEvaluation);
        bool IsCheckAllowed(IGotBillingInformations iGotBillingInformations, IGotLineGroups iGotLineGroups, IGotUserContextInformations iGotUserContextInformations, IGotShippingMethodEvaluation iGotShippingMethodEvaluation);
    }
}
