using FnacDirect.OrderPipe.Base.Business.LogisticLineGroups.Internals.Initializers;
using FnacDirect.OrderPipe.Base.LineGroups;
using FnacDirect.OrderPipe.Base.Model;
using System.Collections.Generic;
using System.Linq;

namespace FnacDirect.OrderPipe.Base.Business.LogisticLineGroups.Initializers
{
    public class IntramagPTLogisticLineGroupInitializer : BaseLogisticLineGroupInitializer
    {
        private readonly ILogisticLineGroupGenerator _logisticLineGroupGenerator;

        public IntramagPTLogisticLineGroupInitializer(ILogisticLineGroupGenerator logisticLineGroupGenerator)
        {
            _logisticLineGroupGenerator = logisticLineGroupGenerator;
        }

        public override IEnumerable<ILogisticLineGroup> Initialize(IDictionary<ILineGroupHeader, IEnumerable<Article>> selectedLineGroups)
        {
            var shippingMethodEvaluation = Context.IGotShippingMethodEvaluation.ShippingMethodEvaluation;
            var output = new List<ILogisticLineGroup>();

            List<Article> mergedArticles = null;
            ILineGroupHeader mergedLineGroup = null;

            if (selectedLineGroups.Keys?.Any() == true)
            {
                foreach (var lg in selectedLineGroups.Keys)
                {
                    // Initialisation
                    if (mergedLineGroup == null)
                    {
                        mergedLineGroup = lg;
                        mergedArticles = new List<Article>(selectedLineGroups[lg]);
                        continue;
                    }

                    // On met tous les articles dans la même liste
                    mergedArticles.AddRange(selectedLineGroups[lg]);
                    mergedLineGroup.RemainingTotal += lg.RemainingTotal;

                    // On ose espérer que si une de ses valeurs est null, c'est qu'il y a un problème
                    if (mergedLineGroup.GlobalPrices != null && lg.GlobalPrices != null)
                    {
                        mergedLineGroup.GlobalPrices.TotalEcoTaxEur += lg.GlobalPrices.TotalEcoTaxEur;
                        mergedLineGroup.GlobalPrices.TotalEcoTaxEurNoVAT += lg.GlobalPrices.TotalEcoTaxEurNoVAT;
                        mergedLineGroup.GlobalPrices.TotalPriceEur += lg.GlobalPrices.TotalPriceEur;
                        mergedLineGroup.GlobalPrices.TotalShoppingCartBusinessPriceDBEur += lg.GlobalPrices.TotalShoppingCartBusinessPriceDBEur;
                        mergedLineGroup.GlobalPrices.TotalShoppingCartBusinessPriceNoVatEur += lg.GlobalPrices.TotalShoppingCartBusinessPriceNoVatEur;
                        mergedLineGroup.GlobalPrices.TotalShoppingCartBusinessPriceUserEur += lg.GlobalPrices.TotalShoppingCartBusinessPriceUserEur;
                    }
                }
            }

            // Ici, on n'a plus qu'un LineGroup et une liste d'articles
            if (mergedLineGroup != null && mergedArticles?.Any() == true)
            {
                var logisticLineGroup = _logisticLineGroupGenerator.Generate(mergedLineGroup);
                logisticLineGroup.OrderType = OrderTypeEnum.IntraMag;
                logisticLineGroup.Informations.OrderType = (int)OrderInfoOrderTypeEnum.Intramag;
                logisticLineGroup.Articles = mergedArticles;
                mergedLineGroup.Informations.OrderType = logisticLineGroup.Informations.OrderType;

                Helper.CopyShippingMethodEvaluation(mergedLineGroup, logisticLineGroup, shippingMethodEvaluation);

                // Ne devrait pas arriver
                if (logisticLineGroup.SelectedShippingChoice != null)
                {
                    foreach (var article in logisticLineGroup.Articles.OfType<StandardArticle>())
                    {
                        article.ShipMethod = logisticLineGroup.SelectedShippingChoice.MainShippingMethodId;
                    }
                }

                output.Add(logisticLineGroup);
            }

            return output;
        }
    }
}
