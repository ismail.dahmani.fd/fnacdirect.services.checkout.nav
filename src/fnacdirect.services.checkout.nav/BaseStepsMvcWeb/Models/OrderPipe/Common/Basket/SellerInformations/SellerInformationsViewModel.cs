﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Common.Basket
{
    public class SellerInformationsViewModel
    {
        public bool HasManySeller { get; set; }

        public int SellerIndex { get; set; }
        public int TotalSeller { get; set; }

        public bool ShouldDisplayIndex { get; set; }

        private readonly string _name;

        public SellerInformationsViewModel()
            : this("FNAC")
        {
        }

        protected SellerInformationsViewModel(string name)
        {
            _name = name;
        }

        public string Name
        {
            get
            {
                return _name;
            }
        }

        public virtual bool IsFnac
        {
            get
            {
                return true;
            }
        }

        public virtual bool IsClickAndCollectOnly { get; set; }
    }
}
