namespace FnacDirect.OrderPipe.Base.Business
{
    public interface IPersonnelNumberBusiness
    {
        void AddPersonnelNumber(int accountId, int orderId);
    }
}
