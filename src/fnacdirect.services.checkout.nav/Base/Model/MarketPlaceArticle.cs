using FnacDirect.Contracts.Online.Model;
using FnacDirect.OrderPipe.Base.BaseModel.Model.MarketPlace;
using System.Text;
using System.Xml.Serialization;

namespace FnacDirect.OrderPipe.Base.Model
{
    public class MarketPlaceArticle : BaseArticle
    {
        [XmlIgnore]
        public int AvailableQuantity { get; set; }

        public int? FnacAvailabilityId { get; set; }

        public decimal? FnacPrice { get; set; }

        /// <summary>
        /// Indique si l'article est concerné par le retrait chez le vendeur.
        /// </summary>
        public bool? HandDelivery { get; set; }

        [XmlIgnore]
        public MarketPlaceOffer Offer { get; set; }

        public int OfferId { get; set; }

        public decimal? OldOfferPrice { get; set; }

        public int PublicID { get; set; }

        // we don't ignore because we need it to display basket preview message when quantity reserved doesn't match quantity wanted
        public MarketPlaceReservation Reservation { get; set; }

        public bool? SoldByFnac { get; set; }

        /// <summary>
        /// Code postal du vendeur.
        /// </summary>
        public string ZipCodeSeller { get; set; }

        public override ArticleReference ToArticleReference()
            => Offer?.ArticleReference;

        public override string ToString()
        {
            var marketPlace = new StringBuilder();
            marketPlace.AppendLine(base.ToString());
            marketPlace.AppendLine($"OfferId : {OfferId}");
            return marketPlace.ToString();
        }

        public override decimal? D3ETax => Offer?.DeeeTax;

        public override decimal? RCPTax => Offer?.CopyTax;

        public override decimal? DEATax => Offer?.DeaTax;

    }
}
