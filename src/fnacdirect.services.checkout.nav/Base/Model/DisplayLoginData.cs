using FnacDirect.Technical.Framework.CoreServices;

namespace FnacDirect.OrderPipe.Base.Model
{
    public class DisplayLoginData : StepData
    {
        public string ChallengeUrl { get; set; }
        public SerializableDictionary<string, string> Parameters { get; set; }
    }
}
