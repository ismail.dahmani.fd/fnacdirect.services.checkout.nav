using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Basket;
using FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Common.Basket;
using System;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Common.Basket.PipeArticle
{
    public class PipeArticleViewModel : RichBasketItemViewModel
    {
        public string SellerName { get; set; } = string.Empty;

        public PipeArticleViewModel(BaseArticle baseArticle, BasketItemWithPictureViewModel basketItem) : base(basketItem)
        {
            ReferentialId = baseArticle.Referentiel;
            ProductId = baseArticle.ProductID.GetValueOrDefault(0);
            IsEligibleTo5pctReduction = false;

            SetUniqueId(baseArticle);
        }

        private void SetUniqueId(BaseArticle baseArticle)
        {
            if (baseArticle is StandardArticle)
            {
                UniqueId = $"{baseArticle.SellerID.GetValueOrDefault()}-{Guid.Empty}-{baseArticle.ProductID.GetValueOrDefault()}";
                SellerName = "FNAC";
            }
            else if (baseArticle is MarketPlaceArticle mpArticle) //cas MP
            {
                UniqueId = $"{baseArticle.SellerID.GetValueOrDefault()}-{mpArticle.Offer.Reference}-{baseArticle.ProductID.GetValueOrDefault()}";
                SellerName = mpArticle.Offer.Seller?.Company ?? string.Empty;
            }
        }
    }
}
