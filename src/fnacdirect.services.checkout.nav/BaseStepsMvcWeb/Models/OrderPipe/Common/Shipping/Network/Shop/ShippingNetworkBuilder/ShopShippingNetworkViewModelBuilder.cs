using FnacDirect.FnacStore.Model;
using FnacDirect.IdentityImpersonation;
using FnacDirect.OrderPipe.Base.BaseModel;
using FnacDirect.OrderPipe.Base.Business.Availability;
using FnacDirect.OrderPipe.Base.Business.Operations;
using FnacDirect.OrderPipe.Base.Business.Shipping;
using FnacDirect.OrderPipe.Base.Business.Shipping.Shop;
using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.OrderPipe.Base.Model.LineGroups;
using FnacDirect.OrderPipe.Base.Model.Pop;
using FnacDirect.OrderPipe.Base.Proxy.Customer;
using FnacDirect.OrderPipe.Base.Steps.Pop;
using FnacDirect.OrderPipe.BaseMvc.Web.Services.ShipToStore;
using FnacDirect.Technical.Framework;
using FnacDirect.Technical.Framework.Web.Mvc;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Common.Shipping.Shop
{
    public class ShopShippingNetworkViewModelBuilder : IShopShippingNetworkViewModelBuilder
    {
        private readonly IStoreSearchService _storeSearchService;
        private readonly IApplicationContext _applicationContext;
        private readonly ICustomerFavoriteStoreService _customerFavoriteStoreService;
        private readonly IShipToStoreAvailabilityService _shipToStoreAvailabilityService;
        private readonly IPopAvailabilityProcessor _popAvailabilityProcessor;
        private readonly ISwitchProvider _switchProvider;
        private readonly IStoreAddressBookService _storeAddressBookService;
        private readonly IShippingChoiceCostService _shippingChoiceCostService;

        private const int ShopSuggestionsCount = 1;
        private readonly IList<int> _shippingMethodIds;
        private readonly bool _hideLongEstimatedDle;

        public ShopShippingNetworkViewModelBuilder(IStoreSearchService storeSearchService,
                                                   IApplicationContext applicationContext,
                                                   ICustomerFavoriteStoreService customerFavoriteStoreService,
                                                   IShipToStoreAvailabilityService shipToStoreAvailabilityService,
                                                   IPopAvailabilityProcessor popAvailabilityProcessor,
                                                   IStoreAddressBookService storeAddressBookService,
                                                   ISwitchProvider switchProvider,
                                                   IShippingChoiceCostService shippingChoiceCostService
                                                   )
        {
            _storeSearchService = storeSearchService;
            _applicationContext = applicationContext;
            _customerFavoriteStoreService = customerFavoriteStoreService;
            _shipToStoreAvailabilityService = shipToStoreAvailabilityService;
            _popAvailabilityProcessor = popAvailabilityProcessor;
            _switchProvider = switchProvider;
            _storeAddressBookService = storeAddressBookService;
            _shippingMethodIds = GetListShippingMethodId_NDD();
            _shippingChoiceCostService = shippingChoiceCostService;
            _hideLongEstimatedDle = _switchProvider.IsEnabled("front.pop.shipping.hidelongestimateddle");
        }

        public List<int> GetListShippingMethodId_NDD()
        {

            var lstShippingMethodIds = new List<int>();

            var lstIds = _applicationContext.GetAppSetting("shippingmethodid.nextdaydelivery").Split(',');

            foreach (var shippingId in lstIds)
            {
                if (int.TryParse(shippingId, out var iId))
                    lstShippingMethodIds.Add(iId);
            }

            return lstShippingMethodIds;
        }

        public string ComputeShippingCost(decimal shippingCost)
        {
            return _applicationContext.GetPriceHelper().Format(shippingCost, PriceFormat);
        }

        public ShopDetailsViewModel BuildDetails(FnacStore.Model.FnacStore fnacStore, decimal shippingCost, int? favoriteStoreId)
        {
            return BuildDetails(fnacStore, shippingCost, favoriteStoreId, null);
        }


        public List<TimetableViewModel> GetTimeTables(int storeId)
        {
            var fnacStore = _storeSearchService.GetStore(storeId);
            return fnacStore.Timetables.ConvertToTimetableViewModel(_applicationContext).ToList();
        }

        public IEnumerable<ExceptionnalClosingViewModel> GetExceptionnalTimetables(int storeId)
        {
            var fnacStore = _storeSearchService.GetStore(storeId);
            var culture = CultureInfo.GetCultureInfo(_applicationContext.GetLocale());

            var exceptionnalClosing = fnacStore.TimetableExceptionnals
                .Where(t => !t.IsOpened)
                .Select(t => new ExceptionnalClosingViewModel()
                {
                    Day = t.Day.ToString(culture.DateTimeFormat.LongDatePattern)
                });

            var exceptionnalOpening = fnacStore.TimetableExceptionnals
                .Where(t => t.IsOpened)
                .Select(t => new ExceptionnalOpeningViewModel()
                {
                    Day = t.Day.ToString(culture.DateTimeFormat.LongDatePattern),
                    Comment = t.Comment,
                    Openings =
                        ExceptionnalOpeningViewModel.GetOpenings(t.OpeningPeriods,
                            _applicationContext.GetUiResourceService(),
                            _applicationContext.GetLocale(),
                            isDrive: fnacStore.IsDrive)
                }).ToList();
            return exceptionnalOpening.Union(exceptionnalClosing).ToList();
        }

        public ShopDetailsViewModel BuildDetails(FnacStore.Model.FnacStore fnacStore, decimal shippingCost, int? favoriteStoreId, Position referenceGeoPoint)
        {
            // On construit les horaires du magasin
            var timetables = GetTimeTables(fnacStore.Id);

            var geoLocalizationAddress = fnacStore.GeoLocalizationAddress.IndexOf(",", StringComparison.Ordinal);

            var latitude = double.Parse(fnacStore.GeoLocalizationAddress.Substring(1, geoLocalizationAddress - 1), CultureInfo.InvariantCulture);
            var longitude = double.Parse(fnacStore.GeoLocalizationAddress.Substring(geoLocalizationAddress + 1, fnacStore.GeoLocalizationAddress.Length - geoLocalizationAddress - 2), CultureInfo.InvariantCulture);

            var openingMessage = fnacStore.GetOpeningMessage(DateTime.Now);
            var displayDateOfDelivery1H = GetDisplayingDateDelivery1H(fnacStore);

            var shopDetailsViewModel = new ShopDetailsViewModel(fnacStore)
            {
                ShortTimetable = openingMessage.GetMessage(_applicationContext.GetUiResourceService(), _applicationContext.GetLocale()),
                IsOpen = (openingMessage is Opened),
                ShippingCost = ComputeShippingCost(shippingCost),
                IsFreeShipping = shippingCost == 0M,
                Timetables = timetables,
                ExceptionnalTimetables = GetExceptionnalTimetables(fnacStore.Id),
                Latitude = latitude,
                Longitude = longitude,
                IsFavorite = favoriteStoreId.HasValue && favoriteStoreId.Value == fnacStore.Id,
                DisplayDateOfDelivery1H = displayDateOfDelivery1H,
                RefUG = fnacStore.RefUG
            };

            ActivateFeatures(shopDetailsViewModel);

            if (referenceGeoPoint != null)
            {
                var shopGeoPoint = new Position(latitude, longitude);

                shopDetailsViewModel.Distance = FormatDistance((decimal)referenceGeoPoint.GetDistanceFrom(shopGeoPoint));
            }

            return shopDetailsViewModel;
        }

        private string GetDisplayingDateDelivery1H(FnacStore.Model.FnacStore fnacStore)
        {
            var displayDateOfDelivery1H = string.Empty;
            var deliveryDateOfWeek = fnacStore.GetDisplaying1HDateDelivery();
            if (deliveryDateOfWeek >= 0)
            {
                if (deliveryDateOfWeek == (int)DateTime.Now.DayOfWeek)
                {
                    if (_switchProvider.IsEnabled("orderpipe.pop.socialdistancing"))
                    {
                        displayDateOfDelivery1H = _applicationContext.GetMessage("orderpipe.pop.shipping.availability.clickandcollect.1h", null);
                    }
                    else
                    {
                        displayDateOfDelivery1H = _applicationContext.GetMessage("orderpipe.pop.shipping.availability.clickandcollect.Today", null);
                    }
                }
                else
                {
                    displayDateOfDelivery1H = _applicationContext.GetMessage("orderpipe.pop.shipping.store.timetable.dayofweek." + deliveryDateOfWeek, null);
                }
            }
            return _applicationContext.GetMessage("orderpipe.pop.shipping.availability.clickandcollect.1HDeliveryDateMessage", displayDateOfDelivery1H);
        }

        public DateTime? ComputeWorstDeliveryDate(int shopId, ShopShippingMethodEvaluationItem shopShippingMethodEvaluationItem, IEnumerable<ILineGroup> lineGroups, IEnumerable<int> excludedLogisticTypes, IdentityImpersonator identityImpersonator)
        {
            IEnumerable<ShipToStoreAvailability> availabilities;
            if (_switchProvider.IsEnabled("orderpipe.pop.fdv.enable", false) && shopShippingMethodEvaluationItem.SelectedShippingMethodId.HasValue && shopShippingMethodEvaluationItem.HasChoices)
            {
                //pour fnacshop nous navons pas besoin de rappeler solex
                availabilities = _shipToStoreAvailabilityService.GetAvailabilitiesForShop(lineGroups.SelectMany(l => l.Articles), shopShippingMethodEvaluationItem, excludedLogisticTypes).ToList();
            }
            else
            {
                var fnacStore = _storeSearchService.GetStore(shopId);
                availabilities = _shipToStoreAvailabilityService.GetAvailabilities(fnacStore, lineGroups.SelectMany(l => l.Articles), excludedLogisticTypes, identityImpersonator).ToList();
            }

            if (availabilities.Any() && availabilities.All(a => (a is ClickAndCollectShipToStoreAvailability)))
            {
                return DateTime.Now;
            }

            var matchingAvailabilities = availabilities.OfType<ShippingShipToStoreAvailability>().ToList();

            if (matchingAvailabilities.Any() && matchingAvailabilities.Any(a => a.DeliveryDayMax.HasValue))
            {
                return matchingAvailabilities.Where(a => a.DeliveryDayMax.HasValue).Max(a => a.DeliveryDayMax);
            }

            if (matchingAvailabilities.Any() && matchingAvailabilities.Any(a => a.DeliveryDayMin.HasValue))
            {
                return matchingAvailabilities.Where(a => a.DeliveryDayMin.HasValue).Max(a => a.DeliveryDayMin);
            }

            return null;
        }

        private string FormatDistance(decimal distance)
        {
            if ((int)(distance * 1000) == 0)
            {
                return null;
            }

            return distance < 1
                   ? (string.Concat("{distance} ", _applicationContext.GetMessage("orderpipe.index.shiptostore.meter", null))).Replace("{distance}", ((int)(distance * 1000)).ToString(CultureInfo.InvariantCulture))
                   : (string.Concat("{distance} ", _applicationContext.GetMessage("orderpipe.index.shiptostore.km", null))).Replace("{distance}", distance.ToString("##.##"));
        }

        /// <summary>
        /// Construction du view model à utiliser pour générer les vues de livraison
        /// </summary>
        public ShopShippingNetworkViewModel Build(PopOrderForm popOrderForm, int sellerId)
        {
            return Build(popOrderForm, sellerId, null);
        }

        public ShopShippingNetworkViewModel Build(PopOrderForm popOrderForm, int sellerId, Position referenceGeoPoint)
        {
            var christmasData = popOrderForm.GetPrivateData<ChristmasData>(false);

            var maybeShippingMethodEvaluationGroup = popOrderForm.ShippingMethodEvaluation.GetShippingMethodEvaluationGroup(sellerId);

            var shippingMethodEvaluationGroup = maybeShippingMethodEvaluationGroup.Value;

            var shopShippingNetworkViewModel = new ShopShippingNetworkViewModel
            {
                CurrentMode = shippingMethodEvaluationGroup.SelectedShippingMethodEvaluationType == ShippingMethodEvaluationType.Shop,
                GeoAutoSelectionEnabled = ComputeGeoAutoSelection(popOrderForm)
            };

            var shopShippingMethodEvaluationItem = shippingMethodEvaluationGroup.ShopShippingMethodEvaluationItem.Value;

            var isGuest = popOrderForm.UserContextInformations.IsGuest;

            var favoriteStoreId = _customerFavoriteStoreService.GetFavoriteStoreId();

            var shippingCost = _shippingChoiceCostService.CalculateShippingCost(shopShippingMethodEvaluationItem.Choices, shopShippingMethodEvaluationItem.SelectedShippingMethodId);

            var shippingCostFormat = ComputeShippingCost(shippingCost);

            var isFreeShipping = shippingCost == 0M;

            var lineGroups = popOrderForm.LineGroups.Seller(sellerId);
            var excludedLogisticTypes = popOrderForm.ShippingMethodEvaluation.ExcludedLogisticTypes;

            var shippingChoice = popOrderForm.GetMainShippingChoice();

            if (shopShippingMethodEvaluationItem.IsDummyAddress
                || shopShippingMethodEvaluationItem.ShippingAddress == null
                || !shopShippingMethodEvaluationItem.ShippingAddress.Identity.HasValue)
            {
                // Pas de magasin valide sélectionné

                if (christmasData != null && christmasData.HasCurrentPhase)
                {
                    //Check if we have at least an elligible article (catalog, allowed availabilities, IsTechnicalOnly...)
                    var hasElligibleArticles = lineGroups.GetArticles().All(a => christmasData.GetShippingBeforChristmasEligibility(a.ProductID.GetValueOrDefault()));

                    shopShippingNetworkViewModel.HasMarketingEvent = hasElligibleArticles;

                    shopShippingNetworkViewModel.MarketingEventPicto = christmasData.PictoUiresourceKey != null ? _applicationContext.GetMessage(christmasData.PictoUiresourceKey) : string.Empty;
                }

                if (isGuest)
                {
                    // Création de compte sans magasin sélectionné -> fin de traitement
                    return shopShippingNetworkViewModel;
                }
            }
            else
            {
                // Cas où un magasin valide a été selectionné

                var fnacStore = _storeSearchService.GetStore(shopShippingMethodEvaluationItem.ShippingAddress.Identity.Value);

                var shopDetailsViewModel = BuildDetails(fnacStore, shippingCost, favoriteStoreId, referenceGeoPoint);

                ApplyAvailabilities(popOrderForm, shopDetailsViewModel, shopShippingMethodEvaluationItem, lineGroups, popOrderForm.ShippingMethodEvaluation.ExcludedLogisticTypes);

                shopShippingNetworkViewModel.Selected = shopDetailsViewModel;
                if (shopShippingNetworkViewModel.Selected.HasValue)
                {
                    shopShippingNetworkViewModel.Selected.Value.CarbonFootprint = shippingChoice?.CarbonFootprint;
                }

                var dateToDeliver = GetDisplayingDateDelivery1H(fnacStore);
                var shopViewModel = new ShopViewModel(fnacStore)
                {
                    ShippingCost = shippingCostFormat,
                    IsFreeShipping = isFreeShipping,
                    IsSelected = shopShippingNetworkViewModel.CurrentMode,
                    IsFavorite = favoriteStoreId.HasValue && favoriteStoreId.Value == fnacStore.Id,
                    DisplayDateOfDelivery1H = dateToDeliver,
                    CarbonFootprint = shippingChoice?.CarbonFootprint,
                };

                ActivateFeatures(shopViewModel);

                ApplyAvailabilities(popOrderForm, shopViewModel, shopShippingMethodEvaluationItem, lineGroups, excludedLogisticTypes);

                shopShippingNetworkViewModel.With(shopViewModel);

                if (christmasData != null && christmasData.HasCurrentPhase)
                {
                    shopShippingNetworkViewModel.HasMarketingEvent = shopViewModel.BeforeXmas;

                    shopShippingNetworkViewModel.MarketingEventPicto = christmasData.PictoUiresourceKey != null ? _applicationContext.GetMessage(christmasData.PictoUiresourceKey) : string.Empty;
                }
            }

            if (!shopShippingNetworkViewModel.Shops.Any())
            {
                var stores = _storeAddressBookService.GetAddresses();

                if (!stores.Any() && favoriteStoreId.HasValue)
                {
                    var fnacStore = _storeSearchService.GetStore(favoriteStoreId.Value);
                    if (fnacStore != null)
                    {
                        stores = _storeSearchService.GetStoreNearStore(fnacStore).Select(s => s.FnacStore);
                    }
                }

                foreach (var store in stores.Take(ShopSuggestionsCount))
                {
                    var shopViewModel = new ShopViewModel(store)
                    {
                        ShippingCost = shippingCostFormat,
                        IsFavorite = favoriteStoreId.HasValue && favoriteStoreId.Value == store.Id,
                        CarbonFootprint = shippingChoice?.CarbonFootprint,
                    };

                    ActivateFeatures(shopViewModel);

                    ApplyAvailabilities(popOrderForm,
                                        shopViewModel,
                                        shopShippingMethodEvaluationItem,
                                        lineGroups,
                                        excludedLogisticTypes);

                    shopShippingNetworkViewModel.With(shopViewModel);
                }

                if (shopShippingNetworkViewModel.Selected.HasValue && shopShippingNetworkViewModel.CurrentMode)
                {
                    var matchingShopViewModel = shopShippingNetworkViewModel.Shops.FirstOrDefault(s => s.Id == shopShippingNetworkViewModel.Selected.Value.Id);

                    if (matchingShopViewModel != null)
                    {
                        matchingShopViewModel.IsSelected = true;
                    }
                }
            }

            foreach (var shop in shopShippingNetworkViewModel.Shops)
            {
                shop.WorstDeliveryDate = ComputeWorstDeliveryDate(shop.Id,
                                                                   shopShippingMethodEvaluationItem,
                                                                   lineGroups,
                                                                   excludedLogisticTypes,
                                                                   popOrderForm.UserContextInformations.IdentityImpersonator);

                if (shopShippingNetworkViewModel.Selected.HasValue)
                {
                    shopShippingNetworkViewModel.Selected.Value.WorstDeliveryDate = ComputeWorstDeliveryDate(shop.Id,
                                                                       shopShippingMethodEvaluationItem,
                                                                       lineGroups,
                                                                       excludedLogisticTypes,
                                                                   popOrderForm.UserContextInformations.IdentityImpersonator);
                }
            }

            if (shopShippingNetworkViewModel.Selected.HasValue)
            {
                var shopDetailsViewModel = shopShippingNetworkViewModel.Selected.Value;
                shopDetailsViewModel.Summary = ComputeSummary(shopDetailsViewModel,
                                                              lineGroups,
                                                              excludedLogisticTypes,
                                                              shopShippingMethodEvaluationItem);
            }

            return shopShippingNetworkViewModel;
        }

        public bool ComputeGeoAutoSelection(PopOrderForm popOrderForm)
        {
            var popData = popOrderForm.GetPrivateData<PopData>(create: false);

            if (popData == null)
            {
                return false;
            }

            var popSellersData = popOrderForm.GetPrivateData<PopShippingData>();

            var currentSeller = popSellersData.GetCurrentShippingSellerData();

            var shippingChoiceValidated = currentSeller != null && currentSeller.ShippingChoiceValidated;

            return popData.GeoAutoSelectionEnabled && !popData.ShippingAddressSetByUser && !shippingChoiceValidated && !popOrderForm.ShippingMethodEvaluation.ContainsArticlesForCollectInStore.GetValueOrDefault();
        }

        private SelectedShippingItemSummary ComputeSummary(ShopDetailsViewModel shopDetailsViewModel,
                                                           IEnumerable<ILineGroup> lineGroups,
                                                           IEnumerable<int> excludedLogisticTypes,
                                                           ShopShippingMethodEvaluationItem shopShippingMethodEvaluationItem)
        {
            var summary = new SelectedShippingItemSummary()
            {
                Label = _applicationContext.GetMessage("orderpipe.pop.oneclick.shipping.shop.prefix")
            };
            var timeNdd = _applicationContext.GetMessage("orderpipe.pop.shipping.availability.prefix.shop.14H");

            var shippingMethodId = shopShippingMethodEvaluationItem.GetDefaultShippingMethodIdFromChoices() ?? 0;

            if (shopDetailsViewModel.DisplayLongEstimatedDeliveryDates)
            {
                summary.Label += _applicationContext.GetMessage("orderpipe.pop.oneclick.shipping.shop.complex");
                summary.DisplayLongEstimatedDeliveryDates = true;
                summary.LongEstimatedDeliveryDate = shopDetailsViewModel.LongEstimatedDeliveryDate;
            }

            if (_switchProvider.IsEnabled("orderpipe.pop.socialdistancing") && (shopDetailsViewModel.FullClickAndCollect || shopDetailsViewModel.AnyClickAndCollect))
            {
                summary.ShortEstimatedDeliveryDate = _applicationContext.GetMessage("orderpipe.pop.shipping.availability.clickandcollect.1h");
            }
            else if(shopDetailsViewModel.FullClickAndCollect)
            {
                summary.ShortEstimatedDeliveryDate = _applicationContext.GetMessage("orderpipe.pop.oneclick.shipping.shop.1H");
            }
            else if (shopDetailsViewModel.AnyClickAndCollect)
            {
                summary.ShortEstimatedDeliveryDate = _applicationContext.GetMessage("orderpipe.pop.oneclick.shipping.shop.starting.1H");
            }
            else
            {
                var articles = lineGroups.GetArticles();
                if (!_switchProvider.IsEnabled("orderpipe.pop.shipping.dlemp.enable"))
                {
                    articles = articles.Where(a => a is StandardArticle);
                }
                var popAvailabilityProcessorInputs = new PopAvailabilityProcessorInputListBuilder(articles, shippingMethodId)
                    .ExcludeLogisticTypes(excludedLogisticTypes)
                    .FilterArticles((article) => (!(article is StandardArticle standardArticle) || !standardArticle.IsEbook))
                    .BuildList();
                var shortGlobalEstimatedDeliveryDate = _popAvailabilityProcessor.GetShortGlobalEstimatedDeliveryDate(shippingMethodId, popAvailabilityProcessorInputs);

                summary.ShortEstimatedDeliveryDate = shortGlobalEstimatedDeliveryDate;
            }

            summary.ShippingCost = shopDetailsViewModel.ShippingCost;

            if (_shippingMethodIds.Contains(shippingMethodId))
            {
                summary.ShortEstimatedDeliveryDate += timeNdd;
            }

            return summary;
        }

        public void ApplyAvailabilities(PopOrderForm popOrderForm, ShopViewModel shopViewModel, ShopShippingMethodEvaluationItem shopShippingMethodEvaluationItem, IEnumerable<ILineGroup> lineGroups, IEnumerable<int> excludedLogisticTypes)
        {
            if (_switchProvider.IsEnabled("orderpipe.pop.meteor.availabilities") && _switchProvider.IsEnabled("orderpipe.pop.meteor.availabilities.shop"))
            {
                var popData = popOrderForm.GetPrivateData<PopData>(create: false);

                var fnacStore = _storeSearchService.GetStore(shopViewModel.Id);

                var availabilities = _shipToStoreAvailabilityService.GetAvailabilities(fnacStore, lineGroups.SelectMany(l => l.Articles), excludedLogisticTypes, popOrderForm.UserContextInformations.IdentityImpersonator).ToList();

                if (availabilities.OfType<ClickAndCollectShipToStoreAvailability>().Count() == availabilities.Count)
                {
                    shopViewModel.FullClickAndCollect = true;
                }

                if (availabilities.OfType<ClickAndCollectShipToStoreAvailability>().Any())
                {
                    shopViewModel.AnyClickAndCollect = true;
                }

                if (fnacStore != null)
                {
                    shopViewModel.IsDrive = fnacStore.IsDrive;
                }

                var shippingMethodId = shopShippingMethodEvaluationItem.GetDefaultShippingMethodId().FirstOrDefault();

                var shortGlobalEstimatedDeliveryDate = _popAvailabilityProcessor.GetShortGlobalEstimatedDeliveryDateForStore(lineGroups.SelectMany(l => l.Articles), availabilities, shippingMethodId, shopViewModel.Id).Where(i => !string.IsNullOrEmpty(i)).ToList();

                //SET EXCLUDEIFSINGLE TO FALSE SO THA WE COULD GET LE AVAILABILITIES WHAT WE USE TO COMPUTE THE FLAG ON THE SHIPPING MODEL

                var longGlobalEstimatedDeliveryDate = _popAvailabilityProcessor.GetLongGlobalEstimatedDeliveryDateForStore(lineGroups.SelectMany(l => l.Articles), availabilities, shippingMethodId, shopViewModel.Id, false);

                var shippingAvailabilities = availabilities.Select(a => a as ShippingShipToStoreAvailability);

                if (shippingAvailabilities.Any())
                {
                    var deliveryDateMin = shippingAvailabilities.Min(sa => sa?.DeliveryDayMin);

                    if (deliveryDateMin.HasValue && _hideLongEstimatedDle)
                    {
                        var diff = deliveryDateMin.Value - DateTime.Now;
                        if (diff.TotalHours > 360)
                        {
                            shortGlobalEstimatedDeliveryDate = new List<string>();
                        }
                    }
                }

                shopViewModel.ShortEstimatedDeliveryDate = shortGlobalEstimatedDeliveryDate;

                shopViewModel.LongEstimatedDeliveryDate = longGlobalEstimatedDeliveryDate;

                shopViewModel.DisplayLongEstimatedDeliveryDates = longGlobalEstimatedDeliveryDate.Count() > 1 && (popData == null || !popData.HasExcludedAvailabilitiesForDeliveryDates);
            }
        }

        private void ActivateFeatures(ShopViewModel shopViewModel)
        {
            shopViewModel.IsGreenDeliveryFeatureActive = _switchProvider.IsEnabled("orderpipe.pop.shipping.greendelivery");
        }

        protected virtual PriceFormat PriceFormat { get { return PriceFormat.CurrencyAsDecimalSeparator; } }

    }
}
