using FnacDirect.OrderPipe.Base.LineGroups;
using System.Collections.Generic;

namespace FnacDirect.OrderPipe.Base.Business.LogisticLineGroups.Internals.Rules
{
    public interface ILogisticLineGroupGenerationRule
    {
        IEnumerable<ILogisticLineGroup> Apply(ILogisticLineGroupGenerationProcessContext logisticLineGroupGenerationProcessContext);

        IEnumerable<ILogisticLineGroup> SimulateInitialize(ILogisticLineGroupGenerationProcessContext logisticLineGroupGenerationProcessContext);
    }
}
