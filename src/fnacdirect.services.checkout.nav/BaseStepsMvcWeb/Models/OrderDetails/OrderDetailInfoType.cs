namespace FnacDirect.OrderPipe.Base.Model
{
    // cf la table tbl_OrderDetailInfoType
    public enum OrderDetailInfoType : int
    {
        ServiceDomicile = 1,
        MarketOffre = 2,
        TheoreticalWorkShop = 3,
        TheoreticalShippingValue = 4,
        CommercialPriority = 5,
        ReservationWorkshop = 6,
        Capacity = 7,
        FndRef = 8,
        FndRefMag = 9,
        MarketOfferOrderStatusLabel = 10,
        MetaCatalogShortDescription = 11,
        PreOrderFlag = 12,
        PreOrderOldPrice = 13,
        PreOrderHigherPrice = 14,
        PreOrderCancelTimer = 15,
        SubscriptionCard = 16,
        PricerResponse = 17,
        QuanityInStoreShelf = 18,
        QuanityInStoreStock = 19,
        RcpTaxe = 20,
        PridConversion = 21,
        PrixConversion = 22,
        SlowMotion = 23,
        RecruitingCardNumber = 24,
        POSACode128 = 25,
        ServiceContractNumber = 26,
        LongTermRentalContractNumber = 27,
        OpenCellSubscriptionNumber = 28,
        Hubside = 29,
        CANAL = 30,
        Engie = 31,
        FREE = 32,
        Deezer = 33,
        Nespresso = 34,
        SouscriptionGP = 35,
        PriceId = 36
    }
}
