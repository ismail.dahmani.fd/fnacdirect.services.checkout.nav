﻿using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.Technical.Framework.Web;
using FnacDirect.Technical.Framework.Web.UrlMgr;
using System.Collections.Generic;
using System.Web.Mvc;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Controllers.Pop
{
    public class PopBackPreviewController : PopBaseController
    {
        private readonly IUrlManager _iUrlManager;

        public PopBackPreviewController(IUrlManager iUrlManager)
        {
            _iUrlManager = iUrlManager;
        }

        [HttpGet]
        public ViewResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Index(string i)
        {
            return Continue();
        }

    }
}
