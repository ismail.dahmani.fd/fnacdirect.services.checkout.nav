using System;
using System.Linq;
using System.Web.Mvc;
using FnacDirect.Context;
using FnacDirect.OrderPipe.Core;
using FnacDirect.OrderPipe.CoreRouting;
using FnacDirect.OrderPipe.Config;
using System.Web.Routing;
using FnacDirect.Technical.Framework.Web.Mvc;
using FnacDirect.Technical.Framework.Web.UrlMgr;
using System.Web;
using FnacDirect.OrderPipe.Base.BaseModel.Model;
using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.Technical.Framework.Utilities;
using System.Collections.Generic;
using FnacDirect.OrderPipe.Mvc.Routing;
using FnacDirect.Technical.Framework.Web.Mvc.Nustache;

namespace FnacDirect.OrderPipe.CoreMvcWeb.Routing
{
    public abstract class OrderPipeController<T1, T2> : ViewEngineSpecificController<T1, T2>
        where T1 : RazorViewEngine
        where T2 : NustacheViewEngine
    {
        private readonly Lazy<OPContext> _opContext;

        public MvcUiDescriptor MvcUiDescriptor { get; private set; }
        public WebUIPipeDescriptor WebUiPipeDescriptor { get; private set; }

        protected string PipeName { get; private set; }

        protected OrderPipeController()
        {
            _opContext = new Lazy<OPContext>(() => DependencyResolver.Current.GetService<OPContext>());
        }

        protected void Rewind()
        {
            var opContext = GetOpContext();

            if (opContext == null)
                return;

            opContext.Pipe.Rewind(opContext, false);
        }

        protected ActionResult RewindAndRedirect()
        {
            var opContext = GetOpContext();

            if (opContext == null)
                return null;

            var pipeResult = opContext.Pipe.Rewind(opContext, false);

            return ApplyResultAndDoRedirectionFor(pipeResult, opContext);
        }

        private ActionResult ApplyResultAndDoRedirectionFor(PipeResult pipeResult, OPContext opContext)
        {
            opContext.CurrentStep = pipeResult.CurrentStep;

            if (pipeResult.UIDescriptor.RedirectMode == RedirectMode.Transfert)
            {
                var mvcUiDescriptor = pipeResult.UIDescriptor as MvcUiDescriptor;

                return new TransferResult(mvcUiDescriptor.Controller, mvcUiDescriptor.Action);
            }
            else
            {
                return DoRedirectionFor(pipeResult, opContext);
            }
        }

        private ActionResult DoRedirectionFor(PipeResult pipeResult, OPContext opContext)
        {
            var orderPipeStepRedirectionStrategyLocator =
             DependencyResolver.Current.GetService<IOrderPipeStepRedirectionStrategyLocator>();

            var orderPipeStepRedirectionStrategy =
                orderPipeStepRedirectionStrategyLocator.GetOrderPipeStepRedirectionStrategyFor(pipeResult.UIDescriptor);

            var url = orderPipeStepRedirectionStrategy.DoRedirectionFor(pipeResult.UIDescriptor, opContext);

            if (this.Response.IsRequestBeingRedirected)
            {
                //redirection has been done in orderPipeStepRedirectionStrategy
                return new EmptyResult();
            }
            else
            {
                return Redirect(url);
            }
        }

        protected ActionResult Reset()
        {
            var orderPipeResetStrategyLocator = DependencyResolver.Current.GetService<IOrderPipeResetStrategyLocator>();

            var opContext = _opContext.Value;

            var orderPipeResetStrategy = orderPipeResetStrategyLocator.GetOrderPipePipeRedirectionStrategyFor(opContext);

            orderPipeResetStrategy.DoResetFor(opContext, true);

            return new EmptyResult();
        }

        protected ActionResult Continue()
        {
            var opContext = GetOpContext();

            if (opContext == null)
                return null;

            var pipeResult = opContext.Pipe.Continue(opContext);

            return ApplyResultAndDoRedirectionFor(pipeResult, opContext);
        }

        protected OPContext GetOpContext()
        {
            return _opContext.Value;
        }

        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            var response = filterContext.HttpContext.Response;

            response.Headers.Remove("Cache-Control");
            if (!response.Headers.AllKeys.Contains("X-Robots-Tag"))
            {
                response.AddHeader("X-Robots-Tag", "noindex");
            }
            response.Cache.AppendCacheExtension("no-store, must-revalidate");

            var opContext = GetOpContext();

            if (opContext != null)
            {
                if (opContext.CurrentStep == null)
                {
                    var userContextProvider = DependencyResolver.Current.GetService<IUserContextProvider>();

                    var userContext = userContextProvider.GetCurrentUserContext();

                    opContext.UID = userContext.GetUserGuid().ToString();

                    var pipeResult = opContext.Pipe.Run(opContext, null, false);

                    opContext.CurrentStep = pipeResult.CurrentStep;
                }

                MvcUiDescriptor = opContext.CurrentStep.UIDescriptor as MvcUiDescriptor;

                if (opContext.Pipe != null)
                {
                    if (opContext.Pipe.UIDescriptor != null)
                    {
                        WebUiPipeDescriptor = opContext.Pipe.UIDescriptor as WebUIPipeDescriptor;
                    }

                    PipeName = opContext.Pipe.PipeName;
                }
            }

            base.OnActionExecuting(filterContext);
        }

        protected ActionResult ToLoginStep()
        {
            var uiDescriptor = WebUiPipeDescriptor;

            var loginStepName = uiDescriptor.LoginStep;

            var url = new UrlWriter(uiDescriptor.DefaultPageUrl).WithParam(UrlWriterParameter.CreateParameter("step", loginStepName));
            url.WithParam("pipe", PipeName).WithBaseUrlParameters();

            return new HttpUnauthorizedResult(url.ToString());
        }
    }

    public abstract class OrderPipeController<T1> : OrderPipeController<T1, DummyNustacheViewEngine>
        where T1 : RazorViewEngine
    {
    }

    public abstract class OrderPipeController : OrderPipeController<OrderPipeRazorViewEngine, DummyNustacheViewEngine>
        //ViewEngineSpecificController<OrderPipeRazorViewEngine, OrderPipeNustacheViewEngine>
    {
    }

    public class DummyNustacheViewEngine : NustacheViewEngine, IDummyViewEngine
    {
    }
}
