using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.OrderPipe.Base.DAL;
using FnacDirect.Contracts.Online.Model;

namespace FnacDirect.OrderPipe.Base.Business
{
    public class NumericalBusiness : INumericalBusiness
    {
        private readonly ICatalogDAL _catalogDal;

        public NumericalBusiness(ICatalogDAL catalogDal)
        {
            _catalogDal = catalogDal;
        }

        public void GetNumericalAddOn(StandardArticle art, SiteContext siteContext)
        {
            _catalogDal.GetNumericalAddOn(art, siteContext);
        }

        public bool IsAuthorizedToSell(int countryId, int marketId, int retailerId)
        {
            return _catalogDal.GetRetailerMarketCountry(countryId, marketId, retailerId);
        }
    }
}