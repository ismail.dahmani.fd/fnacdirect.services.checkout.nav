using System;
using System.ComponentModel.DataAnnotations;
using FnacDirect.Contracts.Online.Model;
using FnacDirect.Customer.Model;
using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.OrderPipe.Base.Model.Facets;
using FnacDirect.OrderPipe.Config;

namespace FnacDirect.OrderPipe.Base.Business.Payment
{
    public interface IPaymentSelectionBusiness
    {
        (bool IsSuccess, UserMessage ErrorMessage) OgoneSelection(int selectedCreditCardId, PopOrderForm popOrderForm, ParameterList globalParameters);
        Tuple<bool, UserMessage> OgoneSelection(string selectedCreditCardReference, int selectedCreditCardId, bool registerCreditCard,
                                                    IGotBillingInformations _gotBillingInformations, IGotUserContextInformations _gotUserContextInformations,
                                                    ParameterList _globalParameters);
        Tuple<bool, UserMessage> OgoneSelection(string selectedCreditCardReference, int selectedCreditCardId, bool registerCreditCard, bool registerCreditCardOnTR, bool registerCreditCardOnCheck, int? cgvVersion,
                                                    IGotBillingInformations _gotBillingInformations, IGotUserContextInformations _gotUserContextInformations,
                                                    ParameterList _globalParameters);
        Tuple<bool, UserMessage> OgoneSelectionAndHandleCreditCard(string selectedCreditCardReference, int selectedCreditCardId, bool registerCreditCard,
                                                    IGotBillingInformations gotBillingInformations, IGotUserContextInformations gotUserContextInformations,
                                                    IGotLogisticLineGroups gotLogisticLineGroups, ParameterList _globalParameters);
        bool SipsSelection(CreditCardEntity oCcd, string creditId, IGotBillingInformations gotBillingInformations, IGotUserContextInformations gotUserContextInformations);

        bool SipsByPhoneSelection(string userId, IGotBillingInformations gotBillingInformations);

        bool LimonetikSelection(int giftCardType ,IGotBillingInformations gotBillingInformations, IGotUserContextInformations gotUserContextInformations);

        bool BankTransfertSelection(IGotBillingInformations gotBillingInformations);

        bool FnacCardCetelemSelection(IGotBillingInformations gotBillingInformations, IGotUserContextInformations gotcontextinformation, string creditId);

        bool FnacCardSelection(CreditCardEntity oCcd, string creditId, IGotBillingInformations gotBillingInformations, IGotUserContextInformations gotUserContextInformations);

        string GetErrorMessage(string messageId);

        ValidationResult CheckSipsCreditCardVerificationCode(string verificationCodeModel, int cryptoLength, bool isFnacCard = false);

        ValidationResult CheckSipsCreditCardHolderName(string name);

        ValidationResult CheckSipsCreditCardExpireDate(int expireMonth, int expireYear, bool hasExpDate);

        ValidationResult CheckSipsCreditCardNumber(string cardNumber, int numberLength, int typeId, string prefix);

        ValidationResult CheckFnacCardNumber(string cardNumber, bool checkLuhn);

        bool UnicreSelection(int selectedCreditCardId,IGotBillingInformations gotBillingInformations);

        bool PayByRefSelection(string bankAccountNumber, IGotBillingInformations gotBillingInformations, IGotUserContextInformations gotUserContextInformations);

        bool MBWaySelection(string phoneNumber, IGotBillingInformations gotBillingInformations, IGotUserContextInformations gotUserContextInformations);

        bool FibSelection(IGotBillingInformations gotBillingInformations, IGotUserContextInformations gotUserContextInformations);

        bool PayOnDeliverySelection(IGotBillingInformations gotBillingInformations);

        bool PayDeferred(IGotBillingInformations gotBillingInformations, IGotUserContextInformations gotcontextinformation);

        bool CheckSelection(string checkNumber, IGotBillingInformations gotBillingInformations, IGotUserContextInformations gotUserContextInformations);

        bool OrangeSelection(IGotBillingInformations gotBillingInformations);
    }
}
