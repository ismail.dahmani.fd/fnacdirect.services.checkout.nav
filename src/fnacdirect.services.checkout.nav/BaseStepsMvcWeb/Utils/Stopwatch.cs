using System;
using System.Collections.Generic;
using System.Globalization;

namespace FnacDirect.OrderPipe
{
    public class Stopwatch : IDisposable
    {
        [ThreadStatic]
        static Stack<Stopwatch> stack = null;

        [ThreadStatic]
        static bool debugMode = false;

        static public void SetDebugMode(bool mode)
        {
            debugMode = mode;
        }

        static Stack<Stopwatch> Stack
        {
            get
            {
                if (stack == null)
                    stack = new Stack<Stopwatch>();
                return stack;
            }
        }


        private int level;

        public static Stopwatch Current
        {
            get
            {
                if (Stack.Count == 0)
                    return null;
                return Stack.Peek();
            }
        }

        private string label = "";

        public string Label
        {
            get { return label; }
            set { label = value; }
        }

        private string result = "";

        public string Result
        {
            get { return result; }
            set { result = value; }
        }

        System.Diagnostics.Stopwatch s;

        public Stopwatch(string label)
        {
            this.label = label;
            if (Current != null)
                Current.LogStart();
            s = new System.Diagnostics.Stopwatch();
            s.Start();
            level = Stack.Count;

            startstring = DateTime.Now.ToString("HH:mm:ss,ffff", CultureInfo.InvariantCulture) + new string(' ', 1 + level * 2) + " " + label;

            Stack.Push(this);
        }

        private string startstring;
        private void LogStart()
        {
            if (startstring != null && debugMode)
                Logger.Log(startstring);
            startstring = null;
        }
        private void LogEnd()
        {
            if (debugMode)
                if (string.IsNullOrEmpty(result))
                    Logger.Log(DateTime.Now.ToString("HH:mm:ss,ffff", CultureInfo.InvariantCulture) + new string(' ', 1 + level * 2) + label + " : " + s.ElapsedMilliseconds + " ms");
                else
                    Logger.Log(DateTime.Now.ToString("HH:mm:ss,ffff", CultureInfo.InvariantCulture) + new string(' ', 1 + level * 2) + label + " (" + result.Trim() + ") : " + s.ElapsedMilliseconds + " ms");
        }

        #region IDisposable Members

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                Stack.Pop();
                s.Stop();
                LogEnd();
            }
        }

        #endregion
    }
}
