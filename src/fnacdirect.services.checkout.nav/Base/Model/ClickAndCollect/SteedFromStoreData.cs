﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace FnacDirect.OrderPipe.Base.Model.ClickAndCollect
{
    public class SteedFromStoreData : GroupData
    {
        private readonly IList<int> _pridsAvailable
            = new List<int>();

        [XmlIgnore]
        public int? StoreId { get; set; }

        [XmlIgnore]
        public int? StoreRefUG { get; set; }

        public SteedFromStoreData()
        {

        }

        public IEnumerable<int> PridsAvailable
        {
            get
            {
                return _pridsAvailable;
            }
        }

        public void ClearAvailabilities()
        {
            _pridsAvailable.Clear();
        }

        public void AddAvailable(int prid)
        {
            if (!_pridsAvailable.Contains(prid))
            {
                _pridsAvailable.Add(prid);
            }
        }
    }
}
