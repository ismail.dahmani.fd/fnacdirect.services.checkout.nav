﻿namespace FnacDirect.OrderPipe.Base.Business.LogisticLineGroups.Internals.InitialSelectors
{
    public interface ILineGroupInitialSelectorFactory
    {
        ILineGroupInitialSelector Get(ILogisticLineGroupGenerationProcessContext context);
    }

    public interface ILineGroupInitialSelectorFactory<out T> : ILineGroupInitialSelectorFactory
            where T : BaseLineGroupInitialSelector
    {
    }
}
