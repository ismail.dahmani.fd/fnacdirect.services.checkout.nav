using FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Common.Payment;
using FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Common.Shipping;
using FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Basket;
using System.Collections.Generic;
using FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.OnePage;
using FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Omniture;
using FnacDirect.OrderPipe.BaseMvc.Web.Models.OrderPipe.Common.Scoring;
using FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Membership;


namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Payment
{
    public class RootViewModel : BaseRootViewModel, IRootViewModel
    {
        public PaymentViewModel Payment { get; set; }

        public OptionsViewModel Options { get; set; }

        public BasketViewModel Basket { get; set; }

        public AddressViewModel BillingAddress { get; set; }

        public List<ISelectedShippingItem> SelectedShippings { get; set; }

        public bool HasAdherentNumber { get; set; }

        public string PageName { get; set; }

        public OmnitureBasketViewModel Omniture { get; set; }
        public TrackingViewModel Tracking { get; set; }

        public TermsAndConditionsViewModel TermsAndConditions { get; set; }

        public ScoringViewModel Scoring { get; set; }
        public MembershipViewModel Membership { get; set; }

    }
}
