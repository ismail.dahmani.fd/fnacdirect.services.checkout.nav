using FnacDirect.OrderPipe.Base.LineGroups;
using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.OrderPipe.Base.Steps.Pop;
using FnacDirect.OrderPipe.CoreMvcWeb.Routing;
using System.Linq;
using System.Web.Routing;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Controllers.Pop
{
    public class PopShippingRouteValueProvider : MvcOrderPipeStepRedirectionRouteValueProvider<PopOrderForm>
    {
        public RouteValueDictionary GetRouteValueFor(PopOrderForm popOrderForm, PopShippingSellerData nextSeller)
        {
            var popShippingData = popOrderForm.GetPrivateData<PopShippingData>();

            var routeValueDictionary = new RouteValueDictionary();

            var sellerId = nextSeller.SellerId;
            var fnacSellerId = SellerIds.GetId(SellerType.Fnac);

            if (nextSeller.SellerId < fnacSellerId)
            {
                sellerId = fnacSellerId;
            }

            var seller = "fnac";

            if (sellerId > fnacSellerId) // Market Place
            {
                var lineGroup = popOrderForm.LineGroups.FirstOrDefault(l => l.Seller.SellerId == sellerId);

                if (lineGroup == null)
                {
                    return routeValueDictionary;
                }

                seller = lineGroup.Seller.DisplayName.ToSeoUrl();
            }

            routeValueDictionary["sellerIndex"] = popShippingData.Sellers.IndexOf(nextSeller);
            routeValueDictionary["seller"] = seller;

            return routeValueDictionary;
        }

        protected override RouteValueDictionary GetRouteValueFor(PopOrderForm orderForm)
        {
            var popShippingData = orderForm.GetPrivateData<PopShippingData>();

            var nextSeller = popShippingData.Sellers.FirstOrDefault(s => !s.UserValidated);

            if (nextSeller == null)
            {
                nextSeller = popShippingData.Sellers.FirstOrDefault(s => s.Forced);

                if (nextSeller == null)
                {
                    nextSeller = popShippingData.Sellers.FirstOrDefault();

                    if (nextSeller == null)
                    {
                        return new RouteValueDictionary();
                    }
                }
            }

            return GetRouteValueFor(orderForm, nextSeller);
        }
    }
}
