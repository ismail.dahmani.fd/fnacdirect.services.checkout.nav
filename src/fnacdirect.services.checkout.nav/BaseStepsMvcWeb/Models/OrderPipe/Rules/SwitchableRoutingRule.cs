﻿using FnacDirect;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Routing;

namespace FnacDirect.OrderPipe.CoreRouting.Rules
{
    public abstract class SwitchableRoutingRule : IRoutingRule
    {
        private readonly ISwitchProvider _switchProvider;
        private readonly string _switchName;

        public SwitchableRoutingRule(ISwitchProvider switchProvider, string switchName)
        {
            _switchProvider = switchProvider;
            _switchName = switchName;
        }

        protected abstract IHttpHandler InnerApply(RequestContext requestContext);

        public IHttpHandler Apply(RequestContext requestContext)
        {
            if(_switchProvider.IsEnabled(_switchName))
            {
                return InnerApply(requestContext);
            }

            return null;
        }
    }
}
