using System;
using FnacDirect.Technical.Framework.CoreServices;
using System.Data.SqlClient;
using System.Data;
using FnacDirect.OrderPipe.Base.DAL;

namespace FnacDirect.OrderPipe.Base.BaseDAL
{
    public class MarketPlaceOfferDAL
    {
        private const string DATABASE_SqlOffer = "SqlMarketPlaceOffer";

        public int GetAvailableOfferQuantity(int offerId, int? reservationIdToExclude)
        {
            var returnValue = 0;
            try
            {
                using (var database = Database.CreateInstance(DATABASE_SqlOffer))
                {
                    using (var command = database.GetStoredProcCommand("spMP_GetAvailableOfferQuantity"))
                    {
                        database.AddInParameter(command, "OfferId", SqlDbType.Int, offerId);
                        database.AddInNullableParameter(command, "ReservationIdToExclude", SqlDbType.Int, reservationIdToExclude);
                        returnValue = (int)command.ExecuteScalar();
                    }
                }
            }
            catch (Exception ex)
            {
                throw DALException.BuildException(ex, DALRange.MarketPlace, 1);
            }

            return returnValue;

        }
    }
}
