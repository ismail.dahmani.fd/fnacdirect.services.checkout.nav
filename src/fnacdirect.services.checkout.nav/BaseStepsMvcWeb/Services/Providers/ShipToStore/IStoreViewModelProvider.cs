﻿using System;
using System.Collections.Generic;
using FnacDirect.OrderPipe.Base.BaseModel;
using FnacDirect.OrderPipe.Base.Model.LineGroups;
using FnacDirect.OrderPipe.BaseMvc.Web.Models.Shipping.ShipToStore;
using FnacDirect.OrderPipe.BaseMvc.Web.Services.ShipToStore;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Providers.ShipToStore
{
    public interface IStoreViewModelProvider
    {
        IEnumerable<SearchResultGeoStoreViewModel> GetSearchResultsGeoStoreViewModel(IEnumerable<StoreSearchGeoResult> storeSearchGeoResults, IEnumerable<ILineGroup> lineGroups, Maybe<ShopShippingMethodEvaluationItem> shippingMethodEvaluationItem);
    }
}