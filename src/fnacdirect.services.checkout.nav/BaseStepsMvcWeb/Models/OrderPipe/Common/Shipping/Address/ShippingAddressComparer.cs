using System;
using System.Collections.Generic;

namespace FnacDirect.OrderPipe.Base.Model
{
    public class ShippingAddressComparer : IEqualityComparer<ShippingAddress>
    {
        public bool Equals(ShippingAddress x, ShippingAddress y)
        {
            if (x is PostalAddress && y is PostalAddress)
            {
                return Equals((PostalAddress)x, (PostalAddress)y);
            }
            else if (x is RelayAddress && y is RelayAddress)
            {
                return Equals((RelayAddress)x, (RelayAddress)y);
            }
            else if (x is ShopAddress && y is ShopAddress)
            {
                return Equals((ShopAddress)x, (ShopAddress)y);
            }
            return false;
        }

        public bool Equals(PostalAddress x, PostalAddress y)
        {
            const StringComparison SC = StringComparison.OrdinalIgnoreCase;
            return
                x.Firstname.Equals(y.Firstname, SC) &&
                x.LastName.Equals(y.LastName, SC) &&
                x.AddressLine1.Equals(y.AddressLine1, SC) &&
                x.AddressLine2.Equals(y.AddressLine2, SC) &&
                x.AddressLine3.Equals(y.AddressLine3, SC) &&
                x.AddressLine4.Equals(y.AddressLine4, SC) &&
                x.ZipCode.Equals(y.ZipCode, SC) &&
                x.City.Equals(y.City, SC);
        }

        public bool Equals(ShopAddress x, ShopAddress y)
        {

            return x.Identity == y.Identity;
        }

        public bool Equals(RelayAddress x, RelayAddress y)
        {

            return x.FDRelayId == y.FDRelayId;
        }


        public int GetHashCode(ShippingAddress a)
        {
            if (a is PostalAddress)
            {
                return GetHashCode((PostalAddress)a);
            }
            else if (a is RelayAddress)
            {
                return GetHashCode((RelayAddress)a);
            }
            else if (a is ShopAddress)
            {
                return GetHashCode((ShopAddress)a);
            }
            return 0;
        }

        public int GetHashCode(PostalAddress pa)
        {
            var hCode = pa.OrderShippingAddressPk.GetValueOrDefault();
            return hCode.GetHashCode();
        }

        public int GetHashCode(RelayAddress ra)
        {
            var hCode = ra.FDRelayId.GetValueOrDefault();
            return hCode.GetHashCode();
        }

        public int GetHashCode(ShopAddress ba)
        {
            var hCode = ba.Identity.GetValueOrDefault();
            return hCode.GetHashCode();
        }

    }
}

