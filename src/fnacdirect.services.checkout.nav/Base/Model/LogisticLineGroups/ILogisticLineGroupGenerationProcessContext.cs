﻿using FnacDirect.OrderPipe.Base.BaseModel;
using FnacDirect.OrderPipe.Base.Business.LogisticLineGroups.Handlers;
using FnacDirect.OrderPipe.Base.Business.LogisticLineGroups.Internals.Handlers;
using FnacDirect.OrderPipe.Base.Model.Facets;
using FnacDirect.OrderPipe.Base.Model.LineGroups;
using System;
using System.Collections.Generic;

namespace FnacDirect.OrderPipe.Base.Business.LogisticLineGroups
{
    public interface ILogisticLineGroupGenerationProcessContext : IDisposable
    {
        IPrivateDataContainer PrivateDataContainer { get; }

        IEnumerable<ILineGroup> LineGroups { get; }

        IGotShippingMethodEvaluation IGotShippingMethodEvaluation { get; }
        IGotBillingInformations IGotBillingInformations { get; }

        IEnumerable<ILogisticLineGroupProcessCompletedHandlerDescriptor> ProcessCompletedHandlers { get; }
        void AddProcessCompletedHandler(ILogisticLineGroupProcessCompletedHandlerDescriptor logisticLineGroupProcessCompletedHandlerDescriptor);
    }
}
