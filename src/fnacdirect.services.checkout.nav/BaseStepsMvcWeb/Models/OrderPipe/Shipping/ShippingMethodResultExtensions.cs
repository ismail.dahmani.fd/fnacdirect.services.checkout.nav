﻿using System.Collections.Generic;
using FnacDirect.OrderPipe.Shipping.Model;
using System.Linq;

namespace FnacDirect.OrderPipe.Base.BaseModel
{
    public static class ShippingMethodResultExtensions
    {
        public static IEnumerable<int> GetShippingMethods(this ShippingMethodResult shippingMethodResult)
        {
            if (shippingMethodResult == null)
                yield break;

            if (shippingMethodResult.ChoiceList == null)
                yield break;

            foreach (var choice in shippingMethodResult.ChoiceList)
            {
                if (choice.Choices == null)
                    continue;

                var orderedChoices = choice.Choices.OrderByDescending(m => m.IsDefault);

                foreach (var detail in orderedChoices)
                {
                    if (detail == null)
                        continue;

                    yield return detail.ShippingMethodId;
                }
            }
        }
    }
}
