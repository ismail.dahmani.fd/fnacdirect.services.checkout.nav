using System;
using System.Collections.Generic;
using System.Text;
using FnacDirect.OrderPipe;
using FnacDirect.Quote.Model.Enums;
using System.Xml.Serialization;

namespace FnacDirect.Quote.Model
{
    public class QuoteDetailButtonsGroupData : GroupData
    {
        #region Members
        
        private QuoteMenuButton _quoteMenuVisibleButtons;

        #endregion Members
        
        /// <summary>
        /// Gets or sets all visibles buttons in quote menu
        /// </summary> 
        [XmlIgnore]
        public QuoteMenuButton QuoteMenuVisiblesButtons { get { return _quoteMenuVisibleButtons; } set { _quoteMenuVisibleButtons = value; } }
    }
}
