﻿namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Common
{
    public class MessageViewModel
    {
        public string MainPaymentMessage { get; set; }
        public string SecondaryPaymentMessage { get; set; }
    }
}
