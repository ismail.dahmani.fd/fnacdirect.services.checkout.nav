﻿using System;
using System.Collections.Generic;
using FnacDirect.Membership.Model.Entities;

namespace FnacDirect.OrderPipe.Base.BaseModel.Model
{
    public enum TryCardStatus
    {
        None,
        ToAdd,
        Added,
        Validated,
        ToRemove,
        Removed
    }

    public class AdherentData : CustomerData
    {
        public enum ErrorEnum
        {
            None,
            InvalidBirthdate,
            MinAge,
            MinAgeForNormal,
            MaxAgeForYoung,
            InvalidAccountLinkWithRenewCard,
            SuggestRenewCard,
            TryAdherentAlreadyAdherent,
            NeedArticleInBasket,
            TryCardOutdatedAdherent,
            AdherentTryCardWithNoArticle
        }

        private ErrorEnum _error = ErrorEnum.None;
        public ErrorEnum Error
        {
            get { return _error; }
            set { _error = value; }
        }

        public bool Has1YearCard { get; set; }

        public bool Has3YearsCard { get; set; }

        public bool HasTryCard { get; set; }

        public bool RemovedTryCard { get; set; }

        public TryCardStatus TryCardStatus { get; set; }

        public bool HasCupAccount { get; set; }

        public bool HasFinarefAccount { get; set; }

        public bool HasCard
        {
            get
            {
                return Has1YearCard || Has3YearsCard || HasTryCard;
            }
        }

        public bool IsYoungPrice { get; set; }
        public bool IgnoreRenewal { get; set; }
        private List<int> _cards = new List<int>();
        public List<int> Cards
        {
            get { return _cards; }
            set { _cards = value; }
        }

        public DateTime? BirthDate { get; set; }

        public AdherentData()
        {
            IsYoungPrice = false;
            Has3YearsCard = false;
            Has1YearCard = false;
            HasTryCard = false;
            RemovedTryCard = false;
            TryCardStatus = TryCardStatus.None;
        }

        public EligibilityRenewCard EligibilityCardInBasket { get; set; }


        public int UserIdentity { get; set; }
    }
}
