using System;

namespace FnacDirect.OrderPipe.BaseMvc.Web.CrossSellBasket
{
    public class CrossSellBasketItemInput
    {
        public int MasterProductId { get; set; }

        public Guid Offer { get; set; }

        public int ProductId { get; set; }

        public int Quantity { get; set; } = 1;

        public int ShopId { get; set; }
    }
}
