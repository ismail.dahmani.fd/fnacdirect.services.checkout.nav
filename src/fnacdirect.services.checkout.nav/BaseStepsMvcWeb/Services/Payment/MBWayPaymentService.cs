using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.OrderPipe.Core.Services;
using FnacDirect.Technical.Framework.Web.Mvc;

namespace FnacDirect.OrderPipe.Base.Business.Payment
{
    public class MBWayPaymentService : IMBWayPaymentService
    {
        private readonly ISwitchProvider _switchProvider;
        private readonly IPaymentEligibilityService _paymentEligibilityService;

        public MBWayPaymentService(IUserExtendedContextProvider userExtendedContextProvider,
            IPipeParametersProvider pipeParametersProvider,
            ISwitchProvider switchProvider,
            IPaymentEligibilityService paymentEligibilityService)
        {
            _switchProvider = switchProvider;
            _paymentEligibilityService = paymentEligibilityService;
        }

        public bool IsAllowed(PopOrderForm popOrderForm)
        {
            var rules = _paymentEligibilityService.GetRules("orderpipe.pop.payment.mbway.eligibilityrules");

            if (!_paymentEligibilityService.IsAllowed(popOrderForm, rules))
            {
                return false;
            }

            return _switchProvider.IsEnabled("orderpipe.pop.payment.mbway.eligibility");
        } 
    }
}
