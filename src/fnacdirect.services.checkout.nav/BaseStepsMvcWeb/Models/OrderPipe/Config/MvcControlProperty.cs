﻿using System.Xml.Serialization;

namespace FnacDirect.OrderPipe.Config
{
    public class MvcControlProperty
    {
        [XmlAttribute("name")]
        public string Name { get; set; }

        [XmlAttribute("value")]
        public string Value { get; set; }
    }
}