﻿using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.OrderPipe.Base.Proxy.Customer;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Common.Payment.BillingMethodFactories
{
    public class PayByRefViewModelFactory : BasePaymentViewModelFactory<PayByRefBillingMethod>
    {
        private readonly IBankAccountNumberService _bankAccountNumberService;

        public PayByRefViewModelFactory(IBankAccountNumberService bankAccountNumberService)
        {
            _bankAccountNumberService = bankAccountNumberService;
        }

        public override BillingMethodViewModel Get(string tabName, PaymentMethod paymentMethod, OPContext opContext, PopOrderForm popOrderForm)
        {
            return new PayByRefBillingMethodViewModel
            {
                Display = true,
                Nib = _bankAccountNumberService.GetBankAccountNumber()
            };
        }
    }
}
