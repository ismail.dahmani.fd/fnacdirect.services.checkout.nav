﻿namespace FnacDirect.OrderPipe.Base.Model
{
    public enum StockAvailability
    {
        /// <summary>
        /// Article indisponible.
        /// </summary>
        None = 0,

        /// <summary>
        /// Article disponible en central.
        /// </summary>
        Central = 1,

        /// <summary>
        /// Article disponible uniquement en magasin (indispo central). 
        /// </summary>
        ClickAndCollectOnly = 2,

        /// <summary>
        /// Article disponible en vente intramag (rayon ou stock) 
        /// </summary>
        Shop = 3
    }
}
