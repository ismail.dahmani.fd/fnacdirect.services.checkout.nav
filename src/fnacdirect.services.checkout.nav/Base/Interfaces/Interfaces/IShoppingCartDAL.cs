﻿namespace FnacDirect.OrderPipe.Base.DAL
{
    public interface IShoppingCartDAL
    {
        string Read(string getsp, string sid);
        string Read(string getsp, string sid, string pipeName);
        void Write(string setsp, string sid, string content);
        void Write(string setsp, string sid, string content, string pipeName);
    }
}