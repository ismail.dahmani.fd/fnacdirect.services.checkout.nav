using System.Collections.Generic;

namespace System.Linq
{
    public static class EnumerableExtensions
    {
        public static TProperty MinOrDefault<TSource, TProperty>(this IEnumerable<TSource> source, Func<TSource, TProperty> selector, TProperty defaultValue = default(TProperty))
        {
            var enumerable = source as IList<TSource> ?? source.ToList();

            return !enumerable.Any() ? defaultValue : enumerable.Min(selector);
        }

        public static TProperty MaxOrDefault<TSource, TProperty>(this IEnumerable<TSource> source, Func<TSource, TProperty> selector, TProperty defaultValue = default(TProperty))
        {
            var enumerable = source as IList<TSource> ?? source.ToList();

            return !enumerable.Any() ? defaultValue : enumerable.Max(selector);
        }

        public static bool Empty<TSource>(this IEnumerable<TSource> source)
        {
            return !source.Any();
        }
        public static bool IsNullOrEmpty<TSource>(this IEnumerable<TSource> source)
        {
            return source == null || !source.Any();
        }
    }
}
