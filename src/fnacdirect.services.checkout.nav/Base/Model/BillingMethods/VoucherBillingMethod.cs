﻿using System.Xml.Serialization;

namespace FnacDirect.OrderPipe.Base.Model
{
    public class VoucherBillingMethod : BillingMethod
    {
        public enum DematConditionEnum
        {
            PhysicalOnly = 0,
            DematOnly = 1,
            Mixed = 2
        }
        public const int IdBillingMethod = 8;

        [XmlIgnore]
        private int? _CheckId;

        public int? CheckId
        {
            get
            {
                return _CheckId;
            }
            set
            {
                _CheckId = value;
            }
        }



        [XmlIgnore]
        private int? _TypeId;

        public int? TypeId
        {
            get
            {
                return _TypeId;
            }
            set
            {
                _TypeId = value;
            }
        }

        [XmlIgnore]
        private bool? _IsCumulable;

        public bool? IsCumulable
        {
            get
            {
                if (!_IsCumulable.HasValue)
                    _IsCumulable = false;
                return _IsCumulable;
            }
            set
            {
                _IsCumulable = value;
            }
        }

        [XmlIgnore]
        private int? _GenId;

        public int? GenId
        {
            get
            {
                return _GenId;
            }
            set
            {
                _GenId = value;
            }
        }

        [XmlIgnore]
        public bool IsVoucherDemat
        {
            get
            {
                return _GenId == 35;
            }
        }

        [XmlIgnore]
        private string _CheckNumber;


        public string CheckNumber
        {
            get
            {
                return _CheckNumber;
            }
            set
            {
                _CheckNumber = value;
            }
        }


        [XmlIgnore]
        private decimal? _AmountEur;

        public decimal? AmountEur
        {
            get
            {
                return _AmountEur;
            }
            set
            {
                _AmountEur = value;
            }
        }

        [XmlIgnore]
        private decimal? _AmountEurNoVAT;

        public decimal? AmountEurNoVAT
        {
            get
            {
                return _AmountEurNoVAT;
            }
            set
            {
                _AmountEurNoVAT = value;
            }
        }


        [XmlIgnore]
        private bool? _Valid;

        [XmlIgnore]
        public bool? Valid
        {
            get
            {
                return _Valid;
            }
            set
            {
                _Valid = value;
            }
        }

        [XmlIgnore]
        private int _ShopId;

        public int ShopId
        {
            get
            {
                return _ShopId;
            }
            set
            {
                _ShopId = value;
            }
        }

        [XmlIgnore]
        private DematConditionEnum _dematCondition;
        public DematConditionEnum DematCondition
        {
            get
            {
                return _dematCondition;
            }
            set
            {
                _dematCondition = value;
            }
        }

        public VoucherBillingMethod()
        {
            BillingMethodId = IdBillingMethod;
            Priority = 10;
            InitialStatus = 3;
        }
    }
}
