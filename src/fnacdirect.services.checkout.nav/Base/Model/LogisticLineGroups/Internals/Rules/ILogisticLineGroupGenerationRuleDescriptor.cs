﻿using System;
using System.Collections.Generic;

namespace FnacDirect.OrderPipe.Base.Business.LogisticLineGroups.Internals.Rules
{
    public interface ILogisticLineGroupGenerationRuleDescriptor
    {
        string SwitchId { get; }

        Type Selector { get; }
        Type Initializer { get; }
        Type Unfolder { get; }

        IEnumerable<Type> LogisticLineGroupGeneratedHandlers { get; }
    }
}
