namespace FnacDirect.OrderPipe.Base.BaseModel.Model
{
    public class AdherentCardAddData : GroupData
    {
        public enum ErrorAddCardEnum
        {
            None,
            SameCardAlreadyInBasket,
            BasketAlreadyContainsCard
        }

        private ErrorAddCardEnum _error = ErrorAddCardEnum.None;
        public ErrorAddCardEnum Error
        {
            get { return _error; }
            set { _error = value; }
        }
    }
}
