using FnacDirect.Membership.Model.Constants;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FnacDirect.OrderPipe.Base.BaseModel.Model
{
    public class CardInfosData
    {
        public int Prid { get; set; }

        public string Label { get; set; }

        public int TypeId { get; set; }

        public bool HasOPC { get; set; }

        public decimal StandardPrice { get; set; }

        public decimal PriceWithOffer { get; set; }

        public string ValidityType { get; set; }

        public int ValidityDuration { get; set; }

        public bool IsTryCard => TypeId == (int)CategoryCardType.SouscriptionFnacPlusTrial || TypeId == (int)CategoryCardType.SouscriptionFnacPlusTrialDecouverte;

        public bool IsRenewCard => TypeId == (int)CategoryCardType.RenouvellementFnacPlus || TypeId == (int)CategoryCardType.Renew;
    }

    public class PushFnacPlusAdherentData : GroupData
    {
        public bool IsFnacPlusEnabled { get; set; }

        public bool IsFnacPlusCanalPlayEnabled { get; set; }
        
        public bool IsAdherent { get; set; }

        public bool IsEligibleEssaiFnacPlus { get; set; }

        public bool IsEligiblePushRecruit { get; set; }

        public bool IsEligiblePushRenouvAdh { get; set; }

        public bool IsEligiblePushRenouvFnacPlus { get; set; }

        public bool IsEligiblePushConfirm { get; set; }

        public bool HasAnyCardInBasket { get; set; }
        public bool HasAdhCardInBasket { get; set; }

        public bool HasFnacPlusCardInBasket { get; set; }

        public bool HasFnacPlusTryCardInBasket { get; set; }

        public CardInfosData CardPlusInfos { get; set; }

        public CardInfosData RenewCardPlusInfos { get; set; }

        public decimal SimulateSavingAmount { get; set; }

        public decimal SimulateDifferedAmount { get; set; }

        public bool IsAddedFromShippingStep { get; set; }

        public bool IsRemovedFromShippingStep { get; set; }

        public void ResetElegibility()
        {
            IsEligibleEssaiFnacPlus = false;
            IsEligiblePushConfirm = false;
            IsEligiblePushRecruit = false;
            IsEligiblePushRenouvAdh = false;
            IsEligiblePushRenouvFnacPlus = false;
        }
    }
}
