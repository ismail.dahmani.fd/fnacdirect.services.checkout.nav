﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace FnacDirect.OrderPipe.Mvc.Routing
{
    public class TransferResult : ActionResult
    {
        private readonly string _controller;
        private readonly string _action;

        public TransferResult(string controller, string action)
        {
            _action = action;
            _controller = controller;
        }

        public override void ExecuteResult(ControllerContext context)
        {
            var routeData = new RouteData();

            foreach (var routeDataValue in context.RequestContext.RouteData.Values)
            {
                if (routeDataValue.Key != "action" && routeDataValue.Key != "controller")
                {
                    routeData.Values.Add(routeDataValue.Key, routeDataValue.Value);
                }
            }

            routeData.Values.Add("action", _action);
            routeData.Values.Add("controller", _controller);

            var requestContext = new RequestContext(context.HttpContext, routeData);

            IHttpHandler mvcHandler = new MvcHandler(requestContext);

            mvcHandler.ProcessRequest(HttpContext.Current);
        }
    }
}