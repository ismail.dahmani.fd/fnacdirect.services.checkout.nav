using System;
using System.Xml.Serialization;
using FnacDirect.Customer.Model;
using FnacDirect.ServiceQuickAddress;

namespace FnacDirect.OrderPipe.Base.Model
{

    public enum AddressVerifStatus
    {
        None = 0,
        OnError = 1,
        Verified = 2,
        ToComplete = 3,
        InteractionRequired = 4,
        Multiple = 5,
        PremisesPartial = 6,
        StreetPartial = 7,
        Same = 8,
    }

    public class VerifiedAddress
    {
        public string Address1;
        public string Address2;
        public string Address3;
        public string ZipCode;
        public string City;
        public string Company;
    }

    /// Objet résultat QAS : donne le statut, la liste des adresses et indique si c'est issu d'un contrôle non demandé
    public class VerifStatusPickList
    {
        public AddressVerifStatus VerifyLevelPickList;
        public Picklist Picklist;
        public bool changed = false; //Indique que le contrôle a été effectué contrairment au souhait de l'utilisateur. Ce cas arrive lorsque l'utilisateur a modifié l'adresse mais pas le choix de contrôle
    }

    abstract public class QasCommonData : GroupData
    {
        public enum AddressTypeEnum
        {
            None,
            Shipping,
            Billing
        }

        public enum VerifiedAddressEnum : int
        {
            Unknown = 0,
            Matched = 2,
            ForcedNoChange = 3,
            Same = 4,
            SameUpdated = 5,
            NoCheckNeeded = 6,
            Updated = 7
        }

        [XmlIgnore]
        private VerifiedAddressEnum _addressStatus = VerifiedAddressEnum.Unknown;
        public VerifiedAddressEnum AddressStatus
        {
            get { return _addressStatus; }
            set { _addressStatus = value; }
        }

        [XmlIgnore]
        private VerifStatusPickList _addressQasResult = null;
        [XmlIgnore]
        public VerifStatusPickList AddressQasResult
        {
            get { return _addressQasResult; }
            set { _addressQasResult = value; }
        }

        [XmlIgnore]
        private AddressEntity _addressVerified = null;
        [XmlIgnore]
        public AddressEntity AddressVerified
        {
            get { return _addressVerified; }
            set { _addressVerified = value; }
        }

        public void Clean()
        {
            _addressStatus = VerifiedAddressEnum.Unknown;
            _addressQasResult = null;
            _addressVerified = null;
        }
    }
}
