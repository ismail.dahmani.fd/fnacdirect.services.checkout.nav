using System;

namespace FnacDirect.OrderPipe.Base.Model.OrderInformations
{
    public interface IOrderGlobalInformations
    {
        string AdvantageCode { get; set; }

        /// <summary>
        /// Codes retour du panier issu du pricer (codes avantages par exemple)
        /// </summary>
        int MessageCodes { get; set; }

        bool ModeBatch { get; set; }

        bool OrderInserted { get; set; }

        bool OrderCancelled { get; set; }

        DateTime OrderDate { get; set; }

        int MainOrderPk { get; set; }

        Guid MainOrderReference { get; set; }

        string MainOrderUserReference { get; set; }

        bool HasOgoneBeenCalled { get; set; }

        OrderTransactionInfoList MainOrderTransactionInfos { get; set; }

        bool OrderValidated { get; set; }

        bool OgoneDirectLinkUnfinished { get; set; }

        string PaymentReference { get; set; }

        string MerchantId { get; set; }

        string BankAccountNumber { get; set; }

        string OrderExternalReference { get; set; }
    }
}
