﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FnacDirect.OrderPipe.Base.Business.ShoppingCart
{
    public interface IW3ShoppingCart
    {
        void IssueNbArtCookie(int quantities);
    }
}
