﻿using FnacDirect.OrderPipe.Base.Business.Shipping.Relay;
using FnacDirect.OrderPipe.Base.Proxy.Customer;
using FnacDirect.OrderPipe.Base.Proxy.Relay;
using FnacDirect.OrderPipe.Shipping;
using FnacDirect.Solex.Shipping.Client;
using FnacDirect.Technical.Framework.Web.Mvc;

namespace FnacDirect.OrderPipe.Base.Business.Shipping
{
    public class ShippingAddressElegibilityServiceProvider : IShippingAddressElegibilityServiceProvider
    {
        private readonly IApplicationContext _applicationContext;
        private readonly IShippingMethodBasketServiceProvider _shippingMethodBasketServiceProvider;
        private readonly IRelayAddressService _relayAddressService;
        private readonly ISwitchProvider _switchProvider;
        private readonly IRelayValidityService _relayValidityService;
        private readonly ICustomerRelayService _customerRelayService;
        private readonly IRelayService _relayService;
        private readonly ISolexBusiness _solexBusiness;
        private readonly ICountryService _countryService;

        public ShippingAddressElegibilityServiceProvider(IApplicationContext applicationContext,
            IShippingMethodBasketServiceProvider shippingMethodBasketServiceProvider,
            IRelayAddressService relayAddressService,
            ISwitchProvider switchProvider,
            IRelayValidityService relayValidityService,
            ICustomerRelayService customerRelayService,
            IRelayService relayService,
            ISolexBusiness solexBusiness,
            ICountryService countryService)
        {
            _applicationContext = applicationContext;
            _shippingMethodBasketServiceProvider = shippingMethodBasketServiceProvider;
            _relayAddressService = relayAddressService;
            _switchProvider = switchProvider;
            _relayValidityService = relayValidityService;
            _customerRelayService = customerRelayService;
            _relayService = relayService;
            _solexBusiness = solexBusiness;
            _countryService = countryService;
        }

        public IShippingAddressElegibilityService GetShippingAddressElegibilityServiceFor(OPContext opContext)
        {
            return new ShippingAddressElegibilityService(opContext, new ShippingMethodBusiness(), _applicationContext, _shippingMethodBasketServiceProvider, _relayAddressService, _switchProvider, _relayValidityService, _customerRelayService, _relayService, _solexBusiness, _countryService);
        }
    }
}
