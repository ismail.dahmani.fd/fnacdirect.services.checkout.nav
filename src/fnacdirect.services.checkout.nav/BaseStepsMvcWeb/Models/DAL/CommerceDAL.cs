using FnacDirect.Contracts.Online.Model;
using FnacDirect.Customer.Model;
using FnacDirect.OrderPipe.Base.BaseDAL;
using FnacDirect.OrderPipe.Base.BaseModel.Model;
using FnacDirect.OrderPipe.Base.LineGroups;
using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.OrderPipe.Base.Model.FDV.Rebate;
using FnacDirect.OrderPipe.Base.Model.LineGroups;
using FnacDirect.OrderPipe.Base.Model.Solex;
using FnacDirect.Technical.Framework.CoreServices;
using FnacDirect.Technical.Framework.Switch;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace FnacDirect.OrderPipe.Base.DAL
{
    public class CommerceDAL : ISplitMethodsDAL, IBillingAddressCommerceDAL, IPaymentMethodDal, ISubscriptionCommerceDal, IShopCommerceDAL, IOrderDetailsDAL, IPromotionDal, IPaymentDAL, ICommerceDal
    {
        private const string DatabaseSqlCommerce = "SqlCommerce";

        #region Purchase insertion

        public void AddOrderDiscount(
            int orderId,
            int orderDetailId,
            int discountTypeId,
            int discountReasonId,
            decimal discountAmount,
            decimal? s18DiscountAmount = null,
            string s18DiscountCode = null,
            string s18DiscountType = null,
            int? typeId = null,
            string code = null,
            int? templateId = null,
            string respComment = null,
            string sellerComment = null)
        {
            try
            {
                using (var database = Database.CreateInstance(DatabaseSqlCommerce))
                {
                    using (var command = database.GetStoredProcCommand("spPipe_Add_OrderDiscount"))
                    {
                        database.AddInParameter(command, "OrderId", SqlDbType.Int, orderId);
                        database.AddInParameter(command, "OrderDetailId", SqlDbType.Int, orderDetailId);
                        database.AddInParameter(command, "OrderDiscountTypeId", SqlDbType.Int, discountTypeId);
                        database.AddInParameter(command, "OrderDiscountReasonId", SqlDbType.Int, discountReasonId);
                        database.AddInParameter(command, "DiscountAmount", SqlDbType.Decimal, discountAmount);

                        if (s18DiscountAmount.HasValue)
                            database.AddInParameter(command, "S18DiscountAmount", SqlDbType.Decimal, s18DiscountAmount.Value);

                        if (s18DiscountCode != null)
                            database.AddInParameter(command, "S18DiscountCode", SqlDbType.VarChar, s18DiscountCode);

                        if (s18DiscountCode != null)
                            database.AddInParameter(command, "S18DiscountType", SqlDbType.VarChar, s18DiscountType);

                        if (typeId.HasValue)
                            database.AddInParameter(command, "type", SqlDbType.Int, typeId.Value);

                        if (!string.IsNullOrEmpty(code))
                            database.AddInParameter(command, "code", SqlDbType.VarChar, code);

                        if (templateId.HasValue)
                            database.AddInParameter(command, "template", SqlDbType.Int, templateId.Value);

                        database.AddInParameter(command, "SellerComment", SqlDbType.VarChar, sellerComment);
                        database.AddInParameter(command, "RespComment", SqlDbType.VarChar, respComment);

                        database.AddOutParameter(command, "OrderDiscountsId", SqlDbType.Int);
                        command.ExecuteNonQuery();
                    }
                }

            }
            catch (Exception ex)
            {
                throw DALException.BuildException(ex, DALRange.Commerce, 4);
            }
        }

        public void AddOrderInfo(int orderId, int orderInfoType, string orderInfo)
        {
            try
            {
                using (var database = Database.CreateInstance(DatabaseSqlCommerce))
                {
                    using (var command = database.GetStoredProcCommand("spPipe_AddOrderInfo"))
                    {
                        database.AddInParameter(command, "orderId", SqlDbType.Int, orderId);
                        database.AddInParameter(command, "orderInfoType", SqlDbType.Int, orderInfoType);
                        database.AddInParameter(command, "orderInfo", SqlDbType.Text, orderInfo);
                        command.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception ex)
            {
                throw DALException.BuildException(ex, DALRange.Commerce, 5);
            }
        }

        public void AddBillingAddress(int orderId, int accountId, string alias, string company, string vatNumber, string firstName, string lastName, string addressLine1, string addressLine2, string addressLine3, string addressLine4, string zipCode, string city, string state, string countryId, string tel, string cellphone, string fax, int orderBillingAddressStatusId, string taxIdNumber, int nationalityCountryId, int legalStatus, int gender, string siretNumber, int? docCountry = null, int? docType = null)
        {
            try
            {
                using (var database = Database.CreateInstance(DatabaseSqlCommerce))
                {
                    using (var command = database.GetStoredProcCommand("spPipe_AddBillingAddress"))
                    {
                        database.AddInParameter(command, "nOrderFk", SqlDbType.Int, orderId);
                        database.AddInParameter(command, "nAccountFk", SqlDbType.Int, accountId);
                        database.AddInParameter(command, "vstrAlias", SqlDbType.VarChar, alias);
                        database.AddInParameter(command, "vstrCompany", SqlDbType.VarChar, company);
                        database.AddInParameter(command, "vstrVatNumber", SqlDbType.VarChar, vatNumber);
                        database.AddInParameter(command, "vstrFirstName", SqlDbType.VarChar, firstName);
                        database.AddInParameter(command, "vstrLastName", SqlDbType.VarChar, lastName);
                        database.AddInParameter(command, "vstrAddressLine1", SqlDbType.VarChar, addressLine1);
                        database.AddInParameter(command, "vstrAddressLine2", SqlDbType.VarChar, addressLine2);
                        database.AddInParameter(command, "vstrAddressLine3", SqlDbType.VarChar, addressLine3);
                        database.AddInParameter(command, "vstrAddressLine4", SqlDbType.VarChar, addressLine4);
                        database.AddInParameter(command, "vstrZipCode", SqlDbType.VarChar, zipCode);
                        database.AddInParameter(command, "vstrCity", SqlDbType.VarChar, city);
                        database.AddInParameter(command, "vstrState", SqlDbType.VarChar, state);
                        database.AddInParameter(command, "vstrCountryFk", SqlDbType.Int, countryId);
                        database.AddInParameter(command, "vstrTel", SqlDbType.VarChar, tel);
                        database.AddInParameter(command, "vstrCellPhone", SqlDbType.VarChar, cellphone);
                        database.AddInParameter(command, "vstrFax", SqlDbType.VarChar, fax);
                        database.AddInParameter(command, "nOrderBillingAddressStatusFk", SqlDbType.Int, orderBillingAddressStatusId);
                        database.AddInParameter(command, "vstrTaxId", SqlDbType.VarChar, taxIdNumber);
                        database.AddInParameter(command, "nNationalityCountryId", SqlDbType.Int, nationalityCountryId);
                        database.AddInParameter(command, "nLegalStatus", SqlDbType.Int, legalStatus);
                        database.AddInParameter(command, "nGender", SqlDbType.Int, gender);
                        database.AddInParameter(command, "vstrSiretNumber", SqlDbType.VarChar, siretNumber);
                        database.AddInParameter(command, "nDocCountry", SqlDbType.Int, docCountry);
                        database.AddInParameter(command, "nDocType", SqlDbType.Int, docType);

                        command.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception ex)
            {
                throw DALException.BuildException(ex, DALRange.Commerce, 5);
            }
        }

        public int AddCreditCard(int orderId, int accountId, int? accountCreditCardId, int orderCreditCardId, string alias, string encryptedCardNumber, string verificationCode, string expireDate, string cardHolder, string creditCardStatusId, int billingMethodId)
        {
            var returnValue = 0;
            try
            {
                using (var database = Database.CreateInstance(DatabaseSqlCommerce))
                {
                    using (var command = database.GetStoredProcCommand("spPipe_AddCreditCard"))
                    {
                        database.AddInParameter(command, "nOrderFk", SqlDbType.Int, orderId);
                        database.AddInParameter(command, "nAccountFk", SqlDbType.Int, accountId);
                        if (accountCreditCardId.HasValue)
                        {
                            database.AddInParameter(command, "nAccountCreditCardfk", SqlDbType.Int, accountCreditCardId.Value);
                        }
                        else
                        {
                            database.AddInParameter(command, "nAccountCreditCardfk", SqlDbType.Int, 0);
                        }
                        database.AddInParameter(command, "nOrderCreditCardTypeFk", SqlDbType.Int, orderCreditCardId);
                        database.AddInParameter(command, "vstrAlias", SqlDbType.VarChar, alias);
                        database.AddInParameter(command, "vstrEncryptedCardNumber", SqlDbType.VarChar, encryptedCardNumber);
                        database.AddInParameter(command, "vstrVerificationCode", SqlDbType.VarChar, verificationCode);
                        database.AddInParameter(command, "vstrExpireDate", SqlDbType.VarChar, expireDate);
                        database.AddInParameter(command, "vstrCardHolder", SqlDbType.VarChar, cardHolder);
                        database.AddInParameter(command, "nCreditCardStatusFk", SqlDbType.Int, int.Parse(creditCardStatusId));
                        database.AddInParameter(command, "BillingMethodIdFk", SqlDbType.Int, billingMethodId);
                        returnValue = int.Parse(((decimal)command.ExecuteScalar()).ToString());
                    }
                }
            }
            catch (Exception ex)
            {
                throw DALException.BuildException(ex, DALRange.Commerce, 7);
            }
            return returnValue;
        }

        public void UpdateCreditCardIngenico(int orderId, string cardNumber, string expirationDate, char ingenicoCardType)
        {
            try
            {
                using (var database = Database.CreateInstance(DatabaseSqlCommerce))
                {
                    using (var command = database.GetStoredProcCommand("spPipe_UpdateCreditCardIngenico"))
                    {
                        database.AddInParameter(command, "prmOrderId", SqlDbType.Int, orderId);
                        database.AddInParameter(command, "prmCardNumber", SqlDbType.VarChar, cardNumber);
                        database.AddInParameter(command, "prmExpirationDate", SqlDbType.VarChar, expirationDate);
                        database.AddInParameter(command, "prmIngenicoCardType", SqlDbType.Char, ingenicoCardType);

                        command.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception ex)
            {
                throw DALException.BuildException(ex, DALRange.Commerce, 47);
            }
        }

        public void AddOrderTransactionIngenico(Guid reference, int? orderId, string amount, string currencyCode, string amount2, string currencyCode2, string responseCode, string c3error, string ticketAvailable, string pan, string numAuto, string signatureDemande, string termNum, string typeMedia, string iso2, string cmc7, string cardType, string ssCarte, string timeLoc, string dateFinValidite, string codeService, string typeForcage, string tenderIdent, string certificatVA, string dateTrns, string heureTrns, string pisteK, string depassPuce, string incidentCam, string condSaisie, string optionChoisie, string optionLibelle, string numContexte, string codeRejet, string caiEmetteur, string caiAuto, string trnsNum, string numFile, string userData1, string userData2, string numDossier, string typeFacture, string axis, string numFileV5, string ffu, string paymentId, string charityAmount)
        {
            try
            {
                using (var database = Database.CreateInstance(DatabaseSqlCommerce))
                {
                    using (var command = database.GetStoredProcCommand("spPipe_AddOrderTransactionIngenico"))
                    {
                        database.AddInParameter(command, "Reference", SqlDbType.UniqueIdentifier, reference);

                        if (!orderId.HasValue)
                            database.AddInParameter(command, "Order_Fk", SqlDbType.Int, DBNull.Value);
                        else
                            database.AddInParameter(command, "Order_Fk", SqlDbType.Int, orderId);

                        if (amount != null)
                            database.AddInParameter(command, "Amount", SqlDbType.VarChar, amount);

                        if (currencyCode != null)
                            database.AddInParameter(command, "CurrencyCode", SqlDbType.VarChar, currencyCode);

                        if (amount2 != null)
                            database.AddInParameter(command, "Amount2", SqlDbType.VarChar, amount2);

                        if (currencyCode2 != null)
                            database.AddInParameter(command, "CurrencyCode2", SqlDbType.VarChar, currencyCode2);

                        if (responseCode != null)
                            database.AddInParameter(command, "ResponseCode", SqlDbType.VarChar, responseCode);

                        if (c3error != null)
                            database.AddInParameter(command, "C3Error", SqlDbType.VarChar, c3error);

                        if (ticketAvailable != null)
                            database.AddInParameter(command, "TicketAvailable", SqlDbType.VarChar, ticketAvailable);

                        if (pan != null)
                            database.AddInParameter(command, "Pan", SqlDbType.VarChar, pan);

                        if (numAuto != null)
                            database.AddInParameter(command, "NumAuto", SqlDbType.VarChar, numAuto);

                        if (signatureDemande != null)
                            database.AddInParameter(command, "SignatureDemande", SqlDbType.VarChar, signatureDemande);

                        if (termNum != null)
                            database.AddInParameter(command, "TermNum", SqlDbType.VarChar, termNum);

                        if (typeMedia != null)
                            database.AddInParameter(command, "TypeMedia", SqlDbType.VarChar, typeMedia);

                        if (iso2 != null)
                            database.AddInParameter(command, "Iso2", SqlDbType.VarChar, iso2);

                        if (cmc7 != null)
                            database.AddInParameter(command, "Cmc7", SqlDbType.VarChar, cmc7);

                        if (cardType != null)
                            database.AddInParameter(command, "CardType", SqlDbType.VarChar, cardType);

                        if (ssCarte != null)
                            database.AddInParameter(command, "SSCarte", SqlDbType.VarChar, ssCarte);

                        if (timeLoc != null)
                            database.AddInParameter(command, "TimeLoc", SqlDbType.VarChar, timeLoc);

                        if (dateFinValidite != null)
                            database.AddInParameter(command, "DateFinValidite", SqlDbType.VarChar, dateFinValidite);

                        if (codeService != null)
                            database.AddInParameter(command, "CodeService", SqlDbType.VarChar, codeService);

                        if (typeForcage != null)
                            database.AddInParameter(command, "TypeForcage", SqlDbType.VarChar, typeForcage);

                        if (tenderIdent != null)
                            database.AddInParameter(command, "TenderIdent", SqlDbType.VarChar, tenderIdent);

                        if (certificatVA != null)
                            database.AddInParameter(command, "CertificatVA", SqlDbType.VarChar, certificatVA);

                        if (dateTrns != null)
                            database.AddInParameter(command, "DateTrns", SqlDbType.VarChar, dateTrns);

                        if (heureTrns != null)
                            database.AddInParameter(command, "HeureTrns", SqlDbType.VarChar, heureTrns);

                        if (pisteK != null)
                            database.AddInParameter(command, "PisteK", SqlDbType.VarChar, pisteK);

                        if (depassPuce != null)
                            database.AddInParameter(command, "DepassPuce", SqlDbType.VarChar, depassPuce);

                        if (incidentCam != null)
                            database.AddInParameter(command, "IncidentCam", SqlDbType.VarChar, incidentCam);

                        if (condSaisie != null)
                            database.AddInParameter(command, "CondSaisie", SqlDbType.VarChar, condSaisie);

                        if (optionChoisie != null)
                            database.AddInParameter(command, "OptionChoisie", SqlDbType.VarChar, optionChoisie);

                        if (optionLibelle != null)
                            database.AddInParameter(command, "OptionLibelle", SqlDbType.VarChar, amount);

                        if (numContexte != null)
                            database.AddInParameter(command, "NumContexte", SqlDbType.VarChar, numContexte);

                        if (codeRejet != null)
                            database.AddInParameter(command, "CodeRejet", SqlDbType.VarChar, codeRejet);

                        if (caiEmetteur != null)
                            database.AddInParameter(command, "CAI_Emetteur", SqlDbType.VarChar, caiEmetteur);

                        if (caiAuto != null)
                            database.AddInParameter(command, "CAI_Auto", SqlDbType.VarChar, caiAuto);

                        if (trnsNum != null)
                            database.AddInParameter(command, "TrnsNum", SqlDbType.VarChar, trnsNum);

                        if (numFile != null)
                            database.AddInParameter(command, "NumFile", SqlDbType.VarChar, numFile);

                        if (userData1 != null)
                            database.AddInParameter(command, "UserData1", SqlDbType.VarChar, userData1);

                        if (userData2 != null)
                            database.AddInParameter(command, "UserData2", SqlDbType.VarChar, userData2);

                        if (numDossier != null)
                            database.AddInParameter(command, "NumDossier", SqlDbType.VarChar, numDossier);

                        if (typeFacture != null)
                            database.AddInParameter(command, "TypeFacture", SqlDbType.VarChar, typeFacture);

                        if (axis != null)
                            database.AddInParameter(command, "Axis", SqlDbType.VarChar, axis);

                        if (numFileV5 != null)
                            database.AddInParameter(command, "NumFileV5", SqlDbType.VarChar, numFileV5);

                        if (ffu != null)
                            database.AddInParameter(command, "FFU", SqlDbType.VarChar, ffu);

                        if (paymentId != null)
                            database.AddInParameter(command, "PaymentId", SqlDbType.VarChar, paymentId);

                        if (charityAmount != null)
                            database.AddInParameter(command, "CharityAmount", SqlDbType.VarChar, charityAmount);


                        command.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception ex)
            {
                throw DALException.BuildException(ex, DALRange.Commerce, 46);
            }
        }

        public void UpdateOrderTransactionIngenico(Guid reference, int? orderId, string amount, string currencyCode, string amount2, string currencyCode2, string responseCode, string c3error, string ticketAvailable, string pan, string numAuto, string signatureDemande, string termNum, string typeMedia, string iso2, string cmc7, string cardType, string ssCarte, string timeLoc, string dateFinValidite, string codeService, string typeForcage, string tenderIdent, string certificatVA, string dateTrns, string heureTrns, string pisteK, string depassPuce, string incidentCam, string condSaisie, string optionChoisie, string optionLibelle, string numContexte, string codeRejet, string caiEmetteur, string caiAuto, string trnsNum, string numFile, string userData1, string userData2, string numDossier, string typeFacture, string axis, string numFileV5, string ffu, string paymentId, string charityAmount)
        {
            try
            {
                using (var database = Database.CreateInstance(DatabaseSqlCommerce))
                {
                    using (var command = database.GetStoredProcCommand("spPipe_UpdateOrderTransactionIngenico"))
                    {
                        database.AddInParameter(command, "Reference", SqlDbType.UniqueIdentifier, reference);

                        if (!orderId.HasValue)
                            database.AddInParameter(command, "Order_Fk", SqlDbType.Int, DBNull.Value);
                        else
                            database.AddInParameter(command, "Order_Fk", SqlDbType.Int, orderId);

                        if (amount != null)
                            database.AddInParameter(command, "Amount", SqlDbType.VarChar, amount);

                        if (currencyCode != null)
                            database.AddInParameter(command, "CurrencyCode", SqlDbType.VarChar, currencyCode);

                        if (amount2 != null)
                            database.AddInParameter(command, "Amount2", SqlDbType.VarChar, amount2);

                        if (currencyCode2 != null)
                            database.AddInParameter(command, "CurrencyCode2", SqlDbType.VarChar, currencyCode2);

                        if (responseCode != null)
                            database.AddInParameter(command, "ResponseCode", SqlDbType.VarChar, responseCode);

                        if (c3error != null)
                            database.AddInParameter(command, "C3Error", SqlDbType.VarChar, c3error);

                        if (ticketAvailable != null)
                            database.AddInParameter(command, "TicketAvailable", SqlDbType.VarChar, ticketAvailable);

                        if (pan != null)
                            database.AddInParameter(command, "Pan", SqlDbType.VarChar, pan);

                        if (numAuto != null)
                            database.AddInParameter(command, "NumAuto", SqlDbType.VarChar, numAuto);

                        if (signatureDemande != null)
                            database.AddInParameter(command, "SignatureDemande", SqlDbType.VarChar, signatureDemande);

                        if (termNum != null)
                            database.AddInParameter(command, "TermNum", SqlDbType.VarChar, termNum);

                        if (typeMedia != null)
                            database.AddInParameter(command, "TypeMedia", SqlDbType.VarChar, typeMedia);

                        if (iso2 != null)
                            database.AddInParameter(command, "Iso2", SqlDbType.VarChar, iso2);

                        if (cmc7 != null)
                            database.AddInParameter(command, "Cmc7", SqlDbType.VarChar, cmc7);

                        if (cardType != null)
                            database.AddInParameter(command, "CardType", SqlDbType.VarChar, cardType);

                        if (ssCarte != null)
                            database.AddInParameter(command, "SSCarte", SqlDbType.VarChar, ssCarte);

                        if (timeLoc != null)
                            database.AddInParameter(command, "TimeLoc", SqlDbType.VarChar, timeLoc);

                        if (dateFinValidite != null)
                            database.AddInParameter(command, "DateFinValidite", SqlDbType.VarChar, dateFinValidite);

                        if (codeService != null)
                            database.AddInParameter(command, "CodeService", SqlDbType.VarChar, codeService);

                        if (typeForcage != null)
                            database.AddInParameter(command, "TypeForcage", SqlDbType.VarChar, typeForcage);

                        if (tenderIdent != null)
                            database.AddInParameter(command, "TenderIdent", SqlDbType.VarChar, tenderIdent);

                        if (certificatVA != null)
                            database.AddInParameter(command, "CertificatVA", SqlDbType.VarChar, certificatVA);

                        if (dateTrns != null)
                            database.AddInParameter(command, "DateTrns", SqlDbType.VarChar, dateTrns);

                        if (heureTrns != null)
                            database.AddInParameter(command, "HeureTrns", SqlDbType.VarChar, heureTrns);

                        if (pisteK != null)
                            database.AddInParameter(command, "PisteK", SqlDbType.VarChar, pisteK);

                        if (depassPuce != null)
                            database.AddInParameter(command, "DepassPuce", SqlDbType.VarChar, depassPuce);

                        if (incidentCam != null)
                            database.AddInParameter(command, "IncidentCam", SqlDbType.VarChar, incidentCam);

                        if (condSaisie != null)
                            database.AddInParameter(command, "CondSaisie", SqlDbType.VarChar, condSaisie);

                        if (optionChoisie != null)
                            database.AddInParameter(command, "OptionChoisie", SqlDbType.VarChar, optionChoisie);

                        if (optionLibelle != null)
                            database.AddInParameter(command, "OptionLibelle", SqlDbType.VarChar, amount);

                        if (numContexte != null)
                            database.AddInParameter(command, "NumContexte", SqlDbType.VarChar, numContexte);

                        if (codeRejet != null)
                            database.AddInParameter(command, "CodeRejet", SqlDbType.VarChar, codeRejet);

                        if (caiEmetteur != null)
                            database.AddInParameter(command, "CAI_Emetteur", SqlDbType.VarChar, caiEmetteur);

                        if (caiAuto != null)
                            database.AddInParameter(command, "CAI_Auto", SqlDbType.VarChar, caiAuto);

                        if (trnsNum != null)
                            database.AddInParameter(command, "TrnsNum", SqlDbType.VarChar, trnsNum);

                        if (numFile != null)
                            database.AddInParameter(command, "NumFile", SqlDbType.VarChar, numFile);

                        if (userData1 != null)
                            database.AddInParameter(command, "UserData1", SqlDbType.VarChar, userData1);

                        if (userData2 != null)
                            database.AddInParameter(command, "UserData2", SqlDbType.VarChar, userData2);

                        if (numDossier != null)
                            database.AddInParameter(command, "NumDossier", SqlDbType.VarChar, numDossier);

                        if (typeFacture != null)
                            database.AddInParameter(command, "TypeFacture", SqlDbType.VarChar, typeFacture);

                        if (axis != null)
                            database.AddInParameter(command, "Axis", SqlDbType.VarChar, axis);

                        if (numFileV5 != null)
                            database.AddInParameter(command, "NumFileV5", SqlDbType.VarChar, numFileV5);

                        if (ffu != null)
                            database.AddInParameter(command, "FFU", SqlDbType.VarChar, ffu);

                        if (paymentId != null)
                            database.AddInParameter(command, "PaymentId", SqlDbType.VarChar, paymentId);

                        if (charityAmount != null)
                            database.AddInParameter(command, "CharityAmount", SqlDbType.VarChar, charityAmount);

                        command.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception ex)
            {
                throw DALException.BuildException(ex, DALRange.Commerce, 46);
            }
        }

        public void AddOrderCreditInfo(int orderId, string codeBareme)
        {
            try
            {
                using (var database = Database.CreateInstance(DatabaseSqlCommerce))
                {
                    using (var command = database.GetStoredProcCommand("spPipe_AddOrderCreditInfos"))
                    {
                        database.AddInParameter(command, "nOrderFk", SqlDbType.Int, orderId);
                        database.AddInParameter(command, "vstrCodeBareme", SqlDbType.VarChar, codeBareme);
                        command.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception ex)
            {
                throw DALException.BuildException(ex, DALRange.Commerce, 8);
            }
        }

        public void AddOrderDetailAddOn(int orderDetailFk, int typeArticle, int itemGroup, bool flagVenteService, string partnerName, NumericalAddOn numAddOn, string koboUserId)
        {
            try
            {
                using (var database = Database.CreateInstance(DatabaseSqlCommerce))
                {
                    using (var command = database.GetStoredProcCommand("spPipe_AddOrderDetailAddOn"))
                    {
                        database.AddInParameter(command, "nOrderDetailFk", SqlDbType.Int, orderDetailFk);
                        database.AddInParameter(command, "nTypeArticle", SqlDbType.Int, typeArticle);
                        database.AddInParameter(command, "nItemGroup", SqlDbType.Int, itemGroup);
                        database.AddInParameter(command, "bFlagVenteService", SqlDbType.Bit, flagVenteService);
                        database.AddInParameter(command, "sVenteServicePartner", SqlDbType.VarChar, partnerName);

                        if (numAddOn != null)
                        {
                            if (Switch.IsEnabled("ebook.kobo.v3", false))
                            {
                                database.AddInParameter(command, "sDematExternalRef", SqlDbType.VarChar, numAddOn.ReferenceFeed4);
                            }
                            else
                            {
                                //Lors du décommissionnement KOBO v2 --> voir si ce champs ne sert que pour les ebooks
                                database.AddInParameter(command, "sDematExternalRef", SqlDbType.VarChar, numAddOn.ExternalRef);
                            }
                            database.AddInParameter(command, "sDematDistributorId", SqlDbType.Int, numAddOn.DistributorId);
                            database.AddInParameter(command, "sDematDistributorLabel", SqlDbType.VarChar, numAddOn.DistributorLabel);
                            database.AddInParameter(command, "sDematEditorId", SqlDbType.Int, numAddOn.EditorId);
                            database.AddInParameter(command, "sDematEditorLabel", SqlDbType.VarChar, numAddOn.EditorLabel);
                            database.AddInParameter(command, "sDematFormatId", SqlDbType.Int, numAddOn.FormatId);
                            database.AddInParameter(command, "sDematFormatLabel", SqlDbType.VarChar, numAddOn.FormatLabel);
                            database.AddInParameter(command, "sDematAccountingFamily", SqlDbType.Int, numAddOn.AccountingFamily);
                            database.AddInParameter(command, "sDematFlag", SqlDbType.Int, numAddOn.Flag);
                            database.AddInParameter(command, "sDematInvoicingMode", SqlDbType.Int, numAddOn.InvoicingMode);
                            database.AddInParameter(command, "sDematSubscriptionMode", SqlDbType.Int, numAddOn.SubscriptionMode);
                            database.AddInParameter(command, "sDematPurchasePrice", SqlDbType.Decimal, numAddOn.PurchasePrice);
                            database.AddInParameter(command, "sDematSellingPrice", SqlDbType.Decimal, numAddOn.SellingPrice);
                            database.AddInParameter(command, "sRetailerId", SqlDbType.Int, numAddOn.RetailerId);
                            database.AddInParameter(command, "sRetailerCode", SqlDbType.VarChar, numAddOn.RetailerCode);
                            database.AddInParameter(command, "sRetailerName", SqlDbType.VarChar, numAddOn.RetailerLabel);
                            if (!string.IsNullOrEmpty(koboUserId))
                                database.AddInParameter(command, "koboUserId", SqlDbType.VarChar, koboUserId);

                            database.AddInParameter(command, "DematSellingPriceBeforeTax", SqlDbType.Decimal, numAddOn.SellingPriceBeforeTax);
                        }

                        command.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception ex)
            {
                throw DALException.BuildException(ex, DALRange.Commerce, 12);
            }
        }

        public void AddSplitPrice(int orderFk, decimal dbPriceFrf, decimal dbPriceEur,
            decimal userPriceFrf, decimal userPriceEur, decimal? priceToPayFrf,
            decimal? priceToPayEur, decimal? vatRate, string vatCode)
        {
            try
            {
                using (var database = Database.CreateInstance(DatabaseSqlCommerce))
                {
                    using (var command = database.GetStoredProcCommand("spPipe_AddSplitPrice"))
                    {
                        database.AddInParameter(command, "nOrderFk", SqlDbType.Int, orderFk);
                        database.AddInParameter(command, "numDbPriceFRF", SqlDbType.Decimal, dbPriceFrf);
                        database.AddInParameter(command, "numDbPriceEUR", SqlDbType.Decimal, dbPriceEur);
                        database.AddInParameter(command, "numUserPriceFRF", SqlDbType.Decimal, userPriceFrf);
                        database.AddInParameter(command, "numUserPriceEUR", SqlDbType.Decimal, userPriceEur);
                        if (priceToPayFrf.HasValue)
                            database.AddInParameter(command, "numPriceToPayFRF", SqlDbType.Decimal, priceToPayFrf.Value);
                        else
                            database.AddInParameter(command, "numPriceToPayFRF", SqlDbType.Decimal, null);
                        if (priceToPayEur.HasValue)
                            database.AddInParameter(command, "numPriceToPayEUR", SqlDbType.Decimal, priceToPayEur.Value);
                        else
                            database.AddInParameter(command, "numPriceToPayEUR", SqlDbType.Decimal, null);
                        database.AddInParameter(command, "numVATRate", SqlDbType.Decimal, vatRate.GetValueOrDefault(0));
                        database.AddInParameter(command, "vstrVatCode", SqlDbType.VarChar, vatCode);
                        command.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception ex)
            {
                throw DALException.BuildException(ex, DALRange.Commerce, 16);
            }
        }

        #endregion

        /// <summary>
        /// Calls Commerce DB to retricve User scoring level
        /// </summary>
        /// <param name="accountID">User identifier</param>
        /// <returns>Level</returns>
        public AccountScoring SearchOrderAccountScoring(int accountID)
        {
            var returnvalue = new AccountScoring();
            try
            {
                using (var database = Database.CreateInstance(DatabaseSqlCommerce))
                {
                    using (var command = database.GetStoredProcCommand("spPipe_SearchOrderAccountScoring"))
                    {
                        database.AddInParameter(command, "AccountId", SqlDbType.Int, accountID);
                        using (var reader = command.ExecuteReader())
                        {
                            if (reader.Read())
                            {
                                returnvalue.UserConfidence = reader.GetField<int>("IndiceConfiance");
                                returnvalue.LastOrderDate = reader.GetField<DateTime?>("LastOrderDate");
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw DALException.BuildException(ex, DALRange.Commerce, 20);
            }
            return returnvalue;
        }

        public List<BillingMethodDescriptor> GetBillingMethodDescriptors(string config, string culture)
        {
            var returnvalue = new List<BillingMethodDescriptor>();
            try
            {
                using (var database = Database.CreateInstance(DatabaseSqlCommerce))
                {
                    using (var command = database.GetStoredProcCommand("spPipe_CalcPaymentMethodCombination"))
                    {
                        database.AddInParameter(command, "currentMethods", SqlDbType.Int, 0);
                        database.AddInParameter(command, "allowedMethods", SqlDbType.Int, 0x7fffffff);
                        database.AddInParameter(command, "config", SqlDbType.NVarChar, config);
                        database.AddInParameter(command, "culture", SqlDbType.NVarChar, culture);
                        using (var reader = database.ExecuteReader(command))
                        {
                            while (reader.Read())
                            {
                                var desc = new BillingMethodDescriptor
                                {
                                    Id = DataReaderHelper.GetField<int>(reader, "fld_pm_pk"),
                                    Priority = DataReaderHelper.GetField<int>(reader, "fld_pm_priority"),
                                    Label = DataReaderHelper.GetField<string>(reader, "fld_pm_Label"),
                                    IsActive = DataReaderHelper.GetField<bool>(reader, "fld_pm_Status_fk")
                                };
                                returnvalue.Add(desc);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw DALException.BuildException(ex, DALRange.Commerce, 25);
            }
            return returnvalue;
        }

        public void AddCheck(int orderId, string checkNumber, decimal? expectedAmountEuro)
        {
            try
            {
                using (var database = Database.CreateInstance(DatabaseSqlCommerce))
                {
                    using (var command = database.GetStoredProcCommand("spPipe_AddCheck"))
                    {
                        database.AddInParameter(command, "nOrderFk", SqlDbType.Int, orderId);
                        database.AddInParameter(command, "nCheckNumber", SqlDbType.Int, checkNumber);
                        database.AddInParameter(command, "nExpectedAmountEUR", SqlDbType.Decimal, expectedAmountEuro);

                        command.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception ex)
            {
                throw DALException.BuildException(ex, DALRange.Commerce, 26);
            }
        }

        public WrapMethodList GetListOfAvailableWrapMethods(int logisticType)
        {
            WrapMethodList retValue = null;
            try
            {
                using (var database = Database.CreateInstance(DatabaseSqlCommerce))
                {
                    using (var command = database.GetStoredProcCommand("spPipe_ListAvailableWraps"))
                    {
                        database.AddInParameter(command, "nLogisticType", SqlDbType.Int, logisticType);
                        using (var reader = database.ExecuteReader(command))
                        {
                            retValue = new WrapMethodList();
                            while (reader.Read())
                            {
                                var wrap = new WrapMethod
                                {
                                    Id = reader.GetInt32(reader.GetOrdinal("Id")),
                                    Reference = reader.GetString(reader.GetOrdinal("Reference")),
                                    Label = reader.GetString(reader.GetOrdinal("Label")),
                                    TypeLogistic = reader.GetInt32(reader.GetOrdinal("LogisticType")),
                                    PriceDetailEur = reader.GetDecimal(reader.GetOrdinal("PriceDetailEUR")),
                                    PriceDetailDBEur = reader.GetDecimal(reader.GetOrdinal("PriceDetailEUR")),
                                    PriceGlobalEur = reader.GetDecimal(reader.GetOrdinal("PriceGlobalEUR")),
                                    PriceGlobalDBEur = reader.GetDecimal(reader.GetOrdinal("PriceGlobalEUR"))
                                };
                                retValue.Add(wrap);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw DALException.BuildException(ex, DALRange.Commerce, 27);
            }
            return retValue;
        }

        public SplitMethodList GetListOfAvailableSplitMethods()
        {
            SplitMethodList retValue = null;
            try
            {
                using (var database = Database.CreateInstance(DatabaseSqlCommerce))
                using (var command = database.GetStoredProcCommand("spPipe_GetSplitSelection"))
                using (var reader = database.ExecuteReader(command))
                {
                    retValue = new SplitMethodList();
                    while (reader.Read())
                    {
                        var split = new SplitMethod
                        {
                            ShippingZone = DataReaderHelper.GetField<int>(reader, "ShippingZone"),
                            LogisticType = DataReaderHelper.GetField<int>(reader, "LogType"),
                            MinDelay = DataReaderHelper.GetField<int>(reader, "MinDelay"),
                            MaxDelay = DataReaderHelper.GetField<int>(reader, "MaxDelay"),
                            UnitPrice = DataReaderHelper.GetField<decimal>(reader, "UnitPrice"),
                            GlobalPrice = DataReaderHelper.GetField<decimal>(reader, "GlobalPrice")
                        };
                        retValue.Add(split);
                    }
                }
            }
            catch (Exception ex)
            {
                throw DALException.BuildException(ex, DALRange.Commerce, 28);
            }
            return retValue;
        }

        public List<PreviousLineGroup> GetOrderDetails(string prmszOrderReference, int referenceType)
        {
            var lstOdet = new List<PreviousLineGroup>();
            try
            {
                using (var database = Database.CreateInstance(DatabaseSqlCommerce))
                {
                    using (var command = database.GetStoredProcCommand("spPipe_GetOrderDetails"))
                    {
                        database.AddInParameter(command, "prmOrderReference", SqlDbType.VarChar, prmszOrderReference);
                        database.AddInParameter(command, "referenceType", SqlDbType.Int, referenceType);
                        using (var reader = database.ExecuteReader(command))
                        {
                            while (reader.Read())
                            {
                                int? currentOrderId = null;

                                if (reader.HasColumn("OrderId") && !DBNull.Value.Equals(reader["OrderId"]))
                                {
                                    currentOrderId = int.Parse(reader["OrderId"].ToString());
                                }

                                var lg = lstOdet.Find(delegate (PreviousLineGroup l)
                                {
                                    return l.OrderPk == currentOrderId;
                                });

                                if (lg == null)
                                {
                                    lg = new PreviousLineGroup();
                                    lstOdet.Add(lg);
                                    lg.OrderPk = currentOrderId;
                                    if (referenceType == 1)
                                    {
                                        try
                                        {
                                            lg.OrderReference = new Guid(prmszOrderReference);
                                        }
                                        catch (Exception ex)
                                        {
                                            Logging.Current.WriteError("Function named GetOrderDetails failed : Order reference " + prmszOrderReference + " isn't correctely Formatted ", ex);
                                            throw;
                                        }
                                    }
                                    lg.OrderMessage = reader["OrderMessage"].ToString();
                                    if (int.TryParse(reader["Network"].ToString(), out var NetworkId))
                                    {
                                        switch (NetworkId)
                                        {
                                            case 1:
                                                var p = new PostalAddress
                                                {
                                                    Firstname = reader["FirstName"].ToString(),
                                                    LastName = reader["LastName"].ToString(),
                                                    AddressLine1 = reader["AddressLine1"].ToString(),
                                                    AddressLine2 = reader["AddressLine2"].ToString(),
                                                    AddressLine3 = reader["AddressLine3"].ToString(),
                                                    AddressLine4 = reader["AddressLine4"].ToString(),
                                                    ZipCode = reader["ZipCode"].ToString(),
                                                    City = reader["City"].ToString()
                                                };
                                                lg.ShippingAddress = p;
                                                break;
                                            case 2:
                                                lg.ShippingAddress = new RelayAddress();
                                                break;
                                            case 3:
                                                lg.ShippingAddress = new ShopAddress();
                                                break;
                                        }
                                    }
                                    lg.BillingAddress = new BillingAddress
                                    {
                                        Firstname = reader["BillingFirstName"].ToString(),
                                        Lastname = reader["BillingLastName"].ToString(),
                                        AddressLine1 = reader["BillingAddressLine1"].ToString(),
                                        AddressLine2 = reader["BillingAddressLine2"].ToString(),
                                        AddressLine3 = reader["BillingAddressLine3"].ToString(),
                                        AddressLine4 = reader["BillingAddressLine4"].ToString(),
                                        ZipCode = reader["BillingZipCode"].ToString(),
                                        City = reader["BillingCity"].ToString()
                                    };
                                }

                                if (!string.IsNullOrEmpty(reader["OfferId"].ToString()))
                                {
                                    var mpa = new MarketPlaceArticle
                                    {
                                        OfferId = int.Parse(reader["OfferId"].ToString()),
                                        ProductID = int.Parse(reader["Prid"].ToString()),
                                        Quantity = int.Parse(reader["Quantity"].ToString()),
                                        ShipMethod = int.Parse(reader["ShippingMethod"].ToString())
                                    };
                                    lg.Articles.Add(mpa);
                                }
                                else if (!string.IsNullOrEmpty(reader["MasterId"].ToString()))
                                {

                                    var std = (StandardArticle)lg.Articles.Find(delegate (Model.Article a)
                                    {
                                        return a is StandardArticle && a.ProductID.GetValueOrDefault(0) == int.Parse(reader["MasterId"].ToString());
                                    });
                                    if (std == null)
                                    {
                                        std = new StandardArticle
                                        {
                                            ProductID = int.Parse(reader["MasterId"].ToString()),
                                            Quantity = int.Parse(reader["Quantity"].ToString())
                                        };
                                        lg.Articles.Add(std);
                                    }
                                    var article = new Service
                                    {
                                        MasterArticles = new ArticleList()
                                    };
                                    article.MasterArticles.Add(std);
                                    article.ProductID = int.Parse(reader["Prid"].ToString());
                                    article.Quantity = int.Parse(reader["Quantity"].ToString());
                                    article.ShipMethod = int.Parse(reader["ShippingMethod"].ToString());
                                    std.Services.Add(article);
                                }
                                else
                                {
                                    var std = (StandardArticle)lg.Articles.Find(delegate (Model.Article a)
                                    {
                                        return a is StandardArticle && a.ProductID.GetValueOrDefault(0) == int.Parse(reader["Prid"].ToString());
                                    });
                                    if (std == null)
                                    {
                                        var article = new StandardArticle
                                        {
                                            ProductID = int.Parse(reader["Prid"].ToString()),
                                            Quantity = int.Parse(reader["Quantity"].ToString()),
                                            ShipMethod = int.Parse(reader["ShippingMethod"].ToString())
                                        };
                                        lg.Articles.Add(article);
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw DALException.BuildException(ex, DALRange.Commerce, 31);
            }
            return lstOdet;
        }

        public int GetRelayId(int orderId)
        {
            var relaisId = 0;
            try
            {
                using (var database = Database.CreateInstance(DatabaseSqlCommerce))
                {
                    using (var command = database.GetStoredProcCommand("sp_FB_ORD_GET_OrderDetailSogep"))
                    {
                        database.AddInParameter(command, "intOrderId", SqlDbType.Int, orderId);
                        using (var reader = database.ExecuteReader(command))
                        {
                            while (reader.Read())
                            {
                                relaisId = Convert.ToInt32(reader["FDRelayId"].ToString());
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw DALException.BuildException(ex, DALRange.Commerce, 44);
            }
            return relaisId;
        }

        public int GetShopId(int orderId)
        {
            var shopId = 0;
            try
            {
                using (var database = Database.CreateInstance(DatabaseSqlCommerce))
                {
                    using (var command = database.GetStoredProcCommand("spBTS_Meteor_GET_OrderShopByOrderId"))
                    {
                        database.AddInParameter(command, "orderId", SqlDbType.Int, orderId);
                        using (var reader = database.ExecuteReader(command))
                        {
                            while (reader.Read())
                            {
                                shopId = Convert.ToInt32(reader["ShopEagenda_fk"].ToString());
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw DALException.BuildException(ex, DALRange.Commerce, 45);
            }
            return shopId;
        }

        public string GetAdvantageCode(int orderId)
        {
            string advantageCode = null;
            try
            {
                using (var database = Database.CreateInstance(DatabaseSqlCommerce))
                {
                    using (var command = database.GetStoredProcCommand("spPipe_GetAdvantageCode"))
                    {
                        database.AddInParameter(command, "OrderId", SqlDbType.Int, orderId);
                        using (var reader = database.ExecuteReader(command))
                        {
                            while (reader.Read())
                            {
                                advantageCode = reader["AdvantageCode"].ToString();
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw DALException.BuildException(ex, DALRange.Commerce, 47);
            }
            return advantageCode;
        }

        public List<CustCreditCardDescriptor> GetCreditCards(string config, string culture, bool isUnfiltered)
        {
            var ret = new List<CustCreditCardDescriptor>();
            try
            {
                using (var database = Database.CreateInstance(DatabaseSqlCommerce))
                {
                    using (var command = database.GetStoredProcCommand(isUnfiltered ? "spSubs_GetCreditCardsUnfiltered" : "spPipe_GetCreditCards"))
                    {
                        if (config != null)
                        {
                            database.AddInParameter(command, "config", SqlDbType.VarChar, config);
                        }

                        if (!string.IsNullOrEmpty(culture))
                        {
                            database.AddInParameter(command, "Language", SqlDbType.VarChar, culture);
                        }

                        using (var reader = database.ExecuteReader(command))
                        {
                            while (reader.Read())
                            {
                                var ccd = new CustCreditCardDescriptor
                                {
                                    Id = DataReaderHelper.GetField<int>(reader, "fld_acct_pk"),
                                    Label = DataReaderHelper.GetField<string>(reader, "fld_acct_Label"),
                                    HasExpDate = !DataReaderHelper.GetField<bool>(reader, "fld_acct_EmptyExpDate"),
                                    Prefix = DataReaderHelper.GetField<string>(reader, "fld_acct_CardPrefix"),
                                    Format = DataReaderHelper.GetField<string>(reader, "fld_acct_Format"),
                                    EndMask = DataReaderHelper.GetField<int>(reader, "fld_acct_EndMaskSize"),
                                    DefaultSelection = DataReaderHelper.GetField<bool>(reader, "fld_acct_Default"),
                                    CryptoLength = DataReaderHelper.GetField<int>(reader, "fld_acct_CryptoFlag_Length"),
                                    BeginMask = DataReaderHelper.GetField<int>(reader, "fld_acct_BeginMaskSize"),
                                    CreditCardPaymentMethod = (CreditCardPaymentMethodType)Enum.Parse(typeof(CreditCardPaymentMethodType), DataReaderHelper.GetField<int>(reader, "fld_acct_CreditCardPaymentMethodType_fk").ToString()),
                                    Brand = DataReaderHelper.GetField<string>(reader, "fld_acct_OgoneBrand"),
                                    PaymentMethod = DataReaderHelper.GetField<string>(reader, "fld_acct_OgonePaymentMethod")
                                };
                                if (!string.IsNullOrEmpty(DataReaderHelper.GetField<string>(reader, "fld_acct_debitType_fk")))
                                    ccd.DebitType = (DebitType)Enum.Parse(typeof(DebitType), DataReaderHelper.GetField<int>(reader, "fld_acct_debitType_fk").ToString());
                                if (!string.IsNullOrEmpty(DataReaderHelper.GetField<string>(reader, "fld_acct_groupType_fk")))
                                    ccd.GroupType = DataReaderHelper.GetField<int>(reader, "fld_acct_groupType_fk");
                                if (reader.HasColumn("fld_acct_OgoneTransactionProcess")
                                    && !string.IsNullOrEmpty(DataReaderHelper.GetField<string>(reader, "fld_acct_OgoneTransactionProcess")))
                                    ccd.TransactionProcess = DataReaderHelper.GetField<string>(reader, "fld_acct_OgoneTransactionProcess");

                                ret.Add(ccd);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw DALException.BuildException(ex, DALRange.Commerce, 32);
            }
            return ret;


        }

        public List<CreditCardGroup> GetCreditCardGroups(SiteContext context)
        {
            var CreditCardGroups = new List<CreditCardGroup>();
            try
            {
                using (var database = Database.CreateInstance(DatabaseSqlCommerce))
                {
                    using (var command = database.GetStoredProcCommand("spPipe_GetCreditCardGroup"))
                    {
                        database.AddInParameter(command, "culture", SqlDbType.Char, context.Culture);
                        using (var reader = database.ExecuteReader(command))
                        {
                            while (reader.Read())
                            {
                                var ccg = new CreditCardGroup
                                {
                                    Id = DataReaderHelper.GetField<int>(reader, "Id"),
                                    GroupLabel = DataReaderHelper.GetField<string>(reader, "GroupLabel")
                                };
                                CreditCardGroups.Add(ccg);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw DALException.BuildException(ex, DALRange.Commerce, 33);
            }
            return CreditCardGroups;
        }

        public List<string> GetECards()
        {
            var ret = new List<string>();
            try
            {
                using (var database = Database.CreateInstance(DatabaseSqlCommerce))
                {
                    using (var command = database.GetStoredProcCommand("spPipe_GetECardPrefixes"))
                    {
                        using (var reader = database.ExecuteReader(command))
                        {
                            while (reader.Read())
                            {
                                ret.Add(DataReaderHelper.GetField<string>(reader, "prefix"));
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw DALException.BuildException(ex, DALRange.Commerce, 33);
            }
            return ret;
        }

        public string GetECardsRange()
        {
            var ret = string.Empty;
            try
            {
                using (var database = Database.CreateInstance(DatabaseSqlCommerce))
                {
                    using (var command = database.GetStoredProcCommand("spPipe_GetECardPrefixesRange"))
                    {
                        using (var reader = database.ExecuteReader(command))
                        {
                            if (reader.Read())
                            {
                                ret = DataReaderHelper.GetField<string>(reader, "prefix");
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw DALException.BuildException(ex, DALRange.Commerce, 33);
            }
            return ret;
        }

        public List<Credit> GetCredits(SiteContext context)
        {
            try
            {
                var lstCredits = new List<Credit>();
                using (var database = Database.CreateInstance(DatabaseSqlCommerce))
                {
                    using (var command = database.GetStoredProcCommand("spCredit_GetCredits"))
                    {
                        database.AddInParameter(command, "culture", SqlDbType.Char, context.Culture);
                        database.AddInParameter(command, "environment", SqlDbType.Int, int.Parse(context.Environment));
                        using (var reader = database.ExecuteReader(command))
                        {
                            while (reader.Read())
                            {
                                var credit = new Credit
                                {
                                    ShortLabel = DataReaderHelper.GetField<string>(reader, "ShortLabel"),
                                    Label = DataReaderHelper.GetField<string>(reader, "Label"),
                                    AmountMin = DataReaderHelper.GetField<decimal>(reader, "fld_ci_AmountMin"),
                                    AmountMax = DataReaderHelper.GetField<decimal>(reader, "fld_ci_AmountMax"),
                                    AmountPart = DataReaderHelper.GetField<decimal>(reader, "fld_ci_AmountPart"),
                                    CostByPart = DataReaderHelper.GetField<decimal>(reader, "fld_ci_CostByPart"),
                                    CreditType = DataReaderHelper.GetField<int>(reader, "fld_ci_CreditType"),
                                    NbMonth = DataReaderHelper.GetField<int>(reader, "fld_ci_Nb_Month"),
                                    TxForCalcul = DataReaderHelper.GetField<decimal>(reader, "fld_ci_Tx_ForCalcul"),
                                    StartDate = DataReaderHelper.GetField<DateTime>(reader, "fld_ci_StartDate"),
                                    EndDate = DataReaderHelper.GetField<DateTime>(reader, "fld_ci_EndDate"),
                                    Id = DataReaderHelper.GetField<string>(reader, "Id"),
                                    LegalText = DataReaderHelper.GetField<string>(reader, "fld_ci_LegalText")
                                };
                                var s = DataReaderHelper.GetField<string>(reader, "fld_ci_TreeNodes");

                                var lst = new List<int>();
                                foreach (var a in s.Split(',', ' ', ';'))
                                {
                                    if (int.TryParse(a, out var node))
                                        lst.Add(node);
                                }
                                if (lst.Count > 0)
                                    credit.ApplicationTreeNodeList = lst;

                                lstCredits.Add(credit);
                            }
                            reader.Close();
                        }
                    }
                }
                return lstCredits;
            }
            catch (Exception ex)
            {
                throw DALException.BuildException(ex, DALRange.Commerce, 34);
            }
        }

        public string GetReferenceId()
        {

            string returnValue = null;
            try
            {
                using (var database = Database.CreateInstance(DatabaseSqlCommerce))
                {
                    using (var command = database.GetStoredProcCommand("spPipe_GetReferenceIdForOrder"))
                    {
                        returnValue = (string)command.ExecuteScalar();
                    }
                }
            }
            catch (Exception ex)
            {
                throw DALException.BuildException(ex, DALRange.Commerce, 35);
            }
            return returnValue;
        }

        /// <summary>
        /// Insère une ligne dans la table tbl_orerPayByRef 
        /// </summary>
        /// <param name="orderId">Id de la commande associée au payement par ref </param>
        /// <param name="transactionReference">la réference de la transaction de payement </param>
        /// <param name="payementReference">la réference sur 9 caractères </param>
        /// <param name="merchantId">ID du marchand </param>
        /// <returns>Id technique de la ligne insérée (>0)</returns>
        public void AddPaymentByReference(int? orderId,
                                          Guid transactionReference,
                                          string payementReference,
                                          string merchantId)
        {
            try
            {
                using (var database = Database.CreateInstance(DatabaseSqlCommerce))
                {
                    using (var command = database.GetStoredProcCommand("spPipe_AddOrderPayByReference"))
                    {
                        if (orderId.HasValue)
                        {
                            database.AddInParameter(command, "OrderFk", SqlDbType.Int, orderId);
                        }
                        else
                        {
                            database.AddInParameter(command, "OrderFk", SqlDbType.Int, DBNull.Value);
                        }

                        database.AddInParameter(command, "TransactionReference", SqlDbType.UniqueIdentifier, transactionReference);
                        database.AddInParameter(command, "PaymentReference", SqlDbType.Int, payementReference);
                        database.AddInParameter(command, "MerchantId", SqlDbType.VarChar, merchantId);
                        command.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception ex)
            {
                throw DALException.BuildException(ex, DALRange.Commerce, 36);
            }
        }

        /// <summary>
        /// Insere une nouvelle ligne dans la table Tbl_ordershop afin d'ajouter une nouvelle
        /// commande magasin
        /// </summary>
        public bool AddOrderShop(int orderId, DateTime? withdrawalDate, string invoiceGUPTRef,
            string refUg, int shopIdDestination, string mailText, string mailHtml, string legalEntity,
            string vendorCode, string vendorLogin, string email, string phoneNumber,
            string mobilePhoneNumber, int? communicationMode, int shopIdOrigin, DateTime? cancelDate, string numCaissier, string rebateValidatorCode)
        {
            var returnValue = false;
            try
            {
                using (var database = Database.CreateInstance(DatabaseSqlCommerce))
                {
                    using (var command = database.GetStoredProcCommand("spPipe_AddOrderShop"))
                    {
                        database.AddInParameter(command, "nOrderFkPk", SqlDbType.Int, orderId);
                        if (withdrawalDate != null)
                            database.AddInParameter(command, "dtwithdrawalDate", SqlDbType.DateTime, withdrawalDate);
                        else
                            database.AddInParameter(command, "dtwithdrawalDate", SqlDbType.DateTime, new DateTime(1900, 01, 01));
                        database.AddInParameter(command, "vstrUGRef", SqlDbType.NVarChar, (refUg ?? "").PadLeft(4, '0'));
                        database.AddInParameter(command, "vstrInvoiceGUPTRef", SqlDbType.NVarChar, string.IsNullOrEmpty(invoiceGUPTRef) ? "" : invoiceGUPTRef);
                        database.AddInParameter(command, "nEAGShopFk", SqlDbType.Int, shopIdDestination);

                        database.AddInParameter(command, "vstrMailtext", SqlDbType.Text, string.IsNullOrEmpty(mailText) ? string.Empty : mailText);
                        database.AddInParameter(command, "vstrMailHtml", SqlDbType.Text, string.IsNullOrEmpty(mailHtml) ? string.Empty : mailHtml);
                        database.AddInParameter(command, "vstrLegalEntity", SqlDbType.NVarChar, legalEntity);
                        database.AddInParameter(command, "vstrVendorCode", SqlDbType.NVarChar, vendorCode);
                        database.AddInParameter(command, "vstrVendorLogin", SqlDbType.NVarChar, vendorLogin);
                        if (!string.IsNullOrEmpty(email))
                            database.AddInParameter(command, "vstrEmail", SqlDbType.NVarChar, email);
                        if (!string.IsNullOrEmpty(phoneNumber))
                            database.AddInParameter(command, "vstrTel", SqlDbType.NVarChar, phoneNumber);
                        if (!string.IsNullOrEmpty(mobilePhoneNumber))
                            database.AddInParameter(command, "vstrCellPhone", SqlDbType.NVarChar, mobilePhoneNumber);
                        if (communicationMode.HasValue)
                            database.AddInParameter(command, "viModeComm", SqlDbType.Int, communicationMode.Value);
                        else
                            database.AddInParameter(command, "viModeComm", SqlDbType.Int, 0);
                        database.AddInParameter(command, "viOrigineShopId", SqlDbType.Int, shopIdOrigin);

                        if (cancelDate.HasValue)
                            database.AddInParameter(command, "dtCancelDate", SqlDbType.DateTime, cancelDate);

                        if (!string.IsNullOrEmpty(numCaissier))
                            database.AddInParameter(command, "vstrNumCaissier", SqlDbType.NVarChar, numCaissier);

                        if (!string.IsNullOrWhiteSpace(rebateValidatorCode))
                            database.AddInParameter(command, "vstrRebateValidatorCode", SqlDbType.NVarChar, rebateValidatorCode);

                        command.ExecuteNonQuery();
                        returnValue = true;

                    }
                }
            }
            catch (Exception ex)
            {
                ex.Data.Add("AddOrderShop.orderId", orderId);
                ex.Data.Add("AddOrderShop.withdrawalDate", withdrawalDate);
                ex.Data.Add("AddOrderShop.InvoiceGUPTRef", invoiceGUPTRef);
                ex.Data.Add("AddOrderShop.refUg", refUg);
                ex.Data.Add("AddOrderShop.shopIdDestination", shopIdDestination);
                ex.Data.Add("AddOrderShop.mailText", mailText);
                ex.Data.Add("AddOrderShop.mailHtml", mailHtml);
                ex.Data.Add("AddOrderShop.legalEntity", legalEntity);
                ex.Data.Add("AddOrderShop.vendorCode", vendorCode);
                ex.Data.Add("AddOrderShop.vendorLogin", vendorLogin);
                ex.Data.Add("AddOrderShop.email", email);
                ex.Data.Add("AddOrderShop.phoneNumber", phoneNumber);
                ex.Data.Add("AddOrderShop.mobilePhoneNumber", mobilePhoneNumber);
                ex.Data.Add("AddOrderShop.communicationMode", communicationMode);
                ex.Data.Add("AddOrderShop.shopIdOrigin", shopIdOrigin);
                throw DALException.BuildException(ex, DALRange.Commerce, 39);
            }
            return returnValue;
        }

        /// <summary>
        /// Mets à jour la ligne de tbl_OrderShop correspondant à l'OrderID avec les informations passées en paramètres
        /// </summary>
        /// <param name="orderId">Identifiant de la commande concernée</param>
        /// <param name="numCaisse">Numéro de caisse utilisé pour le paiement par TPE</param>
        /// <returns>True si l'insertion a été effectuée, false sinon</returns>
        public bool UpdateOrderShop(int orderId, string numCaisse)
        {
            var result = false;
            try
            {
                using (var database = Database.CreateInstance(DatabaseSqlCommerce))
                {
                    using (var command = database.GetStoredProcCommand("spPipe_UpdateOrderShop"))
                    {
                        database.AddInParameter(command, "orderId", SqlDbType.Int, orderId);
                        if (numCaisse == null)
                            database.AddInParameter(command, "numCaisse", SqlDbType.NVarChar, DBNull.Value);
                        else
                            database.AddInParameter(command, "numCaisse", SqlDbType.NVarChar, numCaisse);
                        command.ExecuteNonQuery();
                        result = true;
                    }
                }
            }
            catch (Exception ex)
            {
                ex.Data.Add("UpdateOrderShop.orderId", orderId);
                ex.Data.Add("UpdateOrderShop.numCaisse", numCaisse);
                throw DALException.BuildException(ex, DALRange.Commerce, 39);
            }
            return result;
        }

        public int AddOrderShopTransaction(int orderId, string refUg, int accountId, string orderUserReference, int? orderType)
        {
            var returnValue = -1;
            try
            {
                using (var database = Database.CreateInstance(DatabaseSqlCommerce))
                {
                    using (var command = database.GetStoredProcCommand("spPipe_AddOrderShopTransactionV2"))
                    {
                        database.AddInParameter(command, "orderId", SqlDbType.Int, orderId);
                        database.AddInParameter(command, "accountId", SqlDbType.Int, accountId);
                        database.AddInParameter(command, "userreference", SqlDbType.VarChar, orderUserReference);
                        database.AddInParameter(command, "refUg", SqlDbType.VarChar, refUg);
                        database.AddInParameter(command, "orderType", SqlDbType.Int, orderType);

                        returnValue = (int)command.ExecuteScalar();
                    }
                }
            }
            catch (Exception ex)
            {
                ex.Data.Add("AddOrderShopTransaction.orderId", orderId);
                ex.Data.Add("AddOrderShopTransaction.accountId", accountId);
                ex.Data.Add("AddOrderShopTransaction.refUg", refUg);
                ex.Data.Add("AddOrderShopTransaction.userreference", orderUserReference);
                throw DALException.BuildException(ex, DALRange.Commerce, 42);
            }
            return returnValue;
        }

        /// <summary>
        /// Pour FDV, permet de forcer le statut d'une transaction pour Meteor
        /// </summary>
        public bool UpdateOrderShopTransactionStatus(int pTransactionId, int pOrderId, OrderShopTransactionStatus pStatusId)
        {
            var returnValue = false;
            try
            {
                using (var database = Database.CreateInstance(DatabaseSqlCommerce))
                {
                    using (var command = database.GetStoredProcCommand("spPipe_UpdateOrderShopTransaction"))
                    {
                        database.AddInParameter(command, "orderId", SqlDbType.Int, pOrderId);
                        database.AddInParameter(command, "transactionId", SqlDbType.Int, pTransactionId);
                        database.AddInParameter(command, "statusId", SqlDbType.Int, pStatusId);
                        command.ExecuteNonQuery();
                        returnValue = true;
                    }
                }
            }
            catch (Exception ex)
            {
                ex.Data.Add("UpdateOrderShopTransaction.orderId", pOrderId);
                ex.Data.Add("UpdateOrderShopTransaction.transactionId", pTransactionId);
                ex.Data.Add("UpdateOrderShopTransaction.statusID", pStatusId);
                throw DALException.BuildException(ex, DALRange.Commerce, 42);
            }
            return returnValue;
        }

        /// <summary>
        /// Pour POP, Permet de mettre à jour la donnée guptInvoiceRef après l'appel s3
        /// </summary>
        public void UpdateOrderShopTransactionGuptInvoiceRef(int transactionId, string guptInvoiceRef)
        {
            try
            {
                using (var database = Database.CreateInstance(DatabaseSqlCommerce))
                {
                    using (var command = database.GetStoredProcCommand("spPipe_UpdateOrderShopTransactionV2"))
                    {
                        database.AddInParameter(command, "transactionId", SqlDbType.Int, transactionId);
                        database.AddInParameter(command, "guptInvoiceRef", SqlDbType.VarChar, guptInvoiceRef);
                        command.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception ex)
            {
                ex.Data.Add("UpdateOrderShopTransaction.transactionId", transactionId);
                ex.Data.Add("UpdateOrderShopTransaction.guptInvoiceRef", guptInvoiceRef);
                throw DALException.BuildException(ex, DALRange.Commerce, 42);
            }
        }

        public int GetMainOrderId(string orderUserReference)
        {
            var mainOrderId = -1;

            using (var database = Database.CreateInstance(DatabaseSqlCommerce))
            {
                using (var command = database.GetStoredProcCommand("spPipe_GET_MainOrderId_ByUserReference"))
                {
                    database.AddInParameter(command, "OrderUserReference", SqlDbType.VarChar, orderUserReference);
                    using (var reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            mainOrderId = reader.GetField<int>("MainOrderId");
                        }
                    }
                }
            }
            return mainOrderId;
        }

        /// <summary>
        /// Permet de récupérer les motifs de remises en fonction du type souhaité
        /// </summary>
        /// <param name="pCodeType">Type de remise souhaité</param>
        /// <param name="pIsShop">Indique si l'on récupère les remises magasin ou non (ou toutes si null)</param>
        /// <returns></returns>
        public IEnumerable<RemiseClient> GetOrderDiscountReasonsByCodeType(int pCodeType, bool? pIsShop = null)
        {
            var returnValue = new List<RemiseClient>();
            try
            {
                using (var db = Database.CreateInstance(DatabaseSqlCommerce))
                {
                    using (var command = db.GetStoredProcCommand("spPipe_GetOrderDiscountReasonsByCodeType"))
                    {
                        db.AddInParameter(command, "pCodeType", SqlDbType.Int, pCodeType);
                        if (pIsShop.HasValue)
                            db.AddInParameter(command, "pIsShop", SqlDbType.Bit, pIsShop.Value);
                        else
                            db.AddInParameter(command, "pIsShop", SqlDbType.Bit, DBNull.Value);
                        using (var reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                returnValue.Add(
                                    new RemiseClient()
                                    {
                                        OrderDiscountId = DataReaderHelper.GetField<int>(reader, "DiscountReasonId"),
                                        CodeMotif = DataReaderHelper.GetField<int>(reader, "CodeMotif"),
                                        IsVisible = DataReaderHelper.GetField<bool>(reader, "isVisible"),
                                        MotifRemise = DataReaderHelper.GetField<string>(reader, "DiscountReasonLabel"),
                                        OrdreAffichage = DataReaderHelper.GetField<int>(reader, "VisibilityOrderNumber")
                                    }
                                        );
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ex.Data.Add("GetOrderDiscountReasonsByCodeType", pCodeType);
                throw DALException.BuildException(ex, DALRange.Commerce, 39);
            }
            return returnValue;
        }

        /// <summary>
        /// Insère les informations de prévenance
        /// </summary>
        /// <param name="pOrderId">Id de la commande</param>
        /// <param name="pContactMail">Mail du contact</param>
        /// <param name="pContactPhoneNumber">Téléphone du contact</param>
        /// <param name="pContactInfoType">Type de prévenance</param>
        public void AddContactInfos(int pOrderId, string pContactMail, string pContactPhoneNumber, int pContactInfoType)
        {
            try
            {
                using (var database = Database.CreateInstance(DatabaseSqlCommerce))
                {
                    using (var command = database.GetStoredProcCommand("spPipe_AddContactInfos"))
                    {
                        database.AddInParameter(command, "orderId", SqlDbType.Int, pOrderId);
                        database.AddInParameter(command, "vstrContactMail", SqlDbType.NVarChar, pContactMail);
                        database.AddInParameter(command, "vstrContactPhoneNumber", SqlDbType.NVarChar, pContactPhoneNumber);
                        database.AddInParameter(command, "contactInfoType", SqlDbType.Int, pContactInfoType);
                        command.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception ex)
            {
                ex.Data.Add("InsertContactInfos.OrderId", pOrderId);
                ex.Data.Add("InsertContactInfos.ContactMail", pContactMail);
                ex.Data.Add("InsertContactInfos.ContactPhoneNumber", pContactPhoneNumber);
                ex.Data.Add("InsertContactInfos.ContactInfoType", pContactInfoType);
                throw DALException.BuildException(ex, DALRange.Commerce, 43);
            }
        }

        /// <summary>
        /// Ajout les informations sur toutes les souscriptions active du client.
        /// </summary>
        /// <param name="orderId"></param>
        /// <param name="subscriptionType"></param>
        /// <param name="isTrial"></param>
        public void AddOrderAccountSubscription(int orderId, int subscriptionType, bool isTrial)
        {
            try
            {
                using (var database = Database.CreateInstance(DatabaseSqlCommerce))
                {
                    using (var command = database.GetStoredProcCommand("spPipe_AddOrderAccountSubscription"))
                    {
                        database.AddInParameter(command, "orderId", SqlDbType.Int, orderId);
                        database.AddInParameter(command, "subscriptionTypeId", SqlDbType.Int, subscriptionType);
                        database.AddInParameter(command, "isTrial", SqlDbType.Int, isTrial ? 1 : 0);
                        command.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception ex)
            {
                ex.Data.Add("AddOrderAccountSubscription.orderId", orderId);
                ex.Data.Add("AddOrderAccountSubscription.subscriptionType", subscriptionType);
                ex.Data.Add("AddOrderAccountSubscription.isTrial", isTrial);
                throw DALException.BuildException(ex, DALRange.Commerce, 44);
            }
        }

        public IDictionary<int, int> GetPaymentMethodMatrix(string config)
        {
            var paymentMethodMatrix = new Dictionary<int, int>();

            try
            {
                using (var database = Database.CreateInstance(DatabaseSqlCommerce))
                {
                    using (var command = database.GetStoredProcCommand("spPipe_GetPaymentMethodMatrix"))
                    {
                        database.AddInParameter(command, "config", SqlDbType.NVarChar, config);

                        using (var reader = database.ExecuteReader(command))
                        {
                            while (reader.Read())
                            {
                                var paymentMethodId = reader.GetInt32(reader.GetOrdinal("fld_pmm_PaymentMethod_fk_pk"));
                                var allowedCombination = reader.GetInt32(reader.GetOrdinal("fld_pmm_AllowedCombination"));

                                paymentMethodMatrix.Add(paymentMethodId, allowedCombination);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw DALException.BuildException(ex, DALRange.Commerce, 1);
            }

            return paymentMethodMatrix;
        }

        public int AddRemoteRebate(RemoteRebate remoteRebate)
        {
            try
            {
                using (var database = Database.CreateInstance(DatabaseSqlCommerce))
                {
                    using (var command = database.GetStoredProcCommand("spPipe_AddRemoteRebate"))
                    {
                        database.AddInParameter(command, "shopId", SqlDbType.Int, remoteRebate.ShopId);
                        database.AddInParameter(command, "content", SqlDbType.VarChar, remoteRebate.Content);
                        database.AddInParameter(command, "sellerMatricule", SqlDbType.VarChar, remoteRebate.SellerMatricule);
                        database.AddInParameter(command, "sellerPoste", SqlDbType.VarChar, remoteRebate.SellerPoste);
                        database.AddInParameter(command, "creationDate", SqlDbType.DateTime, remoteRebate.CreationDate);
                        database.AddInParameter(command, "sellerComment", SqlDbType.VarChar, remoteRebate.SellerComment);

                        var ret = database.AddReturnParameter(command);

                        command.ExecuteNonQuery();

                        return (int)ret.Value;
                    }
                }
            }
            catch (Exception ex)
            {
                throw DALException.BuildException(ex, DALRange.Commerce, 1);
            }
        }

        public RemoteRebateStatus UpdateRemoteRebate(int remoteRebateId, RemoteRebateStatus rebateStatus, string respMatricule, string respComment = null)
        {
            try
            {
                using (var database = Database.CreateInstance(DatabaseSqlCommerce))
                {
                    using (var command = database.GetStoredProcCommand("spPipe_UpdateRemoteRebate"))
                    {
                        database.AddInParameter(command, "remoteRebateId", SqlDbType.Int, remoteRebateId);
                        database.AddInParameter(command, "rebateStatus", SqlDbType.Int, (int)rebateStatus);
                        database.AddInParameter(command, "respMatricule", SqlDbType.VarChar, respMatricule);
                        database.AddInParameter(command, "respComment", SqlDbType.VarChar, respComment);

                        var ret = database.AddReturnParameter(command);

                        command.ExecuteNonQuery();

                        return (RemoteRebateStatus)ret.Value;
                    }
                }
            }
            catch (Exception ex)
            {
                throw DALException.BuildException(ex, DALRange.Commerce, 1);
            }
        }

        public PopOrderForm GetOrderByOrderReference(string orderReference, PopOrderForm popOrderForm)
        {
            ShippingAddress shippingAddress = null;

            var lineGroup = new PopLineGroup();
            var logisticLineGroup = new LogisticLineGroup();
            var groups = new List<Group>();

            try
            {
                using (var database = Database.CreateInstance(DatabaseSqlCommerce))
                {
                    using (var command = database.GetStoredProcCommand("spPipe_GetOrderDetailsByOrderReference"))
                    {
                        database.AddInParameter(command, "OrderReference", SqlDbType.UniqueIdentifier, Guid.Parse(orderReference));

                        using (var reader = database.ExecuteReader(command))
                        {
                            //order info
                            while (reader.Read())
                            {
                                if (popOrderForm.UserContextInformations == null)
                                {
                                    popOrderForm.UserContextInformations = new Model.UserInformations.UserContextInformations();
                                }
                                popOrderForm.UserContextInformations.AccountId = DataReaderHelper.GetField<int>(reader, "AccountId");
                                popOrderForm.UserContextInformations.ContactEmail = DataReaderHelper.GetField<string>(reader, "ContactEmail");
                                popOrderForm.UserContextInformations.ClientCard = "";

                                if (popOrderForm.OrderContextInformations == null)
                                {
                                    popOrderForm.OrderContextInformations = new OrderInfo();
                                }
                                popOrderForm.OrderContextInformations.Affiliate = DataReaderHelper.GetField<string>(reader, "OrderAffiliateId");
                                popOrderForm.OrderContextInformations.OrderInSession = DataReaderHelper.GetField<int>(reader, "OrderInSession") == 1;
                                popOrderForm.OrderContextInformations.WebFarm = DataReaderHelper.GetField<string>(reader, "OrderWebFarm");
                                popOrderForm.OrderContextInformations.IPAddress = DataReaderHelper.GetField<string>(reader, "OrderIpAdress");
                                popOrderForm.OrderGlobalInformations.MainOrderPk = DataReaderHelper.GetField<int>(reader, "MainOrderId");
                                popOrderForm.OrderGlobalInformations.PaymentReference = DataReaderHelper.GetField<string>(reader, "OrderPaymentRef");
                                popOrderForm.OrderContextInformations.MasterOrder = DataReaderHelper.GetField<int>(reader, "MasterOrderId");
                                popOrderForm.OrderGlobalInformations.BankAccountNumber = DataReaderHelper.GetField<string>(reader, "BankAccountNumber");
                                popOrderForm.OrderGlobalInformations.MainOrderReference = Guid.Parse(orderReference);

                                popOrderForm.QuoteInfos = new QuoteInfos
                                {
                                    QuoteId = DataReaderHelper.GetField<int>(reader, "QuoteId"),
                                    QuoteReference = DataReaderHelper.GetField<string>(reader, "QuoteUserReference")
                                };
                                popOrderForm.LineGroups = new List<PopLineGroup>();

                                lineGroup.Seller.SellerId = DataReaderHelper.GetField<int>(reader, "SellerId");

                                lineGroup.Informations.OrderDate = DataReaderHelper.GetField<DateTime>(reader, "OrderDate");
                                lineGroup.Informations.GiftOption = DataReaderHelper.GetField<bool>(reader, "IsAGift");
                                lineGroup.Informations.OrderStatusId = DataReaderHelper.GetField<int>(reader, "StatusId");
                                lineGroup.Informations.OrderMessage = DataReaderHelper.GetField<string>(reader, "OrderMessage");
                                lineGroup.Informations.OrderType = DataReaderHelper.GetField<int>(reader, "OrderTypeId");

                                lineGroup.IsVoluminous = DataReaderHelper.GetField<bool>(reader, "IsVoluminous");
                                lineGroup.VatCountry = new VatCountry
                                {
                                    VatCountryId = DataReaderHelper.GetField<int>(reader, "VatCountryId"),
                                    UseVat = DataReaderHelper.GetField<int>(reader, "UseVat") == 1,
                                };

                                popOrderForm.LogisticLineGroups = new List<LogisticLineGroup>();
                                logisticLineGroup.Seller.SellerId = DataReaderHelper.GetField<int>(reader, "SellerId");

                                logisticLineGroup.Informations = new PopLineGroupInformations(lineGroup.Informations as dynamic);

                                logisticLineGroup.OrderPk = DataReaderHelper.GetField<int>(reader, "OrderId");
                                logisticLineGroup.OrderUserReference = DataReaderHelper.GetField<string>(reader, "UserReference");
                                logisticLineGroup.OrderReference = Guid.Parse(orderReference);
                                logisticLineGroup.OrderDate = DataReaderHelper.GetField<DateTime>(reader, "OrderDate");

                            }

                            reader.NextResult();

                            // shipping fees infos

                            while (reader.Read())
                            {
                                var group = new Group
                                {
                                    Id = DataReaderHelper.GetField<int>(reader, "smLogiticTypeId"),
                                    GlobalPrice = new GenericPrice { Cost = DataReaderHelper.GetField<decimal>(reader, "smDbPriceEur") },
                                    Items = new List<ItemInGroup>()
                                };

                                groups.Add(group);
                            }

                            logisticLineGroup.SelectedShippingChoice = new ShippingChoice
                            {
                                Groups = groups,
                                Parcels = new Dictionary<int, Parcel>()
                            };

                            reader.NextResult();

                            // billing address informations
                            while (reader.Read())
                            {
                                popOrderForm.BillingAddress = new BillingAddress
                                {
                                    Company = DataReaderHelper.GetField<string>(reader, "baCompany"),
                                    Lastname = DataReaderHelper.GetField<string>(reader, "baLastName"),
                                    Firstname = DataReaderHelper.GetField<string>(reader, "baFirstName"),
                                    AddressLine1 = DataReaderHelper.GetField<string>(reader, "baAddress1"),
                                    AddressLine2 = DataReaderHelper.GetField<string>(reader, "baAddress2"),
                                    AddressLine3 = DataReaderHelper.GetField<string>(reader, "baAddress3"),
                                    AddressLine4 = DataReaderHelper.GetField<string>(reader, "baAddress4"),
                                    ZipCode = DataReaderHelper.GetField<string>(reader, "baZipCode"),
                                    City = DataReaderHelper.GetField<string>(reader, "baCity"),
                                    State = DataReaderHelper.GetField<string>(reader, "baState"),
                                    CountryId = DataReaderHelper.GetField<int>(reader, "baCountryId"),
                                    Phone = DataReaderHelper.GetField<string>(reader, "baPhone"),
                                    Fax = DataReaderHelper.GetField<string>(reader, "baFax"),
                                    Identity = 0
                                };
                            }

                            reader.NextResult();

                            // shipping address informations

                            while (reader.Read())
                            {
                                popOrderForm.ShippingMethodEvaluation.WithSeller(lineGroup.Seller.SellerId);

                                var shippingMethodEvaluation = popOrderForm.ShippingMethodEvaluation.Seller(lineGroup.Seller.SellerId).Value;

                                var shippingAddressType = DataReaderHelper.GetField<int>(reader, "saNetworkId");

                                switch (shippingAddressType)
                                {
                                    case 1:
                                        shippingAddress = new PostalAddress
                                        {
                                            Company = DataReaderHelper.GetField<string>(reader, "saCompany"),
                                            LastName = DataReaderHelper.GetField<string>(reader, "saLastName"),
                                            Firstname = DataReaderHelper.GetField<string>(reader, "saFirstName"),
                                            AddressLine1 = DataReaderHelper.GetField<string>(reader, "saAddress1"),
                                            AddressLine2 = DataReaderHelper.GetField<string>(reader, "saAddress2"),
                                            AddressLine3 = DataReaderHelper.GetField<string>(reader, "saAddress3"),
                                            AddressLine4 = DataReaderHelper.GetField<string>(reader, "saAddress4"),
                                            ZipCode = DataReaderHelper.GetField<string>(reader, "saZipCode"),
                                            City = DataReaderHelper.GetField<string>(reader, "saCity"),
                                            State = DataReaderHelper.GetField<string>(reader, "saState"),
                                            CountryId = DataReaderHelper.GetField<int>(reader, "saCountryId"),
                                            Phone = DataReaderHelper.GetField<string>(reader, "saTelephone"),
                                            Fax = DataReaderHelper.GetField<string>(reader, "saFax"),
                                            Identity = 0
                                        };
                                        break;
                                    case 2:
                                    case 8:
                                    case 9:
                                        shippingAddress = new RelayAddress
                                        {
                                            Company = DataReaderHelper.GetField<string>(reader, "saCompany"),
                                            LastName = DataReaderHelper.GetField<string>(reader, "saLastName"),
                                            Firstname = DataReaderHelper.GetField<string>(reader, "saFirstName"),
                                            AddressLine1 = DataReaderHelper.GetField<string>(reader, "saAddress1"),
                                            AddressLine2 = DataReaderHelper.GetField<string>(reader, "saAddress2"),
                                            AddressLine3 = DataReaderHelper.GetField<string>(reader, "saAddress3"),
                                            AddressLine4 = DataReaderHelper.GetField<string>(reader, "saAddress4"),
                                            ZipCode = DataReaderHelper.GetField<string>(reader, "saZipCode"),
                                            City = DataReaderHelper.GetField<string>(reader, "saCity"),
                                            State = DataReaderHelper.GetField<string>(reader, "saState"),
                                            CountryId = DataReaderHelper.GetField<int>(reader, "saCountryId"),
                                            Phone = DataReaderHelper.GetField<string>(reader, "saTelephone"),
                                            Fax = DataReaderHelper.GetField<string>(reader, "saFax")
                                        };
                                        break;
                                    case 3:
                                        shippingAddress = new ShopAddress
                                        {
                                            RefUG = DataReaderHelper.GetField<string>(reader, "saRefUG"),
                                            Address = DataReaderHelper.GetField<string>(reader, "saAddress1"),
                                            ZipCode = DataReaderHelper.GetField<string>(reader, "saZipCode"),
                                            City = DataReaderHelper.GetField<string>(reader, "saCity"),
                                            State = DataReaderHelper.GetField<string>(reader, "saState"),
                                            CountryId = DataReaderHelper.GetField<int>(reader, "saCountryId"),
                                            Phone = DataReaderHelper.GetField<string>(reader, "saTelephone"),
                                        };
                                        break;
                                }
                                shippingMethodEvaluation.ReplaceAddress(shippingAddress);
                            }

                            reader.NextResult();

                            // Order Appointment Details
                            var deliverySlotData = popOrderForm.GetPrivateData<DeliverySlotData>(create: true);
                            while (reader.Read())
                            {

                                deliverySlotData.DeliverySlotSelected = new DeliverySlot
                                {
                                    PartnerId = DataReaderHelper.GetField<string>(reader, "oaPartnerId"),
                                    PartnerUniqueIdentifier = DataReaderHelper.GetField<string>(reader, "oaPartnerUniqueId"),
                                    StartDate = DataReaderHelper.GetField<DateTime>(reader, "oaStartDate"),
                                    EndDate = DataReaderHelper.GetField<DateTime>(reader, "oaEndDate"),
                                    SlotType = DataReaderHelper.GetField<string>(reader, "oaSlotType"),
                                };
                            }
                            reader.NextResult();

                            // Order details information

                            while (reader.Read())
                            {
                                var masterArticleId = DataReaderHelper.GetField<int?>(reader, "artMasterOdetId");
                                var logType = DataReaderHelper.GetField<int?>(reader, "artLogisticType");

                                if (masterArticleId.HasValue && logType == (int)LogType.ServicesDivers)
                                {
                                    var service = new Service
                                    {
                                        MasterArticles = new ArticleList { new StandardArticle { OrderDetailPk = masterArticleId } },
                                        ProductID = DataReaderHelper.GetField<int>(reader, "artPrid"),
                                        Quantity = DataReaderHelper.GetField<int>(reader, "artQuantity"),
                                        OrderDetailPk = DataReaderHelper.GetField<int>(reader, "artOdetId"),
                                        LogType = DataReaderHelper.GetField<int?>(reader, "artLogisticType"),
                                        PriceDBEur = DataReaderHelper.GetField<decimal?>(reader, "artDbPriceEur"),
                                        PriceUserEur = DataReaderHelper.GetField<decimal?>(reader, "artUserPriceEur"),
                                        EcoTaxEur = DataReaderHelper.GetField<decimal?>(reader, "artEcoTaxEur"),
                                        VATRate = DataReaderHelper.GetField<decimal?>(reader, "artVatRate"),
                                        VatCountryId = DataReaderHelper.GetField<int?>(reader, "artVatCountryId"),
                                        ReferenceGU = DataReaderHelper.GetField<string>(reader, "artReferenceGu"),
                                        ShipMethod = DataReaderHelper.GetField<int?>(reader, "artShippingMethodId"),
                                        ShipPriceDBEur = DataReaderHelper.GetField<decimal?>(reader, "artShippingPriceDbEur"),
                                        ShipPriceUserEur = DataReaderHelper.GetField<decimal?>(reader, "artShippingPriceUserEur"),
                                        WrapMethod = DataReaderHelper.GetField<int?>(reader, "artWrapMethodId"),
                                        WrapPriceDBEur = DataReaderHelper.GetField<decimal?>(reader, "artWrapDbPriceEur"),
                                        WrapPriceUserEur = DataReaderHelper.GetField<decimal?>(reader, "artWrapUserPriceEur"),
                                        TheoreticalDeliveryDate = DataReaderHelper.GetField<DateTime>(reader, "artTheoricalDeliveryDate"),
                                        TheoreticalDeliveryDateMax = DataReaderHelper.GetField<DateTime?>(reader, "artTheoreticalDeliveryDateMax"),
                                        TheoreticalExpeditionDate = DataReaderHelper.GetField<DateTime>(reader, "artTheoricalExpidationDate"),
                                        Promotions = new PromotionList(),
                                        SalesInfo = new SalesInfo
                                        {
                                            Prices = new PriceCollection { new Price { Type = PriceType.ReducedDiffered } }
                                        }
                                    };

                                    lineGroup.Informations.ChosenWrapMethod = service.WrapMethod.GetValueOrDefault();

                                    lineGroup.Articles.Add(service);
                                }
                                else
                                {
                                    var article = new StandardArticle
                                    {
                                        ProductID = DataReaderHelper.GetField<int>(reader, "artPrid"),
                                        Quantity = DataReaderHelper.GetField<int>(reader, "artQuantity"),
                                        OrderDetailPk = DataReaderHelper.GetField<int>(reader, "artOdetId"),
                                        LogType = DataReaderHelper.GetField<int?>(reader, "artLogisticType"),
                                        PriceDBEur = DataReaderHelper.GetField<decimal?>(reader, "artDbPriceEur"),
                                        PriceUserEur = DataReaderHelper.GetField<decimal?>(reader, "artUserPriceEur"),
                                        EcoTaxEur = DataReaderHelper.GetField<decimal?>(reader, "artEcoTaxEur"),
                                        VATRate = DataReaderHelper.GetField<decimal?>(reader, "artVatRate"),
                                        VatCountryId = DataReaderHelper.GetField<int?>(reader, "artVatCountryId"),
                                        ReferenceGU = DataReaderHelper.GetField<string>(reader, "artReferenceGu"),
                                        ShipMethod = DataReaderHelper.GetField<int?>(reader, "artShippingMethodId"),
                                        ShipPriceDBEur = DataReaderHelper.GetField<decimal?>(reader, "artShippingPriceDbEur"),
                                        ShipPriceUserEur = DataReaderHelper.GetField<decimal?>(reader, "artShippingPriceUserEur"),
                                        WrapMethod = DataReaderHelper.GetField<int?>(reader, "artWrapMethodId"),
                                        WrapPriceDBEur = DataReaderHelper.GetField<decimal?>(reader, "artWrapDbPriceEur"),
                                        WrapPriceUserEur = DataReaderHelper.GetField<decimal?>(reader, "artWrapUserPriceEur"),
                                        TheoreticalDeliveryDate = DataReaderHelper.GetField<DateTime>(reader, "artTheoricalDeliveryDate"),
                                        TheoreticalDeliveryDateMax = DataReaderHelper.GetField<DateTime?>(reader, "artTheoreticalDeliveryDateMax"),
                                        TheoreticalExpeditionDate = DataReaderHelper.GetField<DateTime>(reader, "artTheoricalExpidationDate"),
                                        Promotions = new PromotionList(),
                                        SalesInfo = new SalesInfo
                                        {
                                            Prices = new PriceCollection { new Price { Type = PriceType.ReducedDiffered } }
                                        }
                                    };

                                    lineGroup.Informations.ChosenWrapMethod = article.WrapMethod.GetValueOrDefault();

                                    lineGroup.Articles.Add(article);
                                }
                            }

                            reader.NextResult();

                            // Article promotion informations

                            while (reader.Read())
                            {
                                var productId = DataReaderHelper.GetField<int>(reader, "artPrid");
                                var promotion = new Model.Promotion
                                {
                                    Code = DataReaderHelper.GetField<string>(reader, "artCodePromo"),
                                    Type = DataReaderHelper.GetField<int>(reader, "artPromoType"),
                                    DiscountType = DataReaderHelper.GetField<int>(reader, "artDiscountType"),
                                    HasThreshold = DataReaderHelper.GetField<int>(reader, "artPromoHasThreshold") == 1,
                                    IsNotSameCriteria = DataReaderHelper.GetField<int>(reader, "artPromoHomogeneousCriteria") == 1
                                };

                                var article = lineGroup.Articles.FirstOrDefault(a => a.ProductID == productId) as StandardArticle;
                                article.Promotions.Add(promotion);

                                if (promotion.Type == (int)PromotionType.DiscountDeffered)
                                {
                                    article.SalesInfo.Prices[PriceType.ReducedDiffered].TTC = DataReaderHelper.GetField<decimal>(reader, "artPromoAmount");
                                }
                            }

                            reader.NextResult();

                            // Article remise informations

                            while (reader.Read())
                            {
                                var productId = DataReaderHelper.GetField<int>(reader, "artPrid");
                                var article = lineGroup.Articles.FirstOrDefault(a => a.ProductID == productId) as StandardArticle;

                                var discountTotalAmount = DataReaderHelper.GetField<decimal>(reader, "discountAmount");
                                var unitDiscount = discountTotalAmount / article.Quantity.GetValueOrDefault(1);

                                var remise = new Remise
                                {
                                    DiscountReason = DataReaderHelper.GetField<int>(reader, "discountReasonId"),
                                    DiscountType = DataReaderHelper.GetField<int>(reader, "discountTypeId"),
                                    Type = DataReaderHelper.GetField<int>(reader, "discountPromotionTypeId"),
                                    Rebate = unitDiscount
                                };
                                article.Promotions.Add(remise);
                            }
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                ex.Data.Add("GetOrderFromOrderReference.orderReference", orderReference);
                throw DALException.BuildException(ex, DALRange.Commerce, 45);
            }

            popOrderForm.AddLineGroup(lineGroup);
            popOrderForm.AddLogisticLineGroup(logisticLineGroup);

            return popOrderForm;
        }

        public IEnumerable<RemoteRebate> LoadRemoteRebates()
        {
            try
            {
                using (var database = Database.CreateInstance(DatabaseSqlCommerce))
                {
                    using (var command = database.GetStoredProcCommand("spPipe_LoadRemoteRebates"))
                    {
                        var reader = command.ExecuteReader();
                        var rebates = new List<RemoteRebate>();

                        while (reader.Read())
                        {
                            rebates.Add(new RemoteRebate
                            {
                                Id = DataReaderHelper.GetField<int>(reader, "RebateId"),
                                ShopId = DataReaderHelper.GetField<int>(reader, "ShopId"),
                                Content = DataReaderHelper.GetField<string>(reader, "Overview"),
                                SellerComment = DataReaderHelper.GetField<string>(reader, "SellerComment"),
                                SellerMatricule = DataReaderHelper.GetField<string>(reader, "SellerMatricule"),
                                SellerPoste = DataReaderHelper.GetField<string>(reader, "SellerPoste"),
                                RespComment = DataReaderHelper.GetField<string>(reader, "RespComment"),
                                RespMatricule = DataReaderHelper.GetField<string>(reader, "RespMatricule"),
                                CreationDate = DataReaderHelper.GetField<DateTime>(reader, "CreationDate"),
                            });
                        }

                        return rebates;
                    }
                }
            }
            catch (Exception ex)
            {
                throw DALException.BuildException(ex, DALRange.Commerce, 1);
            }
        }

        public RemoteRebate LoadRemoteRebate(int rebateId)
        {
            try
            {
                using (var database = Database.CreateInstance(DatabaseSqlCommerce))
                {
                    using (var command = database.GetStoredProcCommand("spPipe_LoadRemoteRebate"))
                    {
                        database.AddInParameter(command, "remoteRebateId", SqlDbType.Int, rebateId);

                        var reader = command.ExecuteReader();
                        RemoteRebate rebate = null;

                        if (reader.Read())
                        {
                            rebate = new RemoteRebate
                            {
                                Id = DataReaderHelper.GetField<int>(reader, "RebateId"),
                                Status = (RemoteRebateStatus)DataReaderHelper.GetField<int>(reader, "StatusId"),
                                ShopId = DataReaderHelper.GetField<int>(reader, "ShopId"),
                                Content = DataReaderHelper.GetField<string>(reader, "Overview"),
                                SellerComment = DataReaderHelper.GetField<string>(reader, "SellerComment"),
                                SellerMatricule = DataReaderHelper.GetField<string>(reader, "SellerMatricule"),
                                SellerPoste = DataReaderHelper.GetField<string>(reader, "SellerPoste"),
                                RespComment = DataReaderHelper.GetField<string>(reader, "RespComment"),
                                RespMatricule = DataReaderHelper.GetField<string>(reader, "RespMatricule")
                            };
                        }

                        return rebate;
                    }
                }
            }
            catch (Exception ex)
            {
                throw DALException.BuildException(ex, DALRange.Commerce, 1);
            }
        }

        public bool IsLldOrderValidated(string contractNumber)
        {
            try
            {
                using (var database = Database.CreateInstance(DatabaseSqlCommerce))
                {
                    using (var command = database.GetStoredProcCommand("spPipe_IsLldOrderValidated"))
                    {
                        database.AddInParameter(command, "contractNumber", SqlDbType.VarChar, contractNumber);
                        database.AddOutParameter(command, "orderFound", SqlDbType.Bit);
                        command.ExecuteNonQuery();
                        return database.GetParameterValue<bool>(command, "orderFound");
                    }
                }
            }
            catch (Exception ex)
            {
                throw DALException.BuildException(ex, DALRange.Commerce, 1);
            }
        }

        public bool HasOrderWithContractNumberValidated(string contractNumber, int orderDetailInfoTypeId)
        {
            try
            {
                using (var database = Database.CreateInstance(DatabaseSqlCommerce))
                {
                    using (var command = database.GetStoredProcCommand("spPipe_HasOrderWithContractNumberValidated"))
                    {
                        database.AddInParameter(command, "contractNumber", SqlDbType.VarChar, contractNumber);
                        database.AddInParameter(command, "orderDetailInfoTypeId", SqlDbType.Int, orderDetailInfoTypeId);
                        database.AddOutParameter(command, "orderFound", SqlDbType.Bit);
                        command.ExecuteNonQuery();

                        return database.GetParameterValue<bool>(command, "orderFound");
                    }
                }
            }
            catch (Exception ex)
            {
                throw DALException.BuildException(ex, DALRange.Commerce, 1);
            }
        }

        public void AddOrderTransactionSiebel(int? orderId, Guid reference, string numAdherent, string numContract,
            string advantageType, decimal amount, string cardNumber)
        {
            try
            {
                using (var database = Database.CreateInstance(DatabaseSqlCommerce))
                {
                    using (var command = database.GetStoredProcCommand("spPipe_AddOrderTransactionSiebel"))
                    {
                        database.AddInParameter(command, "reference", SqlDbType.UniqueIdentifier, reference);
                        database.AddInParameter(command, "orderId", SqlDbType.Int, orderId);
                        database.AddInParameter(command, "numAdherent", SqlDbType.VarChar, numAdherent);
                        database.AddInParameter(command, "numContract", SqlDbType.VarChar, numContract);
                        database.AddInParameter(command, "advantageType", SqlDbType.VarChar, advantageType);
                        database.AddInParameter(command, "amount", SqlDbType.Decimal, amount);
                        database.AddInParameter(command, "cardNumber", SqlDbType.VarChar, cardNumber);
                        command.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception ex)
            {
                throw DALException.BuildException(ex, DALRange.Commerce, 1);
            }
        }

        public void UpdateOrderTransactionSiebel(Guid reference, int siebelOrderId, string siebeltransactionId)
        {
            try
            {
                using (var database = Database.CreateInstance(DatabaseSqlCommerce))
                {
                    using (var command = database.GetStoredProcCommand("spPipe_UpdateOrderTransactionSiebel"))
                    {
                        database.AddInParameter(command, "reference", SqlDbType.UniqueIdentifier, reference);
                        database.AddInParameter(command, "siebeltransactionId", SqlDbType.VarChar, siebeltransactionId);
                        database.AddInParameter(command, "siebelOrderId", SqlDbType.Int, siebelOrderId);
                        command.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception ex)
            {
                throw DALException.BuildException(ex, DALRange.Commerce, 1);
            }
        }

        public string GetShopSecurityCode(int shopId, bool forceReset)
        {
            try
            {
                using (var database = Database.CreateInstance(DatabaseSqlCommerce))
                {
                    using (var command = database.GetStoredProcCommand("spRetrait_GetShopSecurityCode"))
                    {
                        database.AddInParameter(command, "pshopId", SqlDbType.Int, shopId);
                        database.AddInParameter(command, "pforceReset", SqlDbType.Bit, forceReset);
                        database.AddOutParameter(command, "rcode", SqlDbType.VarChar, 2);
                        command.ExecuteNonQuery();
                        return database.GetParameterValue<string>(command, "rcode");
                    }
                }
            }
            catch (Exception ex)
            {
                throw DALException.BuildException(ex, DALRange.Commerce, 1);
            }
        }

        public int? GetLatestShippedOrder(int accountId)
        {
            using (var database = Database.CreateInstance(DatabaseSqlCommerce))
            {
                var command = database.GetStoredProcCommand("spPipe_GetLatestShippedOrder");
                database.AddInParameter(command, "Account_Id_pk", SqlDbType.Int, accountId);
                var reader = command.ExecuteReader();
                if (reader.Read())
                {
                    return int.Parse(reader["Order_Id"].ToString());
                }
            }
            return null;
        }
    }
}
