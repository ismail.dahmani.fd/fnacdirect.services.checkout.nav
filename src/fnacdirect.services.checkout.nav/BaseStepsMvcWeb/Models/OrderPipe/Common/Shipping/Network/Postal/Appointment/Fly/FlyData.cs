using FnacDirect.OrderPipe.Base.Model.Fly;

namespace FnacDirect.OrderPipe.Base.Model.Shipping.Postal.Appointment.Fly
{
    public class FlyData : GroupData, IFlyForm
    {
        public int ShippingMethodId { get; set; }

        public int ProductQuantityToTakeBack { get; set; }

        public string PhoneNumber { get; set; }

        public bool HasElevatorAccess { get; set; }

        public bool HasStaircaseStep { get; set; }

        public string AccessCode { get; set; }

        public string Commentary { get; set; }

        public int? FloorNumber { get; set; }

        public int? StaircaseType { get; set; }

        public CalendarSlotIdentifier CalendarSlotIdentifier { get; set; }

        public FlyServiceByArticle[] SelectedFlyServices { get; set; }
    }
}
