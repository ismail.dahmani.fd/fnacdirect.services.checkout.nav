﻿using FnacDirect.OrderPipe.Base.Model.Facets;

namespace FnacDirect.OrderPipe.Base.Business.Shipping.Relay
{
    public interface IRelayCellphoneService
    {
        bool SetRelayCellPhone(string cellphone, IGotShippingMethodEvaluation shippingMethodEvaluation);
    }
}
