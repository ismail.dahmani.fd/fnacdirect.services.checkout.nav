using FnacDirect.OrderPipe.Base.Model;
using System.Linq;


namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Sorry
{
    public class OrderSorryViewModelBuilder : IOrderSorryViewModelBuilder
    {

        public OrderSorryViewModel Build(OPContext opContext, PopOrderForm popOrderForm)
        {
            var orderSorryViewModel = new OrderSorryViewModel();

            if (popOrderForm.OrderBillingMethods.OfType<MBWayBillingMethod>().Any())
            {
                orderSorryViewModel.HasMBWay = true;
            }

            return orderSorryViewModel;
        }
    }
}
