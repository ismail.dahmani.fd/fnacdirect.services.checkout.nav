namespace FnacDirect.OrderPipe.Base.Proxy.Customer
{
    public enum ShippingAddressType
    {
        None,
        Postal,
        Relay
    }
}
