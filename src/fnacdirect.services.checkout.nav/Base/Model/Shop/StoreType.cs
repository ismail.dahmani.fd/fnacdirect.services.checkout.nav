

namespace FnacDirect.OrderPipe.Base.Model
{
    /// <summary>
    /// Type de magasin Fnac, Darty ...
    /// </summary>
    public enum StoreType
    {
        Fnac = 1,
        Darty = 100,
    }
}
