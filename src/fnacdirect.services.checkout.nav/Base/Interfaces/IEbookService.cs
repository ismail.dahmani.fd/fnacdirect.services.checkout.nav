using System.Collections.Generic;
using Fnac.DEMAT.Delivery.Contracts.V3;
using FnacDirect.Front.eBook.Model;
using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.OrderPipe.Base.Model.LineGroups;
using FnacDirect.OrderPipe.Base.Model.UserInformations;
using System;
using FnacDirect.OrderPipe.Base.Model.Facets;

namespace FnacDirect.OrderPipe.Base.Proxy.Ebook
{
    public interface IEbookService
    {
        KoboAccount GetKoboAccount(int userId, string userReference, string email);
        KoboAccount GetKoboAccount(Guid userReference, string email, string countryCode);
        PurchaseVerificationResponse VerifyPurchaseKoboV3(BillingAddress billingAddress, IUserContextInformations userContext, IEnumerable<ILineGroup> lineGroups);
        Fnac.DEMAT.Delivery.Contracts.PurchaseVerificationResponse VerifyPurchaseKobo(BillingAddress billingAddress, IUserContextInformations userContext, IEnumerable<ILineGroup> lineGroups);
        Fnac.DEMAT.Delivery.Contracts.PurchaseVerificationResponse VerifyPurchaseKobo(Fnac.DEMAT.Delivery.Contracts.OrderDematInfo odi);
        PurchaseVerificationResponse VerifyPurchaseKobo(OrderDematInfo odi);
        bool IsAllowedToBeSoldInGeo(BillingAddress billingAddress, IUserContextInformations userContext, IEnumerable<ILineGroup> lineGroups, int marketId);
        void RemoveKoboArticlesWhenExceptionRaise(IGotLineGroups iGotLineGroup, string stepName);
    }
}
