using System.Xml.Serialization;
using FnacDirect.Customer.Model;

namespace FnacDirect.OrderPipe.Base.Model
{
    public class PostalAddress : ShippingAddress
    {
        [XmlIgnore]
        public string Alias { get; set; }

        [XmlIgnore]
        public string Company { get; set; }


        public string Firstname { get; set; }


        public string LastName { get; set; }

        public string AddressLine1 { get; set; }

        public string AddressLine2 { get; set; }

        public string AddressLine3 { get; set; }

        [XmlIgnore]
        public string AddressLine4 { get; set; }


        public string ZipCode { get; set; }

        public string City { get; set; }

        public override bool IsDummy()
        {
            return !Identity.HasValue || Identity.Value == -1;
        }

        public string CellPhone { get; set; }

        [XmlIgnore]
        public string Fax { get; set; }

        public override int Type
        {
            get
            {
                return 1;
            }
        }

        public int GenderId { get; set; }

        [XmlIgnore]
        public string GenderLabel { get; set; }

        public AddressEntity AddressEntity { get; set; }

        [XmlIgnore]
        public string TaxNumberType { get; set; }

        [XmlIgnore]
        public string TaxNumber { get; set; }

        [XmlIgnore]
        public int DocumentCountryId { get; set; }

        public PostalAddress()
        {
        }

        public string GetPhone()
        {
            //TODO refacto this duplicate from AddressViewModelBuilder
            var franceId = (int)CountryEnum.France;
            if (CountryId == franceId)
            {
                return !string.IsNullOrEmpty(CellPhone) ? CellPhone : Phone;
            }
            else
            {
                return !string.IsNullOrEmpty(Phone) ? Phone : CellPhone;
            }

        }
        public static void Copy(AddressEntity ae, PostalAddress pa)
        {
            if (ae == null || pa == null)
            {
                return;
            }

            pa.Identity = ae.AddressBookId;
            pa.Reference = ae.AddressBookReference;
            pa.Alias = ae.AliasName;
            pa.Company = ae.Company;
            pa.Firstname = ae.FirstName;
            pa.LastName = ae.LastName;
            pa.AddressLine1 = ae.AddressLine1;
            pa.AddressLine2 = ae.AddressLine2;
            pa.AddressLine3 = ae.AddressLine3;
            pa.AddressLine4 = ae.AddressLine4;
            pa.ZipCode = ae.Zipcode;
            pa.City = ae.City;
            pa.State = ae.State;
            pa.CountryId = ae.CountryId;
            pa.CountryLabel = ae.CountryLabel;
            pa.ShippingZone = ae.ShippingZone;
            pa.IsCountryUseVAT = ae.UseVat;
            pa.Phone = ae.Tel ?? ae.CellPhone;
            pa.CellPhone = ae.CellPhone;
            pa.Fax = ae.Fax;
            pa.GenderId = ae.GenderId;
            pa.GenderLabel = ae.GenderLabel;
            pa.AddressEntity = ae;
            pa.CountryCode2 = ae.CountryCode2;
            pa.LegalStatus = ae.LegalStatusId;
            pa.TaxNumberType = ae.TaxIdNumberType;
            pa.TaxNumber = ae.TaxIdNumber;
            pa.DocumentCountryId = ae.DocumentCountryId;
        }

        public PostalAddress(AddressEntity address)
        {
            Copy(address, this);
        }

        public IAddressViewModelFieldsValidation Convert()
        {
            IAddressViewModelFieldsValidation address = new AddressViewModelFieldsValidationDto(this);

            return address;
        }
    }

}
