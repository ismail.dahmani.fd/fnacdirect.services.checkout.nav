﻿using FnacDirect.OrderPipe;

/// <summary>
/// Groupe de données permettant de sauvegarder les infos dans le cache
/// </summary>
public class DefferedPaymentData : GroupData
{
    /// <summary>
    /// Total des commandes de la journée
    /// </summary>
    public decimal CumulativeOrdersAmount { get; set; }
    /// <summary>
    /// Obtient ou définit si le total des commandes de la journée du client a été déjà calculé pour la session en cours
    /// (pour le recalculer, il suffira que le client quitte le pipe et y revenir)
    /// </summary>
    public bool IsCumulativeOrdersAmountComputed { get; set; }
    /// <summary>
    /// Obtient ou définit le type d'autorisation octroyé au client
    /// </summary>
    public int AuthorisationType { get; set; }
    /// <summary>
    /// Obtient ou définit si le paiement différé est activé pour le client en cours
    /// </summary>
    public bool CanUseDefferedPayment { get; set; }
    /// <summary>
    /// Délai du paiement différé
    /// </summary>
    public int Delay { get; set; }
    /// <summary>
    /// N° de paiement différé du client
    /// </summary>
    public string DefferedAccountNumber { get; set; }
    /// <summary>
    /// Obtient ou définit si les attributs du client ont été déjà récupérés de la base
    /// </summary>
    public bool AreCustomerAttributesRetrieved { get; set; }
}