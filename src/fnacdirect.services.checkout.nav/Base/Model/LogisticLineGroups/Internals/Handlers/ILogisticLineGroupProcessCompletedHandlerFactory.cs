﻿using System;

namespace FnacDirect.OrderPipe.Base.Business.LogisticLineGroups.Internals.Handlers
{
    public interface ILogisticLineGroupProcessCompletedHandlerFactory
    {
        ILogisticLineGroupProcessCompletedHandler Get(ILogisticLineGroupProcessCompletedHandlerDescriptor logisticLineGroupProcessCompletedHandlerDescriptor);
    }
}
