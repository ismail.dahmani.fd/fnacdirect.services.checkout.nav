using System;

namespace FnacDirect.OrderPipe.Base.Model
{
    /// <summary>
    /// Spécifie les types de commandes.
    /// Cette énumération possède un attribut FlagsAttribute qui permet la combinaison d'opérations de bits de ses valeurs de membres.
    /// </summary>
    [Flags]
    [Serializable]
    public enum OrderTypeEnum
    {
        // 21/11/17 Pierrick Visentin : Remplacement de None en _None pour faire remarcher FnacproQuote. On pourra remettre None quand on aura plus besoin d'utiliser FnacproQuote.
        // Ne pas oublier de mettre à jour les XML en base (fld_Quote_Content sur TBL_Quote sur BDD Quote) à ce moment là.

        /// <summary>
        /// Une commande indéfinie. 
        /// </summary>
        _None = 0,
        /// <summary>
        /// Une commande Fnac.
        /// </summary>
        FnacCom = 1,
        /// <summary>
        /// Une commande concernant un ou des vendeurs indépendants.
        /// </summary>
        MarketPlace = 2,
        /// <summary>
        /// Une commande d'articles numériques.
        /// </summary>
        Numerical = 4,
        /// <summary>
        /// Une commande Fnac Pro.
        /// </summary>
        Pro = 8,
        /// <summary>
        /// Une réservation sur stock pour FDV.
        /// </summary>
        IntraMag = 16,
        /// <summary>
        /// Une commande à retirer en magasin.
        /// </summary>
        CollectInStore = 32,
        /// <summary>
        /// Une commande DematSoft.
        /// </summary>
        DematSoft = 64,
        /// <summary>
        /// Libre service, sans réservation de stock.
        /// </summary>
        IntraMagPE = 128,
    }
}
