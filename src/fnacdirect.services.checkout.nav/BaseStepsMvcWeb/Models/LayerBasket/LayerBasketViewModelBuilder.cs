using FnacDirect.Context;
using FnacDirect.Contracts.Online.Model;
using FnacDirect.Membership.Model.Constants;
using FnacDirect.OrderPipe.Base.BaseModel.Model;
using FnacDirect.OrderPipe.Base.Business.Operations;
using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.OrderPipe.Base.Proxy.Membership;
using FnacDirect.OrderPipe.Base.Proxy.Wishlist;
using FnacDirect.OrderPipe.Base.ReducedPriceBooks;
using FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Basket;
using FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Common.Payment;
using FnacDirect.Technical.Framework.Web.Mvc;
using System.Collections.Generic;
using System.Linq;

namespace FnacDirect.OrderPipe.BaseMvc.Web.LayerBasket
{
    public class LayerBasketViewModelBuilder : ILayerBasketViewModelBuilder
    {
        private readonly IApplicationContext _applicationContext;
        private readonly IRichBasketItemViewModelBuilderProvider _rickBasketItemViewModelBuilderProvider;
        private readonly IAdherentCardArticleService _adherentCardArticleService;
        private readonly ISummaryPricesViewModelBuilder _summaryPricesViewModelBuilder;
        private readonly IPromotionTemplateViewModelBuilder _promotionTemplateViewModelBuilder;
        private readonly IMembershipConfigurationService _membershipConfigurationService;
        private readonly IUserExtendedContextProvider _userExtendedContextProvider;
        private readonly IUserExtendedContext _userExtendedContext;
        private readonly IDeliveredForChristmasService _deliveredForChristmasService;
        private readonly IExecutionContextProvider _executionContextProvider;
        private readonly IWishlistService _wishlistService;


        public LayerBasketViewModelBuilder(IApplicationContext applicationContext,
                                            IRichBasketItemViewModelBuilderProvider richBasketItemViewModelBuilderProvider,
                                            IAdherentCardArticleService adherentCardArticleService,
                                            ISummaryPricesViewModelBuilder summaryPricesViewModelBuilder,
                                            IPromotionTemplateViewModelBuilder promotionTemplateViewModelBuilder,
                                            IMembershipConfigurationService membershipConfigurationService,
                                            IUserExtendedContextProvider userExtendedContextProvider,
                                            IUserExtendedContext userExtendedContext,
                                            IDeliveredForChristmasService deliveredForChristmasService,
                                            IExecutionContextProvider executionContextProvider,
                                            IWishlistService wishlistService)
        {
            _applicationContext = applicationContext;
            _rickBasketItemViewModelBuilderProvider = richBasketItemViewModelBuilderProvider;
            _adherentCardArticleService = adherentCardArticleService;
            _summaryPricesViewModelBuilder = summaryPricesViewModelBuilder;
            _promotionTemplateViewModelBuilder = promotionTemplateViewModelBuilder;
            _membershipConfigurationService = membershipConfigurationService;
            _userExtendedContextProvider = userExtendedContextProvider;
            _userExtendedContext = userExtendedContext;
            _deliveredForChristmasService = deliveredForChristmasService;
            _executionContextProvider = executionContextProvider;
            _wishlistService = wishlistService;
        }

        public LayerBasketViewModel Build(PopOrderForm orderForm)
        {
            var viewModel = new LayerBasketViewModel();

            var priceHelper = _applicationContext.GetPriceHelper();

            var articles = new List<RichBasketItemViewModel>();

            //Prix TTC
            var total = 0m;
            var oldTotal = 0m;

            //Prix HT
            var totalNoVAT = 0m;
            var oldTotalNoVAT = 0m;

            var hasAdherentCardInBasket = orderForm.LineGroups.GetArticles().Any(a => _adherentCardArticleService.IsAdherentCard(a.ProductID.GetValueOrDefault(0)));
            var applyAdherentPromotion = orderForm.UserContextInformations.IsAdherent || hasAdherentCardInBasket;

            var context = new RichBasketItemViewModelBuilderContext(orderForm, orderForm.GlobalPromotions, applyAdherentPromotion);

            var richBasketItemViewModelBuilder = _rickBasketItemViewModelBuilderProvider.Get(context);

            var shopData = orderForm.GetPrivateData<ShopData>(create: false);
            if (shopData != null && shopData.ShopAddress != null)
            {
                viewModel.IsShop = true;
            }

            var reducedPriceData = orderForm.GetPrivateData<ReducedPriceData>(create: false);

            foreach (var article in orderForm.LineGroups.GetArticles().OfType<BaseArticle>())
            {
                var art = richBasketItemViewModelBuilder.Build(article, orderForm);

                var price = article.Quantity.GetValueOrDefault(0) * article.PriceUserEur.GetValueOrDefault(0);
                var oldPrice = article.Quantity.GetValueOrDefault(0) * article.PriceDBEur.GetValueOrDefault(0);

                var priceNoVAT = article.Quantity.GetValueOrDefault(0) * article.PriceNoVATEur.GetValueOrDefault(0);
                var oldPriceNoVAT = article.Quantity.GetValueOrDefault(0) * (article.SalesInfo?.StandardPrice?.HT ?? article.PriceNoVATEur.GetValueOrDefault(0));

                if (article is StandardArticle)
                {
                    var stdArt = article as StandardArticle;

                    if (stdArt.Hidden)
                    {
                        continue;
                    }

                    //EcoTax en TTC
                    if (stdArt.EcoTaxEur.HasValue)
                    {
                        price += stdArt.EcoTaxEur.Value * stdArt.Quantity.Value;
                        oldPrice += stdArt.EcoTaxEur.Value * stdArt.Quantity.Value;
                    }

                    //EcoTax en HT
                    if (stdArt.EcoTaxEurNoVAT.HasValue)
                    {
                        priceNoVAT += stdArt.EcoTaxEurNoVAT.Value * stdArt.Quantity.Value;
                        oldPriceNoVAT += stdArt.EcoTaxEurNoVAT.Value * stdArt.Quantity.Value;
                    }

                    foreach (var service in stdArt.Services)
                    {
                        price += (service.Quantity.GetValueOrDefault(0) * service.PriceUserEur.GetValueOrDefault(0));
                        oldPrice += (service.Quantity.GetValueOrDefault(0) * service.PriceDBEur.GetValueOrDefault(0));

                        priceNoVAT += (service.Quantity.GetValueOrDefault(0) * service.PriceNoVATEur.GetValueOrDefault(0));
                        oldPriceNoVAT += (service.Quantity.GetValueOrDefault(0) * service.SalesInfo.StandardPrice.HT);
                    }
                }

                //totaux TTC
                total += price;
                oldTotal += oldPrice;

                //totaux HT
                totalNoVAT += priceNoVAT;
                oldTotalNoVAT += oldPriceNoVAT;
                
                art.RemoveChildrenDigicopy();

                if (article is StandardArticle && article.IsBook)
                {
                    if (reducedPriceData != null && reducedPriceData.ReducePriceList.ContainsKey(art.ProductId) && viewModel.IsShop)
                    {
                        art.Price.OldPrice = priceHelper.Format(oldPrice, PriceFormat.CurrencyAsDecimalSeparator);
                        art.Price.Price = priceHelper.Format(reducedPriceData.ReducePriceList[art.ProductId] * article.Quantity.GetValueOrDefault(1), PriceFormat.CurrencyAsDecimalSeparator);
                    }
                    else
                    {
                        art.Price.Price = priceHelper.Format(price, PriceFormat.CurrencyAsDecimalSeparator);
                    }

                    if (string.Equals(art.Price.OldPrice, art.Price.Price, System.StringComparison.InvariantCultureIgnoreCase))
                    {
                        art.Price.OldPrice = null;
                    }
                }

                articles.Add(art);
            }

            //nettoyer les OPS Dezzer en double
            _promotionTemplateViewModelBuilder.CleanDeezerPromotions(articles);

            viewModel.Articles = articles;
            viewModel.ReducedPriceBook5pc = _summaryPricesViewModelBuilder.BuildReducedPriceBook5pc(orderForm, total);
    
            if (IsFnacMember() || HasFnacMemberCardInBasket(orderForm) || IsFnacPlusUser() || HasFnacPlusCardInBasket(orderForm))
            {
                viewModel.DifferedSavedAmount = _summaryPricesViewModelBuilder.BuildDifferedSavedAmount(orderForm);
            }
            viewModel.TotalReduced = _summaryPricesViewModelBuilder.BuildTotalReduced(orderForm, oldTotal, total);
            viewModel.TotalReducedNoVAT = _summaryPricesViewModelBuilder.BuildTotalReduced(orderForm, oldTotalNoVAT, totalNoVAT, false);
            if (reducedPriceData != null && reducedPriceData.Reduction > 0 && viewModel.IsShop)
            {
                viewModel.Total = priceHelper.Format(oldTotal - reducedPriceData.Reduction);
                viewModel.TotalNoVAT = priceHelper.Format(oldTotalNoVAT - reducedPriceData.Reduction);
            }
            else
            {
                viewModel.Total = priceHelper.Format(total);
                viewModel.TotalNoVAT = priceHelper.Format(totalNoVAT);
            }

            if (total == 0)
            {
                viewModel.IsFree = true;
            }

            //Operation Christmas
            var christmasWordingKey = _deliveredForChristmasService.GetWordingWithAvailibility(orderForm.LineGroups.GetArticles().OfType<StandardArticle>(), TypeWording.Layer);
            viewModel.ChristmasWording = _applicationContext.GetMessage(christmasWordingKey);

            var executionContext = _executionContextProvider.GetCurrentExecutionContext();
            var headers = executionContext.GetHeaders();
            var cookies = headers["Cookie"];

            var wishlistAsideItemsCount = _wishlistService.GetCountAsideItems(cookies).Result;
            viewModel.HasAsideItems = wishlistAsideItemsCount > 0;

            return viewModel;
        }

        private bool IsFnacMember()
        {
            if (_userExtendedContext != null)
            {
                return _userExtendedContext.Customer.IsValidOrTryAdherent;
            }

            return false;
        }

        private bool HasFnacMemberCardInBasket(PopOrderForm popOrderForm)
        {
            if (popOrderForm != null && _membershipConfigurationService != null)
            {
                return popOrderForm.LineGroups.GetArticles().OfType<StandardArticle>().Any(a => a.TypeId.GetValueOrDefault() == (int)CategoryCardType.Renew ||
                                                                                                a.TypeId.GetValueOrDefault() == (int)CategoryCardType.Recruit);
            }
            return false;
        }

        private bool IsFnacPlusUser()
        {
            if (_userExtendedContextProvider != null)
            {
                var userExtendedContext = _userExtendedContextProvider.GetCurrentUserExtendedContext();
                var segments = userExtendedContext.Customer.ContractDTO.Segments;

                return segments.Contains(CustomerSegment.EssaiPlus.ToString())
                    || segments.Contains(CustomerSegment.ExpressPlus.ToString());
            }

            return false;
        }

        private bool HasFnacPlusCardInBasket(PopOrderForm popOrderForm)
        {
            if (popOrderForm != null && _membershipConfigurationService != null)
            {
                return popOrderForm.LineGroups.GetArticles().Any(a => _membershipConfigurationService.FnacPlusCardTypes.Contains(a.TypeId.GetValueOrDefault()));
            }

            return false;
        }
    }
}
