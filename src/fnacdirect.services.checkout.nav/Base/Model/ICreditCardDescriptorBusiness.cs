using FnacDirect.Contracts.Online.Model;
using FnacDirect.Customer.Model;
using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.OrderPipe.Contracts.Services;
using System;
using System.Collections.Generic;

namespace FnacDirect.OrderPipe.Base.Business
{
	public interface ICreditCardDescriptorBusiness : ICreditCardDescriptorService
    {
	    List<CustCreditCardDescriptor> GetCreditCards(string config, string culture);

		CustCreditCardDescriptor GetCreditCardDescriptor(int id, string config, string culture);
		/// <summary>
		/// Retourne une description de la carte sans filtre sur son statut
		/// </summary>
		CustCreditCardDescriptor GetCreditCardDescriptorUnfiltered(int id, string config, string culture);

        List<Credit> GetAllCredits(SiteContext context);

        bool CheckDateVsOrder(DateTime expDate, ArticleList articles);

        bool CheckECard(string number);

        bool CheckLuhn(string number);
	    string ApplyMask(CustCreditCardDescriptor ccd, string cardNumber);
	    string ApplyMask(CustCreditCardDescriptor ccd, string cardNumberSeparator, string cardNumber);
    }
}
