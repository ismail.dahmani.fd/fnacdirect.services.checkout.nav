using System.Collections.Generic;
using FnacDirect.Technical.Framework.Web.Mvc.DataAnnotations;
using System;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Membership
{
    public class MemberBirthDateViewModel
    {
        public string FirstName { get; set; }

        [Required]
        public int? BirthYear { get; set; }

        [Required]
        public int? BirthMonth { get; set; }

        [Required]
        public int? BirthDay { get; set; }

        public string Error { get; set; }

        public bool IsValid { get; set; }

        public bool HasTryCard { get; set; }

        public int TryCardPrid { get; set; }

        public int FnacPlusCardPrid { get; set; }

        public int AboilliPrid { get; set; }

        public bool HasAboilliCartInBasket { get; set; }

        public bool HasFnacPlusCardInBasket { get; set; }

        public bool HasFnacPlusTryCardInBasket { get; set; }

        public string FnacPlusTryCardDuration { get; set; }

        public bool IsTooYoung { get; set; }

        public int TryCardDuration { get; set; }

        public bool HasBirthDate { get; set; }

        public IEnumerable<ChoiceViewModel> Days { get; set; }

        public IEnumerable<ChoiceViewModel> Months { get; set; }

        public string NIENIF { get; set; }
        public string TypeId { get; set; }

        public bool IsNif { get { return "N".Equals(TypeId, StringComparison.OrdinalIgnoreCase); } }
        public bool IsNie { get { return "T".Equals(TypeId, StringComparison.OrdinalIgnoreCase); } }

        public bool HasOptInEmail { get; set; } = false;

        public bool IsCGVAccepted { get; set; } = false;
    }

    public class MemberBirthDateViewModelLight
    {
        [Required]
        public int? BirthYear { get; set; }

        [Required]
        public int? BirthMonth { get; set; }

        [Required]
        public int? BirthDay { get; set; }
    }
}
