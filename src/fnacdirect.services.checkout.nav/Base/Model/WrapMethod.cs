using System;
using System.Xml.Serialization;

namespace FnacDirect.OrderPipe.Base.Model
{
    #region "Classe"
    [Serializable]
    public class WrapMethod : IAdditionalFeeMethod, IEquatable<WrapMethod>
    {
        [XmlIgnore]
        private int? _Id;

        public int? Id
        {
            get
            {
                return _Id;
            }
            set
            {
                _Id = value;
            }
        }

        [XmlIgnore]
        private string _Reference;

        public string Reference
        {
            get
            {
                return _Reference;
            }
            set
            {
                _Reference = value;
            }
        }

        [XmlIgnore]
        private string _Label;

        [XmlIgnore]
        public string Label
        {
            get
            {
                return _Label;
            }
            set
            {
                _Label = value;
            }
        }

        [XmlIgnore]
        private decimal? _PriceGlobalDBEur;

        [XmlIgnore]
        public decimal? PriceGlobalDBEur
        {
            get
            {
                return _PriceGlobalDBEur;
            }
            set
            {
                _PriceGlobalDBEur = value;
            }
        }

        [XmlIgnore]
        private decimal? _PriceGlobalEur;

        [XmlIgnore]
        public decimal? PriceGlobalEur
        {
            get
            {
                return _PriceGlobalEur;
            }
            set
            {
                _PriceGlobalEur = value;
            }
        }

        [XmlIgnore]
        private decimal? _PriceGlobalNoVatEUR;

        [XmlIgnore]
        public decimal? PriceGlobalNoVatEUR
        {
            get
            {
                return _PriceGlobalNoVatEUR;
            }
            set
            {
                _PriceGlobalNoVatEUR = value;
            }
        }

        [XmlIgnore]
        private decimal? _CurrentPriceNoVatEUR;

        [XmlIgnore]
        public decimal? CurrentPriceNoVatEUR
        {
            get
            {
                return _CurrentPriceNoVatEUR;
            }
            set
            {
                _CurrentPriceNoVatEUR = value;
            }
        }

        [XmlIgnore]
        private decimal? _CurrentPriceTheoEUR;

        [XmlIgnore]
        public decimal? CurrentPriceTheoEUR
        {
            get
            {
                return _CurrentPriceTheoEUR;
            }
            set
            {
                _CurrentPriceTheoEUR = value;
            }
        }

        [XmlIgnore]
        private decimal? _CurrentPriceDbEUR;

        [XmlIgnore]
        public decimal? CurrentPriceDbEUR
        {
            get
            {
                return _CurrentPriceDbEUR;
            }
            set
            {
                _CurrentPriceDbEUR = value;
            }
        }

        [XmlIgnore]
        private decimal? _CurrentPriceEUR;

        [XmlIgnore]
        public decimal? CurrentPriceEUR
        {
            get
            {
                return _CurrentPriceEUR;
            }
            set
            {
                _CurrentPriceEUR = value;
            }
        }

        [XmlIgnore]
        private decimal? _PriceDetailEur;

        [XmlIgnore]
        public decimal? PriceDetailEur
        {
            get
            {
                return _PriceDetailEur;
            }
            set
            {
                _PriceDetailEur = value;
            }
        }

        [XmlIgnore]
        private decimal? _PriceDetailNoVatEUR;

        [XmlIgnore]
        public decimal? PriceDetailNoVatEUR
        {
            get
            {
                return _PriceDetailNoVatEUR;
            }
            set
            {
                _PriceDetailNoVatEUR = value;
            }
        }

        [XmlIgnore]
        private decimal? _PriceDetailDBEur;

        [XmlIgnore]
        public decimal? PriceDetailDBEur
        {
            get
            {
                return _PriceDetailDBEur;
            }
            set
            {
                _PriceDetailDBEur = value;
            }
        }
         

        [XmlIgnore]
        private int? _OrderWrapPricePK;

        public int? OrderWrapPricePK
        {
            get
            {
                return _OrderWrapPricePK;
            }
            set
            {
                _OrderWrapPricePK = value;
            }
        }

        [XmlIgnore]
        private int? _TypeLogistic;

        public int? TypeLogistic
        {
            get
            {
                return _TypeLogistic;
            }
            set
            {
                _TypeLogistic = value;
            }
        }

        [XmlIgnore]
        private decimal? _VATRate;

        [XmlIgnore]
        public decimal? VATRate
        {
            get
            {
                return _VATRate;
            }
            set
            {
                _VATRate = value;
            }
        }

        [XmlIgnore]
        private string _VATCode;

        [XmlIgnore]
        public string VATCode
        {
            get
            {
                return _VATCode;
            }
            set
            {
                _VATCode = value;
            }
        }       

        #region IEquatable<WrapMethod> Members

        public bool Equals(WrapMethod other)
        {
            return Id.Equals(other.Id);
        }

        #endregion
    }
    #endregion

}

