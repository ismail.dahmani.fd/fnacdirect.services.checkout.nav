﻿using FnacDirect.Context;
using FnacDirect.OrderPipe.Config;
using FnacDirect.Technical.Framework.Web.UrlMgr;
using StructureMap;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;
using System.Linq;
using System;
using System.Web.Http;
using FnacDirect.OrderPipe.CoreRouting;
using FnacDirect.Technical.Framework.ServiceLocation;

namespace FnacDirect.OrderPipe.CoreMvcWeb.Routing
{
    public class AuthenticationWebApiActionFilter : ActionFilterAttribute
    {
        public MvcUiDescriptor MvcUiDescriptor { get; private set; }
        public WebUIPipeDescriptor WebUiPipeDescriptor { get; private set; }
        protected string PipeName { get; private set; }

        public override void OnActionExecuting(HttpActionContext filterContext)
        {
            var userContextProvider = ServiceLocator.Current.GetInstance<IUserContextProvider>();

            var userContext = userContextProvider.GetCurrentUserContext();

            if (!userContext.IsAuthenticated)
            {
                var opContext = GetOpContext();

                var directUrl = string.Empty;

                if (opContext != null)
                {
                    var pipe = opContext.Pipe;

                    var pipeName = pipe.PipeName;

                    var uIDescriptor = pipe.UIDescriptor as WebUIPipeDescriptor;

                    if (uIDescriptor != null)
                    {
                        var loginStepName = uIDescriptor.LoginStep;

                        var url = new UrlWriter(uIDescriptor.DefaultPageUrl).WithParam(
                                UrlWriterParameter.CreateParameter("step", loginStepName));
                        url.WithParam("pipe", pipeName).WithBaseUrlParameters();

                        directUrl = url.ToString();
                    }
                }
                else
                {
                    if (filterContext.Request.RequestUri != null)
                        directUrl = filterContext.Request.RequestUri.ToString();
                }

                if (IsAjaxRequest(filterContext.Request))
                {
                    var response = filterContext.Request.CreateResponse<string>(System.Net.HttpStatusCode.Unauthorized, null);
                    filterContext.Response = response;
                }
                else
                {
                    var response = filterContext.Request.CreateResponse<string>(System.Net.HttpStatusCode.Redirect,null);
                    response.Headers.Add("Location", directUrl);
                    filterContext.Response = response;
                }

            }

            base.OnActionExecuting(filterContext);
        }

        private bool IsAjaxRequest(HttpRequestMessage request)
        {
            IEnumerable<string> xRequestedWithHeaders;
            if (request.Headers.TryGetValues("X-Requested-With", out xRequestedWithHeaders))
            {
                var headerValue = xRequestedWithHeaders.FirstOrDefault();
                if (!string.IsNullOrEmpty(headerValue))
                {
                    return string.Equals(headerValue, "XMLHttpRequest", StringComparison.OrdinalIgnoreCase);
                }
            }

            return false;
        }

        protected OPContext GetOpContext()
        {
            return (OPContext)GlobalConfiguration.Configuration.DependencyResolver.GetService(typeof(OPContext));
            //return ObjectFactory.GetInstance<OPContext>();
        }
    }
}