﻿namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Shipping.ShipToStore
{
    public class SearchResultGeoStoreViewModel : SearchResultStoreViewModel
    {
        public double Distance { get; set; }

        public string DistanceFormat { get; set; }
    }
}