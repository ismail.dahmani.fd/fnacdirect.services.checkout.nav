﻿using FnacDirect.Subscriptions.Models;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Serialization;

namespace FnacDirect.OrderPipe.Base.BaseModel.Model
{
    public class AboIlliData : GroupData
    {
        public AboIlliData()
        {
            TypeArtIds = Enumerable.Empty<int>();
        }

        public int TrialAboilliPrid { get; set; }

        public int AboilliPrid { get; set; }

        public int TypeId { get; set; }

        [XmlIgnore]
        public IEnumerable<int> TypeArtIds { get; set; }

        public bool HasAboIlliInBasket { get; set; }

        public bool IsBasketElligibleForAboIlli { get; set; }

        public bool IsAbonne { get; set; }

        public decimal? HomeShippingCostWithAboilli { get; set; }

        public decimal? HomeShippingCostWithoutAboilli { get; set; }

        public decimal? ShopShippingCostWithAboilli { get; set; }

        public decimal? ShopShippingCostWithoutAboilli { get; set; }

        public decimal? RelayShippingCostWithAboilli { get; set; }

        public decimal? RelayShippingCostWithoutAboilli { get; set; }

        public decimal? Price { get; set; }

        public Subscription AboIlliSubscription { get; set; }

        public bool VisibleOnPanierRecap { get; set; }

        public bool VisibleOnPanierLivraison { get; set; }

        public bool IsAddedFromShippingStep { get; set; }

        public bool AddedFromShippingBackFromBasket { get; set; }

        public Base.Model.Article AboIlliSubscriptionArticle { get; set; }

        public bool AboIlliSubscriptionArticleIsTrial { get; set; }
    }
}
