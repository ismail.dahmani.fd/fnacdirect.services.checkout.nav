﻿using System.Collections.Generic;

namespace FnacDirect.OrderPipe.Base.Business.Availability
{
    public interface IAvailabilityLabelRule
    {
        string Get(IEnumerable<PopAvailabilityProcessorInput> availabilities, bool homogenous, string prefix, bool? hasManyDeliveryDates);
        EstimatedDeliveryDate ComputeSingleEstimatedDeliveryDate(PopAvailabilityProcessorInput availability);
    }
}
