﻿namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Common.Shipping
{
    public enum ShippingViewModelMode
    {
        None,
        Postal,
        Relay,
        Shop,
        BillingOnly
    }
}
