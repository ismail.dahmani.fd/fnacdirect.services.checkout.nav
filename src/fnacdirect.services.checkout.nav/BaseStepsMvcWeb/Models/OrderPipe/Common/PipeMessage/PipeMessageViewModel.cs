namespace FnacDirect.OrderPipe.BaseMvc.Web.Pop.Models.Common
{
    public class PipeMessageViewModel
    {
        public string Category { get; set; }
        public string RessourceId { get; set; }
        public string Message { get; set; }
    }
}
