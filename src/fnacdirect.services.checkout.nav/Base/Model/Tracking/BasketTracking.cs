using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace FnacDirect.OrderPipe.Base.Model.Tracking
{
    public enum EventTracking
    {
        [EnumMember(Value = "basket.add")]
        AddBasket,

        [EnumMember(Value = "basket.remove")]
        RemoveBasket,

        [EnumMember(Value = "basket.order")]
        Order
    }

    public class BasketTracking
    {
        [JsonProperty("type")]
        public const string Type = "tracking/1.1";

        [JsonProperty("site")]
        public string Site { get; set; }

        [JsonProperty("event")]
        [JsonConverter(typeof(StringEnumConverter))]
        public EventTracking Event { get; set; }

        [JsonProperty("channel")]
        public string Channel { get; set; }

        [JsonProperty("uid")]
        public string UID { get; set; }

        [JsonProperty("sid")]
        public string SID { get; set; }

        [JsonProperty("vid")]
        public string VID { get; set; }

        [JsonProperty("ts")]
        public DateTime TimeStamp { get; set; }

        [JsonProperty("order", NullValueHandling = NullValueHandling.Ignore)]
        public string Order { get; set; }

        [JsonProperty("products", NullValueHandling = NullValueHandling.Ignore)]
        public IEnumerable<Product> Products { get; set; }

        [JsonProperty("is_bot")]
        public bool IsBot { get; set; }

        public BasketTracking()
        {
            TimeStamp = DateTime.UtcNow;
            TimeStamp = TimeStamp.AddTicks(-(TimeStamp.Ticks % (TimeSpan.TicksPerSecond / 1000)));
        }
    }

    public class Product
    {
        [JsonProperty("sku")]
        public string SKU { get; set; }

        [JsonProperty("offer", NullValueHandling = NullValueHandling.Ignore)]
        public Guid? Offer { get; set; }

        [JsonProperty("baseprice", NullValueHandling = NullValueHandling.Ignore)]
        public decimal? BasePrice { get; set; }

        [JsonProperty("price")]
        public decimal Price { get; set; }

        [JsonProperty("qty", NullValueHandling = NullValueHandling.Ignore)]
        public int? Quantity { get; set; }

        public Product(BaseArticle article, EventTracking eventTracking)
        {
            if (article == null)
                throw new ArgumentNullException(nameof(article));

            SKU = $"{article.Referentiel}-{article.ProductID.GetValueOrDefault()}";

            if (article is StandardArticle stdArticle)
            {
                if (stdArticle.PriceDBEur.HasValue && (stdArticle.PriceDBEur != stdArticle.PriceUserEur))
                    BasePrice = stdArticle.PriceDBEur.GetValueOrDefault(0) + stdArticle.EcoTaxEur.GetValueOrDefault(0);

                Price = stdArticle.PriceUserEur.GetValueOrDefault(0) + stdArticle.EcoTaxEur.GetValueOrDefault(0);
            }

            if (article is MarketPlaceArticle mpArticle)
            {
                if (mpArticle.PriceDBEur.HasValue && (mpArticle.PriceDBEur != mpArticle.PriceUserEur))
                    BasePrice = mpArticle.PriceDBEur;

                Price = mpArticle.PriceUserEur.GetValueOrDefault(0);
                Offer = mpArticle.Offer.Reference;
            }

            if (eventTracking == EventTracking.Order)
                Quantity = article.Quantity;
        }
    }
}
