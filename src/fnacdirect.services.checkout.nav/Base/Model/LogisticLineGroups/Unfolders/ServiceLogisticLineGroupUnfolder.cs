﻿using System.Collections.Generic;
using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.Technical.Framework.Velocity;
using System.Linq;
using FnacDirect.OrderPipe.Base.Business.LogisticLineGroups.Internals.Unfolders;

namespace FnacDirect.OrderPipe.Base.Business.LogisticLineGroups.Unfolders
{
    public class ServiceLogisticLineGroupUnfolder : BaseLogisticLineGroupUnfolder
    {
        public override IEnumerable<Article> Unfold(IEnumerable<Article> articles)
        {
            var unfoldedArticles = new List<Article>();

            foreach (var standardArticle in articles.OfType<StandardArticle>())
            {
                var unfoldedMaster = UnfoldOne(unfoldedArticles, standardArticle, null);

                foreach (var service in standardArticle.Services)
                {
                    var unfoldedService = UnfoldOne(unfoldedArticles, service, unfoldedMaster);

                    unfoldedService.AddOnFormData = unfoldedMaster.AddOnFormData;
                }
            }

            return unfoldedArticles;
        }

        private static StandardArticle UnfoldOne(List<Article> unfoldedCollection, StandardArticle standardArticle, Article master)
        {
            var standardArticleUnfolded = unfoldedCollection.GetByPrid<StandardArticle>(standardArticle.ProductID.Value);

            if (standardArticleUnfolded != null)
            {
                standardArticleUnfolded.Quantity += standardArticle.Quantity;
            }
            else
            {
                standardArticleUnfolded = CloneUtil.DeepCopy(standardArticle);

                unfoldedCollection.Add(standardArticleUnfolded);
            }


            if (standardArticleUnfolded is Service service)
            {
                service.IsValidLink = null;
                if (service.MasterArticles == null)
                {
                    service.MasterArticles = new ArticleList();
                }
                if (master != null)
                {
                    service.MasterArticles.Add(master);
                }
            }

            standardArticleUnfolded.Services = new ServiceList();

            return standardArticleUnfolded;
        }
    }
}
