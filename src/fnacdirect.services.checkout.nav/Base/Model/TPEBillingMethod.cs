using FnacDirect.Ogone.Model;
using System;

namespace FnacDirect.OrderPipe.FDV.Model
{
    public class TPEBillingMethod : Base.Model.BillingMethod
    {
        public const int IdBillingMethod = 67108864;
        public TransactionStatus? PaymentStatus { get; set; }
        public bool IsFullyUpdated { get; set; }
        public string ClientTicket { get; set; }
        public string SellerTicket { get; set; }
        public bool HasBeenCanceledByClient { get; set; }
        public int PrinterId { get; set; }
        public int PrinterModelId { get; set; }
        public string NumCaisse { get; set; }
        public ConfirmationPaiementVenteDatas ConfirmationPaiementResult { get; set; }
        /// <summary>
        /// Numéro de séquence du ticket généré
        /// </summary>
        public string TicketID { get; set; }
        public string TicketName { get; set; }
        public string NumCaissier { get; set; }
        public DateTime? DateLot { get; set; }
        public Ticket GeneratedTicket { get; set; }
        public bool NeedRecredit { get; set; }
        public bool CancelOrderTransactionDone { get; set; }
        public string DateTrns { get; set; }
        public string HeureTrns { get; set; }
        /// <summary>
        /// Montant du don 
        /// </summary>
        public decimal? CharityAmount { get; set; }

        public TPEBillingMethod()
        {
            BillingMethodId = IdBillingMethod;
        }

        public void SetCancelledPaymentStatus()
        {
            PaymentStatus = TransactionStatus.Cancelled;
        }

        public void SetValidatedPaymentStatus()
        {
            PaymentStatus = TransactionStatus.Validated;
        }

        public void SetFailedPaymentStatus()
        {
            PaymentStatus = TransactionStatus.Failed;
        }

        public void SetAbandonPaymentStatus()
        {
            PaymentStatus = TransactionStatus.Abandon;
        }

        public void SetCancelOrderTransactionDone()
        {
            CancelOrderTransactionDone = true;
        }

        public bool HasValidStatus()
        {
            return PaymentStatus.HasValue;
        }

        public bool IsSuccess { get { return HasValidStatus() && PaymentStatus.Value == TransactionStatus.Validated; } }

        public bool IsCanceled { get { return HasValidStatus() && PaymentStatus.Value == TransactionStatus.Cancelled; } }

        public bool IsFailed { get { return HasValidStatus() && PaymentStatus.Value == TransactionStatus.Failed; } }

        public override string ToString()
        {
            return "Paiement FnacShop";
        }

        public string PaymentStatusStr
        {
            get
            {
                if (PaymentStatus.HasValue)
                    return PaymentStatus.Value.ToString();
                return string.Empty;
            }
        }

        public override bool ShouldBePersistent
        {
            get
            {
                return false;
            }
        }
    }
}
