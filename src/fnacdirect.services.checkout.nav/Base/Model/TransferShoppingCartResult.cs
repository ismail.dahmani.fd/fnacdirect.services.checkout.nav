﻿using System;
using FnacDirect.OrderPipe.Base.Model;

namespace FnacDirect.OrderPipe.Base.Business.ShoppingCart
{
    public class TransferShoppingCartResult
    {
        public ArticleList Articles { get; set; }

        public Maybe<ShopAddress> ShopAddress { get; set; }

        public TransferShoppingCartResult()
        {
            Articles = new ArticleList();

            ShopAddress = Maybe<ShopAddress>.Empty();
        }
    }
}
