using FnacDirect.OrderPipe.Base.LineGroups;
using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.Technical.Framework.Velocity;
using System.Collections.Generic;
using System.Linq;

namespace FnacDirect.OrderPipe.Base.Business.LogisticLineGroups
{
    public class LogisticLineGroupGenerator : ILogisticLineGroupGenerator
    {
        private readonly ICloneService _cloneService;

        public LogisticLineGroupGenerator(ICloneService cloneService)
        {
            _cloneService = cloneService;
        }

        public LogisticLineGroup Generate(ILineGroupHeader lineGroupHeader)
        {
            var logisticLineGroup = new LogisticLineGroup();

            if (lineGroupHeader != null)
            {
                if (lineGroupHeader.Informations != null)
                {
                    logisticLineGroup.Informations = new PopLineGroupInformations(lineGroupHeader.Informations as dynamic);
                }

                if (lineGroupHeader.Seller != null)
                {
                    logisticLineGroup.Seller = _cloneService.Clone(lineGroupHeader.Seller);
                }

                if (lineGroupHeader.GlobalPrices != null)
                {
                    logisticLineGroup.GlobalPrices = _cloneService.Clone(lineGroupHeader.GlobalPrices);
                }

                if (lineGroupHeader.LogisticTypes != null)
                {
                    logisticLineGroup.LogisticTypes = _cloneService.Clone(lineGroupHeader.LogisticTypes);
                }

                if (lineGroupHeader.VatCountry != null)
                {
                    logisticLineGroup.VatCountry = _cloneService.Clone(lineGroupHeader.VatCountry);
                }

                if (lineGroupHeader.BillingMethodConsolidation != null)
                {
                    foreach (var pair in lineGroupHeader.BillingMethodConsolidation)
                    {
                        logisticLineGroup.BillingMethodConsolidation.Add(pair.Key, pair.Value);
                    }
                }

                if (lineGroupHeader.AvailableWraps != null)
                {
                    logisticLineGroup.AvailableWraps = _cloneService.Clone(lineGroupHeader.AvailableWraps);
                }

                logisticLineGroup.OrderType = lineGroupHeader.OrderType;
                logisticLineGroup.OrderOrigin = lineGroupHeader.OrderOrigin;
                SetLineGroupUniqueId(lineGroupHeader, logisticLineGroup);
                logisticLineGroup.IsVoluminous = lineGroupHeader.IsVoluminous;
                logisticLineGroup.RemainingTotal = lineGroupHeader.RemainingTotal;
            }

            return logisticLineGroup;
        }

        public LogisticLineGroup Generate(ILogisticLineGroup logisticLineGroupSource, IEnumerable<Article> articles)
        {
            var logisticLineGroup = Generate(logisticLineGroupSource);

            if (logisticLineGroupSource.ShippingAddress != null)
            {
                logisticLineGroup.ShippingAddress = _cloneService.Clone(logisticLineGroupSource.ShippingAddress);
            }

            if (logisticLineGroupSource.ShippingMethodChoice != null)
            {
                logisticLineGroup.ShippingMethodChoice = _cloneService.Clone(logisticLineGroupSource.ShippingMethodChoice);
            }

            if (logisticLineGroupSource.SelectedShippingChoice != null)
            {
                logisticLineGroup.SelectedShippingChoice = logisticLineGroupSource.SelectedShippingChoice.Copy();
            }

            logisticLineGroup.IsSteedFromStore = logisticLineGroupSource.IsSteedFromStore;

            logisticLineGroup.Articles = new List<Article>(articles);

            var usedLogisticTypes = new LogisticTypeList();

            foreach (var article in articles)
            {
                var logisticType = article.LogType.GetValueOrDefault(0);

                var matchingType = logisticLineGroup.LogisticTypes.FirstOrDefault(t => t.Index == logisticType);

                if (matchingType != null && !usedLogisticTypes.Any(t => t.Index == logisticType))
                {
                    usedLogisticTypes.Add(matchingType);
                }
            }

            logisticLineGroup.LogisticTypes = usedLogisticTypes;

            return logisticLineGroup;
        }

        /// <summary>
        /// Methode nécessaire à la surchage FnacShop
        /// </summary>
        /// <param name="lineGroupHeader"></param>
        /// <param name="logisticLineGroup"></param>
        protected virtual void SetLineGroupUniqueId(ILineGroupHeader lineGroupHeader, LogisticLineGroup logisticLineGroup)
        {
            logisticLineGroup.UniqueId = lineGroupHeader.UniqueId;
        }
    }
}
