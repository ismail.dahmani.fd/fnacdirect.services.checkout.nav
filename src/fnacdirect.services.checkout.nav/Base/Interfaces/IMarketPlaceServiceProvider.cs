﻿using FnacDirect.Contracts.Online.Model;

namespace FnacDirect.OrderPipe.Base.Business
{
    public interface IMarketPlaceServiceProvider
    {
        IMarketPlaceService GetMarketPlaceService(SiteContext sitecontext);
    }
}
