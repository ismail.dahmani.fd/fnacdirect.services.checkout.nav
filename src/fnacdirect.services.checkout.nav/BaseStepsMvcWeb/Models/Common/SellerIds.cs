using System;
using System.Collections.Generic;
using System.Linq;

namespace FnacDirect.OrderPipe.Base.LineGroups
{
    public static class SellerIds
    {
        /// <summary>
        /// Les relations entre sous-types et type parent.
        /// </summary>
        private static readonly IDictionary<SellerType, SellerType> _subTypes = new Dictionary<SellerType, SellerType>
        {
            { SellerType.ClickAndCollect, SellerType.Fnac },
            { SellerType.Numerical, SellerType.Fnac },
            { SellerType.Shop, SellerType.Fnac },
            { SellerType.Software, SellerType.Fnac },
            { SellerType.VirtualGiftCheck, SellerType.Fnac },
            { SellerType.ShelfFaded, SellerType.Shelf },
            { SellerType.ShelfForced, SellerType.Shelf },
            { SellerType.Vitrine, SellerType.Shelf },
            { SellerType.StockFaded, SellerType.Stock },
            { SellerType.StockForced, SellerType.Stock }
        };

        /// <summary>
        /// Les types et ID(s) correspondant(s).
        /// </summary>
        private static readonly IDictionary<SellerType, Range> _types = new Dictionary<SellerType, Range>
        {
            { SellerType.MarketPlace, new Range(1, int.MaxValue) },
            { SellerType.Fnac, new Range(0) },
            { SellerType.Numerical, new Range(-1) },
            { SellerType.VirtualGiftCheck, new Range(-2) },
            { SellerType.Shop, new Range(-3) },
            { SellerType.ClickAndCollect, new Range(-4) },
            { SellerType.Software, new Range(-5) },
            { SellerType.Shelf, new Range(-10) },
            { SellerType.ShelfForced, new Range(-11) },
            { SellerType.Stock, new Range(-12) },
            { SellerType.StockForced, new Range(-13) },
            { SellerType.ShelfFaded, new Range(-14) },
            { SellerType.StockFaded, new Range(-15) },
            { SellerType.Vitrine, new Range(-16) },
            { SellerType.ClickAndCollectOnly, new Range(-199, -100) },
        };

        /// <summary>
        /// Retourne l'ID du type de seller donné.
        /// </summary>
        /// <param name="sellerType">Le type de seller</param>
        /// <returns>L'ID correspondant</returns>
        public static int GetId(SellerType sellerType)
            => GetId(sellerType, Enumerable.Empty<int>());

        public static int GetId(SellerType sellerType, IEnumerable<int> reservedSellerIds)
        {
            var range = GetRange(sellerType);
            var reservedSellerIdsArray = reservedSellerIds.ToArray();
            for (var i = range.Min; i <= range.Max; i++)
            {
                if (!reservedSellerIdsArray.Contains(i))
                {
                    return i;
                }
            }

            throw new InvalidOperationException(string.Format("No more available seller id for range ({0}, {1})", range.Min, range.Max));
        }

        /// <summary>
        /// Retourne l'ID du seller parent du seller donné.
        /// </summary>
        /// <param name="sellerId">L'ID du seller</param>
        /// <returns>L'ID du seller parent</returns>
        public static int GetParentSellerId(int sellerId)
        {
            var subType = GetType(sellerId);

            if (_subTypes.TryGetValue(subType, out var parentType))
            {
                return GetId(parentType);
            }

            return sellerId;
        }

        /// <summary>
        /// Retourne le type du seller parent du seller donné.
        /// </summary>
        /// <param name="sellerId">L'ID du seller</param>
        /// <returns>Le type du seller parent</returns>
        public static SellerType GetParentSellerType(int sellerId)
        {
            var subType = GetType(sellerId);

            if (_subTypes.TryGetValue(subType, out var parentType))
            {
                return parentType;
            }

            return GetType(sellerId);
        }

        /// <summary>
        /// Retourne le type du seller donné.
        /// </summary>
        /// <param name="sellerId">L'ID du seller</param>
        /// <returns>Le type du seller</returns>
        public static SellerType GetType(int sellerId)
        {
            foreach (var pair in _types)
            {
                if (pair.Value.Contains(sellerId))
                {
                    return pair.Key;
                }
            }

            throw new ArgumentException(string.Format("No seller type defined for seller id {0}", sellerId));
        }

        /// <summary>
        /// Indique si le seller correspond au groupe consideré comme vendeur Fnac
        /// </summary>
        public static bool IsFnacType(int sellerId)
            => GetType(sellerId) != SellerType.MarketPlace;

        /// <summary>
        /// Indique si le seller correspond un vendeur MarketPlace
        /// </summary>
        public static bool IsMarketPlaceType(int sellerId)
            => GetType(sellerId) == SellerType.MarketPlace;

        /// <summary>
        /// Indique si le seller correspond à de l'intra mag
        /// </summary>
        public static bool IsShopSellerId(int sellerId)
        {
            var shopSellerTypes = new List<SellerType>()
            {
                SellerType.Shelf,
                SellerType.ShelfFaded,
                SellerType.ShelfForced,
                SellerType.Stock,
                SellerType.StockFaded,
                SellerType.StockForced,
                SellerType.Vitrine
            };

            return shopSellerTypes.Contains(GetType(sellerId));
        }

        private static Range GetRange(SellerType sellerType)
        {
            if (!_types.TryGetValue(sellerType, out var range))
            {
                throw new ArgumentException(string.Format("No range defined for seller type {0}", sellerType));
            }

            return range;
        }

        private struct Range
        {
            public int Max { get; private set; }

            public int Min { get; private set; }

            public Range(int value) : this()
            {
                Min = value;
                Max = value;
            }

            public Range(int min, int max) : this()
            {
                Min = min;
                Max = max;
            }

            public bool Contains(int value)
                => Min <= value && value <= Max;
        }
    }
}
