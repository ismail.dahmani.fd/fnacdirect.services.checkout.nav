namespace FnacDirect.OrderPipe.Base.Model
{
    using System;
    using System.Xml.Serialization;
    using System.Reflection;
    using System.Linq;

    #region "Classe"

    public class GlobalPrices
    {
        #region Members

        private decimal? _totalEcoTaxEurNoVAT;

        #endregion Members

        [XmlIgnore]
        private decimal? _TotalSplitPriceDBEur;

        [XmlIgnore]
        public decimal? TotalSplitPriceDBEur
        {
            get
            {
                return _TotalSplitPriceDBEur;
            }
            set
            {
                _TotalSplitPriceDBEur = value;
            }
        }
        
        [XmlIgnore]
        private decimal? _TotalSplitPriceUserEur;

        [XmlIgnore]
        public decimal? TotalSplitPriceUserEur
        {
            get
            {
                return _TotalSplitPriceUserEur;
            }
            set
            {
                _TotalSplitPriceUserEur = value;
            }
        }

        [XmlIgnore]
        private decimal? _TotalSplitPriceDBNoVatEur;

        [XmlIgnore]
        public decimal? TotalSplitPriceDBNoVatEur
        {
            get
            {
                return _TotalSplitPriceDBNoVatEur;
            }
            set
            {
                _TotalSplitPriceDBNoVatEur = value;
            }
        }

        [XmlIgnore]
        private decimal? _TotalSplitPriceUserNoVatEur;

        [XmlIgnore]
        public decimal? TotalSplitPriceUserNoVatEur
        {
            get
            {
                return _TotalSplitPriceUserNoVatEur;
            }
            set
            {
                _TotalSplitPriceUserNoVatEur = value;
            }
        }


        [XmlIgnore]
        private decimal? _TotalShipPriceDBEur;

        [XmlIgnore]
        public decimal? TotalShipPriceDBEur
        {
            get
            {
                return _TotalShipPriceDBEur;
            }
            set
            {
                _TotalShipPriceDBEur = value;
            }
        }



        [XmlIgnore]
        private decimal? _TotalShipPriceUserEur;

        [XmlIgnore]
        public decimal? TotalShipPriceUserEur
        {
            get
            {
                return _TotalShipPriceUserEur;
            }
            set
            {
                _TotalShipPriceUserEur = value;
            }
        }

        [XmlIgnore]
        private decimal? _TotalShipPriceUserEurNoVAT;

        [XmlIgnore]
        public decimal? TotalShipPriceUserEurNoVAT
        {
            get
            {
                return _TotalShipPriceUserEurNoVAT;
            }
            set
            {
                _TotalShipPriceUserEurNoVAT = value;
            }
        }

        [XmlIgnore]
        private decimal? _TotalFnacShipPriceUserEur;

        [XmlIgnore]
        public decimal? TotalFnacShipPriceUserEur
        {
            get
            {
                return _TotalFnacShipPriceUserEur;
            }
            set
            {
                _TotalFnacShipPriceUserEur = value;
            }
        }

        [XmlIgnore]
        private VATAppliedList _VATList = new VATAppliedList();

        /// <summary>
        /// Liste des TVA appliquées aux articles dans la commande en cours
        /// </summary>
        [XmlIgnore]
        public VATAppliedList VATList
        {
            get
            {
                return _VATList;
            }
        }


        [XmlIgnore]
        private decimal? _TotalShoppingCartBusinessPriceDBEur;

        [XmlIgnore]
        public decimal? TotalShoppingCartBusinessPriceDBEur
        {
            get
            {
                return _TotalShoppingCartBusinessPriceDBEur;
            }
            set
            {
                _TotalShoppingCartBusinessPriceDBEur = value;
            }
        }



        [XmlIgnore]
        private decimal? _TotalShoppingCartBusinessPriceUserEur;

        [XmlIgnore]
        public decimal? TotalShoppingCartBusinessPriceUserEur
        {
            get
            {
                return _TotalShoppingCartBusinessPriceUserEur;
            }
            set
            {
                _TotalShoppingCartBusinessPriceUserEur = value;
            }
        }

        [XmlIgnore]
        private decimal? _TotalShoppingCartBusinessPriceNoVatEur;

        [XmlIgnore]
        public decimal? TotalShoppingCartBusinessPriceNoVatEur
        {
            get { return _TotalShoppingCartBusinessPriceNoVatEur; }
            set { _TotalShoppingCartBusinessPriceNoVatEur = value; }
        }

        [XmlIgnore]
        private decimal? _TotalShoppingCartBusinessWrapPriceUserEur;

        [XmlIgnore]
        public decimal? TotalShoppingCartBusinessWrapPriceUserEur
        {
            get
            {
                return _TotalShoppingCartBusinessWrapPriceUserEur;
            }
            set
            {
                _TotalShoppingCartBusinessWrapPriceUserEur = value;
            }
        }

        [XmlIgnore]
        private decimal? _TotalShoppingCartBusinessWrapPriceDBEur;

        [XmlIgnore]
        public decimal? TotalShoppingCartBusinessWrapPriceDBEur
        {
            get
            {
                return _TotalShoppingCartBusinessWrapPriceDBEur;
            }
            set
            {
                _TotalShoppingCartBusinessWrapPriceDBEur = value;
            }
        }

        [XmlIgnore]
        private decimal? _TotalShoppingCartBusinessWrapPriceNoVatEur;
        [XmlIgnore]
        public decimal? TotalShoppingCartBusinessWrapPriceNoVatEur
        {
            get { return _TotalShoppingCartBusinessWrapPriceNoVatEur; }
            set { _TotalShoppingCartBusinessWrapPriceNoVatEur = value; }
        }

        [XmlIgnore]
        private int? _TotalQuantity;

        [XmlIgnore]
        public int? TotalQuantity
        {
            get
            {
                return _TotalQuantity;
            }
            set
            {
                _TotalQuantity = value;
            }
        }



        private decimal? _TotalPriceEur;

        public decimal? TotalPriceEur
        {
            get
            {
                return _TotalPriceEur;
            }
            set
            {
                _TotalPriceEur = value;
            }
        }

        public decimal TotalPriceEurNoVat
        {
            get
            {
                return (_totalEcoTaxEurNoVAT ?? 0) +
                       (_TotalShipPriceUserEurNoVAT ?? 0) +
                       (_TotalSplitPriceUserNoVatEur ?? 0) +
                       (_TotalShoppingCartBusinessWrapPriceNoVatEur ?? 0) +
                       (_TotalShoppingCartBusinessPriceNoVatEur ?? 0) +
                       (_TotalSplitPriceDBNoVatEur ?? 0);
            }
        }

        [XmlIgnore]
        private decimal? _TotalEcoTaxEur;

        [XmlIgnore]
        public decimal? TotalEcoTaxEur
        {
            get
            {
                return _TotalEcoTaxEur;
            }
            set
            {
                _TotalEcoTaxEur = value;
            }
        }

        /// <summary>
        /// Gets or sets eco tax amount without VAT
        /// </summary>
        [XmlIgnore]
        public decimal? TotalEcoTaxEurNoVAT
        {
            get
            {
                return _totalEcoTaxEurNoVAT;
            }
            set
            {
                _totalEcoTaxEurNoVAT = value;
            }
        }

        [XmlIgnore]
        private decimal? _ShipVATRate;

        [XmlIgnore]
        public decimal? ShipVATRate
        {
            get
            {
                return _ShipVATRate;
            }
            set
            {
                _ShipVATRate = value;
            }
        }
        [XmlIgnore]
        private decimal? _SplitVATRate;

        [XmlIgnore]
        public decimal? SplitVATRate
        {
            get
            {
                return _SplitVATRate;
            }
            set
            {
                _SplitVATRate = value;
            }
        }

        [XmlIgnore]
        private decimal? _WrapVATRate;

        [XmlIgnore]
        public decimal? WrapVATRate
        {
            get
            {
                return _WrapVATRate;
            }
            set
            {
                _WrapVATRate = value;
            }
        }

        [XmlIgnore]
        public decimal TotalShipPrice
        {
            get
            {
                return (TotalShipPriceUserEur ?? 0) + (TotalSplitPriceUserEur ?? 0);
            }
        }

        public GlobalPrices() { }
        public GlobalPrices(bool prepareData) { if (prepareData)InitializeDefaultValues(); }

        public VatInfo SplitVATInfo { get; set; }

        /// <summary>
        /// Initialise les valeurs par défaut.
        /// </summary>
        private void InitializeDefaultValues()
        {
            var properties = GetType().GetProperties(BindingFlags.Instance | BindingFlags.Public);

            if (properties != null)
            {
                var propertiesToPrepare = properties.Where(p => p.PropertyType == typeof(decimal?) || p.PropertyType == typeof(int?)).ToList();

                if (propertiesToPrepare != null)
                {
                    foreach (var property in propertiesToPrepare)
                    {
                        var setMethod = property.GetSetMethod();
                        if (setMethod != null)
                        {
                            if (property.PropertyType == typeof(decimal?))
                                setMethod.Invoke(this, new object[] { decimal.Zero });
                            else if (property.PropertyType == typeof(int?))
                                setMethod.Invoke(this, new object[] { 0 });
                        }
                    }
                }
            }
        }
    }

    #endregion
}
