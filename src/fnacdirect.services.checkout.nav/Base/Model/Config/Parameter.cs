using System;
using System.Xml.Serialization;

namespace FnacDirect.OrderPipe.Config
{
	public class Parameter : IXmlSerializable
	{
		public string Key { get; set; }
		public string Value { get; set; }

		public Parameter()
		{
		}

		public Parameter(string key, string value)
		{
			Key = key;
			Value = value;
		}

		#region IXmlSerializable Members

		public System.Xml.Schema.XmlSchema GetSchema()
		{
			return null;
		}

		public void ReadXml(System.Xml.XmlReader reader)
		{
			if (reader == null)
				throw new ArgumentNullException("reader");

			Key = reader.GetAttribute("key");
			Value = reader.ReadInnerXml();
		}

		public void WriteXml(System.Xml.XmlWriter writer)
		{
			if (writer == null)
				throw new ArgumentNullException("writer");

			writer.WriteAttributeString("key", Key);
			writer.WriteRaw(Value);
		}

		#endregion
	}
}