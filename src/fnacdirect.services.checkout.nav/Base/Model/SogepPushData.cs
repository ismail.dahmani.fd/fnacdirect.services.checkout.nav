using FnacDirect.OrderPipe.Base.Model;

namespace FnacDirect.OrderPipe.Base.BaseModel.Model.Push
{
    public class SogepPushData : GroupData
    {
        private SogepPushInfo _value;
        public SogepPushInfo Value
        {
            get { return _value; }
            set { _value = value; }
        }
    }
}
