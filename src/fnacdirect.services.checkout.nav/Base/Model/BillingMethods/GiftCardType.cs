namespace FnacDirect.OrderPipe.Base.Model
{
    public enum GiftCardType : int
    {
        Fnac = 0,
        Kadeos = 1,
        Best = 2,
        Illicado = 3,
        Gift = 4,
        Darty = 5,
        SpiritOfCadeau = 28
    }

    public class GiftCardTypeValue
    {
        public string Label { get; set; }
        public string Value { get; set; }
        public GiftCardType Type { get; set; }
    }
}
