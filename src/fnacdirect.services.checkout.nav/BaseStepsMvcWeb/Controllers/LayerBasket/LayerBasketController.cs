using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using FnacDirect.Context;
using FnacDirect.Front.WebBusiness;
using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.OrderPipe.Contracts.Model.OneClick;
using FnacDirect.OrderPipe.Contracts.Services.OneClick;
using FnacDirect.OrderPipe.CoreRouting;
using FnacDirect.Technical.Framework.Web;
using FnacDirect.Technical.Framework.Web.Mvc;
using FnacDirect.Technical.Framework.Web.Mvc.Security;

namespace FnacDirect.OrderPipe.BaseMvc.Web.LayerBasket
{
    public class LayerBasketController : ViewEngineSpecificController<LayerBasketRazorViewEngine>
    {
        private readonly IExecutePipeService _executePipeService;
        private readonly ILayerBasketViewModelBuilder _layerBasketViewModelBuilder;
        private readonly IUserContextProvider _userContextProvider;
        private readonly IOneClickAvailabilityService _oneClickAvailabilityService;
        private readonly ISwitchProvider _switchProvider;

        private readonly string _orderPipeUrl;
        private readonly string _orderPipeOneClickUrl;

        private const string PipeName = "basket";

        public LayerBasketController(IUrlManager urlManager,
                                     IExecutePipeService executePipeService,
                                     ILayerBasketViewModelBuilder layerBasketViewModelBuilder,
                                     IUserContextProvider userContextProvider,
                                     IOneClickAvailabilityService oneClickAvailabilityService,
                                     ISwitchProvider switchProvider)
        {
            _executePipeService = executePipeService;
            _layerBasketViewModelBuilder = layerBasketViewModelBuilder;
            _userContextProvider = userContextProvider;
            _oneClickAvailabilityService = oneClickAvailabilityService;

            _switchProvider = switchProvider;
            _orderPipeUrl = urlManager.Sites.OrderPipe.GetPage("Root").DefaultUrl.ToString();
            _orderPipeOneClickUrl = urlManager.Sites.OrderPipe.GetPage("OneClickBasket").DefaultUrl.ToString();
        }

        [CrossOriginResourceSharing("GET", allowCredentials: true)]
        public PartialViewResult Display()
        {
            var executionResult = _executePipeService.ExecuteOrderPipe(PipeName);

            var popOrderForm = executionResult.OpContext.OF as PopOrderForm;

            var viewModel = _layerBasketViewModelBuilder.Build(popOrderForm);

            viewModel.OrderPipeUrl = _orderPipeUrl;

            var hasEbook = false;

            var inputs = new List<OneClickAvailabilityInput>();

            foreach (var article in popOrderForm.LineGroups.GetArticles())
            {
                var input = new OneClickAvailabilityInput()
                {
                    Referentiel = article.Referentiel,
                    ProductId = article.ProductID.GetValueOrDefault()
                };

                if (article is MarketPlaceArticle)
                {
                    input.OfferReference = (article as MarketPlaceArticle).Offer.Reference;
                }

                if (article is StandardArticle && (article as StandardArticle).IsEbook)
                {
                    hasEbook = true;
                }

                inputs.Add(input);
            }

            if (_switchProvider.IsEnabled("orderpipe.pop.layerbasket.hasebook.disableoneclick") && hasEbook)
            {
                return PartialView("_View", viewModel);
            }

            if (_oneClickAvailabilityService.IsEligible(inputs))
            {
                var userContext = _userContextProvider.GetCurrentUserContext();
                viewModel.OrderPipeOneclickUrl = _orderPipeOneClickUrl;
            }

            if (!_switchProvider.IsEnabled("orderpipe.pop.fdv.enable"))
            {
                if (popOrderForm != null)
                {
                    W3ShoppingCart.IssueNbArtCookie(NumberOfArticlesInBasket(popOrderForm));
                }
            }
            return PartialView("_View", viewModel);
        }
        
        private int NumberOfArticlesInBasket(PopOrderForm orderForm)
        {
            int quantity = orderForm.LineGroups.GetArticles().Sum(a => a.Quantity.GetValueOrDefault()) - (int)orderForm.LineGroups.GetArticles().OfType<StandardArticle>().Where(a => a.Hidden).Sum(a => a.Quantity);
            quantity += orderForm.LineGroups.GetArticles().OfType<StandardArticle>().Where(a => a.Services != null && a.Services.Any()).SelectMany(a => a.Services).Sum(service => service.Quantity.GetValueOrDefault());
            return quantity;
        }
    }
}
