using FnacDirect.Customer.Model;
using FnacDirect.OrderPipe.Base.Model;

namespace FnacDirect.OrderPipe.Base.Business
{
    public interface IAddressOPBusiness
    {
        string FindAddressReferenceInCustomerAddressBook(PostalAddress orderShippingAddress, string accountReference, string culture);
        AddressEntity GetBillingAddressInCustomerAddressBook(BillingAddress orderBillingAddress, string accountReference, string culture);
        int GetRelayId(int orderId);
        int GetShopId(int orderId);
    }
}
