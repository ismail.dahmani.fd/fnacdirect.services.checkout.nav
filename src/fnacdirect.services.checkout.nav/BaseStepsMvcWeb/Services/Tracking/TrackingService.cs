using Common.Logging;
using FnacDirect.Framework.PubSub.Producer;
using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.OrderPipe.Base.Model.Tracking;
using FnacDirect.OrderPipe.BaseMvc.Web.Models.Tracking;
using FnacDirect.OrderPipe.BaseMvc.Web.Services.Article.Pipe;
using FnacDirect.Technical.Framework.Web;
using FnacDirect.Technical.Framework.Web.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Services.Tracking
{
    public class TrackingService : ITrackingService
    {
        private const string _isBotHeader = "X-DataDome-isbot";
        private const string _isBotValue = "1";
        private const string _vidCookie = "VID";
        private readonly IHttpContextFactory _httpContextFactory;
        private readonly IMessageProducer<BasketTracking> _basketTrackingMessageProducer;
        private readonly IPipeArticleService _pipeArticleService;
        private readonly ILog _logger;
        private readonly IApplicationContext _applicationContext;
        private readonly string _site;
        private readonly string _channel;

        public TrackingService(IHttpContextFactory httpContextFactory,
            IMessageProducer<BasketTracking> basketTrackingMessageProducer,
            IPipeArticleService pipeArticleService,
            ILog logger,
            IApplicationContext applicationContext)
        {
            _basketTrackingMessageProducer = basketTrackingMessageProducer;
            _pipeArticleService = pipeArticleService;
            _logger = logger;
            _applicationContext = applicationContext;
            _site = applicationContext.GetSite().ToLower();
            _channel = GetChannelType();
            _httpContextFactory = httpContextFactory;
        }

        /// <summary>
        /// Emit tracking message for one article
        /// </summary>
        /// <param name="popOrderForm"></param>
        /// <param name="trackingArticleModel"></param>
        /// <param name="eventTracking"></param>
        public void TrackArticle(PopOrderForm popOrderForm, TrackingArticleModel trackingArticleModel, EventTracking eventTracking)
        {
            TrackArticles(popOrderForm, new List<TrackingArticleModel> { trackingArticleModel }, eventTracking);
        }

        /// <summary>
        /// Emit tracking message for list articles
        /// </summary>
        /// <param name="popOrderForm"></param>
        /// <param name="trackingArticleModels"></param>
        /// <param name="eventTracking"></param>
        public void TrackArticles(PopOrderForm popOrderForm, IEnumerable<TrackingArticleModel> trackingArticleModels, EventTracking eventTracking)
        {
            var articles = new List<BaseArticle>();
            var httpContextBase = _httpContextFactory.Create();

            foreach (var trackingArticleModel in trackingArticleModels)
            {
                try
                {
                    var article = _pipeArticleService.GetArticle(trackingArticleModel.Prid, trackingArticleModel.Offer);

                    if (eventTracking == EventTracking.Order)
                        article.Quantity = trackingArticleModel.Quantity;

                    articles.Add(article);
                }
                catch (Exception ex)
                {
                    _logger.Warn($"TrackingService : Failure to get article prid:{trackingArticleModel.Prid} offer:{trackingArticleModel.Offer}", ex);
                }
            }

            try
            {
                if (!articles.Any())
                {
                    return;
                }

                _basketTrackingMessageProducer.Emit(new BasketTracking
                {
                    Site = _site,
                    Event = eventTracking,
                    Channel = _channel,
                    SID = popOrderForm?.SID?.ToLower(),
                    UID = popOrderForm?.UserContextInformations?.UID?.ToLower(),
                    VID = httpContextBase.Request.Cookies[_vidCookie]?.Value,
                    IsBot = httpContextBase.Request.Headers.Get(_isBotHeader) == _isBotValue,
                    Order = eventTracking == EventTracking.Order ? popOrderForm?.OrderGlobalInformations?.MainOrderUserReference : null,
                    Products = articles.Select(a => new Product(a, eventTracking))
                });

            }
            catch (Exception ex)
            {
                _logger.Error($"TrackingService : Failure to Emit message", ex);
            }
        }

        private string GetChannelType()
        {
            if (_applicationContext.IsMobile)
                return Constants.ChannelType.MOBILE;
            if (_applicationContext.IsMobileApplication)
                return Constants.ChannelType.MOBILEAPP;

            return Constants.ChannelType.DESKTOP;
        }
    }
}
