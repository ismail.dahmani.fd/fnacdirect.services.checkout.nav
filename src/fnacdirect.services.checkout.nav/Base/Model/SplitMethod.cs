using System;
using System.Collections.Generic;

namespace FnacDirect.OrderPipe.Base.Model
{
    public class SplitMethodBase
    {
        public int ShippingZone { get; set; }
        public int LogisticType { get; set; }
        public int MinDelay { get; set; }
        public int MaxDelay { get; set; }
        public decimal UnitPrice { get; set; }
        public decimal GlobalPrice { get; set; }
        public GenericPrice UnitPriceGeneric { get; set; }
        public GenericPrice GlobalPriceGeneric { get; set; }
    }

    public class SplitMethod : SplitMethodBase
    {     
    }

    public class SplitPrecoMethod : SplitMethodBase
    {
        public DateTime? datePreco = null;
    }
    public class SplitMethodList
    {
        Dictionary<string, List<SplitMethod>> dico = new Dictionary<string, List<SplitMethod>>();

        public void Add(SplitMethod item)
        {
            var key=string.Format("{0}-{1}", item.ShippingZone, item.LogisticType);
            if (!dico.TryGetValue(key, out var lst))
            {
                lst = new List<SplitMethod>();
                dico.Add(key, lst);
            }
            lst.Add(item);
        }

        public List<SplitMethod> this[int sz, int lt]
        {
            get
            {
                var key = string.Format("{0}-{1}", sz, lt);
                dico.TryGetValue(key, out var met);
                return met;
            }
        }

    }

}
