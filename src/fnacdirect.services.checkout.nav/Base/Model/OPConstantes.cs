﻿namespace FnacDirect.OrderPipe
{
    public class OPConstantes
    {
        public const int ID_Gender_Mr = 2;
        public const int ID_Gender_Mme = 3;
        public const int ID_Gender_Mlle = 4;

        public const string DP_FloatingBasketExceedQty = "DP_FloatingBasketExceedQty";
    }
}
