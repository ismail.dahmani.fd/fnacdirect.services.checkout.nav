namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Basket
{
    public class OptinWarrantyUserChoiceViewModel
    {
        public bool IsOptinSelected { get; set; }
    }
}
