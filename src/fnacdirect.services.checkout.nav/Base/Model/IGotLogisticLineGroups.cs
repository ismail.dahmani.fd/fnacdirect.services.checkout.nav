﻿using FnacDirect.OrderPipe.Base.LineGroups;
using System.Collections.Generic;

namespace FnacDirect.OrderPipe.Base.Model.Facets
{
    public interface IGotLogisticLineGroups : IOF
    {
        IEnumerable<ILogisticLineGroup> LogisticLineGroups { get; }
        void AddLogisticLineGroup(ILogisticLineGroup logisticLineGroup);
        void RemoveLogisticLineGroup(ILogisticLineGroup logisticLineGroup);
    }
}
