﻿using FnacDirect.OrderPipe.Base.Business.Limonetik;
using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.Technical.Framework.Web.Mvc;
using System.Globalization;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Common.Payment.BillingMethodFactories
{
    public class LimonetikViewModelFactory : BasePaymentViewModelFactory<LimonetikBillingMethod>
    {
        private readonly IApplicationContext _applicationContext;
        private readonly ILimonetikBusiness _limonetikBusiness;

        public LimonetikViewModelFactory(IApplicationContext applicationContext,
                                         ILimonetikBusiness limonetikBusiness)
        {
            _applicationContext = applicationContext;
            _limonetikBusiness = limonetikBusiness;
        }

        public override BillingMethodViewModel Get(string tabName, PaymentMethod paymentMethod, OPContext opContext, PopOrderForm popOrderForm)
        {
            var viewModel = new LimonetikBillingMethodViewModel();

            foreach (var id in _limonetikBusiness.GetValidLimonetikCardTypes(opContext))
            {
                viewModel.AddGiftCardTypeViewModel(_applicationContext.GetMessage(string.Format("orderpipe.pop.payment.giftcard.limonetik.{0}", id)), id.ToString(CultureInfo.InvariantCulture));
            }

            viewModel.Display = true;
            return viewModel;
        }
    }
}
