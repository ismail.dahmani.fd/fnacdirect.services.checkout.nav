using FnacDirect.OrderPipe.Base.Business;
using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.OrderPipe.Core.Services;
using FnacDirect.Technical.Framework.Web.Mvc;
using System.Collections.Generic;
using System.Linq;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Common.Payment.BillingMethodFactories
{
    public class GiftCardViewModelFactory : BasePaymentViewModelFactory<GiftCardBillingMethod>
    {
        private readonly IPipeParametersProvider _pipeParametersProvider;
        private readonly IGiftCardBusiness _giftCardBusiness;
        private readonly IPriceHelper _priceHelper;

        public GiftCardViewModelFactory(IPipeParametersProvider pipeParametersProvider,
                                        IGiftCardBusiness giftCardBusiness,
                                        IApplicationContext applicationContext)
        {
            _pipeParametersProvider = pipeParametersProvider;
            _giftCardBusiness = giftCardBusiness;
            _priceHelper = applicationContext.GetPriceHelper();
        }

        public override BillingMethodViewModel Get(string tabName, PaymentMethod paymentMethod, OPContext opContext, PopOrderForm popOrderForm)
        {
            var viewModel = new GiftCardBillingMethodViewModel();
            var isGuest = popOrderForm.UserContextInformations.IsGuest;

            var giftCard = FilterGiftCards(_giftCardBusiness.GetGiftCards(), isGuest);

            giftCard.ForEach(gc =>
                viewModel.AddGiftCardTypeViewModel(
                gc.Label,
                gc.Value,
                _pipeParametersProvider.Get($"gift.card.mask.{gc.Value}", string.Empty)));


            var giftCardBillingMethods = popOrderForm.OrderBillingMethods.OfType<GiftCardBillingMethod>().ToList();
            var gcComparer = new GiftCardBillingMethodComparer();
            giftCardBillingMethods.Sort(gcComparer);
            viewModel.GiftCardsUsed = new List<GiftCardViewModel>();

            foreach (var billingMethod in giftCardBillingMethods)
            {
                viewModel.GiftCardsUsed.Add(new GiftCardViewModel(billingMethod.CardNumber.ApplyMask("#### - #### - #### - #### - ###", '#'),
                                                                  billingMethod.ChoosenAmount.Value,
                                                                  billingMethod.MaxAmount.Value,
                                                                  billingMethod.CardType != "C1",
                                                                  billingMethod.Valid.GetValueOrDefault(),
                                                                  billingMethod.EmiterName));
            }

            // Maximum allowed amount
            if(TryGetMaximumAllowedAmountForClickAndCollect(popOrderForm, out var maximumAllowedAmound))
            {
                viewModel.HasMaximumAllowedAmountForClickAndCollect = true;
                viewModel.MaximumAllowedAmountForClickAndCollect = maximumAllowedAmound;
            }

            // calcul du montant restant
            viewModel.RemainingAmount = popOrderForm.GlobalPrices.TotalPriceEur.GetValueOrDefault();
            foreach (var m in popOrderForm.OrderBillingMethods)
            {
                viewModel.RemainingAmount -= m.Amount.GetValueOrDefault();
            }

            if (viewModel.GiftCardTypes.Count != 0 || viewModel.GiftCardsUsed.Count != 0)
            {
                viewModel.Display = true;
            }

            return viewModel;
        }

        private List<GiftCardTypeValue> FilterGiftCards(List<GiftCardTypeValue> giftCards, bool isGuest)
        {
            if (!isGuest)
            {
                return giftCards;
            }

            return giftCards.Where(g => (g.Type != GiftCardType.Fnac
                                                && g.Type != GiftCardType.Darty
                                                && g.Type != GiftCardType.Kadeos)
                                                && g.Type != GiftCardType.Illicado)
                                       .ToList();
        }

        private bool TryGetMaximumAllowedAmountForClickAndCollect(PopOrderForm popOrderForm, out decimal? maximumAllowedAmount)
        {
            // Si C&C
            if (popOrderForm.HasClickAndCollectArticle())
            {
                maximumAllowedAmount = _pipeParametersProvider.Get("giftcard.constraint.clickandcollect.maxamount", int.MaxValue);
                return true;
            }

            maximumAllowedAmount = null;
            return false;
        }
    }
}
