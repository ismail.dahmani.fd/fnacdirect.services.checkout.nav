using FnacDirect.InstallmentPayment.Models;
using System.Collections.Generic;

namespace FnacDirect.OrderPipe.Base.Business.InstallmentPayment
{
    public interface IInstallmentService
    {
        IEnumerable<Offer> GetEligibles(InstallmentServiceParameters parameters);
        PartnerOffer GetPartnerOffer(InstallmentServiceParameters parameters);
        IEnumerable<Parameters> Get(InstallmentServiceParameters parameters);
        IEnumerable<Parameters> Validate(InstallmentServiceParameters parameters);
        PartnerOffer GetEligibleCreditCardIds(InstallmentServiceParameters parameters);
        IEnumerable<Offer> BuildOffersFromCreditCardTypeConfigs(InstallmentServiceParameters parameters);
        IEnumerable<Offer> BuildOffersFromCreditCardTypeIds(InstallmentServiceParameters parameters);
    }
}