using FnacDirect.OrderPipe.Base.Model;
using System.Collections.Generic;

namespace FnacDirect.OrderPipe.Base.Proxy.Ogone.PspId
{
    public interface IPspIdService
    {
        string ComputePspId(OrderTypeEnum orderType, IEnumerable<Article> articles, OgoneConfigurationData ogoneConfigurationData);
    }
}

