using System;
using System.Collections.Generic;

namespace FnacDirect.OrderPipe.Base.Proxy.Ogone
{
    public interface IOrderTransactionService
    {
        bool SaveTransactions(Guid pUID, Dictionary<string, string> dicoData, string pCulture);
        void UpdateOrderCreditCard(string pOrderCreditCardReference, Dictionary<string, string> pParams, int pUserId, string pCulture);
    }
}
