﻿
using FnacDirect.Technical.Framework;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Common.Shipping
{
    public class GeoPositionViewModel
    {
        public int Id { get; set; }

        public Position ReferencePosition { get; set; }
    }
}
