namespace FnacDirect.OrderPipe.Base.Business.Configuration
{
    public interface ISolexConfigurationProvider
    {
        SolexConfiguration Get();
    }
}
