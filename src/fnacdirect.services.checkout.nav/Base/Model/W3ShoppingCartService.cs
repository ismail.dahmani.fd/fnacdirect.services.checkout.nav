﻿using FnacDirect.Front.WebBusiness;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FnacDirect.OrderPipe.Base.Business.ShoppingCart
{
    public class W3ShoppingCartService : IW3ShoppingCart
    {
        public void IssueNbArtCookie(int quantities)
        {
            W3ShoppingCart.IssueNbArtCookie(quantities);
        }
    }
}
