using FnacDirect.Customer.BLL;
using FnacDirect.Customer.IModel;
using FnacDirect.Ogone.BLL;
using FnacDirect.Ogone.Model;
using FnacDirect.OrderHistory.OrderPaymentRegularization;
using FnacDirect.Technical.Framework.Web.Http.Services;
using System;
using System.Collections.Generic;
using System.Linq;

namespace FnacDirect.OrderPipe.Base.Proxy.Ogone
{
    public class OrderTransactionService : IOrderTransactionService
    {
        private readonly IOrderTransactionManager _orderTransactionManager;
        private readonly ILogger _logger;
        private readonly ICreditCard _creditCard;

        public OrderTransactionService(IOrderTransactionManager orderTransactionManager, ILogger logger, ICreditCard creditCard)
        {
            _orderTransactionManager = orderTransactionManager;
            _logger = logger;
            _creditCard = creditCard;
        }

        public bool SaveTransactions(Guid pUID, Dictionary<string, string> dicoData, string pCulture)
        {
            SaveTransactionsAndCard(pUID, dicoData, pCulture, out var result, out var ogoneAcceptedOrderTransactions);

            return result;
        }

        private void SaveCardInDatabase(Guid pUID, Dictionary<string, string> dicoData, string pCulture, List<FnacDirect.Ogone.Model.OrderTransaction> ogoneAcceptedOrderTransactions)
        {
            if (ogoneAcceptedOrderTransactions.Any())
            {
                dicoData.TryGetValue("AliasSend", out var aliasSend);

                dicoData.TryGetValue("CreditCardReference", out var creditcardReference);

                dicoData.TryGetValue("cbusage", out var cbUsagestr);
                int? cbUsage = null;

                if (!string.IsNullOrEmpty(cbUsagestr))
                {
                    if (int.TryParse(cbUsagestr, out var cbUsageout))
                    {
                        cbUsage = cbUsageout;
                    }
                }

                var SaveAliasInWallet = false;
                if (dicoData.TryGetValue("SaveAlias", out var saveAlias))
                {
                    SaveAliasInWallet = bool.Parse(saveAlias);
                }

                foreach (var ogoneAcceptedOrderTransaction in ogoneAcceptedOrderTransactions)
                {
                    _creditCard.UpdateOrderCreditCard(ogoneAcceptedOrderTransaction.OrderId,
                        ogoneAcceptedOrderTransaction.OrderTransactionOgone,
                        cbUsage,
                        pCulture);

                    if (SaveAliasInWallet)
                    {
                        var NCCREF = _creditCard.SaveCardInDatabase(pUID,
                            ogoneAcceptedOrderTransaction.OrderTransactionOgone,
                            aliasSend,
                            creditcardReference,
                            pCulture,
                            cbUsage,
                            true);

                        dicoData.Add("NCCREF", NCCREF);

                        SaveAliasInWallet = false;
                    }
                }
            }
        }

        private void SaveTransactions(Guid pUID, Dictionary<string, string> dicoData, string pCulture, out bool result, out List<FnacDirect.Ogone.Model.OrderTransaction> ogoneAcceptedOrderTransactions)
        {
            result = _orderTransactionManager.SaveTransactions(pUID, dicoData, pCulture, out ogoneAcceptedOrderTransactions);
        }

        private void SaveTransactionsAndCard(Guid pUID, Dictionary<string, string> dicoData, string pCulture, out bool result, out List<OrderTransaction> ogoneAcceptedOrderTransactions)
        {
            SaveTransactions(pUID, dicoData, pCulture, out result, out ogoneAcceptedOrderTransactions);
            SaveCardInDatabase(pUID, dicoData, pCulture, ogoneAcceptedOrderTransactions);
        }

        public void UpdateOrderCreditCard(string pOrderCreditCardReference, Dictionary<string, string> pParams, int pUserId, string pCulture)
        {
            var oprBusiness = new OPRBusiness();
            oprBusiness.UpdateOrderCreditCard(pOrderCreditCardReference, pParams, pUserId, pCulture);
        }
    }
}
