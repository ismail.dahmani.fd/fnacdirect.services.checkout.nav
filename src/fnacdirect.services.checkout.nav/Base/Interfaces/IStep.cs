using System;
using System.Collections.Generic;
using FnacDirect.OrderPipe.Config;
using FnacDirect;

namespace FnacDirect.OrderPipe
{
    /// <summary>
    /// Interface de base d'une �tape
    /// </summary>
    public interface IStep
    {
        /// <summary>
        /// Nom de l'�tape
        /// </summary>
        string Name { get; set; }

        /// <summary>
        /// Nom du groupe d'�tape auquel appartient l'�tape
        /// </summary>
        string Group { get; set; }
        
        /// <summary>
        /// Liste des raccourcis auxquels appartient l'�tape
        /// </summary>
        List<string> Shortcuts { get; }

        /// <summary>
        /// Retourne la liste des param�tres de l'�tape, tels qu'indiqu�s dans la configuration
        /// </summary>
        ParameterList Parameters { get; set; }

        /// <summary>
        /// Phase d'initialisation, juste apr�s la configuration du Pipe
        /// (Context n'est pas renseign�)
        /// </summary>
        void Init(IOP pipe, ISwitchProvider switchProvider);

        bool Run(OPContext context, PipeResult res, bool force);
    }
}