﻿
using System.ComponentModel.DataAnnotations;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Common.Payment
{
    public class PayByRefBillingMethodViewModel : BillingMethodViewModel
    {
        [Technical.Framework.Web.Mvc.DataAnnotations.RegularExpression("(^$)|(^\\d{21}$)", ErrorMessage = "orderpipe.pop.paybyref.nib.format")]
        [MaxLength(21)]
        
        public string Nib { get; set; }
    }
}
