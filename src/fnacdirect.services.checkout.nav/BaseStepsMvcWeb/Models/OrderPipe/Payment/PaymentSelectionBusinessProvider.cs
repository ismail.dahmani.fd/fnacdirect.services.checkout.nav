

using FnacDirect.Technical.Framework.CoreServices;
using FnacDirect.Technical.Framework.ServiceLocation;
using StructureMap;
namespace FnacDirect.OrderPipe.Base.Business.Payment
{
    public class PaymentSelectionBusinessProvider : IPaymentSelectionBusinessProvider
    {
        public PaymentSelectionBusinessProvider()
        {
        }

        public IPaymentSelectionBusiness GetPaymentSelectionBusinessInstance()
        {
            return ServiceLocator.Current.GetInstance<IPaymentSelectionBusiness>();
        }
   }
}