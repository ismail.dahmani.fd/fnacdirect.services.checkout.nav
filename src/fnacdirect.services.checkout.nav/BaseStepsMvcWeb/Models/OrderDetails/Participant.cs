﻿
namespace FnacDirect.OrderPipe.Base.Business.OrderDetail
{
    public class Participant
    {
        public int Id { get; set; }

        public string Name { get; set; }
    }
}
