namespace FnacDirect.OrderPipe.Base.Business.Payment
{
    public interface ICheckCreditCardsEligibilityBusiness
    {
        bool IsPaypalViaOgoneEligible();
        ECBEligibilityResult CheckECBEligibility();
        bool CheckPayByPhoneEligibility();
        bool CheckPostFinanceEligibility();
        bool IsCaixaEligible();
    }
}
