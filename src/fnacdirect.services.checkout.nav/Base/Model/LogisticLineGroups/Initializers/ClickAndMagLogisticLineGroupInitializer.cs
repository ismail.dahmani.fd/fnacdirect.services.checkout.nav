﻿using FnacDirect.OrderPipe.Base.Business.LogisticLineGroups.Internals.Initializers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FnacDirect.OrderPipe.Base.LineGroups;
using FnacDirect.OrderPipe.Base.Model;

namespace FnacDirect.OrderPipe.Base.Business.LogisticLineGroups.Initializers
{
    public class ClickAndMagLogisticLineGroupInitializer : BaseLogisticLineGroupInitializer
    {
        private readonly ILogisticLineGroupGenerator _logisticLineGroupGenerator;

        public ClickAndMagLogisticLineGroupInitializer(ILogisticLineGroupGenerator logisticLineGroupGenerator)
        {
            _logisticLineGroupGenerator = logisticLineGroupGenerator;
        }

        public override IEnumerable<ILogisticLineGroup> Initialize(IDictionary<ILineGroupHeader, IEnumerable<Article>> selectedLineGroups)
        {
            var shippingMethodEvaluation = Context.IGotShippingMethodEvaluation.ShippingMethodEvaluation;

            var output = new List<ILogisticLineGroup>();

            foreach (var lineGroup in selectedLineGroups.Keys)
            {
                var logisticLineGroup = _logisticLineGroupGenerator.Generate(lineGroup);

                logisticLineGroup.Articles = new List<Article>(selectedLineGroups[lineGroup]);

                Helper.CopyShippingMethodEvaluation(lineGroup, logisticLineGroup, shippingMethodEvaluation);

                if(logisticLineGroup.ShippingAddress is ShopAddress)
                {
                    logisticLineGroup.Informations.OrderType = (int)OrderInfoOrderTypeEnum.CMRetraitMagasin;
                }
                else
                {
                    logisticLineGroup.Informations.OrderType = (int)OrderInfoOrderTypeEnum.CMLivraisonDomicile;
                }

                lineGroup.Informations.OrderType = logisticLineGroup.Informations.OrderType;

                output.Add(logisticLineGroup);
            }

            return output;
        }
    }
}
