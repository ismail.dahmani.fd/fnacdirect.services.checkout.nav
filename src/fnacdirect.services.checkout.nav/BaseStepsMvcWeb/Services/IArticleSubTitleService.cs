﻿using FnacDirect.Contracts.Online.Model;
using BaseArticle = FnacDirect.OrderPipe.Base.Model.BaseArticle;

namespace FnacDirect.OrderPipe.Base.Business
{
    public interface IArticleSubTitleService
    {
        string BuildSubTitle(BaseArticle baseArticle, Article dtoArticle);
    }
}
