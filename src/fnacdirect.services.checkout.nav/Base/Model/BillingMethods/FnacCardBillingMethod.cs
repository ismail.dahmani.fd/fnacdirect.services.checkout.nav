using System.Xml.Serialization;

namespace FnacDirect.OrderPipe.Base.Model
{
    public class FnacCardBillingMethod : BillingMethod
    {
        public const int IdBillingMethod = 65536;

        public override bool IsPrimary => true;

        [XmlIgnore]
        private string _CardNumber;

        public string CardNumber
        {
            get
            {
                return _CardNumber;
            }
            set
            {
                _CardNumber = value;
            }
        }

        [XmlIgnore]
        private string _CardHolder;

        public string CardHolder
        {
            get
            {
                return _CardHolder;
            }
            set
            {
                _CardHolder = value;
            }
        }

        [XmlIgnore]
        private string _CardExpirationDate;

        public string CardExpirationDate
        {
            get
            {
                return _CardExpirationDate;
            }
            set
            {
                _CardExpirationDate = value;
            }
        }

        [XmlIgnore]
        private string _CreditType;

        public string CreditType
        {
            get
            {
                return _CreditType;
            }
            set
            {
                _CreditType = value;
            }
        }

        [XmlIgnore]
        private string _VerificationCode;

        public string VerificationCode
        {
            get
            {
                return _VerificationCode;
            }
            set
            {
                _VerificationCode = value;
            }
        }

        public FnacCardBillingMethod()
        {
            BillingMethodId = IdBillingMethod;
            CanModifyAmountUp = true;
            CanModifyAmountDown = true;
        }
    }
}
