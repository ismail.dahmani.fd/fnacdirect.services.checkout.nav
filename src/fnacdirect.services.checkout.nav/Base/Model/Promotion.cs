using System;
using System.Data;
using System.Xml.Serialization;
using System.Collections.Generic;

namespace FnacDirect.OrderPipe.Base.Model
{
    [Serializable]
    [XmlType("FnacDirect.OrderPipe.Base.Model.Promotion")]
    public class Promotion
    {
        [XmlIgnore]
        public string AdvantageCode { get; set; }
         
        [XmlIgnore]
        private bool? _hasThreshold;

        public bool? HasThreshold
        {
            get
            {
                return _hasThreshold;
            }
            set
            {
                _hasThreshold = value;
            }
        }
        [XmlIgnore]
        private bool? _isNotSameCriteria;

        public bool? IsNotSameCriteria
        {
            get
            {
                return _isNotSameCriteria;
            }
            set
            {
                _isNotSameCriteria = value;
            }
        }
        [XmlIgnore]
        private int? _DiscountType;

        public int? DiscountType
        {
            get
            {
                return _DiscountType;
            }
            set
            {
                _DiscountType = value;
            }
        }
        [XmlIgnore]
        private int? _FreeArtId;

        public int? FreeArtId
        {
            get
            {
                return _FreeArtId;
            }
            set
            {
                _FreeArtId = value;
            }
        }
        [XmlIgnore]
        private string _DescriptionUser;

        public string DescriptionUser
        {
            get
            {
                return _DescriptionUser;
            }
            set
            {
                _DescriptionUser = value;
            }
        }
        [XmlIgnore]
        private string _DisplayText;

        public string DisplayText
        {
            get
            {
                return _DisplayText;
            }
            set
            {
                _DisplayText = value;
            }
        }
        [XmlIgnore]
        private string _ConditionsPage;

        public string ConditionsPage
        {
            get
            {
                return _ConditionsPage;
            }
            set
            {
                _ConditionsPage = value;
            }
        }
        [XmlIgnore]
        private string _Banner;

        public string Banner
        {
            get
            {
                return _Banner;
            }
            set
            {
                _Banner = value;
            }
        }
        [XmlIgnore]
        private string _DisplayImage;

        public string DisplayImage
        {
            get
            {
                return _DisplayImage;
            }
            set
            {
                _DisplayImage = value;
            }
        }
        [XmlIgnore]
        private bool? _Cumulable;

        public bool? Cumulable
        {
            get
            {
                return _Cumulable;
            }
            set
            {
                _Cumulable = value;
            }
        }
        [XmlIgnore]
        private int? _Type;

        public int? Type
        {
            get
            {
                return _Type;
            }
            set
            {
                _Type = value;
            }
        }

        [XmlIgnore]
        private string _Code;

        public string Code
        {
            get
            {
                return _Code;
            }
            set
            {
                _Code = value;
            }
        }

        [XmlIgnore]
        private string _Ship;

        public string Ship
        {
            get
            {
                return _Ship;
            }
            set
            {
                _Ship = value;
            }
        }

         [XmlIgnore]
        private decimal? _DiscountEur;


        [XmlIgnore]
        public decimal? DiscountEur
        {
            get
            {
                return _DiscountEur;
            }
            set
            {
                _DiscountEur = value;
            }
        }
        
        [XmlIgnore]
        public decimal? Rebate { get; set; }

        public int Usage { get; set; }

        public int DisplayTemplate { get; set; }
    }
}
