using System.Collections.Generic;
using Newtonsoft.Json;

namespace FnacDirect.OrderPipe.Base.Model.FDV.OrderTransaction
{
    /// <summary>
    /// Informations relative à un paiment carte cadeau
    /// </summary>
    public class GiftCardInfo
    {
        public GiftCardInfo(decimal amount, int giftCardPaymentType, string giftCardPaymentTypeLabel, int authorizationId, string cardNumber) : base()
        {
            Amount = amount;
            GiftCardPaymentType = giftCardPaymentType;
            GiftCardPaymentTypeLabel = giftCardPaymentTypeLabel;
            AuthorizationId = authorizationId;
            CardNumber = cardNumber;
        }

        /// <summary>
        /// Montant
        /// </summary>
        public decimal Amount { get; set; }

        /// <summary>
        /// Type de carte cadeau
        /// </summary>
        public int GiftCardPaymentType { get; set; }

        /// <summary>
        /// Label du type de  carte
        /// </summary>
        public string GiftCardPaymentTypeLabel { get; set; }

        /// <summary>
        /// Identifiant de transaction pour le prestataire
        /// </summary>
        public int AuthorizationId { get; set; }

        /// <summary>
        /// Numéro de la carte
        /// </summary>
        public string CardNumber { get; set; }

        /// <summary>
        /// Génère un json à partir d'une liste de payments par giftCard
        /// </summary>
        /// <param name="giftcardPayments">Liste des paiments par carte cadeau</param>
        /// <returns></returns>
        public static string GenerateJsonDetails(IList<PaymentTransactionResult> giftcardPayments)
        {
            var lst = new List<GiftCardInfo>();
            foreach (var item in giftcardPayments)
            {
                lst.Add(new GiftCardInfo(item.Amount,
                                            (int)item.PaymentType,
                                            item.PaymentTypeLabel,
                                            item.GiftCardTransactionInfo.AuthorizationId,
                                            item.GiftCardTransactionInfo.CardNumber
                                            ));
            }

            return JsonConvert.SerializeObject(lst);
        }
    }
}
