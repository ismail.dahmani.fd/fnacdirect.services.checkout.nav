using FnacDirect.FnacStore.Model;
using FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Common.Shipping.Shop;
using FnacDirect.Technical.Framework.Web.Mvc;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Common.Shipping
{
    public static class FnacTimetableExtensions
    {
        public static IEnumerable<TimetableViewModel> ConvertToTimetableViewModel(this IEnumerable<FnacTimetable> timetables, IApplicationContext applicationContext)
        {
            if (applicationContext != null && timetables != null)
            {
                var openTimetables = timetables.Where(t => t.OpeningPeriods.Any());

                if (openTimetables.Any())
                {
                    var uiResourceService = applicationContext.GetUiResourceService();
                    var locale = applicationContext.GetLocale();
                    var dayOfWeek = (int)DateTime.Today.DayOfWeek;

                    return openTimetables.Select(t => new TimetableViewModel(
                            t.DayOfWeek == dayOfWeek,
                            t.GetDayOfWeek(uiResourceService, locale),
                            t.OpeningPeriods
                        )).ToArray();
                }
            }

            return Enumerable.Empty<TimetableViewModel>();
        }

        //TODO: Refacto possible avec la méthode GetExceptionnalTimetables() de la class ShopShippingNetworkViewModelBuilder
        public static IEnumerable<ExceptionnalClosingViewModel> ConvertToTimetableExceptionalViewModel(this IEnumerable<FnacTimetableExceptionnal> timetables, IApplicationContext applicationContext)
        {
            if (applicationContext != null && timetables != null)
            {
                var culture = CultureInfo.GetCultureInfo(applicationContext.GetLocale());

                var exceptionnalClosing = timetables
                   .Where(t => !t.IsOpened)
                   .Select(t => new ExceptionnalClosingViewModel()
                   {
                       Day = t.Day.ToString(culture.DateTimeFormat.LongDatePattern)
                   });

                var exceptionnalOpening = timetables
                    .Where(t => t.IsOpened)
                    .Select(t => new ExceptionnalOpeningViewModel()
                    {
                        Day = t.Day.ToString(culture.DateTimeFormat.LongDatePattern),
                        Comment = t.Comment,
                        Openings =
                            ExceptionnalOpeningViewModel.GetOpenings(t.OpeningPeriods,
                                applicationContext.GetUiResourceService(), applicationContext.GetLocale())
                    }).ToList();

                return exceptionnalOpening.Union(exceptionnalClosing).ToList();
            }

            return Enumerable.Empty<ExceptionnalClosingViewModel>();
        }
    }

    public static class FnacTimetableExceptionnalExtensions
    {
        public static IEnumerable<TimetableViewModel> ConvertToTimetableViewModel(this IEnumerable<FnacTimetableExceptionnal> timetables, IApplicationContext applicationContext)
        {
            if (applicationContext != null && timetables != null)
            {
                var openTimetables = timetables.Where(t => t.OpeningPeriods.Any());

                if (openTimetables.Any())
                {
                    var uiResourceService = applicationContext.GetUiResourceService();
                    var locale = applicationContext.GetLocale();
                    var today = DateTime.Today;

                    return openTimetables.Select(t => new TimetableViewModel(
                            t.Day.Date == today,
                            t.Day.ToString("ddd d MMMM yyyy"),
                            t.OpeningPeriods
                        )).ToArray();
                }
            }

            return Enumerable.Empty<TimetableViewModel>();
        }
    }
}
