﻿using FnacDirect.OrderPipe.Base.Business.LogisticLineGroups.Initializers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FnacDirect.OrderPipe.Base.Business.LogisticLineGroups.Rules
{
    public class ClickAndMagLogisticLineGroupGenerationRuleDescriptor : BaseFnacLogisticLineGroupGenerationRuleDescriptor<ClickAndMagLogisticLineGroupInitializer>
    {
        public override string SwitchId
        {
            get
            {
                return "orderpipe.logisticlinegroupgenerationrule.ccm";
            }
        }
    }
}
