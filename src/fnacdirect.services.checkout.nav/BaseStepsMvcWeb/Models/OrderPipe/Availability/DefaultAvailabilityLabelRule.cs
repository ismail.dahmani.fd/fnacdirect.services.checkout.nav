using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.OrderPipe.Core.Services;
using FnacDirect.Technical.Framework.Web.Mvc;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;

namespace FnacDirect.OrderPipe.Base.Business.Availability
{
    public class DefaultAvailabilityLabelRule : IAvailabilityLabelRule
    {
        protected readonly IApplicationContext _applicationContext;
        private readonly ISwitchProvider _switchProvider;
        protected readonly DateTimeFormatInfo _dateTimeFormatInfo;
        private string _customRangeDateFormat;
        private readonly bool _hideLongEstimatedDle;
        private bool _isDLEMPEnable;

        private readonly int _dayThresholdForWeekFormat;

        public DefaultAvailabilityLabelRule(IApplicationContext applicationContext, ISwitchProvider switchProvider, IPipeParametersProvider pipeParametersProvider, DateTimeFormatInfo dateTimeFormatInfo, string customRangeDatesFormat)
        {
            _applicationContext = applicationContext;
            _switchProvider = switchProvider;
            _dateTimeFormatInfo = dateTimeFormatInfo;
            _customRangeDateFormat = customRangeDatesFormat;
            _hideLongEstimatedDle = _switchProvider.IsEnabled("front.pop.shipping.hidelongestimateddle");


            _isDLEMPEnable = _switchProvider.IsEnabled("orderpipe.pop.shipping.dlemp.enable");
            _dayThresholdForWeekFormat = _isDLEMPEnable ? pipeParametersProvider.Get("shipping.mp.weekformat.daytreshold", int.MaxValue) : int.MaxValue;
        }
        public virtual EstimatedDeliveryDate ComputeSingleEstimatedDeliveryDate(PopAvailabilityProcessorInput availability)
        {
            var min = availability.DateMin;
            var max = availability.DateMax;

            if (!availability.IsAvailabilityValid())
            {
                return null;
            }

            // préco ou date unique
            if ((availability.Article.IsPreOrder && (min.HasValue || max.HasValue))
                || (!min.HasValue && max.HasValue)
                || (!max.HasValue && min.HasValue))
            {
                var date = min ?? max.Value;

                if (ShouldUseWeekFormat(availability.Yield(), min, max))
                {
                    var weeks = GetWeekCount(date);
                    return new EstimatedDeliveryDate(_applicationContext.GetMessage("orderpipe.pop.shipping.availability.short.week.from", weeks), date.Ticks, availability);
                }

                return new EstimatedDeliveryDate(_applicationContext.GetMessage("orderpipe.pop.shipping.availability.short.from", date.ToString("dd MMMM", _dateTimeFormatInfo)), date.Ticks, availability);
            }

            // fourchette
            if (min.HasValue && max.HasValue && max.Value != min.Value)
            {
                // Affichage en semaine
                if (ShouldUseWeekFormat(availability.Yield(), min, max))
                {
                    var weekMin = GetWeekCount(min.Value);
                    var weekMax = GetWeekCount(max.Value);

                    if (weekMin == weekMax)
                    {
                        return new EstimatedDeliveryDate(_applicationContext.GetMessage("orderpipe.pop.shipping.availability.short.week.from", weekMin), min.Value.Ticks, availability);
                    }
                    return new EstimatedDeliveryDate(_applicationContext.GetMessage("orderpipe.pop.shipping.availability.short.week.between", weekMin, weekMax), min.Value.Ticks, availability);
                }

                // Même jour
                if (min.Value.Date == max.Value.Date && (min.Value.Hour != max.Value.Hour || min.Value.Minute != max.Value.Minute))
                {
                    //Créneau chronopost
                    return new EstimatedDeliveryDate(
                        _applicationContext.GetMessage("orderpipe.pop.shipping.availability.short.sameday.between",
                                                max.Value.ToString(_customRangeDateFormat, _dateTimeFormatInfo),
                                                min.Value.ToString(@"H\hmm", _dateTimeFormatInfo),
                                                max.Value.ToString(@"H\hmm", _dateTimeFormatInfo)),
                        max.Value.Ticks,
                        availability);
                }

                if (availability.ShippingMethodId == Constants.AggregatedShippingMethods.FLY)
                {
                    
                    return new EstimatedDeliveryDate(_applicationContext.GetMessage("orderpipe.pop.shipping.availability.short.from", min.Value.ToString("dd MMMM")),
                        min.Value.Ticks,
                        availability);
                }

                // Même mois
                if (min.Value.Month == max.Value.Month)
                {
                    return new EstimatedDeliveryDate(_applicationContext.GetMessage("orderpipe.pop.shipping.availability.short.between", min.Value.ToString("dd"), max.Value.ToString("dd MMMM", _dateTimeFormatInfo)), min.Value.Ticks, availability);
                }

                return new EstimatedDeliveryDate(_applicationContext.GetMessage("orderpipe.pop.shipping.availability.short.between", min.Value.ToString(_customRangeDateFormat, _dateTimeFormatInfo), max.Value.ToString("dd MMMM", _dateTimeFormatInfo)), min.Value.Ticks, availability);
            }

            // cas même jour donné
            if (max.HasValue && min.HasValue && max.Value == min.Value)
            {
                if (max.Value.Date == DateTime.Today)
                {
                    return new EstimatedDeliveryDate(_applicationContext.GetMessage("orderpipe.pop.shipping.availability.short.today.1921"), max.Value.Date.Ticks, availability);
                }

                if (ShouldUseWeekFormat(availability.Yield(), max))
                {
                    var weeks = GetWeekCount(max.Value);
                    return new EstimatedDeliveryDate(_applicationContext.GetMessage("orderpipe.pop.shipping.availability.short.week.from", weeks), max.Value.Date.Ticks, availability);
                }

                return new EstimatedDeliveryDate(
                    _applicationContext.GetMessage("orderpipe.pop.shipping.availability.short.on",
                        max.Value.ToString("dd", _dateTimeFormatInfo),
                        max.Value.ToString("MMMM", _dateTimeFormatInfo)),
                    max.Value.Ticks,
                    availability);
            }

            return null;
        }

        public virtual string Get(IEnumerable<PopAvailabilityProcessorInput> availabilitiesRaw, bool homogenous, string prefix, bool? hasManyDeliveryDates)
        {
            var availabilities = availabilitiesRaw.Where(a => a.IsAvailabilityValid());

            if (!availabilities.Any())
            {
                return null;
            }

            var preOrders = availabilities.Where(a => a.Article.IsPreOrder);

            // pour les préco avec des dates de début différentes 
            if (homogenous && preOrders.Count() > 1 && preOrders.All(p => p.DateMin.HasValue))
            {
                homogenous = preOrders.All(p => p.DateMin.Value == preOrders.First().DateMin.Value);
            }

            if (homogenous)
            {
                // pour des préco + autres types de DLE 
                homogenous = !preOrders.Any() || preOrders.Count() == availabilities.Count();
            }

            if (homogenous)
            {
                // cas préco unique
                if (preOrders.Count() == 1 && availabilities.Count() == 1)
                {
                    if (!preOrders.First().DateMin.HasValue)
                    {
                        return null;
                    }

                    var preOrderDateMin = preOrders.First().DateMin.Value;

                    if (ShouldUseWeekFormat(availabilities, preOrderDateMin))
                    {
                        var weeks = GetWeekCount(preOrderDateMin);
                        return _applicationContext.GetMessage("orderpipe.pop.shipping.availability.long.week.from", prefix, weeks);
                    }

                    return _applicationContext.GetMessage("orderpipe.pop.shipping.availability.long.from", prefix, preOrderDateMin.ToString("dd MMMM", _dateTimeFormatInfo));
                }

                var dateMin = availabilities.MinOrDefault(a => a.DateMin);
                var dateMax = availabilities.MaxOrDefault(a => a.DateMax);

                // cas fourchette commune
                DateTime? commonDeliveryDayMax = null;
                if (availabilities.All(a => a.DateMax == dateMax))
                {
                    commonDeliveryDayMax = dateMax;
                }

                DateTime? commonDeliveryDayMin = null;
                if (availabilities.All(a => a.DateMin == dateMin))
                {
                    commonDeliveryDayMin = dateMin;
                }
                if (commonDeliveryDayMin.HasValue && _hideLongEstimatedDle)
                {
                    var diff = commonDeliveryDayMin.Value - DateTime.Now;
                    if (diff.TotalHours > 360)
                    {
                        return null;
                    }
                }
                if (commonDeliveryDayMax.HasValue && commonDeliveryDayMin.HasValue && commonDeliveryDayMax.Value != commonDeliveryDayMin.Value)
                {
                    var min = commonDeliveryDayMin.Value;
                    var max = commonDeliveryDayMax.Value;
                    var prefixTwoDate = _applicationContext.GetMessage("orderpipe.pop.shipping.availability.prefix.twodate");
                    if (!string.IsNullOrEmpty(prefixTwoDate))
                    {
                        prefix = prefixTwoDate;
                    }

                    // Affichage en semaine
                    if (ShouldUseWeekFormat(availabilities, min, max))
                    {
                        var weekMin = GetWeekCount(min);
                        var weekMax = GetWeekCount(max);

                        if (weekMin == weekMax)
                        {
                            return _applicationContext.GetMessage("orderpipe.pop.shipping.availability.long.week.from", prefix, weekMin);
                        }
                        return _applicationContext.GetMessage("orderpipe.pop.shipping.availability.long.week.between", prefix, weekMin, weekMax);
                    }

                    // Même jour
                    if (min.Date == max.Date && (min.Hour != max.Hour || min.Minute != max.Minute))
                    {
                        //Créneau chronopost
                        return _applicationContext.GetMessage("orderpipe.pop.shipping.availability.long.sameday.between",
                            prefix,
                            string.Format(min.ToString("dd {0} MMMM", _dateTimeFormatInfo), _applicationContext.GetMessage("orderpipe.pop.shipping.availability.long.betweendayandmonth")),
                            string.Format(min.ToString(@"H\hmm", _dateTimeFormatInfo)),
                            string.Format(max.ToString(@"H\hmm", _dateTimeFormatInfo)));
                    }

                    // Aggregate Fly
                    if (availabilitiesRaw.Any(s => s.ShippingMethodId == Constants.AggregatedShippingMethods.FLY))
                    {
                        return _applicationContext.GetMessage("orderpipe.pop.shipping.availability.short.from", min.ToString("dd MMMM"));
                    }
                    
                    // Même mois
                    if (min.Month == max.Month)
                    {
                        return _applicationContext.GetMessage("orderpipe.pop.shipping.availability.long.between", prefix, min.ToString("dd"), string.Format(max.ToString("dd {0} MMMM", _dateTimeFormatInfo), _applicationContext.GetMessage("orderpipe.pop.shipping.availability.long.betweendayandmonth")));
                    }

                    return _applicationContext.GetMessage("orderpipe.pop.shipping.availability.long.between", prefix, min.ToString("dd MMMM", _dateTimeFormatInfo), max.ToString("dd MMMM", _dateTimeFormatInfo));
                }

                // cas même jour donné
                if (commonDeliveryDayMax.HasValue && commonDeliveryDayMin.HasValue && commonDeliveryDayMax.Value == commonDeliveryDayMin.Value)
                {
                    var date = commonDeliveryDayMin.Value;

                    if (date.Date == DateTime.Today)
                    {
                        if (availabilities.Any(a => a.ShippingMethodId == -1))
                        {
                            return _applicationContext.GetMessage("orderpipe.pop.shipping.availability.short.1hour", prefix);
                        }
                        return _applicationContext.GetMessage("orderpipe.pop.shipping.availability.long.today.1921", prefix);
                    }

                    return _applicationContext.GetMessage("orderpipe.pop.shipping.availability.long.on", prefix, string.Format(date.ToString("dd {0} MMMM", _dateTimeFormatInfo), _applicationContext.GetMessage("orderpipe.pop.shipping.availability.long.betweendayandmonth")));
                }

                // pour les articles avec des fourchette de DLE 
            }

            var todayDateExist = availabilities.ToList().Exists(av => av.DateMin == av.DateMax && av.DateMin == DateTime.Today);

            if (todayDateExist)
            {
                return _applicationContext.GetMessage("orderpipe.pop.shipping.availability.short.today.from.1921", prefix);
            }

            var globalMin = availabilities.MinOrDefault(a => a.DateMin);

            if (!globalMin.HasValue)
            {
                return null;
            }

            // Affichage en semaine
            if (ShouldUseWeekFormat(availabilities, globalMin))
            {
                var weeks = GetWeekCount(globalMin.Value);
                return _applicationContext.GetMessage("orderpipe.pop.shipping.availability.long.week.from", prefix, weeks);
            }

            return _applicationContext.GetMessage("orderpipe.pop.shipping.availability.long.from", prefix, globalMin.Value.ToString("dd MMMM", _dateTimeFormatInfo));
        }

        #region privates

        private bool ShouldUseWeekFormat(IEnumerable<PopAvailabilityProcessorInput> availabilities, DateTime? minDate, DateTime? maxDate = null)
        {
            if (availabilities.Count() == 0
                || !availabilities.Any(a => a != null && a.Article is MarketPlaceArticle))
            {
                return false;
            }

            var value = minDate ?? maxDate ?? DateTime.Today;
            var dayDelay = (value - DateTime.Today).TotalDays;
            return dayDelay >= _dayThresholdForWeekFormat;
        }

        private int GetWeekCount(DateTime date)
        {
            var dayDelay = (date - DateTime.Today).TotalDays;
            var week = Math.Round(dayDelay / 7);
            return Convert.ToInt32(week);
        }

        #endregion
    }
}
