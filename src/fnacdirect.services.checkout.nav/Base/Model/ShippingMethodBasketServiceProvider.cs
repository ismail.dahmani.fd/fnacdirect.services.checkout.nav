﻿using System;
using FnacDirect.OrderPipe.Config;
using FnacDirect.OrderPipe.Base.Business.Shipping.Rules;
using FnacDirect.OrderPipe.Core.Services;

namespace FnacDirect.OrderPipe.Base.Business.Shipping
{
    public class ShippingMethodBasketServiceProvider : IShippingMethodBasketServiceProvider
    {
        private readonly IShippingMethodBasketBuildingRuleFactory _shippingMethodBasketBuildingRuleFactory;

        public ShippingMethodBasketServiceProvider(IShippingMethodBasketBuildingRuleFactory shippingMethodBasketBuildingRuleFactory)
        {
            _shippingMethodBasketBuildingRuleFactory = shippingMethodBasketBuildingRuleFactory;
        }

        public IShippingMethodBasketService Get(IPipeParametersProvider pipeParametersProvider)
        {
            return new ShippingMethodBasketService(_shippingMethodBasketBuildingRuleFactory, pipeParametersProvider);
        }
    }
}
