using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.OrderPipe.Base.Model.LineGroups;
using FnacDirect.OrderPipe.Base.Model.Pop;
using FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Basket;
using FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FnacDirect.OrderPipe.Mvc
{
    public abstract class PopWebViewPage<TModel> : Technical.Framework.Web.Mvc.WebViewPage<TModel>
    {
        public AlertMessageBuilder Alert
        {
            get
            {
                return new AlertMessageBuilder();
            }
        }

        public FooterViewModel Footer
        {
            get
            {
                var opContext = DependencyResolver.Current.GetService<OPContext>();
                var footerViewModelBuilder = DependencyResolver.Current.GetService<IFooterViewModelBuilder>();

                return footerViewModelBuilder.Build(opContext.OF as PopOrderForm);
            }
        }

        public Operations Operations
        {
            get
            {
                return new Operations();
            }
        }

        public Options Options
        {
            get
            {
                return new Options();
            }
        }
    }

    public class Options
    {
        public WrapViewModel Wrap
        {
            get
            {
                var opContext = DependencyResolver.Current.GetService<OPContext>();
                var wrapViewModelBuilder = DependencyResolver.Current.GetService<IWrapViewModelBuilder>();

                return wrapViewModelBuilder.Build(opContext.OF as PopOrderForm);
            }
        }
    }

    public class Operations
    {
        public Christmas Christmas
        {
            get
            {
                return new Christmas();
            }
        }
    }

    public class Christmas
    {
        private readonly Lazy<IEnumerable<StandardArticle>> _articles
            = new Lazy<IEnumerable<StandardArticle>>(() =>
            {
                var opContext = DependencyResolver.Current.GetService<OPContext>();

                if(opContext.OF is PopOrderForm)
                {
                    return (opContext.OF as PopOrderForm).LineGroups.SelectMany(l => l.Articles).OfType<StandardArticle>();
                }

                if (opContext.OF is BasketOF)
                {
                    return (opContext.OF as BasketOF).Articles.OfType<StandardArticle>();
                }

                return Enumerable.Empty<StandardArticle>();
            });

        private readonly Lazy<bool> _splitIsAvailable
            = new Lazy<bool>(() =>
            {
                var opContext = DependencyResolver.Current.GetService<OPContext>();

                var splitData = opContext.OF.GetPrivateData<SplitData>(false); 

                if(splitData != null)
                {
                    return splitData.IsAvailable;
                }

                return false;
            });

        public bool SplitIsAvailable
        {
            get
            {
                return _splitIsAvailable.Value;
            }
        }

        public bool AnyPT
        {
            get
            {
                return _articles.Value.Any(a => a.IsPT);
            }
        }

        public bool AnyStock
        {
            get
            {
                return _articles.Value.Any(a => a.AvailabilityId == (int)AvailabilityEnum.StockPE || a.AvailabilityId == (int)AvailabilityEnum.StockPT);
            }
        }

        public bool AnyAvailable2To4
        {
            get
            {
                return _articles.Value.Any(a => a.AvailabilityId == (int)AvailabilityEnum.From2To4Days
                                             || a.AvailabilityId == (int)AvailabilityEnum.From2To4DaysBis
                                             || a.AvailabilityId == (int)AvailabilityEnum.From2To4DaysPT
                                             || a.AvailabilityId == (int)AvailabilityEnum.From2To4DaysBisPT);
            }
        }

        public bool AnyAvailable4To8
        {
            get
            {
                return _articles.Value.Any(a => a.AvailabilityId == (int)AvailabilityEnum.From4To8Days
                                             || a.AvailabilityId == (int)AvailabilityEnum.From4To8DaysPT);
            }
        }

        public bool AnyAvailable4To12
        {
            get
            {
                return _articles.Value.Any(a => a.AvailabilityId == (int)AvailabilityEnum.From4To12Days
                                             || a.AvailabilityId == (int)AvailabilityEnum.From4To12DaysPT);
            }
        }

        public bool AnyPEStock
        {
            get
            {
                return _articles.Value.Any(a => a.AvailabilityId == (int)AvailabilityEnum.StockPE);
            }
        }

        public bool SplitIsAvailableAndAnyPEAvailable2To4
        {
            get
            {
                return SplitIsAvailable && AnyAvailable2To4;
            }
        }

        public bool SplitIsAvailableAndAnyPEAvailable4To8
        {
            get
            {
                return SplitIsAvailable && AnyAvailable4To8;
            }
        }
    }
}
