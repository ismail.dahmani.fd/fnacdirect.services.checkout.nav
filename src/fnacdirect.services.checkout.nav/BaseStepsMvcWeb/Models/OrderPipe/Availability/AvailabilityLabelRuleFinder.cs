using FnacDirect.OrderPipe.Base.Business.Shipping;
using FnacDirect.OrderPipe.Core.Services;
using FnacDirect.Technical.Framework.Web.Mvc;
using System.Globalization;

namespace FnacDirect.OrderPipe.Base.Business.Availability
{
    public class AvailabilityLabelRuleFinder : IAvailabilityLabelRuleFinder
    {
        private readonly IAppointmentDeliveryService _appointmentDeliveryService;
        private readonly IApplicationContext _applicationContext;
        private readonly DateTimeFormatInfo _dateTimeFormatInfo;

        private readonly ISwitchProvider _switchProvider;
        private readonly IPipeParametersProvider _pipeParametersProvider;

        private int? _shippingMethodIdSteedFromStore;

        private string _availabilityCustomDateFormat;

        public AvailabilityLabelRuleFinder(IApplicationContext applicationContext, IPipeParametersProvider pipeParametersProvider, ISwitchProvider switchProvider, IAppointmentDeliveryService appointmentDeliveryService)
        {
            _appointmentDeliveryService = appointmentDeliveryService;
            _applicationContext = applicationContext;
            _switchProvider = switchProvider;
            var locale = applicationContext.GetLocale();
            var culture = CultureInfo.GetCultureInfo(locale);

            _dateTimeFormatInfo = culture.DateTimeFormat;

            _pipeParametersProvider = pipeParametersProvider;


        }

        public IAvailabilityLabelRule Find(int? shippingMethodId)
        {
            if (_shippingMethodIdSteedFromStore == null)
                _shippingMethodIdSteedFromStore = _pipeParametersProvider.Get<int>("shippingmethodid.steedfromstore");
            if (_availabilityCustomDateFormat == null)
                _availabilityCustomDateFormat = _pipeParametersProvider.Get("shipping.date.availability.customformat", "dd MMMM");

            if (shippingMethodId.GetValueOrDefault() == _shippingMethodIdSteedFromStore)
            {
                return new SteedFromStoreAvailabilityLabelRule(_applicationContext, _switchProvider, _dateTimeFormatInfo, _availabilityCustomDateFormat, _pipeParametersProvider);
            }

            if (_appointmentDeliveryService.AggregatedShippingMethodIds.Contains(shippingMethodId.GetValueOrDefault()))
            {
                return new AppointmentDeliveryAvailabilityLabelRule(_applicationContext, _switchProvider, _pipeParametersProvider, _dateTimeFormatInfo, _availabilityCustomDateFormat);
            }

            return new DefaultAvailabilityLabelRule(_applicationContext, _switchProvider, _pipeParametersProvider, _dateTimeFormatInfo, _availabilityCustomDateFormat);
        }
    }
}
