using System;

namespace FnacDirect.OrderPipe.Base.BaseModel.Model
{
    [Serializable]
    public class TrackingBillingMethod
    {
        private decimal _amount;
        public decimal Amount
        {
            get { return _amount; }
            set { _amount = value; }
        }

        private int _billingMethodId;

        public int BillingMethodId
        {
            get { return _billingMethodId; }
            set { _billingMethodId = value; }
        }

        private string _label;

        public string Label
        {
            get { return _label; }
            set { _label = value; }
        }
	
    }
}
