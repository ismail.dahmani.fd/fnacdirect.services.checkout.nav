using FnacDirect.OrderPipe.Base.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using DTO = FnacDirect.Contracts.Online.Model;

namespace FnacDirect.OrderPipe.Base.Business
{
    public class ArticleService : IArticleService
    {
        private const string DartyServiceDescriptorName = "darty";

        public ArticleService()
        {
        }

        public IEnumerable<DTO.Article> FilterArticles(IEnumerable<DTO.Article> articles, ArticleFilter filter)
        {
            return articles.Where(a => !IsArticleFiltered(a, filter));
        }

        public IEnumerable<DTO.Article> GetFilteredArticles(IEnumerable<DTO.Article> articles, ArticleFilter filter)
        {
            return articles.Where(a => IsArticleFiltered(a, filter));
        }

        public bool IsFlyService(DTO.Article service)
        { 
            return DartyServiceDescriptorName.Equals(service?.Core?.ServiceDescriptorName, StringComparison.InvariantCultureIgnoreCase);
        }

        public bool IsFlyService(StandardArticle service)
        {
            return DartyServiceDescriptorName.Equals(service?.ServiceDescriptorName, StringComparison.InvariantCultureIgnoreCase);
        }

        #region privates

        private bool IsArticleFiltered(DTO.Article article, ArticleFilter filter)
        {
            if (filter.HasFlag(ArticleFilter.FLY)
                && IsFlyService(article))
            {
                return true;
            }

            return false;
        }

        #endregion
    }
}
