namespace FnacDirect.OrderPipe.Base.Model
{
    public class OgoneAliasGatewayOutputData : GroupData
    {
        public string Alias { get; set; }
        public string CardHolder { get; set; }
        public string CardNumber { get; set; }
        public string Brand { get; set; }
        public string CardExpirationDate { get; set; }
        public string Crypto { get; set; }
        public bool SaveWalet { get; set; }
        public string OgoneResult { get; set; }

        public string ChosenBrandRaw { get; set; }
        public string ChosenBrand => !string.IsNullOrEmpty(ChosenBrandRaw) ? ChosenBrandRaw : Brand;

        public bool IsEmpty()
        {
            return string.IsNullOrEmpty(OgoneResult);
        }
    }
}
