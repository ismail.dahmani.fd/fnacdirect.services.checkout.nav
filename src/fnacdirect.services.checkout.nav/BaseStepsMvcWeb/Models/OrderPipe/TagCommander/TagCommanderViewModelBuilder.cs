using FnacDirect.Contracts.Online.Model;
using FnacDirect.Front.WebBusiness;
using FnacDirect.OrderPipe.Base.BaseModel.Model;
using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.Technical.Framework.Web;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.TagCommander
{
    public class TagCommanderViewModelBuilder : ITagCommanderViewModelBuilder
    {
        public TagCommanderThankYouViewModel BuildThankYou(PopOrderForm popOrderForm, OPContext opContext)
        {
            var trackingData = popOrderForm.GetPrivateData<TrackingData>();

            var trackingOrder = trackingData.Value;

            var viewModel = new TagCommanderThankYouViewModel()
            {
                PageName = GetPageName(opContext.CurrentStep.Name, opContext.Pipe),
                TemplateName = GetTemplateName(opContext.CurrentStep.Name, opContext.Pipe),

                CartId = opContext.SID,

                OrderZip = GetOrderZip(trackingOrder, popOrderForm),
                OrderState = GetOrderState(trackingOrder, popOrderForm),
                OrderTax = GetOrderTax(trackingOrder, popOrderForm),
                OrderAmountAti = GetOrderAmountAti(trackingOrder, popOrderForm),
                OrderAmountWt = GetOrderAmountWt(trackingOrder, popOrderForm),
                OrderAmountAtiWithSf = GetOrderAmountAtiWithSf(trackingOrder, popOrderForm),
                OrderAmountTfWithSf = GetOrderAmountTfWithSf(trackingOrder, popOrderForm),
                OrderCurrency = "EUR",
                OrderShipTf = GetOrderShipTf(trackingOrder, popOrderForm),
                OrderShipAti = GetOrderShipAti(trackingOrder, popOrderForm),
                OrderShippingMethod = GetOrderShippingMethod(trackingOrder),
                OrderNewClient = popOrderForm.UserContextInformations.IsNewCustomer ? "yes" : "no",
                OrderAmount = GetOrderAmount(trackingOrder, popOrderForm).ToString("F", CultureInfo.InvariantCulture),
                OrderPromoCode = GetOrderPromoCode(trackingOrder, popOrderForm),
                OrderNbProducts = GetOrderNbProducts(trackingOrder, popOrderForm).ToString(),
                OrderPayMethod = GetOrderPayMethod(trackingOrder),
                OrderId = trackingOrder.OrderUserReference,
                OrderUserRefs = trackingOrder.LgOrderUserReferences,
                Products = BuildProducts(trackingOrder, popOrderForm).ToList()
            };

            return viewModel;
        }

        public TagCommanderOrderViewModel Build(PopOrderForm popOrderForm, OPContext opContext)
        {
            var trackingData = popOrderForm.GetPrivateData<TrackingData>();
            var trackingOrder = trackingData.Value;

            var viewModel = new TagCommanderOrderViewModel()
            {
                PageName = GetPageName(opContext.CurrentStep.Name, opContext.Pipe),
                TemplateName = GetTemplateName(opContext.CurrentStep.Name, opContext.Pipe),

                CartId = opContext.SID,

                OrderZip = GetOrderZip(trackingOrder, popOrderForm),
                OrderState = GetOrderState(trackingOrder, popOrderForm),
                OrderTax = GetOrderTax(trackingOrder, popOrderForm),
                OrderAmountAti = GetOrderAmountAti(trackingOrder, popOrderForm),
                OrderAmountWt = GetOrderAmountWt(trackingOrder, popOrderForm),
                OrderAmountAtiWithSf = GetOrderAmountAtiWithSf(trackingOrder, popOrderForm),
                OrderAmountTfWithSf = GetOrderAmountTfWithSf(trackingOrder, popOrderForm),
                OrderCurrency = "EUR",
                OrderShipTf = GetOrderShipTf(trackingOrder, popOrderForm),
                OrderShipAti = GetOrderShipAti(trackingOrder, popOrderForm),
                OrderShippingMethod = GetOrderShippingMethod(trackingOrder),
                OrderNewClient = popOrderForm.UserContextInformations.IsNewCustomer ? "yes" : "no",
                OrderAmount = GetOrderAmount(trackingOrder, popOrderForm).ToString("F", CultureInfo.InvariantCulture),
                OrderPromoCode = GetOrderPromoCode(trackingOrder, popOrderForm),
                OrderNbProducts = GetOrderNbProducts(trackingOrder, popOrderForm).ToString(),

                Products = BuildProducts(trackingOrder, popOrderForm).ToList()
            };

            return viewModel;
        }

        private string GetTemplateName(string currentStep, IOP pipe)
        {
            switch (currentStep.ToLowerInvariant())
            {
                case "displaybasket":
                    return pipe.GlobalParameters.Get("tracking.tagcommander.displaybasket.templatename", "");
                case "displaylogin":
                    return pipe.GlobalParameters.Get("tracking.tagcommander.displaylogin.templatename", "");
                case "displayshippingaddress":
                    return  pipe.GlobalParameters.Get("tracking.tagcommander.displayshippingaddress.templatename", "");
                case "displaypayment":
                    return pipe.GlobalParameters.Get("tracking.tagcommander.displaypreview.templatename", "");
                case "thankyou":
                    return pipe.GlobalParameters.Get("tracking.tagcommander.orderthankyou.templatename", "");
                case "displayonepage":
                    return pipe.GlobalParameters.Get("tracking.tagcommander.onepage.templatename", "");
            }

            return string.Empty;
        }

        private string GetPageName(string currentStep, IOP pipe)
        {
            switch (currentStep.ToLowerInvariant())
            {
                case "displaybasket":
                    return pipe.GlobalParameters.Get("tracking.tagcommander.displaybasket.pagename", "");
                case "displaylogin":
                    return pipe.GlobalParameters.Get("tracking.tagcommander.displaylogin.pagename", "");
                case "displayshippingaddress":
                    return pipe.GlobalParameters.Get("tracking.tagcommander.displayshippingaddress.pagename", "");
                case "displaypayment":
                    return pipe.GlobalParameters.Get("tracking.tagcommander.displaypreview.pagename", "");
                case "thankyou":
                    return pipe.GlobalParameters.Get("tracking.tagcommander.orderthankyou.pagename", "");
                case "displayonepage":
                    return pipe.GlobalParameters.Get("tracking.tagcommander.onepage.pagename", "");
            }

            return string.Empty;
        }

        private IEnumerable<TagCommanderProductViewModel> BuildProducts(TrackingOrder trackingOrder, PopOrderForm popOrderForm)
        {
            if (trackingOrder != null && trackingOrder.Products != null)
            {
                foreach (var product in trackingOrder.Products)
                {
                    if (product.ProductId <= 0 || product.ReferentialId <= 0)
                    {
                        continue;
                    }

                    var articleReference = new ArticleReference(product.ProductId, null, null, null, (ArticleCatalog)product.ReferentialId);

                    var w3article = ArticleFactoryNew.GetW3Article(articleReference, true, false);

                    if (w3article != null)
                    {
                        var productViewModel = new TagCommanderProductViewModel
                        {
                            Id = product.ProductId.ToString(),
                            Ref = product.ReferentialId.ToString()
                        };
                        productViewModel.RefId = productViewModel.Ref + "-" + productViewModel.Id;
                        productViewModel.Name = product.Title;
                        productViewModel.Quantity = product.Quantity.ToString();
                        productViewModel.UnitPriceAti = product.PriceUserEur.ToString(CultureInfo.InvariantCulture);
                        productViewModel.UnitPriceTf = product.PriceNoVATEur.ToString(CultureInfo.InvariantCulture);
                        productViewModel.Trademark = product.TradeMark;
                        productViewModel.AvailabiltyLabel = product.AvailabilityLabel;
                        productViewModel.UrlPage = w3article.PageUrl.ToString();
                        productViewModel.Rating = w3article.LiteralAverageRating;
                        productViewModel.UrlPicture = w3article.Literal_110x110_ImageUrl;
                        productViewModel.CategoryLabel = w3article.DTO.Core.Type.Label;
                        productViewModel.CategoryId = product.TypeId.ToString();

                        if (FnacContext.Current.SiteContext.MarketId == (int)FnacMarket.Portugal)
                        {
                            productViewModel.ProductItemType = product.TypeId.ToString();
                            productViewModel.ProductEAN = w3article.EAN;
                            productViewModel.ProductPartNumber = w3article.ArticleReference.ExternalReference;
                        }
                        yield return productViewModel;
                    }
                }
            }
            else if (popOrderForm.LineGroups.Any())
            {
                foreach (var lineGroup in popOrderForm.LineGroups)
                {
                    foreach (var article in lineGroup.Articles)
                    {
                        if (article is Base.Model.VirtualGiftCheck)
                        {
                            continue;
                        }

                        var productViewModel = new TagCommanderProductViewModel();
                        var referenceArt = new ArticleReference(article.ProductID, null, null, null, (ArticleCatalog)article.Referentiel);
                        var w3article = ArticleFactoryNew.GetW3Article(referenceArt, true, false);


                        if (article is StandardArticle standardArticle)
                        {
                            productViewModel.Id = standardArticle.ProductID.Value.ToString();
                            productViewModel.Ref = standardArticle.Referentiel.ToString();
                            productViewModel.RefId = productViewModel.Ref + "-" + productViewModel.Id;
                            productViewModel.Name = standardArticle.Label;
                            productViewModel.Quantity = standardArticle.Quantity.GetValueOrDefault(1).ToString();
                            productViewModel.UnitPriceAti = (standardArticle.PriceUserEur.GetValueOrDefault(0) + standardArticle.EcoTaxEur.GetValueOrDefault(0)).ToString(CultureInfo.InvariantCulture);
                            productViewModel.UnitPriceTf = (standardArticle.PriceNoVATEur.GetValueOrDefault(0) + standardArticle.EcoTaxEur.GetValueOrDefault(0)).ToString(CultureInfo.InvariantCulture);
                            productViewModel.Trademark = standardArticle.Editor;
                            productViewModel.AvailabiltyLabel = standardArticle.AvailabilityLabel;
                            productViewModel.CategoryId = standardArticle.TypeId.GetValueOrDefault().ToString();
                            productViewModel.CategoryLabel = standardArticle.TypeLabel;

                            if (w3article != null)
                            {
                                productViewModel.UrlPage = w3article.PageUrl?.ToString();
                                productViewModel.Rating = w3article.LiteralAverageRating;
                                productViewModel.UrlPicture = w3article.Literal_110x110_ImageUrl;
                                productViewModel.CategoryLabel = w3article.DTO.Core.Type.Label;

                                if (FnacContext.Current.SiteContext.MarketId == (int)FnacMarket.Portugal)
                                {
                                    productViewModel.ProductItemType = standardArticle.TypeId.ToString();
                                    productViewModel.ProductEAN = w3article.EAN;
                                    productViewModel.ProductPartNumber = w3article.ArticleReference.ExternalReference;
                                }
                            }
                        }


                        if (article is MarketPlaceArticle marketPlaceArticle)
                        {
                            productViewModel.Id = marketPlaceArticle.ProductID.Value.ToString();
                            productViewModel.Ref = marketPlaceArticle.Referentiel.ToString();
                            productViewModel.RefId = productViewModel.Ref + "-" + productViewModel.Id;
                            productViewModel.Name = marketPlaceArticle.Label;
                            productViewModel.Quantity = marketPlaceArticle.Quantity.GetValueOrDefault(1).ToString();
                            productViewModel.UnitPriceAti = marketPlaceArticle.PriceUserEur.GetValueOrDefault(0).ToString(CultureInfo.InvariantCulture);
                            productViewModel.UnitPriceTf = marketPlaceArticle.PriceNoVATEur.GetValueOrDefault(0).ToString(CultureInfo.InvariantCulture);
                            productViewModel.Trademark = marketPlaceArticle.Editor;
                            productViewModel.AvailabiltyLabel = marketPlaceArticle.AvailabilityLabel;
                            productViewModel.CategoryId = marketPlaceArticle.TypeId.GetValueOrDefault().ToString();
                            productViewModel.CategoryLabel = marketPlaceArticle.TypeLabel;
                            productViewModel.OfferId = marketPlaceArticle.Offer.Reference;
                            if (w3article != null)
                            {
                                productViewModel.UrlPage = w3article.PageUrl?.ToString();
                                productViewModel.Rating = w3article.LiteralAverageRating;
                                productViewModel.UrlPicture = w3article.Literal_110x110_ImageUrl;
                                productViewModel.CategoryLabel = w3article.DTO.Core.Type.Label;
                                if (FnacContext.Current.SiteContext.MarketId == (int)FnacMarket.Portugal)
                                {
                                    productViewModel.ProductItemType = marketPlaceArticle.TypeId.ToString();
                                    productViewModel.ProductEAN = w3article.EAN;
                                    productViewModel.ProductPartNumber = w3article.ArticleReference.ExternalReference;
                                }
                            }
                        }

                        yield return productViewModel;
                    }
                }
            }
        }

        private string GetOrderZip(TrackingOrder trackingOrder, PopOrderForm popOrderForm)
        {
            var orderZip = string.Empty;

            if (trackingOrder != null)
            {
                orderZip = trackingOrder.OrderBillingZip;
            }
            else if (popOrderForm.BillingAddress != null)
            {
                orderZip = popOrderForm.BillingAddress.ZipCode;
            }

            return orderZip;
        }

        private string GetOrderState(TrackingOrder trackingOrder, PopOrderForm popOrderForm)
        {
            var orderState = string.Empty;

            if (trackingOrder != null)
            {
                orderState = trackingOrder.OrderBillingState;
            }
            else if (popOrderForm.BillingAddress != null)
            {
                orderState = popOrderForm.BillingAddress.City;
            }

            return orderState;
        }

        private string GetOrderTax(TrackingOrder trackingOrder, PopOrderForm popOrderForm)
        {
            decimal orderVAT = 0;
            if (trackingOrder != null)
            {
                orderVAT = trackingOrder.TotalPrice - trackingOrder.TotalPriceWithoutTVA;
            }
            else
            {
                orderVAT = popOrderForm.GlobalPrices.TotalPriceEur.GetValueOrDefault() - (popOrderForm.GlobalPrices.TotalShoppingCartBusinessPriceNoVatEur.GetValueOrDefault() + popOrderForm.GlobalPrices.TotalShipPriceUserEurNoVAT.GetValueOrDefault() + popOrderForm.GlobalPrices.TotalEcoTaxEurNoVAT.GetValueOrDefault());
            }
            return orderVAT.ToString("F", CultureInfo.InvariantCulture);
        }

        private string GetOrderAmountAti(TrackingOrder trackingOrder, PopOrderForm popOrderForm)
        {
            decimal iBasketAmount = 0;

            if (trackingOrder != null)
            {
                iBasketAmount = trackingOrder.TotalBasketPrice;
            }
            else
            {
                iBasketAmount = popOrderForm.GlobalPrices.TotalShoppingCartBusinessPriceUserEur.GetValueOrDefault() + popOrderForm.GlobalPrices.TotalEcoTaxEur.GetValueOrDefault();
            }

            return iBasketAmount.ToString("F", CultureInfo.InvariantCulture);
        }

        private string GetOrderAmountWt(TrackingOrder trackingOrder, PopOrderForm popOrderForm)
        {
            decimal iBasketAmountNoVAT = 0;
            if (trackingOrder != null)
            {
                iBasketAmountNoVAT = trackingOrder.TotalBasketPriceNoVAT;
            }
            else
            {
                iBasketAmountNoVAT = popOrderForm.GlobalPrices.TotalShoppingCartBusinessPriceNoVatEur.GetValueOrDefault() + +popOrderForm.GlobalPrices.TotalEcoTaxEurNoVAT.GetValueOrDefault();
            }
            return iBasketAmountNoVAT.ToString("F", CultureInfo.InvariantCulture);
        }

        private string GetOrderAmountAtiWithSf(TrackingOrder trackingOrder, PopOrderForm popOrderForm)
        {
            decimal totalPrice;
            if (trackingOrder != null)
            {
                totalPrice = trackingOrder.TotalPrice;
            }
            else
            {
                totalPrice = popOrderForm.GlobalPrices.TotalPriceEur.GetValueOrDefault();

            }
            return totalPrice.ToString("F", CultureInfo.InvariantCulture);
        }

        private string GetOrderAmountTfWithSf(TrackingOrder trackingOrder, PopOrderForm popOrderForm)
        {
            decimal totalPriceNoVAT = 0;
            if (trackingOrder != null)
            {
                totalPriceNoVAT = trackingOrder.TotalPriceWithoutTVA;
            }
            else
            {
                totalPriceNoVAT = popOrderForm.GlobalPrices.TotalShoppingCartBusinessPriceNoVatEur.GetValueOrDefault() + popOrderForm.GlobalPrices.TotalShipPriceUserEurNoVAT.GetValueOrDefault() + popOrderForm.GlobalPrices.TotalEcoTaxEurNoVAT.GetValueOrDefault();
            }
            return totalPriceNoVAT.ToString("F", CultureInfo.InvariantCulture);
        }

        private string GetOrderShipTf(TrackingOrder trackingOrder, PopOrderForm popOrderForm)
        {
            decimal orderShippingPriceNoVAT = 0;
            if (trackingOrder != null)
            {
                orderShippingPriceNoVAT = trackingOrder.TotalShippingPriceNoVAT;
            }
            else
            {
                orderShippingPriceNoVAT = popOrderForm.GlobalPrices.TotalShipPriceUserEurNoVAT.GetValueOrDefault();
            }
            return orderShippingPriceNoVAT.ToString("F", CultureInfo.InvariantCulture);
        }

        private string GetOrderShipAti(TrackingOrder trackingOrder, PopOrderForm popOrderForm)
        {
            decimal orderShippingPrice = 0;
            if (trackingOrder != null)
            {
                orderShippingPrice = trackingOrder.TotalShippingPrice;
            }
            else
            {
                orderShippingPrice = popOrderForm.GlobalPrices.TotalShipPriceUserEur.GetValueOrDefault();
            }
            return orderShippingPrice.ToString("F", CultureInfo.InvariantCulture);
        }

        private string GetOrderShippingMethod(TrackingOrder trackingOrder)
        {
            if (trackingOrder != null)
            {
                return trackingOrder.OrderShippingMethods;
            }

            return string.Empty;
        }

        private decimal GetOrderAmount(TrackingOrder trackingOrder, PopOrderForm popOrderForm)
        {
            decimal totalPrice;
            if (trackingOrder != null)
            {
                totalPrice = trackingOrder.TotalPrice;
            }
            else
            {
                totalPrice = popOrderForm.GlobalPrices.TotalPriceEur.GetValueOrDefault();

            }
            return totalPrice;
        }

        private string GetOrderPromoCode(TrackingOrder trackingOrder, PopOrderForm popOrderForm)
        {
            if (trackingOrder != null)
            {
                return trackingOrder.PromotionCode;
            }
            else
            {
                return popOrderForm.OrderGlobalInformations.AdvantageCode;
            }
        }

        private int GetOrderNbProducts(TrackingOrder trackingOrder, PopOrderForm popOrderForm)
        {
            var iNbProduct = 0;
            if (trackingOrder != null)
            {
                iNbProduct = trackingOrder.TotalQuantity;
            }
            else if (popOrderForm.LineGroups != null)
            {
                iNbProduct = popOrderForm.LineGroups.SelectMany(l => l.Articles).Count();
            }
            return iNbProduct;
        }

        private string GetOrderPayMethod(TrackingOrder trackingOrder)
        {
            var orderPayMethod = new StringBuilder();

            if (trackingOrder != null && trackingOrder.BillingMethods != null)
            {
                foreach (var b in trackingOrder.BillingMethods)
                {
                    orderPayMethod.AppendFormat("{0};", b.Label);
                }
            }
            return orderPayMethod.ToString();
        }
    }
}
