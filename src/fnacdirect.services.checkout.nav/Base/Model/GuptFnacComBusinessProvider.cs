using Common.Logging;
using FnacDirect.OrderPipe.Core.Services;
using FnacDirect.Technical.Framework.Utils;
using System.Collections.Generic;
using System.Configuration;

namespace FnacDirect.OrderPipe.Base.Business
{
    public class GuptFnacComBusinessProvider : IGuptFnacComBusinessProvider
    {
        private readonly ILog _logger;
        private readonly ISwitchProvider _switchProvider;
        private IPipeParametersProvider _pipeParametersProvider;
        private List<string> _shopsNotSubjectD3E;

        public GuptFnacComBusinessProvider(ISwitchProvider switchProvider, IPipeParametersProvider pipeParametersProvider, ILog log)
        {
            _logger = log;
            _switchProvider = switchProvider;
            _pipeParametersProvider = pipeParametersProvider;
            _shopsNotSubjectD3E = StringUtils.ConvertParameterToList<string>(ConfigurationManager.AppSettings["MagasinsNotSubjectToD3E"]);
        }

        public IGuptFnacComBusiness GetGuptFnacComBusiness(RangeSet<int> tvArticleTypesId)
        {
            if (_switchProvider.IsEnabled("orderpipe.pop.clickandcollect.terradata"))
            {
                return new ClickAndCollectTerraDataBusiness(_pipeParametersProvider, _logger);
            }

            return new GuptFnacComBusiness(tvArticleTypesId, _shopsNotSubjectD3E);
        }
    }
}
