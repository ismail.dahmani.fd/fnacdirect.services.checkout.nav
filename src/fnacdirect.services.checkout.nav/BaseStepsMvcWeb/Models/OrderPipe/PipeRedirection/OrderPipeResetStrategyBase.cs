using FnacDirect.OrderPipe.Config;

namespace FnacDirect.OrderPipe.CoreRouting
{
    public abstract class OrderPipeResetStrategyBase<TDescriptor>
         : IOrderPipeResetStrategy<TDescriptor>
         where TDescriptor : class, IUiPipeDescriptor
    {
        public abstract string DoResetFor(OPContext opContext, bool applyRedirect);
    }
}
