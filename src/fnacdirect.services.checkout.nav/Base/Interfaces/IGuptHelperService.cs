using FnacDirect.OrderPipe.Base.LineGroups;
using FnacDirect.OrderPipe.Base.Model;
using System.Collections.Generic;


namespace FnacDirect.OrderPipe.Base.Business.FDV
{
    /// <summary>
    /// Boîte à outils pour la GUPT.
    /// </summary>
    public interface IGuptHelperService
    {
        /// <summary>Limite du nombre d'articles (SKUs) que l'on peut envoyer à la GUPT.</summary>
        int GetGuptItemsLimit();

        /// <summary>Flux de vente intra mag gérés par la GUPT.</summary>
        IEnumerable<SellerType> GetGuptManagedSellerTypes();

        /// <summary>Analyse les articles donnés et établit un rapport d'analyse (viabilité du panier, moyens de paiement disponibles, etc.).</summary>
        /// <param name="articles">Les articles à analyser</param>
        GuptHelperResult Process(IEnumerable<StandardArticle> articles);
    }
}
