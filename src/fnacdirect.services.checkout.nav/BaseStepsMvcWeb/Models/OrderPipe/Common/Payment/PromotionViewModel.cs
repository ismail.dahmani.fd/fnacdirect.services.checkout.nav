
namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Common.Payment
{
    public class PromotionViewModel
    {
        public string Code { get; set; }

        public string Description { get; set; }

        public string ErrorMessage { get; set; }

    }
}
