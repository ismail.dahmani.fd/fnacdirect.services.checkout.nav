using FnacDirect.Customer.Model.SelfCheckout;
using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.OrderPipe.Base.Model.SelfCheckout;

namespace FnacDirect.OrderPipe.Base.Business.Helpers
{
    public static class SelfCheckoutErrorHelper
    {
        public static void RaiseError(IOF orderForm, SelfCheckoutStatusCodeEnum errorCode,
            Constants.Error.ErrorType errorType, string errorMessage)
        {
            var selfCheckoutData = orderForm.GetPrivateData<SelfCheckoutData>(create: false);
            if (selfCheckoutData != null)
            {
                selfCheckoutData.DisplaySelfCheckoutValidated = false;
            }

            var errorData = orderForm.GetPrivateData<ErrorData>(create: true);
            if (errorData != null)
            {
                errorData.ErrorCode = (int)errorCode;
                errorData.ErrorType = errorType;
                errorData.ErrorMessage = errorMessage;

                errorData.HttpStatusCode = System.Net.HttpStatusCode.Unauthorized;
            }
        }
    }
}
