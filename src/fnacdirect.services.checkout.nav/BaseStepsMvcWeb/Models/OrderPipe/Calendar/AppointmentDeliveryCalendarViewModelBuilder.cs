using FnacDirect.OrderPipe.Base.BaseModel;
using FnacDirect.OrderPipe.Base.Business.Configuration;
using FnacDirect.OrderPipe.Base.Business.Shipping;
using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.OrderPipe.Base.Model.Facets;
using FnacDirect.OrderPipe.Base.Model.Solex;
using FnacDirect.OrderPipe.BaseMvc.Web.Pop.Models.Common;
using System;
using System.Collections.Generic;
using System.Linq;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Solex.Appointment
{
    public class AppointmentDeliveryCalendarViewModelBuilder : IAppointmentDeliveryCalendarViewModelBuilder
    {
        private readonly ISolexConfigurationProvider _solexConfigurationProvider;
        private readonly IAppointmentDeliveryCalendarBusiness _appointmentDeliveryBusiness;
        private readonly IPipeMessageViewModelBuilder _pipeMessageViewModelBuilder;
        private readonly IAppointmentDeliveryService _appointmentDeliveryService;

        public AppointmentDeliveryCalendarViewModelBuilder(IAppointmentDeliveryCalendarBusiness appointmentDeliveryBusiness,
                                        ISolexConfigurationProvider solexConfigurationProvider,
                                        IPipeMessageViewModelBuilder pipeMessageViewModelBuilder,
                                        IAppointmentDeliveryService appointmentDeliveryService)
        {
            _appointmentDeliveryBusiness = appointmentDeliveryBusiness;
            _solexConfigurationProvider = solexConfigurationProvider;
            _pipeMessageViewModelBuilder = pipeMessageViewModelBuilder;
            _appointmentDeliveryService = appointmentDeliveryService;
        }

        public AppointmentDeliveryCalendarViewModel BuildAppointmentDeliveryCalendarViewModel(int currentPageIndex, DateTime startDate, DateTime endDate, List<DeliverySlot>[] sortedList, List<Article> articles, DeliverySlotData deliverySlotData)
        {
            return new AppointmentDeliveryCalendarViewModel
            {
                CurrentPageIndex = currentPageIndex,
                Slots = sortedList,
                Articles = (from standartArticle in articles.OfType<StandardArticle>()
                            select standartArticle.Label).ToList(),
                HasPreviousPage = currentPageIndex != 1,
                StartDate = startDate.ToShortDateString(),
                EndDate = endDate.ToShortDateString(),
                HasSelectedSlot = deliverySlotData.HasSelectedSlot,
                SelectdSlotTitle = deliverySlotData.HasSelectedSlot ? $"{deliverySlotData.DeliverySlotSelected.StartDate.ToString("dddd dd.MM.yy - HH")}H-{deliverySlotData.DeliverySlotSelected.EndDate.ToString("HH")}H" : string.Empty
            };
        }

        public AppointmentDeliveryCalendarViewModelBuilderResult BuildAppointmentDeliveryCalendarViewModel(PopOrderForm popOrderForm, int sellerId, HashSet<int> shippingMethods, int pageIndex, int pageSize, string sortingCriteria)
        {
            var articles = popOrderForm.LineGroups.SelectMany(l => l.Articles).ToList();
            return BuildAppointmentDeliveryCalendarViewModel(popOrderForm, sellerId, articles, shippingMethods, pageIndex, pageSize, sortingCriteria);
        }

        public AppointmentDeliveryCalendarViewModelBuilderResult BuildAppointmentDeliveryCalendarViewModel(PopOrderForm popOrderForm, int sellerId, IEnumerable<Article> articles, HashSet<int> shippingMethods, int pageIndex, int pageSize, string sortingCriteria)
        {
            var postalShippingMethodEvaluationItemResult = GetPostalShippingMethodEvaluationItem(popOrderForm, sellerId);
            if (!postalShippingMethodEvaluationItemResult.IsSuccess)
            {
                return new AppointmentDeliveryCalendarViewModelBuilderResult()
                {
                    IsSuccess = false,
                    ErrorMessage = postalShippingMethodEvaluationItemResult.ErrorMessage
                };
            }

            var solexConfiguration = _solexConfigurationProvider.Get();
            var endDateMaximum = DateTime.Today.AddDays(solexConfiguration.EndDateValue);
            var deliverySlotData = GetDeliverySlotData(popOrderForm);
            var choices = postalShippingMethodEvaluationItemResult.Item.Choices;
            var shippingAddress = postalShippingMethodEvaluationItemResult.Item.ShippingAddress;

            var appointmentDeliverySortedSlotsData = _appointmentDeliveryBusiness.GetSortedSlotsFromChoices(
                choices, shippingAddress, popOrderForm, articles, shippingMethods, deliverySlotData,
                pageIndex, pageSize, sortingCriteria, endDateMaximum);

            if (appointmentDeliverySortedSlotsData.HasError)
            {
                return new AppointmentDeliveryCalendarViewModelBuilderResult()
                {
                    IsSuccess = false,
                    ErrorMessage = appointmentDeliverySortedSlotsData.ErrorMessage,
                    SlotsData = appointmentDeliverySortedSlotsData
                };
            }

            var appointmentDeliveryCalendarViewModel = BuildAppointmentDeliveryCalendarViewModel(pageIndex, appointmentDeliverySortedSlotsData.MinStartDate, appointmentDeliverySortedSlotsData.MaxEndDate, appointmentDeliverySortedSlotsData.SortedSlots, appointmentDeliverySortedSlotsData.EligibleArticles, deliverySlotData);

            SetPipeMessages(appointmentDeliveryCalendarViewModel, deliverySlotData);

            var isFly = shippingMethods.Contains(Constants.ShippingMethods.Fly2Hours)
                        || shippingMethods.Contains(Constants.ShippingMethods.Fly5Hours);
            // We adjust maximum end date for FLY to avoid having an extra, bugged page in the calendar
            var adjustedEndDateMaximum = isFly
                ? endDateMaximum.AddDays(-1)
                : endDateMaximum;

            SetNextPage(appointmentDeliveryCalendarViewModel, appointmentDeliverySortedSlotsData, pageIndex, pageSize, adjustedEndDateMaximum);

            return new AppointmentDeliveryCalendarViewModelBuilderResult()
            {
                IsSuccess = true,
                SlotsData = appointmentDeliverySortedSlotsData,
                ViewModel = appointmentDeliveryCalendarViewModel
            };
        }

        #region privates

        private DeliverySlotData GetDeliverySlotData(PopOrderForm popOrderForm)
        {
            var iGotUserContextInfo = popOrderForm as IGotUserContextInformations;
            var deliverySlotData = popOrderForm.GetPrivateData<DeliverySlotData>(create: true);

            if (iGotUserContextInfo.UserContextInformations != null)
            {
                //Permet a FSH de supprimer le data si un changement de compte client à lieu
                deliverySlotData.UID = iGotUserContextInfo.UserContextInformations.UID;
            }

            return deliverySlotData;
        }

        private (bool IsSuccess, string ErrorMessage, PostalShippingMethodEvaluationItem Item) GetPostalShippingMethodEvaluationItem(PopOrderForm popOrderForm, int sellerId)
        {
            var shippingMethodEvaluationGroup = popOrderForm.ShippingMethodEvaluation.GetShippingMethodEvaluationGroup(sellerId);
            if (!shippingMethodEvaluationGroup.HasValue)
            {
                return (IsSuccess: false,
                    ErrorMessage: "Aucune shipping methode n'est associée à ce sellerId",
                    Item: null);
            }

            var postalShippingMethodEvaluationItemMaybe = shippingMethodEvaluationGroup.Value.PostalShippingMethodEvaluationItem;
            if (!postalShippingMethodEvaluationItemMaybe.HasValue)
            {
                return (IsSuccess: false,
                    ErrorMessage: "Aucune shipping methode postale",
                    Item: null);
            }

            var shippingAddress = postalShippingMethodEvaluationItemMaybe.Value.ShippingAddress;
            if (shippingAddress == null)
            {
                return (IsSuccess: false,
                    ErrorMessage: "Aucune adresse de livraison n'est définie",
                    Item: null);
            }

            return (IsSuccess: true,
                ErrorMessage: string.Empty,
                Item: postalShippingMethodEvaluationItemMaybe.Value);
        }

        private void SetPipeMessages(AppointmentDeliveryCalendarViewModel appointmentDeliveryCalendarViewModel, DeliverySlotData deliverySlotData)
        {
            const string AppointmentDeliveryCalendarMessagesCategory = "AppointmentDeliveryCalendar.Messages";

            if (deliverySlotData.HasSelectedSlot && deliverySlotData.DeliverySlotSelected.HasChanged)
            {
                var uiResourceId = deliverySlotData.DeliverySlotSelected.PriceHasChanged
                                                     ? "orderpipe.pop.solex.appointment.pricechanged"
                                                     : "orderpipe.pop.solex.appointment.slotunavailable";
                appointmentDeliveryCalendarViewModel.PipeMessages.Add(_pipeMessageViewModelBuilder.CreatePipeMessageViewModel(AppointmentDeliveryCalendarMessagesCategory, uiResourceId));
            }

            if (!appointmentDeliveryCalendarViewModel.Slots.Any())
            {
                appointmentDeliveryCalendarViewModel.PipeMessages.Add(_pipeMessageViewModelBuilder.CreatePipeMessageViewModel(AppointmentDeliveryCalendarMessagesCategory, "orderpipe.pop.solex.appointment.noslotsreturn"));
            }
        }

        private void SetNextPage(AppointmentDeliveryCalendarViewModel appointmentDeliveryCalendarViewModel, AppointmentDeliverySlotsSortingResult appointmentDeliverySortedSlotsData, int pageIndex, int pageSize, DateTime endDateMaximum)
        {
            if (appointmentDeliverySortedSlotsData.IsServiceCallSuccess)
            {
                appointmentDeliveryCalendarViewModel.HasNextPage = appointmentDeliverySortedSlotsData.StartDateReference.AddDays((double)((int)pageIndex - 1) * pageSize).AddDays(pageSize + 1).Date < endDateMaximum.Date;
            }
        }

        #endregion
    }
}
