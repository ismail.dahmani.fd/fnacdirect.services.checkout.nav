using FnacDirect.OrderPipe.Base.BaseModel;
using FnacDirect.OrderPipe.Base.Business;
using FnacDirect.OrderPipe.Base.Business.Availability;
using FnacDirect.OrderPipe.Base.Business.Operations;
using FnacDirect.OrderPipe.Base.Business.Shipping;
using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.OrderPipe.Base.Model.LineGroups;
using FnacDirect.OrderPipe.Base.Model.Pop;
using FnacDirect.OrderPipe.Base.Model.Solex;
using FnacDirect.OrderPipe.Base.Proxy.Membership;
using FnacDirect.OrderPipe.Base.Steps.Pop;
using FnacDirect.OrderPipe.BaseMvc.Web.Pop.Models.Common;
using FnacDirect.OrderPipe.Core.Services;
using FnacDirect.Technical.Framework.Web.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using FnacDirect.OrderPipe.Base.LineGroups;
using FnacDirect.OrderPipe.Base.Model.Shipping.ShippingMethod;
using static FnacDirect.OrderPipe.Constants;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Common.Shipping.Postal
{
    public class PostalShippingNetworkViewModelBuilder : IPostalShippingNetworkViewModelBuilder
    {
        private readonly IApplicationContext _applicationContext;
        private readonly IPostalShippingChoiceViewModelBuilder _postalShippingChoiceViewModelBuilder;
        private readonly IPopAvailabilityProcessor _popAvailabilityProcessor;
        private readonly ISwitchProvider _switchProvider;
        public readonly ISolexBusiness _solexBusiness;
        private readonly IAppointmentDeliveryService _appointmentDeliveryService;
        private readonly IAddressValidationBusiness _addressValidationBusiness;
        private readonly IPipeMessageViewModelBuilder _pipeMessageViewModelBuilder;
        private readonly IPipeParametersProvider _pipeParametersProvider;
        private readonly IAdherentCardArticleService _adherentCardArticleService;
        private readonly HashSet<int> _overseaShipppingZones;

        public PostalShippingNetworkViewModelBuilder(IApplicationContext applicationContext,
                                                     IPostalShippingChoiceViewModelBuilder postalShippingChoiceViewModelBuilder,
                                                     IPopAvailabilityProcessor popAvailabilityProcessor,
                                                     ISwitchProvider switchProvider,
                                                     ISolexBusiness solexBusiness,
                                                     IAppointmentDeliveryService appointmentDeliveryService,
                                                     IAddressValidationBusiness addressValidationBusiness,
                                                     IPipeMessageViewModelBuilder pipeMessageViewModelBuilder,
                                                     IPipeParametersProvider pipeParametersProvider,
                                                     IAdherentCardArticleService adherentCardArticleService)
        {
            _applicationContext = applicationContext;
            _postalShippingChoiceViewModelBuilder = postalShippingChoiceViewModelBuilder;
            _popAvailabilityProcessor = popAvailabilityProcessor;
            _switchProvider = switchProvider;
            _solexBusiness = solexBusiness;
            _appointmentDeliveryService = appointmentDeliveryService;
            _addressValidationBusiness = addressValidationBusiness;
            _pipeMessageViewModelBuilder = pipeMessageViewModelBuilder;
            _pipeParametersProvider = pipeParametersProvider;
            _overseaShipppingZones = new HashSet<int>(pipeParametersProvider.GetAsEnumerable<int>("shippingaddress.OverseaShippingZone"));
            _adherentCardArticleService = adherentCardArticleService;
        }

        public PostalShippingNetworkViewModel Build(PopOrderForm popOrderForm, int sellerId)
        {
            var christmasData = popOrderForm.GetPrivateData<ChristmasData>(false);

            var maybeShippingMethodEvaluationGroup = popOrderForm.ShippingMethodEvaluation.GetShippingMethodEvaluationGroup(sellerId);

            var shippingMethodEvaluationGroup = maybeShippingMethodEvaluationGroup.Value;

            var postalShippingViewModel = new PostalShippingNetworkViewModel
            {
                CurrentMode = shippingMethodEvaluationGroup.SelectedShippingMethodEvaluationType == ShippingMethodEvaluationType.Postal
            };

            var hasMissingInfo = !_addressValidationBusiness.IsAddressValid(shippingMethodEvaluationGroup.PostalShippingMethodEvaluationItem?.Value.ShippingAddress.Convert());

            postalShippingViewModel.Selected = BuildDetail(shippingMethodEvaluationGroup.PostalShippingMethodEvaluationItem.Value.ShippingAddress,
                                                           hasMissingInfo);

            postalShippingViewModel.PostalShippingChoices = BuildPostalChoiceViewModel(popOrderForm, sellerId).ToList();

            if (christmasData != null && christmasData.HasCurrentPhase)
            {
                postalShippingViewModel.HasMarketingEvent = (postalShippingViewModel.PostalShippingChoices?.Any(r => r.BeforeXmas)).GetValueOrDefault();

                postalShippingViewModel.MarketingEventPicto = christmasData.PictoUiresourceKey != null ? _applicationContext.GetMessage(christmasData.PictoUiresourceKey) : string.Empty;
            }

            if (postalShippingViewModel.Selected.HasValue)
            {
                var popShippingData = popOrderForm.GetPrivateData<PopShippingData>();

                var hasManyEligibleBillingAddresses = popShippingData.HasManyEligibleBillingAddresses;

                var postalDetailsViewModel = postalShippingViewModel.Selected.Value;

                postalDetailsViewModel.HasManyEligibleBillingAddresses = hasManyEligibleBillingAddresses;

                var postalShippingChoice = postalShippingViewModel.PostalShippingChoices.FirstOrDefault(c => c.IsSelected);

                if (postalShippingChoice != null)
                {
                    var summary = new SelectedShippingItemSummary()
                    {
                        Label = _applicationContext.GetMessage("orderpipe.pop.oneclick.shipping.postal.prefix"),
                        WorstDeliveryDate = postalShippingChoice.WorstDeliveryDate
                    };
                    if (postalShippingChoice.HasManyLongEstimatedDeliveryDate)
                    {
                        summary.DisplayLongEstimatedDeliveryDates = true;
                        summary.DisplayComplexLongEstimatedDeliveryDates = true;
                        summary.Label += _applicationContext.GetMessage("orderpipe.pop.oneclick.shipping.postal.complex");
                        summary.ComplexLongEstimatedDeliveryDate = postalShippingChoice.LongEstimatedDeliveryDate;
                    }
                    else if (postalShippingChoice.Delays.Any())
                    {
                        summary.Label += postalShippingChoice.Delays.First().Delay;
                    }

                    summary.ShortEstimatedDeliveryDate = postalShippingChoice.ShortEstimatedDeliveryDate;

                    // TODO: retirer le commentaire pour assigner la valeur "réelle" du choice, une fois l'intégration du ShippingCostToDisplay effectuée.
                    summary.ShippingCost = postalShippingChoice.ShippingCost;

                    summary.ShippingCostNoVat = postalShippingChoice.ShippingCostNoVat;

                    summary.ShippingCostToDisplay = postalShippingChoice.ShippingCostToDisplay;

                    summary.ShippingCostModelToDisplay = postalShippingChoice.ShippingCostModelToDisplay;

                    summary.IsFreeShipping = postalShippingChoice.IsFreeShipping;

                    if (postalShippingViewModel.PostalShippingChoices.Count > 1)
                    {
                        summary.CanChooseOther = true;
                    }

                    postalDetailsViewModel.Summary = summary;
                }
            }
            
            if (SellerIds.GetType(sellerId) == SellerType.Fnac)
            {
                var shippingFnacPlusMember = popOrderForm.GetPrivateData<ShippingFnacPlusMember>(create: false);
                if (shippingFnacPlusMember != null)
                {
                    postalShippingViewModel.FnacPlusCardIdFromVirtualExpressFnacPlus = shippingFnacPlusMember.FnacPlusCardIdFromVirtualExpressFnacPlus;
                }
            }

            return postalShippingViewModel;
        }

        public PostalDetailsViewModel BuildDetail(PostalAddress address, bool hasMissingInfo = false, bool hasManyEligibleBillingAddresses = false)
        {
            var siteCountryId = _applicationContext.GetSiteContext().CountryId;

            if (_overseaShipppingZones != null && _overseaShipppingZones.Any())
            {
                address.SetOverseaInformation(_overseaShipppingZones.ToList());
            }

            return new PostalDetailsViewModel(address.ConvertToAddressViewModel(siteCountryId, hasMissingInfo),
                                              address.IsDummy(),
                                              hasManyEligibleBillingAddresses);
        }

        public virtual IEnumerable<PostalShippingChoiceViewModel> BuildPostalChoiceViewModel(PopOrderForm popOrderForm, int sellerId)
        {
            var maybeShippingMethodEvaluationGroup = popOrderForm.ShippingMethodEvaluation.GetShippingMethodEvaluationGroup(sellerId);

            if (!maybeShippingMethodEvaluationGroup.HasValue)
            {
                return Enumerable.Empty<PostalShippingChoiceViewModel>();
            }

            var shippingMethodEvaluationGroup = maybeShippingMethodEvaluationGroup.Value;

            //Parcourir la nouvelle structure pour remplir le modèle IEnumerable<PostalShippingChoiceViewModel>
            var postalShippingMethodEvaluationItem = shippingMethodEvaluationGroup.PostalShippingMethodEvaluationItem;

            var isPostalModeSelected = shippingMethodEvaluationGroup.SelectedShippingMethodEvaluationType == ShippingMethodEvaluationType.Postal;
            
            return InnerBuildPostalChoiceViewModelWithSolex(popOrderForm, postalShippingMethodEvaluationItem, isPostalModeSelected, sellerId);
        }

        private IEnumerable<PostalShippingChoiceViewModel> InnerBuildPostalChoiceViewModelWithSolex(PopOrderForm popOrderForm, Maybe<PostalShippingMethodEvaluationItem> postalShippingMethodEvaluationItem, bool isPostalModeSelected, int sellerId)
        {
            var postalShippingChoices = new List<ShippingChoice>();

            if (postalShippingMethodEvaluationItem.HasValue && postalShippingMethodEvaluationItem.Value.Choices.Any())
            {
                postalShippingChoices = postalShippingMethodEvaluationItem.Value.Choices.ToList();
            }

            var postalShippingChoiceViewModels = new List<PostalShippingChoiceViewModel>();

            var popData = popOrderForm.GetPrivateData<PopData>();

            foreach (var choice in postalShippingChoices)
            {
                if (_switchProvider.IsEnabled("orderpipe.pop.payment.payondelivery"))
                {
                    if (choice.MainShippingMethodId == ShippingMethods.ExpressOneDay
                        && popOrderForm.HasMarketPlaceArticle())
                    {
                        continue;
                    }
                }

                var model = _postalShippingChoiceViewModelBuilder.Builder(choice, postalShippingMethodEvaluationItem.Value);

                if (choice.ContainsShippingMethodId(ShippingMethods.LivraisonDeuxHeuresChrono))
                {
                    var immediateDeliveryData = popOrderForm.GetPrivateData<ImmediateDeliveryData>(create: false);
                    var isImmediateDeliveryEligible = immediateDeliveryData?.IsImmediateDeliveryEligible == true;

                    // Promesse sèche
                    if (choice.MainShippingMethodId == ShippingMethods.LivraisonDeuxHeuresChrono && !isImmediateDeliveryEligible)
                    {
                        model.PipeMessages.Add(_pipeMessageViewModelBuilder.CreatePipeMessageViewModel("PostalShippingNetwork.ImmediateDeliveryMessage", "orderpipe.pop.shipping.solex.appointmentdeliverymethod.57.ineligible"));
                    }
                }

                if (_switchProvider.IsEnabled("orderpipe.pop.shipping.price.checkvateligibility")
                    && (!_switchProvider.IsEnabled("orderpipe.pop.taxcalculationservice.activation") || _pipeParametersProvider.Get("taxcalculationservice.forcelegacy", false))
                    && postalShippingMethodEvaluationItem.HasValue)
                {
                    _postalShippingChoiceViewModelBuilder.CheckVatEligibility(model, postalShippingMethodEvaluationItem.Value, choice);
                }

                model.Delays = new List<DelayModel>();

                var articles = popOrderForm.LineGroups.Seller(sellerId).GetArticles().OfType<BaseArticle>();
                if (!_switchProvider.IsEnabled("orderpipe.pop.shipping.dlemp.enable"))
                {
                    articles = articles.Where(a => a is StandardArticle);
                }
                var availabilities = new List<PopAvailabilityProcessorInput>();
                var longEstimatedDeliveryDate = new List<PopAvailabilityProcessorOutput<PopAvailabilityProcessorOutput>>();

                var currentShippingMethodId = choice.MainShippingMethodId;
                var label = GetLabel(choice, currentShippingMethodId);
                var articleParcelShippingMethodList = new Dictionary<BaseArticle, int>();
                foreach (var article in articles)
                {
                    var selecteOrDefaultChoice = choice.SelectedChoice ?? choice.DefaultChoice;
                    if (selecteOrDefaultChoice != null)
                    {
                        Parcel currentParcel = null;
                        var articleFound = false;
                        foreach (var group in selecteOrDefaultChoice.Groups)
                        {
                            foreach (var item in group.Items)
                            {
                                if (item.Identifier.Prid == article.ProductID)
                                {
                                    articleFound = true;
                                }
                                if (articleFound)
                                {
                                    currentParcel = selecteOrDefaultChoice.Parcels[item.ParcelId];
                                    break;
                                }
                            }
                            if (articleFound)
                            {
                                break;
                            }
                        }
                        if (currentParcel != null)
                        {
                            articleParcelShippingMethodList.Add(article, currentParcel.ShippingMethodId);
                        }
                    }
                }
                _postalShippingChoiceViewModelBuilder.BuildDelays(model, articles.Count(), label, currentShippingMethodId, choice);
                var availabilityList = new PopAvailabilityProcessorInputListBuilder(articles, currentShippingMethodId)
                    .WithShippingMethodId()
                    .WithParcelShippingMethodIds(articleParcelShippingMethodList)
                    .BuildList();

                availabilities.AddRange(availabilityList);

                longEstimatedDeliveryDate.Add(new PopAvailabilityProcessorOutput<PopAvailabilityProcessorOutput>(label, _popAvailabilityProcessor.GetLongGlobalEstimatedDeliveryDate(availabilityList, false)));


                //Si un seul delai alors signaler la vue
                if (model.Delays.Count == 1)
                {
                    model.HasOneDelay = true;
                }

                model.IsAvailable = choice.IsAvailable;
                model.MethodId = choice.MainShippingMethodId;
                model.WorstDeliveryDate = ComputeWorstDeliveryDate(availabilities);

                model.LongEstimatedDeliveryDate = _popAvailabilityProcessor.SortPostalLongGlobalEstimatedDeliveryDate(longEstimatedDeliveryDate);
                var hasleExcludedAvailabilities = popData.HasExcludedAvailabilitiesForDeliveryDates;
                model.ShortEstimatedDeliveryDate = _popAvailabilityProcessor.GetShortGlobalEstimatedDeliveryDate(model.MethodId, availabilities, model.HasManyLongEstimatedDeliveryDate);
                model.DisplayLongEstimatedDeliveryDates = (model.HasManyLongEstimatedDeliveryDate || model.HasOneLongEstimatedDeliveryDateWithSeveralDates) && !hasleExcludedAvailabilities;

                if (isPostalModeSelected && postalShippingMethodEvaluationItem.Value.SelectedShippingMethodId.HasValue)
                {
                    model.IsSelected = postalShippingMethodEvaluationItem.Value.SelectedShippingMethodId.Value == choice.MainShippingMethodId;
                }

                if (postalShippingMethodEvaluationItem.HasValue)
                {
                    model.DefaultShippingMethodId = postalShippingMethodEvaluationItem.Value.DefaultShippingMethodId;
                }

                var deliverySlotData = popOrderForm.GetPrivateData<DeliverySlotData>(create: false);

                model.IsAppointmentDelivery = _appointmentDeliveryService.ShippingMethodsThatOpensCalendar.Contains(choice.MainShippingMethodId);

                if (model.IsAppointmentDelivery && deliverySlotData != null)
                {
                    model.HasSelectedSlot = deliverySlotData.HasSelectedSlot;
                }

                if (!postalShippingMethodEvaluationItem.Value.IsDummyAddress)
                {
                    model.CanOpenDeliveryCalendar = true;
                }
                model = ShouldOpenDeliveryCalendar(postalShippingMethodEvaluationItem, model, deliverySlotData);

                if (!postalShippingChoiceViewModels.Any(c => c.MethodId == model.MethodId))
                {
                    postalShippingChoiceViewModels.Add(model);
                }
            }

            if (_switchProvider.IsEnabled("orderpipe.pop.shipping.postal.expressWithFnacplus"))
            {
                var isAdherentOrHasAdherentCardInBasket = popOrderForm.UserContextInformations.IsAdherent
                                                        || popOrderForm.LineGroups.GetArticles().OfType<StandardArticle>().Any(a => _adherentCardArticleService.IsAdherentCard(a.ProductID.GetValueOrDefault(0)));

                if (postalShippingChoiceViewModels.Any(c => c.MethodId == ShippingMethods.ExpressIDF || c.MethodId == ShippingMethods.ExpressOneDay)
                    && !isAdherentOrHasAdherentCardInBasket)
                {
                    var choiceExpressDelivery = postalShippingChoiceViewModels.First(c => c.MethodId == ShippingMethods.ExpressIDF || c.MethodId == ShippingMethods.ExpressOneDay);
                    var choiceWithFnacPlus = CreateExpressDeliveryWithFnacPlus(choiceExpressDelivery);
                    postalShippingChoiceViewModels.Insert(0, choiceWithFnacPlus);
                }
            }

            return postalShippingChoiceViewModels;
        }

        private PostalShippingChoiceViewModel CreateExpressDeliveryWithFnacPlus(PostalShippingChoiceViewModel choiceExpressDelivery)
        {
            var choiceWithAdherentFnac = new PostalShippingChoiceViewModel();
            choiceWithAdherentFnac.MethodId = ShippingMethods.VirtualExpressFnacPlus;
            choiceWithAdherentFnac.IsSelected = false;
            choiceWithAdherentFnac.DefaultShippingMethodId = choiceExpressDelivery.DefaultShippingMethodId;

            GetPriceModel(choiceWithAdherentFnac, 0m);
            choiceWithAdherentFnac.TotalPrice = 0m;

            choiceWithAdherentFnac.IsFreeShipping = choiceExpressDelivery.IsFreeShipping;

            choiceWithAdherentFnac.HasOneDelay = choiceExpressDelivery.HasOneDelay;
            choiceWithAdherentFnac.IsAvailable = choiceExpressDelivery.IsAvailable;
            choiceWithAdherentFnac.WorstDeliveryDate = choiceExpressDelivery.WorstDeliveryDate;

            choiceWithAdherentFnac.IsAppointmentDelivery = choiceExpressDelivery.IsAppointmentDelivery;
            choiceWithAdherentFnac.OpenAppointmentDeliveryCalendar = choiceExpressDelivery.OpenAppointmentDeliveryCalendar;
            choiceWithAdherentFnac.CanOpenDeliveryCalendar = choiceExpressDelivery.CanOpenDeliveryCalendar;
            choiceWithAdherentFnac.HasSelectedSlot = choiceExpressDelivery.HasSelectedSlot;

            choiceWithAdherentFnac.Weight = choiceExpressDelivery.Weight;
            choiceWithAdherentFnac.CarbonFootprint = choiceExpressDelivery.CarbonFootprint;

            var delays = new List<DelayModel>();
            delays.AddRange(choiceExpressDelivery.Delays);
            choiceWithAdherentFnac.Delays = delays;

            choiceWithAdherentFnac.ShortEstimatedDeliveryDate = choiceExpressDelivery.ShortEstimatedDeliveryDate;

            var longEstimatedDeliveryDate = new List<PopAvailabilityProcessorOutput<PopAvailabilityProcessorOutput>>();
            longEstimatedDeliveryDate.AddRange(choiceExpressDelivery.LongEstimatedDeliveryDate);
            choiceWithAdherentFnac.LongEstimatedDeliveryDate = longEstimatedDeliveryDate;

            choiceWithAdherentFnac.DisplayLongEstimatedDeliveryDates = choiceExpressDelivery.DisplayLongEstimatedDeliveryDates;
            return choiceWithAdherentFnac;
        }

        private void GetPriceModel(PostalShippingChoiceViewModel choiceWithAdherentFnac, decimal price)
        {
            var priceHelper = _applicationContext.GetPriceHelper();
            choiceWithAdherentFnac.ShippingCost = priceHelper.Format(price, PriceFormat.CurrencyAsDecimalSeparator);
            choiceWithAdherentFnac.ShippingCostModel = priceHelper.GetDetails(price);
            choiceWithAdherentFnac.ShippingCostNoVat = priceHelper.Format(price, PriceFormat.CurrencyAsDecimalSeparator);
            choiceWithAdherentFnac.ShippingCostNoVatModel = priceHelper.GetDetails(price);
            choiceWithAdherentFnac.ShippingCostToDisplay = priceHelper.Format(price, PriceFormat.CurrencyAsDecimalSeparator);
            choiceWithAdherentFnac.ShippingCostModelToDisplay = priceHelper.GetDetails(price);
        }

        private PostalShippingChoiceViewModel ShouldOpenDeliveryCalendar(Maybe<PostalShippingMethodEvaluationItem> postalShippingMethodEvaluationItem, PostalShippingChoiceViewModel postalShippingChoiceViewModel, DeliverySlotData deliverySlotData)
        {
            if (postalShippingMethodEvaluationItem.Value.SelectedShippingMethodId.HasValue
                && postalShippingChoiceViewModel.IsAppointmentDelivery
                && !postalShippingMethodEvaluationItem.Value.IsDummyAddress)
            {
                if (deliverySlotData == null)
                {
                    postalShippingChoiceViewModel.OpenAppointmentDeliveryCalendar = true;
                }

                if (deliverySlotData != null && (!deliverySlotData.HasSelectedSlot || deliverySlotData.DeliverySlotSelected.HasChanged))
                {
                    postalShippingChoiceViewModel.OpenAppointmentDeliveryCalendar = true;
                }
            }

            return postalShippingChoiceViewModel;
        }

        private string GetLabel(ShippingChoice choice, int currentShippingMethodId)
        {
            var label = _applicationContext.TryGetMessage("orderpipe.pop.shipping.methods." + currentShippingMethodId);

            if (string.IsNullOrEmpty(label))
            {
                if (!string.IsNullOrEmpty(choice.AltLabel))
                {
                    label = choice.AltLabel;
                }
                else
                {
                    label = choice.Label;
                }
            }

            return label;
        }

        private DateTime? ComputeWorstDeliveryDate(IEnumerable<PopAvailabilityProcessorInput> availabilities)
        {
            var matchingAvailability = availabilities.OrderByDescending(x => x.DateMax).FirstOrDefault();

            if (matchingAvailability != null && matchingAvailability.DateMax.HasValue)
            {
                return matchingAvailability.DateMax;
            }

            matchingAvailability = availabilities.OrderByDescending(x => x.DateMin).FirstOrDefault();

            if (matchingAvailability != null && matchingAvailability.DateMin.HasValue)
            {
                return matchingAvailability.DateMin;
            }

            return null;
        }

        protected virtual PriceFormat PriceFormat { get { return PriceFormat.CurrencyAsDecimalSeparator; } }
    }
}
