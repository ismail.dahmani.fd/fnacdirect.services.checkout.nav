﻿using FnacDirect.OrderPipe.Base.Model;
using System;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Basket
{
    public interface IBasketViewModelBuilder
    {
        Maybe<BasketViewModel> Build(PopOrderForm popOrderForm);
    }
}
