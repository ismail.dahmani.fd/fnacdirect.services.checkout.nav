namespace FnacDirect.OrderPipe.BaseMvc.Web.Pop.Models.Common
{
    public class NavigationViewModel
    {
        public bool HasNextSeller { get; set; }
        public string BackLinkUrl { get; set; }

        public bool IsMobileApplication { get; set; }

        public bool IsIos { get; set; }
    }
}
