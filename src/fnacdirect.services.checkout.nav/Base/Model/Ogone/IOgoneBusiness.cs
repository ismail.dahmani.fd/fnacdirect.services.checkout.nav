using FnacDirect.Customer.Model;
using FnacDirect.Ogone.Model;
using FnacDirect.OrderPipe.Base.BaseModel;
using FnacDirect.OrderPipe.Base.LineGroups;
using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.OrderPipe.Base.Model.Facets;
using FnacDirect.OrderPipe.Base.Model.LineGroups;
using System.Collections.Generic;

namespace FnacDirect.OrderPipe.Base.Business.Ogone
{
    public interface IOgoneBusiness
    {
        SortedList<string, string> BuildOgoneAliasGatewayParameters(string pspId, string pipeName, string culture, string creditCardAlias = null);
        string GetSHASignParameterValue(SortedList<string, string> ogoneUrlParameters);
        void CompleteCurrentOri(OgoneCreditCardBillingMethod billingMethod, ILogisticLineGroup logisticLineGroup, OrderTransactionInfo currentOti, OrderTransactionRuleType? currentOrderTransactionRule, string culture);
        void FillInCurrentOti(OgoneCreditCardBillingMethod billingMethod, DebitType debitType, OrderTransactionInfo currentOti, bool isSelectedForPsp);
        void SetBillingMethodAmount(AutorisationAmount? autorisationAmount, OgoneCreditCardBillingMethod billingMethod, ILogisticLineGroup hightAmountLineGroup);
        decimal? GetTransactionAmount(AutorisationAmount? autorizationAmount, ILogisticLineGroup logisticLineGroup, int billingMethodId);
        /// <summary>
        /// Récupère le pspid en fonction du context
        /// </summary>
        string GetPspId(OrderTypeEnum orderTypeEnum, IEnumerable<Article> articles, OgoneConfigurationData ogoneConfigurationData, int? legalEntityId = null);

        OrderTransactionOgone GetOgoneData(OrderTransactionInfoList orderTransactionInfos, IGotBillingInformations gotBillingInformations);
        OrderTransactionOgone GetOgoneData(PopOrderForm popOrderForm);
    }
}
