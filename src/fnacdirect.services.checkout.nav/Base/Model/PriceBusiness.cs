using Common.Logging;
using FnacDirect.Front.Catalog.IModel;
using FnacDirect.Opc.OpcCore.IBusiness;
using FnacDirect.OrderPipe.Base.BaseModel.Model;
using FnacDirect.OrderPipe.Base.Business.Helpers;
using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.OrderPipe.Base.Model.Facets;
using FnacDirect.OrderPipe.Base.Model.FDV;
using FnacDirect.StoreAvailability.Business;
using FnacDirect.Technical.Framework.Caching2;
using FnacDirect.Technical.Framework.CoreServices;
using FnacDirect.Technical.Framework.ServiceLocation;
using FnacDirect.Technical.Framework.Switch;
using FnacDirect.Technical.Framework.Web.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using DTO = FnacDirect.Contracts.Online.Model;

namespace FnacDirect.OrderPipe.Base.Business
{
    public class PriceBusiness : IPricingService
    {
        private readonly IVatBusiness _vatBusiness;
        private readonly IApplicationContext _applicationContext;
        private readonly ISTOMBusiness _STOMBusiness;
        private readonly ILog _logger;
        private readonly int _marketId;

        //shall we fix this design and pass the marketId as GetVATInfo and GetVATRate
        public PriceBusiness(int pMarketId)
        {
            _vatBusiness = ServiceLocator.Current.GetInstance<IVatBusiness>();
            _applicationContext = ServiceLocator.Current.GetInstance<IApplicationContext>();
            _STOMBusiness = ServiceLocator.Current.GetInstance<ISTOMBusiness>();
            _logger = ServiceLocator.Current.GetInstance<ILog>();
            _marketId = pMarketId;
        }

        public decimal GetPrice(DTO.Price price, VatCountry vatCountry, int vatId, int localCountry)
        {
            return GetPrice(price.TTC, price.HT, vatCountry, vatId, localCountry);
        }

        public decimal GetPrice(decimal priceTTC, decimal? priceHT, VatCountry vatCountry, int vatId, int localCountry)
        {
            var isCountryUseVAT = false;
            int? vatCountryId = null;
            if (vatCountry != null)
            {
                isCountryUseVAT = vatCountry.UseVat;
                vatCountryId = vatCountry.VatCountryId;
            }
            return GetPrice(priceTTC, priceHT, isCountryUseVAT, vatCountryId, vatId, localCountry);
        }

        public decimal GetPrice(decimal priceTTC, decimal? priceHT, bool isCountryUseVAT, int? vatCountryId, int vatId, int localCountryId)
        {
            if (isCountryUseVAT)
            {
                if (vatCountryId == localCountryId)
                {
                    return priceTTC;
                }

                decimal fullPriceHt;
                if (priceHT == null)
                {
                    fullPriceHt = priceTTC / (1 + GetVATRate(localCountryId, vatId) / 100);
                }
                else
                {
                    fullPriceHt = priceHT.Value;
                }

                return Math.Round(fullPriceHt * (1 + (GetVATRate(vatCountryId.Value, vatId) / 100)), 2);
            }

            if (priceHT == null)
            {
                priceHT = Math.Round(priceTTC / (1 + GetVATRate(localCountryId, vatId) / 100), 2);
            }

            return priceHT.Value;
        }

        public decimal GetPriceEbook(decimal priceTTC, decimal? priceHT, bool isCountryUseVAT, int vatId, int localCountryId)
        {
            if (isCountryUseVAT)
            {
                return priceTTC;
            }

            if (priceHT == null)
            {
                priceHT = Math.Round(priceTTC / (1 + GetVATRate(localCountryId, vatId) / 100), 2);
            }

            return priceHT.Value;
        }

        public decimal GetPriceHT(decimal priceTTC, decimal vatRate)
        {
            return vatRate == 0 ? priceTTC : Math.Round(priceTTC / (1 + vatRate / 100), 2);
        }

        public decimal GetPriceHT(decimal priceTTC, int vatId, int localCountry)
        {
            decimal vatRate = 0;

            try
            {
                vatRate = GetVATRate(localCountry, vatId);
            }
            catch { }

            return vatRate > 0 ? Math.Round(priceTTC / (1 + GetVATRate(localCountry, vatId) / 100), 2) : priceTTC;
        }

        public decimal GetVATRate(int? countryId, int prmiVatId)
        {
            return GetVATInfo(countryId.GetValueOrDefault(0), prmiVatId).Rate;
        }

        public VatInfo GetVATInfo(int countryId, int prmiVatId)
        {
            VatInfo returnvalue = null;
            string culture = _applicationContext.GetSiteContext().Culture;
            var vatRates = _vatBusiness.GetVats(culture, _marketId);
            if (vatRates != null && vatRates.Any())
            {
                try
                {
                    var vatRate = vatRates.Single(vat => vat.CountryCode == countryId && vat.VatCategoryId == prmiVatId);
                    if (vatRate != null)
                    {
                        returnvalue = new VatInfo
                        {
                            CodeGU = vatRate.VatRefGu,
                            CountryCode = vatRate.CountryCode,
                            Rate = vatRate.Rate,
                            VatCategory = vatRate.VatCategoryId
                        };
                    }
                }
                catch (InvalidOperationException e)
                {
                    // TVA non présente dans la base de données ou absence d'unicité de TVA après filtre sur le pays et la categorie d'article
                    e.Data["CountryId"] = countryId;
                    e.Data["VatId"] = prmiVatId;
                    _logger.Error("Absence d'unicité retournée pour le taux de TVA", e);
                }
            }
            else
            {   // Aucune TVA  présente dans la base de données
                _logger.Error($"Aucune TVA n'est présente en base de données pour le marché {_marketId} et la culture {culture}.");
            }

            if (returnvalue == null)
            {
                returnvalue = new VatInfo
                {
                    VatCategory = prmiVatId
                };
            }
            return returnvalue;
        }

        public DTO.ShoppingCart GetArticlesPrices(DTO.SiteContext siteContext,
            int localCountry,
            DTO.Customer dtoCustomer,
            MultiFacet<IGotLineGroups, IGotMobileInformations, IGotBillingInformations, IGotShippingMethodEvaluation> multiFacets,
            GetArticlesPricesParameters getArticlesPricesParameters,
            bool useCache)
        {
            return GetArticlesPrices(siteContext,
                                     localCountry,
                                     dtoCustomer,
                                     multiFacets,
                                     multiFacets.Facet1.LineGroups.GetArticles(),
                                     null,
                                     getArticlesPricesParameters,
                                     useCache);
        }

        public DTO.ShoppingCart GetArticlesPrices(DTO.SiteContext siteContext,
                                                  int localCountry,
                                                  DTO.Customer dtoCustomer,
                                                  MultiFacet<IGotLineGroups, IGotMobileInformations, IGotBillingInformations, IGotShippingMethodEvaluation> multiFacets,
                                                  IEnumerable<Article> articles,
                                                  List<DTO.ShoppingCartLineItem> replacedArticles,
                                                  GetArticlesPricesParameters getArticlesPricesParameters,
                                                  bool useCache,
                                                  ShopAddress shippingShop = null)
        {
            var basket = new DTO.ShoppingCart();

            ShippingAddress shippingAddress = shippingShop;

            if (shippingAddress == null)
            {
                if (multiFacets.Facet4.ShippingMethodEvaluation.FnacCom.HasValue)
                {
                    var fnacComShippingMethodEvaluationGroup = multiFacets.Facet4.ShippingMethodEvaluation.FnacCom.Value;

                    // TODO cela ne devrait pas arriver !
                    if (fnacComShippingMethodEvaluationGroup.SelectedShippingMethodEvaluation != null)
                    {
                        shippingAddress = fnacComShippingMethodEvaluationGroup.SelectedShippingMethodEvaluation.ShippingAddress;
                    }
                    else
                    {
                        if (fnacComShippingMethodEvaluationGroup.PostalShippingMethodEvaluationItem.HasValue)
                        {
                            shippingAddress = fnacComShippingMethodEvaluationGroup.PostalShippingMethodEvaluationItem.Value.ShippingAddress;
                        }
                    }
                }
                else if (multiFacets.Facet4.ShippingMethodEvaluation.ShippingMethodEvaluationGroups.Any(a => a.ShopShippingMethodEvaluationItem.HasValue))
                {
                    var shopEvaluationGroup = multiFacets.Facet4.ShippingMethodEvaluation.ShippingMethodEvaluationGroups.First(a => a.ShopShippingMethodEvaluationItem.HasValue);
                    if (shopEvaluationGroup.ShopShippingMethodEvaluationItem.HasValue)
                    {
                        shippingAddress = shopEvaluationGroup.ShopShippingMethodEvaluationItem.Value.ShippingAddress;
                    }
                }
            }

            var contextInfos = GetContextInfos(siteContext,
                                               articles,
                                               multiFacets.Facet3.OrderGlobalInformations.AdvantageCode,
                                               dtoCustomer,
                                               new MultiFacet<IGotLineGroups, IGotMobileInformations, IGotBillingInformations>(multiFacets.Facet1, multiFacets.Facet2, multiFacets.Facet3),
                                               shippingAddress,
                                               basket,
                                               getArticlesPricesParameters);

            return EvaluateShoppingCartWithPreferredArticlesReplacement(contextInfos, useCache, true, replacedArticles);
        }

        public DTO.ShoppingCart GetArticlesPrices(DTO.SiteContext siteContext,
                                                  int localCountry,
                                                  IEnumerable<Article> articles,
                                                  string advantageCode,
                                                  DTO.Customer dtoCustomer,
                                                  BaseOF orderForm,
                                                  GetArticlesPricesParameters getArticlesPricesParameters)
        {
            if (articles is IList<Article>)
            {
                CollectionUtils.RemoveItemsFromCollection(articles as IList<Article>, art => art == null || art.Type == ArticleType.Free);
            }

            var shoppingCart = new DTO.ShoppingCart();

            ShippingAddress shippingAddress = null;
            foreach (var lg in orderForm.LineGroups)
            {
                if (lg.Seller != null && lg.Seller.SubType == LineGroups.SellerType.Fnac // linegroup fnac.com only no marketplace, no numerical, no clickandcollect
                    && lg.ShippingAddress != null)
                {
                    shippingAddress = lg.ShippingAddress;
                    break;
                }
            }

            if (shippingAddress == null)
                shippingAddress = orderForm.CalcShippingAddress;

            var contextInfos = GetContextInfos(siteContext,
                                               articles,
                                               advantageCode,
                                               dtoCustomer,
                                               MultiFacet<IGotLineGroups, IGotMobileInformations, IGotBillingInformations>.From(orderForm),
                                               shippingAddress,
                                               shoppingCart,
                                               getArticlesPricesParameters);

            return EvaluateShoppingCartWithPreferredArticlesReplacement(contextInfos, false, false);
        }

        public DTO.ShoppingCart GetArticlesPrices(DTO.SiteContext siteContext,
                                                  IEnumerable<Article> articles)
        {
            if (articles is IList<Article>)
            {
                CollectionUtils.RemoveItemsFromCollection(articles as IList<Article>, art => art == null || art.Type == ArticleType.Free);
            }

            var shoppingCart = new DTO.ShoppingCart();

            var contextInfos = GetLightContextInfos(siteContext, shoppingCart);
            GetServiceLineInfo(contextInfos, articles);

            return EvaluateShoppingCartWithPreferredArticlesReplacement(contextInfos, false, false);
        }

        private DTO.ShoppingCart EvaluateShoppingCartWithPreferredArticlesReplacement(DTO.Context contextInfos,
                                                                                     bool useCache,
                                                                                     bool shouldReplacePreferredArticles,
                                                                                     List<DTO.ShoppingCartLineItem> replacedArticles = null)
        {
            var serviceFactory = new ServiceFactory<ICoreEngineBusiness>();
            var coreEngineBusiness = serviceFactory.CreateInstance();

            var shoppingCart = coreEngineBusiness.EvaluateShoppingCart(contextInfos, useCache);

            if (shouldReplacePreferredArticles && replacedArticles != null)
            {
                replacedArticles.Clear();

                var lineItemsWithPreferredArticle = shoppingCart.LineItems.Where(li => li.PreferredArticle != null);
                if (lineItemsWithPreferredArticle.Any())
                {
                    foreach (var lineItem in lineItemsWithPreferredArticle)
                    {
                        replacedArticles.Add(lineItem);
                        lineItem.Reference = new DTO.ArticleReference { PRID = lineItem.PreferredArticle.Prid };
                    }

                    contextInfos.ShoppingCart = shoppingCart;
                    shoppingCart = coreEngineBusiness.EvaluateShoppingCart(contextInfos, useCache);
                }
            }

            return shoppingCart;
        }

        /// <summary>
        /// Get retrieval channel to use in the PRICER
        /// </summary>
        /// <returns></returns>
        private static string GetRetrievalChannel(DTO.SiteContext context, IGotMobileInformations orderForm, string forcedRetrievalChannel = null)
        {
            if (!string.IsNullOrEmpty(forcedRetrievalChannel))
                return forcedRetrievalChannel;

            if (orderForm.GetPrivateData<ShopInfosData>(create: false) != null)
                return DTO.RetrievalChannelString.RC_SHOP;

            var retrievalChannel = string.Empty;

            var isMobile = orderForm.OrderMobileInformations.IsMobile;
            switch (context.SiteID)
            {
                case DTO.FnacSites.FNACCOM:
                    if (isMobile)
                        retrievalChannel = DTO.RetrievalChannelString.RC_FNACCOM_MOBILE;
                    else
                        retrievalChannel = DTO.RetrievalChannelString.RC_FNACCOM;
                    break;
                case DTO.FnacSites.FNACPRO:
                    retrievalChannel = DTO.RetrievalChannelString.RC_FNACPRO;
                    break;
                case DTO.FnacSites.CLICETMAG:
                    retrievalChannel = DTO.RetrievalChannelString.RC_SHOP;
                    break;
            }

            return retrievalChannel;
        }

        private static void GetServiceLineInfoPiggyBack(DTO.Context context, IGotLineGroups iContainsLineGroups)
        {
            foreach (var lineGroup in iContainsLineGroups.LineGroups)
            {
                if (!lineGroup.HasFnacComArticle)
                {
                    continue;
                }

                foreach (var standardArticle in lineGroup.Articles.OfType<StandardArticle>())
                {
                    var shoppingCartLineItem = new DTO.ShoppingCartLineItem
                    {
                        Quantity = standardArticle.Quantity.Value,
                        Reference = new DTO.ArticleReference { PRID = standardArticle.ProductID.Value },
                        ShippingMethod = standardArticle.ShipMethod
                    };

                    // Products

                    if (standardArticle.Services.Count > 0)
                    {
                        foreach (var service in standardArticle.Services)
                        {
                            // Services
                            var serviceline = new DTO.ShoppingCartLineItem
                            {
                                Quantity = service.Quantity.Value,
                                Reference = new DTO.ArticleReference
                                {
                                    PRID = service.ProductID.Value
                                }
                            };
                            context.ShoppingCart.LineItems.Add(serviceline);
                        }
                    }

                    context.ShoppingCart.LineItems.Add(shoppingCartLineItem);
                }
            }
        }

        public void GetServiceLineInfo(DTO.Context ctx, IEnumerable<Article> articles, ShippingAddress adr = null)
        {
            foreach (var art in CollectionUtils.FilterOnType<StandardArticle>(articles))
            {
                var shoppingCartLineItem = new DTO.ShoppingCartLineItem
                {
                    Quantity = art.Quantity.Value,
                    Reference = new DTO.ArticleReference
                    {
                        PRID = art.ProductID.Value
                    },
                    AllowDiscount = art.Type == ArticleType.Saleable,
                    ShippingMethod = art.ShipMethod
                };

                if (art.Services.Count > 0)
                {
                    foreach (var service in art.Services)
                    {
                        // Services
                        var serviceline = new DTO.ShoppingCartLineItem
                        {
                            Quantity = service.Quantity.Value,
                            Reference = new DTO.ArticleReference
                            {
                                PRID = service.ProductID.Value
                            },
                            AllowDiscount = true
                        };


                        ctx.ShoppingCart.LineItems.Add(serviceline);
                    }
                }


                ctx.ShoppingCart.LineItems.Add(shoppingCartLineItem);
            }
        }

        public void GetServiceLineInfoForShop(DTO.Context ctx, IEnumerable<Article> articles, ShippingAddress adr)
        {
            foreach (var art in CollectionUtils.FilterOnType<StandardArticle>(articles))
            {
                var shoppingCartLineItem = new DTO.ShoppingCartLineItem
                {
                    Quantity = art.Quantity.Value,
                    Reference = new DTO.ArticleReference
                    {
                        PRID = art.ProductID.Value
                    },
                    AllowDiscount = true,
                    //si retrait magasin, on force la ShippingMethod
                    ShippingMethod = adr is ShopAddress ? (int)ShippingMethodEnum.RetraitMagasin : art.ShipMethod
                };

                // 0104 UG spécifique contenant les prix blancs uniquement pour les lignes produits 1 (PT)
                Func<string, decimal?> prixBlancDepuisStom = sku =>
                {
                    if (Switch.IsEnabled("read.prix.blanc.from.stom", false))
                    {
                        return _STOMBusiness.GetStockInfoBySKU(sku, "0104").FirstOrDefault(s => s.LigneProduit == "1")?.PrixVenteMag;
                    }
                    else
                    {
                        return null;
                    }
                };

                if (art.Services.Count > 0)
                {
                    foreach (var service in art.Services)
                    {
                        // Services
                        var serviceline = new DTO.ShoppingCartLineItem
                        {
                            Quantity = service.Quantity.Value,
                            Reference = new DTO.ArticleReference
                            {
                                PRID = service.ProductID.Value
                            },
                            AllowDiscount = true
                        };

                        if (service.ShopStocks != null)
                        {
                            serviceline.ShopPrice = service.ShopStocks.PrixVenteMag;
                        }
                        else
                        {
                            var prixBlanc = prixBlancDepuisStom(service.ReferenceGU);
                            if (prixBlanc != null)
                            {
                                serviceline.ShopPrice = prixBlanc;
                            }
                        }
                        ctx.ShoppingCart.LineItems.Add(serviceline);
                    }
                }

                decimal? price = null;
                if (art.ShopStocks != null)
                {
                    price = art.ShopStocks.PrixVenteMag;
                }
                else
                {
                    var prixBlanc = prixBlancDepuisStom(art.ReferenceGU);
                    if (prixBlanc != null)
                    {
                        price = prixBlanc;
                    }
                    if (price == null && art.ArticleDTO != null && art.ArticleDTO.Core.SellingPrice.HasValue && art.ArticleDTO.Core.SellingPrice > 0)
                    {
                        price = art.ArticleDTO.Core.SellingPrice;                        
                    }
                }

                if (price != null)
                {
                    shoppingCartLineItem.ShopPrice = price;
                    shoppingCartLineItem.AllowDiscount = !art.HasRemiseClient;
                    if (art.HasRemiseClient && art.Quantity > 0)
                        shoppingCartLineItem.ShopReducedPrice = price - (art.Remise.MontantRemise / art.Quantity);
                }

                ctx.ShoppingCart.LineItems.Add(shoppingCartLineItem);
            }
        }
        private DTO.Context GetContextInfos(DTO.SiteContext context,
            IEnumerable<Article> articles,
            string advantageCode,
            DTO.Customer dtoCustomer,
            MultiFacet<IGotLineGroups, IGotMobileInformations, IGotBillingInformations> orderFormFacets,
            ShippingAddress shippingAddress,
            DTO.ShoppingCart basket,
            GetArticlesPricesParameters getArticlesPricesParameters)
        {
            var contextInfos = GetLightContextInfos(context, basket);

            if (shippingAddress != null)
            {
                if (shippingAddress is PostalAddress)
                {
                    contextInfos.ShippingCountryCode = ((PostalAddress)shippingAddress).CountryCode2;
                    contextInfos.ZipCode = ((PostalAddress)shippingAddress).ZipCode;
                }
                else if (shippingAddress is RelayAddress)
                {
                    //Relais Colis always in France
                    contextInfos.ShippingCountryCode = "FR";
                    contextInfos.ZipCode = ((RelayAddress)shippingAddress).ZipCode;
                }
                else if (shippingAddress is ShopAddress)
                {
                    //Relais Colis always in France
                    contextInfos.ShippingCountryCode = "FR";
                    contextInfos.ZipCode = ((ShopAddress)shippingAddress).ZipCode;
                }
            }

            if (getArticlesPricesParameters.ForAsilage)
                GetServiceLineInfoPiggyBack(contextInfos, orderFormFacets.Facet1);
            else if (orderFormFacets.GetPrivateData<ShopInfosData>(create: false) != null)
                GetServiceLineInfoForShop(contextInfos, articles, shippingAddress);
            else
                GetServiceLineInfo(contextInfos, articles, shippingAddress);

            var adherentData = orderFormFacets.GetPrivateData<AdherentData>();

            var hasTryCardCookie = adherentData != null && (adherentData.TryCardStatus == TryCardStatus.ToAdd || adherentData.TryCardStatus == TryCardStatus.Added || adherentData.TryCardStatus == TryCardStatus.Validated) ? true : false;

            if ((getArticlesPricesParameters.ForceAdherentSegment || getArticlesPricesParameters.HasUnlimitedSubscription || hasTryCardCookie) && dtoCustomer == null)
            {
                dtoCustomer = new DTO.Customer();
            }

            AddSegments(dtoCustomer, getArticlesPricesParameters, adherentData);

            contextInfos.Customer = dtoCustomer;

            contextInfos.ShoppingCart.ID = new Guid(orderFormFacets.Facet1.SID);

            GetPaymentMethodInfo(ref contextInfos, orderFormFacets.Facet3.OrderBillingMethods);

            if (!string.IsNullOrEmpty(advantageCode))
            {
                if (contextInfos.ShoppingCart.AdvantageCodes == null)
                    contextInfos.ShoppingCart.AdvantageCodes = new List<string>();
                contextInfos.ShoppingCart.AdvantageCodes.Add(advantageCode);
            }

            contextInfos.ShopRetrieval = getArticlesPricesParameters.ShopRetrieval;
            contextInfos.RetrievalChannelString = GetRetrievalChannel(context, orderFormFacets.Facet2, getArticlesPricesParameters.ForcedRetrievalChannel);
            if (string.Equals(contextInfos.RetrievalChannelString, DTO.RetrievalChannelString.RC_SHOP))
            {
                contextInfos.ShopRefGu = getArticlesPricesParameters.ShopRefUg;
            }
            return contextInfos;
        }

        private DTO.Context GetLightContextInfos(DTO.SiteContext context, DTO.ShoppingCart basket)
        {
            var contextInfos = new DTO.Context
            {
                ShoppingCart = new DTO.ShoppingCart { LineItems = new List<DTO.ShoppingCartLineItem>() },
                SiteContext = context
            };

            contextInfos.ShoppingCart.IsGiftOrder = basket.IsGiftOrder;

            contextInfos.RetrievePromotionDeffered = true;

            contextInfos.ApplyBottomBasketPromotions = Switch.IsEnabled("orderpipe.pop.promo.applybottombasketpromotions", false);

            return contextInfos;
        }

        private static void AddSegments(DTO.Customer dtoCustomer, GetArticlesPricesParameters getArticlesPricesParameters, AdherentData adherentData)
        {
            if (dtoCustomer != null && dtoCustomer.Segments == null)
            {
                dtoCustomer.Segments = new DTO.SegmentCollection();
            }

            var has3YearsSegment = (dtoCustomer != null && dtoCustomer.Segments != null && dtoCustomer.Segments.Contains(DTO.CustomerSegment.Member3Years));
            var hasTryCardSegment = (dtoCustomer != null && dtoCustomer.Segments != null && dtoCustomer.Segments.Contains(DTO.CustomerSegment.MemberTryCard));

            if (getArticlesPricesParameters.HasUnlimitedSubscription && !dtoCustomer.Segments.Contains(DTO.CustomerSegment.UnlimitedSubscription))
                dtoCustomer.Segments.AddCustomerSegment(DTO.CustomerSegment.UnlimitedSubscription);

            if (getArticlesPricesParameters.ForceAdherentSegment && !has3YearsSegment)
                dtoCustomer.Segments.AddCustomerSegment(DTO.CustomerSegment.Member3Years);

            if (adherentData != null)
            {
                var alreadyAdherent = adherentData.Has1YearCard || adherentData.Has3YearsCard || adherentData.IsYoungPrice;
                if (!alreadyAdherent && (adherentData.TryCardStatus == TryCardStatus.ToAdd || adherentData.TryCardStatus == TryCardStatus.Added || adherentData.TryCardStatus == TryCardStatus.Validated) && !hasTryCardSegment && !has3YearsSegment)
                {
                    //To-Do retirer le segment 3 ans, une fois que les devs sur la récupération du segment côté web business seront finis.
                    dtoCustomer.Segments.AddCustomerSegment(DTO.CustomerSegment.MemberTryCard);
                }
                if ((adherentData.TryCardStatus == TryCardStatus.ToRemove || adherentData.TryCardStatus == TryCardStatus.Removed) && hasTryCardSegment)
                {
                    //To-Do retirer le segment 3 ans, une fois que les devs sur la récupération du segment côté web business seront finis.
                    var trySegment = dtoCustomer.Segments.FirstOrDefault(seg => seg.Key == DTO.CustomerSegment.MemberTryCard.ToString());
                    dtoCustomer.Segments.Remove(trySegment);
                }
            }
        }

        private void GetPaymentMethodInfo(ref DTO.Context ctx, IEnumerable<BillingMethod> billingMethods)
        {
            var lpm = new List<DTO.PaymentMethod>();

            foreach (var billing in billingMethods)
            {
                var methodofpayment = new DTO.PaymentMethod();


                //Payment by credit card
                if (billing is CreditCardBillingMethod bccard)
                {
                    methodofpayment.Id = bccard.TypeId.GetValueOrDefault();
                    methodofpayment.Type = DTO.PaymentType.CreditCard;
                }
                //Payment by payment method
                else
                {
                    methodofpayment.Id = billing.BillingMethodId;
                    methodofpayment.Type = DTO.PaymentType.PaymentMethod;
                }

                lpm.Add(methodofpayment);
            }

            ctx.ShoppingCart.PaymentMethod = new List<DTO.PaymentMethod>();
            ctx.ShoppingCart.PaymentMethod = lpm;
        }


        public decimal GetRoundedPrice(decimal priceRealEur)
        {
            return PriceHelpers.GetRoundedPrice(priceRealEur);
        }
    }
}
