﻿using FnacDirect.OrderPipe.Base.Model;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Common.Shipping
{
    public static class PostalAddressExtensions
    {
        public static AddressViewModel ConvertToAddressViewModel(this PostalAddress postalAddress, int countyId, bool hasMissingInformations = false)
        {
            var addressViewModel = new AddressViewModel(postalAddress)
            {
                ShouldDisplayCountry = countyId != postalAddress.CountryId
            };

            if (addressViewModel.ShouldDisplayCountry)
            {
                addressViewModel.Country = postalAddress.CountryLabel;
            }

            addressViewModel.HasMissingInformations = hasMissingInformations;

            return addressViewModel;
        }
    }
}
