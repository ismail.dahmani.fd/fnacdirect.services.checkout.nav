using System.Collections.Generic;
using System.Linq;

namespace FnacDirect.OrderPipe.Base.Model
{
    public static class ArticleExtensions
    {
        public static T GetByPrid<T>(this IEnumerable<Article> articles, int prid)
            where T : Article
        {
            return articles.OfType<T>().Where(a => a.ProductID.HasValue).FirstOrDefault(a => a.ProductID.Value == prid);
        }

        public static bool AreAllEbooks(this IEnumerable<Article> articles)
        {
            var standardArticles = articles.OfType<StandardArticle>();
            return standardArticles.Any() && standardArticles.All(a => a.IsEbook);
        }

        /// <summary>
        /// Vérifie si nous avons un lien entre la liste des services et l'article passé en paramètre
        /// </summary>
        /// <param name="stdArticle">L'article à tester</param>
        public static bool HasServiceForArticle(this IEnumerable<Article> articles, StandardArticle stdArticle)
        {
            return articles.OfType<Service>().Any(service => service.MasterArticles.Any(master => master.ID == stdArticle.ID));
        }
    }
}
