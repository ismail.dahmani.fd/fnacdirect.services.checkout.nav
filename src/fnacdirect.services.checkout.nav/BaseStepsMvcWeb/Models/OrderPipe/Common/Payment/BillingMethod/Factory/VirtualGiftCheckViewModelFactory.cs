using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.OrderPipe.Core.Services;
using FnacDirect.Technical.Framework.Web.Mvc;
using System;
using System.Linq;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Common.Payment.BillingMethodFactories
{
    public class VirtualGiftCheckViewModelFactory : BasePaymentViewModelFactory<VirtualGiftCheckBillingMethod>
    {
        private readonly IPriceHelper _priceHelper;
        private IPipeParametersProvider _pipeParametersProvider;
        private ISwitchProvider _switchProvider;

        public VirtualGiftCheckViewModelFactory(IApplicationContext applicationContext,
                                                IPipeParametersProvider pipeParametersProvider,
                                                ISwitchProvider switchProvider)
        {
            _priceHelper = applicationContext.GetPriceHelper();
            _pipeParametersProvider = pipeParametersProvider;
            _switchProvider = switchProvider;
        }

        public override BillingMethodViewModel Get(string tabName, PaymentMethod paymentMethod, OPContext opContext, PopOrderForm popOrderForm)
        {
            var ccvAccountBillingData = popOrderForm.GetPrivateData<CCVAccountBillingData>("ccvdata");

            var isGuest = popOrderForm.UserContextInformations.IsGuest;

            var viewModel = new VirtualGiftCheckViewModel()
            {
                AccountBalance = _priceHelper.Format(ccvAccountBillingData.CCVAccountBalance, PriceFormat.Default),
                HasAccountBalance = ccvAccountBillingData.CCVAccountBalance > 0
            };

            // Maximum allowed amount
            if (TryGetMaximumAllowedAmountForClickAndCollect(popOrderForm, out var maximumAllowedAmound))
            {
                viewModel.HasMaximumAllowedAmountForClickAndCollect = true;
                viewModel.MaximumAllowedAmountForClickAndCollect = maximumAllowedAmound;
            }

            var virtualGiftCheckBillingMethod = popOrderForm.OrderBillingMethods.OfType<VirtualGiftCheckBillingMethod>().FirstOrDefault();

            if (virtualGiftCheckBillingMethod != null)
            {
                viewModel.Active = true;

                var usedAmount = virtualGiftCheckBillingMethod.Amount.GetValueOrDefault(0);

                viewModel.UsableAmount = _priceHelper.Format(usedAmount, PriceFormat.Default);

                if (usedAmount < ccvAccountBillingData.CCVAccountBalance)
                {
                    var remainingBalance = ccvAccountBillingData.CCVAccountBalance - usedAmount;
                    viewModel.RemainingBalance = _priceHelper.Format(remainingBalance, PriceFormat.Default);
                    viewModel.HasRemainingBalance = remainingBalance > 0;
                }
            }
            else
            {
                var usableAmount = ccvAccountBillingData.CCVAccountBalance;

                if (viewModel.HasMaximumAllowedAmountForClickAndCollect)
                {
                    usableAmount = Math.Min(usableAmount, maximumAllowedAmound.Value);
                }

                var paymentSelectionData = popOrderForm.GetPrivateData<PaymentSelectionData>("payment");

                var accountBalance = paymentSelectionData.RemainingAmount;
                usableAmount = Math.Min(usableAmount, accountBalance);

                viewModel.UsableAmount = _priceHelper.Format(usableAmount, PriceFormat.Default);
            }

            viewModel.Display = !isGuest;

            return viewModel;
        }

        private bool TryGetMaximumAllowedAmountForClickAndCollect(PopOrderForm popOrderForm, out decimal? maximumAllowedAmount)
        {
            // Si C&C
            if (_switchProvider.IsEnabled("ccv.constraint.clickandcollect.maxamount.enabled")
                && popOrderForm.HasClickAndCollectArticle())
            {
                maximumAllowedAmount = _pipeParametersProvider.Get("ccv.constraint.clickandcollect.maxamount", int.MaxValue);
                return true;
            }

            maximumAllowedAmount = null;
            return false;
        }
    }
}
