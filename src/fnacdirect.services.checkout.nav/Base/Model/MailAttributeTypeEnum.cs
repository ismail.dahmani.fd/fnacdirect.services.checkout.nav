namespace FnacDirect.OrderPipe.Base.Model
{
    public enum MailAttributeTypeEnum : int
    {
        SMS_NEWSLETTER = 140,
        SMS_PRO_MAIL_ATTRIBUTE = 10020
    }
}
