﻿using FnacDirect.OrderPipe.Base.BaseModel.Model.MarketPlace;
using FnacDirect.OrderPipe.Base.Model;
using TFrom = FnacDirect.ShoppingCartEntities.MarketPlaceArticle;
using TTo = FnacDirect.OrderPipe.Base.Model.MarketPlaceArticle;

namespace FnacDirect.OrderPipe.Base.Business.ArticleConversions.ShoppingCartToOrderForm
{
    public partial class ShoppingCartToOrderFormArticleConverter
    {
        protected TTo Convert(TFrom @from)
        {
            var art = new TTo
            {
                InsertDate = @from.InsertDate,
                OriginCode = @from.OriginCode,
                ProductID = @from.ProductID,
                Quantity = @from.Quantity.GetValueOrDefault(1),
                OfferId = @from.OfferId,
                Offer = @from.Offer,
                Referentiel = @from.Referentiel
            };

            if (@from.ReservationId.HasValue)
            {
                art.Reservation = new MarketPlaceReservation
                {
                    Id = @from.ReservationId.GetValueOrDefault(),
                    Quantity = @from.ReservationQuantity.GetValueOrDefault()
                };
            }
            else
            {
                art.Reservation = null;
            }

            art.OldPrice = @from.OldPrice;
            art.OldPriceRecordedDate = @from.OldPriceRecordedDate;

            return art;
        }
    }
}
