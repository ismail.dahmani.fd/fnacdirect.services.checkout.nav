using FnacDirect.OrderPipe.Base.Business.LogisticLineGroups.Internals.InitialSelectors;
using FnacDirect.OrderPipe.Base.Business.Payment;
using FnacDirect.OrderPipe.Base.LineGroups;
using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.OrderPipe.Base.Model.LineGroups;
using FnacDirect.OrderPipe.Core.Services;
using System.Collections.Generic;
using System.Linq;

namespace FnacDirect.OrderPipe.Base.Business.LogisticLineGroups.InitialSelectors
{
    public class ECCVLineGroupInitialSelector : BaseLineGroupInitialSelector
    {
        private readonly ICheckArticleService _checkArticleService;

        public ECCVLineGroupInitialSelector(ICheckArticleService checkArticleService)
        {
            _checkArticleService = checkArticleService;
        }

        public override IDictionary<ILineGroupHeader, IEnumerable<Article>> Select(IEnumerable<ILineGroup> lineGroups)
        {
            var output = new Dictionary<ILineGroupHeader, IEnumerable<Article>>();

            foreach (var lineGroup in lineGroups)
            {
                var eccvs = _checkArticleService.GetECCVs(lineGroup.Articles);
                if (eccvs.Any())
                {
                    output.Add(lineGroup, eccvs.ToList());
                }
            }

            return output;
        }
    }
}
