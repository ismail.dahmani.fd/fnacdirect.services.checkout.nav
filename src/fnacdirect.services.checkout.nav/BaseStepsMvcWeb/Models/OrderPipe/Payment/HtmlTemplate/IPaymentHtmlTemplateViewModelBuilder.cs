﻿using FnacDirect.OrderPipe.Base.Model;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Payment
{
    public interface IPaymentHtmlTemplateViewModelBuilder
    {
        PaymentHtmlTemplateViewModel Build(PopOrderForm popOrderForm, RootViewModel rootViewModel);
    }
}
