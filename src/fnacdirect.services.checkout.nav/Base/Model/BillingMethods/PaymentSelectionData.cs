using System.Collections.Generic;
using System.Xml.Serialization;

namespace FnacDirect.OrderPipe.Base.Model
{
    public class PaymentSelectionData : GroupData
    {
        public PaymentSelectionData()
        {
            RemainingMethods = new List<BillingMethodDescriptor>();
        }

        [XmlIgnore]
        public int AllowedMethodMask { get; set; }
        [XmlIgnore]
        public int RemainingMethodMask { get; set; }
        [XmlIgnore]
        public List<BillingMethodDescriptor> RemainingMethods { get; set; }
        [XmlIgnore]
        public decimal RemainingAmount { get; set; }
        [XmlIgnore]
        public bool Refresh { get; set; }
    }
}
