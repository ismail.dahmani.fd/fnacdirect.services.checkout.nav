﻿namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Shipping.ShipToStore
{
    public class StoreArticleAvailabilityViewModel
    {
        public int Quantity { get; set; }

        public string Title { get; set; }
    }
}