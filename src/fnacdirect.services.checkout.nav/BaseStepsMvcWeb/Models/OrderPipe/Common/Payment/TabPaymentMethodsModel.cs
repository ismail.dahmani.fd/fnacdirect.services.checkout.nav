using System.Collections.Generic;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Common.Payment
{
    public class TabPaymentMethodsModel
    {
        public string Name { get; set; }

        private List<BillingMethodViewModel> _billingMethods = new List<BillingMethodViewModel>();

        public IReadOnlyCollection<BillingMethodViewModel> BillingMethods
        {
            get
            {
                return _billingMethods.AsReadOnly();
            }
        }

        public void AddBillingMethod(BillingMethodViewModel billingMethodViewModel)
        {
            _billingMethods.Add(billingMethodViewModel);
        }

        public void RemoveBillingMethod(BillingMethodViewModel billingMethodViewModel)
        {
            _billingMethods.Remove(billingMethodViewModel);
        }

        public bool IsSelected { get; set; }

        public string Type { get; internal set; }

       
    }
}
