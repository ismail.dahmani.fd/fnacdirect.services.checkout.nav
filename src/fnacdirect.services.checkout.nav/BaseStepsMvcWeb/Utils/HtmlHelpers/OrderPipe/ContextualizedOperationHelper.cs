using FnacDirect.OrderPipe.Base.ContextualizedOperation;
using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.OrderPipe.Base.Model.Pop;
using System;
using System.Web.Mvc;

namespace FnacDirect.OrderPipe.BaseMvc.Web.HtmlHelpers.Pop
{
    public static class ContextualizedOperationHelper
    {
        public static bool IsContextualizedOperation(this HtmlHelper htmlHelper, string operationPublicId)
        {
            var opContext = DependencyResolver.Current.GetService<OPContext>();
            if (opContext != null)
            {
                if (opContext.OF is PopOrderForm popOf)
                {
                    var privateData = popOf.GetPrivateData<ContextualizedOperationData>(false);
                    if (privateData != null)
                    {
                        return string.Compare(privateData.PublicId, operationPublicId, StringComparison.InvariantCultureIgnoreCase) == 0;
                    }
                }
            }

            return false;
        }
    }
}
