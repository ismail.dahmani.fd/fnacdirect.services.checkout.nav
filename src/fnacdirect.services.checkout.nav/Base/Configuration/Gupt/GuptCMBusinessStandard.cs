﻿using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.OrderPipe.Base.Proxy;
using FnacDirect.OrderPipe.CM.Business;
using FnacDirect.Technical.Framework.Utils;
using FnacDirect.Technical.Framework.Web.Mvc;

namespace FnacDirect.OrderPipe.FDV.Business
{
    public class GuptCMBusinessStandard : GuptCMBusiness
    {
        protected override string NRCode
        {
            get { return "995"; }
        }

        public GuptCMBusinessStandard(IClickAndMagService clickAndMagService)
            : base(clickAndMagService)
        { }
    }
}
