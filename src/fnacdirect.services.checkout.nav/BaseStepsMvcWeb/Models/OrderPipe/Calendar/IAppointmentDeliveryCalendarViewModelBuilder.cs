using FnacDirect.OrderPipe.Base.Business.Shipping;
using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.OrderPipe.Base.Model.Solex;
using System;
using System.Collections.Generic;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Solex.Appointment
{
    public interface IAppointmentDeliveryCalendarViewModelBuilder
    {
        AppointmentDeliveryCalendarViewModel BuildAppointmentDeliveryCalendarViewModel(int currentPageIndex, DateTime startDate, DateTime endDate, List<DeliverySlot>[] sortedList, List<Article> articles, DeliverySlotData deliverySlotData);

        AppointmentDeliveryCalendarViewModelBuilderResult BuildAppointmentDeliveryCalendarViewModel(PopOrderForm popOrderForm, int sellerId, HashSet<int> shippingMethods, int pageIndex, int pageSize, string sortingCriteria);
        AppointmentDeliveryCalendarViewModelBuilderResult BuildAppointmentDeliveryCalendarViewModel(PopOrderForm popOrderForm, int sellerId, IEnumerable<Article> articles, HashSet<int> shippingMethods, int pageIndex, int pageSize, string sortingCriteria);
    }
}
