namespace FnacDirect.OrderPipe.Base.BaseModel.Model
{
    public class Partner
    {
        public int AccountId { get; set; }   
        public string ArticleNodes { get; set; }
        public string OutputPath { get; set; }
        public string AdvantageCodes { get; set; }
        public bool AllowAvailabilityOverriding { get; set; }
        public bool AllowBillingAddressOverriding { get; set; }
        public int? OrderType { get; set; }
        public bool AllowPricerPriceOverriding { get; set; }
        public bool AllowSelectiveDistribution { get; set; }
        public bool AllowMultipleExternalReference { get; set; }
    }
}
