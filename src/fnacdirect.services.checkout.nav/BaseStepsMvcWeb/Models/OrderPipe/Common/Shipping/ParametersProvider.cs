using FnacDirect.OrderPipe.Config;
using System;
using System.Collections.Generic;
using FnacDirect.Technical.Framework.Utils;

namespace FnacDirect.OrderPipe.Core.Services
{
    public class ParametersProvider : IPipeParametersProvider
    {
        private readonly ParameterList _parameterList;

        public ParametersProvider(ParameterList parameterList)
        {
            _parameterList = parameterList;
        }

        public T Get<T>(string key)
        {
            return _parameterList.Get<T>(key);
        }

        public T Get<T>(string key, T defaultValue)
        {
            return _parameterList.Get(key, defaultValue);
        }

        public IDictionary<string, T> GetAsDictionary<T>(string key) where T : IComparable<T>
        {
            return _parameterList.GetAsDictionary<T>(key);
        }

        public IEnumerable<T> GetAsEnumerable<T>(string key) where T : IComparable<T>
        {
            return _parameterList.GetAsEnumerable<T>(key);
        }

        public RangeSet<T> GetAsRangeSet<T>(string key) where T : IComparable<T>
        {
            return _parameterList.GetAsRangeSet<T>(key);
        }
    }
}
