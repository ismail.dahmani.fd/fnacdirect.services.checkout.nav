using FnacDirect.OrderPipe.Base.BaseModel.Model;
using System.Collections.Generic;

namespace FnacDirect.OrderPipe.Base.Proxy
{
    public interface ICustomerContext
    {
        int GetUserId();
        string GetUserReference();
        FnacDirect.Contracts.Online.Model.Customer GetContractDTO();
        List<State> GetState();
    }
}
