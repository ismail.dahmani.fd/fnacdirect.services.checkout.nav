using FnacDirect.OrderPipe.Base.Model;

namespace FnacDirect.OrderPipe.Base.Business.Payment
{
    public interface ICreditCardTransactionBusiness
    {
        bool MustAddMainOrderTransaction(OgoneCreditCardBillingMethod billingMethod);
        bool CanSetOrderTransactionStatusOnInitialized(OgoneCreditCardBillingMethod billingMethod);
    }
}
