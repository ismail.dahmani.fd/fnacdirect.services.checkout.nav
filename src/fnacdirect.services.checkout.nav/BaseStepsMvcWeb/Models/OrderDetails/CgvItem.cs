﻿using System.Xml.Serialization;

namespace FnacDirect.OrderPipe.Base.Business.OrderDetail
{
    public class CgvItem
    {
        [XmlAttribute]
        public bool Value { get; set; }

        [XmlAttribute]
        public string CgvUrl { get; set; }
    }
}
