using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.OrderPipe.Base.Model.LineGroups;
using System.Collections.Generic;
using System.Linq;

namespace FnacDirect.OrderPipe
{
    public static class LineGroupExtensions
    {
        public static bool Modified(this ILineGroup lineGroup)
        {
            return lineGroup.Articles.Modified;
        }

        public static IEnumerable<ILineGroup> Seller(this IEnumerable<ILineGroup> lineGroups, int sellerId)
        {
            return lineGroups.Where(lg => lg.Seller.SellerId == sellerId);
        }

        public static IEnumerable<ILineGroup> SubSeller(this IEnumerable<ILineGroup> lineGroups, int subSellerId)
        {
            return lineGroups.Where(lg => lg.Seller.SubSellerId == subSellerId);
        }
        public static IEnumerable<int> Sellers(this IEnumerable<ILineGroup> lineGroups)
        {
            return lineGroups.SellerInformations().Select(s => s.SellerId).Distinct();
        }

        public static IEnumerable<SellerInformation> SellerInformations(this IEnumerable<ILineGroup> lineGroups)
        {
            return lineGroups.Select(lg => lg.Seller).OrderBy(s => s.SellerId);
        }
    }
}
