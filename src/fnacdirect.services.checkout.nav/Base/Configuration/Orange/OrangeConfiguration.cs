using System.Collections.Generic;


namespace FnacDirect.OrderPipe.Base.Business.Configuration
{
    public class OrangeConfiguration
    {
        public string OrangeApiUrl { get; set; }
        public string ProxyOrangeApi { get; set; }
        public string SqlCommerce { get; set; }
    }
}
