﻿using System.Collections.Generic;
using System.Linq;
using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.OrderPipe.BaseMvc.Web.Services.Pop.Payment;
using FnacDirect.Technical.Framework.Web.Mvc;
using FnacDirect.Membership.Model.Constants;
using FnacDirect.OrderPipe.Base.BaseModel.Model;
using FnacDirect.OrderPipe.Base.Proxy.Customer;
using FnacDirect.OrderPipe.Base.Business;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Common.Payment.BillingMethodFactories
{
    public class UnicreViewModelFactory : BaseCreditCardViewModelFactory<UnicreCreditCardBillingMethod>
    {
        private readonly ISwitchProvider _switchProvider;

        public UnicreViewModelFactory(IApplicationContext applicationContext, 
                                      IPaymentMethodService paymentMethodService,
                                     ICreditCardDescriptorBusiness creditCardDescriptorBusiness,
                                     ICustomerCreditCardService customerCreditCardService,
                                      ISwitchProvider switchProvider) 
            : base(applicationContext, creditCardDescriptorBusiness, customerCreditCardService, paymentMethodService)
        {
            _switchProvider = switchProvider;
        }

        public override BillingMethodViewModel Get(string tabName, PaymentMethod paymentMethod, OPContext opContext, PopOrderForm popOrderForm)
        {
            var hasMembershipSouscriptionCardInBasket = popOrderForm.LineGroups.GetArticles()
            .OfType<StandardArticle>()
            .Any(
                art => art.TypeId.GetValueOrDefault() == (int)CategoryCardType.Recruit || art.TypeId.GetValueOrDefault() == (int)CategoryCardType.Renew
            );

            var viewModel = new UnicreBillingMethodViewModel();

            CreditCardBillingMethod(viewModel, popOrderForm, tabName, paymentMethod);

            var aboIlliData = popOrderForm.GetPrivateData<AboIlliData>("AboIlliGroup");

            viewModel.DisplayTacitAgreement = hasMembershipSouscriptionCardInBasket || aboIlliData.HasAboIlliInBasket;

            IEnumerable<CustomerCreditCardViewModel> customerCreditCards = new List<CustomerCreditCardViewModel>();
            if (!_switchProvider.IsEnabled("orderpipe.pop.payment.option.clicinstore") || !IsClicInStore(popOrderForm.UserContextInformations.ContactEmail))
            {
                customerCreditCards = GetCustomerCreditCards(viewModel, popOrderForm.UserContextInformations.UID);
            }

            viewModel.AddCustomerCreditCards(customerCreditCards);

            return viewModel;
        }
    }
}
