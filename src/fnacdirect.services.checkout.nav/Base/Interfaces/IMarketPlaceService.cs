﻿using FnacDirect.OrderPipe.Base.Model;

namespace FnacDirect.OrderPipe.Base.Business
{
    public interface IMarketPlaceService
    {
        void AddReservation(MarketPlaceArticle art, int timeoutInSecond);
        void CancelReservation(MarketPlaceArticle art);
        void RefreshReservation(MarketPlaceArticle art, int timeoutInSecond);
        void ValidateReservation(MarketPlaceArticle art);
        bool GetArticleDetail(MarketPlaceArticle art);
    }
}
