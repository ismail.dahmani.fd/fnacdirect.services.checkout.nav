using System;
using System.Collections.Generic;
using System.Linq;
using FnacDirect.Technical.Framework.Web.Helpers;

namespace FnacDirect.OrderPipe.Base.Business.Operations
{

    /// <summary>
    /// Wrap the Christmas configuration
    /// </summary>
    public class DeliveredForChrismasConfigurationWrapper
    {
        private readonly DeliveredForChrismasConfiguration _deliveredForChrismasConfiguration;
        private readonly IContextSimulator _contextSimulator;
        private const string PhaseDateParamName = "mo-phase-date";
        private const string EventDateParamName = "mo-event-date";

        private const string EventDateSimulatorSwitchId = "orderpipe.pop.context.simulator.christmas";

        public DeliveredForChrismasConfigurationWrapper(IContextSimulator contextSimulator, DeliveredForChrismasConfiguration deliveredForChrismasConfiguration)
        {
            _contextSimulator = contextSimulator;
            _deliveredForChrismasConfiguration = deliveredForChrismasConfiguration;
        }

        /// <summary>
        /// Gets current phase
        /// </summary>
        public DeliveredForChrismasRule CurrentPhase
        {
            get
            {
                var eventDate = _contextSimulator.FromContext(PhaseDateParamName,
                                                              DateTime.Now,
                                                              EventDateSimulatorSwitchId);

                return _deliveredForChrismasConfiguration?.DisplayRules?.FirstOrDefault(o => o.StartDate <= eventDate && o.EndDate >= eventDate);
            }
        }

        /// <summary>
        /// Event date of the current phase
        /// </summary>
        public DateTime EventDate
        {
            get
            {
                return _contextSimulator.FromContext(EventDateParamName,
                                                     () => CurrentPhase.EventDate,
                                                     EventDateSimulatorSwitchId);
            }
        }

        /// <summary>
        /// Check if we are in a phase
        /// </summary>
        public bool HasCurrentPhase => CurrentPhase != null;

        /// <summary>
        /// Check if at least one phase is configured
        /// </summary>
        public bool HasRules => (_deliveredForChrismasConfiguration?.DisplayRules?.Any()).GetValueOrDefault();

        /// <summary>
        /// Cehck if the the availability is allowed
        /// </summary>
        /// <param name="availability"></param>
        /// <returns></returns>
        public bool IsElligibleAvailability(int availability)
        {
            return HasCurrentPhase && ElligibleAvailabilities.Contains(availability);
        }

        /// <summary>
        /// Check id the family type is excluded
        /// </summary>
        /// <param name="itemFamilyType">The family type id of the article</param>
        /// <returns></returns>
        public bool IsExcludedFamily(int itemFamilyType)
        {
            return (_deliveredForChrismasConfiguration.ExcludeFamilies?.Contains(itemFamilyType)).GetValueOrDefault();
        }

        /// <summary>
        /// Check if the catalog is allowed (1 : Fnac, 3 : MarketPlace)
        /// </summary>
        /// <param name="catalog"></param>
        /// <returns></returns>
        public bool IsAllowedCatalog(int catalog)
        {
            return (_deliveredForChrismasConfiguration?.ElligibleCatalogs?.Contains(catalog)).GetValueOrDefault();
        }

        /// <summary>
        /// Start split date
        /// </summary>
        public DateTime? SplitStartDate => _deliveredForChrismasConfiguration?.SplitStartDate;

        /// <summary>
        /// End spluit date
        /// </summary>
        public DateTime? SplitEndDate => _deliveredForChrismasConfiguration?.SplitEndDate;

        public IEnumerable<int> ElligibleAvailabilities => CurrentPhase?.ElligibleAvailibilityIds;


        public IEnumerable<int> ElligibleShippingMethods => CurrentPhase?.ElligibleShippingMethods;
    }
}
