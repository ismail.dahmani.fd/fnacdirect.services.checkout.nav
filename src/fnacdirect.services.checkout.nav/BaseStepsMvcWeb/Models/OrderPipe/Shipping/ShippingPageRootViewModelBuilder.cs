using FnacDirect.Contracts.Online.Model;
using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.OrderPipe.Base.Steps.Pop;
using FnacDirect.OrderPipe.BaseMvc.Web.ApiControllers.Pop;
using FnacDirect.OrderPipe.BaseMvc.Web.Controllers.Pop;
using FnacDirect.OrderPipe.BaseMvc.Web.Models;
using FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Common.Basket;
using FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Common.Payment;
using FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Common.Shipping;
using FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Membership;
using FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Shipping;
using FnacDirect.OrderPipe.BaseMvc.Web.Pop.Models.Common;
using FnacDirect.Technical.Framework.Web.Mvc;
using System;
using System.Linq;
using System.Web.Routing;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Pop.Models.Shipping
{
    public class ShippingPageRootViewModelBuilder : IShippingPageRootViewModelBuilder
    {
        private readonly OPContext _opContext;
        private readonly IApplicationContext _applicationContext;
        private readonly IShippingViewModelBuilder _shippingViewModelBuilder;
        private readonly IBasketViewModelBuilder _basketViewModelBuilder;
        private readonly IOrderPipeUrlBuilder _orderPipeUrlBuilder;
        private readonly IMembershipViewModelBuilder _membershipViewModelBuilder;
        private readonly ISummaryViewModelBuilder _summaryViewModelBuilder;
        private readonly IRemindersViewModelBuilder _remindersViewModelBuilder;
        private readonly IPopBaseRootViewModelBuilder<RootViewModel> _popBaseRootViewModelBuilder;
        private readonly ISwitchProvider _switchProvider;

        public ShippingPageRootViewModelBuilder(OPContext opContext,
                                                IApplicationContext applicationContext,
                                                IShippingViewModelBuilder shippingViewModelBuilder,
                                                IBasketViewModelBuilder basketViewModelBuilder,
                                                IOrderPipeUrlBuilder orderPipeUrlBuilder,
                                                IMembershipViewModelBuilder membershipViewModelBuilder,
                                                ISummaryViewModelBuilder summaryViewModelBuilder,
                                                IRemindersViewModelBuilder remindersViewModelBuilder,
                                                IPopBaseRootViewModelBuilder<RootViewModel> popBaseRootViewModelBuilder,
                                                ISwitchProvider switchProvider)
        {
            _opContext = opContext;
            _applicationContext = applicationContext;
            _shippingViewModelBuilder = shippingViewModelBuilder;
            _basketViewModelBuilder = basketViewModelBuilder;
            _orderPipeUrlBuilder = orderPipeUrlBuilder;
            _membershipViewModelBuilder = membershipViewModelBuilder;
            _summaryViewModelBuilder = summaryViewModelBuilder;
            _remindersViewModelBuilder = remindersViewModelBuilder;
            _popBaseRootViewModelBuilder = popBaseRootViewModelBuilder;
            _switchProvider = switchProvider;
        }

        public RootViewModel GetRootViewModel(PopOrderForm popOrderForm, int sellerId, Func<string, RouteValueDictionary, string> getUrl)
        {
            var popShippingData = popOrderForm.GetPrivateData<PopShippingData>();

            var popShippingSellerData = popShippingData.Sellers.FirstOrDefault(x => x.SellerId == sellerId);

            if (popShippingSellerData == null)
            {
                return null;
            }

            var model = _popBaseRootViewModelBuilder.Build(getUrl);

            model.PageName = PopApiEnvironments.ShippingPage;
            model.IsValid = _shippingViewModelBuilder.IsValid(popOrderForm, popShippingSellerData.SellerId);
            model.Seller = _basketViewModelBuilder.BuildSellerViewModel(popOrderForm, popShippingSellerData);
            model.Basket = _basketViewModelBuilder.BuildBasketViewModel(popOrderForm, popShippingSellerData);
            model.Membership = _membershipViewModelBuilder.Build(popOrderForm);
            model.Shipping = _shippingViewModelBuilder.Build(popOrderForm, popShippingSellerData.SellerId);
            model.Navigation = GetNavigationViewModel(popOrderForm, popShippingSellerData);
            model.Reminders = _remindersViewModelBuilder.Build(popOrderForm);
            model.AdjustedMembershipCard = _membershipViewModelBuilder.BuildAdjustedCard(popOrderForm);
            model.PaymentSummary = _summaryViewModelBuilder.Build(_opContext, popOrderForm);
            if (!model.Seller.IsFnac && model.Shipping.ExpressPlusInfo != null)
            {
                model.Shipping.ExpressPlusInfo.ShowInShipping = false;
            }

            if (MemberHasMissingInfo(popOrderForm))
            {
                model.MemberBirthDate = _membershipViewModelBuilder.BuildMemberBirthDateViewModel(_opContext, popOrderForm);
            }

            return model;
        }

        private bool MemberHasMissingInfo(PopOrderForm popOrderForm)
        {
            var hasAdherentCardInBasket = popOrderForm.LineGroups.GetArticles().OfType<StandardArticle>().Any(art => art.IsAdhesionCard);

            return (hasAdherentCardInBasket && popOrderForm.UserContextInformations.CustomerEntity != null && popOrderForm.UserContextInformations.CustomerEntity.Birthdate.Equals(new DateTime(1900, 1, 1)))
                || ((hasAdherentCardInBasket) && _switchProvider.IsEnabled("orderpipe.pop.adhesion.enable.espopin"));
        }

        private NavigationViewModel GetNavigationViewModel(PopOrderForm popOrderForm, PopShippingSellerData currentPopShippingSellerData)
        {
            var navigationViewModel = new NavigationViewModel();

            var popShippingData = popOrderForm.GetPrivateData<PopShippingData>();

            var nextPopShippingSellerData = popShippingData.Sellers.Where(s => !s.Equals(currentPopShippingSellerData))
                                                                   .FirstOrDefault(s => s.Forced || !s.UserValidated);

            if (nextPopShippingSellerData != null)
            {
                navigationViewModel.HasNextSeller = true;
            }

            if (popShippingData.Sellers.Count > 1)
            {
                var indexOfCurrentSeller = popShippingData.Sellers.IndexOf(currentPopShippingSellerData);

                if (indexOfCurrentSeller > 0)
                {
                    var previousPopShippingSellerData = popShippingData.Sellers[indexOfCurrentSeller - 1];

                    if (previousPopShippingSellerData != null)
                    {
                        var popShippingRouteValueProvider = new PopShippingRouteValueProvider();
                        var routeValues = popShippingRouteValueProvider.GetRouteValueFor(popOrderForm, previousPopShippingSellerData);

                        navigationViewModel.BackLinkUrl = _orderPipeUrlBuilder.GetUrl("DisplayShippingAddress", routeValues);
                    }
                }
            }

            if (string.IsNullOrEmpty(navigationViewModel.BackLinkUrl))
            {
                // lien vers étape panier
                navigationViewModel.BackLinkUrl = _orderPipeUrlBuilder.GetUrl("DisplayBasket");
            }

            return navigationViewModel;
        }
    }
}
