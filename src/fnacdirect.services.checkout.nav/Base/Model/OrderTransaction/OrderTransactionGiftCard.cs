using System;


namespace FnacDirect.OrderPipe.Base.Model
{
    public class OrderTransactionGiftCard
    {
        public Guid Reference { get; set; }

        public int? OrderId { get; set; }

        /// <summary>
        /// Montant du paiement par Carte cadeau (montant cumulé si plusieurs paiements)
        /// </summary>
        public string Amount { get; set; }

        /// <summary>
        /// Format json contenant l'ensemble des transactions
        /// </summary>
        public string PaymentsDetails { get; set; }
    }
}
