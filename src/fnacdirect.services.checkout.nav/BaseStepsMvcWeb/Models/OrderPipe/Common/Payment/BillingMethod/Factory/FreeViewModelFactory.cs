﻿using FnacDirect.OrderPipe.Base.Model;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Common.Payment.BillingMethodFactories
{
    public class FreeViewModelFactory : BasePaymentViewModelFactory<FreeBillingMethod>
    {
        public override BillingMethodViewModel Get(string tabName, PaymentMethod paymentMethod, OPContext opContext, PopOrderForm popOrderForm)
        {
            return new BillingMethodViewModel()
            {
                Display = true
            };
        }
    }
}
