using FnacDirect.OrderPipe.BaseMvc.Web.Pop.Models.Common;
using System.Collections.Generic;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Common.Shipping.Postal
{
    public class PostalDetailsViewModel : AddressViewModel, ISelectedShippingItem, IBaseApiRootViewModel
    {
        public PostalDetailsViewModel(AddressViewModel addressViewModel,
                                      bool isDummy,
                                      bool hasManyEligibleBillingAddresses)
            : base(addressViewModel)
        {
            IsDummy = isDummy;
            HasManyEligibleBillingAddresses = hasManyEligibleBillingAddresses;
        }

        public bool IsDummy { get; set; }

        public bool HasManyEligibleBillingAddresses { get; set; }

        public string Key
        {
            get
            {
                return "Postal";
            }
        }

        public SelectedShippingItemSummary Summary { get; set; }
        public List<PipeMessageViewModel> PipeMessages { get; } = new List<PipeMessageViewModel>();
    }
}
