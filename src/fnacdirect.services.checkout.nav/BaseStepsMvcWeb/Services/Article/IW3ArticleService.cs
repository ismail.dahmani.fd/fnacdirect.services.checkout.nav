﻿using FnacDirect.Contracts.Online.Model;
using FnacDirect.Front.WebBusiness;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FnacDirect.OrderPipe.Base.Proxy.Articles
{
    public interface IW3ArticleService
    {
        W3Article GetArticle(ArticleReference articleReference);

        W3Article GetArticleNoPrice(ArticleReference articleReference);
    }
}
