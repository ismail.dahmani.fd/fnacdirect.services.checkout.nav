﻿
using System.Xml.Serialization;

namespace FnacDirect.OrderPipe.Base.Business.OrderDetail
{
    public class OpeningPeriod
    {
        [XmlAttribute]
        public string Opening { get; set; }
        
        [XmlAttribute]
        public string Closing { get; set; }
    }
}
