﻿namespace FnacDirect.OrderPipe.Base.Model
{
    public enum DeferredPaymentAuthorizationType
    {
        None = 0,
        AcceptCallCenter = 1,
        Declined = 2,
        Deleted = 3,
        AcceptClient = 4
    }
}
