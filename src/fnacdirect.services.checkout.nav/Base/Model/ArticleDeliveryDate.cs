﻿using System;

namespace FnacDirect.OrderPipe.Base.Model
{
	public class ArticleDeliveryDate : IEquatable<ArticleDeliveryDate>
	{
		private readonly int _shippingMethodId;
        public int ShippingMethodId
        {
            get { return _shippingMethodId; }
        }

        private readonly DateTime? _deliveryDateMax;
        public DateTime? DeliveryDateMax
        {
            get { return _deliveryDateMax; }
        }

        private readonly DateTime? _deliveryDateMin;
        public DateTime? DeliveryDateMin
        {
            get { return _deliveryDateMin; }
        }

        public bool IsSelected { get; set; }

        public ArticleDeliveryDate(int shippingMethodId, DateTime? deliveryDateMin, DateTime? deliveryDateMax)
        {
            _shippingMethodId = shippingMethodId;
            _deliveryDateMin = deliveryDateMin;
            _deliveryDateMax = deliveryDateMax;
        }

		public bool Equals(ArticleDeliveryDate other)
		{
			if (ReferenceEquals(null, other)) return false;
			if (ReferenceEquals(this, other)) return true;
			return _deliveryDateMax.Equals(other._deliveryDateMax) && _deliveryDateMin.Equals(other._deliveryDateMin) && _shippingMethodId == other._shippingMethodId;
		}

		public override bool Equals(object obj)
		{
			if (ReferenceEquals(null, obj)) return false;
			if (ReferenceEquals(this, obj)) return true;
			if (obj.GetType() != GetType()) return false;
			return Equals((ArticleDeliveryDate) obj);
		}

		public override int GetHashCode()
		{
			unchecked
			{
				var hashCode = _deliveryDateMax.GetHashCode();
				hashCode = (hashCode*397) ^ _deliveryDateMin.GetHashCode();
				hashCode = (hashCode*397) ^ _shippingMethodId;
				return hashCode;
			}
		}

		public static bool operator ==(ArticleDeliveryDate left, ArticleDeliveryDate right)
		{
			return Equals(left, right);
		}

		public static bool operator !=(ArticleDeliveryDate left, ArticleDeliveryDate right)
		{
			return !Equals(left, right);
		}
	}
}