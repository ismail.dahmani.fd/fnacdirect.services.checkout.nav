﻿using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.OrderPipe.Base.Steps.Pop;
using FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Common.Basket;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Shipping
{
    public interface IBasketViewModelBuilder
    {
        SellerInformationsViewModel BuildSellerViewModel(PopOrderForm popOrderForm, PopShippingSellerData currentPopShippingSellerData);
        BasketViewModel BuildBasketViewModel(PopOrderForm popOrderForm, PopShippingSellerData currentPopShippingSellerData);
    }
}
