using FnacDirect.OrderPipe.Base.BaseModel;
using FnacDirect.OrderPipe.Base.LineGroups;
using FnacDirect.OrderPipe.Base.Model.LineGroups;
using FnacDirect.OrderPipe.Base.Model.UserInformations;
using FnacDirect.OrderPipe.PaymentConfiguration.Results;
using System.Collections.Generic;

namespace FnacDirect.OrderPipe.Base.Business.PaymentConfiguration
{
    public interface IPaymentConfigurationBusiness
    {
        SecurityType GetPaymentSecurityConfiguration(IEnumerable<ILineGroup> lineGroups, IUserContextInformations userContextInformations, ShippingMethodEvaluation shippingMethodEvaluation, decimal totalAmount, int creditCardTypeId);
        SecurityType GetPaymentSecurityConfiguration(IEnumerable<ILogisticLineGroup> logisticLineGroups, IUserContextInformations userContextInformations, decimal totalAmount, int creditCardTypeId);
        IDictionary<string, AuthorizationAmountofLogisticalOrderResult> GetPaymentAuthorizationConfiguration(IEnumerable<ILogisticLineGroup> logisticLineGroups, IUserContextInformations userContextInformations, decimal totalAmount, int creditCardTypeId);
        bool ShouldControlCreditCard(IEnumerable<ILogisticLineGroup> logisticLineGroups, IUserContextInformations userContextInformations, decimal totalAmount, int creditCardTypeId);
        short GetPaymentExemptionLevel(IEnumerable<ILogisticLineGroup> logisticLineGroups, IUserContextInformations userContextInformations, decimal totalAmount, int creditCardTypeId);

    }
}
