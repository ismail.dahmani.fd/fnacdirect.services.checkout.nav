using FnacDirect.OrderPipe.Base.BaseModel.Model;
using FnacDirect.Technical.Framework.Web;
using FnacDirect.Technical.Framework.Web.Mvc;
using System.Linq;


namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Common.Basket
{
    public class ExpressPlusViewModelBuilder : IExpressPlusViewModelBuilder
    {
        private readonly IUserExtendedContextProvider _userExtendedContextProvider;
        private readonly IUserExtendedContext _userExtendedContext;
        private readonly IApplicationContext _applicationContext;
        private readonly IUrlManager _urlManager;
        private readonly IPriceHelper _priceHelper;

        public ExpressPlusViewModelBuilder(IApplicationContext applicationContext, IUserExtendedContextProvider iUserExtendedContextProvider,
            IUrlManager urlManager)
        {
            _applicationContext = applicationContext;
            _userExtendedContextProvider = iUserExtendedContextProvider;
            _userExtendedContext = (IUserExtendedContext)_userExtendedContextProvider.GetCurrentUserExtendedContext();
            _urlManager = urlManager;
            _priceHelper = applicationContext.GetPriceHelper();
        }

        private void ComputeExpressPlusWording(AboIlliData aboIlliData, ExpressPlusViewModel result)
        {
                var firstPart = string.Empty;
                var pricePart = _priceHelper.Format(aboIlliData.Price.Value, PriceFormat.CurrencyAsDecimalSeparator);
                if (result.IsTrial)
                {
                    var periodPart = _applicationContext.GetMessage("OrderPipe.Basket.ExpressPlus.Popin.Trial.SubTitle.month");
                    if (_applicationContext.Operations.Any())
                    {
                        if (_applicationContext.Operations["expressPlusnbj"].Active)
                        {
                            periodPart = _applicationContext.GetMessage("OrderPipe.Basket.ExpressPlus.Popin.Trial.SubTitle.expressPlusnbj");
                        }
                        else if (_applicationContext.Operations["expressPlus15j"].Active)
                        {
                            periodPart = _applicationContext.GetMessage("OrderPipe.Basket.ExpressPlus.Popin.Trial.SubTitle.expressPlus15j");
                        }
                    }

                    firstPart = _applicationContext.GetMessage("orderpipe.pipe.expressplus.option.thenyearly");
                    result.PeriodTitle = periodPart;
                }
                else
                {
                    firstPart = _applicationContext.GetMessage("orderpipe.pipe.expressplus.option.foryearly");
                }
                result.PriceSubtitle = string.Format(firstPart, pricePart);
        }

        public virtual ExpressPlusViewModel Build(AboIlliData aboIlliData)
        {
            if (aboIlliData != null && !aboIlliData.IsAbonne && aboIlliData.IsBasketElligibleForAboIlli && aboIlliData.Price.HasValue)
            {
                var result = new ExpressPlusViewModel
                {
                    HasExpressPlus = aboIlliData.HasAboIlliInBasket,
                    IsTrial = true
                };
                if (aboIlliData.AboIlliSubscription != null)
                {
                    result.IsTrial = aboIlliData.AboIlliSubscription.IsElligibleForTrial;
                }

                var yearString = _applicationContext.GetMessage("orderpipe.pipe.expressplus.option.yearly");
                var price = _priceHelper.Format(aboIlliData.Price.Value, PriceFormat.CurrencyAsDecimalSeparator);
                result.YearlyPrice = string.Format(yearString, price);
                var urlCgv = _urlManager.GetPageInWWWDotNetSecure("popcgvExpressPlus").DefaultUrl.ToString();
                result.UrlCGV = _applicationContext.GetMessage("OrderPipe.Basket.ExpressPlus.Popin.Cgv", urlCgv);
                //Gestion du cashback
                if (_userExtendedContext.Visitor.IsIdentified)
                {
                    if (_userExtendedContext.Customer != null && _userExtendedContext.Customer.MembershipContract != null)
                    {
                        if (_userExtendedContext.Customer.IsValidAdherent && !_userExtendedContext.Customer.IsAdherentOne)
                        {
                            var adhPrice = _applicationContext.GetMessage("orderpipe.pipe.expressplus.offre.adh");
                            result.AdherentSubtitle = adhPrice;
                        }
                        else if (_userExtendedContext.Customer.IsAdherentOne)
                        {
                            var onePrice = _applicationContext.GetMessage("orderpipe.pipe.expressplus.offre.one");
                            result.AdherentSubtitle = onePrice;
                        }
                    }
                }
                result.ShowPopinButton = aboIlliData.HasAboIlliInBasket ? false : true;
                ComputeExpressPlusWording(aboIlliData, result);
                return result;
            }
            return null;
        }
    }
}
