using FnacDirect.Customer.Model;
using FnacDirect.OrderPipe.Base.BaseModel.Model.Quote;
using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace FnacDirect.OrderPipe.Base.Model
{
    public class QuoteInfos
    {
        public string SID { get; set; }

        public int QuoteStatus { get; set; }

        public DateTime QuoteExpirationDate { get; set; }

        public int AccountId { get; set; }

        public string PrivateComment { get; set; }

        public int ContactID { get; set; }
 
        public int QuoteId { get; set; }
 
        public string QuoteReference { get; set; }

        public string QuoteXmlContent { get; set; }

        public CompanyInfo Company { get; set; }

        public int OrderReference { get; set; }

        public int QuoteMaster { get; set; }

        public decimal TotalHT { get; set; }

        public decimal Total_TTC { get; set; }

        public int ArticlesCount { get; set; }

        public DateTime CreationDate { get; set; }

        public string CreatedBy { get; set; }

        public string LastModifiedBy { get; set; }

        public DateTime LastModificationDate { get; set; }

        public QuoteDemandRaisonEnum QuoteDemandRaison { get; set; }

        public string ProAccountNumber { get; set; }

        [XmlIgnore]
        public Dictionary<int, Choice> Choices { get; set; } = new Dictionary<int, Choice>();

        [XmlIgnore]
        public Dictionary<int, Remise> Remises { get; set; } = new Dictionary<int, Remise>();

        [XmlIgnore]
        public Dictionary<int, Remise> GlobalRemises { get; set; } = new Dictionary<int, Remise>();


    }
}
