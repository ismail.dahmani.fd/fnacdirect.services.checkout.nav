using System.Linq;
using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.Technical.Framework.Web.Mvc;
using FnacDirect.Contracts.Online.Model;
using FnacDirect.OrderPipe.Base.Model.Pop;
using FnacDirect.OrderPipe.Base.Business.Wrap;
using System;
using Common.Logging;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Basket
{
    public class WrapViewModelBuilder : IWrapViewModelBuilder
    {
        private readonly IPriceHelper _priceHelper;
        private readonly FnacSites _currentSite;
        private readonly IWrapBusiness _wrapBusiness;
        private readonly ILog _logger;

        public WrapViewModelBuilder(IApplicationContext applicationContext, IWrapBusiness wrapBusiness, ILog logger)
        {
            _priceHelper = applicationContext.GetPriceHelper();
            _currentSite = (FnacSites)applicationContext.GetSiteId();
            _wrapBusiness = wrapBusiness;
            _logger = logger;
        }

        public WrapViewModel Build(PopOrderForm popOrderForm)
        {
            try
            {
                var result = new WrapViewModel();
                result.IsAvailable = _wrapBusiness.IsEligible(popOrderForm);
                if (!result.IsAvailable)
                    return result;

                var lineGroups = popOrderForm.LineGroups;

                var isFnacPro = _currentSite == FnacSites.FNACPRO;
                if (isFnacPro)
                    result.ForceDedicationDisplay = true;

                var price = lineGroups.Where(lg => lg.AvailableWraps != null && lg.AvailableWraps.Any(w => w.WrapId == 1)).Sum(popLineGroup => popLineGroup.AvailableWraps.First(w => w.WrapId == 1).TotalUserPrice);
                result.Price = _priceHelper.Format(price, PriceFormat.CurrencyAsDecimalSeparator);
                result.PriceModel = _priceHelper.GetDetails(price);

                var priceNoVAT = lineGroups.Where(lg => lg.AvailableWraps != null && lg.AvailableWraps.Any(w => w.WrapId == 1)).Sum(popLineGroup => popLineGroup.AvailableWraps.First(w => w.WrapId == 1).TotalNoVATPrice);
                result.PriceNoVAT = _priceHelper.Format(priceNoVAT, PriceFormat.CurrencyAsDecimalSeparator);
                result.PriceNoVATModel = _priceHelper.GetDetails(priceNoVAT);

                var useVATCountry = false;
                if (lineGroups.FirstOrDefault() != null)
                {
                    var lg = lineGroups.First();
                    result.Dedication = lg.Informations.OrderMessage;
                    useVATCountry = lg.VatCountry.UseVat;
                }

                result.PriceToDisplay = (isFnacPro && !useVATCountry) || !useVATCountry ? _priceHelper.Format(priceNoVAT, PriceFormat.CurrencyAsDecimalSeparator) :
                                                   _priceHelper.Format(price, PriceFormat.CurrencyAsDecimalSeparator);
                result.PriceModelToDisplay = (isFnacPro && !useVATCountry) || !useVATCountry ? _priceHelper.GetDetails(priceNoVAT) :
                                                        _priceHelper.GetDetails(price);

                result.IsFree = price == decimal.Zero;
                result.WrapDetailText = popOrderForm.GetPrivateData<WrapData>(create: false)?.Signature;
                result.IsApplied = lineGroups.Any(lg => lg.Informations.ChosenWrapMethod != 0);
                result.DisplayNotWrapedArticlesMessage = lineGroups.SelectMany(lg => lg.LogisticTypes).Any(lt => lt.AvailableWrapMethod == null || !lt.AvailableWrapMethod.Any());

                return result;
            }
            catch (Exception ex)
            {
                _logger.WarnFormat("WrapViewModelBuilder : Error while building Wrap Model : ", ex.Message);
                return new WrapViewModel() { IsAvailable = false };
            }
        }
    }
}
