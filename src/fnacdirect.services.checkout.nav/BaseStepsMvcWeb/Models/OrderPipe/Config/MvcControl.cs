﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Xml.Serialization;

namespace FnacDirect.OrderPipe.Config
{
    public class MvcControl : ICanBeSwitched
    {
        [XmlAttribute("active")]
        [DefaultValue(true)]
        public bool Active = true;

        [XmlAttribute("controller")]
        public string Controller { get; set; }

        [XmlAttribute("action")]
        public string Action { get; set; }

        [XmlAttribute("switchid")]
        public string SwitchId { get; set; }

        [XmlAttribute("switchmode")]
        public string SwitchMode { get; set; }

        [XmlElement("Property")]
        public List<MvcControlProperty> Properties { get; set; }
    }
}