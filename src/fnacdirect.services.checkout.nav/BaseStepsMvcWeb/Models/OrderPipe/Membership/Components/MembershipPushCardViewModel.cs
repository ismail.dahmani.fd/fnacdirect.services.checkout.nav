

namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Membership
{
    public class MembershipPushCardViewModel
    {
        public int Prid { get; set; }

        public string Label { get; set; }

        public int TypeId { get; set; }

        public bool HasOPC { get; set; }

        public string StandardPrice { get; set; }

        public string PriceWithOffer { get; set; }

        public string ValidityType { get; set; }

        public int ValidityDuration { get; set; }

        public bool HasRenewCard { get; set; }

        public int RenewPrid { get; set; }

        public bool RenewCardHasOPC { get; set; }

        public string RenewCardStandardPrice { get; set; }

        public string RenewCardPriceWithOffer { get; set; }
        public bool IsTryCard { get; set; }
    }
}
