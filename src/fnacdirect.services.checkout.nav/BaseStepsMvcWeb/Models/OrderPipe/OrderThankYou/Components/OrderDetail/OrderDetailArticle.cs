﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.OrderThankYou
{
    public enum OrderDetailArticleShippingModeEnum
    {
        None,
        ClickAndCollect,
        Demat,
        AdherentCard,
        SteedFromStore,
        Ebook,
    }

    public class OrderDetailArticle
    {
        public int Prid { get; set; }
        public int Quantity { get; set; }
        public string Title { get; set; }
        public string PicturUrl { get; set; }

        public OrderDetailArticleShippingModeEnum OrderDetailArticleShippingMode { get; set; }

        public string DeliveryDates { get; set; }

        public bool HasDigiCopy { get; set; }
    }
}
