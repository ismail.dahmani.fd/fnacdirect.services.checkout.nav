using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Basket;
using FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.OnePage;
using FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.OnePage.Omniture;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Omniture
{
    public interface IOmnitureViewModelBuilder
    {
        TrackingViewModel BuildTracking(PopOrderForm popOrderForm, TrackedPage trackedPage, string basketAmount);
        OmnitureBasketViewModel BuildBasket(PopOrderForm popOrderForm);
        OmnitureBasketViewModel BuildOnePage(PopOrderForm popOrderForm);
        OmnitureCommonViewModel BuildShipping(PopOrderForm popOrderForm, int sellerId);
        OmnitureCommonViewModel BuildCommon(PopOrderForm popOrderForm);        
        OmnitureOrderThankYouViewModel BuildOrderThankYou(PopOrderForm popOrderForm);
        OmnitureFnacPlusViewModel BuildFnacPlusData(PopOrderForm popOrderForm);
    }

    public enum TrackedPage
    {
        popOnePage,
        popShipping,
        popPayment,
        popBasket,
        scoOrderThankYou
    }
}
