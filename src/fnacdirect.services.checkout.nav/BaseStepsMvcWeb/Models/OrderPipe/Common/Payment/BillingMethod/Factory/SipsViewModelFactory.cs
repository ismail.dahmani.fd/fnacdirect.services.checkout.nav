using FnacDirect.OrderPipe.Base.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FnacDirect.OrderPipe.BaseMvc.Web.Services.Pop.Payment;
using FnacDirect.Technical.Framework.Web.Mvc;
using FnacDirect.OrderPipe.Core.Services;
using FnacDirect.OrderPipe.Base.Proxy.Customer;
using FnacDirect.OrderPipe.Base.Business;
using FnacDirect.OrderPipe.Base.Business.Payment;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Common.Payment.BillingMethodFactories
{
    public class SipsViewModelFactory : BaseCreditCardViewModelFactory<CreditCardBillingMethod>
    {
        private readonly IPipeParametersProvider _pipeParametersProvider;
        private readonly IApplicationContext _applicationContext;
        private readonly ISwitchProvider _switchProvider;
        private readonly IBillingMethodService _billingMethodService;

        public SipsViewModelFactory(IApplicationContext applicationContext, 
                                     ICreditCardDescriptorBusiness creditCardDescriptorBusiness,
                                     ICustomerCreditCardService customerCreditCardService,
                                    IPaymentMethodService paymentMethodService,
                                    IPipeParametersProvider pipeParametersProvider,
                                    ISwitchProvider switchProvider,
                                     IBillingMethodService billingMethodService) 
            : base(applicationContext, creditCardDescriptorBusiness, customerCreditCardService, paymentMethodService)
        {
            _applicationContext = applicationContext;
            _pipeParametersProvider = pipeParametersProvider;
            _switchProvider = switchProvider;
            _billingMethodService = billingMethodService;
        }

        public override BillingMethodViewModel Get(string tabName, PaymentMethod paymentMethod, OPContext opContext, PopOrderForm popOrderForm)
        {
            var viewModel = new SipsBillingMethodViewModel();

            var isGuest = popOrderForm.UserContextInformations.IsGuest;

            var paymentSelectionData = popOrderForm.GetPrivateData<PaymentSelectionData>("payment");

            viewModel.SipsViewModel.Holder = string.Format("{0} {1}", popOrderForm.BillingAddress.Firstname, popOrderForm.BillingAddress.Lastname);

            CreditCardBillingMethod(viewModel, popOrderForm, tabName, paymentMethod);

            var eccvTypeIds = _pipeParametersProvider.GetAsRangeSet<int>("eccv.typeid");

            var hasECCVInBasket = popOrderForm.LineGroups.GetArticles().Any(a => a.TypeId.HasValue && eccvTypeIds.Contains(a.TypeId.Value));

            if ((paymentMethod.SpecificRules.Contains(SpecificRules.Phone)
                            && (paymentSelectionData.RemainingAmount < _pipeParametersProvider.Get("billing.creditcardbyphone.limit.low", 0m)
                            || paymentSelectionData.RemainingAmount > _pipeParametersProvider.Get("billing.creditcardbyphone.limit.high", 1000m)
                            || (popOrderForm.Constraints != null && popOrderForm.Constraints.Contains("billingmethod.telephone.hide"))
                            || hasECCVInBasket))
                || (paymentMethod.SpecificRules.Contains(SpecificRules.Phone) && isGuest))
            {
                viewModel.Display = false;
            }
            var ArticlesTypeIds = _pipeParametersProvider.GetAsEnumerable<int>("credit.typeid");

            if (paymentMethod.SpecificRules.Contains(SpecificRules.Credit))
            {
                viewModel.Credits.Add(new CreditViewModel()
                {
                    Id = string.Empty,
                    Label = _applicationContext.GetMessage("OrderPipe.Preview.Payment.CreditCardCreditList.UsualConditions")
                });

                if (!_switchProvider.IsEnabled("credit.specific.typeIdFilter"))
                {
                    foreach (var credit in popOrderForm.AvailableCredits)
                    {
                        viewModel.Credits.Add(new CreditViewModel()
                        {
                            Id = credit.Id,
                            Label = credit.Label,
                            CreditType = credit.CreditType.ToString()
                        });
                    }
                }
                else
                {
                    foreach (var credit in popOrderForm.AvailableCredits)
                    {
                        if (credit.CreditRule == FnacDirect.Contracts.Online.Model.CreditRule.Permanent)
                        {
                            viewModel.Credits.Add(new CreditViewModel()
                            {
                                Id = credit.Id,
                                Label = credit.Label,
                                CreditType = credit.CreditType.ToString()
                            });
                        }
                    }
                }
            }

            //Permet d'afficher par defaut le billing method "Cartes Privative".
            if (paymentMethod.SpecificRules.Contains(SpecificRules.Private))
            {
                viewModel.IsSelected = true;
            }

            if (_billingMethodService.HasSensibleSupportId(popOrderForm.LineGroups.GetArticles()))
            {
                viewModel.DisplayAlone = true;
            }
            return viewModel;
        }
    }
}
