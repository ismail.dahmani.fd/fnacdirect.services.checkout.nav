using FnacDirect.Customer.Model;
using FnacDirect.OrderPipe.Base.Business.Shipping.Relay;
using FnacDirect.OrderPipe.Base.LineGroups;
using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.OrderPipe.Base.Model.Facets;
using FnacDirect.OrderPipe.Base.Model.LineGroups;
using FnacDirect.OrderPipe.Base.Proxy.Customer;
using FnacDirect.OrderPipe.Base.Proxy.Relay;
using FnacDirect.OrderPipe.Core.Services;
using FnacDirect.OrderPipe.Shipping;
using FnacDirect.OrderPipe.Shipping.Model;
using FnacDirect.Solex.Shipping.Client;
using SolexContract = FnacDirect.Solex.Shipping.Client.Contract;
using FnacDirect.Technical.Framework.Web.Mvc;
using System.Collections.Generic;
using System.Linq;
using System;

namespace FnacDirect.OrderPipe.Base.Business.Shipping
{
    public class ShippingAddressElegibilityService : IShippingAddressElegibilityService
    {
        private readonly string _configShippingMethodSelection;

        private readonly ShippingMethodBusiness _shippingMethodBusiness;
        private readonly IShippingMethodBasketService _shippingMethodBasketService;
        private readonly IApplicationContext _applicationContext;
        private readonly IRelayAddressService _relayAddressService;
        private readonly ISwitchProvider _switchProvider;
        private readonly IRelayValidityService _relayValidityService;
        private readonly ICustomerRelayService _customerRelayService;
        private readonly IRelayService _relayService;
        private readonly ISolexBusiness _solexBusiness;
        private readonly ICountryService _countryService;

        private readonly int _localCountry;

        public ShippingAddressElegibilityService(OPContext opContext,
            ShippingMethodBusiness shippingMethodBusiness,
            IApplicationContext applicationContext,
            IShippingMethodBasketServiceProvider shippingMethodBasketServiceProvider,
            IRelayAddressService relayAddressService,
            ISwitchProvider switchProvider,
            IRelayValidityService relayValidityService,
            ICustomerRelayService customerRelayService,
            IRelayService relayService,
            ISolexBusiness solexBusiness,
            ICountryService countryService)
        {
            _shippingMethodBusiness = shippingMethodBusiness;
            _applicationContext = applicationContext;
            _relayAddressService = relayAddressService;
            _switchProvider = switchProvider;
            _relayValidityService = relayValidityService;
            _customerRelayService = customerRelayService;
            _relayService = relayService;

            _configShippingMethodSelection = applicationContext.GetAppSetting("config.shippingMethodSelection");

            _shippingMethodBasketService = shippingMethodBasketServiceProvider.Get(new ParametersProvider(opContext.Pipe.GlobalParameters));

            _localCountry = _applicationContext.GetCountryId();

            _solexBusiness = solexBusiness;
            _countryService = countryService;
        }

        public ShippingAddress DummyRelayAddress
        {
            get
            {
                var relayAddressEntity = new FnacDirect.Relay.Model.RelayAddressEntity(0);
                var relayAddress = new RelayAddress(relayAddressEntity, _localCountry) { ShippingZone = 1 };
                return relayAddress;
            }
        }

        public ShippingAddress GetDummyRelayAddress(IEnumerable<ILineGroup> lineGroups, OPContext opContext)
        {
            if (_switchProvider.IsEnabled("orderpipe.shipping.relay.calculateshippingpriceforeachrelay"))
            {
                //Si on doit calculer les shippingprices pour tous les relais, cela signifie qu'on a plusieurs zones de livraison. 
                //Et si on a un favori dans une zone différente que 1, il faut renseigner la DummyRelayAddress avec cette zone pour les estimations des Frais de port.

                //On vérifie que le client a des RC valides dans ses favoris.
                var relays = _customerRelayService.GetRelayAddresses().Where(relayAddressEntity => _relayValidityService.CheckRelayValidity(relayAddressEntity, lineGroups)).OrderByDescending(r => r.AddressBookId);
                if (!relays.Any())
                {
                    var defaultRelayAddress = _customerRelayService.GetDefaultRelay();
                    if (defaultRelayAddress != null)
                    {
                        relays = _relayService.GetRelaysNearRelay(defaultRelayAddress.Id).Where(relayAddressEntity => _relayValidityService.CheckRelayValidity(relayAddressEntity, lineGroups)).OrderBy(r => r.Distance);
                    }
                }
                //Si on des RC
                if (relays.Any())
                {
                    var relay = relays.First();
                    var relayAddress = new RelayAddress();
                    RelayAddress.Copy(relay, relayAddress, _localCountry);

                    if (_switchProvider.IsEnabled("orderpipe.shipping.relay.transformcountrywithzipcode"))
                    {
                        _relayAddressService.TransformCountryWithZipCode(relayAddress, opContext);
                    }

                    //Déclaration du CountryId et de la ShippingZone sur le Dummy pour faire les bonnes estimations selon la suggestion qu'il va y avoir. 
                    var relayAddressEntity = new FnacDirect.Relay.Model.RelayAddressEntity(0);
                    relayAddress = new RelayAddress(relayAddressEntity, relayAddress.CountryId.GetValueOrDefault()) { ShippingZone = relayAddress.ShippingZone };
                    return relayAddress;
                }
            }

            return DummyRelayAddress;
        }

        public ShopAddress DummyShopAddress
        {
            get
            {
                var shopAddress = new ShopAddress()
                {
                    CountryId = _applicationContext.GetCountryId(),
                    ShippingZone = 1
                };

                return shopAddress;
            }
        }

        public bool IsElegibleForRelay(IGotShippingMethodEvaluation iContainsShippingMethodEvaluation, IEnumerable<ILineGroup> lineGroups)
        {
            var excludedLogisticType = iContainsShippingMethodEvaluation.ShippingMethodEvaluation.ExcludedLogisticTypes;
            var freeShippings = iContainsShippingMethodEvaluation.ShippingMethodEvaluation.FreeShipping;

            var articles = new List<Article>();
            foreach (var lineGroup in lineGroups)
            {
                articles = articles.Concat(lineGroup.Articles).ToList();
            }

            var relayShippingNetwork = new SolexContract.ShippingNetwork<SolexContract.RelayAddress>();

            relayShippingNetwork.Addresses.Add(new SolexContract.RelayAddress() { Default = true });

            var shippingNetworks = new SolexContract.ShippingNetworks
            {
                Relay = relayShippingNetwork
            };

            var evaluateShoppingCartResult = _solexBusiness.EvaluateShoppingCart(articles, freeShippings, shippingNetworks, "orderpipe.shippingaddresseligibilityservice");

            foreach (var addressInSolexResult in evaluateShoppingCartResult.ShippingNetworks.Relay.Addresses)
            {
                if (addressInSolexResult.Choices.Any()
                    && addressInSolexResult.Choices.FirstOrDefault().Any())
                {
                    return true;
                }
            }

            return false;
        }

        public IEnumerable<AddressEntity> GetEligibleAddresses(IGotShippingMethodEvaluation iContainsShippingMethodEvaluation, IEnumerable<ILineGroup> lineGroups, IEnumerable<AddressEntity> addresses)
        {
            var addressesList = addresses.ToList();
            var postalAddresses = addressesList.Select(a => new PostalAddress(a)).ToList();
            var eligiblePostalAddresses = GetEligibleAddresses(iContainsShippingMethodEvaluation, lineGroups, postalAddresses);

            foreach (var eligiblePostalAddress in eligiblePostalAddresses)
            {
                if (!eligiblePostalAddress.Identity.HasValue)
                {
                    continue;
                }

                var address = addressesList.FirstOrDefault(x => x.AddressBookId == eligiblePostalAddress.Identity.Value);

                if (address != null)
                {
                    yield return address;
                }
            }
        }

        public IEnumerable<ShippingAddress> GetEligibleAddresses(IGotShippingMethodEvaluation iContainsShippingMethodEvaluation,
            IEnumerable<ILineGroup> lineGroups,
            IEnumerable<ShippingAddress> addresses)
        {
            var eligiblePostalAddresses = new List<ShippingAddress>();

            var freeShippings = iContainsShippingMethodEvaluation.ShippingMethodEvaluation.FreeShipping;

            var articles = new List<Article>();
            foreach (var lineGroup in lineGroups)
            {
                articles = articles.Concat(lineGroup.Articles).ToList();
            }
            var activeCountries = _countryService.GetCountries();
            var postalShippingNetwork = new SolexContract.ShippingNetwork<SolexContract.PostalAddress>();
            foreach (var address in addresses)
            {
                var country = activeCountries.FirstOrDefault(c => c.ID == address.CountryId);
                if (!(address is PostalAddress) || country == null)
                {
                    continue;
                }

                var postalAddress = address as PostalAddress;
                var postalAddressSolex = new SolexContract.PostalAddress();

                if (address != null && address.Identity != null && address.Identity != -1 && address is PostalAddress)
                {
                    
                    postalAddressSolex.Country = country.Code3;
                    postalAddressSolex.ZipCode = postalAddress.ZipCode;
                    postalAddressSolex.City = postalAddress.City;
                    postalAddressSolex.AddressLine = ($"{postalAddress.AddressLine1} {postalAddress.AddressLine2} {postalAddress.AddressLine3} {postalAddress.AddressLine4}").TrimEnd();
                }
                else
                {
                    postalAddressSolex.Default = true;
                }
                postalShippingNetwork.Addresses.Add(postalAddressSolex);
            }

            var shippingNetworks = new SolexContract.ShippingNetworks
            {
                Postal = postalShippingNetwork
            };

            var evaluateShoppingCartResult = _solexBusiness.EvaluateShoppingCart(articles, freeShippings, shippingNetworks, "orderpipe.shippingaddresseligibilityservice");
            var postalAddresses = evaluateShoppingCartResult.ShippingNetworks.Postal.Addresses
                .Where(a => a.Choices.Any(choice => choice.Any()));

            foreach (var address in postalAddresses)
            {
                if (string.IsNullOrEmpty(address.Payload))
                {
                    var postalAddress = addresses.OfType<PostalAddress>().Where(a => (string.Equals(($"{a.AddressLine1} {a.AddressLine2} {a.AddressLine3} {a.AddressLine4}").TrimEnd(), address.AddressLine, StringComparison.InvariantCultureIgnoreCase)
                                                                        && string.Equals(a.ZipCode, address.ZipCode, StringComparison.InvariantCultureIgnoreCase)
                                                                        && string.Equals(a.City, address.City, StringComparison.InvariantCultureIgnoreCase)));
                    eligiblePostalAddresses.AddRange(postalAddress);
                }
                else
                {
                    var postalAddress = addresses.Where(a => address.Payload == a.Reference);
                    eligiblePostalAddresses.AddRange(postalAddress);
                }
            }
            return eligiblePostalAddresses;
        }

        public bool IsElegibleForShop(IGotShippingMethodEvaluation iContainsShippingMethodEvaluation, IEnumerable<ILineGroup> lineGroups)
        {
            var freeShippings = iContainsShippingMethodEvaluation.ShippingMethodEvaluation.FreeShipping;

            var articles = new List<Article>();
            foreach (var lineGroup in lineGroups.Where(lg => !lg.HasMarketPlaceArticle))
            {
                articles = articles.Concat(lineGroup.Articles).ToList();
            }

            var shopShippingNetwork = new SolexContract.ShippingNetwork<SolexContract.ShopAddress>();

            shopShippingNetwork.Addresses.Add(new SolexContract.ShopAddress() { Default = true });

            var shippingNetworks = new SolexContract.ShippingNetworks
            {
                Shop = shopShippingNetwork
            };

            var evaluateShoppingCartResult = _solexBusiness.EvaluateShoppingCart(articles, freeShippings, shippingNetworks, "orderpipe.shippingaddresseligibilityservice");

            foreach (var addressInSolexResult in evaluateShoppingCartResult.ShippingNetworks.Shop.Addresses)
            {
                if (addressInSolexResult.Choices.Any()
                    && addressInSolexResult.Choices.FirstOrDefault().Any())
                {
                    return true;
                }
            }

            return false;
        }
    }
}
