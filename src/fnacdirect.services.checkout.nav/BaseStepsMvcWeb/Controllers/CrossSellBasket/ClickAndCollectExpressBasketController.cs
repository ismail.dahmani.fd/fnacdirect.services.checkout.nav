using System.Collections.Generic;
using System.Linq;
using FnacDirect.Basket.Business;
using FnacDirect.Context;
using FnacDirect.Front.WebBusiness;
using FnacDirect.OrderPipe.Base.Business;
using FnacDirect.OrderPipe.BaseMvc.Web.Services.Article.Pipe;
using FnacDirect.OrderPipe.BaseMvc.Web.Services.Tracking;
using FnacDirect.OrderPipe.CoreRouting;
using FnacDirect.Technical.Framework.Web;

namespace FnacDirect.OrderPipe.BaseMvc.Web.CrossSellBasket
{
    public class ClickAndCollectExpressBasketController : OneClickBasketController
    {
        public ClickAndCollectExpressBasketController(IUrlManager urlManager,
                                IUserContextProvider userContextProvider,
                                IExecutePipeService executePipeService,
                                IBasketPersistService basketPersistService,
                                ICrossSellBasketViewModelBuilder crossSellBasketViewModelBuilder,
                                IW3ArticleService w3ArticleService,
                                ISwitchProvider switchProvider,
                                IArticleDetailService articleDetailService,
                                ITrackingService trackingService)
            : base(urlManager,
                  userContextProvider,
                  executePipeService,
                  basketPersistService,
                  crossSellBasketViewModelBuilder,
                  w3ArticleService,
                  switchProvider,
                  articleDetailService,
                  trackingService)
        {
        }

        protected override string ComputeAdditionalContextKeys(IEnumerable<CrossSellBasketItemInput> basketItems)
        {
            var basketItem = basketItems.FirstOrDefault(item => item.ShopId > 0);

            if (basketItem != null)
            {
                return string.Format("&{0}={1}", Constants.ContextKeys.ClickAndCollectExpress, basketItem.ShopId);
            }

            return string.Format("&{0}", Constants.ContextKeys.ClickAndCollectExpress);
        }
    }
}
