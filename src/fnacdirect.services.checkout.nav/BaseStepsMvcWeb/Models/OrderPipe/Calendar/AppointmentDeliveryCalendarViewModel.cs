using FnacDirect.OrderPipe.Base.Model.Solex;
using FnacDirect.OrderPipe.BaseMvc.Web.Pop.Models.Common;
using System.Collections.Generic;
using System.Linq;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Solex.Appointment
{
    public class AppointmentDeliveryCalendarViewModel : IBaseApiRootViewModel
    {
        public int CurrentPageIndex { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public bool HasPreviousPage { get; set; }
        public bool HasNextPage { get; set; }
        public List<DeliverySlot>[] Slots { get; set; } = Enumerable.Empty<List<DeliverySlot>>().ToArray();
        public List<string> Articles { get; set; } = new List<string>();
        public List<PipeMessageViewModel> PipeMessages { get; } = new List<PipeMessageViewModel>();
        public bool HasSelectedSlot { get; set; }
        public string SelectdSlotTitle { get; set; }
    }
}
