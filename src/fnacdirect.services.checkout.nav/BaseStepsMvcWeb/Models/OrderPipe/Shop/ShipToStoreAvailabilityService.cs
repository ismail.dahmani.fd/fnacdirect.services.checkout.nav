using FnacDirect.IdentityImpersonation;
using FnacDirect.OrderPipe.Base.BaseModel;
using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.OrderPipe.Base.Model.Solex;
using System;
using System.Collections.Generic;
using System.Linq;
using SolexContract = FnacDirect.Solex.Shipping.Client.Contract;

namespace FnacDirect.OrderPipe.Base.Business.Shipping.Shop
{
    public class ShipToStoreAvailabilityService : IShipToStoreAvailabilityService
    {
        private readonly ISwitchProvider _switchProvider;
        private readonly ISolexBusiness _solexBusiness;
        private readonly ISolexObjectConverterService _solexObjectConverterService;

        public ShipToStoreAvailabilityService(ISwitchProvider switchProvider, ISolexBusiness solexBusiness, ISolexObjectConverterService solexObjectConverterService)
        {
            _switchProvider = switchProvider;
            _solexBusiness = solexBusiness;
            _solexObjectConverterService = solexObjectConverterService;
        }

        public IEnumerable<ShipToStoreAvailability> GetAvailabilities(FnacStore.Model.FnacStore fnacStore, IEnumerable<Article> articles)
        {
            return GetAvailabilities(fnacStore, articles, new List<int>(), null);
        }

        public IEnumerable<ShipToStoreAvailability> GetAvailabilities(FnacStore.Model.FnacStore fnacStore, IEnumerable<Article> articles, IEnumerable<int> excludedLogisticTypes, IdentityImpersonator identityImpersonator)
        {
            //Filter Choices
            var standardArticles = FilterArticles(articles, excludedLogisticTypes);

            var shopShippingNetwork = new SolexContract.ShippingNetwork<SolexContract.ShopAddress>();

            shopShippingNetwork.Addresses.Add(new SolexContract.ShopAddress() { RefUG = fnacStore.RefUG.ToString() });

            var shippingNetworks = new SolexContract.ShippingNetworks
            {
                Shop = shopShippingNetwork
            };

            var evaluateShoppingCartResult = _solexBusiness.EvaluateShoppingCart(standardArticles, new List<FnacDirect.Contracts.Online.Model.FreeShipping>(), shippingNetworks, "orderpipe.shiptostoreavailabilityservice");

            var choices = _solexObjectConverterService.ConvertSolexChoicesToShippingChoices(evaluateShoppingCartResult.ShippingNetworks.Shop.Addresses.FirstOrDefault().Choices.FirstOrDefault());

            //Select Choice
            ShippingChoice choice;
            var excludeCCforCallCenter = _switchProvider.IsEnabled("orderpipe.pop.shipping.clickandcollect.excludeforcallcenter");

            var shouldExcludedClickAndCollect = choices.Any(c => c.MainShippingMethodId == Constants.ShippingMethods.ClickAndCollect)
                && (articles.OfType<StandardArticle>().Any(article => article.Services.Any())
                    || (identityImpersonator != null && excludeCCforCallCenter));

            if (shouldExcludedClickAndCollect)
            {
                choice = choices.GetBestChoice(Constants.ShippingMethods.ClickAndCollect);
            }
            else
            {
                // we should force user to use click & collect if available
                choice = choices.FirstOrDefault(x => x.MainShippingMethodId == Constants.ShippingMethods.ClickAndCollect);
            }

            // if c&c is not available, then select first choice
            if (choice == null)
            {
                choice = choices.FirstOrDefault();
            }

            if (choice != null)
            {
                foreach (var group in choice.Groups)
                {
                    foreach (var item in group.Items)
                    {
                        var standardArticle = standardArticles.FirstOrDefault(x => x.ProductID == item.Identifier.Prid);

                        if (standardArticle != null)
                        {
                            var parcel = choice.Parcels[item.ParcelId];

                            // RG : On bascule de Clic&Collect au Retrait Mag quand l'article est accompagné d'un service
                            if (parcel.ShippingMethodId == Constants.ShippingMethods.ClickAndCollect
                                && (identityImpersonator == null || (!excludeCCforCallCenter && identityImpersonator.IdOrigin != Constants.OrderOrigin.ClickInStore))
                                 && (standardArticle.IsPE || !standardArticle.HasService(articles)))
                            {
                                yield return ClickAndCollect(standardArticle.ReferenceGU, parcel.ShippingMethodId);
                            }
                            else
                            {
                                yield return Shipping(standardArticle.ReferenceGU, parcel.Source.DeliveryDateMin, parcel.Source.DeliveryDateMax, parcel.ShippingMethodId);
                            }
                        }
                    }
                }
            }
        }

        public IEnumerable<ShipToStoreAvailability> GetAvailabilitiesForShop(IEnumerable<Article> articles, Maybe<ShopShippingMethodEvaluationItem> shopShippingMethodEvaluationItem, IEnumerable<int> excludedLogisticTypes)
        {
            var standardArticles = FilterArticles(articles, excludedLogisticTypes);
            var choice = shopShippingMethodEvaluationItem.Value.Choices.FirstOrDefault(c => c.MainShippingMethodId == shopShippingMethodEvaluationItem.Value.SelectedShippingMethodId);

            if(choice == null)
            {
                yield break;
            }

            foreach (var group in choice.Groups)
            {
                foreach (var item in group.Items)
                {
                    var parcel = choice.Parcels[item.ParcelId];

                    if (standardArticles.FirstOrDefault(x => x.ProductID == item.Identifier.Prid) is StandardArticle standardArticle)
                    {
                        yield return Shipping(standardArticle.ReferenceGU, parcel.Source.DeliveryDateMin, parcel.Source.DeliveryDateMax, parcel.ShippingMethodId);
                    }
                }
            }
        }


        public IEnumerable<ClickAndCollectShipToStoreAvailability> GetClickAndCollectAvailabilities(FnacStore.Model.FnacStore fnacStore, IEnumerable<Article> articles)
        {
            var standardArticles = FilterArticles(articles, Enumerable.Empty<int>());
            return GetClickAndCollectAvailabilities(fnacStore, standardArticles);
        }
        private IEnumerable<StandardArticle> FilterArticles(IEnumerable<Article> articles, IEnumerable<int> excludedLogisticTypes)
        {
            return articles.OfType<StandardArticle>()
                            .Where(a => !excludedLogisticTypes.Contains(a.LogType.GetValueOrDefault()))
                            .Where(a => !a.IsNumerical)
                            .ToList();
        }
        private IEnumerable<ClickAndCollectShipToStoreAvailability> GetClickAndCollectAvailabilities(FnacStore.Model.FnacStore fnacStore, IEnumerable<StandardArticle> standardArticles)
        {
            var eligibleArticles = standardArticles.ToList();

            foreach (var standardArticle in eligibleArticles)
            {
                var storeStockInformations = _solexBusiness.GetStoreStockInformations(fnacStore.RefUG.ToString(), standardArticle);

                if (storeStockInformations != null && storeStockInformations.TotalAvailableClickAndCollect >= standardArticle.Quantity)
                {
                    yield return ClickAndCollect(standardArticle.ReferenceGU);
                }
            }
        }
        private static ClickAndCollectShipToStoreAvailability ClickAndCollect(string referenceGu)
        {
            return new ClickAndCollectShipToStoreAvailability(referenceGu);
        }

        private static ClickAndCollectShipToStoreAvailability ClickAndCollect(string referenceGu, int shippingMethodId)
        {
            return new ClickAndCollectShipToStoreAvailability(referenceGu, shippingMethodId);
        }

        private static ShippingShipToStoreAvailability Shipping(string referenceGu, DateTime? deliveryDayMin, DateTime? deliveryDayMax, int shippingMethodId = 0)
        {
            return new ShippingShipToStoreAvailability(referenceGu, deliveryDayMin, deliveryDayMax, shippingMethodId);
        }

    }
}
