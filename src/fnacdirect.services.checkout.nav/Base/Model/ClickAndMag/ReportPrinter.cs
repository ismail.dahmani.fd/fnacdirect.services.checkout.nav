using System;
using System.Collections.Generic;
using Microsoft.Reporting.WebForms;
using System.Web;
using System.Web.UI.WebControls;
using FnacDirect.OrderPipe.FDV.Model;

namespace FnacDirect.OrderPipe.CM.Business
{
    public class ReportPrinter
    {
        public ReportPrinter(string rdlLocalPath, string generatedFilename)
        {
            RdlLocalPath = rdlLocalPath;
            GeneratedFilename = generatedFilename;
        }

        private static readonly string PDF_MIME_TYPE = "application/pdf";

        private string _RdlLocalPath = string.Empty;
        /// <summary>
        /// Renvoie ou définit le chemin local du fichier de définiton rdl
        /// </summary>
        public string RdlLocalPath
        {
            get { return _RdlLocalPath; }
            set { _RdlLocalPath = value; }
        }

        private string _GeneratedFilename = "ExportedData_{0:yyyy-dd-MM-HH-mm-ss}";
        /// <summary>
        /// Renvoie ou définit le nom du fichier généré
        /// (Prend en argument la date et l'heure de la génération)
        /// </summary>
        public string GeneratedFilename
        {
            get { return _GeneratedFilename; }
            set { _GeneratedFilename = value; }
        }

        /// <summary>
        /// Renvoie une séquence binaire d'un "report" générée à partir d'un objet ReportDataBase
        /// </summary>
        public byte[] GetBuffer(PropCommReportData reportData)
        {
            List<ReportDataSource> dataSources = null;

            using (var localReport = new LocalReport { ReportPath = RdlLocalPath, EnableHyperlinks = true })
            {

                dataSources = GetDataSources(reportData);
                foreach (var dataSource in dataSources)
                {
                    localReport.DataSources.Add(dataSource);
                }

                var permissions = new System.Security.PermissionSet(System.Security.Permissions.PermissionState.Unrestricted);
                localReport.SetBasePermissionsForSandboxAppDomain(permissions);
                var buffer = localReport.Render("PDF", "<DeviceInfo><SimplePageHeaders>True</SimplePageHeaders></DeviceInfo>", out var mimeType, out var encoding, out var fileNameExtension, out var streamids, out var warnings);
                localReport.ReleaseSandboxAppDomain();

                return buffer;
            }
        }

        /// <summary>
        /// Imprime une séquence binaire dans la requête http en cours
        /// </summary>
        public void PrintToWebRequest(byte[] buffer, bool newPage)
        {
            var data = buffer;
            var filename = string.Empty;

            filename = string.Format(GeneratedFilename, DateTime.Now);
            HttpContext.Current.Response.Buffer = true;
            HttpContext.Current.Response.Clear();
            HttpContext.Current.Response.ContentType = PDF_MIME_TYPE;
            if (!newPage)
                HttpContext.Current.Response.AddHeader("content-disposition", "attachment; filename=" + filename);

            HttpContext.Current.Response.AppendHeader("content-length", data.Length.ToString());
            HttpContext.Current.Response.BinaryWrite(data);
            HttpContext.Current.Response.Flush();
            HttpContext.Current.Response.End();
        }

        public List<ReportDataSource> GetDataSources(PropCommReportData reportData)
        {
            ObjectDataSource reportForm = null;
            ObjectDataSource reportLine = null;
            var dataSources = new List<ReportDataSource>();

            reportForm = new ObjectDataSource(typeof(ReportDataProvider).FullName, "GetReportForm");
            reportForm.SelectParameters.Add(new Parameter("reportData", System.Data.DbType.Object));
            reportForm.Selecting += delegate (object sender, ObjectDataSourceSelectingEventArgs e)
            {
                e.InputParameters["reportData"] = reportData;
            };
            dataSources.Add(new ReportDataSource("ReportForm", reportForm));

            reportLine = new ObjectDataSource(typeof(ReportDataProvider).FullName, "GetReportLines");
            reportLine.SelectParameters.Add(new Parameter("reportData", System.Data.DbType.Object));
            reportLine.Selecting += delegate (object sender, ObjectDataSourceSelectingEventArgs e)
            {
                e.InputParameters["reportData"] = reportData;
            };
            dataSources.Add(new ReportDataSource("ReportLine", reportLine));

            return dataSources;
        }
    }
}
