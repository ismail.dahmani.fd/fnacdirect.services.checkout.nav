namespace FnacDirect.OrderPipe.Base.Model.Solex
{
    public class ImmediateDeliveryData : CustomerData
    {
        public string ImmediateDeliveryEligibilityQueryHash { get; set; }
        public bool IsImmediateDeliveryServiceAvailable { get; set; }
        public bool IsImmediateDeliveryEligible { get; set; } = true;
        public string DeliveryAddressHash { get; set; }
    }
}
