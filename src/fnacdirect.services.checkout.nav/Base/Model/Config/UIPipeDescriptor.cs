namespace FnacDirect.OrderPipe.Config
{
	public abstract class UIPipeDescriptor : IUiPipeDescriptor
	{
		public abstract void Init(IOP pipe, Pipe cp);
	}
}