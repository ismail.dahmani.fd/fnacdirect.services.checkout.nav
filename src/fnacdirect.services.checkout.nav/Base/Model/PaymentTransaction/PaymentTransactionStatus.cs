using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FnacDirect.OrderPipe.Base.Model.FDV
{
    public enum PaymentTransactionStatus
    {
        /// <summary>
        /// La transacation n'a pas commencé
        /// </summary>
        NotStarted = 0,
        /// <summary>
        /// La transaction est en cours
        /// </summary>
        InProgress = 1,
        /// <summary>
        /// La transaction attend une réponse d'une ressource externe (WS, EAI etc..)
        /// </summary>
        WaitingForExternalResponse = 2,
        /// <summary>
        /// La transaction est effectuée
        /// </summary>
        OK = 10,
        /// <summary>
        /// La transaction est en erreur
        /// </summary>
        KO = 20,
        /// <summary>
        /// La transaction a été annulée
        /// </summary>
        Canceled = 30
    }
}
