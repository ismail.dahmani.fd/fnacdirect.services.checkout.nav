﻿using System.Linq;
using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.OrderPipe.Base.Model.Facets;
using System.Web.Mvc;
using FnacDirect.OrderPipe.Base.Proxy.Membership;
using FnacDirect.Membership.Model.Constants;
using FnacDirect.Contracts.Online.Model;
using FnacDirect.Technical.Framework.Web.Mvc;

namespace FnacDirect.OrderPipe.BaseMvc.Web.HtmlHelpers
{
    public static class GenericHelpers
    {
        public static bool IsCallCenter(this HtmlHelper htmlHelper)
        {
            var opContext = DependencyResolver.Current.GetService<OPContext>();

            if (opContext == null)
            {
                return false;
            }

            var iGotUserContextInformations = opContext.OF as IGotUserContextInformations;

            if (iGotUserContextInformations == null)
            {
                return false;
            }

            var userContextInformations = iGotUserContextInformations.UserContextInformations;

            return userContextInformations.IdentityImpersonator != null;
        }

        public static bool HasTryCard(this HtmlHelper htmlHelper)
        {
            var opContext = DependencyResolver.Current.GetService<OPContext>();

            var membershipConfigurationService = DependencyResolver.Current.GetService<IMembershipConfigurationService>();

            if (opContext == null)
            {
                return false;
            }

            var iGotLineGroups = opContext.OF as IGotLineGroups;

            if (iGotLineGroups == null)
            {
                return false;
            }            

            return iGotLineGroups.LineGroups.GetArticles().Any(a=>a.ProductID == membershipConfigurationService.GetTryCardPrid());

        }

        public static bool HasFnacPlusTryCard(this HtmlHelper htmlHelper)
        {
            var opContext = DependencyResolver.Current.GetService<OPContext>();

            var membershipConfigurationService = DependencyResolver.Current.GetService<IMembershipConfigurationService>();

            if (opContext == null)
            {
                return false;
            }

            var iGotLineGroups = opContext.OF as IGotLineGroups;

            if (iGotLineGroups == null)
            {
                return false;
            }

            return iGotLineGroups.LineGroups.GetArticles().Any(a => a.TypeId == membershipConfigurationService.FnacPlusTryCardType);

        }

        public static bool HasFnacPlusCard(this HtmlHelper htmlHelper)
        {
            var opContext = DependencyResolver.Current.GetService<OPContext>();

            var membershipConfigurationService = DependencyResolver.Current.GetService<IMembershipConfigurationService>();

            if (opContext == null)
            {
                return false;
            }

            var iGotLineGroups = opContext.OF as IGotLineGroups;

            if (iGotLineGroups == null)
            {
                return false;
            }

            return iGotLineGroups.LineGroups.GetArticles().Any(a => membershipConfigurationService.FnacPlusCardTypes.Contains(a.TypeId.GetValueOrDefault()));
        }

        public static bool IsFnacPlusUser(this HtmlHelper htmlHelper)
        {
            var userExtendedContextProvider = DependencyResolver.Current.GetService<IUserExtendedContextProvider>();

            if (userExtendedContextProvider != null)
            {
                var userExtendedContext = userExtendedContextProvider.GetCurrentUserExtendedContext();
                var segments = userExtendedContext.Customer.ContractDTO.Segments;

                return segments.Contains(CustomerSegment.EssaiPlus.ToString())
                    || segments.Contains(CustomerSegment.ExpressPlus.ToString());
            }

            return false;
        }


    }
}
