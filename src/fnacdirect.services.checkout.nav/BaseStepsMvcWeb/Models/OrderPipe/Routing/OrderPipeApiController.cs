using System;
using System.Web.Http;
using System.Web.Mvc;

namespace FnacDirect.OrderPipe.CoreMvcWeb.Routing
{
    public abstract class OrderPipeApiController : ApiController
    {
        private readonly Lazy<OPContext> _opContext;

        protected OrderPipeApiController()
        {
            _opContext = new Lazy<OPContext>(() => DependencyResolver.Current.GetService<OPContext>());
        }

        protected void Rewind()
        {
            var opContext = GetOpContext();

            if (opContext == null)
                return;

            opContext.Pipe.Rewind(opContext, false);
        }

        protected void Continue()
        {
            var opContext = GetOpContext();

            if (opContext == null)
                return;

            opContext.Pipe.Continue(opContext);
        }

        protected OPContext GetOpContext()
        {
            return _opContext.Value;
        }

        protected T GetOrderForm<T>()
            where T : class, IOF
        {
            return GetOpContext().OF as T;
        }
    }
}
