using System;
using System.Xml.Serialization;

namespace FnacDirect.OrderPipe.Base.Model
{
    [Serializable]
    public class Service:StandardArticle 
    {
        /* En fin de compte ne sert pas */
        [XmlIgnore]
        private int? _IsValidLink;

        [XmlIgnore]
        public int? IsValidLink
        {
            get
            {
                return _IsValidLink;
            }
            set
            {
                _IsValidLink = value;
            }
        }

        private ArticleList _MasterArticles;

        [XmlIgnore]
        public ArticleList MasterArticles
        {
            get { return _MasterArticles; }
            set { _MasterArticles = value; }
        }

        ServiceLinkInfo _ServiceLinkInfo;

        public ServiceLinkInfo ServiceLinkInfo
        {
            get { return _ServiceLinkInfo; }
            set { _ServiceLinkInfo = value; }
        }

        public string DetailURL { get; set; }

        private bool _IsLastAddedService;
        public bool IsLastAddedService
        {
            get { return _IsLastAddedService; }
            set { _IsLastAddedService = value; }
        }

        public string MoreInfoLink { get; set; }
    }
}
