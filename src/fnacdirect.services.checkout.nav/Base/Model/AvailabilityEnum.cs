namespace FnacDirect.OrderPipe.Base.Model
{
    public enum AvailabilityEnum
    {        
        From2To4Days = 1,
        From4To8Days = 2,
        Indisponible = 3,
        From2To3Weeks = 8,
        PreOrder = 10,
        From4To9Weeks = 11,
        From4To12Days = 13,
        From2To4DaysBis = 18,
        ImmediateAvailabilityPE = 20,
        PreOrderRestockingPE = 98,
        StockPE = 99,

        From2To4DaysPT = 101,
        From4To8DaysPT = 102,
        From2To3WeeksPT = 108,
        PreOrderBis = 110,
        From4To9WeeksPT = 111,
        From4To12DaysPT = 113,
        From2To4DaysBisPT = 118,
        ImmediateAvailabilityPT = 120,
        PreOrderRestockingPT = 198,
        StockPT = 199
    }
}
