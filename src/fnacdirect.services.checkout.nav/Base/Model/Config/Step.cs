using System.Xml.Serialization;
using FnacDirect.OrderPipe.Config;

namespace FnacDirect.OrderPipe.Core.Model.Config
{
	public class Step : BaseStep
	{
		[XmlElement("Forward")]
		public ForwardList Forwards { get; set; }
	}
}