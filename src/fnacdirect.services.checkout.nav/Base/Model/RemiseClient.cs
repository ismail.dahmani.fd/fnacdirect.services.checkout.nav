using System;

namespace FnacDirect.OrderPipe.Base.Model
{
    [Serializable]
    public class RemiseClient
    {
        public bool IsVisible { get; set; }
        public decimal MontantRemise { get; set; }
        public int CodeMotif { get; set; }
        public int OrderDiscountId { get; set; }
        public int OrdreAffichage { get; set; }
        public int ProductId { get; set; }
        public string CommentaireResponsable { get; set; }
        public string CommentaireVendeur { get; set; }
        public string MotifRemise { get; set; }

        public RemiseClient() { }

        public RemiseClient(int pOrderDiscountId, string pMotifRemise, decimal pMontantRemise, int pCodeMotif)
        {
            CodeMotif = pCodeMotif;
            MontantRemise = pMontantRemise;
            MotifRemise = pMotifRemise;
            OrderDiscountId = pOrderDiscountId;
        }
    }
}
