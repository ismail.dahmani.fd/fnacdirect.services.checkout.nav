using FnacDirect.Customer.Model;
using FnacDirect.IdentityImpersonation;
using FnacDirect.Technical.Framework.Web;
using System.Xml.Serialization;

namespace FnacDirect.OrderPipe.Base.Model.UserInformations
{
    public interface IUserContextInformations
    {
        string UID { get; set; }

        W3Customer W3Customer { get; set; }

        /// <summary>
        /// Transformation du CustomerEntity en Customer
        /// pour eviter d'appeler la methode .ToCustomer() du customerEntity
        /// qui appel la bdd pour récupérer les segments
        /// </summary>
        FnacDirect.Contracts.Online.Model.Customer Customer { get; set; }
        CustomerEntity CustomerEntity { get; set; }
        int AccountId { get; set; }
        string Identity { get; set; }
        bool IsAdherent { get; set; }
        bool IsRenewable { get; set; }
        bool IsGamer { get; set; }
        string ClientCard { get; set; }
        string ContactEmail { get; set; }
        string Gender { get; set; }
        string LastName { get; set; }
        string FirstName { get; set; }
        int AccountStatus { get; set; }
        string CellPhone { get; set; }
        string KoboUserId { get; set; }
        bool IsNewCustomer { get; set; }
        int UserConfidence { get; set; }
        bool Scored { get; set; }
        int MailMessageFlag { get; set; }
        int ThankYouMessageFlag { get; set; }
        int BlackListCounter { get; set; }
        string PlasticCardNumber { get; set; }
        IdentityImpersonator IdentityImpersonator { get; set; }
        string ZipCode { get; set; }
        /// <summary>
		/// Id technique du magasin calculé à partir du refGU dans l'IdentityImpersonator
		/// </summary>
		[XmlIgnore]
        int StoreCodeId { get; set; }
        bool IsGuest { get; set; }
    }
}
