using System.Net;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Pop.ApiControllers.Models
{
    public class ResponseViewModel
    {
        public HttpStatusCode StatusCode { get; set; }
        public string ErrorMessage { get; set; }
    }
}
