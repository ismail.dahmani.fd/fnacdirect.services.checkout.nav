﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Basket
{
    public class BasketSellerViewModel
    {
        public BasketSellerViewModel()
        {
            Informations = new Common.Basket.SellerInformationsViewModel();
        }

        private readonly List<RichBasketItemViewModel> _items
            = new List<RichBasketItemViewModel>();

        public IReadOnlyCollection<RichBasketItemViewModel> Items
        {
            get
            {
                return _items.AsReadOnly();
            }
        }

        public BasketSellerViewModel WithItem(RichBasketItemViewModel basketItemViewModel)
        {
            _items.Add(basketItemViewModel);

            return this;
        }

        public Common.Basket.SellerInformationsViewModel Informations { get; set; }

        public int SellerId { get; set; }

        public bool ShouldDisplaySellerInfo { get; set; }

        public decimal Reduction { get; set; }

        public decimal TotalWithReductionBook5pc { get; set; }
    }
}
