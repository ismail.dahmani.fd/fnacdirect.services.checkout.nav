using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FnacDirect.OrderPipe.Base.Business
{
    public class RebateValidationThresholdConfiguration
    {
        public decimal? Value
        {
            get;
            set;
        }

        public bool AutoAdhPrice
        {
            get;
            set;
        }
    }
}
