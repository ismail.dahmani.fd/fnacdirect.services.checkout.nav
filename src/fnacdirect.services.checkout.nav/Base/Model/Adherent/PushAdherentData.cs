using System.Collections.Generic;
using FnacDirect.Membership.Model.Constants;

namespace FnacDirect.OrderPipe.Base.BaseModel.Model
{
    public class PushAdherentData : GroupData
    {
        public PushAdherentData()
        {

        }

        public decimal SimulateSavingAmount { get; set; }

        public decimal RealSavingAmount { get; set; }

        public decimal SimulateDifferedAmount { get; set; }

        public decimal RealDifferedAmount { get; set; }

        public decimal SimulateSavingShippingCostAmount { get; set; }

        public decimal RealSavingShippingCostAmount { get; set; }

        public decimal SimulateNonAdherentShippingCost { get; set; }

        public decimal SimulateAdherentShippingCost { get; set; }

        public int PromotionKey { get; set; }

        public int ProfilKey { get; set; }

        public bool Has5PourcentRemImmPer3AnsInBasket { get; set; }

        public bool HasOffreAdhVPMPInBasket { get; set; }

        public bool HasLivOfferte3AnsInBasket { get; set; }

        public bool HasXPourcentRemDiffVPInBasket { get; set; }

        public bool HasXPourcentRemDiffInBasket { get; set; }

        public bool HasOP10_100InBasket { get; set; }

        public CategoryCardType? categoryCardType { get; set; }

        public string cardDuration { get; set; }

        public List<string> BasketPromotions { get; set; }

        public string CombinationKey { get; set; }

        public string BasketMessageKey { get; set; }

        public string PreviewMessageKey { get; set; }
    }
}
