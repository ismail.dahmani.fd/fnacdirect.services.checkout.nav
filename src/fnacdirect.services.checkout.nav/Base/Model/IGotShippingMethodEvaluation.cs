using FnacDirect.OrderPipe.Base.BaseModel;

namespace FnacDirect.OrderPipe.Base.Model.Facets
{
    public interface IGotShippingMethodEvaluation : IOF
    {
        ShippingMethodEvaluation ShippingMethodEvaluation { get; set; }

        BillingAddress BillingAddress { get; set; }

        string RelayCellphone { get; set; }
		
        bool DisableSplit { get; set; }

        bool HasProfessionalAddressForSeller(int sellerId);
    }
}
