using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Common.Payment.BillingMethodFactories;
using StructureMap;
using System;
using System.Linq;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Common.Payment
{
    public class StructureMapPaymentMethodViewModelFactory : IPaymentMethodViewModelFactory
    {
        private readonly IContainer _container;

        public StructureMapPaymentMethodViewModelFactory(IContainer container)
        {
            _container = container;
        }

        public BillingMethodViewModel GetBillingMethodViewModel(BillingMethodDescriptor billingDescription, 
                                                             PaymentMethod paymentMethod, 
                                                             PopOrderForm popOrderForm, 
                                                             OPContext opContext, 
                                                             string tabName)
        {
            var type = typeof(IPaymentViewModelFactory<>).MakeGenericType(billingDescription.Type);

            IPaymentViewModelFactory paymentViewModelFactory = null;

            try
            {
                paymentViewModelFactory = _container.GetInstance(type) as IPaymentViewModelFactory;
            }
            catch(StructureMapConfigurationException)
            {

            }

            BillingMethodViewModel viewModel = null;

            if (paymentViewModelFactory != null)
            {
                viewModel = paymentViewModelFactory.Get(tabName, paymentMethod, opContext, popOrderForm);
            }
            else
            {
                viewModel = new BillingMethodViewModel();
            }

            viewModel.Id = paymentMethod.VirtualBillingMethodId != 0 ? paymentMethod.VirtualBillingMethodId : billingDescription.Id;

            var creditCardTypeCacfIds = opContext.Pipe.GlobalParameters.GetAsEnumerable<int>("installmentpayment.creditcards.cacf.type.ids");

            if (viewModel is InstallmentPaymentBillingMethodViewModel vm && vm.Offers.Any(p => creditCardTypeCacfIds.Contains(p.CreditCardTypeId)))
            {
                viewModel.Type = "CACF";
            }
            else
            {
                viewModel.Type = billingDescription.BillingMethodType;
            }

            viewModel.Label = billingDescription.Label;
            viewModel.Name = paymentMethod.Name;

            return viewModel;
        }
    }
}
