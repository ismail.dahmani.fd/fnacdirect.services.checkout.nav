using FnacDirect.Customer.Model;
using FnacDirect.Front.WebBusiness.Configuration;
using FnacDirect.IdentityImpersonation;
using FnacDirect.OrderPipe.Base.Business;
using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.OrderPipe.Base.Proxy.Customer;
using FnacDirect.OrderPipe.BaseMvc.Web.Services.Pop.Payment;
using FnacDirect.Technical.Framework.Web.Mvc;
using System.Collections.Generic;
using System.Linq;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Common.Payment.BillingMethodFactories
{
    public abstract class BaseCreditCardViewModelFactory<TBillingMethod> : BasePaymentViewModelFactory<TBillingMethod>
        where TBillingMethod : BillingMethod
    {
        private readonly IApplicationContext _applicationContext;
        private readonly ICreditCardDescriptorBusiness _creditCardDescriptorBusiness;
        private readonly ICustomerCreditCardService _customerCreditCardService;
        private readonly IPaymentMethodService _paymentMethodService;

        protected const string CreditCardPictoPattern = "decos/pipe/picto-carte-{0}.png";

        protected BaseCreditCardViewModelFactory(IApplicationContext applicationContext,
                                                 ICreditCardDescriptorBusiness creditCardDescriptorBusiness,
                                                 ICustomerCreditCardService customerCreditCardService,
                                                 IPaymentMethodService paymentMethodService)
        {
            _customerCreditCardService = customerCreditCardService;
            _creditCardDescriptorBusiness = creditCardDescriptorBusiness;
            _applicationContext = applicationContext;
            _paymentMethodService = paymentMethodService;
        }

        protected void CreditCardBillingMethod(CreditCardBillingMethodsViewModel creditCardBillingMethodsViewModel, PopOrderForm popOrderForm, string tabName, PaymentMethod paymentMethod)
        {
            var sortedCreditCardData = popOrderForm.GetPrivateData<SortedCreditCardData>(create: true);

            creditCardBillingMethodsViewModel.RegisterCreditCardPreChecked = false;

            FillInCreditCardsList(creditCardBillingMethodsViewModel, tabName, popOrderForm, sortedCreditCardData, paymentMethod);

            var forceDiplayIfNoCards = !paymentMethod.GroupType.Any();
            var hasCards = creditCardBillingMethodsViewModel.CreditCards.Any();

            creditCardBillingMethodsViewModel.Display = forceDiplayIfNoCards || hasCards;
        }

        // si on vient d'une appli Store, on ne charge pas les cartes bleues du client
        protected bool IsClicInStore(string contactEmail)
        {
            IdentityImpersonator identity = null;

            if (System.Web.HttpContext.Current != null)
            {
                identity = IdentityImpersonator.GetIdentityImpersonatorFromCookie(System.Web.HttpContext.Current.Request.Cookies[WebSiteConfiguration.Value.IdentityImpersonator.CookieName], WebSiteConfiguration.Value.IdentityImpersonator.Base64Key);
            }

            if (identity != null && identity.UserMail == contactEmail && !string.IsNullOrEmpty(identity.StoreCodeGU) && !string.IsNullOrEmpty(identity.VendorCode))
            {
                return true;
            }

            return false;
        }

        protected void FillInCreditCardsList(CreditCardBillingMethodsViewModel creditCardBillingMethodViewModel, string tabName, PopOrderForm popOrderForm, SortedCreditCardData sortedCreditCardData, PaymentMethod paymentMethod)
        {
            var creditCards = _paymentMethodService.GetCreditCardsDescriptor();

            foreach (var creditCard in creditCards)
            {
                if (popOrderForm.HiddenCreditCards.Contains(creditCard.Id))
                {
                    continue;
                }

                if (sortedCreditCardData.SortedCreditCards.ContainsKey(tabName)
                    && sortedCreditCardData.SortedCreditCards[tabName].ContainsKey(paymentMethod.Name)
                    && sortedCreditCardData.SortedCreditCards[tabName][paymentMethod.Name].Contains(creditCard.Id))
                {
                    var creditCardViewModel = new CreditCardViewModel(creditCard.Id, creditCard.Label, creditCard.Brand)
                    {
                        NumberLength = creditCard.NumberLength,
                        CryptoLength = creditCard.CryptoLength,
                        Format = creditCard.Format,
                        Url = _applicationContext.GetImagePath() + string.Format(CreditCardPictoPattern, creditCard.Id),
                        Prefix = creditCard.Prefix,
                        HasExpDate = creditCard.HasExpDate
                    };

                    creditCardBillingMethodViewModel.AddCreditCard(creditCardViewModel);
                }
            }

            if (paymentMethod.SpecificRules.Contains(SpecificRules.Private) && popOrderForm.HasMarketPlaceArticle())
            {
                var cardsToRemove = new List<CreditCardViewModel>();

                foreach (var item in creditCardBillingMethodViewModel.CreditCards.Where(x => x.Label != "CARTE FNAC"))
                {
                    cardsToRemove.Add(item);
                }

                foreach (var item in cardsToRemove)
                {
                    creditCardBillingMethodViewModel.RemoveCreditCard(item);
                }
            }
        }


        protected IEnumerable<CustomerCreditCardViewModel> GetCustomerCreditCards(CreditCardBillingMethodsViewModel creditCardBillingMethodsViewModel, string userId)
        {
            var cards = new List<CustomerCreditCardViewModel>();
            var customerCreditCards = _customerCreditCardService.GetCustomerCreditCards(userId, CreditCardPaymentMethodType.Ogone)
                .Where(cc => cc.TypeId != 11 && ((cc.CGVVersion.HasValue && cc.CGVVersion.Value == 1) || !cc.CGVVersion.HasValue) && (cc.CBUsage & CBUsageFlags.SubsequentPurchases) != 0);
            var config = _applicationContext.GetAppSetting("config.shippingMethodSelection");
            var cardsDescription = _creditCardDescriptorBusiness.GetCreditCards(config, _applicationContext.GetSiteContext().Culture);
            var flag3dsEnable = false;

            foreach (var customerCreditCard in customerCreditCards.OrderByDescending(c => c.CreateDate))
            {
                if (creditCardBillingMethodsViewModel.CreditCards.Any(m => m.Id == customerCreditCard.TypeId))
                {
                    flag3dsEnable = creditCardBillingMethodsViewModel.CreditCards.First(m => m.Id == customerCreditCard.TypeId).Is3DSecureTransaction;
                }

                var d3dEnable = _paymentMethodService.IsDirectLink3DSecureEnable(customerCreditCard.TypeId, flag3dsEnable);
                var expireYear = customerCreditCard.ExpireYear.ToString();

                if (expireYear.Length >= 2)
                {
                    expireYear = expireYear.Substring(2);
                }

                var ccd = cardsDescription.Find(x => x.Id == customerCreditCard.TypeId);

                if (ccd != null)
                {
                    var card = new CustomerCreditCardViewModel
                    {
                        Reference = customerCreditCard.Reference,
                        Brand = customerCreditCard.Brand,
                        CardNumber = _creditCardDescriptorBusiness.ApplyMask(ccd, "-", customerCreditCard.CardNumber),
                        // Numéro de carte masqué par les "XXXX"
                        CardNo = customerCreditCard.CardNumber,
                        MaskedCardNumber = _paymentMethodService.ApplyMask(customerCreditCard.CardNumber),
                        Holder = customerCreditCard.Holder,
                        TypeId = customerCreditCard.TypeId,
                        Alias = customerCreditCard.OgoneCreditCardAlias,
                        OgoneTransactionProcess = (!d3dEnable ? null : ccd.TransactionProcess) ?? "Ecommerce",  // Ecommerce = default process for ogone
                        Is3DSecureTransaction = flag3dsEnable,
                        CryptoLength = ccd.CryptoLength,
                        // Date d'expiration au format Ogone (valeur du param ED envoyé à Alias Gateway)
                        ExpirationDate = string.Format("{0:00}{1}", customerCreditCard.ExpireMonth, expireYear),
                        //Set initially IsCryptoRequired = true
                        IsCryptoRequired = true
                    };

                    card.Label = string.Format("{0} {1}", card.MaskedCardNumber, card.Holder);
                    card.Url = _applicationContext.GetImagePath() + string.Format(CreditCardPictoPattern, customerCreditCard.TypeId);
                    card.IsSelected = false;

                    cards.Add(card);
                }
            }

            if (cards.Any())
            {
                cards.First().IsSelected = true;
            }

            cards.Add(new CustomerCreditCardViewModel { IsDumy = true, Reference = string.Empty, Label = _applicationContext.GetMessage("orderpipe.pop.payment.newcard.title") });

            return cards;
        }
    }
}
