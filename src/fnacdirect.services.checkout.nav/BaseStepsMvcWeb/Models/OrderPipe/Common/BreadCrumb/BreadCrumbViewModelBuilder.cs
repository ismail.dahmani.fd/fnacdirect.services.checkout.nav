using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.OrderPipe.BaseMvc.Web.Models.Common;
using FnacDirect.OrderPipe.Config;
using FnacDirect.Technical.Framework.CoreServices.Localization;
using FnacDirect.Technical.Framework.Web.Mvc;
using System;
using System.Linq;
using System.Web.Routing;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Pop.Models.Common
{
    public class BreadCrumbViewModelBuilder : IBreadCrumbViewModelBuilder
    {
        private readonly IApplicationContext _applicationContext;
        private readonly IUIResourceService _uiResourceService;
        private readonly ISwitchProvider _switchProvider;

        public BreadCrumbViewModelBuilder(IApplicationContext applicationContext,
                                          IUIResourceService uiResourceService,
                                          ISwitchProvider switchProvider)
        {
            _applicationContext = applicationContext;
            _uiResourceService = uiResourceService;
            _switchProvider = switchProvider;
        }

        public BreadCrumbViewModel Build(OPContext opContext, Func<string, RouteValueDictionary, string> getUrl)
        {
            var breadCrumbViewModel = new BreadCrumbViewModel();

            var culture = _applicationContext.GetLocale();

            var pipe = opContext.Pipe;

            var hidePayment = false;

            var popOrderForm = opContext.OF as PopOrderForm;

            if (popOrderForm != null && popOrderForm.Path == Base.Model.Pop.AppliablePath.OnePage && _switchProvider.IsEnabled("op.ogone.enablealiasgateway"))
            {
                hidePayment = true;
            }

            var steps = pipe.Steps.ToList();

            var currentStepName = opContext.CurrentStep.Name;

            var directSteps = (from step in steps.OfType<IInteractiveStep>()
                               let position = steps.IndexOf(step)
                               select new
                               {
                                   step.Name,
                                   Position = position,
                                   UiDescriptor = step.UIDescriptor
                               }).ToList();

            string[] paths;
            if (popOrderForm.Path == Base.Model.Pop.AppliablePath.OnePage)
            {
                paths = new[] { "DisplayBasket", "DisplayOnePage", "DisplayPayment", "ThankYou" };
            }
            else if (popOrderForm.Path == Base.Model.Pop.AppliablePath.Payment)
            {
                paths = new[] { "DisplayPaymentOnePage" };
            }
            else
            {
                paths = new[] { "DisplayBasket", "DisplayShippingAddress", "DisplayPayment", "ThankYou" };
            }

            if (opContext.CurrentStep.Name == "PrepareHtmlTemplate")
            {
                currentStepName = "DisplayPayment";
            }

            foreach (var path in paths)
            {
                var directStep = directSteps.FirstOrDefault(s => s.Name == path);

                if (directStep == null)
                {
                    continue;
                }

                var uiStepDescriptor = directStep.UiDescriptor;

                if (directStep.Name == "DisplayPayment"
                    && hidePayment)
                {
                    continue;
                }

                var displayName = directStep.Name;

                if (uiStepDescriptor.LabelId != null)
                {
                    var resource = _uiResourceService.GetUIResourceValue(uiStepDescriptor.LabelId, culture);

                    if (resource != null)
                    {
                        displayName = resource;
                    }
                }

                var breadCrumbItemViewModel = new BreadCrumbItemViewModel()
                {
                    StepName = directStep.Name,
                    DisplayName = displayName,
                    IsCurrent = directStep.Name == currentStepName
                };

                breadCrumbViewModel.WithItem(breadCrumbItemViewModel);
            }

            foreach (var item in breadCrumbViewModel.Items)
            {
                if (item.IsCurrent)
                {
                    break;
                }
                item.IsPrevious = true;
                if (opContext.CurrentStep.Name != "PrepareHtmlTemplate")
                {
                    item.CanNavigate = true;
                    var step = directSteps.FirstOrDefault(s => s.Name == item.StepName);
                    if (step != null)
                    {
                        var uiStepDescriptor = step.UiDescriptor;
                        if (uiStepDescriptor is MvcUiDescriptor)
                        {
                            var mvcUiDescriptor = uiStepDescriptor as MvcUiDescriptor;

                            var routeValues = new RouteValueDictionary();

                            if (mvcUiDescriptor.RouteValueProvider != null)
                            {
                                routeValues = mvcUiDescriptor.RouteValueProvider.GetRouteValueFor(opContext.OF);
                            }

                            item.Link = getUrl(mvcUiDescriptor.Route, routeValues);
                        }
                    }
                }
            }

            return breadCrumbViewModel;
        }
    }
}
