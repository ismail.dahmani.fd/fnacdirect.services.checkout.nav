﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace FnacDirect.OrderPipe.Core.Steps
{
    public abstract class MultiFacetStep<TOrderFormFacet1, TOrderFormFacet2> : Step<IOF, NullStepData, NullGroupData>
        where TOrderFormFacet1 : class, IOF
        where TOrderFormFacet2 : class, IOF
    {
        public override IEnumerable<Type> DataType
        {
            get { return Enumerable.Empty<Type>(); }
        }

        public override sealed StepReturn Check(OPContext opContext)
        {
            return Check(new MultiFacet<TOrderFormFacet1, TOrderFormFacet2>(opContext.OF as TOrderFormFacet1, opContext.OF as TOrderFormFacet2), opContext);
        }

        protected virtual StepReturn Check(MultiFacet<TOrderFormFacet1, TOrderFormFacet2> orderFormMultiFacets)
        {
            return StepReturn.Process;
        }

        protected virtual StepReturn Check(MultiFacet<TOrderFormFacet1, TOrderFormFacet2> orderFormMultiFacets, OPContext opContext)
        {
            return Check(orderFormMultiFacets);
        }

        public override sealed StepReturn Process(OPContext opContext)
        {
            return Process(new MultiFacet<TOrderFormFacet1, TOrderFormFacet2>(opContext.OF as TOrderFormFacet1, opContext.OF as TOrderFormFacet2), opContext);
        }

        protected virtual StepReturn Process(MultiFacet<TOrderFormFacet1, TOrderFormFacet2> orderFormMultiFacets)
        {
            return StepReturn.Next;
        }

        protected virtual StepReturn Process(MultiFacet<TOrderFormFacet1, TOrderFormFacet2> orderFormMultiFacets, OPContext opContext)
        {
            return Process(orderFormMultiFacets);
        }
    }

    public abstract class MultiFacetStep<TOrderFormFacet1, TOrderFormFacet2, TOrderFormFacet3> : Step<IOF, NullStepData, NullGroupData>
        where TOrderFormFacet1 : class, IOF
        where TOrderFormFacet2 : class, IOF
        where TOrderFormFacet3 : class, IOF
    {
        public override IEnumerable<Type> DataType
        {
            get { return Enumerable.Empty<Type>(); }
        }

        public override sealed StepReturn Check(OPContext opContext)
        {
            return Check(new MultiFacet<TOrderFormFacet1, TOrderFormFacet2, TOrderFormFacet3>(opContext.OF as TOrderFormFacet1, opContext.OF as TOrderFormFacet2, opContext.OF as TOrderFormFacet3), opContext);
        }

        protected virtual StepReturn Check(MultiFacet<TOrderFormFacet1, TOrderFormFacet2, TOrderFormFacet3> orderFormMultiFacets)
        {
            return StepReturn.Process;
        }

        protected virtual StepReturn Check(MultiFacet<TOrderFormFacet1, TOrderFormFacet2, TOrderFormFacet3> orderFormMultiFacets, OPContext opContext)
        {
            return Check(orderFormMultiFacets);
        }

        public override sealed StepReturn Process(OPContext opContext)
        {
            return Process(new MultiFacet<TOrderFormFacet1, TOrderFormFacet2, TOrderFormFacet3>(opContext.OF as TOrderFormFacet1, opContext.OF as TOrderFormFacet2, opContext.OF as TOrderFormFacet3), opContext);
        }

        protected virtual StepReturn Process(MultiFacet<TOrderFormFacet1, TOrderFormFacet2, TOrderFormFacet3> orderFormMultiFacets)
        {
            return StepReturn.Next;
        }

        protected virtual StepReturn Process(MultiFacet<TOrderFormFacet1, TOrderFormFacet2, TOrderFormFacet3> orderFormMultiFacets, OPContext opContext)
        {
            return Process(orderFormMultiFacets);
        }
    }

    public abstract class MultiFacetStep<TOrderFormFacet1, TOrderFormFacet2, TOrderFormFacet3, TOrderFormFacet4> : Step<IOF, NullStepData, NullGroupData>
        where TOrderFormFacet1 : class, IOF
        where TOrderFormFacet2 : class, IOF
        where TOrderFormFacet3 : class, IOF
        where TOrderFormFacet4 : class, IOF
    {
        public override IEnumerable<Type> DataType
        {
            get { return Enumerable.Empty<Type>(); }
        }

        public override sealed StepReturn Check(OPContext opContext)
        {
            return Check(new MultiFacet<TOrderFormFacet1, TOrderFormFacet2, TOrderFormFacet3, TOrderFormFacet4>(opContext.OF as TOrderFormFacet1, opContext.OF as TOrderFormFacet2, opContext.OF as TOrderFormFacet3, opContext.OF as TOrderFormFacet4), opContext);
        }

        protected virtual StepReturn Check(MultiFacet<TOrderFormFacet1, TOrderFormFacet2, TOrderFormFacet3, TOrderFormFacet4> orderFormMultiFacets)
        {
            return StepReturn.Process;
        }

        protected virtual StepReturn Check(MultiFacet<TOrderFormFacet1, TOrderFormFacet2, TOrderFormFacet3, TOrderFormFacet4> orderFormMultiFacets, OPContext opContext)
        {
            return Check(orderFormMultiFacets);
        }

        public override sealed StepReturn Process(OPContext opContext)
        {
            return Process(new MultiFacet<TOrderFormFacet1, TOrderFormFacet2, TOrderFormFacet3, TOrderFormFacet4>(opContext.OF as TOrderFormFacet1, opContext.OF as TOrderFormFacet2, opContext.OF as TOrderFormFacet3, opContext.OF as TOrderFormFacet4), opContext);
        }

        protected virtual StepReturn Process(MultiFacet<TOrderFormFacet1, TOrderFormFacet2, TOrderFormFacet3, TOrderFormFacet4> orderFormMultiFacets)
        {
            return StepReturn.Next;
        }

        protected virtual StepReturn Process(MultiFacet<TOrderFormFacet1, TOrderFormFacet2, TOrderFormFacet3, TOrderFormFacet4> orderFormMultiFacets, OPContext opContext)
        {
            return Process(orderFormMultiFacets);
        }
    }

    public abstract class MultiFacetStep<TOrderFormFacet1, TOrderFormFacet2, TOrderFormFacet3, TOrderFormFacet4, TOrderFormFacet5> : Step<IOF, NullStepData, NullGroupData>
        where TOrderFormFacet1 : class, IOF
        where TOrderFormFacet2 : class, IOF
        where TOrderFormFacet3 : class, IOF
        where TOrderFormFacet4 : class, IOF
        where TOrderFormFacet5 : class, IOF
    {
        public override IEnumerable<Type> DataType
        {
            get { return Enumerable.Empty<Type>(); }
        }

        public override sealed StepReturn Check(OPContext opContext)
        {
            return Check(new MultiFacet<TOrderFormFacet1, TOrderFormFacet2, TOrderFormFacet3, TOrderFormFacet4, TOrderFormFacet5>(opContext.OF as TOrderFormFacet1, opContext.OF as TOrderFormFacet2, opContext.OF as TOrderFormFacet3, opContext.OF as TOrderFormFacet4, opContext.OF as TOrderFormFacet5), opContext);
        }

        protected virtual StepReturn Check(MultiFacet<TOrderFormFacet1, TOrderFormFacet2, TOrderFormFacet3, TOrderFormFacet4, TOrderFormFacet5> orderFormMultiFacets)
        {
            return StepReturn.Process;
        }

        protected virtual StepReturn Check(MultiFacet<TOrderFormFacet1, TOrderFormFacet2, TOrderFormFacet3, TOrderFormFacet4, TOrderFormFacet5> orderFormMultiFacets, OPContext opContext)
        {
            return Check(orderFormMultiFacets);
        }

        public override sealed StepReturn Process(OPContext opContext)
        {
            return Process(new MultiFacet<TOrderFormFacet1, TOrderFormFacet2, TOrderFormFacet3, TOrderFormFacet4, TOrderFormFacet5>(opContext.OF as TOrderFormFacet1, opContext.OF as TOrderFormFacet2, opContext.OF as TOrderFormFacet3, opContext.OF as TOrderFormFacet4, opContext.OF as TOrderFormFacet5), opContext);
        }

        protected virtual StepReturn Process(MultiFacet<TOrderFormFacet1, TOrderFormFacet2, TOrderFormFacet3, TOrderFormFacet4, TOrderFormFacet5> orderFormMultiFacets)
        {
            return StepReturn.Next;
        }

        protected virtual StepReturn Process(MultiFacet<TOrderFormFacet1, TOrderFormFacet2, TOrderFormFacet3, TOrderFormFacet4, TOrderFormFacet5> orderFormMultiFacets, OPContext opContext)
        {
            return Process(orderFormMultiFacets);
        }
    }

    public abstract class MultiFacetStep<TOrderFormFacet1, TOrderFormFacet2, TOrderFormFacet3, TOrderFormFacet4, TOrderFormFacet5, TOrderFormFacet6> : Step<IOF, NullStepData, NullGroupData>
        where TOrderFormFacet1 : class, IOF
        where TOrderFormFacet2 : class, IOF
        where TOrderFormFacet3 : class, IOF
        where TOrderFormFacet4 : class, IOF
        where TOrderFormFacet5 : class, IOF
        where TOrderFormFacet6 : class, IOF
    {
        public override IEnumerable<Type> DataType
        {
            get { return Enumerable.Empty<Type>(); }
        }

        public override sealed StepReturn Check(OPContext opContext)
        {
            return Check(new MultiFacet<TOrderFormFacet1, TOrderFormFacet2, TOrderFormFacet3, TOrderFormFacet4, TOrderFormFacet5, TOrderFormFacet6>(opContext.OF as TOrderFormFacet1, opContext.OF as TOrderFormFacet2, opContext.OF as TOrderFormFacet3, opContext.OF as TOrderFormFacet4, opContext.OF as TOrderFormFacet5, opContext.OF as TOrderFormFacet6), opContext);
        }

        protected virtual StepReturn Check(MultiFacet<TOrderFormFacet1, TOrderFormFacet2, TOrderFormFacet3, TOrderFormFacet4, TOrderFormFacet5, TOrderFormFacet6> orderFormMultiFacets)
        {
            return StepReturn.Process;
        }

        protected virtual StepReturn Check(MultiFacet<TOrderFormFacet1, TOrderFormFacet2, TOrderFormFacet3, TOrderFormFacet4, TOrderFormFacet5, TOrderFormFacet6> orderFormMultiFacets, OPContext opContext)
        {
            return Check(orderFormMultiFacets);
        }

        public override sealed StepReturn Process(OPContext opContext)
        {
            return Process(new MultiFacet<TOrderFormFacet1, TOrderFormFacet2, TOrderFormFacet3, TOrderFormFacet4, TOrderFormFacet5, TOrderFormFacet6>(opContext.OF as TOrderFormFacet1, opContext.OF as TOrderFormFacet2, opContext.OF as TOrderFormFacet3, opContext.OF as TOrderFormFacet4, opContext.OF as TOrderFormFacet5, opContext.OF as TOrderFormFacet6), opContext);
        }

        protected virtual StepReturn Process(MultiFacet<TOrderFormFacet1, TOrderFormFacet2, TOrderFormFacet3, TOrderFormFacet4, TOrderFormFacet5, TOrderFormFacet6> orderFormMultiFacets)
        {
            return StepReturn.Next;
        }

        protected virtual StepReturn Process(MultiFacet<TOrderFormFacet1, TOrderFormFacet2, TOrderFormFacet3, TOrderFormFacet4, TOrderFormFacet5, TOrderFormFacet6> orderFormMultiFacets, OPContext opContext)
        {
            return Process(orderFormMultiFacets);
        }
    }

    public abstract class MultiFacetStep<TOrderFormFacet1, TOrderFormFacet2, TOrderFormFacet3, TOrderFormFacet4, TOrderFormFacet5, TOrderFormFacet6, TOrderFormFacet7> : Step<IOF, NullStepData, NullGroupData>
        where TOrderFormFacet1 : class, IOF
        where TOrderFormFacet2 : class, IOF
        where TOrderFormFacet3 : class, IOF
        where TOrderFormFacet4 : class, IOF
        where TOrderFormFacet5 : class, IOF
        where TOrderFormFacet6 : class, IOF
        where TOrderFormFacet7 : class, IOF
    {
        public override IEnumerable<Type> DataType
        {
            get { return Enumerable.Empty<Type>(); }
        }

        public override sealed StepReturn Check(OPContext opContext)
        {
            return Check(new MultiFacet<TOrderFormFacet1, TOrderFormFacet2, TOrderFormFacet3, TOrderFormFacet4, TOrderFormFacet5, TOrderFormFacet6, TOrderFormFacet7>(opContext.OF as TOrderFormFacet1, opContext.OF as TOrderFormFacet2, opContext.OF as TOrderFormFacet3, opContext.OF as TOrderFormFacet4, opContext.OF as TOrderFormFacet5, opContext.OF as TOrderFormFacet6, opContext.OF as TOrderFormFacet7), opContext);
        }

        protected virtual StepReturn Check(MultiFacet<TOrderFormFacet1, TOrderFormFacet2, TOrderFormFacet3, TOrderFormFacet4, TOrderFormFacet5, TOrderFormFacet6, TOrderFormFacet7> orderFormMultiFacets)
        {
            return StepReturn.Process;
        }

        protected virtual StepReturn Check(MultiFacet<TOrderFormFacet1, TOrderFormFacet2, TOrderFormFacet3, TOrderFormFacet4, TOrderFormFacet5, TOrderFormFacet6, TOrderFormFacet7> orderFormMultiFacets, OPContext opContext)
        {
            return Check(orderFormMultiFacets);
        }

        public override sealed StepReturn Process(OPContext opContext)
        {
            return Process(new MultiFacet<TOrderFormFacet1, TOrderFormFacet2, TOrderFormFacet3, TOrderFormFacet4, TOrderFormFacet5, TOrderFormFacet6, TOrderFormFacet7>(opContext.OF as TOrderFormFacet1, opContext.OF as TOrderFormFacet2, opContext.OF as TOrderFormFacet3, opContext.OF as TOrderFormFacet4, opContext.OF as TOrderFormFacet5, opContext.OF as TOrderFormFacet6, opContext.OF as TOrderFormFacet7), opContext);
        }

        protected virtual StepReturn Process(MultiFacet<TOrderFormFacet1, TOrderFormFacet2, TOrderFormFacet3, TOrderFormFacet4, TOrderFormFacet5, TOrderFormFacet6, TOrderFormFacet7> orderFormMultiFacets)
        {
            return StepReturn.Next;
        }

        protected virtual StepReturn Process(MultiFacet<TOrderFormFacet1, TOrderFormFacet2, TOrderFormFacet3, TOrderFormFacet4, TOrderFormFacet5, TOrderFormFacet6, TOrderFormFacet7> orderFormMultiFacets, OPContext opContext)
        {
            return Process(orderFormMultiFacets);
        }
    }
}
