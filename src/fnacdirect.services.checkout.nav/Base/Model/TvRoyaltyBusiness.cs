using FnacDirect.OrderPipe.Base.DAL;
using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.Technical.Framework.CoreServices;
using FnacDirect.Technical.Framework.Utils;
using FnacDirect.TVRoyaltiesAddressNormalization;
using System;
using System.Collections.Generic;

namespace FnacDirect.OrderPipe.Base.Business
{
    public class TvRoyaltyBusiness : ITvRoyaltyBusiness
    {
        private readonly IOrderInsertionDal _orderInsertionDal;
        private readonly ITVRoyaltyDAL _royaltyDAL;
        private readonly INormalizer _normalizer;

        public TvRoyaltyBusiness(ITVRoyaltyDAL tvRoyaltyDAL, IOrderInsertionDal orderInsertionDal, INormalizer normalizer)
        {
            _orderInsertionDal = orderInsertionDal;
            _royaltyDAL = tvRoyaltyDAL;
            _normalizer = normalizer;
        }

        public Dictionary<int, string> GetTelevisualRoyaltyWays(string defaultLabel)
        {
            return _royaltyDAL.GetTelevisualRoyaltyWays(defaultLabel);
        }

        public void AddTVRoyalty(int orderFk, int accountFk, string company, string firstName, string lastName, string addressLine1, string addressLine2, string addressLine3, string zipCode, string city, string state, string country, string tel, string cel, string fax, string dOB, string bLocation, int genderFk, string streetNo, string streetRange, string streetType, int tVUseFk, int tVUse2Fk, string bState, string mark, int quantity, int orderDetailFk)
        {
            _orderInsertionDal.AddTVRoyalty(orderFk, accountFk, company, firstName, lastName, addressLine1, addressLine2, addressLine3, zipCode, city, state, country, tel, cel, fax, dOB, bLocation, genderFk, streetNo, streetRange, streetType, tVUseFk, tVUse2Fk, bState, mark, quantity, orderDetailFk);
        }

        public BillingAddress GetOrderBillingAddressTvRoyalty(string orderRef,
            out int errorCode, out int orderId, out int pridTv, out int quantity,
            out int orderDetailPk, out int accountId)
        {
            return _royaltyDAL.GetOrderBillingAddressTvRoyalty(orderRef, out errorCode, out orderId, out pridTv, out quantity, out orderDetailPk, out accountId);
        }

        public NormalizerResult NormalizeAddress(string address)
        {
            try
            {
                return _normalizer.Normalize(address);
            }
            catch (Exception ex)
            {
                Logging.Current.WriteWarning(new Exception("TvRoyaltyNormalizationService.Normalize", ex));
            }

            return null;
        }

        /// <summary>
        /// Est-ce que l'article a besoin du formulaire (obligatoire) de la redevance TV (pour la livraison ClickAndCollect et 2HC)
        /// </summary>
        public bool DoesArticleNeedTVRoyaltyForm(Article article, RangeSet<int> tvArticleTypesId)
        {
            if(!(article is StandardArticle standardArticle))
            {
                return false;
            }

            if (!DoesArticleNeedTVRoyalty(standardArticle, tvArticleTypesId))
            {
                return false;
            }

            if (!standardArticle.CollectInShop && !standardArticle.SteedFromStore)
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// Est-ce que l'article a besoin de la redevance TV de l'OTY (hors ClickAndCollect et 2HC)
        /// </summary>
        public bool DoesArticleNeedTVRoyaltyOTY(Article article, RangeSet<int> tvArticleTypesId)
        {
            if (!DoesArticleNeedTVRoyalty(article, tvArticleTypesId))
            {
                return false;
            }

            if (article.CollectInShop || article.SteedFromStore)
            {
                return false;
            }

            return true;
        }

        public bool DoesArticleNeedTVRoyalty(Article article, RangeSet<int> tvArticleTypesId)
        {
            if (!(article is StandardArticle) && !(article is MarketPlaceArticle))
            {
                return false;
            }

            if (!article.TypeId.HasValue || !tvArticleTypesId.Contains(article.TypeId.Value))
            {
                return false;
            }

            return true;
        }
    }
}
