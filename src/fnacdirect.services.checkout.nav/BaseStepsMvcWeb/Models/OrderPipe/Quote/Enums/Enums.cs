using System;
using System.Collections.Generic;
using System.Text;

namespace FnacDirect.Quote.Model.Enums
{

    public enum WebAppliContext
    {
        FnacProQuote=0,
        WebFront=1
    }

    public enum QuoteProductValidation
    {
        Validated = 0,
        New = 1,
        ToUpdate = 2
    }

    /// <summary>
    /// D�finie le pourquoi de la demande de devis
    /// </summary>
    public enum QuoteDemandRaisonEnum
    {
        OrderNewCustomer = 0,
        OrderVolume = 1,
        OrderIndisponibility = 2,
        OrderMax = 3,
        CallCenter = 4,      
        NewQuote =5
    }


    public enum QuoteType
    {
        Standard = 0,
        Custom = 1
    }

    public enum ModeViewEnum
    {
        DetailView = 0,
        EditView = 1,
        NoEdit = 2,
        DetailViewPipe = 3,
        CCVView = 4,
        Void = 5
    }

    /// <summary>
    /// Un devis sera � Valid� �, � Expir�, � Refus� � ou � En attente de validation �
    /// </summary>
    public enum QuoteStatusEnum
    {
        /// <summary>
        /// Cr��
        /// </summary>
        Created = 0,
        /// <summary>
        /// A valider = en attente de validation
        /// </summary>
        ToValidate = 1,
        /// <summary>
        /// Valid�
        /// </summary>
        Validated = 2,
        /// <summary>
        /// Refus�
        /// </summary>
        Refused = 3,
        /// <summary>
        /// Modifi�
        /// </summary>
        Modified = 4,
        /// <summary>
        /// Abandonn�
        /// </summary>
        Aborted = 5,
        /// <summary>
        /// Expir�
        /// </summary>
        TimeExpired = 6,
        /// <summary>
        /// Command� (transform� en commande)
        /// </summary>
        Ordred = 7,
        /// <summary>
        /// Non disponible
        /// </summary>
        Unavailable = 8,
        /// <summary>
        /// Annul� par l'utilisateur
        /// </summary>
        CancelByUser = 9,
        
    }



    public enum QuoteMotif
    {
        Void = 0,
        NewCustomer = 1,
        BigVolume = 2,
        StockDisponibility = 4,
        BigThanMaxQuotePrice = 8,
        QuoteDateHasExpirated = 16,
        AwaitingForKbis = 32
        //nouveau client 
        //commande volumineuse
        //disponibilit� stock 
        //montant devis important
    }

    public enum QuoteEventsEnum
    {
        Add = 0,
        Validate = 1,
        Reject = 2,
        Delete = 3,
        Update = 4,
        UpdatePrice = 5,
        UpdateRemise = 6,
        TransformToCart = 7,
        Save = 8,
        UpdateExpirationDate = 9,
        FirstLine = 99
    }

    public enum ActiveModeEnum
    {
        All = 0,
        CommandOnly = 1,
        SubmitOnly = 2,
        /// <summary>
        /// Needs commercial validation
        /// </summary>
        SubmitCommandDisabled = 3
    }

    public enum QuoteArticleTypeEnum
    {
        CCV = 0,
        Other = 1,
        Service = 2
    }

    public enum QuoteMenuButton 
    {
        Void = 0,
        Print = 1,
        ApplyDiscount = 2,
        Validate = 4,
        Reject = 8,
        SubmitOrder = 16,
        SubmitOrderDisable = 32,
        SaveOrder = 64,
        AddAdress = 128,
        ValidateAddress = 256,
        Update = 512,
        AddQuoteContentToCart = 1024

        
    }    
}
