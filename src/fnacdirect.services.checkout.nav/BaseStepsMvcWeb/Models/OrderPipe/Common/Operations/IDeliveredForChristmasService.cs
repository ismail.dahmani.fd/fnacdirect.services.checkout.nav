using System;
using System.Collections.Generic;
using FnacDirect.OrderPipe.Base.Model;

namespace FnacDirect.OrderPipe.Base.Business.Operations
{
    public interface IDeliveredForChristmasService
    {
        string GetWordingWithAvailibility(IEnumerable<Article> articles, TypeWording typeWording);
        DateTime? GetEventDateOfCurrentPhase();
        bool IsSplitAutoAndFree();
        string GetPictoUiresourceKey();
        string GetBasketUiresourceKey();
        string GetShippingWording();
        bool CanArticleBeDelivredBeforChristmas(Article article);
        List<int> GetElligibleShippingMethods();
        bool HasCurrentPhase();
    }
}
