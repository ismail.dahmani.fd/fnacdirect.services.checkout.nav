using System;
using System.Collections.Generic;
using System.Text;
using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.OrderPipe.Base.Model.LineGroups;

namespace FnacDirect.Quote.Model
{
    public class AddressOrderInfo
    {
        #region attributs

        private LineGroupInfo _orderInfo;
        private int _addressId;

        #endregion attributs

        #region constructeur

        /// <summary>
        /// constructeur par d�faut
        /// </summary>
        public AddressOrderInfo()
        {
        }

        public AddressOrderInfo(int addressId, LineGroupInfo orderInfo)
        {
            _addressId = addressId;
            _orderInfo = orderInfo;
        }

        #endregion constructeur

        #region properties

        public LineGroupInfo OrderInfo
        {
            get{return _orderInfo; }
            set { _orderInfo = value; }
        }

        public int AddressId
        {
            get { return _addressId; }
            set { _addressId = value; }
        }

        #endregion properties
    }

    public class AddressOrderInfoCollection : List<AddressOrderInfo>
    {
        public AddressOrderInfoCollection()
        {
        }
    }
}
