using System.Xml.Serialization;
using FnacDirect.OrderPipe.Core.Model.Config;

namespace FnacDirect.OrderPipe.Config
{
	public class SetErrorForwardStep : Core.Model.Config.BaseStep
    {
		public SetErrorForwardStep()
		{
			Class = "FnacDirect.OrderPipe.SetErrorForwardStep";
		}

		[XmlAttribute("forwardstep")]
		public string ForwardStep
		{
			set
			{
				Parameters["forward.step"] = value;
			}
			get
			{
				return Parameters["forward.step"];
			}
		}
	}
}
