using System;

namespace FnacDirect.OrderPipe.Base.Model
{
    /// <summary>
    /// Liste des groupe de champs utilisés pour les requêtes Ogone.
    /// Initiallement, c'est utilisé pour Ogone 3DS V2: Afin de pouvoir filtrer le flux de données par carte de crédits et pays
    /// </summary>
    [Flags]
    public enum OgoneFieldGroup : int
    {
        None = 0,
        All = ~0,

        CompletionId = 1,
        Cof = 2,
        Auth3DSAmount = 4,
        Browser = 8,
        Exemption = 16,
        LRM = 32
    }
}
