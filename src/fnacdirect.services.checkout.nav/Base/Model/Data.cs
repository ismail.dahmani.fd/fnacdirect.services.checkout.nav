﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace FnacDirect.OrderPipe
{
    /// <summary>
    /// Classe de base pour les données privées de l'orderform
    /// </summary>
    public abstract class Data
    {
        /// <summary>
        /// Identifiant de la donnée (nom de l'étape ou nom du groupe)
        /// </summary>
        [XmlAttribute]
        public string Name { get; set; }
    };

    public abstract class TransientData : Data
    {

    }

    /// <summary>
    /// Classe abstraite pour les données privées d'un groupe d'étape, sur lesquelle il est 
    /// possible de poser des contraintes.
    /// </summary>
    /// 
    public abstract class GroupData : Data
    {
        internal ConstraintList constraints;

        /// <summary>
        /// Liste des contraintes appliquées sur les données
        /// </summary>
        public ConstraintList Constraints
        {
            get { return constraints; }
            set { constraints = value; }
        }

        /// <summary>
        /// Ajoute une contrainte sur les données du groupe
        /// </summary>
        /// <param name="step">Nom de l'étape contraignante</param>
        /// <param name="name">Nom de la contrainte</param>
        /// <returns>La nouvelle contrainte</returns>
        public Constraint AddConstraint(string step, string name)
        {
            var c = new Constraint
            {
                Name = name,
                ConstrainerStepName = step
            };

            if (Constraints == null)
                Constraints = new ConstraintList();
            Constraints.Add(c);
            return c;
        }

    }

    /// <summary>
    /// 
    /// </summary>
    public abstract class CustomerData : GroupData
    {
        
        /// <summary>
        /// UID de client associé au data
        /// </summary>
        [XmlAttribute]
        public string UID { get; set; }
    }

    /// <summary>
    /// Classe abstraite pour les données privées d'une étape
    /// </summary>
    public abstract class StepData : Data
    {
    }

    /// <summary>
    /// Classe concrète vide à utiliser avec les étapes qui n'ont pas besoin de données privées
    /// </summary>
    public sealed class NullStepData : StepData
    {
    }

    /// <summary>
    /// Classe concrète vide à utiliser avec les étapes qui n'ont pas besoin de données de groupe privées
    /// </summary>
    public sealed class NullGroupData : GroupData
    {
    }

    /// <summary>
    /// Liste de données, indexée par le nom de la donnée (nom de l'étape ou nom du groupe)
    /// </summary>
    public class DataList : KeyedCollection<string, Data>
    {
        protected override string GetKeyForItem(Data item)
        {
            if (item == null)
                throw new ArgumentNullException("item");

            return item.Name;
        }

        public T GetByType<T>() where T : Data
        {
            foreach (var d in this)
                if (d is T) return d as T;
            return null;
        }
    }
}
