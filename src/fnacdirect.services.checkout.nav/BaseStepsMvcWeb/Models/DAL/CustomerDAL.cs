using FnacDirect.Contracts.Online.Model;
using FnacDirect.OrderPipe.Base.BaseModel.Model;
using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.OrderPipe.CM.Model;
using FnacDirect.Technical.Framework.CoreServices;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace FnacDirect.OrderPipe.Base.DAL
{
    public class CustomerDAL : ICustomerDAL
    {
        private const string DatabaseSqlCustomer = "SqlCustomer";

        /// <summary>
        /// 
        /// </summary>
        /// <param name="userGuid"></param>
        /// <returns></returns>
        public ShippingAddress GetDefaultShippingAddress(string userGuid)
        {
            ShippingAddress retValue = null;
            try
            {
                using (var database = Database.CreateInstance(DatabaseSqlCustomer))
                {
                    using (var command = database.GetStoredProcCommand("spPL_ORD_GET_DefaultShippingAddress"))
                    {
                        database.AddInParameter(command, "vstrAccountRef", SqlDbType.VarChar, userGuid);
                        using (var reader = database.ExecuteReader(command))
                        {
                            while (reader.Read())
                            {
                                var key = DataReaderHelper.GetField<int?>(reader, "SA_Pk");
                                if (key.HasValue)
                                {
                                    ShippingAddress shippingAddress = null;
                                    var typeShippingAddress = DataReaderHelper.GetField<int>(reader, "SA_Type");
                                    switch (typeShippingAddress)
                                    {
                                        case 1:
                                            shippingAddress = new PostalAddress
                                            {
                                                Identity = key.Value
                                            };
                                            ((PostalAddress)shippingAddress).Reference = DataReaderHelper.GetField<string>(reader, "_SA_Reference");
                                            shippingAddress.InternalId = DataReaderHelper.GetField<int?>(reader, "_SA_InternalId");
                                            break;
                                        case 2:
                                            shippingAddress = new RelayAddress
                                            {
                                                Identity = DataReaderHelper.GetField<int?>(reader, "_SA_InternalId"),
                                                InternalId = key.Value,
                                                Reference = DataReaderHelper.GetField<string>(reader, "_SA_Reference"),
                                                AddressBookReference = DataReaderHelper.GetField<string>(reader, "_SA_Reference")
                                            };
                                            break;
                                        case 3:
                                            shippingAddress = new ShopAddress
                                            {
                                                Identity = key.Value,
                                                InternalId = DataReaderHelper.GetField<int?>(reader, "_SA_InternalId")
                                            };
                                            break;
                                        default:
                                            shippingAddress = null;
                                            break;
                                    }
                                    if (shippingAddress != null)
                                    {
                                        if (typeShippingAddress != 2)
                                        {
                                            shippingAddress.AddressBookReference = GetAddressBookReference(shippingAddress.Identity.Value);
                                        }
                                        retValue = shippingAddress;
                                    }
                                }
                                else
                                {
                                    retValue = null;
                                    break;
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw DALException.BuildException(ex, DALRange.Customer, 2);
            }
            return retValue;
        }

        private string GetAddressBookReference(int addressBookId)
        {
            var returnvalue = string.Empty;
            try
            {
                using (var database = Database.CreateInstance(DatabaseSqlCustomer))
                {
                    using (var command = database.GetStoredProcCommand("spPL_ORD_GET_AddressBookReference"))
                    {
                        database.AddInParameter(command, "AddressBookId", SqlDbType.Int, addressBookId);
                        var sqlResult = command.ExecuteScalar();
                        if (sqlResult != null)
                            returnvalue = ((Guid)sqlResult).ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                throw DALException.BuildException(ex, DALRange.Customer, 3);
            }
            return returnvalue;
        }

        public bool AddBankAccountNumber(int accountId, string bankAccountNumber)
        {
            var returnValue = false;
            try
            {
                using (var database = Database.CreateInstance(DatabaseSqlCustomer))
                {
                    using (var command = database.GetStoredProcCommand("spPipe_AddBankAccountNumber"))
                    {
                        database.AddInParameter(command, "nAccountFk", SqlDbType.Int, accountId);
                        database.AddInParameter(command, "vstrIBAN", SqlDbType.VarChar, bankAccountNumber);
                        database.ExecuteNonQuery(command);
                        returnValue = true;
                    }
                }
            }
            catch (Exception ex)
            {
                throw DALException.BuildException(ex, DALRange.Customer, 11);
            }
            return returnValue;

        }

        public string GetBankAccountNumber(int accountId)
        {
            string bankAccountNumber = null;

            try
            {
                using (var database = Database.CreateInstance(DatabaseSqlCustomer))
                {
                    using (var command = database.GetStoredProcCommand("spPipe_GetBankAccountNumber"))
                    {
                        database.AddInParameter(command, "nAccountPK", SqlDbType.Int, accountId);
                        if (database.ExecuteScalar(command) != null)
                        {
                            bankAccountNumber = database.ExecuteScalar(command).ToString();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw DALException.BuildException(ex, DALRange.Customer, 12);
            }
            return bankAccountNumber;

        }

        public CreditCardBillingMethod GetInformationCreditCardBillingMethod(string userGuid, int? identity, bool? numByPhone)
        {
            var creditCard = new CreditCardBillingMethod();
            try
            {
                using (var database = Database.CreateInstance(DatabaseSqlCustomer))
                {
                    if (identity.HasValue && identity.Value > 0)
                    {
                        using (var command = database.GetStoredProcCommand("spPL_ORD_GET_CreditcardById"))
                        {
                            database.AddInParameter(command, "vstrAccountReference", SqlDbType.VarChar, userGuid);
                            database.AddInParameter(command, "nCCId", SqlDbType.Int, identity.Value);
                            using (var reader = database.ExecuteReader(command))
                            {
                                while (reader.Read())
                                {
                                    creditCard = LoadReader(reader);
                                }
                            }
                        }
                    }
                    else
                    {
                        if (numByPhone.HasValue)
                        {
                            creditCard = GetCreditCardByPhoneInformations();
                        }
                        else
                        {
                            using (var command = database.GetStoredProcCommand("spPL_ORD_GET_DefaultCreditCard"))
                            {
                                database.AddInParameter(command, "vstrAccountReference", SqlDbType.VarChar, userGuid);
                                using (var reader = database.ExecuteReader(command))
                                {
                                    while (reader.Read())
                                    {
                                        creditCard = LoadReader(reader);
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw DALException.BuildException(ex, DALRange.Customer, 6);
            }
            return creditCard;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        private CreditCardBillingMethod GetCreditCardByPhoneInformations()
        {
            var creditCard = new CreditCardBillingMethod
            {
                Alias = "ByPhone",
                Reference = "ByPhone",
                TypeId = 10,
                CardTypeSIPSString = "",
                TypeLabel = "",
                CardNumber = "",
                VerificationCode = "",
                CardHolder = "",
                CardExpirationDate = "",
                Status = 5
            };
            return creditCard;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="reader"></param>
        /// <returns></returns>
        private static CreditCardBillingMethod LoadReader(SqlDataReader reader)
        {
            var creditCard = new CreditCardBillingMethod
            {
                Identity = DataReaderHelper.GetField<int?>(reader, "CC_Pk"),
                Reference = DataReaderHelper.GetField<string>(reader, "_CC_Reference"),
                Alias = DataReaderHelper.GetField<string>(reader, "_CC_Alias"),
                TypeId = DataReaderHelper.GetField<int?>(reader, "_CC_CardTypeFk"),
                CardTypeSIPSString = DataReaderHelper.GetField<string>(reader, "_CC_CardTypeSIPSString"),
                TypeLabel = DataReaderHelper.GetField<string>(reader, "_CC_CardTypeLabel"),
                CardNumber = DataReaderHelper.GetField<string>(reader, "_CC_CardNumber"),
                CardHolder = DataReaderHelper.GetField<string>(reader, "_CC_CardHolder"),
                CardExpirationDate = DataReaderHelper.GetField<string>(reader, "_CC_CardExpireDate"),
                Status = DataReaderHelper.GetField<int?>(reader, "_CC_Status"),
                CryptoFlag = (DataReaderHelper.GetField<int>(reader, "_CC_CryptoFlag") == 1)
            };
            return creditCard;
        }

        /// <summary>
        /// Calls Customer database with account reference to retrieve
        /// - Internal Id
        /// - Account Status
        /// </summary>
        /// <param name="userGuid">User Reference</param>
        /// <returns>AccountId object, which contains the AccountPk and AccountStatus properties</returns>
        public AccountId GetAccountId(string userGuid)
        {
            var accountId = new AccountId()
            {
                AccountPk = -1
            };

            try
            {
                using (var database = Database.CreateInstance(DatabaseSqlCustomer))
                {
                    using (var command = database.GetStoredProcCommand("spPL_ACC_GET_AccountId"))
                    {
                        database.AddInParameter(command, "vstrAccountReference", SqlDbType.VarChar, userGuid);
                        using (var reader = database.ExecuteReader(command))
                        {
                            while (reader.Read())
                            {
                                accountId.AccountPk = DataReaderHelper.GetField<int>(reader, "_AccountPk");
                                accountId.AccountStatus = DataReaderHelper.GetField<int>(reader, "_AccountStatus");
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw DALException.BuildException(ex, DALRange.Customer, 9);
            }
            return accountId;
        }

        public List<CustomerWithAddress> FindCustomers(Dictionary<CustomerSearchField, string> criterias)
        {
            var ret = new List<CustomerWithAddress>();
            using (var database = Database.CreateInstance(DatabaseSqlCustomer))
            {
                using (var command = database.GetStoredProcCommand("spCUST_Search_Customers_Adresse"))
                {
                    BuildParametersFindCustomers(database, command, criterias);
                    using (var reader = database.ExecuteReader(command))
                    {
                        while (reader.Read())
                        {
                            var cus = new FnacDirect.Contracts.Online.Model.Customer
                            {
                                ID = DataReaderHelper.GetField<string>(reader, "AccountReference")
                            };
                            var inf = new FnacDirect.Contracts.Online.Model.UserInfo
                            {
                                AdherentNumber = DataReaderHelper.GetField<string>(reader, "AdherentNumber"),
                                Email = DataReaderHelper.GetField<string>(reader, "EMail"),
                                FirstName = DataReaderHelper.GetField<string>(reader, "FirstName"),
                                Gender = new Gender(-1, DataReaderHelper.GetField<string>(reader, "GenderLabel")),
                                LastName = DataReaderHelper.GetField<string>(reader, "LastName")
                            };
                            cus.Info = inf;

                            var ba = new BillingAddress
                            {
                                Phone = DataReaderHelper.GetField<string>(reader, "Tel")
                            };
                            ;
                            ba.ZipCode = DataReaderHelper.GetField<string>(reader, "ZipCode");
                            ba.City = DataReaderHelper.GetField<string>(reader, "City");
                            ba.AddressLine1 = DataReaderHelper.GetField<string>(reader, "AddressLine1");
                            ba.AddressLine2 = DataReaderHelper.GetField<string>(reader, "AddressLine2");
                            ba.AddressLine3 = DataReaderHelper.GetField<string>(reader, "AddressLine3");

                            var cwa = new CustomerWithAddress
                            {
                                Customer = cus,
                                BillingAddress = ba
                            };

                            ret.Add(cwa);
                        }
                    }
                }
            }
            return ret;
        }

        private void BuildParametersFindCustomers(Database database, SqlCommand command, Dictionary<CustomerSearchField, string> criterias)
        {
            foreach (var criteria in criterias.Keys)
            {
                var dbType = SqlDbType.VarChar;
                var dbValue = criterias[criteria];
                var paramName = string.Empty;
                switch (criteria)
                {
                    case CustomerSearchField.FirstName:
                        paramName = "prmFirstName";
                        break;
                    case CustomerSearchField.LastName:
                        paramName = "prmLastName";
                        break;
                    case CustomerSearchField.EmailAddress:
                        paramName = "prmEMail";
                        break;
                    case CustomerSearchField.MembershipNumber:
                        paramName = "prmAdherentNumber";
                        break;
                    case CustomerSearchField.PostalCode:
                        paramName = "prmZipCode";
                        break;
                    default:
                        throw new IndexOutOfRangeException("Invalid criteria : " + criteria);
                }
                database.AddInParameter(command, paramName, dbType, dbValue);
            }
        }

        /// <summary>
        /// Permet de récupérer la liste des emails correspondant
        /// au pattern de recherche prenom.nomXX@domain
        /// </summary>
        /// <param name="firstname">Prénom du client</param>
        /// <param name="lastname">Nom du client</param>
        /// <param name="domain">Nom de domain des emails autogénéré</param>
        public List<string> SearchAutogeneratedMailAccount(string firstname, string lastname, string domain)
        {
            var mails = new List<string>();
            try
            {
                using (var database = Database.CreateInstance(DatabaseSqlCustomer))
                {
                    using (var command = database.GetStoredProcCommand("spPipe_SearchAutogeneratedMailAccount"))
                    {
                        database.AddInParameter(command, "firstname", SqlDbType.VarChar, firstname);
                        database.AddInParameter(command, "lastname", SqlDbType.VarChar, lastname);
                        database.AddInParameter(command, "domain", SqlDbType.VarChar, domain);

                        using (var reader = database.ExecuteReader(command))
                        {
                            while (reader.Read())
                                mails.Add(DataReaderHelper.GetField<string>(reader, "fld_account_email"));
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw DALException.BuildException(ex, DALRange.Customer, 13);
            }
            return mails;
        }

        public List<Partner> GetPartners()
        {
            var partners = new List<Partner>();

            try
            {
                using (var database = Database.CreateInstance(DatabaseSqlCustomer))
                {
                    using (var command = database.GetStoredProcCommand("spBatchB2B_AccountConfig"))
                    {
                        using (var reader = database.ExecuteReader(command))
                        {
                            while (reader.Read())
                            {
                                partners.Add(new Partner
                                {
                                    AccountId = DataReaderHelper.GetField<int>(reader, "fld_abc_account_id_fk"),
                                    ArticleNodes = DataReaderHelper.GetField<string>(reader, "fld_abc_article_nodes"),
                                    OutputPath = DataReaderHelper.GetField<string>(reader, "fld_abc_output_path"),
                                    AdvantageCodes = DataReaderHelper.GetField<string>(reader, "fld_abc_advantage_code"),
                                    AllowAvailabilityOverriding = DataReaderHelper.GetField<bool>(reader, "fld_abc_can_override_availability"),
                                    AllowBillingAddressOverriding = DataReaderHelper.GetField<bool>(reader, "fld_abc_can_override_billing_address"),
                                    OrderType = DataReaderHelper.GetField<int?>(reader, "fld_abc_OrderType"),
                                    AllowPricerPriceOverriding = DataReaderHelper.GetField<bool>(reader, "fld_abc_can_override_pricer_price"),
                                    AllowSelectiveDistribution = DataReaderHelper.GetField<bool>(reader, "fld_abc_can_override_selective_distribution"),
                                    AllowMultipleExternalReference = DataReaderHelper.GetField<bool>(reader, "fld_abc_AllowMultipleExternalReference")
                                });
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw DALException.BuildException(ex, DALRange.Customer, 13);
            }

            return partners;
        }

        public List<State> GetState()
        {
            var returnvalue = new List<State>();

            try
            {
                using (var database = Database.CreateInstance(DatabaseSqlCustomer))
                {
                    using (var command = database.GetStoredProcCommand("spCUST_GetState"))
                    {
                        using (var reader = database.ExecuteReader(command))
                        {
                            while (reader.Read())
                            {
                                returnvalue.Add(new State
                                {
                                    CodeState = DataReaderHelper.GetField<string>(reader, "fld_state_code"),
                                    LabelState = DataReaderHelper.GetField<string>(reader, "fld_state_label")                                
                                });
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw DALException.BuildException(ex, DALRange.Customer, 13);
            }
            return returnvalue;
        }
    }
}
