using System;

namespace FnacDirect.OrderPipe.Base.Business.Helpers
{
    public static class PriceHelpers
    {
        /// <summary>
        /// Retourne le prix arrondi
        /// </summary>
        /// <param name="price"></param>
        /// <returns></returns>
        public static decimal GetRoundedPrice(decimal price)
        {
            return Math.Round(price, 2);
        }

        /// <summary>
        /// Retourne le prix TTC
        /// </summary>
        /// <param name="priceHT">prix HT</param>
        /// <param name="VATRate">taux TVA</param>
        /// <returns></returns>
        public static decimal GetPriceTTC(decimal priceHT, decimal VATRate, int decimals = 2)
        {
            return VATRate == 0 ? priceHT : Math.Round(priceHT * (1 + VATRate / 100), decimals);
        }

        /// <summary>
        /// Retourne le prix HT
        /// </summary>
        /// <param name="priceTTC">prix TTC</param>
        /// <param name="VATRate">taux TVA</param>
        /// <returns></returns>
        public static decimal GetPriceHT(decimal priceTTC, decimal VATRate, int decimals = 2)
        {
            return VATRate == 0 ? priceTTC : Math.Round(priceTTC / (1 + VATRate / 100), decimals);
        }

        /// <summary>
        ///  Retourne le prix HT en choisissant le mode d'arrondi
        /// </summary>
        /// <param name="priceAllTaxesIncluded"></param>
        /// <param name="VatRate"></param>
        /// <param name="decimals"></param>
        /// <param name="midpointRounding"></param>
        /// <returns></returns>
        public static decimal GetPriceWithoutVat (decimal priceAllTaxesIncluded, decimal VatRate,  int decimals = 2, MidpointRounding midpointRounding = MidpointRounding.AwayFromZero)
        {
            return VatRate == 0 ? priceAllTaxesIncluded : Math.Round(priceAllTaxesIncluded / (1 + VatRate / 100), decimals, midpointRounding);
        }

        /// <summary>
        /// Retourne le prix TTC
        /// </summary>
        /// <param name="priceTTC">prix TTC</param>
        /// <param name="VATRate">taux TVA</param>
        /// <returns></returns>
        public static decimal GetPriceTTC(decimal? priceHT, decimal? VATRate)
        {
            if (priceHT == null)
                return 0;
            if (VATRate == null)
                return priceHT.GetValueOrDefault(0);
            return GetPriceTTC(priceHT.Value, VATRate.Value);
        }

        /// <summary>
        /// Retourne le prix HT
        /// </summary>
        /// <param name="priceTTC">prix TTC</param>
        /// <param name="VATRate">taux TVA</param>
        /// <returns></returns>
        public static decimal GetPriceHT(decimal? priceTTC, decimal? VATRate)
        {
            if (priceTTC == null)
                return 0;
            if (VATRate == null)
                return priceTTC.GetValueOrDefault(0);
            return GetPriceHT(priceTTC.Value, VATRate.Value);
        }

        /// <summary>
        /// Retourne le prix du TVA
        /// </summary>
        /// <param name="priceHT">prix HT</param>
        /// <param name="VATRate">taux TVA</param>
        /// <returns></returns>
        private static decimal GetVATPriceByHT(decimal priceHT, decimal VATRate)
        {
            var ttc = GetPriceTTC(priceHT, VATRate);
            return ttc - priceHT;
        }

        /// <summary>
        /// Retourne le prix du TVA
        /// </summary>
        /// <param name="priceTTC">prix TTC</param>
        /// <param name="VATRate">taux TVA</param>
        /// <returns></returns>
        public static decimal GetVATPrice(decimal priceTTC, decimal VATRate)
        {
            var ht = GetPriceHT(priceTTC, VATRate);
            return priceTTC - ht;
        }

        /// <summary>
        /// Retourne le prix du TVA
        /// </summary>
        /// <param name="priceHT">prix HT</param>
        /// <param name="VATRate">taux TVA</param>
        /// <returns></returns>
        public static decimal GetVATPriceByHT(decimal? priceHT, decimal? VATRate)
        {
            return GetVATPriceByHT(priceHT.GetValueOrDefault(0), VATRate.GetValueOrDefault(0));
        }

        /// <summary>
        /// Retourne le prix du TVA
        /// </summary>
        /// <param name="priceTTC">prix TTC</param>
        /// <param name="VATRate">taux TVA</param>
        /// <returns></returns>
        public static decimal GetVATPrice(decimal? priceTTC, decimal? VATRate)
        {
            return GetVATPrice(priceTTC.GetValueOrDefault(0), VATRate.GetValueOrDefault(0));
        }

        public static decimal GetPriceForNonLocalShippment(decimal priceTTC, decimal localVatRate, decimal countryVatRate)
        {
            var noVatPrice = GetPriceHT(priceTTC, localVatRate);
            var countryPriceTTC = GetPriceTTC(noVatPrice, countryVatRate);

            return countryPriceTTC;
        }
    }
}
