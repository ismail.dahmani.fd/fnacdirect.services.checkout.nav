﻿using FnacDirect.Front.WebBusiness.WebMobile;
using FnacDirect.OrderPipe.Base.Model.Facets;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace FnacDirect.OrderPipe.BaseMvc.Web.HtmlHelpers.Pop.Shipping
{
    public static class GeolocErrorWording
    {
        public static string ErrorActivationGeoloc(this HtmlHelper htmlHelper)
        {
            var ressourceErrorDesktop = "orderpipe.pop.shipping.shop.popin.geosearch.noresults.noactivate.content";
            var ressourceErrorMobile = "orderpipe.pop.shipping.shop.popin.geosearch.noresults.noactivate.content.mobile";
            var ressourceErrorMobileIOS = "orderpipe.pop.shipping.shop.popin.geosearch.noresults.noactivate.content.mobile.ios";
            var ressourceErrorMobileAndroid = "orderpipe.pop.shipping.shop.popin.geosearch.noresults.noactivate.content.mobile.android";


            var opContext = DependencyResolver.Current.GetService<OPContext>();

            if (opContext == null)
            {
                return ressourceErrorDesktop;
            }

            var iGotMobileInformations = opContext.OF as IGotMobileInformations;

            if (iGotMobileInformations == null)
            {
                return ressourceErrorDesktop;
            }

            var callingPlatform = iGotMobileInformations.OrderMobileInformations.CallingPlatform;

            if (MobileAppValues.IsIphoneOrIpadApp(callingPlatform))
            {
                return ressourceErrorMobileIOS;
            }
            else if (MobileAppValues.IsAndroidOrTabletApp(callingPlatform))
            {
                return ressourceErrorMobileAndroid;
            }
            else if (htmlHelper.IsMobile())
            {
                return ressourceErrorMobile;
            }

            return ressourceErrorDesktop;
        }
    }
}
