using FnacDirect.Contracts.Online.Model;
using System;
using System.Collections.Generic;

namespace FnacDirect.OrderPipe.Base.BaseModel.Model.OrderDetails
{
    public class OrderDetailArticleLight
    {
        public int Prid { get; set; }

        public string Title { get; set; }

        public string UrlPicture { get; set; }

        public string TheoreticalDeliveryDate { get; set; }

        public int Quantity { get; set; }

        public bool IsService { get; set; }
    }

    public class OrderDetailArticle : OrderDetailArticleLight
    {
        public OrderDetailArticle()
        {
            Services = new List<OrderDetailArticleLight>();
        }

        public List<OrderDetailArticleLight> Services { get; set; }

        public List<Promotion> Promotions { get; set; }

        public int? OrderDetailPk { get; set; }

        public string Ean { get; set; }

        public decimal? Price { get; set; }

        public int? AvailabilityId { get; set; }
        public Guid? OfferRef { get; set; }

        public DateTime? TheoreticalDeliveryStartDate { get; set; }
        public DateTime? TheoreticalDeliveryEndDate { get; set; }
    }
}
