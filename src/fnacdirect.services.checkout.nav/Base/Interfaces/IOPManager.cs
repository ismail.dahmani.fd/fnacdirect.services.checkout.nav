namespace FnacDirect.OrderPipe
{
    /// <summary>
    /// Interface du gestionnaire d'OrderPipe
    /// </summary>
    public interface IOPManager
    {
        /// <summary>
        /// Vide le cache de configuration
        /// </summary>
        void FlushPipeConfig();

        /// <summary>
        /// Retourne l'instance du Pipe correspondant au nom
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        IOP this[string name] { get; }

        IOP LoadConfigurationOnly(string pipeName);

        IOP LoadConfigurationWithStepsOnly(string pipeName);
        

        bool IsKnown(string pipeName);
    }
}
