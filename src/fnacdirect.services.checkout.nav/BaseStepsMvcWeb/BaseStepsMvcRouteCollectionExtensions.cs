using FnacDirect;
using FnacDirect.OrderPipe.BaseMvc.Web.CrossSellBasket;
using FnacDirect.OrderPipe.BaseMvc.Web.LayerBasket;
using FnacDirect.OrderPipe.BaseMvc.Web.Pop;
using FnacDirect.OrderPipe.CoreMvcWeb.Routing;
using FnacDirect.OrderPipe.CoreRouting;
using System.Web.Mvc;

namespace System.Web.Routing
{
    public static class BaseStepsMvcRouteCollectionExtensions
    {
        public static RouteCollection MapOrderPipe(this RouteCollection routeCollection)
        {
            var context = new AreaRegistrationContext("OP - Base Steps MVC", routeCollection);
            context.Namespaces.Add("FnacDirect.OrderPipe.BaseMvc.Web.*");

            var switchProvider = DependencyResolver.Current.GetService<ISwitchProvider>();

            var crossSellBasketAreaRegistrationPart = DependencyResolver.Current.GetService<CrossSellBasketAreaRegistrationPart>();
            crossSellBasketAreaRegistrationPart.RegisterArea(context);

            var layerBasketAreaRegistrationPart = DependencyResolver.Current.GetService<LayerBasketAreaRegistrationPart>();
            layerBasketAreaRegistrationPart.RegisterArea(context);

            if (switchProvider.IsEnabled("orderpipe.pop"))
            {
                var popAreaRegistrationPart = DependencyResolver.Current.GetService<PopAreaRegistrationPart>();
                popAreaRegistrationPart.RegisterArea(context);
            }

            context.MapOrderPipeRoute(
                 name: "OP - Ogone Return Entry Point",
                 url: "orderpipe/ogonereturn",
                 defaults: new object(),
                 constraints: new { isNotMappedToPhysicalFile = new IsNotMappedToPhysicalFile() }
            );

            context.MapOrderPipeRoute(
                 name: "OP - Unicre Return Entry Point",
                 url: "orderpipe/unicrereturn",
                 defaults: new object(),
                 constraints: new { isNotMappedToPhysicalFile = new IsNotMappedToPhysicalFile() }
            );

            context.MapOrderPipeRoute(
                 name: "OP - Orange Return Entry Point",
                 url: "orderpipe/orangereturn",
                 defaults: new object(),
                 constraints: new { isNotMappedToPhysicalFile = new IsNotMappedToPhysicalFile() }
            );

            return routeCollection.MapOrderPipeCore();
        }
    }
}
