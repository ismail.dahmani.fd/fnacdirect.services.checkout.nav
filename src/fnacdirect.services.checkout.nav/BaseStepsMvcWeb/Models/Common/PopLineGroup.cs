using FnacDirect.OrderPipe.Base.LineGroups;
using FnacDirect.OrderPipe.Shipping.Model;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Xml.Serialization;
using System.Text;
using FnacDirect.OrderPipe.Base.BaseModel;

namespace FnacDirect.OrderPipe.Base.Model.LineGroups
{
    public class PopLineGroup : ILineGroup
    {
        private readonly IDictionary<int, decimal> _billingMethodConsolidation
            = new Dictionary<int, decimal>();

        public PopLineGroup()
        {
            UniqueId = Guid.NewGuid().ToString();
            Articles = new ArticleList();
            Seller = new SellerInformation();
            LogisticTypes = new LogisticTypeList();
            GlobalPrices = new GlobalPrices();
            Informations = new PopLineGroupInformations();
            AvailableWraps = new WrapList();
        }

        /// <summary>
        /// Identifiant unique permettant d'identifier le linegroup
        /// </summary>
        public string UniqueId { get; set; }

        public virtual bool IsTemporary
        {
            get
            {
                return false;
            }
        }

        [XmlIgnore]
        public VatCountry VatCountry { get; set; }

        [XmlIgnore]
        public GlobalPrices GlobalPrices { get; set; }

        public SellerInformation Seller { get; set; }

        public ArticleList Articles { get; set; }

        public bool HasShippingMethod(ShippingMethodEvaluation shippingMethodEvaluation, IEnumerable<int> shippingMethodIds)
        {
            var shippingMethodEvaluationGroup = shippingMethodEvaluation.Seller(Seller.SellerId);
            if (shippingMethodEvaluationGroup.HasValue)
            {
                var shippingMethodEvaluationItem = shippingMethodEvaluationGroup.Value.ShippingMethodEvaluationItems
                    .FirstOrDefault(s => s.ShippingMethodEvaluationType == shippingMethodEvaluationGroup.Value.SelectedShippingMethodEvaluationType);

                if (shippingMethodEvaluationItem != null && shippingMethodEvaluationItem.HasChoices)
                {
                    var selectedChoice = shippingMethodEvaluationItem.Choices
                        .FirstOrDefault(c => c.MainShippingMethodId == shippingMethodEvaluationItem.SelectedShippingMethodId);

                    return selectedChoice != null && shippingMethodIds.Contains(selectedChoice.MainShippingMethodId);
                }
            }

            return false;
        }

        public void RemoveArticlesById(IEnumerable<int?> articlesIdToRemove)
        {
            Articles.RemoveAll(a => articlesIdToRemove.Contains(a.ProductID));
        }

        public OrderTypeEnum OrderType { get; set; }

        [XmlIgnore]
        public int OrderOrigin { get; set; }

        public LogisticTypeList LogisticTypes { get; set; }

        public PopLineGroupInformations Informations { get; set; }

        public bool GlobalShippingMethodChoiceIsSetManually { get; set; }

        [XmlIgnore]
        public WrapList AvailableWraps { get; set; }

        [XmlIgnore]
        public bool IsVoluminous { get; set; }

        [XmlIgnore]
        ILineGroupInformations ILineGroupHeader.Informations
        {
            get
            {
                return Informations;
            }
        }

        [XmlIgnore]
        public IDictionary<int, decimal> BillingMethodConsolidation
        {
            get
            {
                return _billingMethodConsolidation;
            }
        }

        public decimal RemainingTotal { get; set; }

        [XmlIgnore]
        public bool HasMarketPlaceArticle
        {
            get
            {
                return (OrderType & OrderTypeEnum.MarketPlace) == OrderTypeEnum.MarketPlace;
            }
        }

        [XmlIgnore]
        public bool HasFnacComArticle
        {
            get
            {
                return (OrderType & OrderTypeEnum.FnacCom) == OrderTypeEnum.FnacCom
                    || (OrderType & OrderTypeEnum.Pro) == OrderTypeEnum.Pro;
            }
        }

        [XmlIgnore]
        public IEnumerable<Article> CollectInStoreArticles
        {
            get
            {
                return Articles.OfType<StandardArticle>().Where(a => a.CollectInShop || a.SteedFromStore);
            }
        }

        [XmlIgnore]
        public bool HasCollectInStoreArticles
        {
            get
            {
                return CollectInStoreArticles.Any();
            }
        }

        [XmlIgnore]
        public bool HasShopArticle
        {
            get
            {
                return (OrderType & OrderTypeEnum.IntraMag) == OrderTypeEnum.IntraMag || (OrderType & OrderTypeEnum.IntraMagPE) == OrderTypeEnum.IntraMagPE;
            }
        }

        [XmlIgnore]
        public bool HasIntraMagPE
        {
            get
            {
                return (OrderType & OrderTypeEnum.IntraMagPE) == OrderTypeEnum.IntraMagPE;
            }
        }

        [XmlIgnore]
        public bool HasNumericalArticles
        {
            get
            {
                return (OrderType & OrderTypeEnum.Numerical) == OrderTypeEnum.Numerical;
            }
        }

        [XmlIgnore]
        public bool HasDematSoftArticles
        {
            get
            {
                return (OrderType & OrderTypeEnum.DematSoft) == OrderTypeEnum.DematSoft;
            }
        }

        [XmlIgnore]
        public bool IsPro
        {
            get
            {
                return (OrderType & OrderTypeEnum.Pro) == OrderTypeEnum.Pro;
            }
        }

        [XmlIgnore]
        public bool IsMixed
        {
            get
            {
                return (HasMarketPlaceArticle && HasFnacComArticle);
            }
        }

        [XmlIgnore]
        public ShippingMethodResult ShippingMethodChoice { get; set; }

        public override string ToString()
        {
            var line = new StringBuilder();
            line.AppendLine(Articles.ToString());
            //line.AppendLine(Seller.ToString());
            //line.AppendLine(LogisticTypes.ToString());
            //line.AppendLine(Informations.ToString());
            //line.AppendLine(AvailableWraps.ToString());
            return line.ToString();
        }
    }
}
