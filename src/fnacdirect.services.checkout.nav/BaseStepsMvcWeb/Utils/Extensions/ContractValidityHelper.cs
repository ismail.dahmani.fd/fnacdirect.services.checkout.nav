﻿using FnacDirect.Contracts.Online.Model;
using System;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Helpers
{
    public static class ContractValidityHelper
    {
        public static decimal ToMonthlyPrice(this ContractValidity contractValidity, decimal price)
        {
            if (contractValidity != null)
            {
                switch (contractValidity.Type)
                {
                    case ContractValidityType.Year:
                        return Math.Round(price / ((decimal)contractValidity.Value * 12m), 2);
                    case ContractValidityType.Month:
                        return Math.Round(price / (decimal)contractValidity.Value, 2);
                    case ContractValidityType.Day:
                        return Math.Round(price / ((decimal)contractValidity.Value * 30m), 2);
                }
            }
            return 0;
        }
    }
}
