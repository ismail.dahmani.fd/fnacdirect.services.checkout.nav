﻿using FnacDirect.OrderPipe.Core.Model.Config;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace FnacDirect.OrderPipe.Config
{
    public class MvcSlot
    {
        [XmlAttribute("name")]
        public string Name { get; set; }

        [XmlElement("Control")]
        public List<MvcControl> Controls { get; set; }
    }
}