using System.Collections.Generic;
using FnacDirect.Membership.Configuration;
using FnacDirect.Membership.Model.Constants;

namespace FnacDirect.OrderPipe.Base.Proxy.Membership
{
    public interface IMembershipConfigurationService
    {
        AdherentCardArticle GetAdherentCardArticleByPrid(int prid);
        int GetPromotionKey(IEnumerable<string> promotions);
        string GetOrderPipeControlKey(string opScreen, int adherentProfilKey, int promotionKey);

        List<int> FnacPlusCardTypes { get; }
        int FnacPlusTryCardType { get; }
        int PridSavingCard { get; }
        int DelayAfterExpiration { get; }
        int DelayBeforeExpiration { get; }
        bool IsFnacPlusEnabled { get; }
        bool IsFnacPlusCanalPlayEnabled { get; }
        int GetAdherentProfileKey(string adherentProgram, string adherentStatus, string cardDuration);
        IEnumerable<AdherentCardArticle> GetAdherentCardArticles();
        int GetTryCardPrid();
        AdherentCardArticle GetAdherentCardByKey(string key);
        BusinessType BusinessType();
    }
}
