﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FnacDirect.OrderPipe
{
    /// <summary>
    /// Paramètre de contrainte sous la forme d'un couple clé/valeur
    /// </summary>
    public class ConstraintParameter
    {
        public string Key { get; set; }

        public string Value { get; set; }
    }

    /// <summary>
    /// Liste de paramètres de contrainte indexés par la clé de paramètre
    /// </summary>
    public class ConstraintParameterList : KeyedCollection<string, ConstraintParameter>
    {
        protected override string GetKeyForItem(ConstraintParameter item)
        {
            if (item == null)
                throw new ArgumentNullException("item");

            return item.Key;
        }
    }

    /// <summary>
    /// Définition d'une contrainte
    /// </summary>
    public class Constraint
    {
        /// <summary>
        /// Identifiant de la contrainte
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Etape du pipe qui a positionner la contrainte
        /// </summary>
        public string ConstrainerStepName { get; set; }

        internal ConstraintParameterList parameters;

        /// <summary>
        /// Liste des paramètres de la contraintes
        /// </summary>
        public ConstraintParameterList Parameters
        {
            get { return parameters; }
            set { parameters = value; }
        }

        public Constraint AddParam(string key, string value)
        {
            if (parameters == null)
                parameters = new ConstraintParameterList();
            var p = new ConstraintParameter
            {
                Key = key,
                Value = value
            };
            parameters.Add(p);
            return this;
        }
    }

    /// <summary>
    /// Liste de contraintes, indexée par l'identifiant de la contrainte
    /// </summary>
    public class ConstraintList : KeyedCollection<string, Constraint>
    {
        protected override string GetKeyForItem(Constraint item)
        {
            if (item == null)
                throw new ArgumentNullException("item");

            return item.Name;
        }
    }
}
