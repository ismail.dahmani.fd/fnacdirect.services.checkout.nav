using FnacDirect.Contracts.Online.Model;
using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.OrderPipe.BaseMvc.Web.Models.Common;
using FnacDirect.OrderPipe.BaseMvc.Web.Pop.Models.Common;
using FnacDirect.Technical.Framework.Web.Mvc;
using System;
using System.Web.Routing;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Models
{
    public class PopBaseRootViewModelBuilder<T> : IPopBaseRootViewModelBuilder<T>
            where T : BaseRootViewModel, new()
    {
        private readonly IApplicationContext _applicationContext;
        private readonly OPContext _opContext;
        private readonly IOrderPipeUrlBuilder _orderPipeUrlBuilder;
        private readonly IBreadCrumbViewModelBuilder _breadCrumbViewModelBuilder;
        private readonly IPipeMessageViewModelBuilder _pipeMessageViewModelBuilder;
        private readonly IConfigurationViewModelBuilder _configurationViewModelBuilder;

        public PopBaseRootViewModelBuilder(IApplicationContext applicationContext,
                                            OPContext opContext,
                                            IOrderPipeUrlBuilder orderPipeUrlBuilder,
                                            IBreadCrumbViewModelBuilder breadCrumbViewModelBuilder,
                                            IPipeMessageViewModelBuilder pipeMessageViewModelBuilder,
                                            IConfigurationViewModelBuilder configurationViewModelBuilder)
        {
            _applicationContext = applicationContext;
            _opContext = opContext;
            _orderPipeUrlBuilder = orderPipeUrlBuilder;
            _breadCrumbViewModelBuilder = breadCrumbViewModelBuilder;
            _pipeMessageViewModelBuilder = pipeMessageViewModelBuilder;
            _configurationViewModelBuilder = configurationViewModelBuilder;
        }

        public T Build(Func<string, RouteValueDictionary, string> getUrl)
        {
            var popOrderForm = _opContext.OF as PopOrderForm;
            var isGuest = popOrderForm.UserContextInformations.IsGuest;
            var pipeName = _opContext.Pipe.PipeName;
            var pipeMessages = _pipeMessageViewModelBuilder.Build(popOrderForm);

            var instance = new T
            {
                OrderReference = popOrderForm.OrderGlobalInformations.MainOrderReference,
                IsFnacPro = _applicationContext.GetSiteId() == (int)FnacSites.FNACPRO,
                IsGuest = isGuest,
                LocalCountry = _applicationContext.GetLocalCountry(),
                DomainId = System.Configuration.ConfigurationManager.AppSettings["onetrustdomainid"].ToString(),
                Navigation = new NavigationViewModel
                {
                    BackLinkUrl = (pipeName == "pop") ? _orderPipeUrlBuilder.GetUrl("DisplayBasket") : ""
                },
                BreadCrumb = _breadCrumbViewModelBuilder.Build(_opContext, getUrl),
                PipeMessages = pipeMessages,
                Configuration = _configurationViewModelBuilder.Build(popOrderForm),
                PipeRoute = popOrderForm.GetRoute().GetName()
            };

            return instance;
        }
    }
}
