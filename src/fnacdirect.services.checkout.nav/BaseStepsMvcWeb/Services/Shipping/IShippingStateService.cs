﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FnacDirect.OrderPipe.Base.Business.Shipping
{
    public interface IShippingStateService
    {
        /// <summary>
        /// Permet de récupérer la ville à partir du code postal, pour les îles espagnoles
        /// </summary>
        /// <param name="zipCode"></param>
        /// <returns></returns>
        string GetStateByZipCode(string zipCode);
    }
}
