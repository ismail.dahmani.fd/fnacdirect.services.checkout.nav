using FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.OnePage.Address;
using FnacDirect.Technical.Framework.Web.Mvc.Routing;
using FnacDirect.OrderPipe.CoreMvcWeb.Routing;
using System.Web.Mvc;
using FnacDirect.OrderPipe.CoreRouting;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Pop
{
    public class PopAreaRegistrationPart
    {
        public const string PopBasket = "POP - Basket";
        public const string PopShipping = "POP - Shipping";
        public const string PopTvRoyalty = "POP - Tv Royalty";
        public const string PopPayment = "POP - Payment";
        public const string PopPaymentOgone3DS = "POP - Payment Ogone 3DS";
        public const string PopOnePage = "POP - One Page";
        public const string PopOrderThankYou = "POP - Order Thank You";
        public const string PopOrderSorry = "POP - Order Sorry";
        public const string PopBackPreview = "POP - Back Preview";
        public const string PopLogin = "POP - Login";
        public const string PopLoginModal = "POP - Login Modal";
        public const string PopKoboMergeAccounts = "POP - Kobo Merge Accounts";
        public const string PopCreateQuote = "POP - Create Quote";

        public const string PopSpreadRegistration = "POP - Spread registration";

        public const string PopRefreshBillingMethodsOnePage = "POP - Refresh billing methods one page";
        public const string PopRefreshBillingMethodsFull = "POP - Refresh billing methods full";
        public const string PopRefreshSecondaryBillingMethodsOnePage = "POP - Refresh secondary billing methods one page";
        public const string PopRefreshSecondaryBillingMethodsFull = "POP - Refresh secondary billing methods full";

        public const string OnePageAddAddress = "POP - Add Shipping Address";
        public const string OnePageAddBillingAddress = "POP - Add Billing Address";
        public const string OnePageUpdateAddress = "POP - Update Address";
        public const string OnePageUpdateBillingAddress = "POP - Update Billing Address";

        public const string QuoteConvertToOrder = "Quote - Convert To Order";
        public const string QuoteError = "Quote - Error";

        public const string PaymentOnePage = "Payment - One Page";
        public const string PaymentRefreshBillingMethodsOnePage = "Payment - Refresh billing methods one page";
        public const string PaymentRefreshSecondaryBillingMethodsOnePage = "Payment - Refresh secondary billing methods one page";

        public const string SelfCheckoutCrypto = "SelfCheckout - Crypto";
        public const string SelfCheckoutGenerateTicketTemplates = "SelfCheckout - Generate Ticket Templates";

        private readonly IRouteProvider _routeProvider;

        public PopAreaRegistrationPart(IRouteProvider routeProvider)
        {
            _routeProvider = routeProvider;
        }

        public void RegisterArea(AreaRegistrationContext context)
        {
            //Orderpipe challenge authentication
            context.MapRoute(
                   name: "Challenge Authentication",
                   url: "orderpipe/challenge",
                   defaults: new { controller = "PopLoginGateway", action = "challenge" }
            );

            context.MapMvcOrderPipeRoute(
                  name: "OP - BreadCrumb",
                  url: "orderpipe/_childactions/common/breadcrumb",
                  defaults: new { controller = "PopCommon", action = "BreadCrumb" }
            );

            context.MapMvcOrderPipeRoute(
                   name: PopCreateQuote,
                   url: "orderpipe/pop/quote/create",
                   defaults: new { controller = "PopCreateQuote", action = "Index", pipe = "pop" }
           );

            context.MapMvcOrderPipeRoute(
                   name: OnePageAddAddress,
                   url: "orderpipe/pop/shipping/postal/address/create",
                   defaults: new { controller = "PopAddress", action = "CreateAsync", addressFormViewModelMode = AddressFormViewModelMode.Shipping, pipe = "pop" }
           );

            context.MapMvcOrderPipeRoute(
                      name: OnePageAddBillingAddress,
                      url: "orderpipe/pop/billing/address/create",
                      defaults: new { controller = "PopAddress", action = "CreateAsync", addressFormViewModelMode = AddressFormViewModelMode.Billing, pipe = "pop" }
            );

            context.MapMvcOrderPipeRoute(
                      name: OnePageUpdateAddress,
                      url: "orderpipe/pop/shipping/postal/address/update",
                      defaults: new { controller = "PopAddress", action = "UpdateAsync", addressFormViewModelMode = AddressFormViewModelMode.Shipping, pipe = "pop" }
             );

            context.MapMvcOrderPipeRoute(
                      name: OnePageUpdateBillingAddress,
                      url: "orderpipe/pop/billing/address/update",
                      defaults: new { controller = "PopAddress", action = "UpdateAsync", addressFormViewModelMode = AddressFormViewModelMode.Billing, pipe = "pop" }
             );

            context.MapMvcOrderPipeRoute(
                      name: PopRefreshBillingMethodsFull,
                      url: "orderpipe/pop/payment/methods",
                      defaults: new { controller = "PopPayment", action = "Methods", pipe = "pop" }
             );

            context.MapMvcOrderPipeRoute(
                      name: PopSpreadRegistration,
                      url: "orderpipe/pop/spread-registration",
                      defaults: new { controller = "PopLogin", action = "SpreadRegistration", pipe = "pop" }
             );

            context.MapMvcOrderPipeRoute(
                      name: PopRefreshBillingMethodsOnePage,
                      url: "orderpipe/pop/onepage/methods",
                      defaults: new { controller = "PopOnePage", action = "Methods", pipe = "pop" }
             );

            context.MapMvcOrderPipeRoute(
                      name: PopRefreshSecondaryBillingMethodsFull,
                      url: "orderpipe/pop/payment/methods/secondary",
                      defaults: new { controller = "PopPayment", action = "SecondaryMethods", pipe = "pop" }
             );


            context.MapMvcOrderPipeRoute(
                      name: PopRefreshSecondaryBillingMethodsOnePage,
                      url: "orderpipe/pop/onepage/methods/secondary",
                      defaults: new { controller = "PopOnePage", action = "SecondaryMethods", pipe = "pop" }
             );

            context.MapMvcOrderPipeRoute(
                      name: "Pop Preview Ogone",
                      url: "orderpipe/pop/preview/ogone/",
                      defaults: new { controller = "PopPayment", action = "Preview", pipe = "pop" }
             );

            context.MapMvcOrderPipeRoute(
                     name: "POP - Page Redirector",
                     url: "orderpipe/pop/redirect/",
                     defaults: new { controller = "PopPageRedirector", action = "Index", pipe = "pop" }
            );

            context.MapMvcOrderPipeRoute(
                     name: "POP - Page Redirector2",
                     url: "orderpipe/pop/redirect2/",
                     defaults: new { controller = "PopPageRedirector", action = "Index2", pipe = "pop" }
            );

            context.MapMvcOrderPipeRoute(
                     name: "POP - Prepare Mail",
                     url: "orderpipe/pop/_preparemail/",
                     defaults: new { controller = "PopMail", action = "Index", pipe = "pop" }
            );

            context.MapMvcOrderPipeRoute(
                     name: "POP - Debug Mail",
                     url: "orderpipe/pop/_debugmail/",
                     defaults: new { controller = "PopMail", action = "Debug", pipe = "pop" }
            );

            // Pipe Payment 
            context.MapMvcOrderPipeRoute(
                     name: PaymentRefreshBillingMethodsOnePage,
                     url: "orderpipe/payment/onepage/methods",
                     defaults: new { controller = "PaymentOnePage", action = "Methods", pipe = "payment" }
            );

            context.MapMvcOrderPipeRoute(
                      name: PaymentRefreshSecondaryBillingMethodsOnePage,
                      url: "orderpipe/payment/onepage/methods/secondary",
                      defaults: new { controller = "PaymentOnePage", action = "SecondaryMethods", pipe = "payment" }
             );

            context.MapOrderPipeRoute(name: PopBasket, url: _routeProvider.Get(PopBasket));
            context.MapOrderPipeRoute(name: PopLogin, url: _routeProvider.Get(PopLogin));
            context.MapOrderPipeRoute(name: PopShipping, url: _routeProvider.Get(PopShipping));
            context.MapOrderPipeRoute(name: PopTvRoyalty, url: _routeProvider.Get(PopTvRoyalty));
            context.MapOrderPipeRoute(name: PopPayment, url: _routeProvider.Get(PopPayment));
            context.MapOrderPipeRoute(name: PopPaymentOgone3DS, url: _routeProvider.Get(PopPaymentOgone3DS));
            context.MapOrderPipeRoute(name: PopOnePage, url: _routeProvider.Get(PopOnePage));
            context.MapOrderPipeRoute(name: PopOrderThankYou, url: _routeProvider.Get(PopOrderThankYou));
            context.MapOrderPipeRoute(name: PopOrderSorry, url: _routeProvider.Get(PopOrderSorry));
            context.MapOrderPipeRoute(name: PopBackPreview, url: _routeProvider.Get(PopBackPreview));
            context.MapOrderPipeRoute(name: PopKoboMergeAccounts, url: _routeProvider.Get(PopKoboMergeAccounts));

            //Pipe Quote
            context.MapOrderPipeRoute(name: QuoteConvertToOrder, url: _routeProvider.Get(QuoteConvertToOrder));
            context.MapOrderPipeRoute(name: QuoteError, url: _routeProvider.Get(QuoteError));

            //Pipe Payment 
            context.MapOrderPipeRoute(name: PaymentOnePage, url: _routeProvider.Get(PaymentOnePage));

            //Pipe Selfcheckout
            context.MapMvcOrderPipeRoute(
                  name: "OP - SelfCheckout",
                  url: "orderpipe/selfcheckout",
                  defaults: new { controller = "SelfCheckout", action = "Index" }
            );

            context.MapOrderPipeRoute(name: SelfCheckoutCrypto, url: _routeProvider.Get(SelfCheckoutCrypto));

            context.MapMvcOrderPipeRoute(
                  name: SelfCheckoutGenerateTicketTemplates,
                  url: "orderpipe/selfcheckout/build-ticket-template",
                  defaults: new { controller = "SelfCheckoutOrderThankYouController", action = "BuildTicketTemplate" }
            );

            context.MapOrderPipeRoute(
                 name: "SelfCheckout -  Return Entry Point",
                 url: "orderpipe/selfcheckout/return",
                 defaults: new object(),
                 constraints: new { isNotMappedToPhysicalFile = new IsNotMappedToPhysicalFile() }
            );
        }
    }
}
