using FnacDirect.OrderPipe.Base.LineGroups;
using FnacDirect.OrderPipe.Base.Model;
using System.Collections.Generic;
using System.Linq;

namespace FnacDirect.OrderPipe.Base.Business.FDV
{
    /// <summary>
    /// Classe boîte à outils pour la GUPT.
    /// </summary>
    public class GuptHelperService : IGuptHelperService
    {
        /// <summary>Limite du nombre d'articles (SKUs) que l'on peut envoyer à la GUPT</summary>
        private static int GuptItemsLimit = 15;

        /// <summary>Flux de vente intra mag gérés par la GUPT</summary>
        private static IEnumerable<SellerType> GuptManagedSellerTypes = new List<SellerType>
        {
            SellerType.ShelfFaded,
            SellerType.Stock,
            SellerType.StockFaded,
            SellerType.StockForced,
            SellerType.Vitrine
        };


        public int GetGuptItemsLimit()
            => GuptItemsLimit;

        public IEnumerable<SellerType> GetGuptManagedSellerTypes()
            => GuptManagedSellerTypes;

        public GuptHelperResult Process(IEnumerable<StandardArticle> articles)
            => Process_(articles?.Where(a => a.SellerID.HasValue && SellerIds.IsShopSellerId(a.SellerID.Value) && a.IsStockManagedByGupt));

        /// <summary>
        /// Indique le nombre de lignes qui seront envoyées à la GUPT pour un article donné.
        /// On part du principe que l'article donné est un produit dont le stock est géré par la GUPT, on ne vérifie pas ici.
        /// </summary>
        /// <param name="article">Article à comptabiliser</param>
        private static int GetGuptLineCount_(StandardArticle article)
        {
            // Une ligne pour l'article lui-même
            var count = 1;

            if (article is IBundle)
            {
                // On traite les composants de la vente d'ensemble
                var bundle = article as IBundle;
                foreach (var component in bundle.BundleComponents.OfType<StandardArticle>())
                {
                    count += GetGuptLineCount_(component);
                }
            }

            if (article.Services?.Any() == true)
            {
                // On traite les services
                foreach (var service in article.Services)
                {
                    count += GetGuptLineCount_(service);
                }
            }

            if (!string.IsNullOrEmpty(article.EcoTaxCode) || article.EcoTaxEur.GetValueOrDefault(0) > 0)
            {
                // Une ligne pour la D3E (éco-taxe)
                count += 1;
            }

            return count;
        }

        /// <param name="articles">Articles gérés par la GUPT vendus en magasin</param>
        private static GuptHelperResult Process_(IEnumerable<StandardArticle> articles)
        {
            if (articles == null || !articles.Any())
            {
                return new GuptHelperResult
                {
                    CanProcessBasket = true,
                    CanUseCheckoutBillingMethod = true,
                    ShouldHaveShopSelfServiceLineGroups = false,
                    ShouldHaveShopStockLineGroups = false,
                    ShouldPutSelfServiceItemsInShopStockLineGroups = false
                };
            }

            // Nombre d'articles GUPT vendus en libre service (rayon DI/F)
            var selfServiceCount = articles
                .Where(a => !GuptManagedSellerTypes.Contains(SellerIds.GetType(a.SellerID.Value)))
                .Sum(a => GetGuptLineCount_(a));

            // Nombre d'articles GUPT vendus en rayon déstockés par la GUPT (rayon DE/VI)
            var shelfForGuptCount = articles
                .Where(a =>
                    SellerIds.GetParentSellerType(a.SellerID.Value) == SellerType.Shelf &&
                    GuptManagedSellerTypes.Contains(SellerIds.GetType(a.SellerID.Value))
                )
                .Sum(a => GetGuptLineCount_(a));

            // Nombre d'articles GUPT vendus en stock déstockés par la GUPT (stock DI/DE/VI/F)
            var stockForGuptCount = articles
                .Where(a =>
                    SellerIds.GetParentSellerType(a.SellerID.Value) == SellerType.Stock &&
                    GuptManagedSellerTypes.Contains(SellerIds.GetType(a.SellerID.Value))
                )
                .Sum(a => GetGuptLineCount_(a));

            return Process_(selfServiceCount, shelfForGuptCount, stockForGuptCount);
        }

        /// <param name="selfServiceCount">Nombre d'articles vendus en libre service (rayon DI/F)</param>
        /// <param name="shelfForGuptCount">Nombre d'articles vendus en rayon déstockés par la GUPT (rayon DE/VI)</param>
        /// <param name="stockForGuptCount">Nombre d'articles vendus en stock déstockés par la GUPT (stock DI/DE/VI/F)</param>
        private static GuptHelperResult Process_(int selfServiceCount, int shelfForGuptCount, int stockForGuptCount)
        {
            // Les booléens sont initialisés à faux par défaut, ils sont explicités ici pour plus de clareté dans l'algorithme qui suit
            var result = new GuptHelperResult
            {
                CanProcessBasket = false,
                CanUseCheckoutBillingMethod = false,
                ShouldHaveShopSelfServiceLineGroups = false,
                ShouldHaveShopStockLineGroups = false,
                ShouldPutSelfServiceItemsInShopStockLineGroups = false
            };

            result.CanProcessBasket = CanProcessBasket(shelfForGuptCount, stockForGuptCount);

            // Panier non réalisable, on s'arrête là
            if (!result.CanProcessBasket)
            {
                return result;
            }

            result.CanUseCheckoutBillingMethod = CanUseCheckoutBillingMethod(selfServiceCount, shelfForGuptCount, stockForGuptCount);
            result.ShouldHaveShopSelfServiceLineGroups = ShouldHaveShopSelfServiceLineGroups(selfServiceCount, shelfForGuptCount, stockForGuptCount);
            result.ShouldHaveShopStockLineGroups = ShouldHaveShopStockLineGroups(selfServiceCount, shelfForGuptCount, stockForGuptCount);
            result.ShouldPutSelfServiceItemsInShopStockLineGroups = ShouldPutSelfServiceItemsInShopStockLineGroups(selfServiceCount, shelfForGuptCount, stockForGuptCount);

            // Cas particulier : s'il y a du Rayon DI ou F avec du Rayon DE ou VI, on ne sait pas les séparer en deux LogisticLineGroup
            // On ne peut donc traiter ce cas que si l'on peut tout envoyer à la GUPT
            if (0 < selfServiceCount && 0 < shelfForGuptCount && !result.CanUseCheckoutBillingMethod)
            {
                result.CanProcessBasket = false;
                result.CanUseCheckoutBillingMethod = false;
                result.ShouldHaveShopSelfServiceLineGroups = false;
                result.ShouldHaveShopStockLineGroups = false;
                result.ShouldPutSelfServiceItemsInShopStockLineGroups = false;
            }

            return result;
        }

        #region Règles de gestion

        /// <summary>Indique si le panier peut être traité</summary>
        /// <param name="shelfForGuptCount">Nombre d'articles techniques vendus en rayon déstockés par la GUPT (rayon DE/VI)</param>
        /// <param name="stockForGuptCount">Nombre d'articles techniques vendus en magasin stocké / hors rayon</param>
        private static bool CanProcessBasket(int shelfForGuptCount, int stockForGuptCount)
            => (shelfForGuptCount + stockForGuptCount) <= GuptItemsLimit;

        /// <summary>Indique si le paiement en caisse est possible</summary>
        /// <param name="selfServiceCount">Nombre d'articles techniques vendus en magasin en rayon (libre service)</param>
        /// <param name="shelfForGuptCount">Nombre d'articles techniques vendus en rayon déstockés par la GUPT (rayon DE/VI)</param>
        /// <param name="stockForGuptCount">Nombre d'articles techniques vendus en magasin stocké / hors rayon</param>
        private static bool CanUseCheckoutBillingMethod(int selfServiceCount, int shelfForGuptCount, int stockForGuptCount)
            => (selfServiceCount + shelfForGuptCount + stockForGuptCount) <= GuptItemsLimit;

        /// <summary>Indique la future présence d'un LogisticLineGroups intra mag libre service</summary>
        /// <param name="selfServiceCount">Nombre d'articles techniques vendus en magasin en rayon (libre service)</param>
        /// <param name="shelfForGuptCount">Nombre d'articles techniques vendus en rayon déstockés par la GUPT (rayon DE/VI)</param>
        /// <param name="stockForGuptCount">Nombre d'articles techniques vendus en magasin stocké / hors rayon</param>
        private static bool ShouldHaveShopSelfServiceLineGroups(int selfServiceCount, int shelfForGuptCount, int stockForGuptCount)
            => GuptItemsLimit < (selfServiceCount + shelfForGuptCount + stockForGuptCount);

        /// <summary>Indique la future présence d'un LogisticLineGroups intra mag stocké</summary>
        /// <param name="selfServiceCount">Nombre d'articles techniques vendus en magasin en rayon (libre service)</param>
        /// <param name="shelfForGuptCount">Nombre d'articles techniques vendus en rayon déstockés par la GUPT (rayon DE/VI)</param>
        /// <param name="stockForGuptCount">Nombre d'articles techniques vendus en magasin stocké / hors rayon</param>
        private static bool ShouldHaveShopStockLineGroups(int selfServiceCount, int shelfForGuptCount, int stockForGuptCount)
            => 0 < (shelfForGuptCount + stockForGuptCount) || (0 < selfServiceCount && selfServiceCount <= GuptItemsLimit);

        /// <summary>Indique si les articles en libre service doivent être dans le LogisticLineGroups intra mag stocké</summary>
        /// <remarks>S'il y a des articles vendus en rayon et le LLG intra mag stocké est présent mais pas le LLG intra mag libre service</remarks>
        /// <param name="selfServiceCount">Nombre d'articles techniques vendus en magasin en rayon (libre service)</param>
        /// <param name="stockForGuptCount">Nombre d'articles techniques vendus en magasin stocké / hors rayon</param>
        private static bool ShouldPutSelfServiceItemsInShopStockLineGroups(int selfServiceCount, int shelfForGuptCount, int stockForGuptCount)
            => (
                0 < selfServiceCount &&
                ShouldHaveShopStockLineGroups(selfServiceCount, shelfForGuptCount, stockForGuptCount) &&
                !ShouldHaveShopSelfServiceLineGroups(selfServiceCount, shelfForGuptCount, stockForGuptCount)
            );

        #endregion Règles de gestion
    }
}
