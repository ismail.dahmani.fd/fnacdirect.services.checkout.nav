using FnacDirect.Customer.Model;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Serialization;

namespace FnacDirect.OrderPipe.Base.Model
{
    [XmlInclude(typeof(PostalAddress))]
    [XmlInclude(typeof(RelayAddress))]
    [XmlInclude(typeof(ShopAddress))]
    [XmlInclude(typeof(StoreAddress))]
    public abstract class ShippingAddress
    {
        public int? Identity { get; set; }

        [XmlIgnore]
        public string AddressBookReference { get; set; }

        public string Reference { get; set; }

        [XmlIgnore]
        public int? InternalId { get; set; }

        public abstract int Type { get; }

        [XmlIgnore]
        public bool? IsCountryUseVAT { get; set; }


        public int? CountryId { get; set; }

        [XmlIgnore]
        public string CountryLabel { get; set; }

        [XmlIgnore]
        public int? ShippingZone { get; set; }

        /// <summary>
        /// ShippingNetwork à utiliser pour calculer les frais de port.
        /// Cet attribut n'est utilisé que si on veut forcer un ShippingNetwork.
        /// </summary>
        [XmlIgnore]
        public int? CalcNetwork { get; set; }

        [XmlIgnore]
        public string CountryCode2 { get; set; }

        [XmlIgnore]
        public string CountryCode3 { get; set; }

        public string Phone { get; set; }

        [XmlIgnore]
        public virtual string State { get; set; }

        [XmlIgnore]
        public int? OrderShippingAddressPk { get; set; }

        public bool? IsNewAdress { get; set; }

        [XmlIgnore]
        public bool IsDefault { get; set; }

        [XmlIgnore]
        public bool? FlagVatIntra { get; set; }

        [XmlIgnore]
        public bool IsOversea { get; set; }

        public abstract bool IsDummy();

        [XmlIgnore]
        public int LegalStatus { get; set; }

        public ShippingAddressEntity ToShippingAddressEntity()
        {
            return new ShippingAddressEntity()
            {
                Id = Identity.GetValueOrDefault(),
                AddressBookReference = Reference,
                AddressBookId = InternalId.GetValueOrDefault(),
                AddressType = Type
            };
        }

        public void SetOverseaInformation(List<int> overseaShippingZones)
        {
            var isOversea = false;

            if (overseaShippingZones != null && overseaShippingZones.Any() && ShippingZone.HasValue && overseaShippingZones.Contains(ShippingZone.Value))
            {
                isOversea = true;
            }

            IsOversea = isOversea;
        }

    }
}
