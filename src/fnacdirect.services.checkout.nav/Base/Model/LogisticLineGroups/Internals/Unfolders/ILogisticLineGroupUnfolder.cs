﻿using FnacDirect.OrderPipe.Base.Model;
using System.Collections.Generic;

namespace FnacDirect.OrderPipe.Base.Business.LogisticLineGroups.Internals.Unfolders
{
    public interface ILogisticLineGroupUnfolder
    {
        IEnumerable<Article> Unfold(IEnumerable<Article> articles);
    }
}
