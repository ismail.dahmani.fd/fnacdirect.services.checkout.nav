namespace FnacDirect.OrderPipe.Base.BaseModel.Model.MarketPlace
{
    public class MarketPlaceReservation
    {
        public int Id { get; set; }

        public int Quantity { get; set; }
    }
}
