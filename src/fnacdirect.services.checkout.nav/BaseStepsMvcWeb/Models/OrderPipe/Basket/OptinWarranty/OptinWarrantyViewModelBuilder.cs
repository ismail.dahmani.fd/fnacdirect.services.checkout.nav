using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.OrderPipe.Base.Model.OptinWarranty;
using FnacDirect.Technical.Framework.Web.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Basket
{
    public class OptinWarrantyViewModelBuilder : IOptinWarrantyViewModelBuilder
    {
        private readonly IApplicationContext _applicationContext;

        public OptinWarrantyViewModelBuilder(IApplicationContext applicationContext)
        {
            _applicationContext = applicationContext;
        }

        public OptinWarrantyViewModel Build(PopOrderForm orderform, int productid)
        {
            var result = new OptinWarrantyViewModel();

            var data = orderform.GetPrivateData<OptinWarrantyData>(create: false);

            if (data == null || data.ArticlesWithOptinWarrantyChecked == null)
            {
                return null;
            }
                        
            if (data.ArticlesWithOptinWarrantyChecked.Contains(productid))
            {
                result.IsChecked = true;
            }
            else
            {
                result.IsChecked = false;
            }
            result.Label = _applicationContext.GetMessage("orderpipe.pop.warranty.optin.label");

            return result;
        }
    }
}
