namespace FnacDirect.OrderPipe.Base.Proxy.Membership
{
    public interface IAdherentCardArticleService
    {
        bool IsAdherentCard(int prid);

        bool IsRenewAdhesionCard(int prid);

        bool IsRecruitAdherentCard(int prid);
    }
}
