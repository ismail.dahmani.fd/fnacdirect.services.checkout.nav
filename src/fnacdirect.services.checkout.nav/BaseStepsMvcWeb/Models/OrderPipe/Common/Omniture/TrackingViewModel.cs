using Newtonsoft.Json;
using System.Collections.Generic;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Omniture
{
    public class TrackingViewModel
    {
        [JsonProperty("account")]
        public string Account { get; set; }
        [JsonProperty("channel")]
        public string Channel { get; set; }
        [JsonProperty("server")]
        public string Server { get; set; }
        [JsonProperty("UID")]
        public string UID { get; set; }

        [JsonProperty("pipe_type")]
        public string PipeType { get; set; }
        [JsonProperty("plateforme_type")]
        public string PlateformeType { get; set; }

        [JsonProperty("basket_mix")]
        public string BasketMix { get; set; }
        [JsonProperty("basket_amount")]
        public string BasketAmount { get; set; }

        public IEnumerable<ProductTrackingViewModel> Products { get; set; }

        [JsonProperty("status_membership")]
        public string StatusMembership { get; set; }
        [JsonProperty("status_identification")]
        public string StatusIdentification { get; set; }
        [JsonProperty("subscription_type")]
        public string SubscriptionType { get; set; }
        [JsonProperty("status_loggued")]
        public string StatusLoggued { get; set; }
        [JsonProperty("status_subscription")]
        public string StatusSubscription { get; set; }
        [JsonProperty("basket_source")]
        public string BasketSource { get; set; }
        [JsonProperty("time_day")]
        public string Day { get; set; }
        [JsonProperty("time_daytype")]
        public string DayType { get; set; }
        [JsonProperty("time_hour")]
        public string Hour { get; set; }

        [JsonProperty("pagename")]
        public string PageName { get; set; }
        [JsonIgnore]
        public string Prefix { get; set; }
        [JsonProperty("arbo_level_1")]
        public string ArboLevel1 { get; set; }

        [JsonProperty("purchase_ID")]
        public string PurchaseID { get; set; }

        [JsonProperty("payment_method")]
        public string PaymentMethod { get; set; }

        [JsonProperty("order_shipping_method")]
        public string OrderShippingMethod { get; set; }

        [JsonProperty("order_type")]
        public string OrderType { get; set; }
    }
}
