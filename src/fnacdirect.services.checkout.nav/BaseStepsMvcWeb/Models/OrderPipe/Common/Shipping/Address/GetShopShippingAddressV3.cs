using System;
using System.Linq;
using System.Collections.Generic;
using FnacDirect.OrderPipe.Base.BaseModel;
using FnacDirect.OrderPipe.Base.Business.Shipping;
using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.OrderPipe.Base.BaseModel.Model;
using FnacDirect.OrderPipe.Base.Model.Facets;
using FnacDirect.OrderPipe.Base.Proxy.Store;
using FnacDirect.OrderPipe.Core.Steps;

namespace FnacDirect.OrderPipe.Base.Steps
{
    public class ShippingAddressData : GroupData
    {
        public bool ShopDeliveringEnabled { get; set; } = true;
    }

    /// <summary>
    /// Etape permettant de récupérer les adresses magasin de l'OrderForm à partir de la valeur du champ "Identity" des ShippingAddress.
    /// </summary>
    public class GetShopShippingAddressV3 : FacetStep<IGotShippingMethodEvaluation, ShippingMethodEvaluation>
    {
        public override IEnumerable<Type> DataType
        {
            get
            {
                yield return typeof(ShippingAddressData);
                yield return typeof(ShopData);
            }
        }

        protected override ShippingMethodEvaluation GetFacet(IGotShippingMethodEvaluation orderFormFacet)
        {
            return orderFormFacet.ShippingMethodEvaluation;
        }

        private readonly IFnacStoreService _fnacStoreService;
        private readonly IShippingAddressElegibilityServiceProvider _shippingAddressElegibilityServiceProvider;

        public GetShopShippingAddressV3(IFnacStoreService fnacStoreService, IShippingAddressElegibilityServiceProvider shippingAddressElegibilityServiceProvider)
        {
            _fnacStoreService = fnacStoreService;
            _shippingAddressElegibilityServiceProvider = shippingAddressElegibilityServiceProvider;
        }

        protected override StepReturn Check(ShippingMethodEvaluation shippingMethodEvaluation)
        {
            if (!shippingMethodEvaluation.ShippingMethodEvaluationGroups.Any(g => g.AllowedTypes.Contains(ShippingMethodEvaluationType.Shop)))
            {
                return StepReturn.Skip;
            }

            if (!shippingMethodEvaluation.ShippingMethodEvaluationGroups.Any(g => g.ShopShippingMethodEvaluationItem.HasValue))
            {
                return StepReturn.Skip;
            }
            
            return StepReturn.Process;
        }

        protected override StepReturn Process(ShippingMethodEvaluation shippingMethodEvaluation, OPContext opContext)
        {
            foreach (var shippingMethodEvaluationGroup in shippingMethodEvaluation.ShippingMethodEvaluationGroups.Where(g => g.AllowedTypes.Contains(ShippingMethodEvaluationType.Shop) && g.ShopShippingMethodEvaluationItem.HasValue))
            {
                var shippingMethodEvaluationItem = shippingMethodEvaluationGroup.ShopShippingMethodEvaluationItem.Value;

                var shopAddress = shippingMethodEvaluationItem.ShippingAddress;
                var shopAddressIdentity = shopAddress.Identity.GetValueOrDefault();

                var fnacStore = _fnacStoreService.GetStore(shopAddressIdentity);

                if (fnacStore == null)
                {
                    if (!shippingMethodEvaluationItem.IsDummyAddress || shopAddressIdentity != -1)
                    {
                        return StepReturn.Next;
                    }

                    var shippingAddressElegibilityService =
                        _shippingAddressElegibilityServiceProvider.GetShippingAddressElegibilityServiceFor(opContext);

                    shippingMethodEvaluation.FnacCom.Value.ReplaceAddress(shippingAddressElegibilityService.DummyShopAddress, true);
                }
                else
                {
                    ShopAddress.Copy(fnacStore, shopAddress);
                }
            }

            return StepReturn.Next;
        }
    }
}
