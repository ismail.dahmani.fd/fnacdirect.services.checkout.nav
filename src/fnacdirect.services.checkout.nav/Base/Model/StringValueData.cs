namespace FnacDirect.OrderPipe.Base.Model
{
    public class StringValueData : GroupData
    {
        private string _value;

        public string Value
        {
            get { return _value; }
            set { _value = value; }
        }
    }

    public class SplitValueData : GroupData
    {
        private string _value;

        public string Value
        {
            get { return _value; }
            set { _value = value; }
        }
    }

    public class IsManuallyModifiedValueData : GroupData
    {
        private string _value;

        public string Value
        {
            get { return _value; }
            set { _value = value; }
        }
    }

}
