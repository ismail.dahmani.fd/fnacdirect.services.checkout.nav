﻿using System.Xml.Serialization;
namespace FnacDirect.OrderPipe.Base.Business.OrderDetail
{
    public class ShopAddress : BaseAddress
    {
        public ShopAddress()
        {
            Type = "ShopAddress";
        }

        public string Name { get; set; }        

        public Schedule Schedule { get; set; }

        public string Type { get; set; }
    }
}
