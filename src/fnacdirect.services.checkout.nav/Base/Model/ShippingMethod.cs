using System;
using System.Collections.Generic;
using System.Xml.Serialization;


namespace FnacDirect.OrderPipe.Base.Model
{
    public class ShippingMethod : IAdditionalFeeMethod
    {
        [XmlIgnore]
        private int? _MethodId;

        public int? MethodId
        {
            get
            {
                return _MethodId;
            }
            set
            {
                _MethodId = value;
            }
        }

        [XmlIgnore]
        public int? Id
        {
            get
            {
                return _MethodId;
            }
            set
            {
                _MethodId = value;
            }
        }

        [XmlIgnore]
        private string _Reference;

        [XmlIgnore]
        public string Reference
        {
            get
            {
                return _Reference;
            }
            set
            {
                _Reference = value;
            }
        }

        [XmlIgnore]
        private string _Label;

        [XmlIgnore]
        public string Label
        {
            get
            {
                return _Label;
            }
            set
            {
                _Label = value;
            }
        }

        [XmlIgnore]
        private int? _TypeLogistic;

        [XmlIgnore]
        public int? TypeLogistic
        {
            get
            {
                return _TypeLogistic;
            }
            set
            {
                _TypeLogistic = value;
            }
        }

        [XmlIgnore]
        private string _DelayLabel;

        [XmlIgnore]
        public string DelayLabel
        {
            get
            {
                return _DelayLabel;
            }
            set
            {
                _DelayLabel = value;
            }
        }

        [XmlIgnore]
        private int? _DelayInHours;

        [XmlIgnore]
        public int? DelayInHours
        {
            get
            {
                return _DelayInHours;
            }
            set
            {
                _DelayInHours = value;
            }
        }

        [XmlIgnore]
        private string _TransportCompany;

        [XmlIgnore]
        public string TransportCompany
        {
            get
            {
                return _TransportCompany;
            }
            set
            {
                _TransportCompany = value;
            }
        }
        [XmlIgnore]
        private decimal? _PriceGlobalDBEur;

        [XmlIgnore]
        public decimal? PriceGlobalDBEur
        {
            get
            {
                return _PriceGlobalDBEur;
            }
            set
            {
                _PriceGlobalDBEur = value;
            }
        }


        [XmlIgnore]
        private decimal? _PriceGlobalEur;

        [XmlIgnore]
        public decimal? PriceGlobalEur
        {
            get
            {
                return _PriceGlobalEur;
            }
            set
            {
                _PriceGlobalEur = value;
            }
        }

        [XmlIgnore]
        private decimal? _PriceGlobalNoVatDBEUR;

        [XmlIgnore]
        public decimal? PriceGlobalNoVatDBEUR
        {
            get
            {
                return _PriceGlobalNoVatDBEUR;
            }
            set
            {
                _PriceGlobalNoVatDBEUR = value;
            }
        }

        [XmlIgnore]
        private decimal? _PriceGlobalNoVatEUR;

        [XmlIgnore]
        public decimal? PriceGlobalNoVatEUR
        {
            get
            {
                return _PriceGlobalNoVatEUR;
            }
            set
            {
                _PriceGlobalNoVatEUR = value;
            }
        }

        [XmlIgnore]
        private decimal? _PriceDetailEur;

        [XmlIgnore]
        public decimal? PriceDetailEur
        {
            get
            {
                return _PriceDetailEur;
            }
            set
            {
                _PriceDetailEur = value;
            }
        }

        [XmlIgnore]
        private decimal? _PriceDetailNoVatEUR;

        [XmlIgnore]
        public decimal? PriceDetailNoVatEUR
        {
            get
            {
                return _PriceDetailNoVatEUR;
            }
            set
            {
                _PriceDetailNoVatEUR = value;
            }
        }

        [XmlIgnore]
        private decimal? _PriceDetailDBEur;

        [XmlIgnore]
        public decimal? PriceDetailDBEur
        {
            get
            {
                return _PriceDetailDBEur;
            }
            set
            {
                _PriceDetailDBEur = value;
            }
        }


        [XmlIgnore]
        private decimal? _CurrentPriceNoVatEUR;

        [XmlIgnore]
        public decimal? CurrentPriceNoVatEUR
        {
            get
            {
                return _CurrentPriceNoVatEUR;
            }
            set
            {
                _CurrentPriceNoVatEUR = value;
            }
        }

        [XmlIgnore]
        private decimal? _CurrentPriceDbEUR;

        [XmlIgnore]
        public decimal? CurrentPriceDbEUR
        {
            get
            {
                return _CurrentPriceDbEUR;
            }
            set
            {
                _CurrentPriceDbEUR = value;
            }
        }

        [XmlIgnore]
        private decimal? _CurrentPriceTheoEUR;

        [XmlIgnore]
        public decimal? CurrentPriceTheoEUR
        {
            get
            {
                return _CurrentPriceTheoEUR;
            }
            set
            {
                _CurrentPriceTheoEUR = value;
            }
        }

        [XmlIgnore]
        private decimal? _CurrentPriceEUR;

        [XmlIgnore]
        public decimal? CurrentPriceEUR
        {
            get
            {
                return _CurrentPriceEUR;
            }
            set
            {
                _CurrentPriceEUR = value;
            }
        }

        [XmlIgnore]
        private string _PromoCode;

        [XmlIgnore]
        public string PromoCode
        {
            get
            {
                return _PromoCode;
            }
            set
            {
                _PromoCode = value;
            }
        }

        [XmlIgnore]
        private decimal? _VATRate;

        [XmlIgnore]
        public decimal? VATRate
        {
            get
            {
                return _VATRate;
            }
            set
            {
                _VATRate = value;
            }
        }

        [XmlIgnore]
        private string _VATCode;

        [XmlIgnore]
        public string VATCode
        {
            get
            {
                return _VATCode;
            }
            set
            {
                _VATCode = value;
            }
        }

        [XmlIgnore]
        private bool _Active;

        [XmlIgnore]
        public bool Active
        {
            get { return _Active; }
            set { _Active = value; }
        }

        public ShippingMethod()
        {
        }
    }

    [Serializable]
    public class ShippingModeValue
    {

        public enum ShippingModeScop
        {
            a = 0,
            b = 1
        }

        public ShippingModeValue(ShippingModeScop scop)
        {
            DataKey = Guid.NewGuid();
            Scope = scop;
        }

        public ShippingModeValue()
        {
            DataKey = Guid.NewGuid();
        }

        public ShippingModeScop Scope
        {
            get { return scope; }
            set { scope = value; }
        }

        private ShippingModeScop scope;
        private Guid dataKey;

        public Guid DataKey
        {
            get { return dataKey; }
            set { dataKey = value; }
        }

        private int shippingModeId;
        private string shippingModeLabel;
        private decimal shippingModeExcTaxValue;
        private decimal _shippingModeExcTaxValueOld;
        private decimal _methodGlobalCost;
        private decimal _methodGlobalUserCost;
        private int _logisticType;
        private int _addressId;

        public int ShippingModeId
        {
            get { return shippingModeId; }
            set { shippingModeId = value; }
        }

        public string ShippingModeLabel
        {
            get { return shippingModeLabel; }
            set { shippingModeLabel = value; }
        }

        public decimal ShippingModeExcTaxValue
        {
            get { return shippingModeExcTaxValue; }
            set { shippingModeExcTaxValue = value; }
        }

        public decimal ShippingModeExcTaxValueOld
        {
            get
            {
                return _shippingModeExcTaxValueOld;
            }
            set
            {
                _shippingModeExcTaxValueOld = value;
            }
        }

        public decimal MethodGlobalCost
        {
            get
            {
                return _methodGlobalCost;
            }
            set
            {
                _methodGlobalCost = value;
            }
        }

        public decimal MethodGlobalUserCost
        {
            get
            {
                return _methodGlobalUserCost;
            }
            set
            {
                _methodGlobalUserCost = value;
            }
        }

        public int LogisticType
        {
            get
            {
                return _logisticType;
            }
            set
            {
                _logisticType = value;
            }
        }

        public int AddressId
        {
            get
            {
                return _addressId;
            }
            set
            {
                _addressId = value;
            }
        }
    }

    [Serializable]
    public class ShippingModeValueCollection : List<ShippingModeValue>
    {
        private int adressId;
        private string shippingLog;

        public int AdressId
        {
            get { return adressId; }
            set { adressId = value; }
        }

        public string ShippingLog
        {
            get { return shippingLog; }
            set { shippingLog = value; }
        }
    }


}
