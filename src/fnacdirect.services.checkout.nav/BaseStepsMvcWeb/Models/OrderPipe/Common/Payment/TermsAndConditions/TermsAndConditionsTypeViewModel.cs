using System.Collections.Generic;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Common.Payment
{
    public class TermsAndConditionsTypeViewModel
    {
        public List<string> PopinsName { get; set; }
        public List<string> PopinsWarrantiesName { get; set; }
    }
}
