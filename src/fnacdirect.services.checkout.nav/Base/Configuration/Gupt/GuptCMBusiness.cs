using System;
using System.Collections.Generic;
using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.OrderPipe.Base.Business;
using System.Linq;
using FnacDirect.OrderPipe.CM.Model;
using FnacDirect.OrderPipe.Base.LineGroups;
using FnacDirect.Technical.Framework.CoreServices;
using FnacDirect.ProxyEAI.IService;
using FnacDirect.ProxyEAI.Model;
using FnacDirect.OrderPipe.Base.Model.UserInformations;
using FnacDirect.OrderPipe.Base.Proxy;

namespace FnacDirect.OrderPipe.CM.Business
{
    /// <summary>
    /// Dans le cas Clic et Mag, l'objet est d'effectuer un paiement en caisse
    /// via le S3.
    /// Le détail de la commande n'est pas fournie. On indique juste à la GU 
    /// que le montant via une ligne NR.
    /// </summary>
    public abstract class GuptCMBusiness : GuptBusiness, IGuptCMBusiness
    {
        protected abstract string NRCode { get; }

        protected GuptCMBusiness(IClickAndMagService clickAndMagService)
            : base(clickAndMagService)
        {
        }

        public string GetGUPTRefInvoiceShop(
            ILogisticLineGroup lg,
            FnacStore.Model.FnacStore store,
            IEnumerable<BillingMethod> billingMethods,
            IUserContextInformations userInformations,
            string contactEmail,
            BillingAddress billingAddress)
        {
            var result = string.Empty;
            try
            {
                if (ConfigurationManager.Current.Maintenance.ProxyEAI == false)
                {
                    var svc = Service<IProxyEAIService>.Instance;

                    var order = BuildOrder(lg, store, billingMethods, userInformations, false);
                    var customer = BuildCustomer(userInformations, contactEmail, billingAddress, false);
                    var tvRoyalty = BuildTvRoyalty(lg);

                    result = svc.CreerCommande(order, customer, tvRoyalty);
                }
            }
            catch (Exception ex)
            {
                Logging.Current.WriteError("FnacDirect.OrderPipe.CM.Business.GuptCMBusiness.GetGUPTRefInvoiceShop", ex);
            }

            return result;
        }

        protected override ShopTvRoyalty BuildTvRoyalty(ILogisticLineGroup lg)
        {
            return null;
        }

        override protected ShopOrder BuildOrder(
            ILogisticLineGroup lg,
            FnacStore.Model.FnacStore store,
            IEnumerable<BillingMethod> billingMethods,
            IUserContextInformations userInformations,
            bool containsAdhThreeYears)
        {
            if (!billingMethods.OfType<CheckoutBillingMethod>().Any())
            {
                throw new Exception("Impossible de récupérer le moyen de paiement \"à la caisse\" de la commande");
            }

            var giftCardAmount = billingMethods.OfType<VirtualGiftCheckBillingMethod>().SingleOrDefault()?.Amount.GetValueOrDefault() ?? 0m;
            var amount = lg.GlobalPrices.TotalPriceEur.Value - giftCardAmount;

            var order = new ShopOrder
            {
                CountryCode = store.Country.Code ?? "FR",
                ShopCode = string.Format("{0:0000}", store.RefUG),
                RefOrder = GetRefOrder(lg.OrderUserReference, userInformations),
                PaymentCredit = GetOrderCredit(billingMethods),
                MessageType = "FNAC_COM_VENTE_IMM",
                Articles = new List<ShopArticle> { GetArticle(amount) },
                Total = amount
            };

            return order;
        }

        private ShopArticle GetArticle(decimal amount)
            => new ShopArticle
            {
                RefId = NRCode,
                Quantity = 1,
                PricePerUnit = amount
            };

        private string GetRefOrder(string orderUserReference, IUserContextInformations userInformations)
        {
            return !string.IsNullOrEmpty(orderUserReference)
                ? orderUserReference
                :
                    //Pour des fins de tracking avec Fnac SA, dans le cas d'un paiement retraitPTLS le service S3
                    //est appelé avant de faire la transaction, donc avant d'obtenir un numéro de commande. Dans ce
                    //cas on passe le numéro de client dans le parametre RefOrder. Cela permettra  de suivre l'appel
                    //effectué avec Fnac SA, plutôt que de ne rien passer.
                    userInformations.AccountId.ToString();
        }
    }

}
