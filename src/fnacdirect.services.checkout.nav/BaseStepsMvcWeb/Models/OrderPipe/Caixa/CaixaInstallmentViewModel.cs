﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.OrderPipe.Caixa
{
    public class CaixaInstallmentViewModel
    {
        [Range(0, Double.PositiveInfinity)]
        public decimal Price { get; set; }
    }
}
