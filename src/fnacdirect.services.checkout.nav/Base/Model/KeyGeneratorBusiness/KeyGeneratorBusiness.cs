using FnacDirect.KeyGenerator;
using FnacDirect.OrderPipe.Base.LineGroups;
using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.OrderPipe.Base.Model.OrderInformations;
using FnacDirect.OrderPipe.Base.Model.UserInformations;
using System;
using System.Collections.Generic;
using System.Linq;

namespace FnacDirect.OrderPipe.Base.Business.MainOrderTransactionInfo
{
    internal class KeyGeneratorBusiness : IKeyGeneratorBusiness
    {
        public MultiKey GenerateKeyGen(IOrderGlobalInformations orderGlobalInformations, IUserContextInformations userContextInformations, IEnumerable<ILogisticLineGroup> logisticLineGroups, IEnumerable<BillingMethod> orderBillingMethods, BillingMethod mainBillingMethod, int retryCount, int timeout)
        {
            orderGlobalInformations.OrderDate = DateTime.Now;

            var orderdetailcount = 0;

            foreach (var logisticLineGroup in logisticLineGroups)
            {
                orderdetailcount += logisticLineGroup.Articles.Count;
            }

            var multiKey = new MultiKey();

            var count = (logisticLineGroups.Count() == 1 && (mainBillingMethod == null || !mainBillingMethod.IsGlobalPayment)) ? logisticLineGroups.Count() * orderBillingMethods.Count() : (logisticLineGroups.Count() * orderBillingMethods.Count()) + 1;

            multiKey.AccountId = userContextInformations.AccountId < 0 ? KeyGenerator.KeyGeneratorBusiness.ACCOUNT_ID_MAX_VALUE + userContextInformations.AccountId : userContextInformations.AccountId;
            multiKey.AddAndRequestUserReference("MainOrder");
            multiKey.AddAndRequestUserReference("Order", logisticLineGroups.Count());
            multiKey.Add("OrderDetail", orderdetailcount);
            multiKey.Add("Transaction", count);
            multiKey.Add("TransactionOperation", count);

            KeyGenerator.KeyGeneratorBusiness.Generate(multiKey, retryCount, timeout);

            return multiKey;
        }
    }
}
