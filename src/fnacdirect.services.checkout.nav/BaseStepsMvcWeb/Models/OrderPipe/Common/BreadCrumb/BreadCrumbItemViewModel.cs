﻿namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Common
{
    public class BreadCrumbItemViewModel
    {
        public string StepName { get; set; }

        public string DisplayName { get; set; }

        public bool CanNavigate { get; set; }

        public bool IsCurrent { get; set; }
        public bool IsPrevious { get; set; }

        public string Link { get; set; }
    }
}
