using System.Xml.Serialization;
using FnacDirect.Technical.Framework.Web.UrlMgr;

namespace FnacDirect.OrderPipe.Config
{
    public class WebUIPipeDescriptor : UIPipeDescriptor
    {
        public Uri ControlPage { get; set; }
        public Uri DefaultPage { get; set; }

        [XmlIgnore]
        public UrlWriter ControlPageUrl { get; set; }

        [XmlIgnore]
        public UrlWriter DefaultPageUrl { get; set; }

        public string LoginStep { get; set; }

        public override void Init(IOP pipe, Pipe cp)
        {
            ControlPageUrl = UriConverter.Convert(ControlPage);
            DefaultPageUrl = UriConverter.Convert(DefaultPage);
        }
    }
}
