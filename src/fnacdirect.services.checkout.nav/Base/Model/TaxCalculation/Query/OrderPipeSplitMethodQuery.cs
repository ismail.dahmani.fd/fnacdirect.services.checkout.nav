using FnacDirect.OrderPipe.TaxCalculation.Front.Models;

namespace FnacDirect.OrderPipe.Base.Model.TaxCalculation
{
    public class OrderPipeSplitMethodQuery : Query
    {
        public int TaxType { get; set; }
        public decimal GlobalCost { get; set; }
        public decimal UnitCost { get; set; }

        public SplitMethodQuery ToSplitMethodQuery()
        {
            var splitMethodQuery = new SplitMethodQuery
            {
                OrderContext = OrderContext.ToOrderContext(),
                TaxType = TaxType,
                GlobalCost = GlobalCost,
                UnitCost = UnitCost
            };

            return splitMethodQuery;
        }
    }
}
