﻿using Common.Logging;
using FnacDirect.Mailing;
using System;


namespace FnacDirect.OrderPipe.Base.Business.Neolane
{
    public class NeolaneService : INeolaneService
    {
        private readonly ILog _log;

        public NeolaneService(ILog log)
        {
            _log = log;
        }

        public void Send(string content)
        {
            try
            {
                var mailSender = new MailSender();
                mailSender.SendToNeolaneWS(content);
            }
            catch (Exception exception)
            {
                _log.Error("Error when calling neolane", exception);
            }
        }
    }
}
