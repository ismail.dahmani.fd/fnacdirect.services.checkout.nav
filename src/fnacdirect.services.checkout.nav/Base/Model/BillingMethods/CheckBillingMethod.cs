using System.Xml.Serialization;

namespace FnacDirect.OrderPipe.Base.Model
{
    public class CheckBillingMethod : BillingMethod
    {
        public const int IdBillingMethod = 2;

        public override bool IsPrimary => true;

        [XmlIgnore]
        private string _CheckNumber;

        public string CheckNumber
        {
            get
            {
                return _CheckNumber;
            }
            set
            {
                _CheckNumber = value;
            }
        }

        [XmlIgnore]
        private decimal? _ExpectedAmountEur;

        public decimal? ExpectedAmountEur
        {
            get
            {
                return _ExpectedAmountEur;
            }
            set
            {
                _ExpectedAmountEur = value;
            }
        }

        public CheckBillingMethod()
        {
            BillingMethodId = IdBillingMethod;
            Priority = -1;
            CanModifyAmountUp = true;
            CanModifyAmountDown = true;
            ImplyValidateOrder = true;
        }
    }
}
