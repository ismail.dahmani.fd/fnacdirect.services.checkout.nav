﻿using FnacDirect.OrderPipe.Base.Business;
using FnacDirect.OrderPipe.Base.LineGroups;
using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.OrderPipe.Base.Model.UserInformations;
using System.Collections.Generic;

namespace FnacDirect.OrderPipe.CM.Business
{
    public interface IGuptCMBusiness
    {
        string GetGUPTRefInvoiceShop(ILogisticLineGroup lg,
            FnacStore.Model.FnacStore store,
            IEnumerable<BillingMethod> billingMethods,
            IUserContextInformations userInformations,
            string contactEmail,
            BillingAddress billingAddress);
    }
}
