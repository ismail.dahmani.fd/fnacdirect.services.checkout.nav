﻿using FnacDirect.OrderPipe.Base.Model;

namespace FnacDirect.OrderPipe.Base.Business.Shipping
{
    public interface IVatCountryCalculationService
    {
        VatCountry GetVatCountry(int localCountryId, BillingAddress billingAddress, ShippingAddress shippingAddress);
        VatCountry GetVatCountry(int localCountryId, ShippingAddress shippingAddress);
    }
}
