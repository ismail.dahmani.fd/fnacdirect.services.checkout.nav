﻿namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Common
{
    public class CountryViewModel : BaseChoiceData
    {
        public string Code2 { get; set; }

        public string CellPhoneValidation { get; set; }

        public string HomePhoneValidation { get; set; }

        public string PhoneValidation { get; set; }

        public string ZipCodeValidation { get; set; }

        public string TaxNumberValidation { get; set; }

        public bool IsWebsiteCountry { get; set; }

        public bool IsEUCountry { get; set; }
    }
}
