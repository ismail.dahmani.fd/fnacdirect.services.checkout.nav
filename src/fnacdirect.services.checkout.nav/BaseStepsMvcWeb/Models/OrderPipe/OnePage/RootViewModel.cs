using FnacDirect.OrderPipe.BaseMvc.Web.Models.OrderPipe.Common.Scoring;
using FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Basket;
using FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Common;
using FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Common.Basket;
using FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Common.Payment;
using FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Common.Shipping;
using FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Membership;
using FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Omniture;
using System;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.OnePage
{
    public class RootViewModel : BaseRootViewModel, IRootViewModel
    {
        public RootViewModel()
        {
            Payment = Maybe.Empty<PaymentViewModel>();

            MemberBirthDate = Maybe.Empty<MemberBirthDateViewModel>();
        }

        public Basket.BasketViewModel Basket { get; set; }

        public ShippingViewModel Shipping { get; set; }

        public MembershipViewModel Membership { get; set; }

        public Maybe<PaymentViewModel> Payment { get; set; }

        public OptionsViewModel Options { get; set; }

        public Maybe<MembershipCardViewModel> AdjustedMembershipCard { get; set; }

        public Maybe<MemberBirthDateViewModel> MemberBirthDate { get; set; }

        public bool HasAdherentNumber { get; set; }

        public Pop.Basket.LoginPopinViewModel Login { get; set; }

        public bool DisplayRelogin { get; set; }

        public string PageName { get; set; }

        public bool IsPaymentRequired { get; set; }

        public bool IsFixed { get; set; }

        public SellerInformationsViewModel Seller { get; set; }

        public OmnitureBasketViewModel Omniture { get; set; }

        public TrackingViewModel Tracking { get; set; }

        public RemindersViewModel Reminders { get; set; }

        public Pop.Basket.TermsAndConditionsViewModel TermsAndConditions { get; set; }

        public ScoringViewModel Scoring { get; set; }
    }
}
