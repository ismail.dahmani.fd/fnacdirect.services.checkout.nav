﻿using FnacDirect.OrderPipe.Base.Model.UserInformations;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Kobo
{
    public interface IKoboMergeAccountBuilder
    {
        KoboMergeAccountViewModel Build(UserContextInformations userContext);
    }
}