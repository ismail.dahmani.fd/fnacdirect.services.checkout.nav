using FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Solex.Appointment;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.OrderThankYou
{
    public class OrderReferenceViewModel
    {
        public string Reference { get; set; }
        public int Index { get; set; }
        public int Total { get; set; }

        public string Seller { get; set; }

        public bool IsFnac { get; set; }

        public bool DisplaySeller { get; set; }
        public bool DisplayIndex { get; set; }

        public DeliverySlotViewModel DeliverySlotViewModel { get; set; }

        public bool HasDeliverySlot
        {
            get
            {
                return DeliverySlotViewModel != null;
            }
        }
    }
}
