using FnacDirect.Customer.Model;
using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.Technical.Framework.Switch;
using System.Configuration;

namespace FnacDirect.OrderPipe.Base.Business
{
    public enum PromoType
    {
        One = 1,
        ThreeYears = 2,
        OneYearOrClient = 3
    }

    public class PromotionStoreService : IPromotionStoreService
    {
        private readonly GuptConfiguration _guptConfiguration;

        public PromotionStoreService(GuptConfiguration guptConfiguration)
        {
            _guptConfiguration = guptConfiguration;
        }

        public string GetPromoCodeMotif(StandardArticle article, CustomerEntity customerEntity, bool basketContainsNewAdhThreeYears, string promotionCode)
        {
            var promoType = GetPromoTypeKey(article, customerEntity, basketContainsNewAdhThreeYears, promotionCode);
            return _guptConfiguration.Promotions[promoType.ToString()].CodeMotif;
        }

        public string GetPromoCodeType(StandardArticle article, CustomerEntity customerEntity, bool basketContainsNewAdhThreeYears, string promotionCode)
        {
            var promoType = GetPromoTypeKey(article, customerEntity, basketContainsNewAdhThreeYears, promotionCode);
            return _guptConfiguration.Promotions[promoType.ToString()].CodeType;
        }

        public bool IsAdherentPromotion(string promotionCode)
        {
            return !string.IsNullOrWhiteSpace(promotionCode) && promotionCode.ToUpperInvariant().Contains("ADH");
        }

        public int GetClientOrderDiscountCodeId()
        {
            return int.Parse(ConfigurationManager.AppSettings["orderPipe.orderDiscountCodeId"]);
        }

        public bool MustApplyPromotion(string pCodePromo)
        {
            return !Switch.IsEnabled("front.EnablePromoAdhOnly", true) || IsAdherentPromotion(pCodePromo);
        }

        private int GetPromoTypeKey(StandardArticle article, CustomerEntity customerEntity, bool basketContainsNewAdhThreeYears, string promotionCode)
        {
            int promoTypeKey;
            if (HasPromotion(article))
            {
                if (IsAdherentPromotion(promotionCode))
                {
                    if (customerEntity.AdhesionType.GetValueOrDefault(0) == 80)
                    {
                        promoTypeKey = (int)PromoType.One;
                    }
                    else if (customerEntity.FidelityProgramDuration == 3 || basketContainsNewAdhThreeYears)
                    {
                        promoTypeKey = (int)PromoType.ThreeYears;
                    }
                    else
                    {
                        promoTypeKey = (int)PromoType.OneYearOrClient;
                    }
                }
                else
                {
                    promoTypeKey = (int)PromoType.OneYearOrClient;
                }
            }
            else
            {
                promoTypeKey = (int)PromoType.OneYearOrClient;
            }
            return promoTypeKey;
        }

        private bool HasPromotion(StandardArticle article)
        {
            return article.PriceDBEur.Value - article.PriceUserEur.Value > 0;
        }
    }
}
