using FnacDirect.Basket.Business;
using FnacDirect.Contracts.Online.IModel;
using FnacDirect.Contracts.Online.Model;
using FnacDirect.Customer.BLL;
using FnacDirect.OrderPipe.Base.Business.ArticleConversions.OrderFormToShoppingCart;
using FnacDirect.OrderPipe.Base.Business.ArticleConversions.ShoppingCartToOrderForm;
using FnacDirect.OrderPipe.Base.Business.ShoppingCart;
using FnacDirect.OrderPipe.Base.LineGroups;
using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.OrderPipe.Base.Model.Facets;
using FnacDirect.OrderPipe.Base.Model.LineGroups;
using FnacDirect.Technical.Framework.CoreServices;
using FnacDirect.Technical.Framework.ServiceLocation;
using FnacDirect.Technical.Framework.Web;
using System;
using System.Collections.Generic;
using System.Linq;
using Article = FnacDirect.OrderPipe.Base.Model.Article;
using ArticleType = FnacDirect.OrderPipe.Base.Model.ArticleType;
using BM = FnacDirect.OrderPipe.Base.Model;
using SC = FnacDirect.ShoppingCartEntities;

namespace FnacDirect.OrderPipe.Base.Business
{
    public class BaseBusiness : IMergeLineGroupsArticlesBusiness, IUpdateArticlesBusiness, IShoppingCartBaseBusiness
    {
        private readonly IAddressBusiness _addressBusiness;
        private readonly IShoppingCartService _shoppingCartService;
        private readonly IShoppingCartToOrderFormConverter _shoppingCartToOrderFormArticleConverter;
        private readonly IArticleBusiness3 _articleBusiness;

        private readonly OrderFormToShoppingCartArticleConverter _orderFormToShoppingCartArticleConverter;

        public BaseBusiness()
        {
            _addressBusiness = ServiceLocator.Current.GetInstance<IAddressBusiness>();
            _shoppingCartService = ServiceLocator.Current.GetInstance<IShoppingCartService>();
            _shoppingCartToOrderFormArticleConverter = ServiceLocator.Current.GetInstance<IShoppingCartToOrderFormConverter>();
            _articleBusiness = ServiceLocator.Current.GetInstance<IArticleBusiness3>();
            _orderFormToShoppingCartArticleConverter = new OrderFormToShoppingCartArticleConverter();
        }

        public virtual void EmptyShoppingCart(IGotUserContextInformations igotUserContext, IEnumerable<ILineGroup> lineGroups)
        {
            EmptyShoppingCart(igotUserContext, lineGroups, SC.ShoppingCartType.StandardBasket);
        }

        protected void EmptyShoppingCart(IGotUserContextInformations igotUserContext, IEnumerable<ILineGroup> lineGroups, SC.ShoppingCartType shoppingCartType)
        {
            if (!ShoppingCartPersistantConfigurationManager.IsActive())
            {
                var sc = BasketBusiness.Load(shoppingCartType, igotUserContext.SID);
                sc.Articles.Clear();
                BasketBusiness.Save(shoppingCartType, sc);
            }
            else
            {

                _shoppingCartService.RetrieveShoppingCartOrderforms(igotUserContext.UserContextInformations.UID, igotUserContext.SID, shoppingCartType, out var ofSID, out var ofUID, out var ofFusionned, out var ofUIDExists);

                // Persistance de panier
                // Dans le cas où on vide le panier, on vide des 2 côtés
                var articleToDelete = (from SC.Article shoppingCartArticle in ofSID.Articles
                                       let pipeArticle = FindMatchingArticleInOrderForm(shoppingCartArticle, lineGroups.SelectMany(lg => lg.Articles))
                                       where pipeArticle != null
                                       select shoppingCartArticle).ToList();

                foreach (var shoppingCartArticle in articleToDelete)
                {
                    ofSID.Articles.Remove(shoppingCartArticle);
                }

                BasketBusiness.Save(shoppingCartType, ofSID);

                if (ofUIDExists)
                {
                    articleToDelete = (from SC.Article shoppingCartArticle in ofUID.Articles
                                       let pipeArticle = FindMatchingArticleInOrderForm(shoppingCartArticle, lineGroups.SelectMany(lg => lg.Articles))
                                       where pipeArticle != null
                                       select shoppingCartArticle).ToList();

                    foreach (var shoppingCartArticle in articleToDelete)
                    {
                        ofUID.Articles.Remove(shoppingCartArticle);
                    }

                    BasketBusiness.Save(shoppingCartType, ofUID);
                }
            }
        }

        /// <summary>
        /// Vérifie si les 2 articles correspondent
        /// (l'un vient du pipe, et l'autre vient du shoppingCart)
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <returns></returns>
        private bool AreArticlesMatch(Article x, SC.Article y)
        {
            if (x is MarketPlaceArticle && y is SC.MarketPlaceArticle)
            {
                var mpX = x as MarketPlaceArticle;
                var mpY = y as SC.MarketPlaceArticle;
                if (mpX.ProductID == mpY.ProductID && (mpX.OfferId == mpY.OfferId || (mpX.OfferRef != null && mpX.OfferRef.Equals(mpY.OfferReference, StringComparison.InvariantCultureIgnoreCase))))
                    return true;
                else
                    return false;
            }
            else if (x is BM.VirtualGiftCheck && y is SC.VirtualGiftCheck)
            {
                var vgcX = x as BM.VirtualGiftCheck;
                var vgcY = y as SC.VirtualGiftCheck;
                return vgcX.Amount == vgcY.Amount;
            }
            else
            {
                return x.ProductID == y.ProductID && x is StandardArticle && y is SC.StandardArticle;
            }
        }

        /// <summary>
        /// Vérifie si l'article du shoppingCart a été modifié dans l'Orderform du pipe
        /// </summary>
        /// <param name="articlePipe"></param>
        /// <param name="articleSC"></param>
        /// <returns></returns>
        private bool HasArticleChangedInBaseOF(Article articlePipe, SC.Article articleSC)
        {
            if (articlePipe.Quantity != articleSC.Quantity)
                return true;
            if (articlePipe is StandardArticle && articleSC is SC.StandardArticle)
            {
                var stdPipe = articlePipe as StandardArticle;
                var stdSC = articleSC as SC.StandardArticle;
                if (stdPipe.PriceUserEur != stdSC.PriceUserEur)
                    return true;
                if (stdPipe.Services.Count != stdSC.Services.Count)
                    return true;
                foreach (var service in stdPipe.Services)
                {
                    var hasMatch = false;
                    foreach (SC.Service srv in stdSC.Services)
                        if (service.ProductID == srv.ProductID)
                        {
                            hasMatch = true;
                        }
                    if (!hasMatch)
                        return true;
                }
            }
            return false;
        }

        /// <summary>
        /// Cherche une correspondance de l'article du ShoppingCart dans l'orderform
        /// </summary>
        /// <param name="article"></param>
        /// <param name="of"></param>
        /// <returns></returns>
        private Article FindMatchingArticleInOrderForm(SC.Article articleToFind, IGotLineGroups iGotLineGroups)
        {
            foreach (var article in iGotLineGroups.Articles)
            {
                if (!string.IsNullOrEmpty(article.ShoppingCartOriginId) && AreArticlesMatch(article, articleToFind))
                {
                    return article;
                }
            }

            return null;
        }

        /// <summary>
        /// Cherche une correspondance de l'article du ShoppingCart dans une liste d'article
        /// </summary>
        /// <param name="article"></param>
        /// <param name="of"></param>
        /// <returns></returns>
        private Article FindMatchingArticleInOrderForm(SC.Article article, IEnumerable<Article> articles)
        {
            foreach (var art in articles)
            {
                if (AreArticlesMatch(art, article))
                    return art;
            }
            return null;
        }

        /// <summary>
        /// Cherche une correspondance de l'article du ShoppingCart dans le pipe
        /// </summary>
        /// <param name="article"></param>
        /// <param name="of"></param>
        /// <returns></returns>
        private SC.Article FindMatchingArticleInShoppingCart(Article article, SC.Orderform of)
        {
            foreach (SC.Article art in of.Articles)
            {
                if (AreArticlesMatch(article, art))
                    return art;
            }
            return null;
        }

        public void TransfertOrderFormToShoppingCart(MultiFacet<IGotLineGroups, IGotUserContextInformations> multiFacets)
        {
            var iGotLineGroups = multiFacets.Facet1;
            var sid = multiFacets.Facet2.SID;
            var userContextInformations = multiFacets.Facet2.UserContextInformations;

            if (!iGotLineGroups.ArticlesModified)
            {
                return;
            }

            iGotLineGroups.ArticlesModified = false;

            if (!ShoppingCartPersistantConfigurationManager.IsActive())
            {
                var shoppingCart = BasketBusiness.Load(SC.ShoppingCartType.StandardBasket, sid);

                shoppingCart.Articles = _orderFormToShoppingCartArticleConverter.ConvertArticles(iGotLineGroups.Articles);

                if (Front.WebBusiness.Configuration.WebSiteConfiguration.Value.Mode.IsCCM)
                {
                    BasketBusiness.Save(SC.ShoppingCartType.ShopBasket, shoppingCart);
                }
                else
                {
                    BasketBusiness.Save(SC.ShoppingCartType.StandardBasket, shoppingCart);
                }
            }
            else
            {
                var saveUID = false;
                var deletedArticles = new List<SC.Article>();

                _shoppingCartService.RetrieveShoppingCartOrderforms(userContextInformations.UID, sid, SC.ShoppingCartType.StandardBasket, out var shoppingCartSid, out var shoppingCartUid, out var ofFusionned, out var ofUIDExists);

                // On transfert les articles ajoutés dans le pipe vers les paniers
                foreach (var articleFromPipe in iGotLineGroups.Articles)
                {
                    if (string.IsNullOrEmpty(articleFromPipe.ShoppingCartOriginId))
                    {
                        continue;
                    }

                    if (ofFusionned.Articles.Cast<SC.Article>().Any(articleFromFusionned => AreArticlesMatch(articleFromPipe, articleFromFusionned)))
                    {
                        continue;
                    }

                    if (VisitorAdapter.Current.GetVisitor().IsIdentified)
                    {
                        shoppingCartUid.Articles.Add(_orderFormToShoppingCartArticleConverter.Convert(articleFromPipe));
                        saveUID = true;
                    }
                    else
                    {
                        shoppingCartSid.Articles.Add(_orderFormToShoppingCartArticleConverter.Convert(articleFromPipe));
                    }
                }

                // on se base sur le shoppingCart fusionné pour chercher les correspondances
                for (var j = ofFusionned.Articles.Count - 1; j > -1; j--)
                {
                    var art = ofFusionned.Articles[j];
                    var pipeArt = FindMatchingArticleInOrderForm(art, iGotLineGroups);
                    var forceUpdateUID = false;
                    if (pipeArt != null)
                    {
                        // cherche s'il y a eu modification

                        if (!string.IsNullOrEmpty(pipeArt.ShoppingCartOriginId) && pipeArt.ShoppingCartOriginId.Equals(shoppingCartSid.SID, StringComparison.InvariantCultureIgnoreCase))
                        {
                            // 1. On met à jour dans le shoppingCart SID
                            for (var i = shoppingCartSid.Articles.Count - 1; i > -1; i--)
                            {
                                if (AreArticlesMatch(pipeArt, shoppingCartSid.Articles[i]) && HasArticleChangedInBaseOF(pipeArt, shoppingCartSid.Articles[i]))
                                {
                                    if (ofUIDExists)
                                    {
                                        var artUID = FindMatchingArticleInShoppingCart(pipeArt, shoppingCartUid);
                                        if (artUID != null)
                                        {
                                            // si l'utilisateur est connecté,
                                            // il faut faire les modifications dans le panier UID si l'article y existe
                                            forceUpdateUID = true;
                                            break;
                                        }
                                    }
                                    shoppingCartSid.Articles.RemoveAt(i);
                                    pipeArt.InsertDate = DateTime.Now;
                                    shoppingCartSid.Articles.Add(_orderFormToShoppingCartArticleConverter.Convert(pipeArt));
                                    break;
                                }
                            }
                        }

                        // si le client est authentifié, il faut vérifier si un panier n'est pas rattaché à son UID
                        if (ofUIDExists)
                        {
                            if (!string.IsNullOrEmpty(pipeArt.ShoppingCartOriginId) && pipeArt.ShoppingCartOriginId.Equals(shoppingCartUid.SID, StringComparison.InvariantCultureIgnoreCase))
                            {
                                // 2. On met à jour dans le shoppingCart UID
                                for (var i = shoppingCartUid.Articles.Count - 1; i > -1; i--)
                                {
                                    if (AreArticlesMatch(pipeArt, shoppingCartUid.Articles[i]) && HasArticleChangedInBaseOF(pipeArt, shoppingCartUid.Articles[i]) || forceUpdateUID)
                                    {
                                        // Une correspondance est trouvée ou on est en mode forcé (l'article existe dans le SID et dans l'UID)
                                        shoppingCartUid.Articles.RemoveAt(i);
                                        pipeArt.InsertDate = DateTime.Now;
                                        shoppingCartUid.Articles.Add(_orderFormToShoppingCartArticleConverter.Convert(pipeArt));
                                        saveUID = true;
                                        break;
                                    }
                                }
                            }
                            else if (forceUpdateUID)
                            {
                                // Une correspondance a été trouvée dans le SID, et il faut forcer la mis-à-jour dans l'UID
                                for (var i = shoppingCartUid.Articles.Count - 1; i > -1; i--)
                                {
                                    if (AreArticlesMatch(pipeArt, shoppingCartUid.Articles[i]))
                                    {
                                        // Une correspondance est trouvée ou on est en mode forcé (l'article existe dans le SID et dans l'UID)
                                        shoppingCartUid.Articles.RemoveAt(i);
                                        pipeArt.InsertDate = DateTime.Now;
                                        shoppingCartUid.Articles.Add(_orderFormToShoppingCartArticleConverter.Convert(pipeArt));
                                        saveUID = true;
                                        break;
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        // on cherche l'article potentiellement supprimé dans l'OrderForm.RemovedArticles,
                        // et on vérifie bien qu'il a un ShoppingCartOriginId de spécifié
                        pipeArt = FindMatchingArticleInOrderForm(art, iGotLineGroups.RemovedArticles);

                        if (pipeArt != null)
                        {
                            // il y a eu suppression
                            deletedArticles.Add(art);
                        }
                    }
                }

                var basketBusiness = new BasketBusiness();

                // suppression: on supprime dans les 2 paniers
                foreach (var article in deletedArticles)
                {
                    SC.Article articleShoppingCartSid = null;

                    if (article is SC.MarketPlaceArticle)
                    {
                        articleShoppingCartSid = basketBusiness.GetMarketPlaceArticle(shoppingCartSid.SID,
                                                  article.ProductID.GetValueOrDefault(),
                                                  (article as SC.MarketPlaceArticle).OfferReference);
                    }
                    else
                    {
                        articleShoppingCartSid = basketBusiness.GetArticle(shoppingCartSid.SID, article.ProductID.GetValueOrDefault());
                    }

                    var sidArticle = GetArticleInShoppingCart(shoppingCartSid, articleShoppingCartSid); //on vérifie que l'article existe bien dans la collection d'Articles du shoppingCartSid et on prends l'objet existant dans celle-ci pour le supprimer.
                    if (sidArticle != null)
                    {
                        shoppingCartSid.Articles.Remove(sidArticle);
                    }

                    if (ofUIDExists)
                    {
                        // on vérifie bien que l'article appartient bien à l'utilisateur courant
                        SC.Article articleShoppingCartUid = null;


                        if (article is SC.MarketPlaceArticle)
                        {
                            articleShoppingCartUid = basketBusiness.GetMarketPlaceArticle(shoppingCartUid.SID,
                                                                                          article.ProductID.GetValueOrDefault(),
                                                                                          (article as SC.MarketPlaceArticle).OfferReference);
                        }
                        else
                        {
                            articleShoppingCartUid = basketBusiness.GetArticle(shoppingCartUid.SID, article.ProductID.GetValueOrDefault());
                        }
                        var uidArticle = GetArticleInShoppingCart(shoppingCartUid, articleShoppingCartUid);

                        if (uidArticle != null)
                        {
                            shoppingCartUid.Articles.Remove(uidArticle);
                            saveUID = true;
                        }
                    }
                }

                // enregistrement
                BasketBusiness.Save(SC.ShoppingCartType.StandardBasket, shoppingCartSid);
                if (ofUIDExists && saveUID)
                {
                    BasketBusiness.Save(SC.ShoppingCartType.StandardBasket, shoppingCartUid);
                }
            }
        }

        public SC.Article GetArticleInShoppingCart(SC.Orderform of, SC.Article stdArt)
        {
            var isMarketPlace = stdArt is SC.MarketPlaceArticle;
            SC.Article existInOfSID = null;
            if (stdArt != null)
            {
                foreach (SC.Article art in of.Articles) //on vérifie que l'article existe bien dans la collection d'Articles du shoppingCartSid et on prends l'objet existant dans celle-ci pour le supprimer.
                {
                    if (art.ProductID == stdArt.ProductID)
                    {
                        if (isMarketPlace)
                        {
                            if (art is SC.MarketPlaceArticle mpArt
                                && mpArt.OfferReference == (stdArt as SC.MarketPlaceArticle).OfferReference)
                            {
                                existInOfSID = art;
                                break;
                            }
                        }
                        else
                        {
                            existInOfSID = art;
                            break;
                        }
                    }
                }
            }
            return existInOfSID;
        }

        public void AddOrderFormArticlesToShoppingCart(IGotLineGroups of)
        {
            if (of.Articles != null && of.Articles.Any())
            {
                var sc = BasketBusiness.Load(SC.ShoppingCartType.StandardBasket, of.SID);
                if (sc.Articles != null && sc.Articles.Count == 0)
                {
                    sc.Articles = _orderFormToShoppingCartArticleConverter.ConvertArticles(of.Articles);
                }
                else
                {
                    var scArticleList = _shoppingCartToOrderFormArticleConverter.ConvertArticles(sc.Articles);
                    if (scArticleList != null && scArticleList.Count > 0)
                    {
                        foreach (var currentArticle in of.Articles)
                        {
                            bool ArticleFound;
                            var searchResult = scArticleList.Find((Article p) => (p is StandardArticle || p is Bundle) && p.ProductID == currentArticle.ProductID);

                            ArticleFound = (searchResult != null);
                            if (currentArticle is MarketPlaceArticle)
                            {
                                var mpa = scArticleList.Find(delegate (Article p)
                                                        {
                                                            return p is MarketPlaceArticle && (p as MarketPlaceArticle).OfferId == (currentArticle as MarketPlaceArticle).OfferId;
                                                        });
                                ArticleFound = (mpa != null);
                            }
                            if (!ArticleFound)
                            {
                                var sca = _orderFormToShoppingCartArticleConverter.Convert(currentArticle);
                                if (sca != null)
                                    sc.Articles.Add(sca);
                            }
                        }
                    }
                }
                BasketBusiness.Save(SC.ShoppingCartType.StandardBasket, sc);
                of.ArticlesModified = false;
            }
        }

        public void LoadBillingAddress(BaseOF orderForm, OPContext context)
        {
            if (orderForm.BillingAddress != null && orderForm.UserInfo.CustomerEntity != null)
            {
                var billingAddress = orderForm.BillingAddress;
                if ((!string.IsNullOrEmpty(billingAddress.Reference)) && (billingAddress.Reference != "ADD"))
                {
                    try
                    {
                        var ba = _addressBusiness.Populate(orderForm.UserInfo.UID, billingAddress.Reference, Customer.IModel.CustomerEmums.FDAddressLoadFlags.lfAdrProperties, context.SiteContext.Culture);
                        if (ba.Id <= 0)
                        {
                            orderForm.BillingAddress = null;
                        }
                        else
                            orderForm.BillingAddress = new BillingAddress(ba);

                    }
                    catch (Exception ex)
                    {
                        orderForm.BillingAddress = null;
                        Logging.Current.WriteInformation("Erreur de chargement d'une adresse", ex);
                    }
                }
                else if (billingAddress.Reference != "ADD")
                {
                    orderForm.BillingAddress = null;
                }
            }
        }

        /// <summary>
        /// Change la quantité de l'article au niveau du linegroup
        /// </summary>
        public bool ChangeArticleQuantity(LineGroup lg, Article art, int newQty)
        {
            return ChangeArticleQuantity(lg.Articles, art, newQty);
        }

        /// <summary>
        /// Change la quantité de l'article au niveau d'une collection d'articles
        /// </summary>
        /// <param name="articles">Une collection d'articles</param>
        /// <param name="article">L'article à modifier</param>
        /// <param name="newQuantity">La nouvelle quantité</param>
        /// <returns></returns>
        public bool ChangeArticleQuantity(IEnumerable<Article> articles, Article article, int newQuantity)
        {
            foreach (var a in articles)
            {
                if ((a.ID == article.ID) && (a.SellerID == article.SellerID) && a.Quantity != newQuantity)
                {
                    a.Quantity = newQuantity;
                    if (a is StandardArticle std)
                    {
                        foreach (var service in std.Services)
                        {
                            service.Quantity = newQuantity;
                        }
                    }
                    return true;
                }
            }

            return false;
        }

        public bool DeleteArticle(OPContext opcontext, int prid)
        {
            var removed = false;
            var removeCount = 0;
            var orderForm = (BaseOF)opcontext.OF;
            var ar = new ArticleReference(prid, null, string.Empty, string.Empty);
            var art = _articleBusiness.GetArticle(opcontext.SiteContext, ar);
            if (art != null)
            {
                var count = orderForm.Articles.RemoveAll(
                delegate (Article article)
                {
                    if (article is StandardArticle std)
                    {
                        removeCount += CollectionUtils.RemoveItems(std.Services,
                            delegate (Service service)
                            {
                                if (service.ID == art.Reference.PRID)
                                {
                                    if (!orderForm.RemovedArticles.Contains(article))
                                        orderForm.RemovedArticles.Add(article);
                                    return true;
                                }
                                else return false;
                            }
                        );
                        if (std.ID == art.Reference.PRID)
                        {
                            if (!orderForm.RemovedArticles.Contains(article))
                                orderForm.RemovedArticles.Add(article);
                            return true;
                        }
                        else return false;
                    }
                    else
                        return false;
                });
                count += removeCount;
                removed = (count > 0);
            }
            return removed;
        }

        public bool DeleteArticle(OPContext opcontext, int prid, string offerReference)
        {
            var removed = false;
            var removeCount = 0;
            var orderForm = (BaseOF)opcontext.OF;
            var gOfferRef = new Guid(offerReference);

            var mpb = new MarketPlaceBusiness(opcontext.SiteContext);
            var offerId = mpb.GetOfferId(gOfferRef);
            var ar = new ArticleReference(prid, null, string.Empty, string.Empty);
            var art = _articleBusiness.GetArticle(opcontext.SiteContext, ar, ArticleLoadingOption.NoPriceAndInactive);

            if (art != null)
            {
                var count = orderForm.Articles.RemoveAll(
                delegate (Article article)
                {
                    if (article is MarketPlaceArticle mpa)
                        if (mpa.ID == art.Reference.ID && mpa.OfferId == offerId)
                        {
                            if (!orderForm.RemovedArticles.Contains(article))
                                orderForm.RemovedArticles.Add(article);
                            return true;
                        }
                        else return false;
                    else
                        return false;
                });
                count += removeCount;
                removed = (count > 0);
            }
            return removed;
        }

        public bool DeleteArticle(BaseOF of, Article art)
        {
            var c = 0;
            var count = of.Articles.RemoveAll(
                delegate (Article article)
                {
                    if (article is StandardArticle std)

                        c += CollectionUtils.RemoveItems(std.Services,
                            delegate (Service service)
                            {
                                if ((service.ID == art.ID) && (service.SellerID == art.SellerID))
                                {
                                    if (!of.RemovedArticles.Contains(article))
                                        of.RemovedArticles.Add(article);
                                    return true;
                                }
                                else
                                    return false;
                            }
                        );
                    if ((article.ID == art.ID) && (article.SellerID == art.SellerID))
                    {
                        if (!of.RemovedArticles.Contains(article))
                            of.RemovedArticles.Add(article);
                        return true;
                    }
                    else return false;
                }
            );
            count += c;
            return count > 0;
        }

        public bool DeleteArticle(IGotLineGroups iGotLineGroups, Article art)
        {
            var c = 0;
            var count = 0;
            foreach (var lg in iGotLineGroups.LineGroups)
            {
                count = lg.Articles.RemoveAll(
                    (Article article) =>
                    {
                        if (article is StandardArticle std)

                            c += CollectionUtils.RemoveItems(std.Services,
                                (Service service) =>
                                {
                                    if ((service.ID == art.ID) && (service.SellerID == art.SellerID))
                                    {
                                        if (!iGotLineGroups.RemovedArticles.Contains(article))
                                            iGotLineGroups.RemovedArticles.Add(article);
                                        return true;
                                    }

                                    return false;
                                }
                                );
                        if ((article.ID == art.ID) && (article.SellerID == art.SellerID))
                        {
                            if (!iGotLineGroups.RemovedArticles.Contains(article))
                                iGotLineGroups.RemovedArticles.Add(article);
                            return true;
                        }
                        return false;
                    }
                    );
            }
            count += c;
            return count > 0;
        }

        public bool DeleteArticle(BaseOF of, Article art, string parentId)
        {
            var c = 0;
            var count = of.Articles.RemoveAll(
                (Article article) =>
                {
                    if (article is StandardArticle std)
                    {
                        c += CollectionUtils.RemoveItems(std.Services,
                            (Service service) =>
                            {
                                if (parentId != string.Empty)
                                    if ((service.ID == art.ID) && (article.ID.GetValueOrDefault() == Convert.ToInt32(parentId)))
                                    {
                                        if (!of.RemovedArticles.Contains(article))
                                            of.RemovedArticles.Add(article);
                                        return true;
                                    }
                                    else
                                        return false;
                                else
                                    return false;
                            }
                        );
                        if ((article.ID == art.ID) && (article.SellerID == art.SellerID || article.SellerID == SellerIds.GetId(LineGroups.SellerType.ClickAndCollect)))
                        {
                            if (!of.RemovedArticles.Contains(article))
                                of.RemovedArticles.Add(article);
                            return true;
                        }
                        else
                            return false;
                    }
                    else
                    {
                        if (article is MarketPlaceArticle mpa)
                        {
                            if ((article.ID == art.ID) && (article.SellerID == art.SellerID))
                            {
                                if (!of.RemovedArticles.Contains(article))
                                    of.RemovedArticles.Add(article);
                                return true;
                            }
                            else return false;
                        }
                        else
                        {
                            if (article is BM.VirtualGiftCheck vcc)
                            {
                                if ((article.ID == art.ID) && (article.SellerID == art.SellerID) && (vcc.Amount == ((BM.VirtualGiftCheck)art).Amount))
                                {
                                    if (!of.RemovedArticles.Contains(article))
                                        of.RemovedArticles.Add(article);
                                    return true;
                                }
                                else return false;
                            }
                            else
                                return false;
                        }
                    }
                }
            );
            count += c;
            return count > 0;
        }

        /// <summary>
        /// Supprime les services de la ligne de commande égales à l'article passé en paramètre.
        /// </summary>
        /// <param name="lg">Ligne de commande à purger.</param>
        /// <param name="art">Article représentant un service à supprimer.</param>
        /// <returns>Retourne vrai si au moins un service a été supprimé.</returns>
        public bool DeleteArticle(LineGroup lg, Article art)
        {
            var c = 0;
            var count = lg.Articles.RemoveAll(
                delegate (Article article)
                {
                    if (article is StandardArticle std)
                        c += CollectionUtils.RemoveItems(std.Services,
                            delegate (Service service)
                            {
                                return service == art;
                            }
                        );
                    return article == art;
                }
            );
            count += c;
            return count > 0;
        }

        /// <summary>
        /// Supprime tout les articles dont la quantité est égale à 0.
        /// </summary>
        /// <param name="of">Formalaire de commande.</param>
        /// <returns>Retourne vrai si au moins un article a été supprimé.</returns>
        public bool DeleteZeroedArticle(BaseOF of)
        {
            var count = of.Articles.RemoveAll(
                delegate (Article article)
                {
                    if (article.Quantity == 0)
                    {
                        if (!of.RemovedArticles.Contains(article))
                            of.RemovedArticles.Add(article);
                        return true;
                    }
                    else
                        return false;
                }
            );
            return count > 0;
        }

        /// <summary>
        /// Supprime les articles ayant une quantité égale à 0 dans les linegroups
        /// </summary>
        /// <param name="of"></param>
        /// <returns></returns>
        public bool DeleteZeroedArticleFromLineGroups(BaseOF of)
        {
            var deletedCount = 0;

            foreach (var lg in of.LineGroups)
            {
                deletedCount += lg.Articles.RemoveAll(article => article.Quantity == 0);
            }
            return deletedCount > 0;
        }

        /// <summary>
        /// Effectue un merge de la quantité des articles du BaseOF.Articles vers BaseOF.LineGroups
        /// </summary>
        /// <param name="of"></param>
        public void MergeLineGroupsAndArticles(BaseOF of)
        {
            var articlesNotFound = new ArticleList(); // articles orphelins
            var servicesNotFound = new ArticleList();

            foreach (var lg in of.LineGroups)
            {
                servicesNotFound.Clear();
                foreach (var lg_article in lg.Articles)
                {
                    // 1 - trouve la correspondance des articles de l'OF et des linegroups
                    var of_articles = FindMatchingLineGroupArticlesFromArticles(lg_article, of);
                    if (of_articles == null || of_articles.Count <= 0)
                        articlesNotFound.Add(lg_article);
                    else
                    {
                        if (lg_article is StandardArticle lg_StdArt)
                        {
                            foreach (var lg_serv in lg_StdArt.Services)
                            {
                                var serviceFound = false;
                                foreach (var of_art in of_articles.OfType<StandardArticle>().ToList())
                                {
                                    foreach (var of_serv in of_art.Services)
                                    {
                                        if (of_serv.ID == lg_serv.ID)
                                        {
                                            serviceFound = true;
                                            break;
                                        }
                                    }
                                    if (serviceFound)
                                    {
                                        break;
                                    }
                                }
                                if (!serviceFound)
                                {
                                    servicesNotFound.Add(lg_serv);
                                }
                            }

                        }
                    }
                }
                foreach (var serv in servicesNotFound)
                {
                    DeleteArticle(lg, serv);
                }
            }

            foreach (var lg_article in articlesNotFound)
            {
                foreach (var lg in of.LineGroups)
                {
                    lg.Articles.Remove(lg_article);
                }
            }
        }

        /// <summary>
        /// Cherche et renvoie les articles correspondants dans les linegroups
        /// </summary>
        /// <param name="of_article"></param>
        /// <param name="of"></param>
        /// <returns></returns>
        private ArticleList FindMatchingLineGroupArticlesFromArticles(Article lg_article, BaseOF of)
        {
            var articles = new ArticleList();

            // l'article est standard
            if (lg_article is StandardArticle stdArticle)
                foreach (var stdArt in CollectionUtils.FilterOnType<StandardArticle>(of.Articles))
                {
                    if (stdArticle.ProductID == stdArt.ProductID)
                        // l'article a été trouvé
                        articles.Add(stdArt);
                }

            // l'article est un ccv
            if (lg_article is BM.VirtualGiftCheck ccvArticle)
                foreach (var ccv in CollectionUtils.FilterOnType<BM.VirtualGiftCheck>(of.Articles))
                {
                    if (ccvArticle.Amount == ccv.Amount)
                        // l'article a été trouvé
                        articles.Add(ccv);
                }


            // l'article est un MP
            if (lg_article is MarketPlaceArticle mparticle)
                foreach (var mpart in CollectionUtils.FilterOnType<MarketPlaceArticle>(of.Articles))
                {
                    if ((mparticle.ID == mpart.ID) && (mparticle.SellerID == mpart.SellerID))
                        // l'article a été trouvé
                        articles.Add(mpart);
                }

            return articles;
        }

        public void AddArticleToFnacBasket(BaseOF orderForm, int productId)
        {
            var art = new StandardArticle
            {
                ProductID = productId,
                Type = ArticleType.Saleable,
                Quantity = 1
            };

            orderForm.Articles.Add(art);
            orderForm.Articles.Modified = true;

            if (orderForm.FnacArticles != null && orderForm.FnacArticles.Count > 0)
                orderForm.FnacArticles[0].Articles.Add(art);
            else
            {
                var lg = new LineGroup();
                lg.Seller.SellerId = art.SellerID.GetValueOrDefault();
                lg.ShippingAddress = orderForm.CalcShippingAddress;
                lg.VatCountry = orderForm.VatCountry;
                lg.Articles.Add(art);
                orderForm.LineGroups.Add(lg);
            }
        }

        public void AddArticleToFnacBasket(IGotLineGroups iGotLineGroups, StandardArticle article)
        {
            iGotLineGroups.ArticlesModified = true;

            if (iGotLineGroups.LineGroups.Any(lg => lg.HasFnacComArticle))
            {
                iGotLineGroups.LineGroups.First(lg => lg.HasFnacComArticle).Articles.Add(article);
            }
            else
            {
                var lg = new PopLineGroup
                {
                    Seller =
                    {
                        SellerId = article.SellerID.GetValueOrDefault()
                    }
                };
                lg.Articles.Add(article);
                iGotLineGroups.AddLineGroup(lg);
            }
        }

        public void AddArticleToFnacBasket(IGotLineGroups orderForm, int productId)
        {
            var art = new StandardArticle
            {
                ProductID = productId,
                Type = ArticleType.Saleable,
                Quantity = 1
            };

            AddArticleToFnacBasket(orderForm, art);
        }

        public void AddAccessoryToBasket(OPContext opcontext, int masterArticlePrid, List<Article> accessoryArticlesList, int quantity)
        {
            var orderForm = (BaseOF)opcontext.OF;
            foreach (var a in accessoryArticlesList)
            {
                var ar = new ArticleReference(a.ProductID, null, string.Empty, string.Empty);
                var article = _articleBusiness.GetArticle(opcontext.SiteContext, ar);
                if (article != null)
                {
                    StandardArticle art;
                    if (article.Core.IsBundle)
                        art = new Bundle();
                    else
                        art = new StandardArticle();
                    art.ProductID = a.ProductID;
                    art.Type = ArticleType.Saleable;
                    art.Quantity = quantity;
                    art.FatherProductId = masterArticlePrid;
                    orderForm.Articles.Add(art);
                }
            }
            orderForm.Articles.Modified = true;
        }
    }
}
