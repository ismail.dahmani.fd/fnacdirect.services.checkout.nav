using FnacDirect.OrderPipe.Base.Business.LogisticLineGroups.Internals.Handlers;
using FnacDirect.OrderPipe.Base.LineGroups;
using FnacDirect.Technical.Framework.ServiceLocation;
using StructureMap.Pipeline;
using System;
using System.Collections.Generic;
using System.Linq;

namespace FnacDirect.OrderPipe.Base.Business.LogisticLineGroups.Handlers
{
    public class DispatchFnacShippingFeesAndRoundProcessCompletedHandlerDescriptor : CompletedHandlerDescriptor<DispatchFnacShippingFeesAndRoundProcessCompletedHandler>
    {
        private readonly IEnumerable<string> _sourceIds;
        private readonly IEnumerable<string> _destinationIds;

        public DispatchFnacShippingFeesAndRoundProcessCompletedHandlerDescriptor(IEnumerable<string> sourceIds,
                                                                                IEnumerable<string> destinationIds)
        {
            _sourceIds = sourceIds;
            _destinationIds = destinationIds;
        }

        public override ExplicitArguments GetExplicitArguments()
        {
            var explicitArguments = new ExplicitArguments();

            explicitArguments.Set(new DispatchFnacShippingFeesEquallyProcessCompletedHandlerArguments(_sourceIds, _destinationIds));

            return explicitArguments;
        }

        public override int Priority
        {
            get
            {
                return 100;
            }
        }
    }

    public class DispatchFnacShippingFeesAndRoundProcessCompletedHandler : DispatchFnacShippingFeesEquallyProcessCompletedHandler
    {
        public DispatchFnacShippingFeesAndRoundProcessCompletedHandler(DispatchFnacShippingFeesEquallyProcessCompletedHandlerArguments arguments)
            : base(arguments)
        {
        }

        protected override void DispatchShippingFees(IEnumerable<ILogisticLineGroup> logisticLineGroups, Dictionary<int, int> logisticTypesCount, decimal initialAmountFnacLineGroups)
        {
            //TODO: (pour plus tard) Refacto possible avec DispatchFnacShippingFeesEquallyProcessCompletedHandler (la grosse différence est la méthode de calcul)
            //On fait la répartition des frais
            for (var i = 0; i < _allIds.Count(); i++)
            {
                var logisticLineGroup = logisticLineGroups.FirstOrDefault(l => l.UniqueId == _allIds.ElementAt(i));

                if (logisticLineGroup != null)
                {
                    //Remise a zero du Total price pour le recalculer le bons apres le scindage par la suite.
                    logisticLineGroup.SelectedShippingChoice.TotalPrice.Cost = 0;
                    logisticLineGroup.SelectedShippingChoice.TotalPrice.CostToApply = 0;
                    logisticLineGroup.SelectedShippingChoice.TotalPrice.NoTaxCost = 0;


                    //Parcours des groups pour lister les LogTypes.
                    foreach (var group in logisticLineGroup.SelectedShippingChoice.Groups)
                    {
                        //Si on a eu le scindage des Commandes Logistiques (Fnac+Offre locale) avec les memes LogType dans les deux commandes log.
                        if (logisticTypesCount.ContainsKey(group.Id) && logisticTypesCount[group.Id] > 1)
                        {
                            //Scindage des fais de port globaux
                            var newMethodTotalUserCost = (logisticLineGroup.GlobalPrices.TotalShoppingCartBusinessPriceUserEur.GetValueOrDefault() / initialAmountFnacLineGroups) * group.GlobalPrice.Cost;
                            group.GlobalPrice.Cost = Math.Round(((newMethodTotalUserCost * 100) / 5) * 5 / 100, 1);

                            if (group.GlobalPrice.CostToApply.HasValue)
                            {
                                var newMethodTotalUserCostToApply = (logisticLineGroup.GlobalPrices.TotalShoppingCartBusinessPriceUserEur.GetValueOrDefault() / initialAmountFnacLineGroups) * group.GlobalPrice.CostToApply.Value;
                                group.GlobalPrice.CostToApply = Math.Round(((newMethodTotalUserCostToApply * 100) / 5) * 5 / 100, 1);
                            }

                            if (group.GlobalPrice.NoTaxCost.HasValue)
                            {
                                var newMethodTotalUserNoTaxCost = (logisticLineGroup.GlobalPrices.TotalShoppingCartBusinessPriceUserEur.GetValueOrDefault() / initialAmountFnacLineGroups) * group.GlobalPrice.NoTaxCost.Value;
                                group.GlobalPrice.NoTaxCost = Math.Round(((newMethodTotalUserNoTaxCost * 100) / 5) * 5 / 100, 1);
                            }
                        }

                        //Calcule des prix unitaires.
                        var totalUnitPrice = 0m;
                        decimal? totalToApplyUnitPrice = 0m;
                        decimal? totalNoTaxCostUnitPrice = 0m;

                        foreach (var item in group.Items.Where(item => logisticLineGroup.Articles.Any(a => a.ProductID == item.Identifier.Prid)))
                        {
                            totalUnitPrice += item.UnitPrice.Cost * item.Quantity;
                            totalToApplyUnitPrice += item.UnitPrice.CostToApply * item.Quantity;
                            totalNoTaxCostUnitPrice += item.UnitPrice.NoTaxCost * item.Quantity;
                        }

                        //Recalcule du total.
                        logisticLineGroup.SelectedShippingChoice.TotalPrice.Cost += group.GlobalPrice.Cost + totalUnitPrice;
                        logisticLineGroup.SelectedShippingChoice.TotalPrice.CostToApply += group.GlobalPrice.CostToApply + totalToApplyUnitPrice;
                        logisticLineGroup.SelectedShippingChoice.TotalPrice.NoTaxCost += group.GlobalPrice.NoTaxCost + totalNoTaxCostUnitPrice;
                    }

                    logisticLineGroup.GlobalPrices.TotalShipPriceUserEur = logisticLineGroup.SelectedShippingChoice.TotalPrice.Cost;
                }
            }
        }
    }
}
