﻿using FnacDirect.OrderPipe.Base.Model;

namespace FnacDirect.OrderPipe.Base.DAL
{
    public interface ISplitMethodsDAL
    {
        SplitMethodList GetListOfAvailableSplitMethods();

        void AddSplitPrice(int orderFk, decimal dbPriceFrf, decimal dbPriceEur,
            decimal userPriceFrf, decimal userPriceEur, decimal? priceToPayFrf, decimal? priceToPayEur, decimal? vatRate, string vatCode);
    }
}
