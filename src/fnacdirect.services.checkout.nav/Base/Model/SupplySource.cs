﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FnacDirect.OrderPipe.Base.BaseModel.Model
{
    public class SupplySource
    {
        public int? SupplySourceId { get; set; }

        public int? OfferId { get; set; }

        public Guid? OfferReference { get; set; }

        public int? PreparationTimeInDays { get; set; }

    }
}
