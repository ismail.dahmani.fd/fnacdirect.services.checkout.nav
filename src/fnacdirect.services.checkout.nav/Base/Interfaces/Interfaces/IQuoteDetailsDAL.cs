using FnacDirect.OrderPipe.Base.Model;

namespace FnacDirect.OrderPipe.Base.DAL
{
    public interface IQuoteDetailsDal
    {
        QuoteInfos GetQuoteDetailBySid(string quoteSid, bool withDetail);

        void UpdateQuoteStatus(int quoteId, int newQuoteStatus, int newMainOrderPk);
    }
}
