using FnacDirect.OrderPipe.Base.BaseModel;
using FnacDirect.OrderPipe.Base.Business.Shipping.Shop;
using FnacDirect.OrderPipe.Base.LineGroups;
using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.OrderPipe.Base.Model.LineGroups;
using FnacDirect.OrderPipe.BaseMvc.Web.Models.Shipping.ShipToStore;
using FnacDirect.OrderPipe.BaseMvc.Web.Services.ShipToStore;
using FnacDirect.Technical.Framework.Web.Mvc;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Providers.ShipToStore
{
    public class StoreViewModelProvider : IStoreViewModelProvider
    {
        private readonly IShipToStoreAvailabilityService _shipToStoreAvailabilityService;
        private readonly ICustomerFavoriteStoreService _customerFavoriteStoreService;
        private readonly IApplicationContext _context;
        private readonly IStoreEligibilityLineGroupsService _storeEligibilityLineGroupsService;

        public StoreViewModelProvider(IShipToStoreAvailabilityService shipToStoreAvailabilityService, 
                                      ICustomerFavoriteStoreService customerFavoriteStoreService, 
                                      IApplicationContext context,
                                      IStoreEligibilityLineGroupsService storeEligibilityLineGroupsService)
        {
            _shipToStoreAvailabilityService = shipToStoreAvailabilityService;
            _customerFavoriteStoreService = customerFavoriteStoreService;
            _context = context;
            _storeEligibilityLineGroupsService = storeEligibilityLineGroupsService;
        }

        public IEnumerable<SearchResultGeoStoreViewModel> GetSearchResultsGeoStoreViewModel(IEnumerable<StoreSearchGeoResult> storeSearchGeoResults, IEnumerable<ILineGroup> lineGroups, Maybe<ShopShippingMethodEvaluationItem> shippingMethodEvaluationItem)
        {
            return GetSearchResultsStoreViewModelForArticlesAndApplyFavorite<SearchResultGeoStoreViewModel>(
               storeSearchGeoResults.Select(s => s.FnacStore),
               lineGroups,
               shippingMethodEvaluationItem,
               r =>
               {
                   var match = storeSearchGeoResults.FirstOrDefault(s => s.FnacStore.Id == r.Id);

                   if (match == null)
                       return;

                   r.Distance = match.DistanceFromSearch;
                   r.DistanceFormat =
                       match.DistanceFromSearch < 1
                       ? (string.Concat("{distance} ", _context.GetMessage("orderpipe.index.shiptostore.meter", null))).Replace("{distance}", (match.DistanceFromSearch * 1000).ToString(CultureInfo.InvariantCulture))
                       : (string.Concat("{distance} ", _context.GetMessage("orderpipe.index.shiptostore.km", null))).Replace("{distance}", match.DistanceFromSearch.ToString(CultureInfo.InvariantCulture));
               });
        }

        private IEnumerable<T> GetSearchResultsStoreViewModelForArticlesAndApplyFavorite<T>(IEnumerable<FnacStore.Model.FnacStore> fnacStores, IEnumerable<ILineGroup> lineGroups, Maybe<ShopShippingMethodEvaluationItem> shippingMethodEvaluationItem, Action<T> additionalInitializer = null)
   where T : SearchResultStoreViewModel, new()
        {
            var lineGroupList = lineGroups as IList<ILineGroup> ?? lineGroups.ToList();
            var articles = lineGroupList.GetArticles().OfType<StandardArticle>().Where(a => a.LogType.HasValue && a.LogType.Value != 9).ToList();            
            var eligibleStores = _storeEligibilityLineGroupsService.GetStoresEligibleForLineGroups(fnacStores, lineGroupList, shippingMethodEvaluationItem);

            foreach (var fnacStore in eligibleStores)
            {
                var availabilities = _shipToStoreAvailabilityService.GetAvailabilities(fnacStore, lineGroupList.GetArticles()).ToList();
                var searchResultStoreViewModel = SearchResultStoreViewModelFactory.Current.CreateSearchResultStoreViewModel(fnacStore, availabilities, articles, additionalInitializer);

                TryApplyFavoriteTo(searchResultStoreViewModel);

                yield return searchResultStoreViewModel;
            }
        }

        private void TryApplyFavoriteTo(StoreViewModel searchResultStoreViewModel)
        {
            var favoriteStoreId = _customerFavoriteStoreService.GetFavoriteStoreId();

            if (!favoriteStoreId.HasValue)
                return;

            if (searchResultStoreViewModel.Id == favoriteStoreId.Value)
            {
                searchResultStoreViewModel.IsFavorite = true;
            }
        }
    }
}
