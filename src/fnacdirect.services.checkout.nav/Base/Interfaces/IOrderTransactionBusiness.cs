using FnacDirect.OrderPipe.Base.LineGroups;
using FnacDirect.OrderPipe.Base.Model;
using System.Collections.Generic;

namespace FnacDirect.OrderPipe.Base.Business.OrderTransaction
{
    public interface IOrderTransactionBusiness
    {
        bool CanSetMainOrderTransaction(IEnumerable<ILogisticLineGroup> lineGroups, IEnumerable<BillingMethod> orderBillingMethods, BillingMethod mainBillingMethod = null);
        bool CanSetOrderTransaction(IEnumerable<BillingMethod> orderBillingMethods);
    }
}
