using System.Collections.Generic;

namespace FnacDirect.OrderPipe.Base.Business.Mailing
{
    public class BillingInfo
    {        
        public bool IsPayByRefBillingMethodUsed { get; set; }

        public bool IsMBWayBillingMethodUsed { get; set; }

        public bool IsPhoneBillingMethodUsed { get; set; }

        public bool IsCheckBillingMethoddUsed { get; set; }

        public bool IsVoucherBillingMethodUsed { get; set; }

        public bool IsVirtualGiftCheckBillingMethodUsed { get; set; }

        public bool IsOgoneCreditCardBillingMethodUsed { get; set; }

        public bool IsSipsCreditCardBillingMethodUsed { get; set; }

        public bool IsPayPalBillingMethodUsed { get; set; }

        public bool IsFibBillingMethodUsed { get; set; }

        public bool IsBankTransferMethodUsed { get; set; }

        public bool IsDeferredPaymentMethodUsed { get; set; }

        public bool IsOrangeBillingMethodUsed { get; set; }

        public List<PaymentMethod> PaymentMethods { get; set; }
    }
}
