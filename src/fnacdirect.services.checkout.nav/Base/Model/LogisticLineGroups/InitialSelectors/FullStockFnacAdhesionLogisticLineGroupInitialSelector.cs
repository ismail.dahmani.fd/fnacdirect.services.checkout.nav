﻿using FnacDirect.OrderPipe.Base.Business.LogisticLineGroups.Internals.InitialSelectors;
using FnacDirect.OrderPipe.Base.LineGroups;
using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.OrderPipe.Base.Model.LineGroups;
using FnacDirect.Technical.Framework.Switch;
using System.Collections.Generic;
using System.Linq;

namespace FnacDirect.OrderPipe.Base.Business.LogisticLineGroups.InitialSelectors
{
    public class FullStockFnacAdhesionLogisticLineGroupInitialSelector : BaseLineGroupInitialSelector
    {
        private readonly IEnumerable<int> _stockAvailabilityIds = new int[] { 1, 99, 101, 199 };

        public override IDictionary<ILineGroupHeader, IEnumerable<Article>> Select(IEnumerable<ILineGroup> lineGroups)
        {
            var articles = lineGroups.GetArticles();
            var standardArticles = articles.OfType<StandardArticle>();

            if (HasOnlyFullStockLineGroup(standardArticles.Where(a => !a.IsAdhesionCard)) && standardArticles.Any(a => a.IsAdhesionCard))
            {
                return lineGroups.Where(l => l.Seller.Type == SellerType.Fnac).ToDictionary(l => l as ILineGroupHeader, l => l.Articles.OfType<Article>());
            }

            return new Dictionary<ILineGroupHeader, IEnumerable<Article>>(); 
        }

        private bool HasOnlyFullStockLineGroup(IEnumerable<StandardArticle> articles)
        {
            if (!articles.Any())
            {
                return false;
            }

            foreach (var article in articles.OfType<StandardArticle>())
            {
                if (!_stockAvailabilityIds.Contains(article.AvailabilityId.GetValueOrDefault()))
                {
                    return false;
                }
            }
            return true;
        }
    }
}
