using Common.Logging;
using FnacDarty.AddressValidator.Models;
using FnacDarty.AddressValidator.Models.Request;
using FnacDirect.Contracts.Online.Model;
using FnacDirect.Technical.Framework.Web.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FnacDirect.OrderPipe.Base.Business.Shipping.Postal
{
    public class CapencyService : ICapencyService
    {
        private readonly FnacDarty.AddressValidator.Abstractions.IAddressService _addressService;
        private readonly ICountryService _countryService;
        private readonly ILog _logger;

        public CapencyService(
            FnacDarty.AddressValidator.Abstractions.IAddressService addressService,
            ICountryService countryService,
            ILog logger)
        {
            _addressService = addressService;
            _countryService = countryService;
            _logger = logger;
        }

        /// <summary>
        /// Verifie l'adresse pass� en param�tre avec Capency.
        /// </summary>
        /// <returns>Retourne le r�sultat de Capency. Retourne Invalid ou Valid avec l'adresse qui est pass� en param's en cas d'erreur technique pour la r�silience.</returns>
        public async Task<CapencyResult> VerifyAsync(IAddress address)
        {
            try
            {
                if (address == null)
                {
                    throw new ArgumentException("Address is empty");
                }

                if (address.CountryId != Constants.Countries.France)
                {
                    return new CapencyResult(AddressValidityType.Valid, address);
                }

                var country = _countryService.GetCountry(address.CountryId);
                if (country == null)
                {
                    throw new ArgumentException($"Couldn't find country by id: {address.CountryId}");
                }

                var request = new VerifyAddressRequest()
                {
                    Street = address.AddressLine1,
                    AdditionalInfos1 = address.AddressLine2,
                    AdditionalInfos2 = address.AddressLine3,
                    City = address.City,
                    ZipCode = address.Zipcode,
                    CountryCode = country.Code3
                };

                var result = await _addressService.VerifyAddressAsync(request);

                var resultType = result.Response.AddressValidity;
                var addresses = ConvertToAddressValidityAddresses(address, result.Response.Addresses);

                return new CapencyResult(resultType, addresses.ToArray());
            }
            catch (ArgumentException ex)
            {
                _logger.Warn("CapencyService, address or country not found.", ex);
                return new CapencyResult(AddressValidityType.Invalid, address);
            }
            catch (Exception ex)
            {
                _logger.Error("CapencyService, couldn't verify address.", ex);
                return new CapencyResult(AddressValidityType.Valid, address);
            }
        }

        private IEnumerable<AddressValidityAddress> ConvertToAddressValidityAddresses(IAddress originalAddress, IEnumerable<Address> verifiedAddresses)
        {
            var result = new List<AddressValidityAddress>();

            foreach (var verifiedAddress in verifiedAddresses)
            {
                var country = _countryService.GetCountries().FirstOrDefault(c => c.Code3.Equals(verifiedAddress.CountryCode, StringComparison.InvariantCultureIgnoreCase));
                result.Add(new AddressValidityAddress(originalAddress)
                {
                    AddressLine1 = verifiedAddress.Street.FullName,
                    AddressLine2 = verifiedAddress.Building.Name,
                    City = verifiedAddress.Locality.City,
                    Zipcode = verifiedAddress.Locality.ZipCode,
                    State = country.Label,
                    CountryId = country.ID
                });
            }

            return result;
        }
    }
}
