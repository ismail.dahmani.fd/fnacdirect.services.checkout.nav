using FnacDirect.OrderPipe.Base.Model.Facets;
using FnacDirect.OrderPipe.Core.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using Vanilla.Middle.Arborescence;
using Vanilla.Middle.Arborescence.Enums;

namespace FnacDirect.OrderPipe.Base.Business.Payment
{
    public class PaymentEligibilityService : IPaymentEligibilityService
    {
        private readonly ICheckArticleService _checkArticleService;
        private readonly IPipeParametersProvider _pipeParametersProvider;
        private readonly ITreeLeafServices _treeLeafServices;

        private readonly IEnumerable<int> _clickAndCollectExcludedArticleCategories;
        
        public PaymentEligibilityService(ICheckArticleService checkArticleService,
            IPipeParametersProvider pipeParametersProvider,
            ITreeLeafServices treeLeafServices)
        {
            _checkArticleService = checkArticleService;
            _pipeParametersProvider = pipeParametersProvider;
            _treeLeafServices = treeLeafServices;

            _clickAndCollectExcludedArticleCategories = _pipeParametersProvider.GetAsEnumerable<int>("clickandcollect.excludedarticlescategories");
        }

        public PaymentEligibilityRule GetRules(string property, bool disableEccvCheck = false, bool disableClickAndCollectCheck = false)
        {
            var allowedFieldGroupByCard = _pipeParametersProvider.Get(property, 0);
            var rules = (PaymentEligibilityRule)allowedFieldGroupByCard;

            if (disableEccvCheck)
            {
                rules = rules.Remove(
                    PaymentEligibilityRule.ECCV_Forbidden);
            }

            if (disableClickAndCollectCheck)
            {
                rules = rules.Remove(
                    PaymentEligibilityRule.ClickAndCollect_ArticleExclusion,
                    PaymentEligibilityRule.ClickAndCollect_Forbidden);
            }

            return rules;
        }

        public bool IsAllowed(IGotLineGroups gotLineGroups, PaymentEligibilityRule rules)
        {
            if (rules.HasFlag(PaymentEligibilityRule.ECCV_Forbidden)
                && HasECCVInBasket(gotLineGroups))
            {
                return false;
            }

            if (rules.HasFlag(PaymentEligibilityRule.ClickAndCollect_ArticleExclusion)
                && HasClickAndCollectArticles(gotLineGroups)
                && AreArticlesExcluded(gotLineGroups.LineGroups.SelectMany(lg => lg.CollectInStoreArticles)))
            {
                return false;
            }

            if (rules.HasFlag(PaymentEligibilityRule.ClickAndCollect_Forbidden)
                && HasClickAndCollectArticles(gotLineGroups))
            {
                return false;
            }

            if (rules.HasFlag(PaymentEligibilityRule.Numerical_Forbidden)
                && IsOnlyNumerical(gotLineGroups))
            {
                return false;
            }

            return true;
        }

        #region privates

        private bool AreArticlesExcluded(IEnumerable<Model.Article> articles)
        {
            return articles
                .Select(a => a.ID)
                .Select(id => _treeLeafServices.GetLeavesToObject(LeafType.Article, id.Value, 1)
                    .Union(_treeLeafServices.GetLeavesToObject(LeafType.ExternalArticle, id.Value, 1)))
                .SelectMany(tl => tl).Any(l => _clickAndCollectExcludedArticleCategories.Contains(l.TreeNode.Target.Id));
        }

        private bool HasECCVInBasket(IGotLineGroups gotLineGroups) => _checkArticleService.HasECCV(gotLineGroups.Articles);

        private bool HasClickAndCollectArticles(IGotLineGroups gotLineGroups) => gotLineGroups.LineGroups.SelectMany(lg => lg.CollectInStoreArticles).Any();

        private bool IsOnlyNumerical(IGotLineGroups gotLineGroups) => gotLineGroups.IsOnlyNumerical();

        #endregion
    }
}
