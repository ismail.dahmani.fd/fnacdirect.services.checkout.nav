﻿using System;
using System.Collections.Generic;
using System.Web.Routing;
using FnacDirect.OrderPipe.CoreRouting;

namespace FnacDirect.OrderPipe.CoreMvcWeb.Routing
{
    public static class RouteCollectionExtensions
    {
        public static Route MapOrderPipeRoute(this RouteCollection routeCollection, string name, string url)
        {
            return MapOrderPipeRoute(routeCollection, name, url, null);
        }

        public static Route MapOrderPipeRoute(this RouteCollection routeCollection, string name, string url,
                                              object defaults)
        {
            return MapOrderPipeRoute(routeCollection, name, url, defaults, null);
        }

        public static Route MapOrderPipeRoute(this RouteCollection routeCollection, string name, string url,
                                              string[] namespaces)
        {
            return MapOrderPipeRoute(routeCollection, name, url, null, namespaces);
        }

        public static Route MapOrderPipeRoute(this RouteCollection routeCollection, string name, string url,
                                              object defaults, object constraints)
        {
            return MapOrderPipeRoute(routeCollection, name, url, defaults, constraints, null);
        }

        public static Route MapOrderPipeRoute(this RouteCollection routeCollection, string name, string url,
                                              object defaults, string[] namespaces)
        {
            return MapOrderPipeRoute(routeCollection, name, url, defaults, null, namespaces);
        }

        public static Route MapOrderPipeRoute(this RouteCollection routeCollection, string name, string url, object defaults, object constraints, string[] namespaces)
        {
            if (routeCollection == null)
                throw new ArgumentNullException("routeCollection");

            if(url == null)
                throw new ArgumentNullException("url");

            var route = new Route(url, new OrderPipeRouteHandler())
            {
                Defaults = CreateRouteValueDictionary(defaults),
                Constraints = CreateRouteValueDictionary(constraints),
                DataTokens = new RouteValueDictionary
                {
                    { "__RouteName", name }
                }
            };

            if ((namespaces != null) && (namespaces.Length > 0))
            {
                route.DataTokens["Namespaces"] = namespaces;
            }

            routeCollection.Add(name, route);

            return route;
        }


        public static Route MapMvcOrderPipeRoute(this RouteCollection routeCollection, string name, string url)
        {
            return MapMvcOrderPipeRoute(routeCollection, name, url, null);
        }

        public static Route MapMvcOrderPipeRoute(this RouteCollection routeCollection, string name, string url,
                                              object defaults)
        {
            return MapMvcOrderPipeRoute(routeCollection, name, url, defaults, null);
        }

        public static Route MapMvcOrderPipeRoute(this RouteCollection routeCollection, string name, string url,
                                              string[] namespaces)
        {
            return MapMvcOrderPipeRoute(routeCollection, name, url, null, namespaces);
        }

        public static Route MapMvcOrderPipeRoute(this RouteCollection routeCollection, string name, string url,
                                              object defaults, object constraints)
        {
            return MapMvcOrderPipeRoute(routeCollection, name, url, defaults, constraints, null);
        }

        public static Route MapMvcOrderPipeRoute(this RouteCollection routeCollection, string name, string url,
                                              object defaults, string[] namespaces)
        {
            return MapMvcOrderPipeRoute(routeCollection, name, url, defaults, null, namespaces);
        }

        public static Route MapMvcOrderPipeRoute(this RouteCollection routeCollection, string name, string url, object defaults, object constraints, string[] namespaces)
        {
            if (routeCollection == null)
                throw new ArgumentNullException("routeCollection");

            if (url == null)
                throw new ArgumentNullException("url");

            var route = new Route(url, new MvcOrderPipeRouteHandler())
            {
                Defaults = CreateRouteValueDictionary(defaults),
                Constraints = CreateRouteValueDictionary(constraints),
                DataTokens = new RouteValueDictionary()
            };

            if ((namespaces != null) && (namespaces.Length > 0))
            {
                route.DataTokens["Namespaces"] = namespaces;
            }

            routeCollection.Add(name, route);

            return route;
        }

        private static RouteValueDictionary CreateRouteValueDictionary(object values)
        {
            var dictionary = values as IDictionary<string, object>;
            
            return dictionary != null ? new RouteValueDictionary(dictionary) : new RouteValueDictionary(values);
        }
    }
}