﻿namespace FnacDirect.OrderPipe.BaseMvc.Web.Services.ShipToStore
{
    public interface ICustomerFavoriteStoreService
    {
        int? GetFavoriteStoreId();
    }
}