﻿using FnacDirect.OrderPipe.Base.Model;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Common.Payment
{
    public interface IPaymentMethodViewModelFactory
    {
        BillingMethodViewModel GetBillingMethodViewModel(BillingMethodDescriptor billingDescription, PaymentMethod paymentMethod, PopOrderForm popOrderForm, OPContext opContext, string tabName);        
    }
}
