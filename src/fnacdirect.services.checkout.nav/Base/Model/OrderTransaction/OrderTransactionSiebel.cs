using FnacDirect.Membership.Model.Entities;
using System;

namespace FnacDirect.OrderPipe.FDV.Model
{
    public class OrderTransactionSiebel
    {
        public Guid Reference { get; set; }
        public int? OrderId { get; set; }
        public int? OrderSiebelId { get; set; }
        public string TransactionSiebelId { get; set; }
        public string Amount { get; set; }

        public string AdherentNumber { get; set; }

        public string ContractNumber { get; set; }

        public string CardNumber { get; set; }

        public int AdvantageType
        {
            get
            {
                return (int)AdvantageNature.FidelityEuros;
            }
        }
    }
}
