﻿using System;
using System.ComponentModel.DataAnnotations;


namespace FnacDirect.OrderPipe.BaseMvc.Web.Attributes
{

    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false, Inherited = true)]
    public sealed class RequiredIfAttribute : ValidationAttribute
    {
        private const string DefaultErrorMessage = "The {0} field is required.";
        public string OtherProperty { get; private set; }
        public bool ValueProperty { get; private set; }
        public string ResourceName { get; set; }

        public RequiredIfAttribute(string otherProperty, bool value, string resourceName)
        {
            if (string.IsNullOrEmpty(otherProperty))
            {
                throw new ArgumentNullException("otherProperty");
            }

            OtherProperty = otherProperty;
            ValueProperty = value;
            ResourceName = resourceName;
        }



        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            var referencePropertyInfo = validationContext.ObjectInstance.GetType().GetProperty(OtherProperty);
            var referencePropertyValue = referencePropertyInfo.GetValue(validationContext.ObjectInstance, null);


            if (!(referencePropertyValue is bool))
            {
                throw new NotSupportedException(OtherProperty + " must be a bool");
            }


            if ((bool)referencePropertyValue == ValueProperty && string.IsNullOrEmpty(value as string))
            {
                ErrorMessage = !string.IsNullOrEmpty(ResourceName) ? MessageHelper.Get(Technical.Framework.Web.FnacContext.Current.SiteContext, ResourceName) : DefaultErrorMessage;
                return new ValidationResult(string.Format(ErrorMessageString, validationContext.DisplayName));
            }

            return ValidationResult.Success;
        }


    }


}
