﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FnacDirect.OrderPipe.BaseMvc.Web.ApiControllers.Pop
{
    public static class PopApiEnvironments
    {
        public const string BasketPage = "DisplayBasket";
        public const string OnePage = "DisplayOnePage";
        public const string ShippingPage = "DisplayShippingAddress";
        public const string PaymentPage = "DisplayPayment";
    }
}
