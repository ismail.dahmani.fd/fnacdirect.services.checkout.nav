using FnacDirect.OrderPipe.Base.Model;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Pop.Models.SelfCheckout
{
    public interface ISelfCheckoutSummaryViewModelBuilder
    {
        SelfCheckoutSummaryViewModel Build(PopOrderForm popOrderForm);
    }
}
