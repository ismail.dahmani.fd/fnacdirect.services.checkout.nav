using FnacDirect.Basket.Business;
using FnacDirect.Customer.Model;
using FnacDirect.OrderPipe.Base.Business;
using FnacDirect.OrderPipe.Base.Business.Helpers;
using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.OrderPipe.BaseMvc.Web.Controllers.Pop;
using FnacDirect.Quote.Business;
using FnacDirect.Quote.DAL;
using FnacDirect.Quote.Model;
using FnacDirect.Quote.Model.Enums;
using FnacDirect.Technical.Framework.Web.Mvc;
using System;
using System.Linq;
using System.Web.Mvc;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Pop.Controllers
{
    public class PopCreateQuoteController : PopBaseController
    {
        private readonly OPContext _opContext;
        private readonly IBasketPersistService _basketPersistService;
        private readonly int _vatRateId;

        private readonly double QUOTE_EXPIRATION_DELAY;

        public PopCreateQuoteController(OPContext opContext,
                                        IApplicationContext applicationContext,
                                        IBasketPersistService basketPersistService)
        {
            _opContext = opContext;
            _basketPersistService = basketPersistService;
            _vatRateId = opContext.Pipe.GlobalParameters.Get("shipping.vat.rate.id", -1);

            try
            {
                QUOTE_EXPIRATION_DELAY = double.Parse(applicationContext.GetAppSetting("quote.expirationDelay"));
            }
            catch (Exception)
            {
                QUOTE_EXPIRATION_DELAY = 8;
            }
        }

        [HttpGet]
        public ActionResult Index()
        {
            var popOrderForm = _opContext.OF as PopOrderForm;

            if (popOrderForm.BillingAddress == null || string.IsNullOrWhiteSpace(popOrderForm.BillingAddress.Reference))
            {
                if (!popOrderForm.PipeMessages.GetAll().Any(m => m.RessourceId == "orderpipe.pop.quote.create.noaddress"))
                {
                    var pipeMessage = popOrderForm.AddErrorMessage("Quote.Message", "orderpipe.pop.quote.create.noaddress");
                    pipeMessage.Behavior = PipeMessageBehavior.Persist;
                }
                return RewindAndRedirect();
            }
            if (!popOrderForm.LineGroups.Any())
            {
                if (!popOrderForm.PipeMessages.GetAll().Any(m => m.RessourceId == "orderpipe.pop.quote.create.emptybasket"))
                {
                    var pipeMessage = popOrderForm.AddErrorMessage("Quote.Message", "orderpipe.pop.quote.create.emptybasket");
                    pipeMessage.Behavior = PipeMessageBehavior.Persist;
                }
                return RewindAndRedirect();
            }
            if (popOrderForm.ShippingMethodEvaluation.ShippingMethodEvaluationGroups.Any(sme => sme.SelectedShippingMethodEvaluation.SelectedShippingMethodId == (int)Constants.ShippingMethods.ChronopostDelivery))
            {
                if (!popOrderForm.PipeMessages.GetAll().Any(m => m.RessourceId == "orderpipe.pop.quote.create.usingChronopost"))
                {
                    var pipeMessage = popOrderForm.AddErrorMessage("Quote.Message", "orderpipe.pop.quote.create.usingChronopost");
                    pipeMessage.Behavior = PipeMessageBehavior.Persist;
                }
                return RewindAndRedirect();
            }

            var quoteForm = new QuoteForm()
            {
                IsPro = true
            };

            const string currentStep = "EntryStep";

            quoteForm.ShoppingCartSID = popOrderForm.SID;
            quoteForm.SID = Guid.NewGuid().ToString();
            quoteForm.AccountID = popOrderForm.UserContextInformations.AccountId;

            quoteForm.CreationDate = DateTime.Now;
            quoteForm.ExpirationDate = quoteForm.CreationDate.AddDays(QUOTE_EXPIRATION_DELAY);
            quoteForm.LastModificationDate = DateTime.Now;
            quoteForm.LastModifiedBy = popOrderForm.UserContextInformations.AccountId.ToString();

            quoteForm.QuoteMaster = -1;
            if (popOrderForm.UserContextInformations.IdentityImpersonator != null)
            {
                quoteForm.Status = (int)QuoteStatusEnum.ToValidate;
                quoteForm.QuoteDemandRaison = QuoteDemandRaisonEnum.CallCenter;
            }
            else
            {
                quoteForm.Status = (int)QuoteStatusEnum.Validated;
            }

            quoteForm.CurrentStep = currentStep;

            quoteForm.Company = new CompanyInfo()
            {
                ID = popOrderForm.UserContextInformations.CustomerEntity.CompanyId,
                Contact = new CustomerEntity()
                {
                    ProAccountNumber = popOrderForm.UserContextInformations.CustomerEntity.ProAccountNumber
                }
            };

            quoteForm.TotalHtBeforeDiscount = quoteForm.TotalHT = (popOrderForm.GlobalPrices.TotalShoppingCartBusinessPriceNoVatEur.Value + popOrderForm.GlobalPrices.TotalEcoTaxEurNoVAT.GetValueOrDefault());
            quoteForm.TotalTtcBeforeDiscount = quoteForm.Total_TTC = popOrderForm.GlobalPrices.TotalPriceEur.Value;
            quoteForm.PortCost = popOrderForm.GlobalPrices.TotalShipPriceUserEurNoVAT.GetValueOrDefault();
            quoteForm.WrapCost = popOrderForm.GlobalPrices.TotalShoppingCartBusinessWrapPriceNoVatEur.GetValueOrDefault();
            quoteForm.VAT = popOrderForm.GlobalPrices.VATList;

            quoteForm.LineGroups = new LineGroupList();
            quoteForm.AddressRepository = new QuoteAddressEntityCollection();
            var shippingFranceOnly = true;

            foreach (var lineGroup in popOrderForm.LineGroups)
            {
                var shippingMethodEvaluationGroup = popOrderForm.ShippingMethodEvaluation.Seller(lineGroup.Seller.SellerId).Value;
                var selectedShippingMethodEvaluation = shippingMethodEvaluationGroup.SelectedShippingMethodEvaluation;

                if (shippingMethodEvaluationGroup.SelectedShippingMethodEvaluationType == Base.BaseModel.ShippingMethodEvaluationType.Postal)
                {
                    var addressEntity = shippingMethodEvaluationGroup.PostalShippingMethodEvaluationItem.Value.ShippingAddress.AddressEntity;

                    if (addressEntity == null)
                    {
                        if (!popOrderForm.PipeMessages.GetAll().Any(m => m.RessourceId == "orderpipe.pop.quote.create.wrongaddress"))
                        {
                            var pipeMessage = popOrderForm.AddErrorMessage("Quote.Message", "orderpipe.pop.quote.create.wrongaddress");
                            pipeMessage.Behavior = PipeMessageBehavior.Persist;
                        }
                        return RewindAndRedirect();
                    }

                    if (!quoteForm.AddressRepository.Exists(item => string.Equals(addressEntity.AddressBookReference, item.AddressBookReference, StringComparison.InvariantCultureIgnoreCase)))
                    {
                        quoteForm.AddressRepository.Add(new QuoteAddressEntity(addressEntity));
                    }
                }

                var quoteLineGroup = new LineGroup()
                {
                    Articles = new ArticleList()
                };
                quoteForm.LineGroups.Add(quoteLineGroup);

                var addressID = 0;
                QuotePostalAddress addressOnCreation = null;

                addressID = (int)shippingMethodEvaluationGroup.PostalShippingMethodEvaluationItem.Value.ShippingAddress.Identity;
                addressOnCreation = new QuotePostalAddress(shippingMethodEvaluationGroup.PostalShippingMethodEvaluationItem.Value.ShippingAddress.AddressEntity);
                quoteLineGroup.ShippingAddress = addressOnCreation;

                if (quoteLineGroup.ShippingAddress.CountryId != 74)  // 74 = France Metropolitaine
                {
                    shippingFranceOnly = false;
                }

                var priceBusiness = new PriceBusiness(1);
                var vatRate = priceBusiness.GetVATRate(lineGroup.VatCountry.VatCountryId, _vatRateId);

                var selectedChoice = selectedShippingMethodEvaluation.Choices.FirstOrDefault(x => x.MainShippingMethodId == selectedShippingMethodEvaluation.SelectedShippingMethodId);

                quoteLineGroup.GlobalShippingMethodChoice = selectedChoice.MainShippingMethodId;
                quoteLineGroup.ShippingModeValueCollection = (from @group in selectedChoice.Groups
                                                              where @group.Items.Any()
                                                              let shippingMethodId = selectedChoice.Parcels[@group.Items.ElementAt(0).ParcelId].ShippingMethodId
                                                              select new ShippingModeValue()
                                                              {
                                                                  LogisticType = @group.Id,
                                                                  ShippingModeId = shippingMethodId,
                                                                  MethodGlobalUserCost = @group.GlobalPrice.Cost
                                                              }).ToList();

                quoteForm.OrderType = lineGroup.OrderType;
                quoteForm.LogisticTypes.AddRange(lineGroup.LogisticTypes);
                quoteLineGroup.OrderType = lineGroup.OrderType;
                quoteLineGroup.OrderInfo = new LineGroupInfo
                {
                    OrderType = lineGroup.Informations.OrderType,
                    GiftOption = lineGroup.Informations.GiftOption,
                    OrderDate = lineGroup.Informations.OrderDate,
                    OrderMessage = lineGroup.Informations.OrderMessage,
                    ChosenWrapMethod = lineGroup.Informations.ChosenWrapMethod,
                    OrderStatusId = lineGroup.Informations.OrderStatusId,
                    OrderTransactionInfos = lineGroup.Informations.OrderTransactionInfos
                };
                quoteLineGroup.LogisticTypes = lineGroup.LogisticTypes;

                foreach (var standardArticle in lineGroup.Articles.OfType<StandardArticle>())
                {
                    quoteLineGroup.Articles.Add(standardArticle);

                    var quoteLineDetail = new QuoteLineDetail();
                    var smv = new ShippingModeValue();
                    var group = selectedChoice.Groups.FirstOrDefault(x => x.Id == standardArticle.LogType.GetValueOrDefault());
                    var itemInGroup = group.Items.FirstOrDefault(x => x.Identifier.Prid == standardArticle.ProductID);
                    var parcel = selectedChoice.Parcels[itemInGroup.ParcelId];
                    var shippingMethodDetail = new Shipping.Model.ShippingMethodChoiceItemDetail
                    {
                        LogisticType = group.Id,
                        Quantity = standardArticle.GetQuantity(),
                        MethodGlobalCost = group.GlobalPrice.Cost,
                        MethodDetailCost = itemInGroup.UnitPrice.Cost,
                        MethodGlobalUserCost = group.GlobalPrice.Cost,
                        MethodDetailUserCost = itemInGroup.UnitPrice.Cost,
                        TotalCost = selectedChoice.TotalPrice.Cost,
                        UserTotalCost = selectedChoice.TotalPrice.Cost,
                        ShippingMethod = new Shipping.Model.ShippingMethod()
                        {
                            MethodId = parcel.ShippingMethodId,
                            TypeLogistic = group.Id,
                            IsActive = true,
                            PriceGlobal = group.GlobalPrice.Cost,
                            PriceDetail = itemInGroup.UnitPrice.Cost,
                            OriginalPriceGlobal = group.GlobalPrice.Cost,
                            OriginalPriceDetail = itemInGroup.UnitPrice.Cost,
                        }
                    };

                    standardArticle.ShippingMethodDetail = shippingMethodDetail;
                    standardArticle.ShipMethod = parcel.ShippingMethodId;

                    smv.LogisticType = group.Id;
                    smv.ShippingModeId = parcel.ShippingMethodId;
                    smv.ShippingModeLabel = selectedChoice.Label;

                    if (selectedChoice.TotalPrice.Cost == 0)
                    {
                        smv.MethodGlobalCost = 0;
                        smv.MethodGlobalUserCost = 0;
                        smv.ShippingModeExcTaxValue = 0;
                        smv.ShippingModeExcTaxValueOld = 0;
                    }
                    else
                    {
                        smv.MethodGlobalCost = itemInGroup.UnitPrice.Cost;
                        smv.MethodGlobalUserCost = group.GlobalPrice.Cost;
                        smv.ShippingModeExcTaxValue = PriceHelpers.GetPriceHT(group.GlobalPrice.Cost, vatRate);
                        smv.ShippingModeExcTaxValueOld = PriceHelpers.GetPriceHT(group.GlobalPrice.Cost, vatRate);
                    }

                    quoteLineDetail.QuoteDetailShippingPriceExclTax = PriceHelpers.GetPriceHT(itemInGroup.UnitPrice.Cost, vatRate);
                    quoteLineDetail.QuoteDetailOldShippingPrice = itemInGroup.UnitPrice.Cost;
                    quoteLineDetail.QuoteDetailOldShippingPriceExclTax = PriceHelpers.GetPriceHT(itemInGroup.UnitPrice.Cost, vatRate);

                    quoteLineDetail.ShipPriceNoVATEur = PriceHelpers.GetPriceHT(itemInGroup.UnitPrice.Cost, vatRate);
                    quoteLineDetail.ShipPriceUserEur = itemInGroup.UnitPrice.Cost;
                    quoteLineDetail.ShipPriceDBEur = itemInGroup.UnitPrice.Cost;
                    quoteLineDetail.GlobalShipPriceDBEur = group.GlobalPrice.Cost;

                    quoteLineDetail.QuoteShippingModeValue = smv;
                    quoteLineDetail.LogType = standardArticle.LogType;
                    quoteLineDetail.ProductValidation = QuoteProductValidation.New;

                    quoteLineDetail.AddressID = addressID;
                    quoteLineDetail.AddressOnCreation = addressOnCreation;

                    quoteLineDetail.QuoteArticleType = QuoteArticleTypeEnum.Other;
                    quoteLineDetail.QuoteDetailQuantity = (int)standardArticle.Quantity;
                    quoteLineDetail.QuoteDetailArticleReference = standardArticle.ReferenceGU;
                    quoteLineDetail.EcoTaxPrice = standardArticle.EcoTaxEur.GetValueOrDefault(0);
                    quoteLineDetail.EcoTaxPriceNoVAT = standardArticle.EcoTaxEurNoVAT.GetValueOrDefault(0);
                    quoteLineDetail.QuoteDetailArticleAvailabilityID = standardArticle.AvailabilityId;
                    quoteLineDetail.QuoteDetailArticleAvailability = standardArticle.AvailabilityLabel;
                    // end complent 

                    quoteLineDetail.QuoteDetailShippingTheoricDate = standardArticle.TheoreticalExpeditionDate;

                    // enregistrement des remises OPC
                    // ajoute les différents types de remise OPC dans la collection des remises
                    if (standardArticle.Promotions != null && standardArticle.Promotions.Count > 0)
                    {
                        AddOPCPriceDiscount(quoteLineDetail);
                        AddOPCShippingDiscount(quoteLineDetail);
                        AddOPCWrapDiscount(quoteLineDetail);
                    }

                    var newList = new ServiceList();

                    foreach (var service in standardArticle.Services)
                    {
                        var quoteLineService = new QuoteLineService();
                        ModelHelper.Copy(service, quoteLineService);
                        quoteLineService.QuoteDetailParentId = quoteLineDetail.QuoteDetailID;
                        quoteLineService.UnitPriceExclTax = quoteLineService.PriceNoVATEur.GetValueOrDefault(0);
                        newList.Add(quoteLineService);
                    }

                    ModelHelper.Copy(standardArticle, quoteLineDetail);

                    quoteLineDetail.Services = newList;
                    quoteLineDetail.QuoteDetailUnitDiscountAmountExclTax = 0;

                    quoteForm.QuoteLineDetailCollection.Add(quoteLineDetail);
                    quoteForm.Articles.Add(standardArticle);
                }
            }

            quoteForm.QuoteReference = QuoteDAL.GetQuoteSequenceReference().ToString();

            quoteForm.PrivateComment = string.Empty;

            quoteForm.ArticlesCount = quoteForm.Articles.Count;

            quoteForm.BillingAddressReference = popOrderForm.BillingAddress.Reference;

            if (!shippingFranceOnly && (quoteForm.LineGroups.Select(lg => lg.Articles.Sum(a => a.Quantity)).Sum() > 50 || quoteForm.Total_TTC > 10000))
            {
                if (!popOrderForm.PipeMessages.GetAll().Any(m => m.RessourceId == "orderpipe.pop.quote.create.personalize"))
                {
                    var pipeMessage = popOrderForm.AddErrorMessage("Quote.Message", "orderpipe.pop.quote.create.personalize");
                    pipeMessage.Behavior = PipeMessageBehavior.Persist;
                }
                return RewindAndRedirect();
            }

            quoteForm.UserInfo = new UserInfo()
            {
                AccountId = popOrderForm.UserContextInformations.AccountId,
                UID = popOrderForm.UserContextInformations.UID
            };

            var quoteBusiness = new QuoteBusiness();

            quoteBusiness.Save(QuoteType.Standard, quoteForm);

            _basketPersistService.BasketBus.Clear(ShoppingCartEntities.ShoppingCartType.StandardBasket, popOrderForm.SID);
            _basketPersistService.BasketBus.Clear(ShoppingCartEntities.ShoppingCartType.StandardBasket, popOrderForm.UserContextInformations.UID);
            Front.WebBusiness.W3ShoppingCart.IssueNbArtCookie(0);

            return RedirectToRoute("account.quotes.details.origin", new
            {
                quoteReference = quoteForm.QuoteReference,
                originStep = popOrderForm.CurrentStep
            });
        }

        private void AddOPCPriceDiscount(QuoteLineDetail quoteLineDetail)
        {
            AddOPCDiscount(quoteLineDetail, (int)DiscountRaison.OPC, (int)DiscountType.Article, quoteLineDetail.PriceUserEur.GetValueOrDefault(0), quoteLineDetail.PriceDBEur.GetValueOrDefault(0));
        }

        private void AddOPCShippingDiscount(QuoteLineDetail quoteLineDetail)
        {
            AddOPCDiscount(quoteLineDetail, (int)DiscountRaison.OPC, (int)DiscountType.ShippingPrice, quoteLineDetail.ShipPriceUserEur.GetValueOrDefault(0), quoteLineDetail.ShipPriceDBEur.GetValueOrDefault(0));
        }

        private void AddOPCWrapDiscount(QuoteLineDetail quoteLineDetail)
        {
            AddOPCDiscount(quoteLineDetail, (int)DiscountRaison.OPC, (int)DiscountType.WrapPrice, quoteLineDetail.WrapPriceUserEur.GetValueOrDefault(0), quoteLineDetail.WrapPriceDBEur.GetValueOrDefault(0));
        }

        private void AddOPCDiscount(QuoteLineDetail quoteLineDetail, int discountReason, int discountType, decimal userPrice, decimal dbPrice)
        {
            decimal discountPrice = 0;
            QuoteLineRemise remise = null;

            discountPrice = userPrice - dbPrice;

            remise = quoteLineDetail.Remises.Find(delegate (QuoteLineRemise r)
            {
                return r.DiscountReason == discountReason && r.DiscountType == discountType;
            });

            if (remise == null)
            {
                remise = new QuoteLineRemise()
                {
                    DiscountReason = discountReason,
                    DiscountType = discountType
                };
                quoteLineDetail.Remises.Add(remise);
            }

            remise.PriceDiscount = discountPrice;

            if (remise.PriceDiscount == 0)
            {
                quoteLineDetail.Remises.Remove(remise);
            }
        }
    }
}
