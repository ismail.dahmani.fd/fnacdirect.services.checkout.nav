using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.OrderPipe.Base.Model.FDV.Rebate;
using System;
using System.Collections.Generic;

namespace FnacDirect.OrderPipe.Base.DAL
{
    public interface IShopCommerceDAL
    {
        /// <summary>
        /// Insere une nouvelle ligne dans la table Tbl_ordershop afin d'ajouter une nouvelle commande
        /// </summary>
        bool AddOrderShop(int orderId, DateTime? withdrawalDate, string invoiceGUPTRef,
            string refUg, int shopIdDestination, string mailText, string mailHtml, string legalEntity,
            string vendorCode, string vendorLogin, string email, string phoneNumber,
            string mobilePhoneNumber, int? communicationMode, int shopIdOrigin, DateTime? cancelDate, string numCaissier, string rebateValidatorCode);

        bool UpdateOrderShop(int orderId, string numcaisse);

        int AddOrderShopTransaction(int orderId, string refUg, int accountId, string orderUserReference, int? orderType);

        bool UpdateOrderShopTransactionStatus(int pTransactionId, int pOrderId, OrderShopTransactionStatus pStatusId);

        /// <summary>
        /// Pour POP, Permet de mettre à jour la donnée guptInvoiceRef après l'appel s3
        /// </summary>
        void UpdateOrderShopTransactionGuptInvoiceRef(int transactionId, string guptInvoiceRef);

        void AddOrderInfo(int orderId, int orderInfoType, string orderInfo);

        IEnumerable<RemiseClient> GetOrderDiscountReasonsByCodeType(int pCodeType, bool? pIsShop = null);

        void AddContactInfos(int pOrderId, string pContactMail, string pContactPhoneNumber, int pContactInfoType);

        int AddRemoteRebate(RemoteRebate remoteRebate);

        RemoteRebateStatus UpdateRemoteRebate(int remoteRebateId, RemoteRebateStatus rebateStatus, string respMatricule, string respComment = null);

        IEnumerable<RemoteRebate> LoadRemoteRebates();

        RemoteRebate LoadRemoteRebate(int rebateId);

        bool IsLldOrderValidated(string contractNumber);

        bool HasOrderWithContractNumberValidated(string contractNumber, int orderDetailInfoTypeId);

        string GetShopSecurityCode(int shopId, bool forceReset);

        int GetShopId(int orderId);

        int GetRelayId(int orderId);
    }
}
