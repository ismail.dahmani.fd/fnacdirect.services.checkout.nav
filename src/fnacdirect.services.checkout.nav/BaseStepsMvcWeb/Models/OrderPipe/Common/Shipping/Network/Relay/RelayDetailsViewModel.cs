using FnacDirect.OrderPipe.Base.Business.Availability;
using FnacDirect.Relay.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Common.Shipping.Relay
{
    public class RelayDetailsViewModel : RelayViewModel, ISelectedShippingItem
    {
        public RelayDetailsViewModel(RelayAddressEntity relayAddressEntity, 
                                     string shortEstimatedDeliveryDate, 
                                     IEnumerable<PopAvailabilityProcessorOutput> longEstimatedDeliveryDate, 
                                     bool displayLongEstimatedDeliveryDates)
            : base(relayAddressEntity, shortEstimatedDeliveryDate, longEstimatedDeliveryDate, displayLongEstimatedDeliveryDates)
        {
            Timetables = Enumerable.Empty<TimetableViewModel>();
        }

        public double Latitude { get; set; }

        public double Longitude { get; set; }

        public string Distance { get; set; }

        public string CellPhoneNumber { get; set; }
        
        public string RegexPhoneNumber
        {
            get;          
            set;
        }
        public IEnumerable<TimetableViewModel> Timetables { get; set; }

        public string Key
        {
            get
            {
                return "Relay";
            }
        }

        public bool IsCellPhoneNumberSet
        {
            get
            {
                return !string.IsNullOrEmpty(CellPhoneNumber);
            }
        }
        
        public SelectedShippingItemSummary Summary { get; set; }
        public string Label { get; set; }
    }
}
