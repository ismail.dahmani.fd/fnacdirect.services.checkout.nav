using System.Collections.Generic;
using FnacDirect.OrderPipe.Base.Model.LineGroups;

namespace FnacDirect.OrderPipe.Base.Model.Facets
{
    public interface IGotLineGroups : IOF
    {
        IEnumerable<ILineGroup> LineGroups { get; }

        IEnumerable<Article> Articles { get; }
        void AddArticle(Article article);
        void RemoveArticles(ArticleList articlesToRemove);

        ArticleList DeletedArticles { get; }
        ArticleList RemovedArticles { get; }

        bool ArticlesModified { get; set; }

        void AddLineGroup(ILineGroup lineGroup);
        void InsertLineGroup(ILineGroup lineGroup, int index);
        void RemoveLineGroup(ILineGroup lineGroup);
        void ClearLineGroups();
        ILineGroup BuildLineGroup(ArticleList articleList);
        ILineGroup BuildTemporaryLineGroup(ArticleList articleList);

        OrderTypeEnum OrderType { get; set; }
        BasketType BasketType { get; set; }
    }
}
