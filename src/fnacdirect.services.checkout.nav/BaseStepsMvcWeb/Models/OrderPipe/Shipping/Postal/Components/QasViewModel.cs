﻿using System.Collections.Generic;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.OnePage.Address
{
    public class QasViewModel
    {
        private readonly List<AddressViewModel> _addresses =new List<AddressViewModel>();        

        public IReadOnlyCollection<AddressViewModel> Addresses
        {
            get
            {
                return _addresses.AsReadOnly();
            }
        }

        public void AddAddresses(IEnumerable<AddressViewModel> addressViewModels)
        {
            _addresses.AddRange(addressViewModels);
        }

        public void AddAddress(AddressViewModel addressViewModel)
        {
            _addresses.Add(addressViewModel);
        }
    }
}
