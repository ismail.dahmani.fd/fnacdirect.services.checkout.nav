using FnacDirect.Contracts.Online.IModel;
using FnacDirect.Contracts.Online.Model;
using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.StoreAvailability.Business;
using FnacDirect.StoreAvailability.Model;
using FnacDirect.Technical.Framework.Web.Mvc;
using System.Linq;

namespace FnacDirect.OrderPipe.Base.Business
{
    public class ArticleDetailsShopService : IArticleDetailsShopService
    {
        private readonly ISTOMBusiness _sTOMBusiness;
        private readonly IApplicationContext _applicationContext;
        private readonly IArticleBusiness3 _articleBusiness3;
        private readonly ISiteManagerBusiness2 _siteManagerBusiness2;

        public ArticleDetailsShopService(ISTOMBusiness sTOMBusiness, IApplicationContext applicationContext, IArticleBusiness3 articleBusiness3, ISiteManagerBusiness2 siteManagerBusiness2)
        {
            _sTOMBusiness = sTOMBusiness;
            _applicationContext = applicationContext;
            _articleBusiness3 = articleBusiness3;
            _siteManagerBusiness2 = siteManagerBusiness2;
        }

        public void GetArticleCodeSegment(StandardArticle prmoArticle)
        {
            if (prmoArticle.ProductID.HasValue)
            {
                var productTree = _articleBusiness3.GetProductTreeByPrid(prmoArticle.ProductID.Value);

                if (productTree != null)
                {
                    var segment = productTree.FindByLevel(ProductTreeLevel.Segment);
                    if (segment != null)
                    {
                        prmoArticle.CodeSegment = segment.SubFamilyNumber;
                    }
                    else
                    {
                        prmoArticle.CodeSegment = string.Empty;
                    }
                }
            }
        }

        public void GetArticleDetailsShopStocks(StandardArticle standardArticle, string refUG)
        {
            var context = _applicationContext.CreateInstanceOfOPContext(true);
            ArticleReference articleRef = new ArticleReference(); 
            var article = _siteManagerBusiness2.GetArticle(context, ArticleReference.FromPrid(standardArticle.ProductID.Value), ArticleLoadingOption.DefaultAndInactive, ArticleLoadingLevel.Default, out articleRef);

            var salesInfo = article.SalesInfo;
            
            var allStocks = _sTOMBusiness.GetStockInfoBySKU(standardArticle.ReferenceGU, refUG);

            StockSTOM stock = null;

            if (allStocks != null && allStocks.Any())
            {
                stock = allStocks.FirstOrDefault();
            }

            if (stock != null && standardArticle.IsBook)
            {
                stock.PrixVenteMag = GetPrice(salesInfo, PriceType.Standard);
            }

            if (stock != null && salesInfo != null && GetUserPrice(salesInfo) != 0)
                stock.PrixVenteAdh = GetUserPrice(salesInfo);

            standardArticle.ShopStocks = stock;

            foreach (var service in standardArticle.Services)
            {
                var srv = _siteManagerBusiness2.GetArticle(context, ArticleReference.FromPrid(service.ProductID.Value), ArticleLoadingOption.DefaultAndInactive, ArticleLoadingLevel.Default, out articleRef);

                var allStocksService = _sTOMBusiness.GetStockInfoBySKU(service.ReferenceGU, refUG);
                StockSTOM stockService = null;

                if (allStocksService != null && allStocksService.Any())
                {
                    stockService = allStocksService.FirstOrDefault();
                }

                if (stockService != null && srv.SalesInfo != null && GetUserPrice(srv.SalesInfo) != 0)
                    stockService.PrixVenteAdh = GetUserPrice(srv.SalesInfo);

                if (stockService != null)
                {
                    standardArticle.Services.First(x => x.ReferenceGU == service.ReferenceGU).ShopStocks = stockService;
                }
            }
        }

        private decimal GetPrice(SalesInfo salesInfo, PriceType priceType)
        {
            if(salesInfo == null
                || salesInfo.Prices == null
                || !salesInfo.Prices.Contains(priceType))
            {
                return 0;
            }
            else
            {
                return salesInfo.Prices[priceType].TTC;
            }
        }

        private decimal GetUserPrice(SalesInfo salesInfo)
        {
            var result = GetPrice(salesInfo, PriceType.Standard);

            var reducedPrice = GetPrice(salesInfo, PriceType.Reduced);
            if (reducedPrice != 0 && reducedPrice < result)
            {
                result = reducedPrice;
            }

            var memberPrice = GetPrice(salesInfo, PriceType.Member);
            if (memberPrice != 0 && memberPrice < result)
            {
                result = memberPrice;
            }

            return result;
        }
    }
}
