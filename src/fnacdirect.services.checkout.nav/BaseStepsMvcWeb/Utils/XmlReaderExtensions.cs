﻿using System.Xml;

namespace FnacDirect.OrderPipe
{
    public static class XmlReaderExtensions
    {
        public static void ReadToNextElementOrEndElement(this XmlReader xmlReader)
        {
            xmlReader.Read();
            while (xmlReader.NodeType != XmlNodeType.Element && xmlReader.NodeType != XmlNodeType.EndElement)
            {
                xmlReader.Read();
            }
        }
    }
}
