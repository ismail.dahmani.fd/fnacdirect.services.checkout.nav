using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.OrderPipe.BaseMvc.Web.Controllers.Pop;
using FnacDirect.OrderPipe.CoreMvcWeb.Http;
using System.Web.Mvc;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Pop.Controllers.SelfCheckout
{
    /// <summary>
    /// Ce controller doit être laisser tel quel.
    /// Il sert de point d'arret pour bloquer le pipe selfcheckout avant le paiement
    /// </summary>
    public class SelfCheckoutController : PopBaseController
    {
        private readonly OPContext _opContext;

        public SelfCheckoutController(OPContext opContext)
        {
            _opContext = opContext;
        }

        public ActionResult Index()
        {
            var errorData = _opContext.OF.GetPrivateData<ErrorData>(create: false);
            if(errorData != null)
            {
                return View(errorData.ErrorCode);
            }
            return Content(string.Empty);
        }
    }
}
