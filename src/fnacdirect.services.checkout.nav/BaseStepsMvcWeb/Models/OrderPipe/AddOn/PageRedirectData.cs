using FnacDirect.Technical.Framework.CoreServices;
using System;

namespace FnacDirect.OrderPipe.Base.Model.AddOn
{
    [Serializable]
    public class PageRedirectData : GroupData
    {
        public string Url { get; set; }
        public PageRedirectType Type { get; set; }
        public SerializableDictionary<string,string> Parameters { get; set; }

        public PageRedirectData()
        {
            Url = string.Empty;
            Parameters = new SerializableDictionary<string, string>();
        }

        public void Reset()
        {
            var newInstance = new PageRedirectData();

            Url = newInstance.Url;
            Type = newInstance.Type;
            Parameters = newInstance.Parameters;
        }
    }
}
