using FnacDirect.Customer.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FnacDirect.OrderPipe.Base.Model
{
    public class AddressElegibility
    {
        public AddressEntity AddressEntity { get; set; }

        public bool IsElegible { get; set; }

        public bool HasMissingInformations { get; set; } = false;

        public bool IsValid => IsElegible && !HasMissingInformations;

        public IAddressViewModelFieldsValidation Convert()
        {
            IAddressViewModelFieldsValidation address = new AddressViewModelFieldsValidationDto(AddressEntity);

            return address;
        }
    }
}

