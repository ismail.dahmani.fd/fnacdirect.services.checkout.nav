using System;
using System.Linq;
using FnacDirect.OrderPipe.CoreRouting;
using FnacDirect.OrderPipe.Config;
using System.Web;
using System.Web.Routing;
using System.Web.Mvc;

namespace FnacDirect.OrderPipe.CoreMvcWeb.Routing
{
    public class MvcOrderPipeStepRedirectionStrategy : OrderPipeStepRedirectionStrategyBase<MvcUiDescriptor>
    {
        public override string DoRedirectionFor(MvcUiDescriptor descriptor, OPContext opContext)
        {
            var routeValueDictionary = new RouteValueDictionary();

            if (descriptor.RouteValueProvider != null)
            {
                routeValueDictionary = descriptor.RouteValueProvider.GetRouteValueFor(opContext.OF);
            }

            var queryStringToPreserve = HttpContext.Current.Request.QueryString.AllKeys
                .Where(k => k != null
                    && k != "step"
                    && k != "reset"
                    && k != "context")
                .ToList();

            if (descriptor.QueryStringMode == QueryStringMode.Whitelist.ToString())
            {
                var whitelist = descriptor.QueryStringsToPreserve();

                queryStringToPreserve.RemoveAll(k => !whitelist.Contains(k.ToLowerInvariant()));
            }

            foreach (var key in queryStringToPreserve)
            {
                if (!routeValueDictionary.ContainsKey(key))
                {
                    routeValueDictionary.Add(key, HttpContext.Current.Request.QueryString[key]);
                }
            }

            if (descriptor.RedirectMode == RedirectMode.Redirect)
            {
                var urlHelper = new UrlHelper(HttpContext.Current.Request.RequestContext);

                var redirection = urlHelper.RouteUrl(descriptor.Route, routeValueDictionary);

                if (redirection != null)
                {
                    HttpContext.Current.Response.Redirect(redirection, true);

                    return redirection;
                }
            }

            throw new NotImplementedException();
        }
    }
}
