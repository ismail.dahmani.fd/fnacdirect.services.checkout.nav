﻿using FnacDirect.OrderPipe.Config;

namespace FnacDirect.OrderPipe.CoreRouting
{
    public interface IOrderPipeStepRedirectionStrategyLocator
    {
        IOrderPipeStepRedirectionStrategy GetOrderPipeStepRedirectionStrategyFor<TDescriptor>(TDescriptor descriptor) 
            where TDescriptor : IUiDescriptor;
    }
}
