using FnacDirect.OrderPipe.Base.BaseModel;
using FnacDirect.OrderPipe.Base.LineGroups;
using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.OrderPipe.Base.Model.LineGroups;
using System.Collections.Generic;

namespace FnacDirect.OrderPipe.Base.Business.OrderDetail
{
    public interface IOrderDetailService
    {
        BaseAddress ComputeShippingAddress(ShippingMethodEvaluation shippingMethodEvaluation);
        BaseAddress ComputeShippingAddressForSeller(ShippingMethodEvaluation shippingMethodEvaluation, ILogisticLineGroup logisticLineGroup, int sellerId);
        bool HasManyShippingAddress(ShippingMethodEvaluation shippingMethodEvaluation);
        string FormatAddress(string addressLine1, string addressLine2, string addressLine3, string addressLine4);

        TheoreticalDeliveryDateList ComputeTheoreticalDeliveryDate(PopOrderForm popOrderForm);
        TheoreticalDeliveryDateList ComputeTheoreticalDeliveryDate(IEnumerable<ILogisticLineGroup> logisticLineGroups, ShippingMethodEvaluation shippingMethodEvaluation);

        IEnumerable<Article> BuildArticles(IEnumerable<Model.Article> articles, TheoreticalDeliveryDateList theoreticalDeliveryDates);
        Cgv BuildCgv(PopOrderForm popOrderForm);

        string GeneratePictureUrl(Model.BaseArticle article);
        string GeneratePictureUrl(string pictureUrl);
    }
}
