using System.Linq;
using System.Web;
using System.Web.Http.Routing;
using System.Web.Routing;

namespace FnacDirect.OrderPipe
{
    public static class RouteCollectionExtensions
    {
        public static string GetPipeName(this RouteCollection routeTable)
        {
            if(HttpContext.Current.Items["pipe"] != null)
            {
                return HttpContext.Current.Items["pipe"].ToString();
            }

            var routeDatas = RouteTable.Routes.GetRouteData(new HttpContextWrapper(HttpContext.Current));

            var pipeName = string.Empty;

            if (string.IsNullOrEmpty(pipeName))
                pipeName = HttpContext.Current.Request.QueryString["pipe"];

            if (routeDatas != null)
            {
                var httpRouteDatas = routeDatas.Values.Values.OfType<IHttpRouteData[]>().SelectMany(d => d);

                foreach (var httpRouteData in httpRouteDatas.Where(httpRouteData => httpRouteData.Values.ContainsKey("pipe")))
                {
                    pipeName = httpRouteData.Values["pipe"].ToStringOrDefault();
                }
            }

            if (string.IsNullOrEmpty(pipeName) && routeDatas != null)
                pipeName = routeDatas.Values["pipe"].ToStringOrDefault();

            if (string.IsNullOrEmpty(pipeName))
                pipeName = "default";

            return pipeName.ToLowerInvariant();
        }
    }
}
