namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Basket
{
    public class TermsAndConditionsViewModel
    {
        public string TermsAndConditionsLabel { get; set; }
        public string TermsAndConditionsUrl  { get; set; }
    }
    
}
