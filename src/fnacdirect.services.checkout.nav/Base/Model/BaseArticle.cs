using FnacDirect.Contracts.Online.Model;
using FnacDirect.OrderPipe.Shipping.Model;
using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;

namespace FnacDirect.OrderPipe.Base.Model
{
    public abstract class BaseArticle : Article
    {
        public BaseArticle()
        {
            DeliveryDates = new List<ArticleDeliveryDate>();
        }

        // TO DO : Refacto de l'object standardarticle pour encapsuler certains champs (prix, wrap, eco-tax...)
        [XmlIgnore]
        public string Title { get; set; }

        [XmlIgnore]
        public string Editor { get; set; }

        [XmlIgnore]
        public string MarqueEditor { get; set; }

        [XmlIgnore]
        public string Label { get; set; }

        [XmlIgnore]
        public string FacturetteLabel
        {
            get
            {
                var aOctets = Encoding.GetEncoding(1251).GetBytes(Label);
                var sEnleverAccents = Encoding.ASCII.GetString(aOctets);
                return sEnleverAccents;
            }
        }

        private PromotionList _promotions;

        [XmlIgnore]
        public PromotionList Promotions
        {
            get
            {
                return _promotions ?? (_promotions = new PromotionList());
            }
            set
            {
                _promotions = value;
            }
        }

        [XmlIgnore]
        public string Participant1_id { get; set; }

        [XmlIgnore]
        public string Language { get; set; }

        [XmlIgnore]
        public string Content { get; set; }

        [XmlIgnore]
        public string CopyControl { get; set; }

        [XmlIgnore]
        public string LittleScan { get; set; }

        [XmlIgnore]
        public string SerieTitle { get; set; }

        [XmlIgnore]
        public string Participant1 { get; set; }

        [XmlIgnore]
        public string Participant2 { get; set; }

        [XmlIgnore]
        public string ScanURL { get; set; }

        [XmlIgnore]
        public string ErgoBasketPictureURL { get; set; }
        [XmlIgnore]
        public string Picture_Principal_340x340 { get; set; }

        [XmlIgnore]
        public string FAURL { get; set; }

        [XmlIgnore]
        public int? Format { get; set; }

        [XmlIgnore]
        public string FormatLabel { get; set; }

        [XmlIgnore]
        public string Ean13 { get; set; }

        [XmlIgnore]
        public virtual bool IsBook
        {
            get
            {
                return (ArticleGroup)Family.GetValueOrDefault(0) == ArticleGroup.Book;
            }
        }

        public virtual bool IsEbook
        {
            get
            {
                return false;
            }
        }

        [XmlIgnore]
        public virtual bool HasD3E
        {
            get
            {
                return D3ETax.HasValue;
            }
        }

        [XmlIgnore]
        public virtual bool HasRCP
        {
            get
            {
                return RCPTax.HasValue;
            }
        }

        [XmlIgnore]
        public virtual decimal? D3ETax { get; set; }

        [XmlIgnore]
        public virtual decimal? DEATax { get; set; }

        [XmlIgnore]
        public virtual decimal? RCPTax { get; set; }

        [XmlIgnore]
        public virtual bool IsSoftware
        {
            get
            {
                return (ArticleGroup)Family.GetValueOrDefault() == ArticleGroup.Software;
            }
        }

        [XmlIgnore]
        public virtual bool IsPE
        {
            get
            {
                var group = (ArticleGroup)Family.GetValueOrDefault(0);

                return (group == ArticleGroup.Book
                    || group == ArticleGroup.Video
                    || group == ArticleGroup.Audio
                    || group == ArticleGroup.Software);
            }
        }

        [XmlIgnore]
        public virtual bool IsPT
        {
            get
            {
                return !IsPE;
            }
        }

        [XmlIgnore]
        public decimal TotalDbEur { get { return PriceDBEur.GetValueOrDefault() * Quantity.GetValueOrDefault(); } }

        [XmlIgnore]
        public decimal? PriceDBEur { get; set; }

        [XmlIgnore]
        public decimal PriceDBEurNoVAT { get; set; }

        [XmlIgnore]
        public decimal TotalUserEur { get { return PriceUserEur.GetValueOrDefault() * Quantity.GetValueOrDefault(); } }

        [XmlIgnore]
        public decimal? PriceUserEur { get; set; }

        [XmlIgnore]
        public decimal PriceUserEurNoVAT { get; set; }

        [XmlIgnore]
        public decimal? PriceRealEur { get; set; }

        [XmlIgnore]
        public decimal PriceRealEurNoVAT { get; set; }

        [XmlIgnore]
        public decimal? PriceNoVATEur { get; set; }

        [XmlIgnore]
        public virtual decimal? PriceMemberEur { get; set; }

        [XmlIgnore]
        public decimal? VATRate { get; set; }

        [XmlIgnore]
        public int? VATId { get; set; }

        [XmlIgnore]
        public string VATCodeGu { get; set; }

        public int? ShipMethod { get; set; }

        [XmlIgnore]
        public string ShipMethodLabel { get; set; }

        [XmlIgnore]
        public int? ShipDelay { get; set; }

        [XmlIgnore]
        public decimal? ShipPriceDBEur { get; set; }

        public ShippingMethodChoiceItemDetail ShippingMethodDetail { get; set; }

        [XmlIgnore]
        public decimal PreviewShippingCost { get; set; }

        [XmlIgnore]
        public decimal? ShipPriceUserEur { get; set; }

        [XmlIgnore]
        public decimal? ShipPriceNoVATEur { get; set; }

        [XmlIgnore]
        public decimal? ShipPriceForNoLocalShippmentEur { get; set; }

        [XmlIgnore]
        public List<ShippingPromotionRule> ShippingPromotionRules { get; } = new List<ShippingPromotionRule>();

        [XmlIgnore]
        public SalesInfo SalesInfo { get; set; }

        [XmlIgnore]
        public int? BillMethod { get; set; }

        public bool IsPreOrder
        {
            get
            {
                return AvailabilityId == (int)AvailabilityEnum.PreOrder
                    || AvailabilityId == (int)AvailabilityEnum.PreOrderBis
                    || AvailabilityId == (int)AvailabilityEnum.PreOrderRestockingPE
                    || AvailabilityId == (int)AvailabilityEnum.PreOrderRestockingPT;
            }
        }

        [XmlIgnore]
        public int? AvailabilityId { get; set; }

        [XmlIgnore]
        public bool IsAvailable { get; set; }
        [XmlIgnore]
        public string AvailabilityLabel { get; set; }

        [XmlIgnore]
        public DateTime AvailabilityDelay { get; set; }

        /// <summary>
        /// Ancien prix de l'article 
        /// <remarks>Utilisé pour la notification de changement de prix</remarks>
        /// </summary>
        public decimal? OldPrice { get; set; }

        /// <summary>
        /// Date à laquelle a été sauvegardé l'ancien prix de l'article 
        /// <remarks>Utilisé pour la notification de changement de prix</remarks>
        /// </summary>
        public DateTime? OldPriceRecordedDate { get; set; }

        [XmlIgnore]
        public int? VatCountryId { get; set; }

        public string ShippingCartURL { get; set; }

        [XmlIgnore]
        public bool UseVat { get; set; }

        public string OfferRef { get; set; }

        /// <summary>
        /// Conditionne l'affichage de la disponibilité de l'article dans le pipe
        /// </summary>
        [XmlIgnore]
        public bool ShowAvailibility { get; set; }

        /// <summary>
        /// Conditionne la possibilité de mettre à jour la quantité dans le pipe
        /// </summary>
        [XmlIgnore]
        public bool EnableUpdateQuantity { get; set; }


        [XmlIgnore]
        public List<ArticleDeliveryDate> DeliveryDates { get; }

        [XmlIgnore]
        public DateTime? TheoreticalExpeditionDate { get; set; }

        [XmlIgnore]
        public DateTime? TheoreticalDeliveryDate { get; set; }

        private DateTime? _theoreticalDeliveryDateMax;

        [XmlIgnore]
        public DateTime? TheoreticalDeliveryDateMax
        {
            get => _theoreticalDeliveryDateMax ?? TheoreticalDeliveryDate;
            set => _theoreticalDeliveryDateMax = value;
        }

        [XmlIgnore]
        public int? ArticleDelay { get; set; }

        [XmlIgnore]
        public string PriceExternalRef { get; set; }
    }
}
