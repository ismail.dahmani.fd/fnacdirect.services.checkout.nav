using FnacDirect.OrderPipe.Base.Business;
using FnacDirect.OrderPipe.Base.Business.Shipping.Postal.Appointment.Fly;
using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.OrderPipe.BaseMvc.Web.Services.Article.Pipe;
using FnacDirect.Technical.Framework.Web.Mvc;
using System.Collections.Generic;
using System.Linq;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Orderpipe.Common.Shipping.Network.Postal.Appointment.Fly
{
    public class FlyViewModelBuilder : IFlyViewModelBuilder
    {
        private readonly IFlyBusiness _flyBusiness;
        private readonly IPipeArticleService _pipeArticleService;
        private readonly IArticleService _articleService;
        private readonly IPriceHelper _priceHelper;
        private readonly IApplicationContext _applicationContext;

        private const int DefaultFlyServicePrid = -1;

        public FlyViewModelBuilder(
            IFlyBusiness flyBusiness,
            IPipeArticleService pipeArticleService,
            IArticleService articleService,
            IPriceHelper priceHelper,
            IApplicationContext applicationContext)
        {
            _flyBusiness = flyBusiness;
            _pipeArticleService = pipeArticleService;
            _articleService = articleService;
            _priceHelper = priceHelper;
            _applicationContext = applicationContext;
        }

        public FlyViewModel Build(PopOrderForm popOrderForm)
        {
            var flyForm = _flyBusiness.GetFlyOrder(popOrderForm);

            var flyViewModel = new FlyViewModel
            {
                Articles = BuildArticles(popOrderForm).ToArray()
            };

            if(flyForm != null)
            {
                flyViewModel.FlyForm = new FlyFormViewModel()
                {
                    ShippingMethodId = flyForm.ShippingMethodId,
                    AccessCode = flyForm.AccessCode,
                    CalendarSlotIdentifier = flyForm.CalendarSlotIdentifier,
                    Commentary = flyForm.Commentary,
                    FloorNumber = flyForm.FloorNumber,
                    HasElevatorAccess = flyForm.HasElevatorAccess,
                    HasStaircaseStep = flyForm.HasStaircaseStep,
                    PhoneNumber = flyForm.PhoneNumber,
                    ProductQuantityToTakeBack = flyForm.ProductQuantityToTakeBack,
                    SelectedFlyServices = flyForm.SelectedFlyServices,
                    StaircaseType = flyForm.StaircaseType,
                };
            }

            return flyViewModel;
        }

        private IEnumerable<FlyArticleViewModel> BuildArticles(PopOrderForm popOrderForm)
        {
            var viewModelContainer = new List<FlyArticleViewModel>();

            var articles = _flyBusiness.GetEligibleArticlesForFly(popOrderForm, _flyBusiness.SellerId);
            if (articles == null)
            {
                return new FlyArticleViewModel[0];
            }

            foreach (var article in articles)
            {
                var viewModel = Build(article);
                if (viewModel != null)
                {
                    viewModelContainer.Add(viewModel);
                }
            }

            return viewModelContainer;
        }


        #region privates

        private FlyArticleViewModel Build(Article article)
        {
            if (!article.ProductID.HasValue)
            {
                return null;
            }

            var baseArticle = _pipeArticleService.GetArticle(article.ProductID.Value);
            var flyArticleViewModel = new FlyArticleViewModel()
            {
                Prid = article.ProductID.Value,
                Title = baseArticle.Title,
                Services = GetFlyServices(baseArticle)
            };

            return flyArticleViewModel;
        }

        private FlyServiceViewModel[] GetFlyServices(BaseArticle baseArticle)
        {
            if (!(baseArticle is StandardArticle standardArticle))
            {
                return Enumerable.Empty<FlyServiceViewModel>().ToArray();
            }

            var serviceViewModels = new List<FlyServiceViewModel>();
            var services = _pipeArticleService.GetServices(standardArticle);
            var flyServices = _articleService.GetFilteredArticles(services, filter: ArticleFilter.FLY);
            
            foreach (var service in flyServices)
            {
                var price = service.SalesInfo.ReducedPrice.TTC;
                var serviceViewModel = new FlyServiceViewModel()
                {
                    Prid = service.Reference.PRID.GetValueOrDefault(),
                    Title = service.Core.Title,
                    SubTitle = service.Core.SubTitle1,
                    PriceDetails = _priceHelper.GetDetails(price)
                };
                serviceViewModels.Add(serviceViewModel);
            }

            serviceViewModels.Add(AddDefaultFlyService());

            return serviceViewModels.ToArray();
        }

        private FlyServiceViewModel AddDefaultFlyService()
        {
            return new FlyServiceViewModel()
            {
                Prid = DefaultFlyServicePrid,
                Title = _applicationContext.GetMessage($"orderpipe.pop.fly.service.default.title"),
                PriceDetails = _priceHelper.GetDetails(0)
            };
        }

        #endregion
    }
}
