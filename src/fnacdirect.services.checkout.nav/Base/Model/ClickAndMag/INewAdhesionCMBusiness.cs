using FnacDirect.Contracts.Online.Model;
using FnacDirect.OrderPipe.Base.Model;
using System;
using System.Collections.Generic;

namespace FnacDirect.OrderPipe.Base.Business
{
    public interface INewAdhesionCMBusiness
    {
        bool IsLowerThanAdherenAgeMin(DateTime pBirthDate);
        string GenerateLogin(Seller vendor);
        string GenerateMagasin(FnacStore.Model.FnacStore store);
        bool IsValidCardNumber(string cardNumber);
        void RemoveCards(ArticleList articles, string culture);
        IDictionary<int, string> NewAdhesionCards(string culture, DateTime pBirthDate);
        bool IsContainsNewAdhesion(IEnumerable<Model.Article> articles);
        bool IsContainsAdhesion(IEnumerable<Model.Article> articles);
        bool IsContainsAdhesion3Years(IEnumerable<Model.Article> articles);
        bool IsNewAdhesion(int articleId);
        bool IsAdh3Years(int articleId);
        bool IsContainsNewFnacPlus(IEnumerable<Model.Article> articles);
        IDictionary<int, string> GetAdhesionCards();        
    }
}
