using System;
using FnacDirect.Technical.Framework.CoreServices;
using System.Runtime.Serialization;
using System.Globalization;
using System.Security.Permissions;

namespace FnacDirect.OrderPipe
{

    public enum OPRange : ushort
    {
        None = 0,
        Config = 56000,
        Transaction = 56050,
        Run = 56100
    }

    [Serializable]
    public class OPException : Exception
    {
        ushort code;

        private OPException(string message, Exception e, ushort code)
            : base(message, e)
        {
            this.code = code;
        }

        public OPException(string message)
            : base(message)
        {
        }

        public OPException(string message, Exception e)
            : base(message, e)
        {
        }

        public OPException()
            : base()
        {
        }

        protected OPException(SerializationInfo si, StreamingContext sc) : base(si, sc)
        {
        }

        public void Log()
        {
            Logging.Current.WriteError(this, code);
        }

        public static OPException BuildException(string msg, OPRange range, ushort subcode, params object[] prm)
        {
            return BuildException(msg, null, null, range, subcode, prm);
        }

        public static OPException BuildException(string msg, Exception ex, OPRange range, ushort subcode, params object[] prm)
        {
            return BuildException(msg, null, ex, range, subcode, prm);
        }

        public static OPException BuildException(string msg, OPContext context, Exception ex, OPRange range, ushort subcode, params object[] prm)
        {
            var code = (ushort)(range + subcode);
            if (ex==null && context != null && context.ErrorInfo != null)
                ex = context.ErrorInfo.Exception;
            var d = new OPException(
                string.Format(CultureInfo.InvariantCulture,
                    "[{0:00000}] {1}{2}", 
                    code,
                    msg, 
                    ex == null ? "" : " (" + ex.Message + ")"
                ), ex, code);
            d.Data["MESSAGE"] = msg;
            d.Data["CODE"] = code;
            if (context != null)
            {
                if (context.Pipe!=null)
                    d.Data["PIPE"] = context.Pipe.PipeName;
                if (context.CurrentStep!=null)
                    d.Data["STEP"] = context.CurrentStep.Name;
                d.Data["SID"] = context.SID;
                d.Data["UID"] = context.UID;
                if (context.ErrorInfo != null)
                {
                    if (context.ErrorInfo.FailedStep != null)
                        d.Data["ERROR.STEP"] = context.ErrorInfo.FailedStep.Name;
                    if (context.ErrorInfo.Error != null)
                    {
                        d.Data["ERROR.CODE"] = context.ErrorInfo.Error.Code;
                        d.Data["ERROR.DESCRIPTION"] = context.ErrorInfo.Error.Description;
                        d.Data["ERROR.TYPE"] = context.ErrorInfo.Error.Type;
                    }
                }
            }
            if (prm != null)
                for (var i = 0; i < prm.Length / 2; i++)
                    d.AddComplementaryData(prm[i], prm[i + 1]);
            d.Log();
            return d;
        }

        [SecurityPermission(SecurityAction.Demand, SerializationFormatter = true)]
        public override void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            base.GetObjectData(info, context);
        }

        public void AddComplementaryData(object key, object value)
        {
            Data["DATA." + key] = value;
        }
    }

}
