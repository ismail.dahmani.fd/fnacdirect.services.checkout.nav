using FnacDirect.Contracts.Online.Model;
using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.OrderPipe.Base.Model.Facets;
using FnacDirect.VirtualGiftCheck.Model;

namespace FnacDirect.OrderPipe.Base.Business
{
    public interface IVoucherBusiness
    {
        void CheckExistingVouchers(MultiFacet<IGotBillingInformations, IGotLineGroups, IGotUserContextInformations> multiFacet, SiteContext context);
        VoucherBusiness.CheckCode CheckVoucher(string code, int shop, IGotUserContextInformations userContext, IGotLineGroups lineGroups, IGotBillingInformations igGotBillingInformations, out VoucherInfo info, SiteContext context);
        void RemoveVoucher(IGotBillingInformations orderForm, string code);
        AddVoucherStatus AddVoucher(OPContext context, IGotLineGroups igGotLineGroups, IGotBillingInformations billingInformations, IGotUserContextInformations userContext, IGotContextInformations contextInformations, string code);
    }
}
