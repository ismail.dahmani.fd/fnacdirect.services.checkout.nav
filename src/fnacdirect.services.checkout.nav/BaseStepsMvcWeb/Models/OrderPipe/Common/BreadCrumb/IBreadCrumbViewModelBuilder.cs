﻿using FnacDirect.OrderPipe.BaseMvc.Web.Models.Common;
using System;
using System.Web.Routing;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Pop.Models.Common
{
    public interface IBreadCrumbViewModelBuilder
    {
        BreadCrumbViewModel Build(OPContext opContext, Func<string, RouteValueDictionary, string> getUrl);
    }
}
