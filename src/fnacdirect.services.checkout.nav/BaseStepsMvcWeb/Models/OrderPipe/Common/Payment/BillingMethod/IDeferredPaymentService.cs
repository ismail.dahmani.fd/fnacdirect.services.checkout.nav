using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.OrderPipe.Base.Model.Facets;

namespace FnacDirect.OrderPipe.Base.Business.Payment
{
    public interface IDeferredPaymentService
    {
        DeferredPaymentEligibility GetDeferredPaymentEligibility(int accountId, decimal orderTotalPrice);
        bool IsAllowed(PopOrderForm popOrderForm, IGotBillingInformations iGotBillingInformations, IGotUserContextInformations iGotUserContextInformations);
    }
}
