using FnacDirect.OrderPipe.Base.BaseModel;
using FnacDirect.OrderPipe.Base.Business.Configuration;
using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.OrderPipe.Base.Model.Solex;
using FnacDirect.Solex.AppointmentDelivery.Client;
using FnacDirect.Solex.AppointmentDelivery.Client.Contract;
using FnacDirect.Technical.Framework.CoreServices;
using FnacDirect.Technical.Framework.Web.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using DeliverySlot = FnacDirect.Solex.AppointmentDelivery.Client.Contract.DeliverySlot;

namespace FnacDirect.OrderPipe.Base.Business.Shipping
{
    public class AppointmentDeliveryCalendarBusiness : IAppointmentDeliveryCalendarBusiness
    {
        private readonly ISolexConfigurationProvider _solexConfigurationProvider;
        private readonly IAppointmentDeliveryServiceClient _appointmentDeliveryServiceClient;
        private readonly ICountryService _countryService;
        private readonly IAppointmentDeliveryCalendarService _appointmentDeliveryCalendarService;
        private readonly IAppointmentDeliveryService _appointmentDeliveryService;

        public AppointmentDeliveryCalendarBusiness(ISolexConfigurationProvider solexConfigurationProvider,
            IAppointmentDeliveryServiceClient appointmentDeliveryServiceClient,
            ICountryService countryService,
            IAppointmentDeliveryCalendarService appointmentDeliveryCalendarService,
            IAppointmentDeliveryService appointmentDeliveryService)
        {
            _solexConfigurationProvider = solexConfigurationProvider;
            _appointmentDeliveryServiceClient = appointmentDeliveryServiceClient;
            _countryService = countryService;
            _appointmentDeliveryCalendarService = appointmentDeliveryCalendarService;
            _appointmentDeliveryService = appointmentDeliveryService;
        }
        public CalendarQuery CreateGetCalendarQuery(DeliveryAddress deliveryAddress, List<Item> items, DateTime startDate, DateTime endDate, IEnumerable<Solex.AppointmentDelivery.Client.Contract.Promotion> promotions, int shippingMethodId)
        {
            var solexConfiguration = _solexConfigurationProvider.Get();

            return new CalendarQuery
            {
                Context = new Solex.AppointmentDelivery.Client.Contract.Context
                {
                    Channel = solexConfiguration.Channel,
                    Market = solexConfiguration.Market
                },
                DeliveryAddress = deliveryAddress,
                Items = items,
                StartDate = startDate,
                EndDate = endDate,
                ShippingMethodId = shippingMethodId,
                Promotions = promotions
            };
        }
        public CalendarResult GetCalendarResult(CalendarQuery query)
        {
            return _appointmentDeliveryServiceClient.GetCalendar(query);
        }

        public CalendarResult GetCalendarResult(DeliveryAddress deliveryAddress, List<Item> items, DateTime startDateReference, DateTime endDateMaximum, int pageSize, int pageIndex, IEnumerable<Solex.AppointmentDelivery.Client.Contract.Promotion> promotions, int shippingMethodId)
        {
            var daysIndexOffset = (double)((pageIndex - 1) * pageSize);
            var startDate = (startDateReference).AddDays(daysIndexOffset);
            var endDate = startDate.AddDays(pageSize);

            if (endDate.Date > endDateMaximum.Date)
            {
                endDate = endDateMaximum;
                startDate = endDateMaximum.AddDays(-pageSize);
            }

            var calendarQuery = CreateGetCalendarQuery(deliveryAddress, items, startDate, endDate, promotions, shippingMethodId);

            var calendarResult = _appointmentDeliveryServiceClient.GetCalendar(calendarQuery);

            // Happens when there is an exception on Solex side, for example on ValidateShippingMethodId with shipping method id = -1
            if (calendarResult.Success == null)
            {
                return null;
            }

            if (calendarResult?.Slots.Any() == false)
            {
                calendarResult = SecondCallWithQueryDatesRangeCheck(calendarQuery, startDate, calendarResult.MinStartDate, endDate, calendarResult.MaxEndDate, pageSize, endDateMaximum, promotions, shippingMethodId);
            }

            return calendarResult;
        }

        private CalendarResult SecondCallWithQueryDatesRangeCheck(CalendarQuery calendarQuery, DateTime startDate, DateTime minStartDate, DateTime endDate, DateTime maxEndDate, int pageSize, DateTime endDateMaximum, IEnumerable<Solex.AppointmentDelivery.Client.Contract.Promotion> promotions, int shippingMethodId)
        {
            if (startDate < minStartDate)
            {
                startDate = minStartDate;
                endDate = startDate.AddDays(pageSize) < endDateMaximum ? startDate.AddDays(pageSize)
                                                                       : endDateMaximum;
            }

            if (endDate > maxEndDate)
            {
                endDate = maxEndDate;
                startDate = endDate.AddDays(-pageSize) < minStartDate ? minStartDate
                                                                      : endDate.AddDays(-pageSize);
            }

            calendarQuery = CreateGetCalendarQuery(calendarQuery.DeliveryAddress, calendarQuery.Items.ToList(), startDate, endDate, promotions, shippingMethodId);

            return _appointmentDeliveryServiceClient.GetCalendar(calendarQuery);
        }

        public List<Model.Solex.DeliverySlot>[] GetSortingSlotsTabs(IEnumerable<ShippingChoice> choices, DeliverySlotData deliverySlotData, IEnumerable<Solex.AppointmentDelivery.Client.Contract.DeliverySlot> resultSlots, int pageSize, string sortingCriteria)
        {
            choices = choices ?? Enumerable.Empty<ShippingChoice>();
            var slots = new List<Model.Solex.DeliverySlot>();
            var pricePerMethodId = new Dictionary<int, decimal>();

            foreach (var methodId in _appointmentDeliveryService.ShippingMethods)
            {
                var foundChoice = choices.FirstOrDefault(choice => choice.ContainsShippingMethodId(methodId));
                if (foundChoice != null)
                {
                    var methodChoice = foundChoice.GetChoice(methodId);
                    var cost = methodChoice?.TotalPrice?.CostToApply ?? methodChoice?.TotalPrice?.Cost;
                    if (cost != null)
                    {
                        pricePerMethodId.Add(methodId, cost.Value);
                    }
                }
            }

            foreach (var slot in resultSlots)
            {
                slots.Add(new Model.Solex.DeliverySlot
                {
                    StartDate = slot.StartDate,
                    EndDate = slot.EndDate,
                    Price = pricePerMethodId.ContainsKey(slot.ShippingMethodId) ? pricePerMethodId[slot.ShippingMethodId] : slot.Price,
                    SlotType = slot.SlotType,
                    ShippingMethodId = slot.ShippingMethodId,
                    PartnerId = slot.PartnerId,
                    PartnerUniqueIdentifier = slot.PartnerUniqueIdentifier,
                    PartnerInformations = ConvertTo(slot.PartnerInformation),
                    IsAvailable = slot.IsAvailable,
                    IsSelected = false
                });
            }
            deliverySlotData.DeliverySlotChoices = slots;

            if (deliverySlotData.HasSelectedSlot)
            {
                var selectedDeliverySlot = deliverySlotData.DeliverySlotSelected;

                var slot = slots.FirstOrDefault(s => s.StartDate == selectedDeliverySlot.StartDate
                                        && s.EndDate == selectedDeliverySlot.EndDate
                                        && s.SlotType == selectedDeliverySlot.SlotType);
                if (slot != null)
                {
                    slot.IsSelected = selectedDeliverySlot.IsSelected;
                    slot.HasChanged = selectedDeliverySlot.HasChanged;
                    slot.PriceHasChanged = selectedDeliverySlot.PriceHasChanged;
                }
            }

            return SortingSlotListBySortChoice(slots, sortingCriteria, pageSize);
        }

        private List<Model.Solex.DeliverySlot>[] SortingSlotListBySortChoice(List<Model.Solex.DeliverySlot> slots, string sortingCriteria, int pageSize)
        {
            var sortedArrayOfDeliverySlots = new List<Model.Solex.DeliverySlot>[pageSize];
            var slotsFirstStartDate = slots.OrderBy(s => s.StartDate).First().StartDate;
            var slotsFirstEndDate = slots.OrderBy(s => s.StartDate).First().EndDate;
            var slotsLastDate = slots.OrderByDescending(s => s.StartDate).First().StartDate.Date;

            var currentDayCursor = 0;
            var daysAdded = 0;

            switch (sortingCriteria)
            {
                case "D":
                    while (daysAdded < pageSize)
                    {
                        var date = slotsFirstStartDate.Date.AddDays(currentDayCursor).Date;
                        if (date > slotsLastDate)
                        {
                            sortedArrayOfDeliverySlots[daysAdded] = new List<Model.Solex.DeliverySlot>
                            {
                                new Model.Solex.DeliverySlot()
                                {
                                    StartDate = date + slotsFirstStartDate.TimeOfDay,
                                    EndDate = date + slotsFirstEndDate.TimeOfDay,
                                }
                            };
                            daysAdded++;
                        }

                        var slotsAvailableThatDay = slots.Where(s => s.StartDate.Date == date && s.IsAvailable).OrderBy(d => d.StartDate).ToList();

                        if (slotsAvailableThatDay.Any())
                        {
                            sortedArrayOfDeliverySlots[daysAdded] = slotsAvailableThatDay;
                            daysAdded++;
                        }
                        currentDayCursor++;
                    }
                    break;
                case "F":
                    while (daysAdded < pageSize)
                    {
                        var date = slotsFirstStartDate.Date.AddDays(currentDayCursor).Date;
                        if (date > slotsLastDate)
                        {
                            sortedArrayOfDeliverySlots[daysAdded] = new List<Model.Solex.DeliverySlot>
                            {
                                new Model.Solex.DeliverySlot()
                                {
                                    StartDate = date + slotsFirstStartDate.TimeOfDay,
                                    EndDate = date + slotsFirstEndDate.TimeOfDay,
                                }
                            };
                            daysAdded++;
                            continue;
                        }

                        var slotsAvailableThatDay = slots.Where(s => s.StartDate.Date == date && s.IsAvailable).OrderBy(d => d.StartDate).ToList();

                        if (slotsAvailableThatDay.Any())
                        {
                            sortedArrayOfDeliverySlots[daysAdded] = slotsAvailableThatDay;
                            daysAdded++;
                        }
                        else
                        {
                            sortedArrayOfDeliverySlots[daysAdded] = new List<Model.Solex.DeliverySlot>
                            {
                                new Model.Solex.DeliverySlot()
                                {
                                    StartDate = date + slotsFirstStartDate.TimeOfDay,
                                    EndDate = date + slotsFirstEndDate.TimeOfDay,
                                }
                            };
                            daysAdded++;
                        }

                        currentDayCursor++;
                    }
                    break;
                default:
                    sortedArrayOfDeliverySlots = null;
                    break;
            }

            return sortedArrayOfDeliverySlots;
        }

        private SerializableDictionary<string, string> ConvertTo(IDictionary<string, string> dicoSource)
        {
            var newDico = new SerializableDictionary<string, string>();
            foreach (var key in dicoSource.Keys)
                newDico.Add(key, dicoSource[key]);

            return newDico;
        }

        public Model.Solex.DeliverySlot CheckSlotAvailability(Model.Solex.DeliverySlot deliverySlot, DeliveryAddress deliveryAddress, List<Item> items, IEnumerable<Solex.AppointmentDelivery.Client.Contract.Promotion> promotions, int shippingMethodId)
        {
            var calendarQuery = CreateGetCalendarQuery(deliveryAddress, items, deliverySlot.StartDate.Date.AddDays(-1), deliverySlot.EndDate.Date.AddDays(1), promotions, shippingMethodId);
            var calendarResult = _appointmentDeliveryServiceClient.GetCalendar(calendarQuery);
            var slotAvailability = new Model.Solex.DeliverySlot();

            if (calendarResult.Slots.Any())
            {
                var resultSlot = calendarResult.Slots.FirstOrDefault(s => s.StartDate == deliverySlot.StartDate && s.EndDate == deliverySlot.EndDate);

                slotAvailability.StartDate = resultSlot?.StartDate ?? deliverySlot.StartDate;
                slotAvailability.EndDate = resultSlot?.StartDate ?? deliverySlot.EndDate;
                slotAvailability.Price = resultSlot?.Price ?? deliverySlot.Price;
                slotAvailability.SlotType = resultSlot?.SlotType ?? deliverySlot.SlotType;
                slotAvailability.PartnerId = resultSlot?.PartnerId ?? deliverySlot.PartnerId;
                slotAvailability.PartnerUniqueIdentifier = resultSlot?.PartnerUniqueIdentifier ?? deliverySlot.PartnerUniqueIdentifier;
                slotAvailability.PartnerInformations = resultSlot != null ? ConvertTo(resultSlot.PartnerInformation) : deliverySlot.PartnerInformations;
                slotAvailability.IsAvailable = resultSlot?.IsAvailable ?? false;
            }

            return slotAvailability;
        }

        public IEnumerable<Solex.AppointmentDelivery.Client.Contract.Promotion> GetPromotions(IEnumerable<Model.Article> articles)
        {
            var promotions = new List<Solex.AppointmentDelivery.Client.Contract.Promotion>();

            var baseArticles = articles.OfType<BaseArticle>().Where(art => art.ShippingPromotionRules != null);
            if (baseArticles != null && baseArticles.Any())
            {
                foreach (var baseArticle in baseArticles)
                {
                    foreach (var shippingPromotionRule in baseArticle.ShippingPromotionRules.Where(x => x != null))
                    {
                        var shippingPromotionTrigger = new Solex.AppointmentDelivery.Client.Contract.Promotion
                        {
                            TriggerKey = shippingPromotionRule.TriggerKey,
                            PromotionCode = shippingPromotionRule.Promotion != null ? shippingPromotionRule.Promotion.Code : string.Empty,
                            AdditionalRuleParam1 = shippingPromotionRule.Parameter1,
                            AdditionalRuleParam2 = shippingPromotionRule.Parameter2,
                            ApplyToLogisticTypes = shippingPromotionRule.LogisticFamily ?? new List<int>(),
                            Quantity = baseArticle.Quantity.GetValueOrDefault()
                        };

                        var existingTrigger = promotions.FirstOrDefault(x => x.TriggerKey == shippingPromotionTrigger.TriggerKey);

                        if (existingTrigger == null)
                        {
                            promotions.Add(shippingPromotionTrigger);
                        }
                        else
                        {
                            existingTrigger.Quantity += baseArticle.Quantity.GetValueOrDefault();
                        }
                    }
                }
            }

            return promotions;
        }

        public PreOrderResult GetPreOrderResult(Model.Solex.DeliverySlot deliverySlotSelected, PostalAddress deliveryAddress, List<Item> items)
        {
            var orderQuery = new PreOrderQuery()
            {
                StartDate = deliverySlotSelected.StartDate,
                EndDate = deliverySlotSelected.EndDate,
                PartnerId = deliverySlotSelected.PartnerId,
                PartnerUniqueIdentifier = deliverySlotSelected.PartnerUniqueIdentifier,

                DeliveryAddress = new DeliveryAddress()
                {
                    AddressLines = new[] { deliveryAddress.AddressLine1 },
                    City = deliveryAddress.City,
                    Country = _countryService.GetCountries().FirstOrDefault(c => c.ID == deliveryAddress.CountryId).Code3,
                    FirstName = deliveryAddress.Firstname,
                    LastName = deliveryAddress.LastName,
                    Phone = deliveryAddress.Phone,
                    ZipCode = deliveryAddress.ZipCode
                },
                Items = items
            };

            return _appointmentDeliveryServiceClient.PreOrder(orderQuery);
        }

        public AppointmentDeliverySlotsSortingResult GetSortedSlotsFromChoices(IEnumerable<ShippingChoice> choices, PostalAddress shippingAddress, PopOrderForm popOrderForm, IEnumerable<Model.Article> articles, HashSet<int> shippingMethods, DeliverySlotData deliverySlotData, int pageIndex, int pageSize, string sortingCriteria, DateTime endDateMaximum)
        {
            DateTime startDateReference;

            if (deliverySlotData.StartDateReference.HasValue && deliverySlotData.HasSelectedSlot)
            {
                startDateReference = deliverySlotData.StartDateReference.Value;
            }
            else
            {
                startDateReference = DateTime.Today.Date;
                deliverySlotData.StartDateReference = DateTime.Today.Date;
            }

            if (pageIndex < 0)
            {
                return AppointmentDeliverySlotsSortingResult.CreateInstanceWithError("PageIndex out of range");
            }

            var deliveryAddress = new DeliveryAddress
            {
                FirstName = shippingAddress.Firstname,
                LastName = shippingAddress.LastName,
                AddressLines = new[] { shippingAddress.AddressLine1, shippingAddress.AddressLine2, shippingAddress.AddressLine3, shippingAddress.AddressLine4 },
                City = shippingAddress.City,
                Country = _countryService.GetCountries().First(c => c.ID == shippingAddress.CountryId).Code3,
                Phone = shippingAddress.Phone,
                ZipCode = shippingAddress.ZipCode
            };

            var calendarResults = new List<CalendarResult>();
            List<Model.Article> eligibleArticles = null;
            var isFly = shippingMethods.Contains(Constants.ShippingMethods.Fly2Hours)
                        || shippingMethods.Contains(Constants.ShippingMethods.Fly5Hours);

            foreach (var appointmentMethodId in shippingMethods)
            {
                var appointmentDeliveryData = GetDataForAppointmentDeliveryFromOrderForm(popOrderForm, articles, appointmentMethodId);

                if (eligibleArticles == null
                    && appointmentDeliveryData?.EligibleArticles != null
                    && appointmentDeliveryData?.EligibleArticles.Count() > 0)
                {
                    eligibleArticles = appointmentDeliveryData.EligibleArticles;
                }

                if (!choices.Any(choice => choice.ContainsShippingMethodId(appointmentMethodId)))
                {
                    continue;
                }

                // On décale la plage de jours d'un jour pour FLY afin d'avoir un calendrier correct
                var adjustedStartDateReference = isFly
                    ? startDateReference.AddDays(1)
                    : startDateReference;

                var calendarResult = GetCalendarResultForAppointmentDelivery(appointmentMethodId, appointmentDeliveryData, adjustedStartDateReference, deliveryAddress, pageIndex, pageSize, endDateMaximum);
                if (calendarResult != null)
                {
                    calendarResults.Add(calendarResult);
                }
            }


            if (calendarResults?.Any() != true)
            {
                return AppointmentDeliverySlotsSortingResult.CreateInstanceWithError("No delivery shipping method can be found");
            }

            var allSlots = calendarResults
                        .Where(result => result?.Success == true)
                        .SelectMany(result => result.Slots);

            allSlots = FilterSlots(allSlots, shippingMethods);
            allSlots = SortSlots(allSlots, shippingMethods);

            if (!allSlots.Any())
            {
                return new AppointmentDeliverySlotsSortingResult
                (
                    minStartDate: startDateReference,
                    maxEndDate: endDateMaximum,
                    eligibleArticles: eligibleArticles,
                    pageIndex: pageIndex,
                    sortedSlots: new List<Model.Solex.DeliverySlot>[0],
                    startDateReference: startDateReference,
                    isServiceCallSuccess: calendarResults.Any(result => result?.Success == true)
                );
            }
            
            // We don't filter FLY by page cause Solex already returns the correct number of days
            var slots = isFly 
                ? allSlots
                : FilterSlotsByPage(pageIndex, pageSize, endDateMaximum, allSlots, out startDateReference);
           
            var sortedSlots = GetSortingSlotsTabs(choices, deliverySlotData, slots, pageSize, sortingCriteria);


            if (sortedSlots == null)
            {
                return AppointmentDeliverySlotsSortingResult.CreateInstanceWithError("Le critere de tri n'est pas valide");
            }

            var minStartDate = calendarResults.Min(result => result.MinStartDate);
            var maxEndDate = calendarResults.Min(result => result.MaxEndDate);

            return new AppointmentDeliverySlotsSortingResult(
                minStartDate: minStartDate,
                maxEndDate: maxEndDate,
                eligibleArticles: eligibleArticles,
                pageIndex: pageIndex,
                sortedSlots: sortedSlots,
                startDateReference: startDateReference
            );
        }

        /// <summary>
        /// temporary fix to deal with a chronopost query over the full 14 days and dealing with the pagination
        /// </summary>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="endDateMaximum"></param>
        /// <param name="allSlots"></param>
        /// <param name="startDateReference"></param>
        /// <returns></returns>
        private static IEnumerable<DeliverySlot> FilterSlotsByPage(int pageIndex, int pageSize, DateTime endDateMaximum,
            IEnumerable<DeliverySlot> allSlots, out DateTime startDateReference)
        {
            startDateReference = allSlots.OrderBy(s => s.StartDate).ThenBy(s => s.EndDate).First(s => s.IsAvailable)
                .StartDate.Date;
            
            var daysIndexOffset = (double) ((pageIndex - 1) * pageSize);
            var startDate = startDateReference.AddDays(daysIndexOffset);
            var endDate = startDate.AddDays(pageSize);

            if (endDate.Date > endDateMaximum.Date)
            {
                startDate = endDateMaximum.AddDays(-pageSize);
            }

            return allSlots.Where(s => s.StartDate.Date >= startDate.Date);
        }

        private IEnumerable<Solex.AppointmentDelivery.Client.Contract.DeliverySlot> FilterSlots(IEnumerable<Solex.AppointmentDelivery.Client.Contract.DeliverySlot> slotsToFilter, HashSet<int> shippingMethods)
        {
            var shippingMethodIds = _appointmentDeliveryService.ShippingMethodsPriorityOrder.Any() ? _appointmentDeliveryService.ShippingMethodsPriorityOrder : shippingMethods.ToHashSet();
            var slots = new List<Solex.AppointmentDelivery.Client.Contract.DeliverySlot>();
            var daysWithSlots = new List<Tuple<DateTime, DateTime>>();

            foreach (var slot in slotsToFilter)
            {
                if (!shippingMethodIds.Contains(slot.ShippingMethodId))
                {
                    continue;
                }

                var slotDay = new Tuple<DateTime, DateTime>(slot.StartDate, slot.EndDate);
                if (daysWithSlots.Contains(slotDay))
                {
                    continue;
                }

                slots.Add(slot);
                daysWithSlots.Add(slotDay);
            }

            return slots;
        }

        private IEnumerable<Solex.AppointmentDelivery.Client.Contract.DeliverySlot> SortSlots(IEnumerable<Solex.AppointmentDelivery.Client.Contract.DeliverySlot> slotsToSort, HashSet<int> shippingMethods)
        {
            var priorityOrder = _appointmentDeliveryService.ShippingMethodsPriorityOrder.Any() ? _appointmentDeliveryService.ShippingMethodsPriorityOrder.ToList() : shippingMethods.ToList();
            return slotsToSort.OrderBy(slot => priorityOrder.IndexOf(slot.ShippingMethodId));
        }

        private CalendarResult GetCalendarResultForAppointmentDelivery(int shippingMethodId, ItemsForAppointmentDeliveryRequest appointmentDeliveryData, DateTime startDateReference, DeliveryAddress deliveryAddress, int pageIndex, int pageSize, DateTime endDateMaximum)
        {
            CalendarResult calendarResult;

            try
            {
                calendarResult = GetCalendarResult(deliveryAddress, appointmentDeliveryData.Items, startDateReference, endDateMaximum, pageSize, pageIndex, appointmentDeliveryData.Promotions, shippingMethodId);
            }
            catch (Exception exception)
            {
                Logging.Current.WriteError($"Error while getting calendar result for ShippingId = {shippingMethodId}", exception);
                calendarResult = null;
            }

            return calendarResult;
        }

        private ItemsForAppointmentDeliveryRequestWitEligibleArticles GetDataForAppointmentDeliveryFromOrderForm(PopOrderForm popOrderForm, IEnumerable<Model.Article> articles, int shippingMethodId)
        {
            var postalShippingMethodEvaluationItem = GetPostalShippingMethodEvaluationItem(popOrderForm.ShippingMethodEvaluation.FnacCom);

            var itemsForAppointmentDeliveryRequest = _appointmentDeliveryCalendarService.GetItemsAndPromotionForAppointmentDeliveryRequest(articles, postalShippingMethodEvaluationItem, shippingMethodId);

            var eligibleArticles = (from article in articles
                                    where itemsForAppointmentDeliveryRequest.Items.Any(i => i.Identifier.Prid == article.ProductID)
                                    select article).ToList();

            return new ItemsForAppointmentDeliveryRequestWitEligibleArticles(itemsForAppointmentDeliveryRequest.Promotions, itemsForAppointmentDeliveryRequest.Items, eligibleArticles);
        }

        private PostalShippingMethodEvaluationItem GetPostalShippingMethodEvaluationItem(Maybe<ShippingMethodEvaluationGroup> fnacComShippingMethodEvaluationGroup)
        {
            if (!fnacComShippingMethodEvaluationGroup.HasValue)
            {
                return null;
            }

            if (!fnacComShippingMethodEvaluationGroup.Value.PostalShippingMethodEvaluationItem.HasValue)
            {
                return null;
            }

            return fnacComShippingMethodEvaluationGroup.Value.PostalShippingMethodEvaluationItem.Value;
        }

    }
}
