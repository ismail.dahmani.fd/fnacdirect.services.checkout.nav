﻿using FnacDirect.OrderPipe.CM.Business;
using FnacDirect.OrderPipe.FDV.Model;
using FnacDirect.Technical.Framework.CoreServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace FnacDirect.OrderPipe.CM.Business
{
    [Serializable]
    public class Parameters
    {
        public PropCommReportData Value { get; set; }
    }



    public class ReportPrinterFacade : MarshalByRefObject 
    {
        private ReportPrinter _existing = new ReportPrinter(HttpContext.Current.Server.MapPath(ConfigurationManager.Current.AppSettings.Settings["report.PropComm.rdl.relativepath"].Value), string.Empty);

        public byte[] GetBuffer(Parameters reportData)
        {
            return _existing.GetBuffer(reportData.Value);
        }
    }
}
