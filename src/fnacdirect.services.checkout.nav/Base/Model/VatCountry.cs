using System.Xml.Serialization;

namespace FnacDirect.OrderPipe.Base.Model
{
    public class VatCountry
    {
        private int? _vatCountryId;
        [XmlIgnore]
        public int? VatCountryId
        {
            get { return _vatCountryId; }
            set { _vatCountryId = value; }
        }

        private bool _useVat;
        [XmlIgnore]
        public bool UseVat
        {
            get { return _useVat; }
            set { _useVat = value; }
        }
    }
}
