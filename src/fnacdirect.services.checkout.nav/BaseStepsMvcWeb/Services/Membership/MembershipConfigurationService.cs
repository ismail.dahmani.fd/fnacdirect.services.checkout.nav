using System.Collections.Generic;
using System.Linq;
using FnacDirect.Membership.Business;
using FnacDirect.Membership.Configuration;
using FnacDirect.Membership.Model.Constants;

namespace FnacDirect.OrderPipe.Base.Proxy.Membership
{
    public class MembershipConfigurationService : IMembershipConfigurationService
    {
        public AdherentCardArticle GetAdherentCardArticleByPrid(int prid)
        {
            return MembershipConfigurationBusiness.AdherentCardArticle(prid);
        }

        public IEnumerable<AdherentCardArticle> GetAdherentCardArticles()
        {
            return MembershipConfigurationBusiness.GetAdherentCardArticles();
        }

        public int GetPromotionKey(IEnumerable<string> promotions)
        {
            return MembershipConfigurationBusiness.GetPromotionKey(promotions.ToList());
        }

        public string GetOrderPipeControlKey(string opScreen, int adherentProfilKey, int promotionKey)
        {
            return MembershipConfigurationBusiness.GetOrderPipeControlKey(opScreen, adherentProfilKey, promotionKey);
        }

        public List<int> FnacPlusCardTypes
        {
            get { return MembershipConfigurationBusiness.FnacPlusCardTypes; }
        }

        public int FnacPlusTryCardType
        {
            get { return MembershipConfigurationBusiness.FnacPlusTryCardType; }
        }

        public int PridSavingCard
        {
            get { return MembershipConfigurationBusiness.PridSavingCard; }
        }

        public int GetAdherentProfileKey(string adherentProgram, string adherentStatus, string cardDuration)
        {
            return MembershipConfigurationBusiness.GetAdherentProfileKey(adherentProgram, adherentStatus, cardDuration);
        }

        public int GetTryCardPrid()
        {
            return MembershipConfigurationBusiness.TryCardPrid;
        }

        public int DelayAfterExpiration
        {
            get { return MembershipConfigurationBusiness.DelayAfterExpiration; }
        }

        public int DelayBeforeExpiration
        {
            get { return MembershipConfigurationBusiness.DelayBeforeExpiration; }
        }

        public bool IsFnacPlusEnabled
        {
            get { return MembershipConfigurationBusiness.IsFnacPlusEnabled; }
        }

        public bool IsFnacPlusCanalPlayEnabled
        {
            get { return MembershipConfigurationBusiness.IsFnacPlusCanalPlayEnabled; }
        }

        public AdherentCardArticle GetAdherentCardByKey(string key)
        {
            return MembershipConfigurationBusiness.GetAdherentCardByKey(key);
        }

        public BusinessType BusinessType()
        {
            return MembershipConfigurationBusiness.BusinessChoice;
        }
    }
}
