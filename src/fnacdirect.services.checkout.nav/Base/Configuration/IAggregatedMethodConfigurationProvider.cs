using FnacDirect.OrderPipe.Base.Business.Shipping;
using System.Collections.Generic;

namespace FnacDirect.OrderPipe.Base.Business.Configuration
{
    public interface IAggregatedMethodConfigurationProvider
    {
        IDictionary<int, AggregatedMethodConfiguration> AggregatedMethodsConfiguration { get; }
    }
}
