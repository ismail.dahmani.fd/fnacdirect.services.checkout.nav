using FnacDirect.OrderPipe.Base.Model;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Common.Payment
{
    public interface ICustomerCreditCardBaseViewModelBuilder
    {
        CustomerCreditCardBaseViewModel Build(string userReference);
        CustomerCreditCardViewModel BuildSelfCheckoutCreditCardViewModel(PopOrderForm popOrderForm, string userReference);
    }
}
