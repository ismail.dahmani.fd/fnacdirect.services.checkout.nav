﻿namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Common.Payment
{
    public class CreditCardViewModel
    {
        public CreditCardViewModel()
        {
            
        }

        public CreditCardViewModel(int id, string label, string brand)
        {
            Id = id;
            Label = label;
            Brand = brand;
        }

        public int Id { get; set; }

        public int Order { get; set; }

        public string Label { get; set; }

        public string Brand { get; set; }

        public string Url { get; set; }

        public int NumberLength { get; set; }

        public int CryptoLength { get; set; }

        public string Format { get; set; }

        public string Prefix { get; set; }

        public string TransactionProcess { get; set; }

        public bool Is3DSecureTransaction { get; set; }

        public bool IsCryptoRequired { get; set; }

        public bool HasExpDate { get; set; }

        // Valorisé uniquement si op.ogone.enablealiasgateway est true
        public decimal TransactionAmount { get; set; }

        // Valorisé uniquement si op.ogone.enablealiasgateway est true
        public decimal TransactionRealAmount { get; set; }
    }
}
