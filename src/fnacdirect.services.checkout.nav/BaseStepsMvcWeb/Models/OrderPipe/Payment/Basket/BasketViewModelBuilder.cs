using System.Globalization;
using FnacDirect.OrderPipe.Base.BaseModel;
using FnacDirect.OrderPipe.Base.Business.Availability;
using FnacDirect.OrderPipe.Base.Business.Shipping.Shop;
using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.OrderPipe.Base.Model.LineGroups;
using FnacDirect.OrderPipe.Base.Proxy.Articles;
using FnacDirect.OrderPipe.Base.Steps.Pop;
using FnacDirect.OrderPipe.BaseMvc.Web.Controllers.Pop;
using FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Common.Basket;
using FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Common.Shipping;
using FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Common.Shipping.Relay;
using FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Common.Shipping.Shop;
using FnacDirect.Technical.Framework.Web.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using FnacDirect.OrderPipe.Base.Proxy.Store;
using static FnacDirect.OrderPipe.Constants;
using FnacDirect.IdentityImpersonation;
using FnacDirect.OrderPipe.Base.Business;
using FnacDirect.OrderPipe.Base.Model.Solex;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Payment
{
    public class BasketViewModelBuilder : IBasketViewModelBuilder
    {
        private readonly IApplicationContext _applicationContext;
        private readonly IBasketItemViewModelBuilder _basketItemViewModelBuilder;
        private readonly IOrderPipeUrlBuilder _orderPipeUrlBuilder;
        private readonly IShipToStoreAvailabilityService _shipToStoreAvailabilityService;
        private readonly ISellerInformationsViewModelBuilder _sellerInformationsViewModelBuilder;
        private readonly IPopAvailabilityProcessor _popAvailabilityProcessor;
        private readonly IW3ArticleService _w3ArticleService;
        private readonly IFnacStoreService _fnacStoreService;
        private readonly ISwitchProvider _switchProvider;
        private readonly IMembershipDateBusiness _membershipDateBusiness;

        public BasketViewModelBuilder(IApplicationContext applicationContext,
            ISellerInformationsViewModelBuilder sellerInformationsViewModelBuilder,
            IShipToStoreAvailabilityService shipToStoreAvailabilityService,
            IBasketItemViewModelBuilder basketItemViewModelBuilder,
            IOrderPipeUrlBuilder orderPipeUrlBuilder,
            IPopAvailabilityProcessor popAvailabilityProcessor,
            IW3ArticleService w3ArticleService,
            IFnacStoreService fnacStoreService,
            ISwitchProvider switchProvider,
            IMembershipDateBusiness membershipDateBusiness)
        {
            _applicationContext = applicationContext;
            _sellerInformationsViewModelBuilder = sellerInformationsViewModelBuilder;
            _basketItemViewModelBuilder = basketItemViewModelBuilder;
            _orderPipeUrlBuilder = orderPipeUrlBuilder;
            _shipToStoreAvailabilityService = shipToStoreAvailabilityService;
            _popAvailabilityProcessor = popAvailabilityProcessor;
            _fnacStoreService = fnacStoreService;
            _w3ArticleService = w3ArticleService;
            _switchProvider = switchProvider;
            _membershipDateBusiness = membershipDateBusiness;
        }

        public IShippingViewModel GetShippingViewModel(ShippingMethodEvaluationGroup shippingMethodEvaluationGroup, int countryId)
        {
            IShippingViewModel shippingViewModel = null;

            if (shippingMethodEvaluationGroup.SelectedShippingMethodEvaluationType != ShippingMethodEvaluationType.Unknown)
            {
                if (shippingMethodEvaluationGroup.SelectedShippingMethodEvaluationType == ShippingMethodEvaluationType.Postal)
                {
                    var postalShippingMethodEvaluationItem = shippingMethodEvaluationGroup.PostalShippingMethodEvaluationItem.Value;

                    shippingViewModel = new PostalShippingViewModel(postalShippingMethodEvaluationItem.ShippingAddress.ConvertToAddressViewModel(countryId));
                }
                else if (shippingMethodEvaluationGroup.SelectedShippingMethodEvaluationType == ShippingMethodEvaluationType.Shop)
                {
                    var shopShippingMethodEvaluationItem = shippingMethodEvaluationGroup.ShopShippingMethodEvaluationItem.Value;

                    shippingViewModel = new ShopShippingViewModel(new ShopAddressViewModel(shopShippingMethodEvaluationItem.ShippingAddress));
                }
                else if (shippingMethodEvaluationGroup.SelectedShippingMethodEvaluationType == ShippingMethodEvaluationType.Relay)
                {
                    var relayShippingMethodEvaluationItem = shippingMethodEvaluationGroup.RelayShippingMethodEvaluationItem.Value;

                    shippingViewModel = new RelayShippingViewModel(new RelayAddressViewModel(relayShippingMethodEvaluationItem.ShippingAddress));
                }
            }

            return shippingViewModel;
        }

        public string GetAvailability(StandardArticle standardArticle, ShippingMethodEvaluationGroup shippingMethodEvaluationGroup, IEnumerable<ILineGroup> lineGroups, IEnumerable<int> excludedLogisticTypes, IdentityImpersonator identityImpersonator)
        {
            if (standardArticle.AvailabilityId == (int)AvailabilityEnum.PreOrder || standardArticle.AvailabilityId == (int)AvailabilityEnum.PreOrderBis)
            {

                var w3Article = _w3ArticleService.GetArticle(standardArticle.ToArticleReference());
                var dtoArticle = w3Article.DTO;

                var locale = _applicationContext.GetLocale();

                var preOrder = _applicationContext.GetMessage("orderpipe.pop.basket.availability.preorder");

                if (standardArticle.IsNumerical)
                {
                    preOrder = _applicationContext.GetMessage("orderpipe.pop.basket.availability.numerical.preorder");
                }

                if (dtoArticle.ExtendedProperties.DisplayAnnouncementDay.HasValue && dtoArticle.ExtendedProperties.DisplayAnnouncementDay.Value)
                {
                    var preOrderFullDate = _applicationContext.GetMessage("orderpipe.pop.basket.availability.preorder.fulldate");
                    var date = standardArticle.AnnouncementDate.ToString("dd MMMM yyyy", CultureInfo.GetCultureInfo(locale));
                    return string.Format(preOrder, string.Format(preOrderFullDate, date));
                }

                var preOrderMonth = _applicationContext.GetMessage("orderpipe.pop.basket.availability.preorder.month");
                var month = standardArticle.AnnouncementDate.ToString("MMMM yyyy", CultureInfo.GetCultureInfo(locale));
                return string.Format(preOrder, string.Format(preOrderMonth, month));

            }


            if (shippingMethodEvaluationGroup.SelectedShippingMethodEvaluationType == ShippingMethodEvaluationType.Shop)
            {
                var shopShippingMethodEvaluationItem = shippingMethodEvaluationGroup.ShopShippingMethodEvaluationItem.Value;

                var shopAddress = shopShippingMethodEvaluationItem.ShippingAddress;

                var fnacStore = _fnacStoreService.GetStore(shopAddress.Identity.GetValueOrDefault());

                var shopAvailabilities = _shipToStoreAvailabilityService.GetAvailabilities(fnacStore, lineGroups.SelectMany(l => l.Articles), excludedLogisticTypes, identityImpersonator);

                return _popAvailabilityProcessor.GetShortGlobalEstimatedDeliveryDateForStore(fnacStore, standardArticle, shopAvailabilities);
            }
            else
            {
               var shippingMethod = shippingMethodEvaluationGroup.SelectedShippingMethodEvaluation.GetDefaultShippingMethodId().FirstOrDefault();

                var availabilities = new List<PopAvailabilityProcessorInput>();

                var selectedShippingMethodId = shippingMethodEvaluationGroup.SelectedShippingMethodEvaluation.SelectedShippingMethodId;
                var choice = shippingMethodEvaluationGroup.SelectedShippingMethodEvaluation.Choices.FirstOrDefault(c => c.MainShippingMethodId == selectedShippingMethodId);

                if (choice != null) {
                    foreach (var parcel in choice.Parcels.OrderBy(p => p.Value.Source.DeliveryDateMax))
                    {
                        var parcelId = parcel.Key;

                        if (_switchProvider.IsEnabled("orderpipe.pop.shipping.excludeparcelwithservicearticles"))
                        {
                            var parcelIsExcluded = choice.Groups
                                .Where(g => excludedLogisticTypes.Contains(g.Id))
                                .SelectMany(x => x.Items)
                                .Any(i => i.ParcelId == parcelId);

                            if (parcelIsExcluded)
                            {
                                continue;
                            }
                        }

                        var currentShippingMethodId = parcel.Value.ShippingMethodId;

                        var label = _applicationContext.TryGetMessage("orderpipe.pop.shipping.methods." + currentShippingMethodId);

                        if (string.IsNullOrEmpty(label))
                        {
                            if (!string.IsNullOrEmpty(choice.AltLabel))
                            {
                                label = choice.AltLabel;
                            }
                            else
                            {
                                label = choice.Label;
                            }
                        }

                        List<Identifier> articlesForParcel = null;
                        if (_switchProvider.IsEnabled("orderpipe.pop.shipping.excludeparcelwithservicearticles"))
                        {
                            articlesForParcel = choice.Groups.SelectMany(x => x.Items).Where(i => i.ParcelId == parcelId).Select(i => i.Identifier).ToList();
                        }
                        else
                        {
                            articlesForParcel = choice.Groups
                            .Where(g => !excludedLogisticTypes.Contains(g.Id))
                            .SelectMany(x => x.Items)
                            .Where(i => i.ParcelId == parcelId)
                            .Select(i => i.Identifier).ToList();
                        }

                        var articles = lineGroups.GetArticles();
                        if (!_switchProvider.IsEnabled("orderpipe.pop.shipping.dlemp.enable"))
                        {
                            articles = articles.Where(a => a is StandardArticle);
                        }
                        var availabilityList = new PopAvailabilityProcessorInputListBuilder(articles, currentShippingMethodId)
                            .ExcludeLogisticTypes(excludedLogisticTypes)
                            .FilterArticles((article) => (articlesForParcel.Any(a => a.Prid == article.ProductID)))
                            .WithShippingMethodId()
                            .BuildList();
                        availabilities.AddRange(availabilityList);
                    }
                }

                var artAvailabilities = availabilities.Where(art => art.Article.ProductID == standardArticle.ProductID);
                if (artAvailabilities.Any())
                {
                    return _popAvailabilityProcessor.GetShortGlobalEstimatedDeliveryDate(shippingMethod, artAvailabilities, null);
                }
            }

            return string.Empty;
        }

        public BasketSellerViewModel GetBasketSellerViewModel(PopOrderForm popOrderForm, ShippingMethodEvaluationGroup shippingMethodEvaluationGroup, int countryId)
        {
            var popShippingData = popOrderForm.GetPrivateData<PopShippingData>();

            var lineGroups = popOrderForm.LineGroups.Seller(shippingMethodEvaluationGroup.SellerId);

            var basketSellerViewModel = new BasketSellerViewModel();

            var shippingViewModel = GetShippingViewModel(shippingMethodEvaluationGroup, countryId);

            var articles = lineGroups.SelectMany(l => l.Articles).ToList();

            foreach (var article in articles)
            {
                BasketItemViewModel basketItemViewModel = null;

                if (article is StandardArticle)
                {
                    basketItemViewModel = _basketItemViewModelBuilder.BuildItemStandardArticle(article as StandardArticle, popOrderForm.GlobalPromotions);

                    var standardArticle = article as StandardArticle;

                    if (standardArticle.Hidden)
                    {
                        continue;
                    }

                    basketItemViewModel.Availability = GetAvailability(standardArticle, shippingMethodEvaluationGroup, lineGroups, popOrderForm.ShippingMethodEvaluation.ExcludedLogisticTypes, popOrderForm.UserContextInformations.IdentityImpersonator);
                }
                else if (article is MarketPlaceArticle)
                {
                    basketItemViewModel = _basketItemViewModelBuilder.BuildItemMpArticle(article as MarketPlaceArticle);
                }

                if (basketItemViewModel != null)
                {
                    var selectedShippingMethod = shippingMethodEvaluationGroup.SelectedShippingMethod;
                    basketItemViewModel.IsAppointment = selectedShippingMethod.HasValue && selectedShippingMethod.Value == ShippingMethods.Fly5Hours;
                    basketSellerViewModel.WithItem(basketItemViewModel);
                }
            }

            if (shippingViewModel != null)
            {
                var popShippingSellerData = popShippingData.Sellers.FirstOrDefault(s => s.SellerId == shippingMethodEvaluationGroup.SellerId);

                TryApplyLinkAndShippingCostToShippingViewModel(popOrderForm, popShippingSellerData, lineGroups, shippingViewModel);

                basketSellerViewModel.Shipping = Maybe.Some(shippingViewModel);
            }

            return basketSellerViewModel;
        }

        public void TryApplyLinkAndShippingCostToShippingViewModel(PopOrderForm popOrderForm, PopShippingSellerData popShippingSellerData, IEnumerable<ILineGroup> lineGroups, IShippingViewModel shippingViewModel)
        {
            if (popShippingSellerData != null)
            {
                var popShippingRouteValueProvider = new PopShippingRouteValueProvider();
                var routeValues = popShippingRouteValueProvider.GetRouteValueFor(popOrderForm, popShippingSellerData);
                shippingViewModel.Link = _orderPipeUrlBuilder.GetUrl("DisplayShippingAddress", routeValues);
            }

            var shippingCost = 0m;

            foreach (var lineGroup in lineGroups)
            {
                shippingCost += lineGroup.GlobalPrices.TotalShipPriceUserEur.GetValueOrDefault();
            }

            var priceHelper = _applicationContext.GetPriceHelper();

            shippingViewModel.ShippingCost = priceHelper.Format(shippingCost, PriceFormat.CurrencyAsDecimalSeparator);
        }

        public BasketViewModel Build(PopOrderForm popOrderForm)
        {
            var basketViewModel = new BasketViewModel
            {
                DisplayBirthDatePopin = _membershipDateBusiness.DisplayBirthDatePopin(popOrderForm)
            };

            var sellers = popOrderForm.LineGroups.Sellers().ToList();

            var shippingMethodEvaluation = popOrderForm.ShippingMethodEvaluation;

            var countryId = _applicationContext.GetSiteContext().CountryId;

            foreach (var seller in sellers)
            {
                var maybeShippingMethodEvaluationGroup = shippingMethodEvaluation.GetShippingMethodEvaluationGroup(seller);

                if (!maybeShippingMethodEvaluationGroup.HasValue)
                {
                    continue;
                }

                var basketSellerViewModel = GetBasketSellerViewModel(popOrderForm, maybeShippingMethodEvaluationGroup.Value, countryId);

                basketSellerViewModel.Informations = _sellerInformationsViewModelBuilder.Build(popOrderForm, seller);

                basketViewModel.WithSeller(basketSellerViewModel);
            }

            return basketViewModel;
        }
    }
}
