using FnacDirect.Ogone.BLL;
using FnacDirect.OrderPipe.Base.Model;
using System.Collections.Generic;
using System.Linq;

namespace FnacDirect.OrderPipe.Base.Proxy.Ogone.PspId
{
    /// <summary>
    /// Contient les methodes relatives au PspId, pour le pipe SelfCheckout
    /// </summary>
    public class SelfCheckoutPspIdService : IPspIdService
    {
        private readonly int _legalEntityId;
        private readonly AOgoneConfigurationManager _aOgoneConfigurationManager;

        //should i put this into conf ?
        private const string PSPID_CODIREP = "PSPIDCODIREP";
        private const string PSPID_RELAISFNAC = "PSPIDRELAISFNAC";
        private const string PSPID_FNAC_PERIPHERIE = "PSPIDFNACPERIPHERIE";
        private const string PSPID_FNACPARIS = "PSPIDFNACPARIS";
        private const string PSPID_MONACO = "PSPIDMONACO";
        private const string PSPID_FNACACCES = "PSPIDFNACACCES";

        public SelfCheckoutPspIdService(int legalEntityId, AOgoneConfigurationManager aOgoneConfigurationManager)
        {
            _legalEntityId = legalEntityId;
            _aOgoneConfigurationManager = aOgoneConfigurationManager;
        }

        public string ComputePspId(OrderTypeEnum orderType, IEnumerable<Article> articles, OgoneConfigurationData ogoneConfigurationData)
        {
            // Contrairement a DefaultPspIdService, nous recuperons le PspId selon l'identifiant LegalEntity du magasin.
            var pspIdKey = string.Empty;

            switch (_legalEntityId)
            {
                case 1: //Fnac Paris
                    pspIdKey = PSPID_FNACPARIS;
                    break;
                case 2: //Codirep
                    pspIdKey = PSPID_CODIREP;
                    break;
                case 3: //Relais Fnac
                    pspIdKey = PSPID_RELAISFNAC;
                    break;
                case 4: //Fnac Monaco
                    pspIdKey = PSPID_MONACO;
                    break;
                case 9: //Fnac Periph
                    pspIdKey = PSPID_FNAC_PERIPHERIE;
                    break;
                case 24: //Fnac Acces
                    pspIdKey = PSPID_FNACACCES;
                    break;
                default: //Si on n est pas avec un LegalEntity correct, on
                    var rootMessage = "The Legal Entity is not allowed for SelfCheckout. ";
                    var shopMessage = string.Empty;
                    var firstArticle = articles.FirstOrDefault(a => a is StandardArticle sa && sa.ShopStocks != null);
                    if(firstArticle != null)
                    {
                        if (firstArticle is StandardArticle stdArticle)
                        {
                            var refUG = stdArticle.ShopStocks?.RefUG;
                            if (!string.IsNullOrEmpty(refUG))
                            {
                                shopMessage = $"The RegUG of the store is {refUG}.";
                            }
                        }
                    }
                    throw new System.Exception(rootMessage + shopMessage);
            }

            return _aOgoneConfigurationManager.InputParams[pspIdKey].Value;
        }
    }
}
