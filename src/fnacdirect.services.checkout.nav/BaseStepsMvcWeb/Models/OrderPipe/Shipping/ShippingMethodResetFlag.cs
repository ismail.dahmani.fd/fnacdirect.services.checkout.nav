﻿using System;

namespace FnacDirect.OrderPipe.Base.Model.Shipping.ShippingMethod
{
    [Flags]
    public enum ShippingMethodResetFlag
    {
        None = 0,
        All = ~0,
        ResetShopItems = 1,
        ResetPostalItems = 2,
        ResetRelayItems = 4,
        ResetSelectedShippingMethod = 8,
        ResetSelectedShippingMethodEvaluationType = 16,
        ResetLineGroupLogisticTypeShippingMethodSelection = 32
    }
}