using FnacDirect.OrderPipe.Base.Business.Availability;
using System.Collections.Generic;
using FnacDirect.OrderPipe.Base.Business.Operations;
using FnacDirect.Technical.Framework.ServiceLocation;
using FnacDirect.Technical.Framework.Web.Mvc;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Common.Shipping
{
    public class SelectedShippingItemSummary
    {
        public string Label { get; set; }
        public string ShortEstimatedDeliveryDate { get; set; }
        public string ShippingCost { get; set; }

        public string ShippingCostNoVat { get; set; }

        public string ShippingCostToDisplay { get; set; }
        public PriceDetailsViewModel ShippingCostModelToDisplay { get; set; }
        public bool DisplayComplexLongEstimatedDeliveryDates { get; set; }
        public bool DisplayLongEstimatedDeliveryDates { get; set; }
        public IEnumerable<PopAvailabilityProcessorOutput<PopAvailabilityProcessorOutput>> ComplexLongEstimatedDeliveryDate { get; set; }
        public IEnumerable<PopAvailabilityProcessorOutput> LongEstimatedDeliveryDate { get; set; }

        public bool CanChooseOther { get; set; }

        public System.DateTime? WorstDeliveryDate { get; set; }
        public bool BeforeXmas
        {
            get
            {
                var deliveredForChristmasService = ServiceLocator.Current.GetInstance<IDeliveredForChristmasService>();
                var eventDate = deliveredForChristmasService.GetEventDateOfCurrentPhase();

                if (eventDate.HasValue)
                {
                    return WorstDeliveryDate.HasValue && WorstDeliveryDate.Value < eventDate.Value;
                }
                else
                {
                    return false;
                }
            }
        }

        public bool IsFreeShipping { get; set; }
    }
}
