﻿using FnacDirect.Contracts.Online.Model;

namespace FnacDirect.OrderPipe.Base.Business
{
    public class MarketPlaceServiceProvider : IMarketPlaceServiceProvider
    {
        public IMarketPlaceService GetMarketPlaceService(SiteContext sitecontext)
        {
            return new MarketPlaceBusiness(sitecontext);
        }
    }
}
