﻿namespace FnacDirect.OrderPipe.Base.Business
{
    public interface IPricingServiceProvider
    {
        IPricingService GetPricingService(int marketId);
    }
}
