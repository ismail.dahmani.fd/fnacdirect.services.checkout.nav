using FnacDirect.Relay.Model;
using FnacDirect.Technical.Framework.Web.Mvc;
using FnacDirect.Technical.Framework.ServiceLocation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Serialization;

namespace FnacDirect.OrderPipe.Base.Model
{
    public class RelayAddress : ShippingAddress
    {
        [XmlIgnore]
        public string Alias { get; set; }

        [XmlIgnore]
        public string Company { get; set; }

        [XmlIgnore]
        public string Firstname { get; set; }

        [XmlIgnore]
        public string LastName { get; set; }

        [XmlIgnore]
        public string AddressLine1 { get; set; }

        [XmlIgnore]
        public string AddressLine2 { get; set; }

        [XmlIgnore]
        public string AddressLine3 { get; set; }

        [XmlIgnore]
        public string AddressLine4 { get; set; }

        [XmlIgnore]
        public string ZipCode { get; set; }

        [XmlIgnore]
        public string City { get; set; }

        [XmlIgnore]
        public string Status { get; set; }

        [XmlIgnore]
        public string CellPhone { get; set; }

        [XmlIgnore]
        public string Fax { get; set; }

        [XmlIgnore]
        public DateTime FirstDateParcel { get; set; }

        [XmlIgnore]
        public DateTime LastDateParcel { get; set; }

        [XmlIgnore]
        public DateTime ClosureDate { get; set; }

        [XmlIgnore]
        public int? ForeignRelayId { get; set; }

        [XmlIgnore]
        public int? FDRelayId { get; set; }

        public override int Type
        {
            get
            {
                var appContext = ServiceLocator.Current.GetInstance<IApplicationContext>();
                return int.Parse(appContext.GetAppSetting("relay.network"));
            }
        }

        [XmlIgnore]
        public List<RelayAdditionalPropertyEntity> AdditionalProperties { get; set; }

        public RelayAddress()
        {
        }

        /// <summary>
        /// Concanténation des AddtionalProperties "xeet" et "pseudoRvc" pour identifier un relais colis pour un article MarketPlace
        /// Insérer dans le champ codeGU de la ShippingAddress pour les commandes MarketPlace en livraison relais colis
        /// </summary>
        [XmlIgnore]
        public string CodeXeetPseudoRvc
        { 
            get { 
                string result = null;
                if (AdditionalProperties != null 
                    && AdditionalProperties.Any(ap => ap.Label == "xeet")
                    && AdditionalProperties.Any(ap => ap.Label == "pseudoRvc"))
                {
                    result = string.Concat( AdditionalProperties.First(p => p.Label == "xeet").Value.Trim(),
                                            "|",
                                            AdditionalProperties.First(p => p.Label == "pseudoRvc").Value.Trim());
                }

                return result;
            }
        }


        public RelayAddress(RelayAddressEntity address, int countryId)
        {
            Copy(address, this, countryId);
        }

        public static void Copy(RelayAddressEntity rae, RelayAddress ra, int countryId)
        {
            ra.Identity = rae.Id;
            ra.FDRelayId = rae.Id;
            ra.ForeignRelayId = rae.ForeignRelayId;
            ra.AddressBookReference = rae.AddressBookReference;
            ra.Reference = rae.AddressBookReference;

            ra.Alias = string.Format("Relais {0}", rae.BusinessName);
            ra.LastName = rae.BusinessName;
            ra.Company = string.Empty;

            ra.AddressLine1 = rae.AddressPart3;
            ra.AddressLine2 = rae.AddressPart1;
            ra.AddressLine3 = rae.AddressPart2;
            ra.AddressLine4 = string.Empty;
            ra.City = rae.City;
            ra.ZipCode = rae.ZipCode;
            ra.CountryId = countryId;
            ra.IsCountryUseVAT = true;
            ra.ShippingZone = 1;

            ra.FirstDateParcel = rae.FirstPackageDate;
            ra.LastDateParcel = rae.LastPackageDate;
            ra.ClosureDate = rae.ClosureDate;
            ra.Status = rae.Status;
            ra.AdditionalProperties = rae.AdditionalProperties;
        }

        public override bool IsDummy()
        {
            return !Identity.HasValue || Identity.Value <= 0;
        }

        public int? GetDistanceToNearestStore()
        {
            if (AdditionalProperties == null || AdditionalProperties.Count == 0)
                return null;

            var addPropNearestStoreDistance = AdditionalProperties.FirstOrDefault(p => p.Label == "NearestStoreDistance");

            if (addPropNearestStoreDistance == null)
                return null;

            if (!int.TryParse(addPropNearestStoreDistance.Value, out var distance))
                return null;

            return distance;            
        }
    }
}
