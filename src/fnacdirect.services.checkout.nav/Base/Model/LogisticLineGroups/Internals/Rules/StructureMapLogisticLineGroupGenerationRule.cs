﻿using FnacDirect.OrderPipe.Base.Business.LogisticLineGroups.Handlers;
using FnacDirect.OrderPipe.Base.Business.LogisticLineGroups.Internals.Handlers;
using FnacDirect.OrderPipe.Base.Business.LogisticLineGroups.Internals.Initializers;
using FnacDirect.OrderPipe.Base.Business.LogisticLineGroups.Internals.InitialSelectors;
using FnacDirect.OrderPipe.Base.Business.LogisticLineGroups.Internals.Unfolders;
using FnacDirect.OrderPipe.Base.Business.LogisticLineGroups.Rules;
using FnacDirect;
using StructureMap;
using System;
using System.Linq;
using System.Collections.Generic;

namespace FnacDirect.OrderPipe.Base.Business.LogisticLineGroups.Internals.Rules
{
    public class StructureMapLogisticLineGroupGenerationRule : BaseLogisticLineGroupGenerationRule
    {
        public IContainer Container { get; set; }

        public StructureMapLogisticLineGroupGenerationRule(ISwitchProvider switchProvider, ILogisticLineGroupGenerator logisticLineGroupGenerator)
            : base(switchProvider, logisticLineGroupGenerator)
        {
        }

        private void EnsureTypeIsILogisticLineGroupUnfolderFactory(Type type)
        {
            if (!typeof(ILogisticLineGroupUnfolder).IsAssignableFrom(type))
            {
                throw new ArgumentException("Type should be ILogisticLineGroupUnfolder");
            }
        }

        protected override ILogisticLineGroupUnfolderFactory GetUnfolderFactory(Type type)
        {
            EnsureTypeIsILogisticLineGroupUnfolderFactory(type);

            var factoryType = typeof(LogisticLineGroupUnfolderFactory<>).MakeGenericType(type);

            return Container.GetInstance(factoryType) as ILogisticLineGroupUnfolderFactory;
        }

        private void EnsureTypeIsILineGroupInitialSelectorFactory(Type type)
        {
            if (!typeof(ILineGroupInitialSelector).IsAssignableFrom(type))
            {
                throw new ArgumentException("Type should be ILineGroupInitialSelector");
            }
        }

        protected override ILineGroupInitialSelectorFactory GetInitialSelectorFactory(Type type)
        {
            EnsureTypeIsILineGroupInitialSelectorFactory(type);

            var factoryType = typeof(LineGroupInitialSelectorFactory<>).MakeGenericType(type);

            return Container.GetInstance(factoryType) as ILineGroupInitialSelectorFactory;
        }

        private void EnsureTypeIsILogisticLineGroupInitializerFactory(Type type)
        {
            if (!typeof(ILogisticLineGroupInitializer).IsAssignableFrom(type))
            {
                throw new ArgumentException("Type should be ILogisticLineGroupInitializer");
            }
        }

        protected override ILogisticLineGroupInitializerFactory GetInitializerFactory(Type type)
        {
            EnsureTypeIsILogisticLineGroupInitializerFactory(type);

            var factoryType = typeof(LogisticLineGroupInitializerFactory<>).MakeGenericType(type);

            return Container.GetInstance(factoryType) as ILogisticLineGroupInitializerFactory;
        }

        protected override IEnumerable<ILogisticLineGroupGeneratedHandler> GetLogisticLineGroupGeneratedHandlers(IEnumerable<Type> types)
        {
            return from type in types select Container.GetInstance(type) as ILogisticLineGroupGeneratedHandler;
        }
    }
}
