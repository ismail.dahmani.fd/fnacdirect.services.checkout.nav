using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FnacDirect.OrderPipe.Base.Model
{
    public class PricesOrderDetail
    {
        public decimal PriceDBEur { get; set; }
        public decimal PriceUserEur { get; set; }
        public decimal ShipPriceDBEur { get; set; }
        public decimal ShipPriceNoVATEur { get; set; }
        public decimal ShipPriceUserEur { get; set; }
        public decimal ShipPriceToPay { get; set; }
        public decimal WrapPriceDBEur { get; set; }
        public decimal WrapPriceUserEur { get; set; }
        public decimal WrapPriceNoVATEur { get; set; }
        public decimal EcoTaxEur { get; set; }
        public decimal PriceRealEur { get; set; }
    }
}
