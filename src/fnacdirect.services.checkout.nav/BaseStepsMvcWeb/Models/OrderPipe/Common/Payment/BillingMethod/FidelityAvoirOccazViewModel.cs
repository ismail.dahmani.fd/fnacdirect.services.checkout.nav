﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Common.Payment
{
    public class FidelityAvoirOccazViewModel : BillingMethodViewModel
    {
        public FidelityAvoirOccazViewModel()
        {
            Values = Enumerable.Empty<SelectListItem>();
        }

        public bool HasUniqueExpiration { get; set; }
        public bool ExpireInLessThanOneMonth { get; set; }

        public bool Active { get; set; }

        public string Used { get; set; }
        public string Remaining { get; set; }        

        public string Amount { get; set; }
        public string AmountToExpire { get; set; }

        public string Expiration { get; set; }

        public string MaxUsableAmount { get; set; }

        public IEnumerable<SelectListItem> Values { get; set; }
    }
}
