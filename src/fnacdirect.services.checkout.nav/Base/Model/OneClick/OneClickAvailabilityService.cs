using System;
using System.Collections.Generic;
using System.Linq;
using FnacDirect.Contracts.Online.IModel;
using FnacDirect.Contracts.Online.Model;
using FnacDirect.OfferService.IBusiness;
using FnacDirect.OrderPipe.Base.Business.OnePage;
using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.OrderPipe.Base.Model.Pop;
using FnacDirect.OrderPipe.Base.OneClick;
using FnacDirect.OrderPipe.Contracts.Model.OneClick;
using FnacDirect.OrderPipe.Contracts.Services.OneClick;
using FnacDirect.Technical.Framework.Utils;
using FnacDirect.Technical.Framework.Web.Mvc;

namespace FnacDirect.OrderPipe.Base.Business.OneClick
{
    public class OneClickAvailabilityService : IOneClickAvailabilityService, IComputeOneClickAvailability
    {
        private readonly IApplicationContext _applicationContext;
        private readonly ISiteManagerMultiStoreBusiness2 _siteManagerBusiness2;
        private readonly IOfferBusiness _offerBusiness;
        private readonly ISwitchProvider _switchProvider;
        private readonly IOnePageService _onePageService;

        private readonly RangeSet<int> _onePageExcludedLogisticTypes;
        private readonly RangeSet<int> _excludedTypes;
        private readonly RangeSet<int> _onePageExcludedTypes;

        public OneClickAvailabilityService(IApplicationContext applicationContext,
                                           ISwitchProvider switchProvider,
                                           ISiteManagerMultiStoreBusiness2 siteManagerBusiness2,
                                           IOfferBusinessProvider offerBusinessProvider,
                                           IOnePageService onePageService)
        {
            _applicationContext = applicationContext;

            _onePageExcludedLogisticTypes = RangeSet.Parse<int>(_applicationContext.GetAppSetting("orderpipe.pop.onepage.excludedlogistictypes"));
            _onePageExcludedTypes = RangeSet.Parse<int>(_applicationContext.GetAppSetting("orderpipe.pop.onepage.excludedtypes"));
            _excludedTypes = RangeSet.Parse<int>(_applicationContext.GetAppSetting("orderpipe.pop.oneclick.excludedtypes"));

            _switchProvider = switchProvider;
            _siteManagerBusiness2 = siteManagerBusiness2;
            _offerBusiness = offerBusinessProvider.GetOfferBusiness(_applicationContext);
            _onePageService = onePageService;
        }

        public bool IsEligible(IEnumerable<OneClickAvailabilityInput> input)
        {
            if (!_switchProvider.IsEnabled("orderpipe.pop.oneclick"))
            {
                return false;
            }

            var articles = new List<FnacDirect.Contracts.Online.Model.Article>();
            var siteContext = _applicationContext.GetSiteContext();
            var context = new FnacDirect.Contracts.Online.Model.Context(null, null, null) { SiteContext = siteContext };

            foreach (var item in input)
            {
                var articleReference = new ArticleReference
                {
                    PRID = item.ProductId,
                    Catalog = (ArticleCatalog)item.Referentiel
                };
                var article = _siteManagerBusiness2.GetArticle(context, articleReference, ArticleLoadingOption.None);
                if (article != null)
                {
                    articles.Add(article);
                }
            }

            if (articles.Any(a => _onePageExcludedTypes.Contains(a.Core.Type.ID)))
            {
                return false;
            }

            if (articles.Any(a => _excludedTypes.Contains(a.Core.Type.ID)))
            {
                return false;
            }

            return HasOnlyOneSeller(input, articles);
        }

        private bool HasOnlyOneSeller(IEnumerable<OneClickAvailabilityInput> input, IEnumerable<FnacDirect.Contracts.Online.Model.Article> articles)
        {
            var notLogisticArticles = articles.Select(a => !_onePageExcludedLogisticTypes.Contains(a.ExtendedProperties.LogisticID));

            //Only MP sellers
            if (notLogisticArticles.Any()
                && input.All(i => i.OfferReference.HasValue && i.OfferReference.Value != Guid.Empty))
            {
                return input.Select(i => _offerBusiness.GetOfferByRef(i.OfferReference.Value).Seller.SellerId).Distinct().Count() == 1;
            }

            //Fnac and MP sellers
            if (notLogisticArticles.Any()
                && input.Any(i => i.OfferReference.HasValue && i.OfferReference.Value != Guid.Empty))
            {
                return false;
            }

            //Only Fnac
            return true;
        }

        public StepReturn ComputeAvailability(PopOrderForm orderForm)
        {
            var inputs = new List<OneClickAvailabilityInput>();

            var oneClickData = orderForm.GetPrivateData<OneClickData>(create: true);
            var popData = orderForm.GetPrivateData<PopData>(create: false);

            var isClickAndCollectExpress = orderForm.ExecutionContext.IsIn(Constants.ContextKeys.ClickAndCollectExpress);

            if (isClickAndCollectExpress)
            {
                oneClickData.IsAvailable = _onePageService.IsEligible(orderForm,
                                                                      orderForm.ShippingMethodEvaluation.ExcludedLogisticTypes,
                                                                      popData != null && popData.UseReact);

                return StepReturn.Next;
            }

            foreach (var article in orderForm.LineGroups.GetArticles())
            {
                var input = new OneClickAvailabilityInput
                {
                    Referentiel = article.Referentiel,
                    ProductId = article.ProductID.GetValueOrDefault()
                };

                if (article is MarketPlaceArticle)
                {
                    input.OfferReference = (article as MarketPlaceArticle).Offer.Reference;
                }

                inputs.Add(input);
            }

            oneClickData.IsAvailable = IsEligible(inputs);

            return StepReturn.Next;
        }
    }
}
