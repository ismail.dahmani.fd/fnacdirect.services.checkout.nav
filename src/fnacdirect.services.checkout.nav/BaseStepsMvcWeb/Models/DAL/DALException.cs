using System;
using FnacDirect.Technical.Framework.CoreServices;
using System.Diagnostics;

namespace FnacDirect.OrderPipe.Base.DAL
{
    public enum DALRange
    {
        Catalog = 55100,
        VGC = 55140,
        Commerce = 55150,
        ShoppingCart = 55190,
        Customer = 55200,
        MarketPlaceRef = 55300,
        MarketPlace = 55400,
        Adherent = 55600,
        Stock = 55700,
        Quote=55900,
        Agenda = 55950
    }

    public class DALException: Exception
    {

        private DALException(string message, Exception e):base(message,e)
        {
        }

        public static DALException BuildException(Exception e, DALRange range, int subcode)
        {
            var trace=new StackTrace(1);
            var dal = trace.GetFrame(0).GetMethod().ReflectedType.Name;
            var method = trace.GetFrame(0).GetMethod().Name;
            var code = (int)range + subcode;
            var d = new DALException(
                string.Format("[{0:00000}] {1}.{2} : {3}",code,dal,method,e.Message) , e);
            d.Data["DAL"] = dal;
            d.Data["METHOD"] = method;
            d.Data["CODE"] = code;
            Logging.Current.WriteError(d, code);
            return d;
        }
    }
}
