using System;
using System.Xml.Serialization;

namespace FnacDirect.OrderPipe.Base.Model
{
    public class BillingMethodDescriptor
    {
        public int Id;
        public string BillingMethodType;
        public string Name;
        public Type Type;
        public int Priority;
        public string Label;
        public bool IsActive;
    }

    public abstract class BillingMethod: IComparable<BillingMethod>
    {
        [XmlIgnore]
        private int _billingMethodId;

        public int BillingMethodId
        {
            get
            {
                return _billingMethodId;
            }
            set
            {
                _billingMethodId = value;
            }
        }

        public virtual string BillingMethodType
        {
            get;
        }

        [XmlIgnore]
        private string _name;

        public string Name
        {
            get
            {
                return _name;
            }
            set
            {
                _name = value;
            }
        }

        [XmlIgnore]
        private int _billingMethodFk;

        public int BillingMethodFk
        {
            get
            {
                return _billingMethodFk;
            }
            set
            {
                _billingMethodFk = value;
            }
        }
        [XmlIgnore]
        private string _UserGUID;

        public string UserGUID
        {
            get
            {
                return _UserGUID;
            }
            set
            {
                _UserGUID = value;
            }
        }
        [XmlIgnore]
        private decimal? _amount;

        public decimal? Amount
        {
            get
            {
                return _amount;
            }
            set
            {
                _amount = value;
            }
        }

        public virtual bool IsPrimary
        {
            get
            {
                return false;
            }
        }

        [XmlIgnore]
        private Guid _guid = Guid.NewGuid();

        [XmlIgnore]
        public Guid Guid
        {
            get { return _guid; }
            set { _guid = value; }
        }

        [XmlIgnore]
        private int? _initialStatus;

        public int? InitialStatus
        {
            get { return _initialStatus; }
            set { _initialStatus = value; }
        }

        [XmlIgnore]
        private int? _extensionId;

        [XmlIgnore]
        public int? ExtensionId
        {
            get { return _extensionId; }
            set { _extensionId = value; }
        }

        [XmlIgnore]
        public int Priority { get; set; }

        [XmlIgnore]
        public bool CanModifyAmountUp { get; set; }

        [XmlIgnore]
        public bool CanModifyAmountDown { get; set; }

        [XmlIgnore]
        public bool UseMonetique { get; set; }

        protected BillingMethod()
        {
            ImplyValidateOrder = false;
            UseMonetique = false;
            Priority = 0;
            CanModifyAmountUp = false;
            CanModifyAmountDown = false;
        }

        virtual public bool UseTransaction
        {
            get
            {
                return false;
            }
        }

        virtual public bool IsGlobalPayment
        {
            get
            {
                return false;
            }
        }

        [XmlIgnore]
        public bool ImplyValidateOrder { get; set; }

        public virtual BillingMethodDescriptor GetBillingMethodDescriptor()
        {
            return new BillingMethodDescriptor()
            {
                Id = _billingMethodId,
                Name = _name,
                Type = GetType()
            };
        }

        [XmlIgnore]
        public virtual bool ShouldBePersistent
        {
            get
            {
                return true;
            }
        }

        #region IComparable<BillingMethod> Members

        public int CompareTo(BillingMethod other)
        {
            return other.Priority.CompareTo(Priority);
        }

        #endregion
    }
}
