using FnacDirect.OrderPipe.Base.Model;
using System.Linq;
using FnacDirect.OrderPipe.Base.Model.Facets;
using FnacDirect.OrderPipe.Base.Model.Pop;
using FnacDirect.OrderPipe.Core.Steps;
using System.Collections.Generic;
using System;
using System.Xml.Serialization;
using FnacDirect.OrderPipe.Base.Business;

namespace FnacDirect.OrderPipe.Base.Steps.Pop
{
    public class PreparePopShippingAddressData : MultiFacetStep<IGotLineGroups, IGotShippingMethodEvaluation>
    {
        private readonly IAddressValidationBusiness _addressValidationBusiness;

        public PreparePopShippingAddressData(IAddressValidationBusiness addressValidationBusiness)
        {
            _addressValidationBusiness = addressValidationBusiness;
        }

        protected override StepReturn Process(MultiFacet<IGotLineGroups, IGotShippingMethodEvaluation> orderFormMultiFacets)
        {
            var popShippingAddressData = GetGroupData<PopShippingData>(orderFormMultiFacets);

            var lineGroups = orderFormMultiFacets.Facet1.LineGroups;
            var shippingMethodEvaluation = orderFormMultiFacets.Facet2.ShippingMethodEvaluation;

            foreach (var lineGroup in lineGroups.OrderBy(l => l.Seller.SellerId))
            {
                var billingAddress = orderFormMultiFacets.Facet2.BillingAddress;

                if (billingAddress != null
                    && !billingAddress.IsDummy()
                    && _addressValidationBusiness.IsAddressValid(billingAddress.Convert()))
                {
                    if (lineGroup.Articles.All(a => a.LogType.HasValue && shippingMethodEvaluation.ExcludedLogisticTypes.Contains(a.LogType.Value)))
                    {
                        continue;
                    }
                }

                popShippingAddressData.TryCreateSeller(lineGroup.Seller.SellerId);
            }

            foreach (var sellerId in popShippingAddressData.Sellers.Select(s => s.SellerId).ToList())
            {
                if (!lineGroups.Any(l => l.Seller.SellerId == sellerId))
                {
                    popShippingAddressData.RemoveSeller(sellerId);
                }
            }

            return StepReturn.Next;
        }
    }

    public class PopShippingData : GroupData
    {
        public List<PopShippingSellerData> Sellers { get; set; }

        public PopShippingData()
        {
            Sellers = new List<PopShippingSellerData>();
        }

        public void TryCreateSeller(int sellerId)
        {
            if (!Sellers.Any(s => s.SellerId == sellerId))
            {
                var popShippingSellerData = new PopShippingSellerData { SellerId = sellerId };

                Sellers.Add(popShippingSellerData);

                Sellers = Sellers.OrderBy(s => s.SellerId).ToList();
            }
        }

        public void RemoveSeller(int sellerId)
        {
            Sellers.RemoveAll(s => s.SellerId == sellerId);
        }

        public PopShippingSellerData GetCurrentShippingSellerData()
        {
            foreach (var shippingSellerData in Sellers)
            {
                if (shippingSellerData.Forced || !shippingSellerData.UserValidated)
                {
                    return shippingSellerData;
                }
            }

            return null;
        }

        public bool HasManyEligibleBillingAddresses { get; set; }
    }

    public class PopShippingSellerData
    {
        public int SellerId { get; set; }
        public bool UserValidated { get; set; }
        [XmlIgnore]
        public bool UserValidating { get; set; }
        public bool ShippingChoiceValidated { get; set; }
        public bool Forced { get; set; }

        public override int GetHashCode()
        {
            return SellerId;
        }

        public override bool Equals(object obj)
        {
            return obj.GetHashCode() == GetHashCode();
        }
    }
}
