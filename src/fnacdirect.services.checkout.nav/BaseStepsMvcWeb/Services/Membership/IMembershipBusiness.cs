using FnacDirect.OrderPipe.Base.Model;
using System;

namespace FnacDirect.OrderPipe.Base.Proxy.Membership
{
    public interface IMembershipBusiness
    {
        void RegisterAccountToNewsletter(PopOrderForm popOrderForm, int accountId);

        void UpdateMembershipData(PopOrderForm popOrderForm,
            DateTime? membershipBirthdate = null,
            string adherentNieNifType = null,
            string adherentNieNifValue = null);
    }
}
