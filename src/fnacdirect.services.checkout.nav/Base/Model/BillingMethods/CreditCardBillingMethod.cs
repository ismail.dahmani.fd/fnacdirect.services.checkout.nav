using System.Collections.Generic;
using System.Xml.Serialization;

namespace FnacDirect.OrderPipe.Base.Model
{
    public class CreditCardBillingMethod : BillingMethod
    {

        public const int IdBillingMethod = 1;

        public static string BillingMethodName
        {
            get
            {
                return "CreditCardBillingMethod";
            }
        }

        public override bool IsPrimary
        {
            get
            {
                return true;
            }
        }

        [XmlIgnore]
        private int? _Identity;

        public int? Identity
        {
            get
            {
                return _Identity;
            }
            set
            {
                _Identity = value;
            }
        }



        [XmlIgnore]
        private string _Reference;

        public string Reference
        {
            get
            {
                return _Reference;
            }
            set
            {
                _Reference = value;
            }
        }

        [XmlIgnore]
        private string _CreditBareme;

        public string CreditBareme
        {
            get
            {
                return _CreditBareme;
            }
            set
            {
                _CreditBareme = value;
            }
        }

        [XmlIgnore]
        private string _Alias;

        public string Alias
        {
            get
            {
                return _Alias;
            }
            set
            {
                _Alias = value;
            }
        }


        private int? _TypeId;

        public int? TypeId
        {
            get
            {
                return _TypeId;
            }
            set
            {
                _TypeId = value;
            }
        }


        [XmlIgnore]
        private string _CardTypeSIPSString;

        public string CardTypeSIPSString
        {
            get
            {
                return _CardTypeSIPSString;
            }
            set
            {
                _CardTypeSIPSString = value;
            }
        }

        [XmlIgnore]
        private string _TypeLabel;

        public string TypeLabel
        {
            get
            {
                return _TypeLabel;
            }
            set
            {
                _TypeLabel = value;
            }
        }


        private string _CardNumber;


        public string CardNumber
        {
            get
            {
                return _CardNumber;
            }
            set
            {
                _CardNumber = value;
            }
        }


        private string _CardHolder;


        public string CardHolder
        {
            get
            {
                return _CardHolder;
            }
            set
            {
                _CardHolder = value;
            }
        }
        


        private string _CardExpirationDate;


        public string CardExpirationDate
        {
            get
            {
                return _CardExpirationDate;
            }
            set
            {
                _CardExpirationDate = value;
            }
        }

        [XmlIgnore]
        private int? _Status;

        public int? Status
        {
            get
            {
                return _Status;
            }
            set
            {
                _Status = value;
            }
        }



        [XmlIgnore]
        private bool? _CryptoFlag;

        public bool? CryptoFlag
        {
            get
            {
                return _CryptoFlag;
            }
            set
            {
                _CryptoFlag = value;
            }
        }



        [XmlIgnore]
        private bool? _NumByPhone;

        public bool? NumByPhone
        {
            get
            {
                return _NumByPhone;
            }
            set
            {
                _NumByPhone = value;
            }
        }


        private string _VerificationCode;
        
        public string VerificationCode
        {
            get
            {
                return _VerificationCode;
            }
            set
            {
                _VerificationCode = value;
            }
        }

        [XmlIgnore]
        private string _baremeCredit;

        public string BaremeCredit
        {
            get
            {
                return _baremeCredit;
            }
            set
            {
                _baremeCredit = value;
            }
        }

        public override bool ShouldBePersistent
        {
            get
            {
                return false;
            }
        }

        public CreditCardBillingMethod()
        {
            BillingMethodId = IdBillingMethod;
            CanModifyAmountUp = true;
            CanModifyAmountDown = true;
            ImplyValidateOrder = true;
            Name = BillingMethodName;
        }
    }
}
