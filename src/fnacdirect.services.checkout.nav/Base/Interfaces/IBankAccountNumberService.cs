namespace FnacDirect.OrderPipe.Base.Proxy.Customer
{
    public interface IBankAccountNumberService
    {
        bool AddBankAccountNumber(string iban);
        string GetBankAccountNumber();
    }
}
