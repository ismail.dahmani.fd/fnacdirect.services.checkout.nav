﻿using System.Collections.Generic;
using System.Linq;
using FnacDirect.FnacStore.Model;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Common.Shipping
{
    public class TimetableViewModel
    {
        private readonly bool _isToday;
        private readonly string _dayOfWeek;
        private readonly string _openingPeriods;

        public TimetableViewModel(bool isToday, string dayOfWeek, IEnumerable<FnacOpeningPeriod> openingPeriods)
        {
            _isToday = isToday;
            _dayOfWeek = dayOfWeek;

            if (openingPeriods.Any())
            {
                _openingPeriods = string.Join(" ", openingPeriods.Where(o => o != null).Select(p => string.Format("{0} - {1}", p.Opening.ToString("hh\\:mm"), p.Closing.ToString("hh\\:mm"))).ToArray());
            }
            else
            {
                _openingPeriods = string.Empty;
            }
        }

        public bool IsToday
        {
            get { return _isToday; }
        }

        public string DayOfWeek
        {
            get { return _dayOfWeek; }
        }

        public string OpeningPeriods
        {
            get { return _openingPeriods; }
        }
    }
}
