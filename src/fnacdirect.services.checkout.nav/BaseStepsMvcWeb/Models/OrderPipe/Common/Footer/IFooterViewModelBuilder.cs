﻿using FnacDirect.OrderPipe.Base.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Common
{
    public interface IFooterViewModelBuilder
    {
        FooterViewModel Build(PopOrderForm popOrderForm);
    }
}
