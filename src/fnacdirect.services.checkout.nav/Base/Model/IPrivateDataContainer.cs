namespace FnacDirect.OrderPipe
{
    public interface IPrivateDataContainer
    {
        /// <summary>
        /// Retourne les données privées d'étape ou de groupe correspondant au nom indiqué
        /// </summary>
        /// <typeparam name="T">Type de la donnée</typeparam>
        /// <param name="dataName">Nom de la donnée</param>
        /// <param name="create">Vrai si la donnée est initialisée en cas de non-existance</param>
        /// <returns>La donnée. Si elle n'existe pas, elle est instanciée</returns>
        T GetPrivateData<T>(string dataName, bool create) where T : Data, new();

        /// <summary>
        /// Retourne les données privées d'étape ou de groupe correspondant au nom indiqué
        /// </summary>
        /// <typeparam name="T">Type de la donnée</typeparam>
        /// <param name="create">Vrai si la donnée est initialisée en cas de non-existance</param>
        T GetPrivateData<T>(bool create) where T : Data, new();
        
        /// <summary>
        /// Retourne les données privées d'étape ou de groupe correspondant au nom indiqué
        /// </summary>
        /// <typeparam name="T">Type de la donnée</typeparam>
        /// <param name="dataName">Nom de la donnée</param>
        /// <returns>La donnée. Si elle n'existe pas, elle est instanciée</returns>
        T GetPrivateData<T>(string dataName) where T : Data, new();

        T GetPrivateData<T>() where T : Data, new();

        /// <summary>
        /// Supprime les données privées en fonction du type spécifié
        /// </summary>
        /// <typeparam name="T">Type de la donnée</typeparam>
        void RemovePrivateData<T>() where T : Data, new();

        /// <summary>
        /// Supprime les données privées en fonction du type spécifié et du nom donné à la data
        /// </summary>
        /// <typeparam name="T">Type de la donnée</typeparam>
        /// <param name="name">Nom de la donnée</param>
        void RemovePrivateData<T>(string name) where T : Data, new();

        /// <summary>
        /// Supprime les données privées spécifiées en paramètre
        /// </summary>
        /// <typeparam name="T">Type de la donnée</typeparam>
        /// <param name="data">Donnée</param>
        void RemovePrivateData<T>(T data) where T : Data;
    }
}
