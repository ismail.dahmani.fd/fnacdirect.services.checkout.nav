﻿using System.Web.Mvc;
using FnacDirect.OrderPipe.CoreMvcWeb.Routing;
using FnacDirect.OrderPipe.CoreRouting;
using FnacDirect.Technical.Framework.Web.Routing;

namespace System.Web.Routing
{
    public static class OrderPipeCoreRouteCollectionExtensions
    {
        public static RouteCollection MapOrderPipeCore(this RouteCollection routeCollection)
        {
            var context = new AreaRegistrationContext("OP - Core", routeCollection);
            context.Namespaces.Add("FnacDirect.OrderPipe.Mvc.*");

            context.MapOrderPipeRoute(
                 name: "OP - GlobalEntryPoint",
                 url: "orderpipe/{pipe}",
                 defaults: new { },
                 constraints: new { isNotMappedToPhysicalFile = new IsNotMappedToPhysicalFile() });

            context.MapOrderPipeRoute(
                 name: "OP - Alternative GlobalEntryPoint",
                 url: "orderpipe",
                 defaults: new { },
                 constraints: new
                 {
                     isNotMappedToPhysicalFile = new IsNotMappedToPhysicalFile()
                 });

            var pipeAspxPages = new[]
            {
                "OrderPipe/default.aspx",
                "mobile/OrderPipe/default.aspx",
                "OrderPipe/genstep.aspx",
                "OrderPipe/genstepFDV.aspx",
                "OrderPipe/newgenstep.aspx",
                "OrderPipe/continue.aspx",
                "OrderPipe/Addresses/BillingAddress.aspx",
                "OrderPipe/ThankYou/ThankYou.aspx",
                "OrderPipe/ThankYou.aspx",
                "OrderPipe/Sorry/Sorry.aspx",
                "OrderPipe/MailHtml/mailhtml.aspx",
                "OrderPipe/Ogone/OrderPaymentTemplateHTML/PaymentHTMLTemplate.aspx"
            };

            foreach (var pipeAspxPage in pipeAspxPages)
            {
                context.MapOrderPipeRoute(
                    name: "OP - " + pipeAspxPage,
                    url: pipeAspxPage
                );
            }

            return routeCollection;
        }
    }
}
