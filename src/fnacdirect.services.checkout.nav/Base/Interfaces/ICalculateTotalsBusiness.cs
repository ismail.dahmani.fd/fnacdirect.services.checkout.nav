using FnacDirect.OrderPipe.Base.LineGroups;
using FnacDirect.OrderPipe.Base.Model.Facets;
using System.Collections.Generic;

namespace FnacDirect.OrderPipe.Base.Business
{
    public interface ICalculateTotalsBusiness
    {
        void CalculateSubTotals(ILogisticLineGroup logisticLineGroup);
        void CalculateTotals(IGotBillingInformations iGotBillingInformations, IEnumerable<ILogisticLineGroup> logisticLineGroups);
    }
}
