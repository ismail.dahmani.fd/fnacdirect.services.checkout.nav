﻿using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.OrderPipe.Base.Model.LineGroups;
using FnacDirect.OrderPipe.Shipping.Model;
using System;
using System.Collections.Generic;

namespace FnacDirect.OrderPipe.Base.Business.Shipping.Rules
{
    public interface IShippingMethodBasketBuildingRule
    {
        Func<ShippingMethodBasket, ShippingMethodBasket> GetModifier(IPrivateDataContainer privateDataContainer, IEnumerable<Article> articles,ShippingAddress shippingAddress);
    }
}
