using FnacDirect.OrderPipe.Base.Model;
using System.Collections.Generic;

namespace FnacDirect.OrderPipe.Base.Proxy.Ogone.PspId
{
    /// <summary>
    /// Contient les methodes relatives au PspId (dont la recuperation de cette donnee)
    /// </summary>
    public class DefaultPspIdService : IPspIdService
    {
        private readonly string _accountId;

        private readonly IOgoneConfigurationService _ogoneConfigurationService;

        public DefaultPspIdService(string accountId, IOgoneConfigurationService ogoneConfigurationService)
        {
            _accountId = accountId;
            _ogoneConfigurationService = ogoneConfigurationService;
        }

        public string ComputePspId(OrderTypeEnum orderType, IEnumerable<Article> articles, OgoneConfigurationData ogoneConfigurationData)
        {
            return _ogoneConfigurationService.ComputePspId(orderType, articles, _accountId, ogoneConfigurationData);
        }
    }
}

