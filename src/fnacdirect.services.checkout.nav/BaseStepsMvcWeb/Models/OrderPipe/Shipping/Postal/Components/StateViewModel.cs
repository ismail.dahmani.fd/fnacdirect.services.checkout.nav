namespace FnacDirect.OrderPipe.BaseMvc.Web.Models
{
    public class StateViewModel
    {
        public string Id { get; set; }
        public string Label { get; set; }
    }
}
