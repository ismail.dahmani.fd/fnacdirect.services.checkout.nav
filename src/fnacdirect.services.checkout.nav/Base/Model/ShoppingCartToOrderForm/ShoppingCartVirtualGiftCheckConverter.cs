﻿using FnacDirect.OrderPipe.Base.LineGroups;
using FnacDirect.OrderPipe.Base.Model;
using TFrom = FnacDirect.ShoppingCartEntities.VirtualGiftCheck;
using TTo = FnacDirect.OrderPipe.Base.Model.VirtualGiftCheck;

namespace FnacDirect.OrderPipe.Base.Business.ArticleConversions.ShoppingCartToOrderForm
{
    public partial class ShoppingCartToOrderFormArticleConverter
    {
        protected TTo Convert(TFrom @from)
        {
            return new TTo
            {
                Amount = @from.Amount,
                OriginCode = @from.OriginCode,
                Format = @from.Format,
                FromEmail = @from.FromEmail,
                FromName = @from.FromName,
                LogType = 1,
                Message = @from.Message,
                Quantity = @from.Quantity,
                ProductID = @from.ProductID,
                Type = ArticleType.Saleable,
                SellerID = SellerIds.GetId(SellerType.VirtualGiftCheck)
            };
        }
    }
}
