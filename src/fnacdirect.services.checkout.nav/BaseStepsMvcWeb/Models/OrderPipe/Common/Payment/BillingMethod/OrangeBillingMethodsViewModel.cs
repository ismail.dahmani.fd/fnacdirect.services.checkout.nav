using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Common.Payment
{
    public class OrangeBillingMethodsViewModel : BillingMethodViewModel
    {
        public string OrangeAuthApiUrl { get; set; }
        public bool IsAssociated { get; set; }
    }
}
