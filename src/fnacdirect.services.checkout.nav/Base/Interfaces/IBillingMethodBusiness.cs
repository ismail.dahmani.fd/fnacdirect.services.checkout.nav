﻿using FnacDirect.OrderPipe.Base.LineGroups;
using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.OrderPipe.Base.Model.Facets;
using System.Collections.Generic;

namespace FnacDirect.OrderPipe.Base.Business
{
    public interface IBillingMethodBusiness
    {
        ILogisticLineGroup GetHighAmountLineGroup(IEnumerable<ILogisticLineGroup> lineGroups);
        OgoneCreditCardBillingMethod GetOgoneBillingMethod(IGotBillingInformations gotBillingInformations);
        T GetBillingMethod<T>(IGotBillingInformations orderForm, int idBillingMethod) where T : BillingMethod;
        T GetBillingMethod<T>(IGotBillingInformations orderForm) where T : BillingMethod;
    }
}
