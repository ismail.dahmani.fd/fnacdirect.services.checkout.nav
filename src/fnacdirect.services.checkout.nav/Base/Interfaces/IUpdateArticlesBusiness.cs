﻿using System.Collections.Generic;
using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.OrderPipe.Base.Model.LineGroups;

namespace FnacDirect.OrderPipe.Base.Business
{
    public interface IUpdateArticlesBusiness
    {
        bool ChangeArticleQuantity(LineGroup lg, Article art, int newQty);

        /// <summary>
        /// Change la quantité de l'article au niveau d'une collection d'articles
        /// </summary>
        /// <param name="articles">Une collection d'articles</param>
        /// <param name="article">L'article à modifier</param>
        /// <param name="newQuantity">La nouvelle quantité</param>
        /// <returns></returns>
        bool ChangeArticleQuantity(IEnumerable<Article> articles, Article article, int newQuantity);
    }
}
