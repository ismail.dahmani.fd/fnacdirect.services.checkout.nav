using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Shipping;
using System;
using System.Web.Routing;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Pop.Models.Shipping
{
    public interface IShippingPageRootViewModelBuilder
    {
        RootViewModel GetRootViewModel(PopOrderForm popOrderForm, int sellerId, Func<string, RouteValueDictionary, string> getUrl);
    }
}
