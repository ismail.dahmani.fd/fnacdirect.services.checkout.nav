namespace FnacDirect.OrderPipe
{
    public enum PipeMessageBehavior
    {
        AutoClear,
        Persist,
        ForcePersist
    }
}
