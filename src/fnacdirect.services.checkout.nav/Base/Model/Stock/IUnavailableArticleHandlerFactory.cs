﻿using FnacDirect.OrderPipe.Base.Model.Facets;

namespace FnacDirect.OrderPipe.Base.Business.Stock
{
    public interface IUnavailableArticleHandlerFactory
    {
        IUnavailableArticleHandler Get(IGotLineGroups iGotLineGroups);
    }
}
