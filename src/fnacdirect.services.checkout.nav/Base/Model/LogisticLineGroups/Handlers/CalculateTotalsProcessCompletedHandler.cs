﻿using System;
using System.Collections.Generic;
using FnacDirect.OrderPipe.Base.LineGroups;
using FnacDirect.OrderPipe.Base.Business.LogisticLineGroups.Internals.Handlers;

namespace FnacDirect.OrderPipe.Base.Business.LogisticLineGroups.Handlers
{
    public class CalculateTotalsProcessCompletedHandler : ILogisticLineGroupProcessCompletedHandler
    {
        private ICalculateTotalsBusiness _calculateTotalsBusiness;

        public CalculateTotalsProcessCompletedHandler(ICalculateTotalsBusiness calculatedtotalsBusiness)
        {
            _calculateTotalsBusiness = calculatedtotalsBusiness;
        }
        public void OnGenerated(ILogisticLineGroupGenerationProcessContext logisticLineGroupGenerationProcessContext, IEnumerable<ILogisticLineGroup> logisticLineGroups)
        {
            _calculateTotalsBusiness.CalculateTotals(logisticLineGroupGenerationProcessContext.IGotBillingInformations, logisticLineGroups);
        }
    }
}
