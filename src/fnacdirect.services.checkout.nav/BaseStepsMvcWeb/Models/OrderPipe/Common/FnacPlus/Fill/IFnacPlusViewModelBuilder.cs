﻿using FnacDirect.OrderPipe.Base.BaseModel.Model;
using System;
namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Common.Basket
{
    public interface IFnacPlusViewModelBuilder
    {
        void Fill(IGotFnacPlusData viewModel, PushFnacPlusAdherentData sourceData);
    }
}
