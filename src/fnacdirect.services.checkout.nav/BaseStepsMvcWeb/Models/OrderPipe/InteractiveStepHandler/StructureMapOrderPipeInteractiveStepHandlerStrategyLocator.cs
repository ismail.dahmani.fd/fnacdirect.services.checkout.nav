﻿using FnacDirect.OrderPipe.Config;
using StructureMap;

namespace FnacDirect.OrderPipe.CoreRouting
{
    public class StructureMapOrderPipeInteractiveStepHandlerStrategyLocator : IOrderPipeInteractiveStepHandlerStrategyLocator
    {
        private readonly IContainer _container;

        public StructureMapOrderPipeInteractiveStepHandlerStrategyLocator(IContainer container)
        {
            _container = container;
        }

        public IOrderPipeInteractiveStepHandlerStrategy GetOrderPipeInteractiveStepHandlerStrategyFor<TDescriptor>(TDescriptor descriptor)
            where TDescriptor : class, IUiDescriptor
        {
            var instanceType = descriptor.GetType();
            var serviceType = typeof (IOrderPipeInteractiveStepHandlerStrategy<>);
            var concreteServiceType = serviceType.MakeGenericType(instanceType);

            return _container.GetInstance(concreteServiceType) as IOrderPipeInteractiveStepHandlerStrategy;
        }
    }
}
