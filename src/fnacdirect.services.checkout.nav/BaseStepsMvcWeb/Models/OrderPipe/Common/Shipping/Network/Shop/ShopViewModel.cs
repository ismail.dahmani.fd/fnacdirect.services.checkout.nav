using FnacDirect.OrderPipe.Base.Business.Availability;
using FnacDirect.OrderPipe.Base.Business.Operations;
using System;
using System.Collections.Generic;
using System.Linq;
using FnacDirect.Technical.Framework.ServiceLocation;
using FnacDirect.Technical.Framework.Web.Mvc;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Common.Shipping.Shop
{
    public class ShopViewModel : ShopAddressViewModel
    {
        public ShopViewModel(FnacStore.Model.FnacStore fnacStore)
            : base(fnacStore)
        {
            ShortEstimatedDeliveryDate = new List<string>();
            LongEstimatedDeliveryDate = new List<PopAvailabilityProcessorOutput>();
        }

        public string ShippingCost { get; set; }

        public decimal? CarbonFootprint { get; set; }

        public bool IsFreeShipping { get; set; }

        public bool IsSelected { get; set; }

        public bool IsDefaultShippingShop { get; set; }

        public bool IsFavorite { get; set; }

        public bool FullClickAndCollect { get; set; }

        public bool AnyClickAndCollect { get; set; }

        public bool IsDrive { get; set; }

        public DateTime? WorstDeliveryDate { get; set; }

        public bool BeforeXmas
        {
            get
            {
                return LongEstimatedDeliveryDate.ComputeBeforeXmas();
            }
        }

        public string PictoLabel
        {
            get
            {
                return LongEstimatedDeliveryDate.GetPictoLabel();
            }
        }

        public bool IsOpen { get; set; }

        public IEnumerable<string> ShortEstimatedDeliveryDate { get; set; }

        public IEnumerable<PopAvailabilityProcessorOutput> LongEstimatedDeliveryDate { get; set; }

        public bool HasManyLongEstimatedDeliveryDate
        {
            get
            {
                return LongEstimatedDeliveryDate.Count() > 1;
            }
        }

        public bool DisplayLongEstimatedDeliveryDates { get; set; }

        public string DisplayDateOfDelivery1H { get; set; }

        public bool IsGreenDeliveryFeatureActive { private get; set; }

        public bool IsEcological {
            get
            {
                return IsGreenDeliveryFeatureActive
                    && AnyClickAndCollect;
            }
        }
    }
}
