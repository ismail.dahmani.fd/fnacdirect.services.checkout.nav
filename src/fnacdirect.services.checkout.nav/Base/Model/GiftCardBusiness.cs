using FnacDirect.Contracts.Online.Model;
using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.OrderPipe.Base.Model.Facets;
using FnacDirect.OrderPipe.Core.Services;
using FnacDirect.Partners.Kadeos.BLL;
using FnacDirect.Partners.Kadeos.Model;
using FnacDirect.Technical.Framework.CoreServices;
using FnacDirect.Technical.Framework.Switch;
using FnacDirect.Technical.Framework.Web;
using FnacDirect.Technical.Framework.Web.Http.Services;
using FnacDirect.Technical.Framework.Web.Mvc;
using FnacDirect.VirtualGiftCheck.BLL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace FnacDirect.OrderPipe.Base.Business
{
    public class GiftCardBusiness : IGiftCardBusiness
    {
        private readonly IApplicationContext _applicationContext;
        private readonly ISwitchProvider _switchProvider;
        private readonly IKadeosBusiness _kadeosBusiness;
        private readonly ILogger _logger;
        private readonly int _maxAllowedAmountGiftCard;
        private readonly string _regexMaxAmountRules;
        private const string regexMaxAmountKey = "op.giftcard.dsp2.regex.maxamount";

        public GiftCardBusiness(IApplicationContext applicationContext,
                                ISwitchProvider switchProvider,
                                IKadeosBusiness kadeosBusiness,
                                IPipeParametersProvider pipeParametersProvider,
                                ILogger logger)
        {
            _applicationContext = applicationContext;
            _switchProvider = switchProvider;
            _kadeosBusiness = kadeosBusiness;
            _logger = logger;
            _maxAllowedAmountGiftCard = pipeParametersProvider.Get("giftcard.constraint.clickandcollect.maxamount", int.MaxValue);
            _regexMaxAmountRules = pipeParametersProvider.Get(regexMaxAmountKey, string.Empty);
        }

        private Dictionary<Regex, decimal> BuildFromPipeParameter(string paramValue)
        {
            var dictionary = new Dictionary<Regex, decimal>();

            if (paramValue.IsNullOrEmpty())
                return dictionary;

            var parts = (paramValue ?? string.Empty).Split('~');
            if (parts.Length % 2 != 0)
            {
                _logger.WriteError($"GiftCardBusiness: bad parameter on property {regexMaxAmountKey}: {paramValue}");
                return dictionary;
            }
            for (var i = 0; i < parts.Length; i += 2)
            {
                var regex = new Regex(parts[i]);
                if (!decimal.TryParse(parts[i + 1], out var maxAmount))
                {
                    _logger.WriteError($"GiftCardBusiness: bad max amount on property {regexMaxAmountKey}: regex={parts[i]} maxamount={parts[i + 1]}");
                    break;
                }
                dictionary[regex] = maxAmount;
            }
            return dictionary;
        }

        private (decimal maxAmount, string userMessageKey) GetCardMaxUsableAmountForOrder(KadeosCard kadeosCard, IGotBillingInformations billingInfo, IGotLineGroups lineGroups)
        {
            var maxAmountsMessages = new List<(decimal maxAmount, string userMessageKey)>
            {
                (kadeosCard.MaxAmount, string.Empty)
            };

            if (GetPerCardSpecificLimit(kadeosCard.Number, out var cardSpecificLimit))
                maxAmountsMessages.Add((cardSpecificLimit, "op.gift.errmsg.giftcard.dsp2.maxamount"));

            var orderRemainingAmountToPay = billingInfo.GlobalPrices.TotalPriceEur.Value;
            foreach (var m in billingInfo.OrderBillingMethods)
                orderRemainingAmountToPay -= m.Amount.GetValueOrDefault();
            maxAmountsMessages.Add((orderRemainingAmountToPay, string.Empty));

            if (_switchProvider.IsEnabled("giftcard.constraint.clickandcollect.maxamount.enabled") && lineGroups.HasClickAndCollectArticle())
            {
                var giftCardOrderAmountAlreadyUsed = billingInfo.OrderBillingMethods?.OfType<GiftCardBillingMethod>().Sum(x => x.Amount.Value) ?? 0;

                var ccMaxAllowedGiftcardAmount = Math.Max(0, _maxAllowedAmountGiftCard - giftCardOrderAmountAlreadyUsed);

                maxAmountsMessages.Add((ccMaxAllowedGiftcardAmount, string.Empty));
            }

            return maxAmountsMessages.OrderBy(t => t.maxAmount).ThenBy(t => t.userMessageKey).First();
        }

        private bool GetPerCardSpecificLimit(string cardNumber, out decimal maxAmount)
        {
            var giftcardMaxAmountRules = BuildFromPipeParameter(_regexMaxAmountRules);

            foreach (var item in giftcardMaxAmountRules)
            {
                if (item.Key.IsMatch(cardNumber))
                {
                    maxAmount = item.Value;
                    return true;
                }
            }

            maxAmount = 0;
            return false;
        }

        public bool AddGiftCard(IOF iOF, string cardNumber, string pin)
        {
            var gotUserContextInformations = (IGotUserContextInformations)iOF;
            var gotBillingInformations = (IGotBillingInformations)iOF;
            var gotLineGroups = (IGotLineGroups)iOF;

            if (IsGiftcardAlreadyUsedOrTooMany(gotBillingInformations, cardNumber))
                return false;

            if (!GetKadeosCard(gotUserContextInformations, cardNumber, pin, out var kadeosCard))
                return false;

            var (maxAmount, userMessageKey) = GetCardMaxUsableAmountForOrder(kadeosCard, gotBillingInformations, gotLineGroups);
            if (!string.IsNullOrWhiteSpace(userMessageKey))
            {
                gotUserContextInformations.UserMessages.Add(
                    "giftcard",
                    UserMessage.MessageLevel.Warning,
                    userMessageKey,
                    maxAmount.ToString());
            }

            var gcbm = new GiftCardBillingMethod(kadeosCard, maxAmount)
            {
                Valid = true
            };

            gotBillingInformations.AddBillingMethod(gcbm);

            return true;
        }

        private static bool IsGiftcardAlreadyUsedOrTooMany(IGotBillingInformations gotBillingInformations, string cardNumber)
        {
            var count = 0;
            var doublon = false;

            foreach (var gc in CollectionUtils.FilterOnType<GiftCardBillingMethod>(gotBillingInformations.OrderBillingMethods))
            {
                if (gc.CardNumber == cardNumber)
                {
                    doublon = true;
                }
                count++;
            }

            if (count >= 100 || doublon)
                return true;

            return false;
        }

        private bool GetKadeosCard(IGotUserContextInformations gotUserContextInformations, string cardNumber, string pin, out KadeosCard kadeosCard)
        {
            var userMessages = gotUserContextInformations.UserMessages;
            var customerEntity = gotUserContextInformations.UserContextInformations.CustomerEntity;
            var ccvacc = new Account(customerEntity);
            kadeosCard = null;

            if (ccvacc.IsBlackListed)
            {
                userMessages.Add("giftcard", UserMessage.MessageLevel.Error, "OP.GIFT.ErrMsg.invalid");
                return false;
            }

            kadeosCard = _kadeosBusiness.Get(customerEntity.Identity, customerEntity.LtName, customerEntity.FirstName, cardNumber, pin);

            if (kadeosCard.ErrorCode == -100)
            {
                ccvacc.RegisterFailure();
                gotUserContextInformations.UserMessages.Add("giftcard", UserMessage.MessageLevel.Error, "OP.GIFT.ErrMsg.invalid");
                return false;
            }

            if (kadeosCard.ErrorCode == -200)
            {
                gotUserContextInformations.UserMessages.Add("giftcard", UserMessage.MessageLevel.Error, "OP.GIFT.ErrMsg.technical");
                return false;
            }

            if (kadeosCard.ErrorCode == 0 && !Switch.IsEnabled("op.gifcard.bestcadeau", false) && kadeosCard.Type == KadeosCardType.BEST)
            {
                ccvacc.RegisterFailure();
                gotUserContextInformations.UserMessages.Add("giftcard", UserMessage.MessageLevel.Error, "OP.GIFT.ErrMsg.invalid");
                return false;
            }

            if (kadeosCard.ErrorCode == 0 && !Switch.IsEnabled("op.gifcard.illicado", false) && kadeosCard.Type == KadeosCardType.ILLI)
            {
                ccvacc.RegisterFailure();
                gotUserContextInformations.UserMessages.Add("giftcard", UserMessage.MessageLevel.Error, "OP.GIFT.ErrMsg.invalid");
                return false;
            }

            //1 Day removed form the expiration date to allow usage of the card for 1 more day (Must expire at 23:59:59)
            if (kadeosCard.ExpirationDate.Date <= DateTime.Now.AddDays(-1).Date)
            {
                ccvacc.RegisterFailure();
                gotUserContextInformations.UserMessages.Add("giftcard", UserMessage.MessageLevel.Error, "OP.GIFT.ErrMsg.invalid");
                return false;
            }

            return true;
        }

        public decimal? GetUsabledAmount(IOF iOF, decimal remainingAmount, decimal actualChoosenAmount)
        {
            var gotBillingInformations = (IGotBillingInformations)iOF;
            var gotLineGroups = (IGotLineGroups)iOF;

            //If is C&C and MaxAmount Enabled
            if (_switchProvider.IsEnabled("giftcard.constraint.clickandcollect.maxamount.enabled")
                && gotLineGroups.HasClickAndCollectArticle())
            {
                //Total Amount GiftCard
                var totalAmountGiftCard = gotBillingInformations.OrderBillingMethods?.OfType<GiftCardBillingMethod>().Sum(x => x.Amount.Value);

                //give the usable amount of the gift card according to the max allowed amount giftCard
                if (totalAmountGiftCard > 0 && totalAmountGiftCard < _maxAllowedAmountGiftCard)
                {
                    var usabledAmount = Math.Min(_maxAllowedAmountGiftCard, remainingAmount);
                    var predictAmount = usabledAmount + totalAmountGiftCard.Value;
                    if (predictAmount >= _maxAllowedAmountGiftCard)
                        usabledAmount = _maxAllowedAmountGiftCard - totalAmountGiftCard.Value;

                    return usabledAmount;
                }
                //exceeds max allowed amount giftCard
                if (totalAmountGiftCard >= _maxAllowedAmountGiftCard)
                    return 0;
            }

            return remainingAmount <= actualChoosenAmount ? remainingAmount : actualChoosenAmount;
        }

        public bool GreaterThanMaximumAllowedAmount(IOF iOF, string cardNumber, decimal newAmount)
        {
            var gotBillingInformations = (IGotBillingInformations)iOF;
            var gotLineGroups = (IGotLineGroups)iOF;

            //If is C&C and MaxAmount Enabled
            if (_switchProvider.IsEnabled("giftcard.constraint.clickandcollect.maxamount.enabled")
                && gotLineGroups.HasClickAndCollectArticle())
            {
                decimal totalAmountGiftCard = 0;
                foreach (var gc in gotBillingInformations.OrderBillingMethods.OfType<GiftCardBillingMethod>())
                {
                    //added the new value in total to check the max allowed
                    if (gc.CardNumber == cardNumber)
                        totalAmountGiftCard += newAmount;
                    else
                        totalAmountGiftCard += gc.Amount.Value;

                    //exceeds max allowed amount giftCard
                    if (totalAmountGiftCard > _maxAllowedAmountGiftCard)
                    {
                        return true;
                    }
                }
            }

            if (GetPerCardSpecificLimit(cardNumber, out var maxAmount) && newAmount > maxAmount)
            {
                iOF.UserMessages.Add("giftcard", UserMessage.MessageLevel.Error, "op.gift.errmsg.giftcard.dsp2.maxamount", maxAmount.ToString());
                return true;
            }

            return false;
        }

        public bool RemoveGiftCard(IGotBillingInformations gotBillingInformations, string number)
        {
            gotBillingInformations.RemoveBillingMethod(x => x is GiftCardBillingMethod && ((GiftCardBillingMethod)x).CardNumber == number);
            return true;
        }

        public bool ModifyGiftCard(IGotBillingInformations gotBillingInformations, string p, decimal val)
        {
            foreach (var gc in gotBillingInformations.OrderBillingMethods.OfType<GiftCardBillingMethod>())
            {
                if (gc.CardNumber == p)
                {
                    if (val > gc.MaxAmount) val = gc.MaxAmount.Value;

                    gc.ChoosenAmount = val;
                    gc.Amount = val;
                    return true;
                }
            }
            return false;
        }

        public bool TransformGiftCard(IGotUserContextInformations gotUserContextInformations, IGotContextInformations gotContextInformations, GiftCardBillingMethod m, ref VirtualGiftCheckBillingMethod vgcbm)
        {
            var ccvacc = new Account(gotUserContextInformations.UserContextInformations.CustomerEntity);

            if (ccvacc.IsBlackListed)
            {
                gotUserContextInformations.UserMessages.Add("giftcard", UserMessage.MessageLevel.Error, "OP.GIFT.ErrMsg.invalid");
                return false;
            }

            if (GetPerCardSpecificLimit(m.CardNumber, out var maxAmount) && m.Amount.Value > maxAmount)
            {
                gotUserContextInformations.UserMessages.Add("giftcard", UserMessage.MessageLevel.Error, "op.gift.errmsg.giftcard.dsp2.maxamount", maxAmount);
                return false;
            }

            var card = new KadeosCard
            {
                Number = m.CardNumber,
                PinCode = m.PinCode,
                Type = (KadeosCardType)Enum.Parse(typeof(KadeosCardType), m.CardType)
            };
            var customerEntity = gotUserContextInformations.UserContextInformations.CustomerEntity;
            var ret = _kadeosBusiness.Debit(customerEntity.Identity, customerEntity.LtName, customerEntity.FirstName, card, m.Amount.Value);

            if (ret.ErrorCode == -100)
            {
                ccvacc.RegisterFailure();
                gotUserContextInformations.UserMessages.Add("giftcard", UserMessage.MessageLevel.Error, "OP.GIFT.ErrMsg.invalid");
                return false;
            }

            if (ret.ErrorCode == -200)
            {
                gotUserContextInformations.UserMessages.Add("giftcard", UserMessage.MessageLevel.Error, "OP.GIFT.ErrMsg.technical");
                return false;
            }

            if (ret.ErrorCode == 115)
            {
                gotUserContextInformations.UserMessages.Add("giftcard", UserMessage.MessageLevel.Error, "op.gift.errmsg.giftcard.dsp2.maxamount", maxAmount);
                return false;
            }

            try
            {
                ccvacc.CreateVGC(ret, m.Amount.Value, gotContextInformations.OrderContextInformations.Culture);

            }
            catch (Exception e)
            {
                Logging.Current.WriteError(MessageHelper.Get(FnacContext.Current.SiteContext, "OrderPipe.Base.Business.GiftCardBusiness.ErrorKadeosCard", card.Number, card.PinCode, gotUserContextInformations.UserContextInformations.ContactEmail, gotUserContextInformations.UserContextInformations.AccountId), e);
                throw new Exception(MessageHelper.Get(FnacContext.Current.SiteContext, "OrderPipe.Base.Business.GiftCardBusiness.ErrorKadeosCard", card.Number, card.PinCode, gotUserContextInformations.UserContextInformations.ContactEmail, gotUserContextInformations.UserContextInformations.AccountId), e);
            }

            if (vgcbm == null)
            {
                vgcbm = new VirtualGiftCheckBillingMethod();
            }

            if (!vgcbm.Amount.HasValue)
                vgcbm.Amount = 0.0m;

            if (!vgcbm.AmountEur.HasValue)
                vgcbm.AmountEur = 0.0m;

            vgcbm.Amount += m.Amount.Value;
            vgcbm.AmountEur += m.Amount.Value;
            vgcbm.UserGUID = gotUserContextInformations.UserContextInformations.UID;

            return true;
        }

        public List<GiftCardTypeValue> GetGiftCards()
        {
            var giftCardTypes = new List<GiftCardTypeValue>();

            AddGiftCard(giftCardTypes,
                "op.gifcard.gift",
                "orderpipe.Preview.CustomAdvantages.GiftCard.ddlstGiftCardType1",
                GiftCardType.Gift);

            AddGiftCard(giftCardTypes,
                "op.gifcard.fnac",
                "orderpipe.Preview.CustomAdvantages.GiftCard.ddlstGiftCardType2",
                GiftCardType.Fnac);

            AddGiftCard(giftCardTypes,
                "op.gifcard.darty",
                "orderpipe.Preview.CustomAdvantages.GiftCard.ddlstGiftCardType6",
                GiftCardType.Darty);

            AddGiftCard(giftCardTypes,
                "op.gifcard.kadeos",
                "orderpipe.Preview.CustomAdvantages.GiftCard.ddlstGiftCardType3",
                GiftCardType.Kadeos);

            AddGiftCard(giftCardTypes,
                "op.gifcard.bestcadeau",
                "orderpipe.Preview.CustomAdvantages.GiftCard.ddlstGiftCardType4",
                GiftCardType.Best);

            AddGiftCard(giftCardTypes,
                "op.gifcard.illicado",
                "orderpipe.Preview.CustomAdvantages.GiftCard.ddlstGiftCardType5",
                GiftCardType.Illicado);

            return giftCardTypes;
        }

        private void AddGiftCard(IList<GiftCardTypeValue> giftCardTypes, string switchKey, string labelResourceKey, GiftCardType giftCardType)
        {
            if (_switchProvider.IsEnabled(switchKey, false))
            {
                giftCardTypes.Add(new GiftCardTypeValue() { Label = MessageHelper.Get(_applicationContext.GetSiteContext(), labelResourceKey), Value = ((int)giftCardType).ToString(), Type = giftCardType });
            }
        }

        public static GiftCardType? GetGiftCardTypeFromEmitterName(string emitterName)
        {
            if (string.IsNullOrEmpty(emitterName))
            {
                return null;
            }

            switch (emitterName.ToUpperInvariant())
            {
                case "GIFT":
                    return GiftCardType.Gift;

                case "FNAC":
                    return GiftCardType.Fnac;

                case "DARTY":
                    return GiftCardType.Darty;

                case "KADEOS":
                    return GiftCardType.Kadeos;

                case "BEST":
                    return GiftCardType.Best;

                case "ILLICADO":
                    return GiftCardType.Illicado;

                default:
                    return null;
            }
        }
    }
}
