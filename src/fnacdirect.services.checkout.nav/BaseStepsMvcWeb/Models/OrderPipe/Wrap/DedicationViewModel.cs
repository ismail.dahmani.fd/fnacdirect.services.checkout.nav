using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.OrderPipe.Wrap
{
    public class DedicationViewModel
    {
        [StringLength(208)]
        public string Dedication { get; set; }

        [StringLength(18)]
        public string Signature { get; set; }
    }
}
