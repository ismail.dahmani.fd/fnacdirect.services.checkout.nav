﻿using FnacDirect.OrderPipe.Base.Business.LogisticLineGroups.Internals.Initializers;
using FnacDirect.OrderPipe.Base.LineGroups;
using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.Technical.Framework.Web.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FnacDirect.OrderPipe.Base.Business.LogisticLineGroups.Initializers
{
    public abstract class BaseSupplySourceLogisticLineGroupInitializer : BaseLogisticLineGroupInitializer
    {
        protected readonly IMarketPlaceService _marketPlaceService;
        protected readonly ISwitchProvider _switchProvider;

        protected int _reservationTimeoutInSecond;


        protected BaseSupplySourceLogisticLineGroupInitializer(IApplicationContext applicationContext, IMarketPlaceServiceProvider marketPlaceServiceProvider, ISwitchProvider switchProvider)
        {
            var siteContext = applicationContext.GetSiteContext();
            _marketPlaceService = marketPlaceServiceProvider.GetMarketPlaceService(siteContext);
            _switchProvider = switchProvider;
            _reservationTimeoutInSecond = 180;
        }

        protected MarketPlaceArticle ConvertToMarketPlaceArticle(StandardArticle standardArticle)
        {
            var marketPlaceArticle = new MarketPlaceArticle
            {
                OfferId = standardArticle.SupplySource.OfferId.Value,
                Quantity = standardArticle.Quantity
            };

            if (!_marketPlaceService.GetArticleDetail(marketPlaceArticle))
            {
                return null;
            }

            marketPlaceArticle.ShipMethod = standardArticle.ShipMethod;
            marketPlaceArticle.ShipPriceDBEur = standardArticle.ShipPriceDBEur;
            marketPlaceArticle.ShipPriceUserEur = standardArticle.ShipPriceUserEur;
            marketPlaceArticle.AvailabilityId = standardArticle.AvailabilityId;
            marketPlaceArticle.VatCountryId = standardArticle.VatCountryId;
            marketPlaceArticle.PriceDBEur = standardArticle.PriceDBEur + standardArticle.EcoTaxEur.GetValueOrDefault(0);
            marketPlaceArticle.PriceMemberEur = standardArticle.PriceMemberEur + standardArticle.EcoTaxEur.GetValueOrDefault(0);
            marketPlaceArticle.PriceNoVATEur = standardArticle.PriceUserEur + standardArticle.EcoTaxEur.GetValueOrDefault(0);
            marketPlaceArticle.PriceRealEur = standardArticle.PriceRealEur + standardArticle.EcoTaxEur.GetValueOrDefault(0);
            marketPlaceArticle.PriceUserEur = standardArticle.PriceUserEur + standardArticle.EcoTaxEur.GetValueOrDefault(0);
            marketPlaceArticle.LogType = standardArticle.LogType;
            marketPlaceArticle.VATRate = 0;
            marketPlaceArticle.OrderDetailPk = standardArticle.OrderDetailPk;
            marketPlaceArticle.Type = standardArticle.Type;

            marketPlaceArticle.Promotions = Helper.Clone(standardArticle.Promotions);
            for (var i = 0; i < standardArticle.Promotions.Count; i++)
            {
                marketPlaceArticle.Promotions[i] = Helper.Clone(standardArticle.Promotions[i]);
            }

            marketPlaceArticle.SalesInfo = Helper.Clone(standardArticle.SalesInfo);

            marketPlaceArticle.ShippingPromotionRules.AddRange(Helper.Clone(standardArticle.ShippingPromotionRules));
            
            // In case of local Preorder we must set the standard article value 
            if (marketPlaceArticle.AvailabilityId.HasValue && (marketPlaceArticle.AvailabilityId.Value == (int)AvailabilityEnum.PreOrder 
                || marketPlaceArticle.AvailabilityId.Value == (int)AvailabilityEnum.PreOrderBis))
            {
                marketPlaceArticle.Quantity = standardArticle.Quantity;
            }

            _marketPlaceService.AddReservation(marketPlaceArticle, _reservationTimeoutInSecond);

            return marketPlaceArticle;
        }
    }
}
