﻿
namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Membership
{
    public class MembershipFidelityAccountViewModel
    {
        public string AvailableAmount { get; set; }

        public string FirstExpiryAmount { get; set; }

        public string FirstExpiryDate { get; set; }
    }
}
