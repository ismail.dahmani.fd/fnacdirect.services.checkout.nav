using System;

namespace FnacDirect.OrderPipe.Base.Model
{
    public class BankCreditCardBillingMethod : BillingMethod
    {

        public static int IdBillingMethod
        {
            get
            {
                return 1;
            }
        }

        private DateTime transactionDate;

        public DateTime TransactionDate
        {
            get { return transactionDate; }
            set { transactionDate = value; }
        }

        private int transactionId;

        public int TransactionId
        {
            get { return transactionId; }
            set { transactionId = value; }
        }

        private string paymentMean;

        public string PaymentMean
        {
            get { return paymentMean; }
            set { paymentMean = value; }
        }

        public BankCreditCardBillingMethod()
        {
            BillingMethodId = SipsBillingMethod.IdBillingMethod;
            CanModifyAmountUp = true;
            CanModifyAmountDown = true;
            UseMonetique = true;
        }
    }
}
