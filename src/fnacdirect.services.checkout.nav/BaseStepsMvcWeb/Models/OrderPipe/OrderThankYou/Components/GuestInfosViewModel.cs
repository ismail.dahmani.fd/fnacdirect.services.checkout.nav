﻿namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.OrderThankYou
{
    public class GuestInfosViewModel
    {
        public bool IsGuest { get; set; }
        public string GuestOrderTrackingUrl { get; set; }
    }
}
