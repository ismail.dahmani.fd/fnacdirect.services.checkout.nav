﻿namespace FnacDirect.OrderPipe.Base.Model
{
    public enum DeferredPaymentInegibilityReason
    {
        None = -1,
        CustomerNotFound = 0,
        CallCenterOnly = 1,
        CustomerDeclined = 2,
        CustomerDeleted = 3,
        AmountTooLow = 4,
        AmountTooHigh = 5,
        CumulativeAmountTooHigh = 6
    }
}
