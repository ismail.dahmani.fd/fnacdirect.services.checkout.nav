using FnacDirect.Basket.Model;
using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.OrderPipe.Base.Model.Tracking;
using FnacDirect.OrderPipe.BaseMvc.Web.Models.Tracking;
using System.Collections.Generic;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Services.Tracking
{
    public interface ITrackingService
    {
        /// <summary>
        /// Emit tracking message for one article
        /// </summary>
        /// <param name="popOrderForm"></param>
        /// <param name="trackingArticleModel"></param>
        /// <param name="eventTracking"></param>
        void TrackArticle(PopOrderForm popOrderForm, TrackingArticleModel trackingArticleModel, EventTracking eventTracking);
        /// <summary>
        /// Emit tracking message for list articles
        /// </summary>
        /// <param name="popOrderForm"></param>
        /// <param name="trackingArticleModels"></param>
        /// <param name="eventTracking"></param>
        void TrackArticles(PopOrderForm popOrderForm, IEnumerable<TrackingArticleModel> trackingArticleModels, EventTracking eventTracking);
    }
}
