using FnacDirect.OrderPipe.Base.Business.LogisticLineGroups.Internals.Handlers;
using FnacDirect.OrderPipe.Base.Business.LogisticLineGroups.Internals.Initializers;
using FnacDirect.OrderPipe.Base.Business.LogisticLineGroups.Internals.InitialSelectors;
using FnacDirect.OrderPipe.Base.Business.LogisticLineGroups.Internals.Rules;
using FnacDirect.OrderPipe.Base.Business.LogisticLineGroups.Internals.Unfolders;
using FnacDirect.OrderPipe.Base.LineGroups;
using System;
using System.Collections.Generic;
using System.Linq;

namespace FnacDirect.OrderPipe.Base.Business.LogisticLineGroups.Rules
{
    public abstract class BaseLogisticLineGroupGenerationRule : ILogisticLineGroupGenerationRule
    {
        private readonly ILogisticLineGroupGenerator _logisticLineGroupGenerator;
        private readonly ISwitchProvider _switchProvider;

        public ILogisticLineGroupGenerationRuleDescriptor LogisticLineGroupGenerationRuleDescriptor { get; set; }

        protected BaseLogisticLineGroupGenerationRule(ISwitchProvider switchProvider, ILogisticLineGroupGenerator logisticLineGroupGenerator)
        {
            _switchProvider = switchProvider;
            _logisticLineGroupGenerator = logisticLineGroupGenerator;
        }

        protected abstract ILogisticLineGroupUnfolderFactory GetUnfolderFactory(Type type);

        protected abstract ILineGroupInitialSelectorFactory GetInitialSelectorFactory(Type type);

        protected abstract ILogisticLineGroupInitializerFactory GetInitializerFactory(Type type);

        protected abstract IEnumerable<ILogisticLineGroupGeneratedHandler> GetLogisticLineGroupGeneratedHandlers(IEnumerable<Type> types);

        public IEnumerable<ILogisticLineGroup> Apply(ILogisticLineGroupGenerationProcessContext logisticLineGroupGenerationProcessContext)
        {
            if (!_switchProvider.IsEnabled(LogisticLineGroupGenerationRuleDescriptor.SwitchId, false))
            {
                return Enumerable.Empty<ILogisticLineGroup>();
            }

            var initialSelectorFactory = GetInitialSelectorFactory(LogisticLineGroupGenerationRuleDescriptor.Selector);
            var initializerFactory = GetInitializerFactory(LogisticLineGroupGenerationRuleDescriptor.Initializer);
            var unfolderFactory = GetUnfolderFactory(LogisticLineGroupGenerationRuleDescriptor.Unfolder);

            var initialSelector = initialSelectorFactory.Get(logisticLineGroupGenerationProcessContext);
            var initializer = initializerFactory.Get(logisticLineGroupGenerationProcessContext);

            var initialSelection = initialSelector.Select(logisticLineGroupGenerationProcessContext.LineGroups.Where(l => l.Articles.Any()));

            var output = new List<ILogisticLineGroup>();

            if(initialSelection.Count == 0 || initialSelection.Values.All(v => !v.Any()))
            {
                return output;
            }

            var logisticLineGroups = initializer.Initialize(initialSelection).ToList();
            
            foreach (var logisticLineGroup in logisticLineGroups)
            {
                var unfolder = unfolderFactory.Get(logisticLineGroupGenerationProcessContext);

                var unfoldedArticles = unfolder.Unfold(logisticLineGroup.Articles);

                var builtLogisticLineGroup = _logisticLineGroupGenerator.Generate(logisticLineGroup, unfoldedArticles);

                output.Add(builtLogisticLineGroup);
            }

            foreach (var lineGroupHeader in initialSelection.Keys)
            {
                var matchingSourceLineGroup = logisticLineGroupGenerationProcessContext.LineGroups.FirstOrDefault(l => l.UniqueId == lineGroupHeader.UniqueId);

                if (matchingSourceLineGroup != null)
                {
                    foreach (var article in initialSelection[lineGroupHeader].ToList())
                    {
                        matchingSourceLineGroup.Articles.Remove(article);
                    }
                }
            }

            foreach (var logisticLineGroupGeneratedHandler in GetLogisticLineGroupGeneratedHandlers(LogisticLineGroupGenerationRuleDescriptor.LogisticLineGroupGeneratedHandlers))
            {
                logisticLineGroupGeneratedHandler.OnGenerated(initialSelection.Keys, output);
            }

            return output;
        }

        public IEnumerable<ILogisticLineGroup> SimulateInitialize(ILogisticLineGroupGenerationProcessContext logisticLineGroupGenerationProcessContext)
        {
            if (!_switchProvider.IsEnabled(LogisticLineGroupGenerationRuleDescriptor.SwitchId))
            {
                return Enumerable.Empty<ILogisticLineGroup>();
            }

            var initialSelectorFactory = GetInitialSelectorFactory(LogisticLineGroupGenerationRuleDescriptor.Selector);
            var initializerFactory = GetInitializerFactory(LogisticLineGroupGenerationRuleDescriptor.Initializer);

            var initialSelector = initialSelectorFactory.Get(logisticLineGroupGenerationProcessContext);
            var initializer = initializerFactory.Get(logisticLineGroupGenerationProcessContext);

            var initialSelection = initialSelector.Select(logisticLineGroupGenerationProcessContext.LineGroups.Where(l => l.Articles.Any()));

            var logisticLineGroups = initializer.SimulateInitialize(initialSelection).ToList();

            foreach (var lineGroupHeader in initialSelection.Keys)
            {
                var matchingSourceLineGroup = logisticLineGroupGenerationProcessContext.LineGroups.FirstOrDefault(l => l.UniqueId == lineGroupHeader.UniqueId);

                if (matchingSourceLineGroup != null)
                {
                    foreach (var article in initialSelection[lineGroupHeader].ToList())
                    {
                        matchingSourceLineGroup.Articles.Remove(article);
                    }
                }
            }

            return logisticLineGroups;
        }
    }
}
