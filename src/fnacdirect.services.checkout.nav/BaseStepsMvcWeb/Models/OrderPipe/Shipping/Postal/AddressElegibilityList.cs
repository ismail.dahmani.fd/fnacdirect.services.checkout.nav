using System;
using System.Collections.Generic;
using System.Linq;

namespace FnacDirect.OrderPipe.Base.Model
{
    public class AddressElegibilityList : List<AddressElegibility>
    {
        private readonly Func<AddressElegibility, bool> _validPredicate = a => a.IsValid;
        private readonly Func<AddressElegibility, bool> _eligiblePredicate = a => a.IsElegible;
        private readonly Func<AddressElegibility, bool> _completePredicate = a => !a.HasMissingInformations;

        public AddressElegibilityList(IEnumerable<AddressElegibility> list) : base(list) { }

        public AddressElegibilityList() : base() { }

        /// <summary>
        /// Retourne la liste des adresses valides (éligibles et sans informations manquantes) et utilisables.
        /// </summary>
        public IEnumerable<AddressElegibility> GetValidAddresses()
        {
            return this.Where(_validPredicate);
        }

        /// <summary>
        /// Retourne la liste des adresses éligibles à la livraison, uniquement.
        /// Ces adresses peuvent avoir des informations manquantes.
        /// </summary>
        public IEnumerable<AddressElegibility> GetEligibleAddresses()
        {
            return this.Where(_eligiblePredicate);
        }

        /// <summary>
        /// Retourne la première adresse valide (éligible et sans informations manquantes) et utilisable.
        /// </summary>
        public AddressElegibility GetFirstValidAddress()
        {
            return GetValidAddresses()
                .FirstOrDefault();
        }

        /// <summary>
        /// Retourne la première adresse complète (sans informations manquantes).
        /// Cette addresse n'est pas obligatoirement éligible.
        /// </summary>
        public AddressElegibility GetFirstCompleteAddress()
        {
            return this
                .Where(_completePredicate)
                .FirstOrDefault();
        }

        /// <summary>
        /// Retourne la première adresse éligible à la livraison.
        /// Cette addresse peut avoir des informations manquantes.
        /// </summary>
        public AddressElegibility GetFirstEligibleAddress()
        {
            return GetEligibleAddresses()
                .FirstOrDefault();
        }

        /// <summary>
        /// Retourne une adresse.
        /// </summary>
        public AddressElegibility GetFirstAddress()
        {
            return this.FirstOrDefault();
        }


        /// <summary>
        /// Retourne une adresse éligible à la livraison, selon l'id "AdressBookId".
        /// Cette méthode vérifie si l'adresse est valide avant de vérifier s'il est éligible.
        /// </summary>
        public AddressElegibility GetElegibleAddressByAddressBookId(int? addressBookId)
        {
            if (!addressBookId.HasValue)
            {
                return null;
            }

            // On retourne en priorité une adresse valide
            return GetEligibleAddresses()
                .OrderByDescending(_validPredicate)
                .FirstOrDefault(a => a.AddressEntity.AddressBookId == addressBookId.Value);
        }
    }
}

