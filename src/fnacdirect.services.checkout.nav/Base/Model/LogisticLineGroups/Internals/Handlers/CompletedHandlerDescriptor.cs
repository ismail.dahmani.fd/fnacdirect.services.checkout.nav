﻿using System;
using StructureMap.Pipeline;

namespace FnacDirect.OrderPipe.Base.Business.LogisticLineGroups.Internals.Handlers
{
    public class CompletedHandlerDescriptor<T> : ILogisticLineGroupProcessCompletedHandlerDescriptor
        where T : ILogisticLineGroupProcessCompletedHandler
    {
        public virtual int Priority { get; set; }

        public Type Type
        {
            get
            {
                return typeof(T);
            }
        }

        public virtual ExplicitArguments GetExplicitArguments()
        {
            return new ExplicitArguments();
        }
    }
}
