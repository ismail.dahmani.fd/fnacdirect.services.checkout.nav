﻿using FnacDirect.OrderPipe.Config;
using FnacDirect.OrderPipe.CoreRouting.UiDescriptorRenderer;
using StructureMap;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace FnacDirect.OrderPipe.Mvc
{
    public class MvcUiDescriptorRenderer : UiDescriptorRendererBase<MvcUiDescriptor>
    {
        private readonly IContainer _container;

        public MvcUiDescriptorRenderer(IContainer container)
        {
            _container = container;
        }

        public override string DoRenderFor(MvcUiDescriptor descriptor)
        {
            var textWriter = new StringWriter();

            var controllerFactory = ControllerBuilder.Current.GetControllerFactory();

            var requestContext = new RequestContext(new HttpContextWrapper(HttpContext.Current), new RouteData());
            requestContext.RouteData.DataTokens["Area"] = descriptor.AreaName;
            requestContext.RouteData.Values["controller"] = descriptor.Controller;
            requestContext.RouteData.Values["action"] = descriptor.Action;

            var controller = controllerFactory.CreateController(requestContext, descriptor.Controller) as Controller;
            var controllerContext = new ControllerContext(requestContext, controller);
            controller.ControllerContext = controllerContext;

            var methodInfo = controller.GetType().GetMethod(descriptor.Action);

            var result = methodInfo.Invoke(controller, null) as ViewResultBase;

            var viewEngineResult = controller.ViewEngineCollection.FindView(controllerContext, result.ViewName, null);
            var view = viewEngineResult.View;

            var viewData = new ViewDataDictionary(result.Model);

            var viewContext = new ViewContext(controllerContext, view, controller.ViewData, controller.TempData, textWriter);

            view.Render(viewContext, textWriter);

            return textWriter.ToString();
        }
    }
}
