using FnacDirect.Contracts.Online.Model;
using FnacDirect.Customer.Model;
using FnacDirect.OrderPipe.Base.DAL;
using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.Technical.Framework.Caching2;
using FnacDirect.Technical.Framework.ServiceLocation;
using FnacDirect.Technical.Framework.Velocity;
using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;

namespace FnacDirect.OrderPipe.Base.Business
{
    public class CreditCardDescriptorBusiness : ICreditCardDescriptorBusiness
    {
        private readonly CommerceDAL _commerceDAL = new CommerceDAL();
        private readonly ISwitchProvider _switchProvider = ServiceLocator.Current.GetInstance<ISwitchProvider>();

        private bool IsAliasGateway => _switchProvider.IsEnabled("op.ogone.enablealiasgateway");

        private const string CacheSettingsName = "RefOrderPipe";

        #region creditCard Descriptors

        public List<CustCreditCardDescriptor> GetCreditCards(string config, string culture)
        {
            return GetCreditCardDescriptors(config, culture, "CreditCardDescriptor", false);
        }

        public List<CustCreditCardDescriptor> GetCreditCardsUnfiltered(string config, string culture)
        {
            return GetCreditCardDescriptors(config, culture, "CreditCardDescriptorUnfiltered", true);
        }

        private List<CustCreditCardDescriptor> GetCreditCardDescriptors(string config, string culture, string cacheName, bool unfiltered)
        {
            return CacheManager.Get(
                new CacheKey(CacheSettingsName, cacheName, config, IsAliasGateway),
                delegate ()
                {
                    var ret = _commerceDAL.GetCreditCards(config, culture, unfiltered);
                    foreach (var ccd in ret)
                    {
                        ccd.NumberLength = 0;
                        foreach (var c in ccd.Format)
                            if (c == '@') ccd.NumberLength++;
                    }

                    if (!IsAliasGateway)
                        foreach (var ccd in ret)
                        {
                            ccd.TransactionProcess = null;
                        }

                    return ret;
                }
            );
        }

        public CustCreditCardDescriptor GetCreditCardDescriptor(int id, string config, string culture)
        {
            return GetCreditCardDescriptor(id, config, culture, "CreditCardDescriptorDico", true);
        }

        public CustCreditCardDescriptor GetCreditCardDescriptorUnfiltered(int id, string config, string culture)
        {
            return GetCreditCardDescriptor(id, config, culture, "CreditCardDescriptorDicoUnfiltered", true);
        }

        private CustCreditCardDescriptor GetCreditCardDescriptor(int id, string config, string culture, string cacheName, bool unFiltered)
        {
            var dico = CacheManager.Get(
                new CacheKey(CacheSettingsName, cacheName, config, IsAliasGateway),
                delegate ()
                {
                    var _dico = new Dictionary<int, CustCreditCardDescriptor>();
                    var descriptors = unFiltered ? GetCreditCardsUnfiltered(config, culture) : GetCreditCards(config, culture);

                    foreach (var ccd in descriptors)
                    {
                        _dico.Add(ccd.Id, ccd);
                    }

                    return _dico;
                }
            );

            dico.TryGetValue(id, out var desc);
            return desc;
        }

        #endregion

        public List<CreditCardGroup> GetCreditCardGroups(SiteContext context)
        {
            return _commerceDAL.GetCreditCardGroups(context);
        }

        public string ApplyMask(CustCreditCardDescriptor ccd, string cardNumber)
        {
            return ApplyMask(ccd, null, cardNumber);
        }

        public string ApplyMask(CustCreditCardDescriptor ccd, string cardNumberSeparator, string cardNumber)
        {
            var sb = new StringBuilder(Regex.Replace(cardNumber, "-", string.Empty));

            for (var i = ccd.BeginMask; i <= sb.Length - ccd.EndMask; i++)
            {
                sb[i] = '*';
            }

            var sb2 = new StringBuilder();
            var idx = 0;
            foreach (var c in ccd.Format)
            {
                switch (c)
                {
                    case '@':
                        sb2.Append(sb[idx]);
                        idx++;
                        break;
                    default:
                        sb2.Append(c);
                        break;
                }
                if (idx >= sb.Length)
                    break;
            }

            return Regex.Replace(sb2.ToString(), " ", cardNumberSeparator ?? " ");
        }

        public bool CheckECard(string number)
        {
            var ecardsPrefix = CacheManager.Get(
                new CacheKey(CacheSettingsName, "ECards"),
                delegate ()
                {
                    return _commerceDAL.GetECards();
                }
            );

            foreach (var s in ecardsPrefix)
            {
                if (number.StartsWith(s))
                    return true;
            }
            return false;
        }

        public string GetECardPrefixesRange()
        {
            var ecardsPrefix = CacheManager.Get(
                new CacheKey(CacheSettingsName, "ECardPrefixesRange"),
                delegate ()
                {
                    return _commerceDAL.GetECardsRange();
                }
            );

            return ecardsPrefix;
        }

        public bool CheckLuhn(string number)
        {
            int s = 0, i = number.Length;
            while (i-- > 0)
            {
                var v = number[(number.Length - 1) - i] - '0' << i % 2;
                s += (v > 9) ? v - 9 : v;
            }

            return (s % 10) == 0;
        }

        public bool CheckDateVsOrder(DateTime expDate, ArticleList articles)
        {
            var dtMaxdate = new DateTime(1980, 1, 1); // Initialize
            foreach (var bskItem in CollectionUtils.FilterOnType<StandardArticle>(articles))
            {
                if (bskItem.Type == Model.ArticleType.Saleable)
                {
                    var dtDateLiv = default(DateTime);
                    switch (bskItem.AvailabilityId.Value)
                    {
                        case 10:
                        case 110:
                            if (bskItem.AnnouncementDate != default(DateTime))
                                dtDateLiv = bskItem.AnnouncementDate.AddDays(-1);
                            break;
                        default:
                            dtDateLiv = bskItem.TheoreticalExpeditionDate.GetValueOrDefault(DateTime.Today.AddHours(bskItem.ArticleDelay.Value));
                            break;
                    }
                    if (dtDateLiv > dtMaxdate)
                        dtMaxdate = dtDateLiv;
                }
            }
            if (dtMaxdate > expDate)
                return false;

            return true;

        }

        public List<Credit> GetAllCredits(SiteContext context)
        {
            return CacheManager.Get(
               new CacheKey(CacheSettingsName, "Credit"),
                delegate ()
                {
                    var credits = _commerceDAL.GetCredits(context);
                    return credits;
                });
        }

        public IEnumerable<Credit> GetFilteredCredits(decimal amount, SiteContext context)
        {
            foreach (var cr in GetAllCredits(context))
            {
                if (cr.StartDate <= DateTime.Now && cr.EndDate > DateTime.Now && cr.AmountMin <= amount && cr.AmountMax >= amount)
                {
                    yield return CloneUtil.DeepCopy(cr);
                }
            }
        }
    }
}
