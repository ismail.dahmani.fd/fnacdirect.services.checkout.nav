﻿namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Shipping
{
    public class PostalAddressValidationViewModel
    {
        public string Company { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string AddressLine3 { get; set; }
        public string ZipCode { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public int CountryId { get; set; }
    }
}
