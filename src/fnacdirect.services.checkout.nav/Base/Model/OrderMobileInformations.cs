﻿using FnacDirect.OrderPipe.Base.BaseModel.Model;
using System.Xml.Serialization;

namespace FnacDirect.OrderPipe.Base.Model.OrderInformations
{
    /// <summary>
    /// Contains context informations about pipe execution in a mobile context.
    /// All data are recomputed at each pipe processing, the whole structure SHOULD NOT be serialized.
    /// </summary>
    public class OrderMobileInformations : IOrderMobileInformations
    {
        public bool IsMobile { get; set; }

        public string CallingPlatform { get; set; }

        public int? Origin { get; set; }

        [XmlIgnore]
        public OriginType OriginType
        {
            get
            {
                if (!Origin.HasValue)
                {
                    return OriginType.Unknown;
                }

                return (OriginType)Origin.Value;
            }
        }
    }
}
