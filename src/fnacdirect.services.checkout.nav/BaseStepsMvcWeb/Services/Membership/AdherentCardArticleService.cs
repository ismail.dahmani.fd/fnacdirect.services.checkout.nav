using FnacDirect.Membership.Business;

namespace FnacDirect.OrderPipe.Base.Proxy.Membership
{
    public class AdherentCardArticleService : IAdherentCardArticleService
    {
        private readonly AdherentCardArticleBusiness _adherentCardArticleBusiness;

        public AdherentCardArticleService(AdherentCardArticleBusiness adherentCardArticleBusiness)
        {
            _adherentCardArticleBusiness = adherentCardArticleBusiness;
        }

        public bool IsAdherentCard(int prid)
        {
            return _adherentCardArticleBusiness.IsAdherentCard(prid);
        }

        public bool IsRenewAdhesionCard(int prid)
        {
            return _adherentCardArticleBusiness.IsRenewAdherentCard(prid);
        }

        public bool IsRecruitAdherentCard(int prid)
        {
            return _adherentCardArticleBusiness.IsRecruitAdherentCard(prid);
        }
    }
}
