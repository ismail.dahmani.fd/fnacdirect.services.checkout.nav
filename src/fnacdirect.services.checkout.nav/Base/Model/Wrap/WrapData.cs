
namespace FnacDirect.OrderPipe.Base.Model.Pop
{
    public class WrapData : GroupData
    {
        //Boolean utilisé pour notifier le client que son wrap a été supprimé ultérieurement, notamment à cause de son choix de livraison
        public bool HasBeenRemovedForShopShipping { get; set; }

        public string Signature { get; set; }

        public void RemoveWrap()
        {
            HasBeenRemovedForShopShipping = false;
            Signature = null;
        }
    }
}
