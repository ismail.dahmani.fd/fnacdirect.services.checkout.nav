﻿using FnacDirect.OrderPipe.Base.BaseModel;
using FnacDirect.OrderPipe.Base.Business.Shipping.Postal;
using FnacDirect.OrderPipe.Base.Business.Shipping.Relay;
using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.OrderPipe.BaseMvc.Web.Services.ShipToStore;
using System.Collections.Generic;
using System.Linq;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Services.Pop.Shipping
{
    public class AddressSelectorService : IAddressSelectorService
    {
        private readonly IAddressService _addressService;
        private readonly IRelayValidityService _relayValidityService;
        private readonly IStoreEligibilityLineGroupsService _storeEligibilityLineGroupsService;
        private readonly IStoreSearchService _storeSearchService;

        public AddressSelectorService(IAddressService addressService,
                                      IRelayValidityService relayValidityService,
                                      IStoreEligibilityLineGroupsService storeEligibilityLineGroupsService,
                                      IStoreSearchService storeSearchService)
        {
            _addressService = addressService;
            _relayValidityService = relayValidityService;
            _storeEligibilityLineGroupsService = storeEligibilityLineGroupsService;
            _storeSearchService = storeSearchService;
        }

        public void SelectAddressForSellers(OPContext opContext, PopOrderForm popOrderForm, int currentSellerId, IEnumerable<int> sellers)
        {
            var currentSellerShippingMethodEvaluationGroup = popOrderForm.ShippingMethodEvaluation.GetShippingMethodEvaluationGroup(currentSellerId).Value;

            switch (currentSellerShippingMethodEvaluationGroup.SelectedShippingMethodEvaluationType)
            {
                case ShippingMethodEvaluationType.Postal:
                    {
                        var shippingAddress = currentSellerShippingMethodEvaluationGroup.PostalShippingMethodEvaluationItem.Value.ShippingAddress;

                        foreach (var sellerId in sellers)
                        {
                            var lineGroups = popOrderForm.LineGroups.Seller(sellerId);
                            if (_addressService.IsAddressEligible(opContext, popOrderForm, lineGroups, shippingAddress.AddressEntity))
                            {
                                var shippingMethodEvaluationGroup = popOrderForm.ShippingMethodEvaluation.GetShippingMethodEvaluationGroup(sellerId).Value;
                                shippingMethodEvaluationGroup.SelectShippingMethodEvaluationType(ShippingMethodEvaluationType.Postal);
                                shippingMethodEvaluationGroup.ReplaceAddress(shippingAddress);
                            }
                        }
                    }
                    break;
                case ShippingMethodEvaluationType.Relay:
                    {
                        var shippingAddress = currentSellerShippingMethodEvaluationGroup.RelayShippingMethodEvaluationItem.Value.ShippingAddress;

                        foreach (var sellerId in sellers)
                        {
                            var maybeShippingMethodEvaluationGroup = popOrderForm.ShippingMethodEvaluation.GetShippingMethodEvaluationGroup(sellerId);
                            if (maybeShippingMethodEvaluationGroup.HasValue)
                            {
                                var shippingMethodEvaluationGroup = maybeShippingMethodEvaluationGroup.Value;

                                if (shippingMethodEvaluationGroup.RelayShippingMethodEvaluationItem.HasValue)
                                {
                                    if (_relayValidityService.CheckRelayValidity(shippingAddress, popOrderForm.LineGroups.Seller(sellerId)))
                                    {
                                        shippingMethodEvaluationGroup.SelectShippingMethodEvaluationType(ShippingMethodEvaluationType.Relay);
                                        shippingMethodEvaluationGroup.ReplaceAddress(shippingAddress);
                                    }
                                }
                            }
                        }
                    }
                    break;
                case ShippingMethodEvaluationType.Shop:
                    {
                        var shippingAddress = currentSellerShippingMethodEvaluationGroup.ShopShippingMethodEvaluationItem.Value.ShippingAddress;

                        foreach (var sellerId in sellers)
                        {
                            var maybeShippingMethodEvaluationGroup = popOrderForm.ShippingMethodEvaluation.GetShippingMethodEvaluationGroup(sellerId);
                            if (maybeShippingMethodEvaluationGroup.HasValue)
                            {
                                var shippingMethodEvaluationGroup = maybeShippingMethodEvaluationGroup.Value;

                                if (shippingMethodEvaluationGroup.ShopShippingMethodEvaluationItem.HasValue && shippingAddress != null && shippingAddress.Identity.HasValue)
                                {
                                    var currentSellerShippingStore = _storeSearchService.GetStore(shippingAddress.Identity.Value);
                                    var eligibleStores = _storeEligibilityLineGroupsService.GetStoresEligibleForLineGroups(currentSellerShippingStore.Yield(),
                                                                                                                           popOrderForm.LineGroups.Seller(sellerId),
                                                                                                                           shippingMethodEvaluationGroup.ShopShippingMethodEvaluationItem.Value);
                                    if (eligibleStores.Any())
                                    {
                                        shippingMethodEvaluationGroup.SelectShippingMethodEvaluationType(ShippingMethodEvaluationType.Shop);
                                        shippingMethodEvaluationGroup.ReplaceAddress(shippingAddress);
                                    }
                                }
                            }
                        }
                    }
                    break;
            }
        }
    }
}
