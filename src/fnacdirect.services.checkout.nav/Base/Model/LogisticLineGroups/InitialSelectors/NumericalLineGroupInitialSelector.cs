﻿using FnacDirect.OrderPipe.Base.Business.LogisticLineGroups.Internals.InitialSelectors;
using FnacDirect.OrderPipe.Base.LineGroups;
using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.OrderPipe.Base.Model.LineGroups;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FnacDirect.OrderPipe.Base.Business.LogisticLineGroups.InitialSelectors
{
    public class NumericalLineGroupInitialSelector : BaseLineGroupInitialSelector
    {
        public override IDictionary<ILineGroupHeader, IEnumerable<Article>> Select(IEnumerable<ILineGroup> lineGroups)
        {
            return lineGroups.Where(l => l.HasNumericalArticles).ToDictionary(l => l as ILineGroupHeader, l => l.Articles
                                                                                                                .OfType<StandardArticle>()
                                                                                                                .Where(a => a.IsEbook)
                                                                                                                .AsEnumerable<Article>());
        }
    }
}
