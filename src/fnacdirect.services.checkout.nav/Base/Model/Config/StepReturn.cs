﻿using System;

namespace FnacDirect.OrderPipe
{
    /// <summary>
    /// Classe de base pour le résultat de l'exécution d'une étape
    /// </summary>
    public class StepReturn
    {
        private static readonly StepReturn _process = new ProcessStepReturn();
        private static readonly StepReturn _skip = new SkipStepReturn();
        private static readonly StepReturn _next = new NextStepReturn();
        private static readonly StepReturn _rewind = new RewindStepReturn();
        private static readonly StepReturn _forceSkip = new ForceSkipStepReturn();

        /// <summary>
        /// Valeur prédéfinie indiquant que la phase Process doit être exécutée
        /// </summary>        
        public static StepReturn Process
        {
            get
            {
                return _process;
            }
        }

        /// <summary>
        /// Valeur prédéfinie indiquant que la phase Process ne doit pas être exécutée
        /// </summary>        
        public static StepReturn Skip
        {
            get
            {
                return _skip;
            }
        }

        /// <summary>
        /// Valeur prédéfinie indiquant que l'étape suivante définie dans le pipe peut être exécutée
        /// </summary>
        public static StepReturn Next
        {
            get
            {
                return _next;
            }
        }

        /// <summary>
        /// Valeur prédéfinie indiquant que l'exécution du Pipe doit être reprise depuis le début
        /// </summary>
        public static StepReturn Rewind
        {
            get
            {
                return _rewind;
            }
        }

        /// <summary>
        /// Valeur prédéfinie indiquant que l'exécution du Pipe doit être reprise depuis le début
        /// </summary>
        public static StepReturn RewindWithInfos(string returnInfos)
        {
            return new RewindStepReturn(returnInfos);
        }

        public static StepReturn ForceSkip
        {
            get
            {
                return _forceSkip;
            }
        }

        private readonly string _type;

        public string Type
        {
            get { return _type; }
        }

        private readonly string _returnInfos;

        public string ReturnInfos
        {
            get { return _returnInfos; }
        }

        internal StepReturn(string type)
        {
            _type = type;
        }

        internal StepReturn(string type, string returnInfos)
        {
            _type = type;
            _returnInfos = returnInfos;
        }

        public override int GetHashCode()
        {
            return _type.GetHashCode();
        }

        public override bool Equals(object obj)
        {
            var item = obj as StepReturn;

            if (item == null)
            {
                return false;
            }

            return Type.Equals(item.Type);
        }
    }

    public class ProcessStepReturn : StepReturn
    {
        public ProcessStepReturn()
            : base("Process")
        {
        }
    }

    public class SkipStepReturn : StepReturn
    {
        public SkipStepReturn()
            : base("Skip")
        {
        }
    }

    public class NextStepReturn : StepReturn
    {
        public NextStepReturn()
            : base("Next")
        {
        }
    }

    public class RewindStepReturn : StepReturn
    {
        public RewindStepReturn()
            : base("Rewind")
        {
        }

        public RewindStepReturn(string returnInfos)
            : base("Rewind", returnInfos)
        {
        }
    }

    public class ForceSkipStepReturn : StepReturn
    {
        public ForceSkipStepReturn()
            : base("ForceSkip")
        {
        }
    }

    /// <summary>
    /// Spécialisation de StepReturn indiquant que l'exécution du pipe doit se poursuivre à partir de l'étape indiquée
    /// </summary>
    public class StepReturnForward : StepReturn
    {
        private string forward;
        private bool doForce = true;
        public StepReturnForward(string forward) : base("Forward") { this.forward = forward; }
        public StepReturnForward(string forward, bool doforce) : base("Forward") { this.forward = forward; doForce = doforce; }
        public string Forward { get { return forward; } }
        public bool DoForce { get { return doForce; } }
    }

    /// <summary>
    /// Spécialisation de StepReturn indiquant qu'une erreur est survenue
    /// </summary>
    public class StepReturnError : StepReturn
    {
        private string code;
        private string description;
        public StepReturnError(string code, string desc) : base("Error") { this.code = code; description = desc; }
        public string Code { get { return code; } }
        public string Description { get { return description; } }
    }

}
