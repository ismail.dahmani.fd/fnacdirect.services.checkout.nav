﻿using System.Collections.Generic;
using FnacDirect.Technical.Framework.Utils;

namespace FnacDirect.OrderPipe.Base.Proxy.Appointment
{
    public interface IAppointmentService
    {
        RangeSet<int> GetAppointmentArticleTypeIds();

        void SaveAppointments(IEnumerable<FnacDirect.Appointment.Model.Appointment> appointments, string user, string comment);
    }
}
