using FnacDirect.Context;
using FnacDirect.OrderPipe.Base.Business.Shipping.Postal.Appointment.Fly;
using FnacDirect.OrderPipe.Base.LineGroups;
using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.OrderPipe.Base.Model.Shipping.Postal.Appointment.Fly;
using FnacDirect.OrderPipe.Core.Services;
using FnacDirect.Solex.AppointmentDelivery.Client.Contract;
using System;
using System.Collections.Generic;
using System.Linq;

namespace FnacDirect.OrderPipe.Base.Business.Shipping.Postal.AppointmentDelivery.Fly
{
    public class FlyBusiness : IFlyBusiness
    {
        private readonly IUserContext _userContext;
        private readonly IArticleService _articleService;
        private readonly ISolexBusiness _solexBusiness;
        private readonly IAppointmentDeliveryService _appointmentDeliveryService;

        private const int _sellerId = (int)SellerType.Fnac;
        private readonly IEnumerable<int> _shippingMethodIds;

        public int SellerId => _sellerId;
        public IEnumerable<int> ShippingMethodIds => _shippingMethodIds;

        public FlyBusiness(
            IArticleService articleService,
            IUserContext userContext,
            ISolexBusiness solexBusiness,
            IPipeParametersProvider pipeParametersProvider,
            IAppointmentDeliveryService appointmentDeliveryService)
        {
            _userContext = userContext;
            _articleService = articleService;
            _solexBusiness = solexBusiness;
            _appointmentDeliveryService = appointmentDeliveryService;

            _shippingMethodIds = pipeParametersProvider.GetAsEnumerable<int>("shipping.fly.ids");
        }

        public IFlyForm GetFlyOrder(PopOrderForm popOrderForm)
            => popOrderForm.GetPrivateData<FlyData>(create: false);

        public void SaveFlyOrder(PopOrderForm popOrderForm, IFlyForm flyForm)
        {
            var fnaclineGroups = popOrderForm.LineGroups.Seller(SellerId);

            foreach (var flyServiceByArticle in flyForm.SelectedFlyServices)
            {
                var article = fnaclineGroups.GetArticles().OfType<StandardArticle>().FirstOrDefault(a => a.ProductID.HasValue && a.ProductID.Value == flyServiceByArticle.ArticlePrid);
                if (article == null)
                    continue; 

                article.Services.RemoveAll(s => _articleService.IsFlyService(s));

                if (IsPaidService(flyServiceByArticle.FlyServicePrid))
                {
                    article.Services.Add(new Service
                    {
                        ProductID = flyServiceByArticle.FlyServicePrid,
                        Quantity = article.Quantity,
                        Type = ArticleType.Saleable,
                        ShoppingCartOriginId = _userContext.IsIdentified ? popOrderForm.UserContextInformations.UID : popOrderForm.SID
                    });
                }
            }

            popOrderForm.ArticlesModified = true;

            var selectedDeliverySlot = _appointmentDeliveryService.GetDeliverySlot(popOrderForm, flyForm.CalendarSlotIdentifier.PartnerId, flyForm.CalendarSlotIdentifier.PartnerUniqueIdentifier);
            if (selectedDeliverySlot == null)
                throw new NotImplementedException();

            _appointmentDeliveryService.SetSelectedDeliverySlot(popOrderForm, selectedDeliverySlot, SellerId, flyForm.ShippingMethodId);

            var flyData = popOrderForm.GetPrivateData<FlyData>(create: true);
            flyData.ShippingMethodId = flyForm.ShippingMethodId;
            flyData.AccessCode = flyForm.AccessCode;
            flyData.CalendarSlotIdentifier = flyForm.CalendarSlotIdentifier;
            flyData.Commentary = flyForm.Commentary;
            flyData.FloorNumber = flyForm.FloorNumber;
            flyData.HasElevatorAccess = flyForm.HasElevatorAccess;
            flyData.HasStaircaseStep = flyForm.HasStaircaseStep;
            flyData.PhoneNumber = flyForm.PhoneNumber;
            flyData.ProductQuantityToTakeBack = flyForm.ProductQuantityToTakeBack;
            flyData.SelectedFlyServices = flyForm.SelectedFlyServices;
            flyData.StaircaseType = flyForm.StaircaseType;
        }

        public QualifyingSurvey ToFlyQualifyingSurvey(FlyData flyData)
        {
            var qualifyingSurvey = new QualifyingSurvey()
            {
                ProductsToRecoverCount = flyData.ProductQuantityToTakeBack,
                MobilePhone = flyData.PhoneNumber,
                IsLift = flyData.HasElevatorAccess,
                IsMoreThanThreeStairs = flyData.HasStaircaseStep,
                DoorCode = flyData.AccessCode,
                Comment = flyData.Commentary,
                FloorNumber = flyData.FloorNumber
            };

            if (flyData.StaircaseType.HasValue && Enum.IsDefined(typeof(StairType), flyData.StaircaseType.Value))
            {
                var type = (StairType)flyData.StaircaseType.Value;
                qualifyingSurvey.StairsType = type;
            }

            return qualifyingSurvey;
        }

        public IEnumerable<Model.Article> GetEligibleArticlesForFly(PopOrderForm popOrderForm, int sellerId)
        {
            var shippingMethodEvaluationGroup = popOrderForm.ShippingMethodEvaluation.GetShippingMethodEvaluationGroup(sellerId).Value;
            var postalShippingMethodEvaluationItem = shippingMethodEvaluationGroup.PostalShippingMethodEvaluationItem.Value;

            var prids = new HashSet<int>();
            foreach (var shippingMethodId in ShippingMethodIds)
            {
                var identifiers = _solexBusiness.GetIdentifiers(postalShippingMethodEvaluationItem, shippingMethodId).ToList();
                foreach (var identifier in identifiers)
                {
                    if (identifier.Prid.HasValue)
                    {
                        prids.Add(identifier.Prid.Value);
                    }
                }
            }

            return popOrderForm.LineGroups.Seller(sellerId).GetArticles().Where(a => a.ProductID.HasValue && prids.Contains(a.ProductID.Value));
        }

        public IEnumerable<ILogisticLineGroup> GetFlyLogisticLineGroups(IEnumerable<ILogisticLineGroup> logisticLineGroups)
        {
            return logisticLineGroups.Where(llg => IsFlyShippingSelected(llg));

            bool IsFlyShippingSelected(ILogisticLineGroup logisticLineGroup)
            {
                var shippingChoice = logisticLineGroup.SelectedShippingChoice;

                return shippingChoice != null && ShippingMethodIds.Any(sm => shippingChoice.ContainsShippingMethodId(sm));
            }
        }

        #region Private

        private static bool IsPaidService(int servicePrid) => servicePrid > 0;

        #endregion
    }
}
