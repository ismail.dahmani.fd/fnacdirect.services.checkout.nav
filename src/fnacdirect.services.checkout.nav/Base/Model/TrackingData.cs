using System;

namespace FnacDirect.OrderPipe.Base.BaseModel.Model
{
    [Serializable]
    public class TrackingData : GroupData
    {
        private TrackingOrder _value;
        public TrackingOrder Value
        {
            get { return _value; }
            set { _value = value; }
        }
    }
}
