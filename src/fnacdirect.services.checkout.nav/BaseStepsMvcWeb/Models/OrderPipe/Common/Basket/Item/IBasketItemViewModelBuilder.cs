﻿using FnacDirect.OrderPipe.Base.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Common.Basket
{
    public interface IBasketItemViewModelBuilder
    {
        BasketItemViewModel BuildItemStandardArticle(StandardArticle standardArticle, IEnumerable<Promotion> promotions);
        BasketItemWithPictureViewModel BuildItemStandardArticleWithPicture(StandardArticle standardArticle, IEnumerable<Promotion> promotions);

        BasketItemViewModel BuildItemMpArticle(MarketPlaceArticle mpArticle);
        BasketItemWithPictureViewModel BuildItemMpArticleWithPicture(MarketPlaceArticle marketPlaceArticle);
    }
}
