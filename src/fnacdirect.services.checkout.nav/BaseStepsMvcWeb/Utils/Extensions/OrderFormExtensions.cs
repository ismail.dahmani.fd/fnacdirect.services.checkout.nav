﻿using System.Collections.Generic;
using System.Linq;
using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.OrderPipe.Base.Model.LineGroups;
using FnacDirect.OrderPipe.BaseMvc.Web.Services.ShipToStore;
using FnacDirect.OrderPipe.Base.Proxy.Store;
using FnacDirect.OrderPipe.Base.BaseModel.Model;
using FnacDirect.Technical.Framework.Web.Mvc;
using FnacDirect.Technical.Framework.Utils;

namespace FnacDirect.OrderPipe.BaseMvc.Web
{
    public static class OrderFormExtensions
    {
        public static bool HasNoFnacComShippableArticle(this IOF orderForm, IApplicationContext applicationContext)
        {
            var service16000Ids = RangeSet.Parse<int>(applicationContext.GetAppSetting("Service16000Ids"));
            var service19000Ids = RangeSet.Parse<int>(applicationContext.GetAppSetting("Service19000Ids"));
            var serviceOther = RangeSet.Parse<int>(applicationContext.GetAppSetting("ServiceOtherIds"));
           
            if (orderForm is PopOrderForm)
            {
                var popOrderForm = (PopOrderForm)orderForm;
                var aboIlliData = popOrderForm.GetPrivateData<AboIlliData>();
                var adherentCards = popOrderForm.GetPrivateData<AdherentData>().Cards;

                var articles = popOrderForm.LineGroups.Fnac().GetArticles().OfType<StandardArticle>().ToList();

                if (articles.Any() && articles.Exists(art => !art.IsDematSoft && !art.IsEbook && !art.IsNumerical 
                    && art.ProductID != aboIlliData.AboilliPrid && art.ProductID != aboIlliData.TrialAboilliPrid 
                    && (adherentCards.Any() && !adherentCards.Contains(art.ProductID.Value) 
                    && !service16000Ids.Contains(art.ProductID.Value)
                    && !service19000Ids.Contains(art.ProductID.Value)
                    && !serviceOther.Contains(art.ProductID.Value))))
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }

            return false;
        }
    }
}