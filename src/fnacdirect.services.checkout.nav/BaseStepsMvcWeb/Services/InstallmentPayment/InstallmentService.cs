using FnacDirect.InstallmentPayment.Business.Interfaces;
using FnacDirect.InstallmentPayment.Models;
using FnacDirect.OrderPipe.Base.BaseModel;
using FnacDirect.OrderPipe.Core.Services;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace FnacDirect.OrderPipe.Base.Business.InstallmentPayment
{
    public class InstallmentService : IInstallmentService
    {
        private readonly ILogger<InstallmentService> _logger;
        private readonly IAvailabilityService _availabilityService;
        private readonly IConfigurationProvider _configurationProvider;
        private readonly IConfigurationValidator _configurationValidator;
        private readonly IOfferService _offerService;
        private readonly IEnumerable<int> _allowedBillingCountries;
        private readonly IEnumerable<int> _allowedShippingCountries;
        private readonly IEnumerable<int> _excludedShippingMethods;
        private readonly Regex _customerAllowedForInstallmentPayment;

        public InstallmentService(
            ILogger<InstallmentService> logger,
            IAvailabilityService availabilityService, 
            IConfigurationProvider configurationProvider, 
            IConfigurationValidator configurationValidator, 
            IOfferService offerService,
            IPipeParametersProvider pipeParametersProvider)
        {
            _logger = logger;
            _availabilityService = availabilityService;
            _configurationProvider = configurationProvider;
            _configurationValidator = configurationValidator;
            _offerService = offerService;
            
            _allowedBillingCountries = pipeParametersProvider.GetAsEnumerable<int>("installmentpayment.allowed.billing.countries");
            _allowedShippingCountries = pipeParametersProvider.GetAsEnumerable<int>("installmentpayment.allowed.shipping.countries");
            _excludedShippingMethods = pipeParametersProvider.GetAsEnumerable<int>("installmentpayment.excluded.shippingmethods");
            _customerAllowedForInstallmentPayment =
                new Regex(pipeParametersProvider.Get("installmentpayment.allowed.accounts", @"[\D]"));
        }

        public IEnumerable<Offer> GetEligibles(InstallmentServiceParameters parameters)
        {
            if (!parameters.IsValid(InstallmentServiceParameters.CheckField.Articles |
                                         InstallmentServiceParameters.CheckField.PriceToPay |
                                         InstallmentServiceParameters.CheckField.ShippingMethodIds |
                                         InstallmentServiceParameters.CheckField.Country))
            {
                _logger.LogWarning($"InstallmentServiceParameters model is incomplete. Model must have: {nameof(parameters.Articles)}" +
                          $", {nameof(parameters.PriceToPay)}, {nameof(parameters.ShippingMethodIds)} and {nameof(parameters.Country)}");
                return Enumerable.Empty<Offer>();
            }

            return _availabilityService.GetEligibles(parameters.Articles, parameters.PriceToPay.Value, parameters.IsAdherent, parameters.Country, parameters.ShippingMethodIds);
        }

        public PartnerOffer GetPartnerOffer(InstallmentServiceParameters parameters)
        {
            if (!parameters.IsValid(InstallmentServiceParameters.CheckField.Articles |
                                         InstallmentServiceParameters.CheckField.PriceToPay |
                                         InstallmentServiceParameters.CheckField.ShippingMethodIds |
                                         InstallmentServiceParameters.CheckField.Country))
            {
                _logger.LogWarning($"InstallmentServiceParameters model is incomplete. Model must have: {nameof(parameters.Articles)}" +
                                   $", {nameof(parameters.PriceToPay)}, {nameof(parameters.ShippingMethodIds)} and {nameof(parameters.Country)}");
                return new PartnerOffer
                {
                    Offers = Enumerable.Empty<Offer>()
                };
            }

            return _availabilityService.GetPartnerOffer(parameters.Articles, parameters.PriceToPay.Value, parameters.IsAdherent, parameters.Country, parameters.ShippingMethodIds);
        }

        public IEnumerable<Parameters> Get(InstallmentServiceParameters parameters)
        {
            if (!parameters.IsValid(InstallmentServiceParameters.CheckField.Articles | InstallmentServiceParameters.CheckField.Country))
            {
                _logger.LogWarning($"InstallmentServiceParameters model is incomplete. Model must have: {nameof(parameters.Articles)} and {nameof(parameters.Country)}");
                return null;
            }

            return _configurationProvider.Get(parameters.Articles, parameters.Country);
        }

        public IEnumerable<Parameters> Validate(InstallmentServiceParameters parameters)
        {
            if (!parameters.IsValid(InstallmentServiceParameters.CheckField.Parameters))
            {
                _logger.LogWarning($"InstallmentServiceParameters model is incomplete. Model must have: {nameof(parameters.Parameters)}");
                return null;
            }

            return _configurationValidator.Validate(parameters.Parameters);
        }

        public PartnerOffer GetEligibleCreditCardIds(InstallmentServiceParameters parameters)
        {

            if (!parameters.IsValid(InstallmentServiceParameters.CheckField.Articles |
                                    InstallmentServiceParameters.CheckField.HasOrderBillingMethods |
                                    InstallmentServiceParameters.CheckField.AccountId |
                                    InstallmentServiceParameters.CheckField.IsGuest |
                                    InstallmentServiceParameters.CheckField.IsVisitorAuthenticated |
                                    InstallmentServiceParameters.CheckField.ShippingMethodEvaluation |
                                    InstallmentServiceParameters.CheckField.Country))
            {
                _logger.LogWarning($"InstallmentServiceParameters model is incomplete. Model must have: {nameof(parameters.Articles)}" +
                                   $", {nameof(parameters.HasOrderBillingMethods)}, {nameof(parameters.AccountId)}, {nameof(parameters.IsGuest)}" +
                                   $", {nameof(parameters.IsVisitorAuthenticated)}, {nameof(parameters.ShippingMethodEvaluation)} and {nameof(parameters.Country)}");
                return new PartnerOffer { Offers = Enumerable.Empty<Offer>() };
            }

            if (parameters.HasOrderBillingMethods.Value)
            {
                return new PartnerOffer { Offers = Enumerable.Empty<Offer>() };
            }

            if (!IsBillingAddressValid(parameters.BillingAddressCountryId)
                    || !AreShippingMethodsValid(parameters.ShippingMethodEvaluation, parameters.IsGuest.Value, parameters.IsVisitorAuthenticated.Value))
            {
                return new PartnerOffer { Offers = Enumerable.Empty<Offer>() };
            }

            var customerAllowed = _customerAllowedForInstallmentPayment.IsMatch(parameters.AccountId);

            if (!customerAllowed || parameters.IsCallCenter)
            {
                return new PartnerOffer { Offers = Enumerable.Empty<Offer>() };
            }

            if (parameters.TotalPriceEur == null)
            {
                return new PartnerOffer { Offers = Enumerable.Empty<Offer>() };
            }

            var remainingPriceToPay = parameters.TotalPriceEur;

            if (remainingPriceToPay == 0
                && !parameters.HasOrderBillingMethods.Value)
            {
                remainingPriceToPay = parameters.TotalPriceEur.GetValueOrDefault(0);
            }

            var selectedShippingMethodIds = new List<int>();
            var shippingMethodEvaluations = parameters.ShippingMethodEvaluation.ShippingMethodEvaluationGroups
                .Where(group => group.SelectedShippingMethodEvaluation?.SelectedShippingMethodId != null)
                .Select(group => group.SelectedShippingMethodEvaluation);

            foreach (var methodEvaluation in shippingMethodEvaluations)
            {
                var selectedChoice = methodEvaluation.Choices?.First(choice => choice.MainShippingMethodId == methodEvaluation.SelectedShippingMethodId);
                if (selectedChoice?.SelectedChoice != null)
                {
                    selectedShippingMethodIds.Add(selectedChoice.SelectedChoice.MainShippingMethodId);
                }
            }

            return _availabilityService.GetPartnerOffer(parameters.Articles, remainingPriceToPay.Value, parameters.IsAdherent, parameters.Country, selectedShippingMethodIds);
        }

        private bool IsBillingAddressValid(int? billingAddressCountryId)
        {
            return billingAddressCountryId != null
                   && _allowedBillingCountries.Contains(billingAddressCountryId.GetValueOrDefault());
        }

        private bool AreShippingMethodsValid(
            ShippingMethodEvaluation shippingMethodEvaluation,
            bool isGuest,
            bool isVisitorAuthenticated)
        {
            foreach (var group in shippingMethodEvaluation.ShippingMethodEvaluationGroups)
            {
                if (group.SelectedShippingMethodEvaluationType == ShippingMethodEvaluationType.Unknown)
                {
                    if (isGuest || isVisitorAuthenticated)
                    {
                        return false;
                    }
                }

                if (group.SelectedShippingMethodEvaluation != null &&
                    group.SelectedShippingMethodEvaluation.SelectedShippingMethodId.HasValue &&
                    _excludedShippingMethods.Contains((int)group.SelectedShippingMethodEvaluation.SelectedShippingMethodId))
                {
                    return false;
                }

                if (group.SelectedShippingMethodEvaluationType == ShippingMethodEvaluationType.Postal && _allowedShippingCountries.Any())
                {
                    var countryId = group.PostalShippingMethodEvaluationItem?.Value.ShippingAddress?.CountryId;

                    if (countryId.HasValue && !_allowedShippingCountries.Contains(countryId.Value))
                    {
                        return false;
                    }
                }
            }

            return true;
        }

        public IEnumerable<Offer> BuildOffersFromCreditCardTypeConfigs(InstallmentServiceParameters parameters)
        {
            if (!parameters.IsValid(InstallmentServiceParameters.CheckField.CreditCardTypesConfig |
                                    InstallmentServiceParameters.CheckField.PriceToPay |
                                    InstallmentServiceParameters.CheckField.Articles |
                                    InstallmentServiceParameters.CheckField.Country))
            {
                _logger.LogWarning($"InstallmentServiceParameters model is incomplete. Model must have: {nameof(parameters.Articles)}" +
                                   $", {nameof(parameters.PriceToPay)}; {nameof(parameters.CreditCardTypesConfig)} and {nameof(parameters.Country)}");
                return Enumerable.Empty<Offer>();
            }

            return _offerService.BuildOffers(parameters.CreditCardTypesConfig, parameters.PriceToPay.Value, parameters.Articles, parameters.Country);
        }

        public IEnumerable<Offer> BuildOffersFromCreditCardTypeIds(InstallmentServiceParameters parameters)
        {
            if (!parameters.IsValid(InstallmentServiceParameters.CheckField.CreditCardTypesConfig |
                                    InstallmentServiceParameters.CheckField.PriceToPay |
                                    InstallmentServiceParameters.CheckField.Articles |
                                    InstallmentServiceParameters.CheckField.Country))
            {
                _logger.LogWarning($"InstallmentServiceParameters model is incomplete. Model must have: {nameof(parameters.Articles)}" +
                                   $", {nameof(parameters.PriceToPay)}; {nameof(parameters.CreditCardTypesConfig)} and {nameof(parameters.Country)}");
                return Enumerable.Empty<Offer>();
            }

            return _offerService.BuildOffers(parameters.CreditCardTypeIds, parameters.PriceToPay.Value, parameters.Articles, parameters.Country);
        }
    }
}