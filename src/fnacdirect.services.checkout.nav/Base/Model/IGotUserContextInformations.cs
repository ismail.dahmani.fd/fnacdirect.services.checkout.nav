﻿using FnacDirect.OrderPipe.Base.Model.UserInformations;

namespace FnacDirect.OrderPipe.Base.Model.Facets
{
    public interface IGotUserContextInformations : IOF
    {
        IUserContextInformations UserContextInformations { get; }
    }
}
