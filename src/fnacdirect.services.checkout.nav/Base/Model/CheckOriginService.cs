using FnacDirect.Contracts.Online.Model;
using FnacDirect.OrderPipe.Base.BaseModel.Model;
using FnacDirect.OrderPipe.Base.Model.Facets;
using FnacDirect.Technical.Framework.Web.Mvc;

namespace FnacDirect.OrderPipe.Base.Business.Payment
{
    public class CheckOriginService : ICheckOriginService
    {
        private readonly IApplicationContext _applicationContext;

        public CheckOriginService(IApplicationContext applicationContext)
        {
            _applicationContext = applicationContext;
        }

        public bool IsFromFnacPro()
        {
            return _applicationContext.GetSiteId() == (int)FnacSites.FNACPRO;
        }

        public bool IsFromFnacProAndCallCenter(IGotUserContextInformations gotUserContextInformations)
        {            
            return IsFromFnacPro() && IsFromCallCenter(gotUserContextInformations);
        }

        public bool IsFromCallCenter(IGotUserContextInformations gotUserContextInformations)
        {
            var fromCallCenter = gotUserContextInformations?.UserContextInformations.IdentityImpersonator != null;
            return fromCallCenter;
        }

        public bool IsIphoneOrIpad(IGotMobileInformations mobileInformations)
        {
            return mobileInformations.OrderMobileInformations.OriginType == OriginType.IpadApp
                            || mobileInformations.OrderMobileInformations.OriginType == OriginType.IphoneApp;
        }
    }
}
