using FnacDirect.Contracts.Online.Model;
using FnacDirect.Membership.Business;
using FnacDirect.OrderPipe.Base.BaseModel.Model;
using FnacDirect.OrderPipe.Base.LineGroups;
using FnacDirect.OrderPipe.Core.Utils;
using FnacDirect.StoreAvailability.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using Vanilla.Middle.Arborescence;

namespace FnacDirect.OrderPipe.Base.Model
{
    [Serializable]
    public class StandardArticle : BaseArticle
    {
        public const string LIST_NUMBER_EIGHT_VALUE = "8";

        [XmlIgnore]
        private ServiceList _services;

        [XmlIgnore]
        private int? _splitLevel;

        [XmlIgnore]
        private int? _splitLevelPreco;

        public string AddOnFormData { get; set; }

        [XmlIgnore]
        public DateTime AnnouncementDate { get; set; }

        [XmlIgnore]
        public StockAvailability Availability { get; set; }

        [XmlIgnore]
        public int AvailableStock { get; set; }

        [XmlIgnore]
        public int AvailableStockToDisplay { get; set; }

        [XmlIgnore]
        public decimal? BestOfferPrice { get; set; }

        public virtual bool CanChangeSaleStream
        {
            get
            {
                if (ForceCannotBeShopArticle || IsPosterioriSelling)
                {
                    return false;
                }
                if (CodeSegment == "7304")
                {
                    return false;
                }
                if (!HasGestStock)
                {
                    return true;
                }
                if ((ArticleGroup)Family.GetValueOrDefault() == ArticleGroup.Software && IsNumerical)
                {
                    return false;
                }

                return true;
            }
        }

        [XmlIgnore]
        public char? CodeOP { get; set; }

        [XmlIgnore]
        public string CodePromoBO { get; set; }

        [XmlIgnore]
        public string CodeSegment { get; set; }

        [XmlIgnore]
        public bool? DisplayAnnouncementDay { get; set; }

        [XmlIgnore]
        public string EcoTaxCode { get; set; }

        [XmlIgnore]
        public decimal? EcoTaxEur { get; set; }

        [XmlIgnore]
        public decimal? EcoTaxEurNoVAT { get; set; }

        public int? FatherProductId { get; set; }

        [XmlIgnore]
        public bool FatherProductIdSpecified => FatherProductId.HasValue;

        [XmlIgnore]
        public int FlagStock { get; set; }

        public bool ForceCannotBeShopArticle { get; set; }

        public bool ForceCannotDoRebate { get; set; }

        [XmlIgnore]
        public int? FreeArticleTypeLogistic { get; set; }

        [XmlIgnore]
        public bool? HasAccServ { get; set; }

        [XmlIgnore]
        public override bool HasD3E => EcoTaxEur.HasValue;
        [XmlIgnore]
        public override decimal? D3ETax => EcoTaxEur;

        [XmlIgnore]
        public override decimal? DEATax => null; // TODO: Update when value is received

        [XmlIgnore]
        public override bool HasRCP => RCPTax.HasValue;

        [XmlIgnore]
        public bool HasExceededQuantity { get; set; }

        [XmlIgnore]
        public bool HasGestStock { get; set; }

        /// <summary>Indique si l'article a une quantité max imposée</summary>
        /// <remarks>Souvent dans le cas d'une promotion</remarks>
        [XmlIgnore]
        public bool HasLimitedQuantity { get; set; }

        [XmlIgnore]
        public bool HasMaxQuantityInBasket { get; set; }

        [XmlIgnore]
        public bool? HasOffer { get; set; }

        [XmlIgnore]
        public bool? HasOPS { get; set; }

        public bool HasRemiseClient => Remise != null;

        [XmlIgnore]
        public bool HasServiceDescriptor => !string.IsNullOrEmpty(ServiceDescriptorName);

        public bool HasStockRayon => ShopStocks?.QteMagasin > 0;
        
        public bool HasStockRayonDefraichi => ShopStocks?.QteDefraichiRayon > 0;

        public bool HasStockReserve => ShopStocks?.QteReserve > 0;

        public bool HasStockReserveDefraichi => ShopStocks?.QteDefraichiReserve > 0;

        public bool HasStockVitrine => ShopStocks?.QteVitrine > 0;

        [XmlIgnore]
        public bool Hidden { get; set; }

        [XmlIgnore]
        public int? InitialProductId { get; set; }

        public int? InsuranceStart { get; set; }

        public int InvoiceIdPosterioriService { get; set; }

        [Obsolete("Should not be used, use _adherentCardArticleService instead for testing sake")]
        [XmlIgnore]
        public virtual bool IsAdhesionCard => AdherentCardArticleBusiness.Instance.IsAdherentCard(ProductID.GetValueOrDefault(0));

        [XmlIgnore]
        public bool? IsBundle { get; set; }

        [XmlIgnore]
        public bool IsDematSoft => IsNumerical && SellerID == SellerIds.GetId(Base.LineGroups.SellerType.Software);

        [XmlIgnore]
        public override bool IsEbook => IsNumerical && IsBook;

        [XmlIgnore]
        public bool? IsEpsilonEligible { get; set; }

        [XmlIgnore]
        public bool IsForSale { get; set; }

        /// <summary>Indique si le stock de l'article est géré par la GUPT</summary>
        [XmlIgnore]
        public bool IsStockManagedByGupt => ShopStocks?.LigneProduit == "1";

        [XmlIgnore]
        public bool IsNumerical => ItemPhysicalType == PhysicalType.Numeric;

        public bool IsPosterioriSelling { get; set; }

        public bool IsRebatePossible
        {
            get
            {
                if (ForceCannotDoRebate)
                {
                    return false;
                }

                if (!CodeOP.HasValue)
                {
                    return true;
                }

                switch (CodeOP.Value)
                {
                    case 'Z':
                        return false;
                    default:
                        return true;
                }
            }
        }

        [XmlIgnore]
        public bool IsRecruitAdherentCard => AdherentCardArticleBusiness.Instance.IsRecruitAdherentCard(ProductID.GetValueOrDefault(0));

        [XmlIgnore]
        public bool IsRenewAdhesionCard => AdherentCardArticleBusiness.Instance.IsRenewAdherentCard(ProductID.GetValueOrDefault(0));

        [XmlIgnore]
        public bool IsSensitive => ShopStocks?.PrdSen == 1;

        [XmlIgnore]
        public virtual bool IsShopPE => IsPE && !(IsSoftware && !IsNumerical);

        [XmlIgnore]
        public PhysicalType ItemPhysicalType { get; set; }

        [XmlIgnore]
        public string ListNumber { get; set; }

        [XmlIgnore]
        public int MaxQuantityAllowed { get; set; }

        public NumericalAddOn NumericalAddOn { get; set; }

        [XmlIgnore]
        public bool? OPShasFnacOffer { get; set; }

        [XmlIgnore]
        public bool? OPSIsValid { get; set; }

        public string OPSName { get; set; }

        [XmlIgnore]
        public string PartnerIdentifier { get; set; }

        public int PosterioriMasterSKU { get; set; }

        [XmlIgnore]
        public int? PurchaseId { get; set; }

        public int QteReserve => ShopStocks?.QteReserve ?? 0;

        [XmlIgnore]
        public string RefEcoTax { get; set; }

        [XmlIgnore]
        public string ReferenceGU { get; set; }

        public string RefInvoicePosterioriService { get; set; }

        public RemiseClient Remise { get; set; }

        [XmlIgnore]
        public ArticleDeliveryDate SelectedDeliveryDate => DeliveryDates.SingleOrDefault(d => d.IsSelected);

        [XmlIgnore]
        public string ServiceDescriptorName { get; set; }

        public ServiceList Services
        {
            get => _services ?? (_services = new ServiceList());
            set => _services = value;
        }

        [XmlIgnore]
        public StockSTOM ShopStocks { get; set; }

        public int? SplitLevel
        {
            get => _splitLevel ?? 0;
            set => _splitLevel = value;
        }

        public int? SplitLevelPreco
        {
            get => _splitLevelPreco ?? 0;
            set => _splitLevelPreco = value;
        }

        [XmlIgnore]
        public string SubTitle1 { get; set; }

        public SupplySource SupplySource { get; set; }

        [XmlIgnore]
        public int SupportId { get; set; }

        [XmlIgnore]
        public int? TotalOfferCount { get; set; }

        [XmlIgnore]
        [DontDump]
        public List<TreeNode> TreeNodes { get; set; }

        [XmlIgnore]
        public string TypePromoBO { get; set; }

        [XmlIgnore]
        public int? WrapMethod { get; set; }

        [XmlIgnore]
        public decimal? WrapPriceDBEur { get; set; }

        [XmlIgnore]
        public decimal? WrapPriceNoVATEur { get; set; }

        [XmlIgnore]
        public decimal? WrapPriceUserEur { get; set; }


        public StandardArticle() : base()
        {
            Availability = StockAvailability.Central;
            HasOPS = false;
            IsForSale = true;
            OPSIsValid = false;
        }

        /// <summary>
        /// For IsAvailable property found during migration NotForSale.vbs if null then true considered as default value
        /// For IsForSale property found during migration NotForSale.vbs if null then true considered as default value
        /// </summary>
        public StandardArticle(ShoppingCartLineItem shoppingCartLineItem)
            : this()
        {
            ApplyShoppingCartLineItem(shoppingCartLineItem);
        }


        public void ApplyShoppingCartLineItem(ShoppingCartLineItem shoppingCartLineItem)
        {
            ArticleDelay = shoppingCartLineItem.Availability.Delay;
            AvailabilityId = shoppingCartLineItem.Availability.ID;
            AvailabilityLabel = shoppingCartLineItem.Availability.Label;
            Hidden = !shoppingCartLineItem.Display.HasValue || !shoppingCartLineItem.Display.Value;
            IsAvailable = shoppingCartLineItem.Availability.IsAvailable;
            PriceDBEur = 0;
            PriceMemberEur = 0;
            PriceNoVATEur = 0;
            PriceRealEur = 0;
            PriceUserEur = 0;
            ProductID = shoppingCartLineItem.Reference.PRID.Value;
            Quantity = Math.Max(shoppingCartLineItem.Quantity, 1);
            SalesInfo = shoppingCartLineItem.SalesInfo;
            Type = ArticleType.Free;
            VATId = shoppingCartLineItem.SalesInfo.VATId;
        }

        /// <summary>
        /// Copie le StandardArticle courant dans une nouvelle instance de T
        /// </summary>
        /// <typeparam name="T">Type d'objet à créer pour la nouvelle instance</typeparam>
        /// <returns>StandardArticle créé à partir du StandardArticle courant</returns>
        public virtual StandardArticle Copy<T>() where T : StandardArticle, new()
        {
            StandardArticle copy = new T
            {
                AddOnFormData = AddOnFormData,
                Address = Address,
                AnnouncementDate = AnnouncementDate,
                ArticleDelay = ArticleDelay,
                ArticleDTO = ArticleDTO,
                Availability = Availability,
                AvailabilityDelay = AvailabilityDelay,
                AvailabilityId = AvailabilityId,
                AvailabilityLabel = AvailabilityLabel,
                AvailableStock = AvailableStock,
                AvailableStockToDisplay = AvailableStockToDisplay,
                BestOfferPrice = BestOfferPrice,
                BillMethod = BillMethod,
                CodeOP = CodeOP,
                CodePromoBO = CodePromoBO,
                CodeSegment = CodeSegment,
                CollectAvailabilityLabel = CollectAvailabilityLabel,
                CollectInShop = CollectInShop,
                Content = Content,
                CopyControl = CopyControl,
                DeliveryDate = DeliveryDate,
                DisplayAnnouncementDay = DisplayAnnouncementDay,
                DistributorAvailabilityTypeId = DistributorAvailabilityTypeId,
                Ean13 = Ean13,
                EcoTaxCode = EcoTaxCode,
                EcoTaxEur = EcoTaxEur,
                EcoTaxEurNoVAT = EcoTaxEurNoVAT,
                Editor = Editor,
                EnableUpdateQuantity = EnableUpdateQuantity,
                ErgoBasketPictureURL = ErgoBasketPictureURL,
                ExternalOrderDetailReference = ExternalOrderDetailReference,
                Family = Family,
                FatherProductId = FatherProductId,
                FAURL = FAURL,
                FlagStock = FlagStock,
                ForceCannotBeShopArticle = ForceCannotBeShopArticle,
                ForceCannotDoRebate = ForceCannotDoRebate,
                Format = Format,
                FormatLabel = FormatLabel,
                FreeArticleTypeLogistic = FreeArticleTypeLogistic,
                HasAccServ = HasAccServ,
                HasExceededQuantity = HasExceededQuantity,
                HasGestStock = HasGestStock,
                HasLimitedQuantity = HasLimitedQuantity,
                HasMaxQuantityInBasket = HasMaxQuantityInBasket,
                HasOffer = HasOffer,
                HasOPS = HasOPS,
                Hidden = Hidden,
                ID = ID,
                InitialProductId = InitialProductId,
                InsertDate = InsertDate,
                InsuranceStart = InsuranceStart,
                InvoiceIdPosterioriService = InvoiceIdPosterioriService,
                IsAvailable = IsAvailable,
                IsAvailableInStore = IsAvailableInStore,
                IsBundle = IsBundle,
                IsEpsilonEligible = IsEpsilonEligible,
                IsForSale = IsForSale,
                IsPosterioriSelling = IsPosterioriSelling,
                ItemPhysicalType = ItemPhysicalType,
                Label = Label,
                Language = Language,
                ListNumber = ListNumber,
                LittleScan = LittleScan,
                LogType = LogType,
                MarqueEditor = MarqueEditor,
                MaximumQuantity = MaximumQuantity,
                MaxQuantityAllowed = MaxQuantityAllowed,
                NumericalAddOn = NumericalAddOn,
                OfferRef = OfferRef,
                OldPrice = OldPrice,
                OldPriceRecordedDate = OldPriceRecordedDate,
                OPShasFnacOffer = OPShasFnacOffer,
                OPSIsValid = OPSIsValid,
                OPSName = OPSName,
                OrderDetailPk = OrderDetailPk,
                OriginCode = OriginCode,
                Participant1 = Participant1,
                Participant1_id = Participant1_id,
                Participant2 = Participant2,
                PartnerIdentifier = PartnerIdentifier,
                Picture_Principal_340x340 = Picture_Principal_340x340,
                PosterioriMasterSKU = PosterioriMasterSKU,
                PreviewShippingCost = PreviewShippingCost,
                PriceDBEur = PriceDBEur,
                PriceDBEurNoVAT = PriceDBEurNoVAT,
                PriceMemberEur = PriceMemberEur,
                PriceNoVATEur = PriceNoVATEur,
                PriceRealEur = PriceRealEur,
                PriceRealEurNoVAT = PriceRealEurNoVAT,
                PriceUserEur = PriceUserEur,
                PriceUserEurNoVAT = PriceUserEurNoVAT,
                ProductID = ProductID,
                Promotions = Promotions,
                PurchaseId = PurchaseId,
                Quantity = Quantity,
                RCPTax = RCPTax,
                RefEcoTax = RefEcoTax,
                ReferenceGU = ReferenceGU,
                Referentiel = Referentiel,
                RefInvoicePosterioriService = RefInvoicePosterioriService,
                Remise = Remise,
                SalesInfo = SalesInfo,
                ScanURL = ScanURL,
                SellerID = SellerID,
                SerieTitle = SerieTitle,
                ServiceDescriptorName = ServiceDescriptorName,
                Services = Services,
                ShipDelay = ShipDelay,
                ShipMethod = ShipMethod,
                ShipMethodLabel = ShipMethodLabel,
                ShippingCartURL = ShippingCartURL,
                ShippingMethodDetail = ShippingMethodDetail,
                ShipPriceDBEur = ShipPriceDBEur,
                ShipPriceForNoLocalShippmentEur = ShipPriceForNoLocalShippmentEur,
                ShipPriceNoVATEur = ShipPriceNoVATEur,
                ShipPriceUserEur = ShipPriceUserEur,
                ShopFromStore = ShopFromStore,
                ShoppingCartOriginId = ShoppingCartOriginId,
                ShopStocks = ShopStocks,
                ShowAvailibility = ShowAvailibility,
                SplitLevel = SplitLevel,
                SplitLevelPreco = SplitLevelPreco,
                SteedFromStore = SteedFromStore,
                SubTitle1 = SubTitle1,
                SupplySource = SupplySource,
                SupportId = SupportId,
                TheoreticalDeliveryDate = TheoreticalDeliveryDate,
                TheoreticalDeliveryDateMax = TheoreticalDeliveryDateMax,
                TheoreticalExpeditionDate = TheoreticalExpeditionDate,
                Title = Title,
                TotalOfferCount = TotalOfferCount,
                TreeNodes = TreeNodes,
                Type = Type,
                TypeId = TypeId,
                TypeLabel = TypeLabel,
                TypePromoBO = TypePromoBO,
                UniverseInfo = UniverseInfo,
                UseVat = UseVat,
                VATCodeGu = VATCodeGu,
                VatCountryId = VatCountryId,
                VATId = VATId,
                VATRate = VATRate,
                WrapMethod = WrapMethod,
                WrapPriceDBEur = WrapPriceDBEur,
                WrapPriceNoVATEur = WrapPriceNoVATEur,
                WrapPriceUserEur = WrapPriceUserEur
            };

            return copy;
        }

        public override int GetQuantity()
            => base.GetQuantity() + Services.Sum(s => s.GetQuantity());

        public bool HasService()
            => Services.Any();

        public bool HasService(IEnumerable<Article> articles)
            => HasService() || articles.HasServiceForArticle(this);

        public bool IsAvailableLessThanOneDay()
            => (AvailabilityEnum)AvailabilityId == AvailabilityEnum.StockPE ||
                (AvailabilityEnum)AvailabilityId == AvailabilityEnum.StockPT ||
                (AvailabilityEnum)AvailabilityId == AvailabilityEnum.ImmediateAvailabilityPT ||
                (AvailabilityEnum)AvailabilityId == AvailabilityEnum.ImmediateAvailabilityPE;

        public override ArticleReference ToArticleReference()
            => new ArticleReference(ProductID, null, null, ReferenceGU);

        public override string ToString()
        {
            var standardArticle = new StringBuilder();
            standardArticle.AppendLine(base.ToString());
            standardArticle.AppendLine($"HasGestStock : {HasGestStock}");
            standardArticle.AppendLine($"Availability {Availability}");
            standardArticle.AppendLine($"IsAvailable {IsAvailable}");
            standardArticle.AppendLine($"PriceUserEur {PriceUserEur}");
            standardArticle.AppendLine($"HasExceededQuantity {HasExceededQuantity}");
            standardArticle.AppendLine($"HasStockReserve {HasStockReserve}");
            standardArticle.AppendLine($"HasStockRayon {HasStockRayon}");
            standardArticle.AppendLine($"IsBundle {IsBundle}");
            return standardArticle.ToString();
        }
    }
}
