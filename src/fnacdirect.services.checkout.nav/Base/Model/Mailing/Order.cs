
using FnacDirect.OrderPipe.Base.Business.OrderDetail;
using System.Collections.Generic;
using FnacDirect.Mailing.Model;
using System.Xml.Serialization;
using System.Linq;

namespace FnacDirect.OrderPipe.Base.Business.Mailing
{
    [XmlType(AnonymousType = true, IncludeInSchema = false, Namespace = "")]
    [XmlRoot("ctx", Namespace = "")]
    public class Order : MailContext
    {
        public UserInfo User { get; set; }

        public OrderGlobalInfo OrderInfo { get; set; }

        public List<OrderDetail> OrderDetails { get; set; }

        public PostalAddress BillingAddress { get; set; }

        public BaseAddress ShippingAddress { get; set; }

        public BillingInfo Billing { get; set; }

        public Cgv Cgv { get; set; }

        public string MainOrderReference { get; set; }

        public string OrderExternalReference { get; set; }

        public bool IsShopShipping { get; set; }

        public bool IsShippingFly { get; set; }

        public GuestInfos GuestInfos { get; set; }

        public bool OrderContainsTypeId(int typeId)
        {
            return OrderDetails.SelectMany(x => x.Articles).Any(x => x.TypeId.GetValueOrDefault() == typeId);
        }
    }
}
