﻿namespace FnacDirect.OrderPipe.Base.Business.Stock
{
    public interface IStockReservationServiceFactory
    {
        IStockReservationService Get(StockReservationMode stockReservationMode);
    }
}
