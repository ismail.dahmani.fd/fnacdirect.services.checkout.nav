using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FnacDirect.OrderPipe.Base.Model;

namespace FnacDirect.OrderPipe.Base.Model
{
	public class LimonetikBillingMethod : BillingMethod
	{
        public const int IdBillingMethod = 33554432;

        public override bool IsPrimary
        {
            get
            {
                return true;
            }
        }

		public LimonetikBillingMethod()
		{
            BillingMethodId = IdBillingMethod;
		}
	}
}
