﻿namespace FnacDirect.OrderPipe.BaseMvc.Web.Pop.ApiControllers.Models
{
    public class ShippingMethodSelectionViewModel
    {
        public int ShippingMethodId { get; set; }
    }
}
