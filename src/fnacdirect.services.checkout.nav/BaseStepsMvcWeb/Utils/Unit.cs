﻿namespace FnacDirect.OrderPipe.Core.Utils
{
    /// <summary>
    /// Represents a void object
    /// </summary>
    public class Unit
    {
        /// <summary>
        /// Gets an unique Unit instance
        /// </summary>
        public static Unit Default
        {
            get
            {
                return new Unit();
            }
        }
    }
}
