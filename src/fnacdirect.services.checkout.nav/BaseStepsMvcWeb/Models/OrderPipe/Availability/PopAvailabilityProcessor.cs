using FnacDirect.OrderPipe.Base.Business.Operations;
using FnacDirect.OrderPipe.Base.Business.Shipping.Shop;
using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.Technical.Framework.Web.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;

namespace FnacDirect.OrderPipe.Base.Business.Availability
{
    public class PopAvailabilityProcessor : IPopAvailabilityProcessor
    {
        private readonly IApplicationContext _applicationContext;
        private readonly IAvailabilityLabelRuleFinder _availabilityLabelRuleFinder;
        private readonly IList<int> _shippingMethodIds;
        private readonly IDeliveredForChristmasService _deliveredForChristmasService;

        public PopAvailabilityProcessor(IApplicationContext applicationContext, IAvailabilityLabelRuleFinder availabilityLabelRuleFinder, IDeliveredForChristmasService deliveredForChristmasService)
        {
            _applicationContext = applicationContext;

            _availabilityLabelRuleFinder = availabilityLabelRuleFinder;
            _shippingMethodIds = GetListShippingMethodId_NDD();

            _deliveredForChristmasService = deliveredForChristmasService;
        }

        private List<int> GetListShippingMethodId_NDD()
        {
            var lstShippingMethodIds = new List<int>();

            var lstIds = _applicationContext.GetAppSetting("shippingmethodid.nextdaydelivery").Split(',');

            foreach (var shippingId in lstIds)
            {
                if (int.TryParse(shippingId, out var iId))
                    lstShippingMethodIds.Add(iId);
            }

            return lstShippingMethodIds;
        }

        public string GetShortGlobalEstimatedDeliveryDate(int shippingMethodId, IEnumerable<PopAvailabilityProcessorInput> availabilities)
        {
            return GetShortGlobalEstimatedDeliveryDate(shippingMethodId, availabilities, null);
        }

        public string GetShortGlobalEstimatedDeliveryDate(int shippingMethodId, IEnumerable<PopAvailabilityProcessorInput> availabilities, bool? hasManyDeliveryDates)
        {
            if (!availabilities.Any())
            {
                return string.Empty;
            }
            var prefix = GetPrefix(availabilities);

            return ComputeShortGlobalEstimatedDeliveryDate(shippingMethodId, availabilities, true, prefix, hasManyDeliveryDates);
        }

        public string GetShortGlobalEstimatedDeliveryDate(int shippingMethodId, IEnumerable<PopAvailabilityProcessorInput> availabilities, bool? hasManyDeliveryDates, string prefix)
        {
            return ComputeShortGlobalEstimatedDeliveryDate(shippingMethodId, availabilities, true, prefix, hasManyDeliveryDates);
        }

        public string GetShortGlobalEstimatedDeliveryDateForStore(FnacStore.Model.FnacStore fnacStore, StandardArticle standardArticle, IEnumerable<ShipToStoreAvailability> availabilities)
        {
            var availability = availabilities.FirstOrDefault(a => a.ReferenceGu == standardArticle.ReferenceGU);

            if (availability == null || standardArticle.IsEbook)
            {
                return string.Empty;
            }

            var prefix = _applicationContext.GetMessage("orderpipe.pop.shipping.availability.prefix.shop.single");

            if (availability is ClickAndCollectShipToStoreAvailability)
            {
                if (fnacStore != null)
                {
                    var deliveryDateOfWeek = fnacStore.GetDisplaying1HDateDelivery();

                    var displayDateOfDelivery1H = deliveryDateOfWeek < 0 ? string.Empty :
                        deliveryDateOfWeek == (int)DateTime.Now.DayOfWeek ?
                        _applicationContext.GetMessage("orderpipe.pop.shipping.availability.clickandcollect.Today", null) :
                        _applicationContext.GetMessage("orderpipe.pop.shipping.store.timetable.dayofweek." + deliveryDateOfWeek, null);

                    return prefix + _applicationContext.GetMessage("orderpipe.pop.shipping.availability.short.1hour.Message", displayDateOfDelivery1H);
                }
                return prefix + _applicationContext.GetMessage("orderpipe.pop.shipping.availability.short.1hour");
            }

            var shippingAvailability = availability as ShippingShipToStoreAvailability;

            var articleAvailabilities = new PopAvailabilityProcessorInput(standardArticle, shippingAvailability.DeliveryDayMin, shippingAvailability.DeliveryDayMax);

            return ComputeShortGlobalEstimatedDeliveryDate(null, articleAvailabilities.Yield(), true, prefix);
        }

        public IEnumerable<string> GetShortGlobalEstimatedDeliveryDateForStore(IEnumerable<Article> articles, IEnumerable<ShipToStoreAvailability> availabilities, int shippingMethodId, int storeId)
        {
            var clickAndCollectCount = availabilities.OfType<ClickAndCollectShipToStoreAvailability>().Count();
            var shippedAvailabilities = availabilities.OfType<ShippingShipToStoreAvailability>();

            if (clickAndCollectCount == 0 && !shippedAvailabilities.Any())
            {
                return Enumerable.Empty<string>();
            }

            var output = new List<string>();

            if (clickAndCollectCount > 0)
            {
                if (clickAndCollectCount > 1)
                {
                    output.Add(_applicationContext.GetMessage("orderpipe.pop.shipping.availability.products.1hour", clickAndCollectCount));
                }
                else
                {
                    output.Add(_applicationContext.GetMessage("orderpipe.pop.shipping.availability.product.1hour"));
                }
            }

            if (shippedAvailabilities.Any())
            {
                var articleAvailabilities = shippedAvailabilities.Select(a => new PopAvailabilityProcessorInput(a.FindArticle(articles), a.DeliveryDayMin, a.DeliveryDayMax, a.ShippingMethodId)).ToList();

                var prefix = string.Empty;
                var timeNdd = string.Empty;

                if (clickAndCollectCount == 0)
                {
                    prefix = _applicationContext.GetMessage("orderpipe.pop.shipping.availability.prefix.shop");
                    timeNdd = _applicationContext.GetMessage("orderpipe.pop.shipping.availability.prefix.shop.14H");
                }

                var shortGlobalEstimatedDeliveryDate = ComputeShortGlobalEstimatedDeliveryDate(null, articleAvailabilities, clickAndCollectCount == 0, prefix);

                if (shippingMethodId == 0)
                {
                    shippingMethodId = articleAvailabilities.FirstOrDefault(x => x.ShippingMethodId != null)?.ShippingMethodId ?? 0;
                }

                if (_shippingMethodIds.Contains(shippingMethodId))
                {
                    shortGlobalEstimatedDeliveryDate += timeNdd;
                }

                if (clickAndCollectCount == 0)
                {
                    output.Add(shortGlobalEstimatedDeliveryDate);
                }
                else if (!string.IsNullOrWhiteSpace(shortGlobalEstimatedDeliveryDate))
                {
                    if (articleAvailabilities.Count > 1)
                    {
                        output.Add(_applicationContext.GetMessage("orderpipe.pop.shipping.availability.products", articleAvailabilities.Count, shortGlobalEstimatedDeliveryDate));
                    }
                    else
                    {
                        output.Add(_applicationContext.GetMessage("orderpipe.pop.shipping.availability.product", shortGlobalEstimatedDeliveryDate));
                    }
                }
            }

            return output;
        }

        private string ComputeShortGlobalEstimatedDeliveryDate(int? shippingMethodId, IEnumerable<PopAvailabilityProcessorInput> availabilities, bool homogenous, string prefix)
        {
            return ComputeShortGlobalEstimatedDeliveryDate(shippingMethodId, availabilities, homogenous, prefix, false);
        }

        private string GetPrefix(IEnumerable<PopAvailabilityProcessorInput> availabilities)
        {
            var prefix = _applicationContext.GetMessage("orderpipe.pop.shipping.availability.prefix");

            var dateMax = availabilities.MaxOrDefault(a => a.DateMax);
            var dateMin = availabilities.MinOrDefault(a => a.DateMin);

            if (availabilities.All(a => a.DateMax == dateMax) &&
                availabilities.All(a => a.DateMin == dateMin) &&
                dateMax.Value.Date == DateTime.Today &&
                dateMin.Value.Date == DateTime.Today)
            {
                var prefixSameDayDelivery = _applicationContext.GetMessage("orderpipe.pop.shipping.availability.sdd.prefix");

                if (!string.IsNullOrEmpty(prefixSameDayDelivery))
                {
                    return prefixSameDayDelivery;
                }
            }

            if (availabilities.All(a => a.ShippingMethodId == Constants.ShippingMethods.ChronopostDelivery) &&
                availabilities.All(a => a.DateMax == dateMax) &&
                availabilities.All(a => a.DateMin == dateMin) &&
                dateMin.Value.Date == dateMax.Value.Date)
            {
                var culture = System.Globalization.CultureInfo.GetCultureInfo(_applicationContext.GetCulture());

                return culture.DateTimeFormat.GetDayName(dateMin.Value.DayOfWeek);
            }

            return prefix;
        }

        public IEnumerable<PopAvailabilityProcessorOutput> GetLongGlobalEstimatedDeliveryDateForStore(IEnumerable<Article> articles, IEnumerable<ShipToStoreAvailability> availabilities, int shippingMethodId, int storeId, bool excludeIfSingle = true)
        {
            var clickAndCollectAvailabilities = availabilities.OfType<ClickAndCollectShipToStoreAvailability>();
            var shippedAvailabilities = availabilities.OfType<ShippingShipToStoreAvailability>();

            var output = new List<PopAvailabilityProcessorOutput>();

            if (clickAndCollectAvailabilities.Any())
            {
                var clickAncCollectArticles = clickAndCollectAvailabilities.Select(c => c.FindArticle(articles));

                var isBeforeXmas = DeliveryBeforeXmas(DateTime.Now, clickAndCollectAvailabilities.First().ShippingMethodId, clickAncCollectArticles);

                output.Add(
                    new PopAvailabilityProcessorOutput(
                        _applicationContext.GetMessage("orderpipe.pop.shipping.availability.short.1hour"),
                        clickAndCollectAvailabilities
                            .Select(a => a.FindArticle(articles))
                                .Select(s => new AvailabilityItemInfo { Id = s.ProductID.GetValueOrDefault(), Title = s.Title }),
                        isBeforeXmas,
                        true));
            }

            if (shippedAvailabilities.Any())
            {
                var articleAvailabilities = shippedAvailabilities.Select(a => new PopAvailabilityProcessorInput(a.FindArticle(articles), a.DeliveryDayMin, a.DeliveryDayMax, a.ShippingMethodId)).ToList();

                var details = GetLongGlobalEstimatedDeliveryDate(articleAvailabilities, false);

                foreach (var detail in details)
                {
                    output.Add(new PopAvailabilityProcessorOutput(detail.Key, detail.Items, detail.BeforeXmas));
                }
            }

            if (output.Count <= 1 && excludeIfSingle)
            {
                return Enumerable.Empty<PopAvailabilityProcessorOutput>();
            }

            return output;
        }

        public IEnumerable<PopAvailabilityProcessorOutput> GetLongGlobalEstimatedDeliveryDate(IEnumerable<PopAvailabilityProcessorInput> availabilities, bool excludeIfSingle = true)
        {
            var estimations = new List<EstimatedDeliveryDate>();

            foreach (var availability in availabilities)
            {
                var availabilityLabelRule = _availabilityLabelRuleFinder.Find(availability.ShippingMethodId);

                var single = availabilityLabelRule.ComputeSingleEstimatedDeliveryDate(availability);


                if (single != null)
                {
                    single.MethodId = availability.ShippingMethodId.HasValue ? (int)availability.ShippingMethodId : 0;

                    var worstDeliveryDate = ComputeWorstDeliveryDate(availability);

                    single.BeforeXmas = DeliveryBeforeXmas(worstDeliveryDate, single.MethodId, availability.Article.Yield());

                    estimations.Add(single);
                }
            }

            estimations = estimations.OrderBy(e => e.Priority).ToList();

            var output = (from estimation in estimations
                          group estimation by estimation.Label into g
                          select new PopAvailabilityProcessorOutput(g.Key, g.Select(i => new AvailabilityItemInfo
                          {
                              Id = i.Prid,
                              OfferRef = i.OfferRef,
                              Title = i.ArticleTitle,
                              MethodId = i.MethodId,
                              EstimatedDeliveryStartDate = i.StartDate,
                              EstimatedDeliveryEndDate = i.EndDate
                          }), g.All(i => i.BeforeXmas))).ToList();

            if (output.Count <= 1 && excludeIfSingle)
            {
                return new List<PopAvailabilityProcessorOutput>();
            }

            return output;
        }

        private DateTime? ComputeWorstDeliveryDate(PopAvailabilityProcessorInput availability)
        {

            if (availability != null && availability.DateMax.HasValue)
            {
                return availability.DateMax;
            }

            if (availability != null && availability.DateMin.HasValue)
            {
                return availability.DateMin;
            }

            return null;
        }

        private bool DeliveryBeforeXmas(DateTime? worstDeliveryDate, int shippingMethodId, IEnumerable<Article> articles)
        {
            var eventDate = _deliveredForChristmasService.GetEventDateOfCurrentPhase();

            var elligibleshippingMethods = _deliveredForChristmasService.GetElligibleShippingMethods();

            if (eventDate.HasValue && articles != null && articles.Any())
            {
                return worstDeliveryDate.HasValue && worstDeliveryDate.Value < eventDate.Value &&
                    (elligibleshippingMethods?.Contains(shippingMethodId)).GetValueOrDefault() &&
                    (articles.All(a => _deliveredForChristmasService.CanArticleBeDelivredBeforChristmas(a)));
            }
            else
            {
                return false;
            }
        }

        public IEnumerable<PopAvailabilityProcessorOutput<PopAvailabilityProcessorOutput>> SortPostalLongGlobalEstimatedDeliveryDate(List<PopAvailabilityProcessorOutput<PopAvailabilityProcessorOutput>> inputs)
        {
            var outputsTmp = new List<PopAvailabilityProcessorOutput<PopAvailabilityProcessorOutput>>();
            var outputs = new List<PopAvailabilityProcessorOutput<PopAvailabilityProcessorOutput>>();

            foreach (var input in inputs)
            {
                if (outputsTmp.Any(o => o.Key == input.Key))
                {
                    var output = outputsTmp.FirstOrDefault(o => o.Key == input.Key);
                    var listTmp = new List<PopAvailabilityProcessorOutput>();
                    listTmp.AddRange(output.Items);
                    listTmp.AddRange(input.Items);
                    var newOutput = new PopAvailabilityProcessorOutput<PopAvailabilityProcessorOutput>(output.Key, listTmp);
                    outputsTmp.RemoveAll(o => o.Key == output.Key);
                    outputsTmp.Add(newOutput);
                }
                else
                {
                    outputsTmp.Add(input);
                }
            }

            foreach (var output in outputsTmp)
            {
                var newOutputItems = new List<PopAvailabilityProcessorOutput>();

                var results = (from dle in output.Items
                               group dle.Items by dle.Key into d
                               select new { Key = d.Key, List = d.SelectMany(x => x) });

                foreach (var result in results)
                {
                    var beforeXmas = false;

                    if (output.Items.Any(x => x.Key == result.Key))
                    {
                        beforeXmas = output.Items.Where(x => x.Key == result.Key).All(a => a.BeforeXmas);
                    }

                    newOutputItems.Add(new PopAvailabilityProcessorOutput(result.Key, result.List, beforeXmas));
                }
                outputs.Add(new PopAvailabilityProcessorOutput<PopAvailabilityProcessorOutput>(output.Key, newOutputItems));
            }

            return outputs;
        }

        public string GetShortGlobalEstimatedDeliveryDateForStore(StandardArticle standardArticle, IEnumerable<ShipToStoreAvailability> availabilities)
        {
            return GetShortGlobalEstimatedDeliveryDateForStore(null, standardArticle, availabilities);
        }

        private string ComputeShortGlobalEstimatedDeliveryDate(int? shippingMethodId, IEnumerable<PopAvailabilityProcessorInput> availabilities, bool homogenous, string prefix, bool? hasManyDeliveryDates)
        {
            if (!availabilities.Any())
            {
                return null;
            }

            var availabilityLabelRule = _availabilityLabelRuleFinder.Find(shippingMethodId);

            return availabilityLabelRule.Get(availabilities, homogenous, prefix, hasManyDeliveryDates);
        }
    }
}
