using FnacDirect.Contracts.Online.Model;
using FnacDirect.Technical.Framework.Web.Configuration;
using FnacDirect.Technical.Framework.Web.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Basket
{
    public class PromotionTemplateViewModelBuilder : IPromotionTemplateViewModelBuilder
    {
        private readonly IApplicationContext _applicationContext;

        public PromotionTemplateViewModelBuilder(IApplicationContext applicationContext)
        {
            _applicationContext = applicationContext;
        }

        public IEnumerable<PromotionTemplateViewModel> Build(IEnumerable<Promotion> promotions)
        {
            if (promotions == null || !promotions.Any())
            {
                return Enumerable.Empty<PromotionTemplateViewModel>();
            }

            var opcPush = _applicationContext.ConfigurationOpcPush;

            var templates = new List<PromotionTemplateViewModel>();

            foreach (var promo in promotions)
            {
                var template = BuildModel(promo, opcPush.Select(x => x.Value));

                if (template != null)
                {
                    template.IsFirst = !templates.Where(x => !x.IsLongLabel).Any();
                    templates.Add(template);
                }

                if (templates.Count >= 2)
                {
                    break;
                }
            }

            return templates;
        }

        private PromotionTemplateViewModel BuildModel(Promotion promotion, IEnumerable<OpcPush> opcPush)
        {
            var push = opcPush.FirstOrDefault(x => x.DisplayTemplate == promotion.DisplayTemplate);

            if (push == null || !push.Visibility.Contains(VisibilityPlace.PIPE))
            {
                return null;
            }

            var model = new PromotionTemplateViewModel
            {
                CssClass = push.CssClass,
                Label = push.PipeUseLongLabel ? promotion.LongWording : promotion.ShortWording,
                IsLongLabel = push.PipeUseLongLabel,
                Comment = promotion.ArticleText,
                ShoppingCartText = promotion.ShoppingCartText
            };

            if (!push.PipeUseLongLabel && string.IsNullOrEmpty(model.Label))
            {
                model.Label = push.ShortLabel;
            }

            return model;
        }

        public List<RichBasketItemViewModel> CleanDeezerPromotions(List<RichBasketItemViewModel> basketItemViewModelList)
        {
            Func<PromotionTemplateViewModel, bool> filter = delegate (PromotionTemplateViewModel t)
            {
                return
                  (t.ShoppingCartText != null && t.ShoppingCartText.Contains(Constants.ShoppingCardDeezer))
                  ||
                  (t.Label != null && t.Label.Contains(Constants.ShoppingCardDeezer));
            };

            var basketItemViewModelListDeezer = basketItemViewModelList.Where(p => p.Price != null && p.Price.Templates.Any(filter));

            if (basketItemViewModelListDeezer != null && basketItemViewModelListDeezer.Count() > 0)
            {
                var basketItemList = basketItemViewModelListDeezer.Where(i => !i.IsFnacPlusCard && !i.IsFnacPlusTryCard).ToList();
                if (basketItemViewModelListDeezer.Any(i => i.IsFnacPlusCard || i.IsFnacPlusTryCard))
                {
                    foreach (var basketItem in basketItemList)
                    {
                        var template = basketItem.Price.Templates.Where(filter).FirstOrDefault();
                        template.IsHidden = true;
                    }
                }
                else
                {
                    for (var i = 1; i < basketItemList.Count(); i++)
                    {
                        var deezerTemplate = basketItemList[i].Price.Templates.Where(filter).FirstOrDefault();
                        if (deezerTemplate != null)
                        {
                            deezerTemplate.IsHidden = true;
                        }
                    }
                }
            }

            return basketItemViewModelList;
        }
    }
}
