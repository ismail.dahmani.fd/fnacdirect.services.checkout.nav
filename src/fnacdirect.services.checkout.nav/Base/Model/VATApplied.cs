using System;

namespace FnacDirect.OrderPipe.Base.Model
{
    /// <summary>
    /// Repr�sente une TVA appliqu�e sur un article
    /// </summary>
    [Serializable]
    public class VATApplied
    {
        private decimal _Rate = 0;
        public decimal Rate
        {
            get { return _Rate; }
            set { _Rate = value; }
        }

        private decimal _Total = 0;
        public decimal Total
        {
            get { return _Total; }
            set { _Total = value; }
        }

        public string RateLiteral
        {
            get { return string.Format("TVA {0}", _Rate >= 0 ? string.Format("{0}%", _Rate.ToString("#0.##")) : string.Empty); }
        }

        public VATApplied()
        {
        }
        public VATApplied(decimal rate, decimal total)
        {
            Rate = rate;
            Total = total;
        }
    }
}
