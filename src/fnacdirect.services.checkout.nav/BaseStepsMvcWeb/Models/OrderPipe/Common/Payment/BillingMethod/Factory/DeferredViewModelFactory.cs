﻿using FnacDirect.OrderPipe.Base.Model;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Common.Payment.BillingMethodFactories
{
    public class DeferredViewModelFactory : BasePaymentViewModelFactory<DefferedPaymentBillingMethod>
    {
        public override BillingMethodViewModel Get(string tabName, PaymentMethod paymentMethod, OPContext opContext, PopOrderForm popOrderForm)
        {
            var data = popOrderForm.GetPrivateData<DeferredPaymentEligibilityData>(false);

            var viewModel = new DeferredPaymentBillingMethodViewModel();

            if (data != null)
            {
                viewModel.IsEligible = data.IsEligible;
                viewModel.PaymentDelay = data.PaymentDelay;
                viewModel.DeferredAccountNumber = data.DeferredAccountNumber;
            }

            viewModel.Display = true;

            return viewModel;
        }
    }
}
