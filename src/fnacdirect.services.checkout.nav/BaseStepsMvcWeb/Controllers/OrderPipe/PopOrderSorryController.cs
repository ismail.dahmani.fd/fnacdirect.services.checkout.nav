using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Sorry;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.DataLayer;
using FnacDirect.OrderPipe.BaseMvc.Web.Services.Pop.DataLayer;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Controllers.Pop
{
    public class PopOrderSorryController: PopBaseController
    {
        private readonly OPContext _opContext;
        private readonly IOrderSorryViewModelBuilder _orderSorryViewModelBuilder;
        private readonly IDataLayerService _dataLayerService;

        public PopOrderSorryController(OPContext opContext, IOrderSorryViewModelBuilder orderSorryViewModelBuilder, IDataLayerService dataLayerService)
        {
            _opContext = opContext;
            _orderSorryViewModelBuilder = orderSorryViewModelBuilder;
            _dataLayerService = dataLayerService;
        }

        [HttpGet]
        public ViewResult Index()
        {
            var popOrderForm = _opContext.OF as PopOrderForm;

            var ordersorryViewModel = _orderSorryViewModelBuilder.Build(_opContext, popOrderForm);

            var trackingModel = new DatalayerViewModel()
            {
                PageType = PageTypeEnum.OrderSorry,
                PopOrderForm = popOrderForm,
                OpContext = _opContext
            };

            var viewResult = View(ordersorryViewModel);
            //Datalayer and Tracking switchabled
            _dataLayerService.ComputeTransaction(trackingModel, viewResult as ViewResultBase, ControllerContext);

            return viewResult;
        }
    }
}
