using FnacDirect.Contracts.Online.Model;
using FnacDirect.Front.WebBusiness;

namespace FnacDirect.OrderPipe.Base.Proxy.Articles
{
    public class W3ArticleService : IW3ArticleService
    {
        public W3Article GetArticle(ArticleReference articleReference)
        {
            return ArticleFactoryNew.GetArticle(articleReference);
        }

        public W3Article GetArticleNoPrice(ArticleReference articleReference)
        {
            return ArticleFactoryNew.GetArticle(articleReference, true, false, false, true);
        }
    }
}
