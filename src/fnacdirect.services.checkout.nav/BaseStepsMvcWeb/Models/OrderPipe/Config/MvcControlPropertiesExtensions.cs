﻿using System.Collections.Generic;
using System.Web.Routing;

namespace FnacDirect.OrderPipe.Config
{
    public static class MvcControlPropertiesExtensions
    {
        public static RouteValueDictionary ToRouteValueDictionary(this List<MvcControlProperty> mvcControlProperties)
        {
            var routeValueDictionary = new RouteValueDictionary();

            foreach (var mvcControlProperty in mvcControlProperties)
            {
                routeValueDictionary.Add(mvcControlProperty.Name, mvcControlProperty.Value);
            }

            return routeValueDictionary;
        }
    }
}