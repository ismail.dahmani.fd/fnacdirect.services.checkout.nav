using FnacDarty.FnacCom.DataLayer.Models.Request;
using FnacDirect.Context;
using FnacDirect.Contracts.Online.Model;
using FnacDirect.Front.WebBusiness;
using FnacDirect.OrderPipe.Base.BaseModel.Model;
using FnacDirect.OrderPipe.Base.BaseModel.Model.OrderDetails;
using FnacDirect.OrderPipe.Base.Business;
using FnacDirect.OrderPipe.Base.LineGroups;
using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.OrderPipe.Base.Model.ThankYou;
using FnacDirect.Technical.Framework.Caching2;
using FnacDirect.Technical.Framework.CoreServices;
using FnacDirect.Technical.Framework.Web;
using FnacDirect.Technical.Framework.Web.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.DataLayer
{
    public class DataLayerBuilder : IDataLayerBuilder
    {
        private readonly IApplicationContext _applicationContext;
        private readonly IVisitorAdapter _visitorAdapter;
        private readonly ICacheService _cacheService;
        private readonly Base.Proxy.Articles.IW3ArticleService _w3ArticleService;
        private readonly IBillingMethodsManager _billingMethodsManager;
        private readonly IExecutionContextProvider _executionContextProvider;

        public DataLayerBuilder(
            IApplicationContext applicationContext,
            IVisitorAdapter visitorAdapter,
            ICacheService cacheService,
            Base.Proxy.Articles.IW3ArticleService w3ArticleService,
            IBillingMethodsManager billingMethodsManager,
            IExecutionContextProvider executionContextProvider
        )
        {
            _applicationContext = applicationContext;
            _visitorAdapter = visitorAdapter;
            _cacheService = cacheService;
            _w3ArticleService = w3ArticleService;
            _billingMethodsManager = billingMethodsManager;
            _executionContextProvider = executionContextProvider;
        }

        public UserRequest BuildUserRequest()
        {
            var visitor = _visitorAdapter.GetVisitor();
            var customer = visitor.Customer;
            var contractCustomer = customer.ContractDTO;

            return new UserRequest()
            {
                UserProfiles = GetUserProfiles(),
                Segment = new UserRequest.SegmentRequest()
                {
                    SubscriptionType = GetSubscriptionType(),
                    SubscriptionOffer = GetSubscriptionOffer(),
                    SubscriptionStatus = GetSubscriptionStatus(),
                    SubscriptionStartDate = customer.MembershipContract?.AdhesionStartDate,
                    SubscriptionEndDate = customer.MembershipContract?.AdhesionEndDate,
                    SegmentCodes = contractCustomer.Segments.Where(x => x.Key != "SampleIndicator").Select(x => x.Key)// See also segmentationService.GetSegmentsByCustomer(customer.ContractDTO.Identity).Select(x => x.Code)
                }
            };

            IEnumerable<UserRequest.UserProfileRequest> GetUserProfiles()
            {
                yield return new UserRequest.UserProfileRequest
                {
                    ProfileId = visitor.UidAsString(),
                    UserName = GetUserName(),
                    Email = customer.DTO.EMail,
                    IsIdentified = visitor.IsIdentified,
                    IsAuthenticated = visitor.IsAuthenticated
                };
            }

            SubscriptionType GetSubscriptionType()
            {
                if (!customer.IsValidOrTryAdherent)
                    return SubscriptionType.None;
                if (customer.IsFnacPlus && customer.IsAdherentOne)
                    return SubscriptionType.FnacOnePlus;
                if (customer.IsFnacPlus)
                    return SubscriptionType.FnacPlus;
                if (customer.IsAdherentOne)
                    return SubscriptionType.FnacOne;
                return SubscriptionType.Fnac;
            }

            SubscriptionOffer GetSubscriptionOffer()
            {
                if (customer.IsValidTryAdherent)
                    return SubscriptionOffer.FreeTrial;
                if (customer.IsValidAdherent)
                    return SubscriptionOffer.Paid;
                return SubscriptionOffer.None;
            }

            SubscriptionStatus GetSubscriptionStatus()
            {
                if (customer.IsValidOrTryAdherent)
                    return SubscriptionStatus.Active;
                return SubscriptionStatus.NonApplicable;
            }
        }

        public PageRequest BuildPageRequest(DatalayerViewModel model, ViewResultBase viewResultBase, ControllerContext controllerContext)
        {
            var executionContext = _executionContextProvider.GetCurrentExecutionContext();
            var pageName = GetPageName(model.PageType);
            var pipeRoute = GetPipeRoute();

            return new PageRequest()
            {
                Category = GetCategory(),
                ServerName = executionContext.GetMachineName(),
                Version = GetVersion()?.Version,
                DeviceType = GetDeviceType(),
                EnvironmentType = GetEnvironmentType(),
                MarketId = GetMarketId(),
                SiteId = GetSiteId(),
                PageName = pageName,
                PageId = GetPageId(),
                Language = _applicationContext.GetCulture(),
                DestinationUrl = GetDestinationUrl(_applicationContext.GetRequest().RawUrl),
                ReferringUrl = GetReferringUrl(),
                UserAgent = executionContext.GetUserAgent(),
                ClientIP = GetClientIP(),
                BreadCrumbs = new List<string>(){
                    _applicationContext.GetMessage("core.common.breadcrumb.home"),
                    pageName,
                },
            };

            string GetPageId()
            {
                return $"orderpipe-{GetPageType()}";
            }

            string GetPageType()
            {
                switch (model.PageType)
                {
                    case PageTypeEnum.PopOneClick:
                    case PageTypeEnum.PopOnePage:
                        return "checkout-onepage";

                    case PageTypeEnum.PopShipping:
                        return "checkout-shipping";

                    case PageTypeEnum.PopPayment:
                        return "checkout-payment";

                    case PageTypeEnum.ScoCrypto:
                        return "checkout-csc-verification";

                    case PageTypeEnum.PopBasket:
                        return "cart";

                    case PageTypeEnum.ScoOrderThankYou:
                    case PageTypeEnum.OrderThankYou:
                        return "order-complete";

                    case PageTypeEnum.OrderSorry:
                        return "order-error";
                }

                return null;
            }

            PageRequest.PageCategoryRequest GetCategory()
            {
                return new PageRequest.PageCategoryRequest()
                {
                    PageTemplate = GetPageTemplate(),
                    PageType = GetPageType(),
                    PrimaryCategoryId = pipeRoute,
                };

                string GetPageTemplate()
                {
                    // Based on https://github.com/aspnetwebstack/aspnetwebstack/blob/a0b7fe4a95fa29a273b66c6273ee7430ba454754/src/System.Web.Mvc/ViewResultBase.cs#L82-L93
                    var viewName = string.IsNullOrEmpty(viewResultBase.ViewName) ? controllerContext.RouteData.GetRequiredString("action") : viewResultBase.ViewName;
                    var view = viewResultBase.View;
                    if (view == null)
                    {
                        switch (viewResultBase)
                        {
                            case ViewResult viewResult:
                                // Based on https://github.com/aspnetwebstack/aspnetwebstack/blob/a0b7fe4a95fa29a273b66c6273ee7430ba454754/src/System.Web.Mvc/ViewResult.cs#L21
                                view = viewResult.ViewEngineCollection.FindView(controllerContext, viewName, viewResult.MasterName).View;
                                break;

                            case PartialViewResult partialViewResult:
                                // Based on https://github.com/aspnetwebstack/aspnetwebstack/blob/a0b7fe4a95fa29a273b66c6273ee7430ba454754/src/System.Web.Mvc/PartialViewResult.cs#L13
                                view = partialViewResult.ViewEngineCollection.FindPartialView(controllerContext, viewName).View;
                                break;
                        }
                    }

                    return (view as BuildManagerCompiledView)?.ViewPath;
                }
            }
            string GetPipeRoute()
            {
                switch (model.PopOrderForm.GetRoute())
                {
                    case OFRoute.Full:
                        return Constants.PipeRoute.FULL;
                    case OFRoute.OnePage:
                        return Constants.PipeRoute.ONEPAGE;
                    case OFRoute.OneClick:
                        return Constants.PipeRoute.ONECLICK;
                    default:
                        return Constants.PipeRoute.UNKNOWN;
                }
            }
            DeviceType GetDeviceType()
            {
                if (_applicationContext.IsMobile)
                    return DeviceType.MobileWeb;
                if (_applicationContext.IsMobileApplication)
                    return DeviceType.MobileApp;

                return DeviceType.DesktopWeb;
            }

            IPAddress GetClientIP()
            {
                var userHostAddress = executionContext.GetUserHostAddress();
                IPAddress address = null;
                if (userHostAddress != null)
                {
                    var ipStr = userHostAddress.Split(':')[0].Trim();// only IP without port
                    // Note: check IP with IPAddress.TryParse() could validate "0.0.0.0", "127.000000000.0.1", "5"
                    // See https://stackoverflow.com/questions/11412956/what-is-the-best-way-of-validating-an-ip-address
                    IPAddress.TryParse(ipStr, out address);
                }
                return address;
            }

            Uri GetDestinationUrl(string rawUrl) => new Uri($"https{Uri.SchemeDelimiter}{HttpContext.Current.Request.Url.Host}{rawUrl}");

            Uri GetReferringUrl()
            {
                try
                {
                    return _applicationContext.GetRequest().UrlReferrer;
                }
                catch (Exception ex)
                {
                    ex.Data["Method"] = "InitPageRequest";
                    Logging.Current.WriteWarning(ex);
                }
                return null;
            }

            SiteId? GetSiteId()
            {
                switch (_applicationContext.GetSiteContext().SiteID)
                {
                    case FnacSites.FNACPRO:
                        return SiteId.FnacPro;

                    default:
                        return SiteId.FnacCom;
                }
            }

            MarketId? GetMarketId()
            {
                switch (_applicationContext.GetSiteContext().Market)
                {
                    case Constants.LocalCountry.BELGIQUE:
                        return MarketId.Be;

                    case Constants.LocalCountry.SUISSE:
                        return MarketId.Ch;

                    case Constants.LocalCountry.ESPAGNE:
                        return MarketId.Es;

                    case Constants.LocalCountry.PORTUGAL:
                        return MarketId.Pt;

                    default:
                        return MarketId.Fr;
                }
            }

            EnvironmentType? GetEnvironmentType()
            {
                var env = _applicationContext.GetAppSetting("TechEnvFamily");
                switch (env.ToUpper())
                {
                    case Constants.Environement.Production:
                        return EnvironmentType.Production;

                    case Constants.Environement.Recette:
                        return EnvironmentType.Integration;

                    default:
                        return EnvironmentType.Dev;
                }
            }

            FnacVersionViewModel GetVersion()
            {
                var cacheKey = new CacheKey("FnacVersion");
                var fnacVersionConfiguration = _cacheService.Get<FnacVersionViewModel>(cacheKey);
                if (fnacVersionConfiguration == null)
                {
                    var path = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "FnacVersion.json");
                    try
                    {
                        fnacVersionConfiguration = JsonConvert.DeserializeObject<FnacVersionViewModel>(File.ReadAllText(path));
                        _cacheService.Set(cacheKey, fnacVersionConfiguration);
                    }
                    catch (Exception ex)
                    {
                        ex.Data["Method"] = "GetFnacVersionConfiguration";
                        Logging.Current.WriteWarning(ex);
                    }
                }
                return fnacVersionConfiguration;
            }
        }

        public CartRequest BuildCartRequest(PopOrderForm popOrderForm)
        {
            var shippingChoice = popOrderForm.GetMainShippingChoice();

            return new CartRequest
            {
                CartId = popOrderForm.UserContextInformations.UID,
                Price = new CartRequest.CartPriceRequest()
                {
                    BasePrice = popOrderForm.GlobalPrices.TotalPriceEurNoVat,
                    Currency = GetCurrency(),
                    PriceWithTax = popOrderForm.GlobalPrices.TotalPriceEur.GetValueOrDefault(),
                    Shipping = popOrderForm.GlobalPrices.TotalShipPriceUserEur.GetValueOrDefault(),
                    ShippingMethod = GetShippingLabel(shippingChoice?.MainShippingMethodId),
                    ShippingMethodId = shippingChoice?.MainShippingMethodId.ToString(),
                    TaxRate = popOrderForm.GlobalPrices.ShipVATRate,
                    CartTotal = popOrderForm.GlobalPrices.TotalPriceEur.GetValueOrDefault(),
                },
                Items = BuildItems(popOrderForm)
            };
        }

        public TransactionRequest BuildTransactionRequest(DatalayerViewModel model)
        {
            var popOrderForm = model.PopOrderForm;
            var trackingData = popOrderForm.GetPrivateData<TrackingData>(false)?.Value;
            var mainPaymentMethodRequest = GetMainPaymentMethodRequests(model);
            var paymentMethodRequests = GetPaymentMethodRequests(model).ToList();

            return new TransactionRequest
            {
                TransactionId = popOrderForm.OrderGlobalInformations.MainOrderUserReference,
                Attributes = new TransactionRequest.AttributesRequest()
                {
                    CartId = popOrderForm.UserContextInformations.UID,
                    PaymentMethod = mainPaymentMethodRequest.PaymentMethod,
                    PaymentMethodId = mainPaymentMethodRequest.PaymentMethodId,
                    PaymentMethodStorage = mainPaymentMethodRequest.PaymentMethodStorage,
                    SplitPaymentMethods = paymentMethodRequests
                },
                Profile = new PurchaserProfileRequest()
                {
                    BillingAddress = new AddressRequest()
                    {
                        Line1 = trackingData?.OrderBillingLine1,
                        Line2 = trackingData?.OrderBillingLine2,
                        City = trackingData?.OrderBillingCity,
                        PostalCode = trackingData?.OrderBillingPostalCode,
                        Country = trackingData?.OrderBillingCountry
                    },
                    ProfileInfo = new ProfileInfoRequest()
                    {
                        ProfileId = _visitorAdapter.GetVisitor()?.UidAsString(),
                        UserName = GetUserName(),
                    },
                    // main shipping address only
                    ShippingAddress = new AddressRequest()
                    {
                        Name = trackingData?.Name,
                        Line1 = trackingData?.ShippingAddressLine1,
                        Line2 = trackingData?.ShippingAddressLine2,
                        City = trackingData?.ShippingAddressCity,
                        PostalCode = trackingData?.ShippingAddressPostalCode,
                        Country = trackingData?.ShippingAddressCountry
                    }
                },
                Total = new TransactionRequest.TotalRequest()
                {
                    TransactionTotal = (trackingData?.TotalBasketPrice + trackingData?.TotalShippingPrice).GetValueOrDefault(),
                    BasePrice = (trackingData?.TotalBasketPriceNoVAT).GetValueOrDefault(0),
                    Currency = GetCurrency(),
                    PriceWithTax = (trackingData?.TotalPrice).GetValueOrDefault(0),
                    Shipping = (trackingData?.TotalShippingPrice).GetValueOrDefault(0),
                    ShippingMethod = trackingData?.FirstShippingMethod,
                },
                Items = BuildItems(popOrderForm)
            };
        }

        public IEnumerable<OrderRequest> BuildOrderRequests(DatalayerViewModel model)
        {
            var thankYouOrderDetailData = model.PopOrderForm.GetPrivateData<ThankYouOrderDetailData>(false) ?? new ThankYouOrderDetailData();
            return thankYouOrderDetailData.ThankYouOrdersDetails.Select(orderDetail => BuildOrderRequest(orderDetail, model));
        }

        internal AddressRequest BuildBillingAddressRequest(BillingAddress baseAddress)
        {
            return new AddressRequest()
            {
                Line1 = baseAddress?.AddressLine1,
                Line2 = baseAddress?.AddressLine2,
                City = baseAddress?.City,
                PostalCode = baseAddress?.ZipCode,
                Country = baseAddress?.CountryLabel
            };
        }

        internal AddressRequest BuildShippingAddressRequest(OrderDetailBaseAddress baseAddress)
        {
            var address = new AddressRequest()
            {
                //Address2, address3 concat in address
                Line1 = baseAddress?.Address,
                City = baseAddress?.City,
                Country = baseAddress?.Country,
                PostalCode = baseAddress?.ZipCode
            };

            if (baseAddress is OrderDetailRelayAddress relayAddress)
            {
                address.Name = relayAddress?.Name;
            }
            if (baseAddress is OrderDetailShopAddress shopAddress)
            {
                address.Name = shopAddress?.Name;
            }
            if (baseAddress is OrderDetailPostalAddress postalAddress)
            {
                address.Name = $"{postalAddress?.FirstName} {postalAddress?.LastName}";
            }

            return address;
        }

        internal PriceRequest GetProductPrice(Base.Model.Article article, TrackingProduct trackingArticle)
        {
            if (trackingArticle != null)
            {
                var shippingId = trackingArticle.IsDemat ? (int)ShippingMethodEnum.Telechargement : trackingArticle.ShipMethod;
                return new PriceRequest
                {
                    BasePrice = (trackingArticle?.PriceNoVATEur).GetValueOrDefault(0),
                    Currency = GetCurrency(),
                    TaxRate = (trackingArticle?.VATRate).GetValueOrDefault(0),
                    PriceWithTax = (trackingArticle?.PriceUserEur).GetValueOrDefault(0),
                    ShippingMethod = !string.IsNullOrEmpty(trackingArticle.ShippingArticleMode) ? trackingArticle.ShippingArticleMode : GetShippingLabel(shippingId),
                    ShippingMethodId = shippingId.ToString(),
                    Shipping = trackingArticle?.ShipPriceUserEur,
                };
            }
            if (article is StandardArticle stdArticle)
            {
                var shippingId = GetIsDemat(stdArticle) ? (int)ShippingMethodEnum.Telechargement : stdArticle.ShipMethod;
                return new PriceRequest
                {
                    BasePrice = stdArticle.PriceNoVATEur.GetValueOrDefault(0),
                    Currency = GetCurrency(),
                    TaxRate = stdArticle.VATRate,
                    PriceWithTax = stdArticle.PriceUserEur.GetValueOrDefault(0),
                    ShippingMethod = !string.IsNullOrEmpty(stdArticle.ShipMethodLabel) ? stdArticle.ShipMethodLabel : GetShippingLabel(shippingId),
                    ShippingMethodId = shippingId.ToString(),
                    Shipping = stdArticle?.ShipPriceDBEur,
                };
            }
            else if (article is MarketPlaceArticle mpArticle)
            {
                return new PriceRequest
                {
                    BasePrice = mpArticle.PriceNoVATEur.GetValueOrDefault(0),
                    Currency = GetCurrency(),
                    TaxRate = mpArticle.VATRate,
                    PriceWithTax = mpArticle.PriceUserEur.GetValueOrDefault(0),
                    ShippingMethod = !string.IsNullOrEmpty(mpArticle.ShipMethodLabel) ? mpArticle.ShipMethodLabel : GetShippingLabel(mpArticle.ShipMethod),
                    ShippingMethodId = mpArticle?.ShipMethod.ToString(),
                    Shipping = mpArticle?.ShipPriceDBEur,
                };
            }
            return null;
        }

        internal ProductRequest.AttributesRequest GetAttributeRequest(Base.Model.Article article, TrackingProduct trackingArticle, ILineGroupHeader lineGroup)
        {
            if (trackingArticle != null)
            {
                return new ProductRequest.AttributesRequest()
                {
                    Catalog = (ProductCatalog?)trackingArticle.ReferentialId ?? ProductCatalog.NewRef,
                    OfferId = trackingArticle.OfferId?.ToString(),
                    SellerType = GetSellerType(),
                    SellerId = trackingArticle.SellerId?.ToString(),
                    SellerName = trackingArticle.SellerName,
                    ProductNature = trackingArticle.IsDemat ? ProductNature.Digital : ProductNature.Physic,
                    ProductType = FnacDarty.FnacCom.DataLayer.Models.Request.ProductType.Good,
                    Condition = (Condition)trackingArticle.ProductStatus
                };
            }
            else if (article is StandardArticle stdArticle)
            {
                var sellerFnac = lineGroup?.Seller;
                return new ProductRequest.AttributesRequest()
                {
                    Catalog = ProductCatalog.NewRef,
                    OfferId = stdArticle?.OfferRef?.ToString(),
                    SellerType = GetSellerType(),
                    SellerId = Guid.Empty.ToString(),
                    SellerName = sellerFnac?.DisplayName ?? sellerFnac?.Type.ToString(),
                    ProductNature = GetIsDemat(stdArticle) ? ProductNature.Digital : ProductNature.Physic,
                    ProductType = FnacDarty.FnacCom.DataLayer.Models.Request.ProductType.Good,
                    Condition = Condition.New
                };
            }
            else if (article is MarketPlaceArticle mpArticle)
            {
                return new ProductRequest.AttributesRequest()
                {
                    Catalog = (ProductCatalog?)mpArticle.Referentiel ?? ProductCatalog.NewRef,
                    OfferId = mpArticle?.Offer?.Reference.ToString(),
                    SellerType = GetSellerType(),
                    SellerId = mpArticle?.Offer?.Seller?.Reference.ToString(),
                    SellerName = mpArticle?.Offer?.Seller?.Company,
                    ProductNature = ProductNature.Physic,
                    ProductType = FnacDarty.FnacCom.DataLayer.Models.Request.ProductType.Good,
                    Condition = (Condition)mpArticle?.Offer?.ProductStatus
                };
            }
            return null;
        }

        internal bool GetIsDemat(StandardArticle stdArticle)
        {
            return stdArticle.IsNumerical && (stdArticle.IsEbook || stdArticle.IsDematSoft);
        }

        internal string GetPaymentMethodId(BillingMethod billingMethod)
        {
            if (billingMethod is CreditCardBillingMethod ccbm)
            {
                return $"{billingMethod.BillingMethodId}-{ccbm.TypeId ?? 0}";
            }
            if (billingMethod is VoucherBillingMethod vbm)
            {
                return $"{billingMethod.BillingMethodId}-{vbm.TypeId ?? 0}";
            }

            return billingMethod.BillingMethodId.ToString();
        }

        internal string GetPaymentMethodName(BillingMethod billingMethod)
        {
            switch (billingMethod.BillingMethodId)
            {
                case OgoneCreditCardBillingMethod.IdBillingMethod when billingMethod is OgoneCreditCardBillingMethod occbm:
                    return occbm.TypeLabel;

                case CreditCardBillingMethod.IdBillingMethod when billingMethod is CreditCardBillingMethod ccbm:
                    if (ccbm.NumByPhone.HasValue && ccbm.NumByPhone.Value)
                    {
                        return _applicationContext.GetMessage("orderpipe.pop.paymentmethod.byphone");
                    }
                    return ccbm.TypeLabel;

                default:
                    {
                        return _billingMethodsManager.GetBillingMethodDescriptor(billingMethod.BillingMethodId).Label;
                    }
            }
        }

        #region PRIVATE

        private Currency GetCurrency()
        {
            switch (_applicationContext.GetSiteContext().Market)
            {
                case Constants.LocalCountry.SUISSE:
                    return Currency.SwissFranc;

                default:
                    return Currency.Euro;
            }
        }

        private IEnumerable<ProductRequest> BuildItems(ThankYouOrderDetail orderDetail, PopOrderForm popOrderForm)
        {
            var trackingArticles = popOrderForm.GetPrivateData<TrackingData>(false)?.Value?.Products;
            var logisticLineGroup = popOrderForm.LogisticLineGroups.FirstOrDefault(llg => llg.OrderReference == orderDetail.OrderReference);

            foreach (var article in orderDetail.Articles)
            {
                yield return BuildProduct(article);

                foreach (var service in article.Services)
                {
                    yield return BuildProduct(service);
                }
            }

            ProductRequest BuildProduct(OrderDetailArticleLight article)
            {
                var trackingArticle = trackingArticles?.FirstOrDefault(p => p.ProductId == article.Prid);
                var articleRef = GetArticleRef();
                var w3Article = _w3ArticleService.GetArticleNoPrice(articleRef);

                return new ProductRequest()
                {
                    ProductInformations = GetProductInfos(w3Article),
                    Quantity = article.Quantity,
                    //article is null
                    PriceDetail = GetProductPrice(null, trackingArticle),
                    ProductCategory = GetProductCategory(w3Article),
                    Attributes = GetAttributeRequest(null, trackingArticle, logisticLineGroup)
                };

                ArticleReference GetArticleRef()
                {
                    var prid = trackingArticle != null ? trackingArticle.ProductId : article.Prid;
                    var catalog = trackingArticle != null ? (ArticleCatalog)trackingArticle.ReferentialId : ArticleCatalog.FnacDirect;
                    return new ArticleReference(prid, null, null, null, catalog);
                }
            }
        }

        private IEnumerable<ProductRequest> BuildItems(PopOrderForm popOrderForm)
        {
            var trackingArticles = popOrderForm.GetPrivateData<TrackingData>(false)?.Value?.Products;
            foreach (var lineGroup in popOrderForm.LineGroups)
                foreach (var article in lineGroup.Articles)
                {
                    yield return BuildProduct(article, lineGroup);

                    if (article is StandardArticle standardArticle)
                    {
                        foreach (var service in standardArticle.Services)
                        {
                            var serviceProduct = new ProductRequest();
                            var sellerFnac = lineGroup?.Seller;

                            yield return new ProductRequest()
                            {
                                ProductInformations = new ProductRequest.ProductInfoRequest()
                                {
                                    Id = service?.ID.ToString(),
                                    Name = service?.Title,
                                    Url = TryUri(service?.DetailURL),
                                    Sku = service?.ReferenceGU,
                                },
                                Quantity = service?.Quantity,
                                PriceDetail = new PriceRequest()
                                {
                                    BasePrice = (service?.PriceNoVATEur).GetValueOrDefault(),
                                    Currency = GetCurrency(),
                                    TaxRate = service?.VATRate,
                                    PriceWithTax = (service?.PriceUserEur).GetValueOrDefault(),
                                    //NONE SHIPPING for service
                                },
                                ProductCategory = new CategoryRequest()
                                {
                                    PrimaryCategory = service?.TypeLabel,
                                    PrimaryCategoryId = service?.TypeId.ToString(),
                                },
                                Attributes = new ProductRequest.AttributesRequest()
                                {
                                    Catalog = ProductCatalog.NewRef,
                                    OfferId = service?.OfferRef?.ToString(),
                                    SellerType = GetSellerType(),
                                    SellerId = Guid.Empty.ToString(),
                                    SellerName = sellerFnac?.DisplayName ?? sellerFnac?.Type.ToString(),
                                    ProductNature = ProductNature.Digital,
                                    ProductType = FnacDarty.FnacCom.DataLayer.Models.Request.ProductType.Service,
                                    Condition = Condition.New
                                }
                            };
                        }
                    }
                }

            ProductRequest BuildProduct(Base.Model.Article article, ILineGroupHeader lineGroup)
            {
                var trackingArticle = trackingArticles?.FirstOrDefault(p => p.ProductId == article.ProductID && p.ReferentialId == article.Referentiel);
                var articleRef = GetArticleRef();
                var w3Article = _w3ArticleService.GetArticleNoPrice(articleRef);

                return new ProductRequest()
                {
                    ProductInformations = GetProductInfos(w3Article),
                    Quantity = article.Quantity,
                    PriceDetail = GetProductPrice(article, trackingArticle),
                    ProductCategory = GetProductCategory(w3Article),
                    Attributes = GetAttributeRequest(article, trackingArticle, lineGroup)
                };

                ArticleReference GetArticleRef()
                {
                    var prid = trackingArticle != null ? trackingArticle.ProductId : article.ProductID;
                    var catalog = trackingArticle != null ? (ArticleCatalog)trackingArticle.ReferentialId : (ArticleCatalog)article.Referentiel;
                    return new ArticleReference(prid, null, null, null, catalog);
                }
            }
        }

        private IEnumerable<PayableRequest.PaymentMethodRequest> GetPaymentMethodRequests(DatalayerViewModel model)
        {
            var popOrderForm = model.PopOrderForm;
            var trackingData = popOrderForm.GetPrivateData<TrackingData>(false)?.Value;
            var orderBillingMethods = popOrderForm.OrderBillingMethods ?? trackingData.OrderBillingMethods;

            foreach (var billingMethod in orderBillingMethods)
            {
                yield return new PayableRequest.PaymentMethodRequest()
                {
                    PaymentMethod = GetPaymentMethodName(billingMethod),
                    PaymentMethodId = GetPaymentMethodId(billingMethod),
                    PaymentMethodStorage = GetPaymentMethodStorage(billingMethod, popOrderForm),
                };
            }
        }

        private PayableRequest.PaymentMethodRequest GetMainPaymentMethodRequests(DatalayerViewModel model)
        {
            var popOrderForm = model.PopOrderForm;
            var trackingData = popOrderForm.GetPrivateData<TrackingData>(false)?.Value;
            var mainBillingMethod = popOrderForm.MainBillingMethod ?? trackingData.BillingMethod;

            return new PayableRequest.PaymentMethodRequest()
            {
                PaymentMethod = GetPaymentMethodName(mainBillingMethod),
                PaymentMethodId = GetPaymentMethodId(mainBillingMethod),
                PaymentMethodStorage = GetPaymentMethodStorage(mainBillingMethod, popOrderForm),
            };
        }

        private ProductRequest.ProductInfoRequest GetProductInfos(W3Article w3Article)
        {
            return new ProductRequest.ProductInfoRequest
            {
                Id = w3Article?.ArticleReference?.PRID?.ToString() ?? string.Empty,
                Name = w3Article?.Title,
                Url = TryUri(w3Article?.PageUrl?.ToString()),
                Sku = w3Article?.ArticleReference?.ReferenceGU,
                Manufacturer = w3Article?.Maker?.ValuesWithoutLink,
            };
        }

        private CategoryRequest GetProductCategory(W3Article w3Article)
        {
            return new CategoryRequest
            {
                PrimaryCategory = w3Article?.DTO?.Core?.Type.Label,
                PrimaryCategoryId = w3Article?.DTO?.Core?.Type.ID.ToString(),
                SubCategory1 = w3Article?.DTO?.Core?.Family?.Label,
                SubCategory1Id = w3Article?.DTO?.Core?.Family?.ID.ToString(),
            };
        }

        private OrderRequest BuildOrderRequest(ThankYouOrderDetail orderDetail, DatalayerViewModel model)
        {
            var popOrderForm = model.PopOrderForm;
            var paymentMethodRequests = GetPaymentMethodRequests(model);
            var mainPaymentMethodRequest = GetMainPaymentMethodRequests(model);
            var trackingData = popOrderForm.GetPrivateData<TrackingData>(false)?.Value;

            return new OrderRequest()
            {
                OrderId = orderDetail.OrderUserReference,
                Total = new OrderRequest.TotalRequest()
                {
                    BasePrice = (orderDetail?.GlobalPrices?.TotalPriceEurNoVat).GetValueOrDefault(),
                    Currency = GetCurrency(),
                    PriceWithTax = (orderDetail.GlobalPrices?.TotalPriceEur).GetValueOrDefault(),
                    Shipping = orderDetail.GlobalPrices?.TotalShipPriceUserEur.GetValueOrDefault(),
                    TaxRate = orderDetail.GlobalPrices?.ShipVATRate,
                    OrderTotal = (orderDetail.GlobalPrices?.TotalPriceEur).GetValueOrDefault(),
                    ShippingMethod = trackingData?.FirstShippingMethod,
                    ShippingMethodId = orderDetail.ShippingMethodId.ToString()
                },
                Profile = new PurchaserProfileRequest()
                {
                    ProfileInfo = new ProfileInfoRequest()
                    {
                        ProfileId = _visitorAdapter.GetVisitor()?.UidAsString(),
                        UserName = GetUserName(),
                    },
                    BillingAddress = BuildBillingAddressRequest(popOrderForm.BillingAddress),
                    ShippingAddress = BuildShippingAddressRequest(orderDetail.ShippingAddress)
                },
                Attributes = new OrderRequest.AttributesRequest()
                {
                    TransactionId = popOrderForm.OrderGlobalInformations.MainOrderUserReference,
                    //Status = OrderStatus.Unapproved,//FIXME there is other status like Paymentdue?
                    OrderDate = orderDetail.OrderDate,
                    // Main billing method
                    PaymentMethod = mainPaymentMethodRequest.PaymentMethod,
                    PaymentMethodId = mainPaymentMethodRequest.PaymentMethodId,
                    PaymentMethodStorage = mainPaymentMethodRequest.PaymentMethodStorage,
                    // Other billing methods
                    SplitPaymentMethods = paymentMethodRequests
                },
                Items = BuildItems(orderDetail, popOrderForm)
            };
        }

        private Uri TryUri(string url)
        {
            var checkUrl = Uri.TryCreate(url, UriKind.Absolute, out var resultUrl);
            return checkUrl ? resultUrl : null;
        }

        private string GetPaymentMethodStorage(BillingMethod billingMethod, PopOrderForm popOrderForm)
        {
            if (billingMethod is OgoneCreditCardBillingMethod occbm)
            {
                if (!string.IsNullOrEmpty(popOrderForm.GetPrivateData<DirectLinkData>()?.AliasSend))
                    return "stored";
                else if (occbm.SavingCardAutorisation)
                    return "authorized";
                else
                    return "unauthorized";
            }
            else if (billingMethod is CreditCardBillingMethod)
                return "unauthorized";

            return null;
        }

        private string GetShippingLabel(int? shippingMethodId)
        {
            return shippingMethodId != null ? _applicationContext.TryGetMessage($"orderpipe.pop.shipping.methods.{shippingMethodId}") : string.Empty;
        }

        private string GetPageName(PageTypeEnum pageType)
        {
            switch (pageType)
            {
                case PageTypeEnum.PopOneClick:
                    return _applicationContext.GetMessage($"orderpipe.pop.onepage.oneclick.pagename");

                case PageTypeEnum.PopOnePage:
                    return _applicationContext.GetMessage($"orderpipe.pop.onepage.pagename");

                case PageTypeEnum.PopShipping:
                    return _applicationContext.GetMessage($"orderpipe.pop.shipping.pagename");

                case PageTypeEnum.PopPayment:
                    return _applicationContext.GetMessage($"orderpipe.pop.payment.pagename");

                case PageTypeEnum.PopBasket:
                    return _applicationContext.GetMessage($"orderpipe.pop.basket.pagename");

                case PageTypeEnum.ScoOrderThankYou:
                    return _applicationContext.GetMessage($"orderpipe.pop.scoorderthankyou.pagename");

                case PageTypeEnum.OrderThankYou:
                    return _applicationContext.GetMessage($"orderpipe.pop.orderthankyou.pagename");

                case PageTypeEnum.OrderSorry:
                    return _applicationContext.GetMessage($"orderpipe.pop.ordersorry.pagename");

                default:
                    return null;
            }
        }

        private string GetUserName()
        {
            var dto = _visitorAdapter.GetVisitor().Customer.DTO;
            var parts = new[] { dto.FirstName?.Trim(), dto.LtName?.Trim() };
            var result = string.Join(" ", parts.Where(x => !string.IsNullOrEmpty(x)));
            return result == "" ? null : result;
        }

        private FnacDarty.FnacCom.DataLayer.Models.Request.SellerType GetSellerType()
        {
            switch (_applicationContext.GetSiteContext().SiteID)
            {
                case FnacSites.FNACPRO:
                    return FnacDarty.FnacCom.DataLayer.Models.Request.SellerType.Professional;

                default:
                    return FnacDarty.FnacCom.DataLayer.Models.Request.SellerType.FnacCom;
            }
        }

        #endregion PRIVATE
    }
}
