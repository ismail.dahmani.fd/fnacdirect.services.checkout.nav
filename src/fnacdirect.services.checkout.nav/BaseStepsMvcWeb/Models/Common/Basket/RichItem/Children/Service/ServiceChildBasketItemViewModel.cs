namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Basket.Children
{
    public class ServiceChildBasketItemViewModel : ChildBasketItemViewModel
    {
        public string Image { get; set; }
        public int ProductId { get; set; }
        public PriceViewModel Price { get; set; }

        public bool IsService
        {
            get
            {
                return true;
            }
        }

        public bool CanDelete { get; set; }
        public bool CanUpdate { get; set; }
        public int MasterProductId { get; set; }
        public int? TypeId { get; set; }
        public string Title { get; set; }
        public int AvailabilityId { get; set; }
        public int TotalOfferCount { get; set; }
        public bool IsNumerical { get; set; }
        public string MoreInfoLink { get; set; }
        public string LldContractNumber { get; set; }
    }
}
