
namespace FnacDirect.OrderPipe.Base.Business.OrderDetail
{
    public abstract class BaseArticle
    {
        public string Title { get; set; }

        public string PicturUrl { get; set; }

        public string Price { get; set; }

        public string OldPrice { get; set; }

        public string PriceNoVATEur { get; set; }

        public string Quantity { get; set; }

        public string TheoreticalDeliveryDate { get; set; }
    }
}
