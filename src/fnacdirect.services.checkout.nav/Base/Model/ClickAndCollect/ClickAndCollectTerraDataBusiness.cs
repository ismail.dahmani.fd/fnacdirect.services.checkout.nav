using System;
using System.Collections.Generic;
using FnacDirect.OrderPipe.Base.BaseModel.Model;
using FnacDirect.OrderPipe.Base.LineGroups;
using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.OrderPipe.Base.Model.UserInformations;
using System.Xml.Serialization;
using System.Text;
using System.IO;
using FnacDirect.OrderPipe.Core.Services;
using FnacDirect.OrderPipe.Base.Proxy.TerraData;
using FnacDirect.OrderPipe.Base.Model.Pop;
using FnacDirect.Contracts.Online.Model;
using System.Linq;
using FnacDirect.Technical.Framework.CoreServices;
using FnacDirect.Technical.Framework.ServiceLocation;
using System.Xml;
using Common.Logging;

namespace FnacDirect.OrderPipe.Base.Business
{
    public class ClickAndCollectTerraDataBusiness : IClickAndCollectTerraDataBusiness, IGuptFnacComBusiness
    {
        private readonly ILog _logger;

        private const string RETURN_OK = "OK";
        private IPipeParametersProvider _pipeParametersProvider;

        public PUBLISH PublishRequest { get; set; }

        public ClickAndCollectTerraDataBusiness(IPipeParametersProvider pipeParametersProvider, ILog log)
        {
            _pipeParametersProvider = pipeParametersProvider;
            _logger = log;
        }

        public string GetResultReserveClickAndCollect(
            ILogisticLineGroup logisticLineGroup,
            IUserContextInformations userContext,
            IEnumerable<BillingMethod> billingMethods,
            BillingAddress billingAddress,
            ShopData data,
            TelevisualRoyaltyData televisualRoyaltyData,
            ShopAddress shopAddress,
            bool isMobile,
            PopAdherentData popAdherentData)
        {
            try
            {
                if (shopAddress == null || shopAddress.RefUG.IsNullOrEmpty())
                    throw new ArgumentNullException("shopAddress", "Unable to find ShopAddress RefUG");

                BuildRequest(billingAddress, userContext, logisticLineGroup, popAdherentData, shopAddress);

                var publishRequestStringBuilder = SerializePublishRequest(PublishRequest);

                return CallTerraData(publishRequestStringBuilder);
            }
            catch (Exception ex)
            {
                _logger.Error("ClickAndCollectTerraDataBusiness.GetGuptRefInvoiceShop", ex);
                return string.Empty;
            }
        }

        private string CallTerraData(StringBuilder publishRequestStringBuilder)
        {
            using (var messageServiceSoap = ServiceLocator.Current.GetInstance<IMessageServiceSoap>())
            {
                var response = messageServiceSoap.ReceiveFromWeb(new ReceiveFromWebRequest { Body = new ReceiveFromWebRequestBody { inputMessage = publishRequestStringBuilder.ToString() } });

                if (response?.Body.ReceiveFromWebResult.ErrorCode > 0)
                    Logging.Current.WriteError("ClickAndCollectTerraDataBusiness.GetGuptRefInvoiceShop", new Exception(response.Body.ReceiveFromWebResult.Description));
                else
                    return RETURN_OK;
            }

            return string.Empty;
        }

        private StringBuilder SerializePublishRequest(object request)
        {
            var emptyNamepsaces = new XmlSerializerNamespaces(new[] { new XmlQualifiedName("xsi", "http://www.fnac.com/iwebup") });
            var settings = new XmlWriterSettings()
            {
                Indent = true,
                OmitXmlDeclaration = true
            };

            var xmlSerializer = new XmlSerializer(typeof(PUBLISH));
            var publishStringBuilder = new StringBuilder();


            using (var stream = new StringWriter())
            using (var writer = XmlWriter.Create(publishStringBuilder, settings))
            {
                xmlSerializer.Serialize(writer, request, emptyNamepsaces);
            }

            return publishStringBuilder;
        }

        private PUBLISH BuildRequest(BillingAddress billingAddress, IUserContextInformations userContext, ILogisticLineGroup logisticLineGroup, PopAdherentData popAdherentData, ShopAddress shopAddress)
        {
            PublishRequest = InitPublishRequest();

            PublishRequest.CONTENT = BuildContent(billingAddress, logisticLineGroup, userContext, popAdherentData, shopAddress);


            return PublishRequest;
        }

        private PUBLISH InitPublishRequest()
        {
            var publishRequest = new PUBLISH()
            {
                HEADER = new PUBLISHHEADER()
                {
                    SENDER_ID = "WUP-CDE",
                    RECEIVER_ID = "SPN-GUS",
                    COMPANY = "FNAC",
                    COUNTRY = "ES",
                    DATE = DateTime.UtcNow,
                    MESSAGE_TYPE = "RESERVATION",
                    VERSION = "001",
                    REFERENTIEL_ID = "SPN-WEB",
                    APPLICATION_SCOPE = "FNAC.COM"
                }
            };

            return publishRequest;
        }

        private PUBLISHCONTENT BuildContent(BillingAddress billingAddress, ILogisticLineGroup logisticLineGroup, IUserContextInformations userContext, PopAdherentData popAdherentData, ShopAddress shopAddress)
        {
            var content = new PUBLISHCONTENT
            {
                CUSTOMER = new PUBLISHCONTENTCUSTOMER
                {
                    BILLING_ADDRESS = new PUBLISHCONTENTCUSTOMERBILLING_ADDRESS
                    {
                        BIL_ADDR1 = billingAddress.AddressLine1,
                        BIL_ADDR2 = billingAddress.AddressLine2,
                        BIL_ADDR3 = billingAddress.AddressLine3,
                        BIL_CELLPHONE = billingAddress.CellPhone,
                        BIL_CITY = billingAddress.City,
                        BIL_COUNTRY = billingAddress.CountryCode2,
                        BIL_FIRSTNAME = billingAddress.Firstname,
                        BIL_LASTNAME = billingAddress.Lastname,
                        BIL_PHONE = billingAddress.Phone,
                        BIL_STATE = billingAddress.State,
                        BIL_ZIP = billingAddress.ZipCode,
                        BIL_CIVILITY = billingAddress.GenderId.ToString(),
                        BIL_FISCAL_CODE = popAdherentData.NIENIF
                    },
                    WEB_CUSTOMER = new PUBLISHCONTENTCUSTOMERWEB_CUSTOMER
                    {
                        CUS_EMAIL = userContext.ContactEmail,
                        CUS_ID = userContext.AccountId.ToString(),
                    }
                },
                BASKET = new PUBLISHCONTENTBASKET
                {
                    BSK_WEB_ORDER_ID = logisticLineGroup.OrderPk.Value.ToString(),
                    BSK_WEB_ORDER_CUS_REF = logisticLineGroup.OrderUserReference,
                    BSK_FULLPRICE = logisticLineGroup.RemainingTotal,
                    LINES = new PUBLISHCONTENTBASKETLINES[] { }
                }
            };

            if (userContext.CustomerEntity != null && userContext.CustomerEntity.IsValidAdherent)
            {
                content.CUSTOMER.WEB_CUSTOMER.MEMBER = new PUBLISHCONTENTCUSTOMERWEB_CUSTOMERMEMBER
                {
                    MBR_ACCOUNT_ID = userContext.CustomerEntity.AdherentNumber,
                    MBR_CARD_ID = userContext.CustomerEntity.AdherentCardNumber
                };
            }

            var lines = BuildLines(logisticLineGroup, shopAddress);
            if (lines.Any())
            {
                content.BASKET.LINES = lines.ToArray();
            }

            return content;
        }

        private List<PUBLISHCONTENTBASKETLINES> BuildLines(ILogisticLineGroup logisticLineGroup, ShopAddress shopAddress)
        {
            var lines = new List<PUBLISHCONTENTBASKETLINES>();

            var forcedShippingDateTypeId = _pipeParametersProvider.GetAsRangeSet<int>("forced.shipping.date.typeid");

            for (var i = 0; i < logisticLineGroup.Articles.Count; i++)
            {
                var standardArticle = logisticLineGroup.Articles[i] as StandardArticle;

                var dtTheoExpDate = new DateTime?();

                if (logisticLineGroup.IsClickAndCollect)
                {
                     dtTheoExpDate = logisticLineGroup.OrderDate;
                }
                else
                {
                     dtTheoExpDate = (!logisticLineGroup.Articles[i].TypeId.HasValue || !forcedShippingDateTypeId.Contains(logisticLineGroup.Articles[i].TypeId.Value)) ? standardArticle.TheoreticalExpeditionDate : logisticLineGroup.OrderDate;
                }


                var line = new PUBLISHCONTENTBASKETLINESLINE
                {
                    LIN_ID = standardArticle.OrderDetailPk.Value.ToString(),
                    LIN_SKU = standardArticle.ReferenceGU,
                    LIN_QTY = standardArticle.Quantity.ToString(),
                    LIN_PROMISE_DATE = dtTheoExpDate,
                    LIN_UNIT_GROSS = standardArticle.PriceDBEur.GetValueOrDefault(),
                    LIN_UNIT_PRICE = standardArticle.PriceUserEur.GetValueOrDefault(),
                    LIN_GROSS = (decimal)standardArticle.Quantity * standardArticle.PriceDBEur.GetValueOrDefault(),
                    LIN_PRICE = (decimal)standardArticle.Quantity * standardArticle.PriceUserEur.GetValueOrDefault(),
                    LIN_TAX_PRICE = (decimal)standardArticle.Quantity * standardArticle.EcoTaxEur.GetValueOrDefault(),
                    LIN_FULLPRICE = (decimal)standardArticle.Quantity * (standardArticle.EcoTaxEur.GetValueOrDefault() + standardArticle.PriceUserEur.GetValueOrDefault()),
                    LIN_VAT_AMOUNT = (decimal)standardArticle.Quantity * (standardArticle.PriceUserEur.GetValueOrDefault() - standardArticle.PriceNoVATEur.GetValueOrDefault()),
                    LIN_DSC_PRICE = (decimal)standardArticle.Quantity * (standardArticle.PriceDBEur.GetValueOrDefault() - standardArticle.PriceUserEur.GetValueOrDefault()),
                    LIN_VAT_CODE = standardArticle.VATCodeGu,
                    LIN_VAT_RATE = standardArticle.VATRate.GetValueOrDefault() / 100,
                    CARRIER = new PUBLISHCONTENTBASKETLINESLINECARRIER
                    {
                        CAR_WISH_COMPANY = "CW",
                        STORE_CODE = shopAddress?.RefUG?.PadLeft(2, '0')
                    },
                    DISCOUNTS = new PUBLISHCONTENTBASKETLINESLINEDISCOUNT[] { }
                };

                var discounts = BuildDiscountsLine(standardArticle, logisticLineGroup);
                if (discounts.Any())
                {
                    line.DISCOUNTS = discounts.ToArray();
                }

                lines.Add(new PUBLISHCONTENTBASKETLINES { LINE = line });
            }

            return lines;
        }

        private List<PUBLISHCONTENTBASKETLINESLINEDISCOUNT> BuildDiscountsLine(StandardArticle standardArticle, ILogisticLineGroup logisticLineGroup)
        {
            var discounts = new List<PUBLISHCONTENTBASKETLINESLINEDISCOUNT>();

            foreach (var promotion in standardArticle.Promotions)
            {
                var discount = new PUBLISHCONTENTBASKETLINESLINEDISCOUNT
                {
                    DSC_PROMOTION_ID = logisticLineGroup.OrderPk.Value.ToString(),
                    DSC_REASON = string.Empty,
                    DSC_TYPE = ((PromotionType)Enum.Parse(typeof(PromotionType), promotion.DiscountType.ToString())).ToString(),
                    DSC_UNIT_PRICE = 0m
                };
                discounts.Add(discount);
            }

            foreach (var shippingPromotionRule in standardArticle.ShippingPromotionRules)
            {
                if (shippingPromotionRule.Promotion != null
                    && logisticLineGroup.ShippingMethodChoice.AppliedShippingPromotion.Any(a => a.Key == standardArticle.ShipMethod.GetValueOrDefault() && a.Value.Any(r => r.PromoCode == shippingPromotionRule.Promotion.Code)))
                {
                    var discount = new PUBLISHCONTENTBASKETLINESLINEDISCOUNT
                    {
                        DSC_PROMOTION_ID = logisticLineGroup.OrderPk.Value.ToString(),
                        DSC_REASON = string.Empty,
                        DSC_TYPE = ((PromotionType)Enum.Parse(typeof(PromotionType), shippingPromotionRule.Promotion.Type.ToString())).ToString(),
                        DSC_UNIT_PRICE = 0m
                    };
                    discounts.Add(discount);
                }
            }

            return discounts;
        }
    }
}
