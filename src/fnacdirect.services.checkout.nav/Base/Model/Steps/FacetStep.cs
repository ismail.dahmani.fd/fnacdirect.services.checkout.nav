using System;
using System.Collections.Generic;
using System.Linq;
using StructureMap;

namespace FnacDirect.OrderPipe.Core.Steps
{
    public abstract class FacetStep<TOrderFormFacet> : Step<TOrderFormFacet, NullStepData, NullGroupData>
       where TOrderFormFacet : class, IOF
    {
        public override IEnumerable<Type> DataType
        {
            get { return Enumerable.Empty<Type>(); }
        }

        public override sealed StepReturn Check(OPContext opContext)
        {
            return Check(opContext.OF as TOrderFormFacet, opContext);
        }

        protected internal virtual StepReturn Check(TOrderFormFacet facet)
        {
            return StepReturn.Process;
        }

        protected internal virtual StepReturn Check(TOrderFormFacet facet, OPContext opContext)
        {
            return Check(facet);
        }

        public override sealed StepReturn Process(OPContext opContext)
        {
            return Process(opContext.OF as TOrderFormFacet, opContext);
        }

        protected internal virtual StepReturn Process(TOrderFormFacet facet)
        {
            return StepReturn.Next;
        }

        protected internal virtual StepReturn Process(TOrderFormFacet facet, OPContext opContext)
        {
            return Process(facet);
        }
    }

    public abstract class FacetStep<TOrderFormFacet, TFacet> : Step<TOrderFormFacet, NullStepData, NullGroupData>
        where TOrderFormFacet : class, IOF 
    {
        public override IEnumerable<Type> DataType
        {
            get { return Enumerable.Empty<Type>(); }
        }

        protected abstract TFacet GetFacet(TOrderFormFacet orderFormFacet);

        public override sealed StepReturn Check(OPContext opContext)
        {
            var facet = GetFacet(opContext.OF as TOrderFormFacet);

            return Check(facet, opContext);
        }

        protected virtual StepReturn Check(TFacet facet)
        {
            return StepReturn.Process;
        }

        protected internal virtual StepReturn Check(TFacet facet, OPContext opContext)
        {
            return Check(facet);
        }

        public override sealed StepReturn Process(OPContext opContext)
        {
            var facet = GetFacet(opContext.OF as TOrderFormFacet);

            return Process(facet, opContext);
        }
        
        protected internal virtual StepReturn Process(TFacet facet)
        {
            return StepReturn.Next;
        }

        protected virtual StepReturn Process(TFacet facet, OPContext opContext)
        {
            return Process(facet);
        }
    }

    public abstract class FacetStep<TOrderFormFacet, TStepData, TGroupData> : Step<TOrderFormFacet, TStepData, TGroupData>
        where TOrderFormFacet : class, IOF
        where TStepData : StepData, new() 
        where TGroupData : GroupData, new()
    {
        public override IEnumerable<Type> DataType
        {
            get
            {
                var types = new List<Type>();

                if (typeof (TStepData) != typeof (NullStepData))
                {
                    types.Add(typeof (TStepData));
                }

                if (typeof(TGroupData) != typeof(NullGroupData))
                {
                    types.Add(typeof(TGroupData));
                }

                return types;
            }
        }

        public override sealed StepReturn Check(OPContext opContext)
        {
            var stepData = new TStepData();
            var groupData = new TGroupData();
            if (typeof(TStepData) != typeof(NullStepData))
            {
                stepData = opContext.OF.GetPrivateData<TStepData>(Name);
            }
            if (typeof(TGroupData) != typeof(NullGroupData))
            {
                groupData = opContext.OF.GetPrivateData<TGroupData>(Group);
            }
            return Check(opContext.OF as TOrderFormFacet, opContext, stepData, groupData);
        }

        protected internal virtual StepReturn Check(TOrderFormFacet facet)
        {
            return StepReturn.Process;
        }

        protected internal virtual StepReturn Check(TOrderFormFacet facet, OPContext opContext, TStepData stepData, TGroupData groupData)
        {
            return Check(facet);
        }

        public override sealed StepReturn Process(OPContext opContext)
        {
            var stepData = new TStepData();
            var groupData = new TGroupData();
            if (typeof (TStepData) != typeof (NullStepData))
            {
                stepData = opContext.OF.GetPrivateData<TStepData>(Name);
            }
            if (typeof (TGroupData) != typeof (NullGroupData))
            {
                groupData = opContext.OF.GetPrivateData<TGroupData>(Group);
            }
            return Process(opContext.OF as TOrderFormFacet, opContext, stepData, groupData);
        }

        protected internal virtual StepReturn Process(TOrderFormFacet orderFormFacet)
        {
            return StepReturn.Next;
        }

        protected internal virtual StepReturn Process(TOrderFormFacet orderFormFacet, OPContext opContext, TStepData stepData, TGroupData groupData)
        {
            return Process(orderFormFacet);
        }
    }
}
