﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Common.Payment
{
    public class VirtualGiftCheckResultViewModel
    {
        public bool IsValid { get; set; }

        public string Error { get; set; }
    }
}
