using FnacDirect.OrderPipe.Base.Business;
using FnacDirect.OrderPipe.Base.Business.Availability;
using FnacDirect.OrderPipe.Base.Business.Shipping;
using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.OrderPipe.Base.Proxy.Membership;
using FnacDirect.OrderPipe.BaseMvc.Web.Pop.Models.Common;
using FnacDirect.OrderPipe.Core.Services;
using FnacDirect.Technical.Framework.Web.Mvc;
using System.Collections.Generic;
using System.Linq;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Common.Shipping.Postal
{
    public class FdvPostalShippingNetworkViewModelBuilder : PostalShippingNetworkViewModelBuilder
    {
        public FdvPostalShippingNetworkViewModelBuilder(IApplicationContext applicationContext,
                                                        IPostalShippingChoiceViewModelBuilder postalShippingChoiceViewModelBuilder,
                                                        IPopAvailabilityProcessor popAvailabilityProcessor,
                                                        ISwitchProvider switchProvider,
                                                        ISolexBusiness solexBusiness,
                                                        IAppointmentDeliveryService appointmentDeliveryService,
                                                        IAddressValidationBusiness addressValidationBusiness,
                                                        IPipeMessageViewModelBuilder pipeMessageViewModelBuilder,
                                                        IPipeParametersProvider pipeParametersProvider,
                                                        IAdherentCardArticleService adherentCardArticleService)
            : base(applicationContext,
                  postalShippingChoiceViewModelBuilder,
                  popAvailabilityProcessor,
                  switchProvider,
                  solexBusiness,
                  appointmentDeliveryService,
                  addressValidationBusiness,
                  pipeMessageViewModelBuilder,
                  pipeParametersProvider, 
                  adherentCardArticleService)
        {

        }

        protected override PriceFormat PriceFormat { get { return PriceFormat.Default; } }

        public override IEnumerable<PostalShippingChoiceViewModel> BuildPostalChoiceViewModel(PopOrderForm popOrderForm, int sellerId)
        {
            var postalViewModels = base.BuildPostalChoiceViewModel(popOrderForm, sellerId);

            if (postalViewModels != null && postalViewModels.Count() > 1)
            {
                postalViewModels = postalViewModels.OrderBy(psc => psc.TotalPrice).ThenByDescending(tsc => tsc.Weight);
            }

            return postalViewModels;
        }
    }
}
