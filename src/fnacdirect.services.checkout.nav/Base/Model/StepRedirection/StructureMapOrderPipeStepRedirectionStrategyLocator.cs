﻿using FnacDirect.OrderPipe.Config;
using StructureMap;

namespace FnacDirect.OrderPipe.CoreRouting
{
    public class StructureMapOrderPipeStepRedirectionStrategyLocator : IOrderPipeStepRedirectionStrategyLocator
    {
        private readonly IContainer _container;

        public StructureMapOrderPipeStepRedirectionStrategyLocator(IContainer container)
        {
            _container = container;
        }

        public IOrderPipeStepRedirectionStrategy GetOrderPipeStepRedirectionStrategyFor<TDescriptor>(TDescriptor descriptor) where TDescriptor :IUiDescriptor
        {
            var instanceType = descriptor.GetType();
            var serviceType = typeof(IOrderPipeStepRedirectionStrategy<>);
            var concreteServiceType = serviceType.MakeGenericType(instanceType);

            return _container.GetInstance(concreteServiceType) as IOrderPipeStepRedirectionStrategy;
        }
    }
}
