﻿using FnacDirect.OrderPipe.Base.Model;
using System.Collections.Generic;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Basket
{
    public interface IDonationViewModelBuilder
    {
        DonationsViewModel Build(PopOrderForm popOrderForm, OPContext opContext);
    }
}
