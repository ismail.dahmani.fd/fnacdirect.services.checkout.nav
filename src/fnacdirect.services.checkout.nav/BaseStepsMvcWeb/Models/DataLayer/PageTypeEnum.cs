using System.ComponentModel;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.DataLayer
{
    public enum PageTypeEnum
    {
        PopOnePage,
        PopOneClick,
        PopShipping,
        PopPayment,
        PopBasket,
        ScoCrypto,
        ScoOrderThankYou,
        OrderThankYou,
        OrderSorry,
    }
}
