
using FnacDirect.Technical.Framework.Web.Mvc.DataAnnotations;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Common.Payment
{
    public class CheckMethodViewModel : BillingMethodViewModel
    {
        [Required(ErrorMessage = "orderpipe.pop.payment.check.required")]
        [RegularExpression(@"^[0-9]{7}$", ErrorMessage = "orderpipe.pop.payment.check.format")]
        public string CheckNumber { get; set; }
    }
}
