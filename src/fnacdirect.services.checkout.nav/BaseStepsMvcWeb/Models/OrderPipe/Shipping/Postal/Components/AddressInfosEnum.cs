namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.OnePage.Address
{
    public enum TaxNumberType
    {
        Passport = 1,
        Nif = 2, 
        CIF = 3,
        Nie = 4,
        NifIntra = 5,
        FiscalNumber = 6,
        Unknown = 99
    }
}
