﻿using System;

namespace FnacDirect.OrderPipe.Base.Model
{
    public class OrderLine
    {
        public string OrderUserRef { get; set; }
        public DateTime OrderDate { get; set; }
        public int OdetId { get; set; }
        public int ArtQuantity { get; set; }
        public int ArtNumber { get; set; }
        public string ArtLabel { get; set; }
        public string Info { get; set; }
    }
}
