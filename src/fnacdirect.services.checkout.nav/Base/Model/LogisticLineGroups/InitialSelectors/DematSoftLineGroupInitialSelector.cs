﻿using FnacDirect.OrderPipe.Base.Business.LogisticLineGroups.Internals.InitialSelectors;
using FnacDirect.OrderPipe.Base.LineGroups;
using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.OrderPipe.Base.Model.LineGroups;
using System.Collections.Generic;
using System.Linq;

namespace FnacDirect.OrderPipe.Base.Business.LogisticLineGroups.InitialSelectors
{
    public class DematSoftLineGroupInitialSelector : BaseLineGroupInitialSelector
    {
        public override IDictionary<ILineGroupHeader, IEnumerable<Article>> Select(IEnumerable<ILineGroup> lineGroups)
        {
            return lineGroups.Where(l => l.HasDematSoftArticles).ToDictionary(l => l as ILineGroupHeader, l => l.Articles
                                                                                                                .OfType<StandardArticle>()
                                                                                                                .Where(a => a.IsDematSoft)
                                                                                                                .AsEnumerable<Article>());
        }
    }
}
