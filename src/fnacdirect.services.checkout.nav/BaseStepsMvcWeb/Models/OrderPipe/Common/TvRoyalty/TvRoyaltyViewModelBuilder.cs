using FnacDirect.OrderPipe.Base.Business;
using FnacDirect.OrderPipe.Base.Model.Facets;
using FnacDirect.OrderPipe.Base.Proxy.Customer;
using FnacDirect.Technical.Framework.Web.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop
{
    public class TvRoyaltyViewModelBuilder : ITvRoyaltyViewModelBuilder
    {
        private readonly IApplicationContext _applicationContext;
        private readonly ITvRoyaltyBusiness _tvRoyaltyBusiness;
        private readonly ICustomerService _customerService;

        public TvRoyaltyViewModelBuilder(IApplicationContext applicationContext, ITvRoyaltyBusiness tvRoyaltyBusiness, ICustomerService customerService)
        {
            _applicationContext = applicationContext;
            _tvRoyaltyBusiness = tvRoyaltyBusiness;
            _customerService = customerService;
        }

        public TvRoyaltyFormViewModel Build(TvRoyaltyFormViewModel viewModel)
        {
            if (viewModel != null)
            {
                viewModel.Civilities = BuildCivilities();
                viewModel.StreetRanks = BuildStreetRank();
                viewModel.StreetTypes = BuildStreetTypes();
                viewModel.TerritoriesOfUse = BuildTerritoriesOfUse();
                viewModel.TvUses = BuildTvUse();
                viewModel.Days = BuildDays();
                viewModel.Months = BuildMonths();
                viewModel.Years = BuildYears();

                return viewModel;
            }

            return null;
        }

        public TvRoyaltyFormViewModel Build(IGotShippingMethodEvaluation iGotShippingMethodEvaluation)
        {
            var userInfo = iGotShippingMethodEvaluation.BillingAddress;
            var currentCustomer = _customerService.GetCurrentCustomer();

            var result = new TvRoyaltyFormViewModel()
            {
                LastName = userInfo.Lastname,
                FirstName = userInfo.Firstname,
                StreetAndNumber = userInfo.AddressLine1,
                OptionalAddress = userInfo.AddressLine2,
                StreetRank = string.Empty,
                PostalCode = userInfo.ZipCode,
                City = userInfo.City,
                BirthDay = currentCustomer.Birthdate.Day,
                BirthMonth = currentCustomer.Birthdate.Month,
                BirthYear = currentCustomer.Birthdate.Year
            };

            // mapping entre ce qui est attendu par le formulaire (orderpipe.pop.tvroyalty.civility.*) et la table [CUSTOMER].[dbo].[tbl_Gender]
            switch (userInfo.GenderId)
            {
                case 2:
                    result.Civility = 1; // Monsieur
                    break;

                case 3:
                    result.Civility = 3; // Madame
                    break;

                case 4:
                    result.Civility = 2; // Mademoiselle
                    break;

                default:
                    result.Civility = 4; // Inconnu
                    break;
            }

            return Build(result);
        }

        private IEnumerable<CivilityInfo> BuildCivilities()
        {
            var civilities = new List<CivilityInfo>();
            for (var i = 1; i < 10; i++)
            {
                if (i == 5)
                {
                    continue;
                }
                var label = _applicationContext.TryGetMessage("orderpipe.pop.tvroyalty.civility." + i);
                var info = new CivilityInfo()
                {
                    Id = i,
                    Label = label
                };
                civilities.Add(info);
            }
            return civilities;
        }

        private IEnumerable<StreetRankInfo> BuildStreetRank()
        {
            var streetRanks = new List<StreetRankInfo>
            {
                new StreetRankInfo()
                {
                    Id = string.Empty,
                    Label = string.Empty
                }
            };

            var label = _applicationContext.TryGetMessage("orderpipe.Addresses.TelevisualRoyaltyForm.Bis");
            streetRanks.Add(new StreetRankInfo()
            {
                Id = "B",
                Label = label
            });
            label = _applicationContext.TryGetMessage("orderpipe.Addresses.TelevisualRoyaltyForm.Ter");
            streetRanks.Add(new StreetRankInfo()
            {
                Id = "T",
                Label = label
            });
            label = _applicationContext.TryGetMessage("orderpipe.Addresses.TelevisualRoyaltyForm.Quater");
            streetRanks.Add(new StreetRankInfo()
            {
                Id = "Q",
                Label = label
            });
            label = _applicationContext.TryGetMessage("orderpipe.Addresses.TelevisualRoyaltyForm.Quinquies");
            streetRanks.Add(new StreetRankInfo()
            {
                Id = "C",
                Label = label
            });

            return streetRanks;
        }

        private IEnumerable<StreetTypeInfo> BuildStreetTypes()
        {
            var streetTypes = new List<StreetTypeInfo>();
            var label = _applicationContext.TryGetMessage("orderpipe.pop.tvroyalty.choosestreettype");

            var typesFromDB = _tvRoyaltyBusiness.GetTelevisualRoyaltyWays(label);

            foreach (var val in typesFromDB)
            {
                streetTypes.Add(new StreetTypeInfo()
                {
                    Id = val.Key,
                    Label = val.Value
                });
            }

            return streetTypes;
        }

        private IEnumerable<TerritoryOfUseInfo> BuildTerritoriesOfUse()
        {
            var label1 = _applicationContext.TryGetMessage("orderpipe.pop.tvroyalty.territoryofuse.1");
            var label2 = _applicationContext.TryGetMessage("orderpipe.pop.tvroyalty.territoryofuse.2");
            var territories = new List<TerritoryOfUseInfo>()
            {
                new TerritoryOfUseInfo()
                {
                    Id = 1,
                    Label = label1
                },
                new TerritoryOfUseInfo()
                {
                    Id = 2,
                    Label = label2
                }
            };

            return territories;
        }

        private IEnumerable<TvUseInfo> BuildTvUse()
        {
            var uses = new List<TvUseInfo>();

            for (var i = 1; i <= 5; i++)
            {
                uses.Add(new TvUseInfo()
                {
                    Id = i,
                    Label = _applicationContext.TryGetMessage("orderpipe.pop.tvroyalty.tvuse." + i)
                });
            }

            return uses;
        }

        private IEnumerable<int> BuildDays()
        {
            return Enumerable.Range(1, 31);
        }

        private IEnumerable<int> BuildMonths()
        {
            return Enumerable.Range(1, 12);
        }

        private IEnumerable<int> BuildYears()
        {
            return Enumerable.Range(1900, DateTime.Now.Year - 1900);
        }
    }
}
