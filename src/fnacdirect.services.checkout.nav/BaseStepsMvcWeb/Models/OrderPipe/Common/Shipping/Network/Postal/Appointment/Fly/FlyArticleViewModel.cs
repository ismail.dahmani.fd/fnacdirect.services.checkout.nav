namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Orderpipe.Common.Shipping.Network.Postal.Appointment.Fly
{
    public class FlyArticleViewModel
    {
        public int Prid { get; set; }

        public string Title { get; set; }

        public FlyServiceViewModel[] Services { get; set; }
    }
}
