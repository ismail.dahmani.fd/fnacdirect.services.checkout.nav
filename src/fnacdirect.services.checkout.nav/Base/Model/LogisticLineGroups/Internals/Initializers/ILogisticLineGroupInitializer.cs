using FnacDirect.OrderPipe.Base.LineGroups;
using FnacDirect.OrderPipe.Base.Model;
using System.Collections.Generic;

namespace FnacDirect.OrderPipe.Base.Business.LogisticLineGroups.Internals.Initializers
{
    public interface ILogisticLineGroupInitializer
    {
        IEnumerable<ILogisticLineGroup> Initialize(IDictionary<ILineGroupHeader, IEnumerable<Article>> selectedLineGroups);
        IEnumerable<ILogisticLineGroup> SimulateInitialize(IDictionary<ILineGroupHeader, IEnumerable<Article>> selectedLineGroups);
    }
}
