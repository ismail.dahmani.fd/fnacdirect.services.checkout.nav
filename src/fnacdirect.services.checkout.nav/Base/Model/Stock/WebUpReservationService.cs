using System;
using System.Collections.Generic;
using System.Linq;
using FnacDirect.Stock.IModel;
using FnacDirect.Technical.Framework.CoreServices;
using FnacDirect.OrderPipe.Base.LineGroups;

namespace FnacDirect.OrderPipe.Base.Business.Stock
{
    public class WebUpReservationService : IStockReservationService
    {
        private readonly IReservationService _reservationService;

        public WebUpReservationService()
        {
            _reservationService = ServiceFactory<IReservationService>.GetInstance();
        }

        public void Reserve(IEnumerable<ILogisticLineGroup> logisticLineGroups, string market, int userId)
        {
            foreach (var logisticLineGroup in logisticLineGroups.Where(a => a.IsFnacCom && !a.IsClickAndCollect))
            {
                _reservationService.Reserve(logisticLineGroup.OrderUserReference);
            }
        }
    }
}
