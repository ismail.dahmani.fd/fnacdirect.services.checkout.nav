﻿using FnacDirect.Technical.Framework.Web.Mvc;
using System.Web.Mvc;

namespace FnacDirect.OrderPipe.BaseMvc.Web.ContextualizedOperations
{
    public class ContextualizedOperationRazorViewEngine : LocalizationRazorViewEngine
    {
        public ContextualizedOperationRazorViewEngine()
            : base("OrderPipe/ContextualizedOperations")
        {

        }

        protected override string ExpandViewName(ControllerContext controllerContext, string viewName)
        {
            var partnerName = controllerContext.RequestContext.RouteData.Values["publicId"].ToString();

            if (!string.IsNullOrEmpty(partnerName))
            {
                var partnerNameView = string.Format("{0}.{1}", viewName, partnerName.ToLowerInvariant());
                var expanded = base.ExpandViewName(controllerContext, partnerNameView);

                if (!string.IsNullOrEmpty(expanded))
                {
                    return expanded;
                }

                return partnerNameView;
            }

            return null;
        }
    }
}
