﻿namespace FnacDirect.OrderPipe.Base.Business
{
    public class PricingServiceProvider : IPricingServiceProvider
    {
        public IPricingService GetPricingService(int marketId)
        {
            return new PriceBusiness(marketId);
        }
    }
}
