﻿namespace FnacDirect.OrderPipe.Base.Business
{
    public class GetArticlesPricesParameters
    {
        public bool ShopRetrieval { get; set; }
        public bool ForAsilage { get; set; }
        public bool ForceAdherentSegment { get; set; }
        public bool HasUnlimitedSubscription { get; set; }
        public string ForcedRetrievalChannel { get; set; }
        public int? ShopRefUg { get; set; }

        public static GetArticlesPricesParameters Default
        {
            get
            {
                return new GetArticlesPricesParameters();
            }
        }
    }
}
