using System.Collections.Generic;
using FnacDirect.Customer.Model;

namespace FnacDirect.Front.AccountMVC.Business.NewsLetter
{
    public interface INewsLetterService
    {
        List<NewsLetterSetting> GetCustomerNewsLetter(int customerId, int siteId);
        void AddNewsLetterSetting(int accountId, int newsLetterTypeId, int value , string email = "");
        void SetMailFrequency(int accountId, MailingFrequency mailingFrequency);

        void UpdateCellPhone(int accountId, string cellPhone);
    }
}
