﻿using FnacDirect.OrderPipe.Base.DAL;
using FnacDirect.Technical.Framework.Web;
using FnacDirect.Technical.Framework.Web.Mvc;
using System;
using System.Web.Mvc;

namespace FnacDirect.OrderPipe.BaseMvc.Web.ContextualizedOperations
{
    public class ContextualizedOperationController : ViewEngineSpecificController<ContextualizedOperationRazorViewEngine>
    {
        private readonly IContextualizedOperationDal _contextualizedOperationDal;
        private readonly IUrlManager _urlManager;

        public ContextualizedOperationController(IContextualizedOperationDal contextualizedOperationDal,
                                                 IUrlManager urlManager)
        {
            _contextualizedOperationDal = contextualizedOperationDal;
            _urlManager = urlManager;
        }

        [HttpGet]
        public ActionResult Index(IndexViewModel viewModel)
        {
            if (string.IsNullOrEmpty(viewModel.PublicId))
            {
                return HttpNotFound();
            }

            var contextualizedOperation = _contextualizedOperationDal.Get(viewModel.PublicId);

            if (contextualizedOperation == null)
            {
                return HttpNotFound();
            }

            if (!string.IsNullOrEmpty(viewModel.Token))
            {
                if (_contextualizedOperationDal.Get(viewModel.PublicId, viewModel.Token) == null)
                {
                    viewModel.Token = string.Empty;
                    viewModel.InvalidToken = true;
                }
            }

            return View(viewModel);
        }

        [HttpPost, ActionName("Index")]
        public ActionResult GoToPipe(IndexViewModel viewModel)
        {
            if(string.IsNullOrEmpty(viewModel.PublicId))
            {
                return HttpNotFound();
            }

            var contextualizedOperation = _contextualizedOperationDal.Get(viewModel.PublicId);

            if (contextualizedOperation == null)
            {
                return HttpNotFound();
            }

            if (!string.IsNullOrEmpty(viewModel.Token))
            {
                if (_contextualizedOperationDal.Get(viewModel.PublicId, viewModel.Token) == null)
                {
                    viewModel.Token = string.Empty;
                    viewModel.InvalidToken = true;
                    return View(viewModel);
                }
            }

            var page = _urlManager.Sites.OrderPipe.GetPage("Root").DefaultUrl;

            var parameter = Constants.ContextKeys.Operation + "=" + Uri.EscapeDataString(viewModel.PublicId + "_" + viewModel.Token);

            page.WithParam("context", Uri.EscapeDataString(parameter));

            return Redirect(page.ToString());
        }
    }
}
