using System;
using System.Web.Routing;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Models
{
    public interface IPopBaseRootViewModelBuilder<T>
        where T : BaseRootViewModel, new()
    {
        T Build(Func<string, RouteValueDictionary, string> getUrl);
    }
}
