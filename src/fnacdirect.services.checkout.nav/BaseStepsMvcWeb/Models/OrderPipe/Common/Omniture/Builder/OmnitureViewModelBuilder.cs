using FnacDirect.Context;
using FnacDirect.Contracts.Online.Model;
using FnacDirect.Front.WebBusiness.WebMobile;
using FnacDirect.Membership.Model.Constants;
using FnacDirect.OrderPipe.Base.BaseModel.Model;
using FnacDirect.OrderPipe.Base.Business;
using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.OrderPipe.Base.Model.ThankYou;
using FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Basket;
using FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.OnePage.Omniture;
using FnacDirect.Technical.Framework.Web.Mvc;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web;

namespace FnacDirect.OrderPipe.BaseMvc.Web.Models.Pop.Omniture
{
    public class OmnitureViewModelBuilder : IOmnitureViewModelBuilder
    {
        private readonly IApplicationContext _applicationContext;
        private readonly IUserExtendedContextProvider _userExtendedContextProvider;
        private readonly IFnacPlusCardService _fnacPlusCardService;
        private readonly IExecutionContextProvider _executionContextProvider;
        private readonly IUserContextProvider _userContextProvider;

        public OmnitureViewModelBuilder(IApplicationContext applicationContext,
                                        IUserExtendedContextProvider userExtendedContextProvider,
                                        IFnacPlusCardService fnacPlusCardService,
                                        IExecutionContextProvider executionContextProvider,
                                        IUserContextProvider userContextProvider)
        {
            _applicationContext = applicationContext;
            _userExtendedContextProvider = userExtendedContextProvider;
            _fnacPlusCardService = fnacPlusCardService;
            _executionContextProvider = executionContextProvider;
            _userContextProvider = userContextProvider;
        }

        public TrackingViewModel BuildTracking(PopOrderForm popOrderForm, TrackedPage trackedPage, string basketAmount)
        {
            var culture = CultureInfo.GetCultureInfo(_applicationContext.GetLocale());

            var trackingViewModel = new TrackingViewModel
            {
                Prefix = GetPrefix(popOrderForm),
                Account = GetAccount(),
                Channel = _applicationContext.GetMessage("orderpipe.pop.omniture.channel.order"),
                Server = _executionContextProvider.GetCurrentExecutionContext().GetMachineName(),
                StatusMembership = GetStatusMembersihp(popOrderForm),
                UID = GetUID(popOrderForm),
                PipeType = trackedPage == TrackedPage.scoOrderThankYou ? "Self Checkout" : "new pipe",
                PlateformeType = popOrderForm.OrderMobileInformations.IsMobile ? "Web Mobile" : "Web Desktop",
                StatusIdentification = _userContextProvider.GetCurrentUserContext().IsIdentified ? "Identifie" : "Non identifie",
                SubscriptionType = StringUtils.ReplaceBadChar(GetStatusSubscription(popOrderForm)),
                StatusLoggued = _userContextProvider.GetCurrentUserContext().IsAuthenticated ? "Loggue" : "Non Loggue",
                BasketSource = GetBasketSource()
            };

            trackingViewModel.PageName = GetPageName(popOrderForm, trackingViewModel.Prefix, trackedPage);
            trackingViewModel.ArboLevel1 = trackingViewModel.PageName;
            trackingViewModel.BasketAmount = basketAmount.Replace(culture.NumberFormat.CurrencySymbol, ".");

            if (trackedPage == TrackedPage.scoOrderThankYou)
                FillTrackingDataFromOTYPrivatedata(popOrderForm, trackingViewModel);
            else
                FillArticleDetailsFromLinegroup(popOrderForm, trackingViewModel);

            FillDateAndTimeInfo(trackingViewModel);

            trackingViewModel.StatusSubscription = GetSubscriptionStatus(trackingViewModel);

            return trackingViewModel;
        }

        #region Omniture Model Builders
        public OmnitureCommonViewModel BuildCommon(PopOrderForm popOrderForm)
        {
            var omnitureViewModel = new OmnitureCommonViewModel()
            {
                Prefix = GetPrefix(popOrderForm),
                Suffix = GetSuffix(popOrderForm),
                FnacPlus = BuildFnacPlusData(popOrderForm)
            };

            try
            {
                var products = from lineGroup in popOrderForm.LineGroups
                               let isFnac = lineGroup.OrderType != OrderTypeEnum.MarketPlace
                               let seller = isFnac ? "Fnac" : lineGroup.Seller.DisplayName
                               let type = GetType(lineGroup)
                               from article in lineGroup.Articles
                               let prid = article.ProductID.GetValueOrDefault()
                               let pridToTrack = article.Referentiel == (int)ArticleCatalog.MarketPlace ? $"MP-{prid}" : prid.ToString()
                               let state = isFnac || (article as MarketPlaceArticle).Offer.ProductStatus == ProductStatus.Neuf ? "1" : "0"
                               select string.Format(";{0};;;;evar7={1}|evar8={2}|evar9={3}", pridToTrack, seller, type, state);

                omnitureViewModel.Products = string.Join(",", products);
            }
            catch
            {
                // something went wrong when building products list
                // but this should should not throw 500
            }

            return omnitureViewModel;
        }

        private static string GetType(Base.Model.LineGroups.PopLineGroup lineGroup)
        {
            switch (lineGroup.OrderType)
            {
                case OrderTypeEnum.MarketPlace:
                    return "Marketplace";
                case OrderTypeEnum.Numerical:
                    return "Dematerialise";
                default:
                    return "Fnac.com";
            }
        }

        public OmnitureBasketViewModel BuildBasket(PopOrderForm popOrderForm)
        {
            var commonViewModel = BuildCommon(popOrderForm);

            return new OmnitureBasketViewModel
            {
                Prefix = commonViewModel.Prefix,
                FnacPlus = commonViewModel.FnacPlus,
                Products = commonViewModel.Products,

                Channel = _applicationContext.GetMessage("orderpipe.pop.omniture.channel.basket"),
                Server = _executionContextProvider.GetCurrentExecutionContext().GetMachineName(),
                PlateformeType = popOrderForm.OrderMobileInformations.IsMobile ? "Web Mobile" : "Web Desktop",
                PipeType = popOrderForm.PipeName,
                UID = GetUID(popOrderForm),
                SubscriptionType = StringUtils.ReplaceBadChar(GetStatusSubscription(popOrderForm)),
                StatusMembership = GetStatusMembersihp(popOrderForm),
                StatusIdentification = _userContextProvider.GetCurrentUserContext().IsIdentified ? "Identifie" : "Non identifie",
                StatusLoggued = _userContextProvider.GetCurrentUserContext().IsAuthenticated ? "Loggue" : "Non Loggue",
                BasketSource = GetBasketSource(),
                BasketMix = GetBasketInfos(popOrderForm)
            };
        }

        public OmnitureBasketViewModel BuildOnePage(PopOrderForm popOrderForm)
        {
            var commonViewModel = BuildCommon(popOrderForm);

            return new OmnitureBasketViewModel
            {
                Prefix = commonViewModel.Prefix,
                FnacPlus = commonViewModel.FnacPlus,
                Products = commonViewModel.Products,

                Channel = _applicationContext.GetMessage("orderpipe.pop.omniture.channel.order"),
                Server = _executionContextProvider.GetCurrentExecutionContext().GetMachineName(),
                PlateformeType = popOrderForm.OrderMobileInformations.IsMobile ? "Web Mobile" : "Web Desktop",
                PipeType = popOrderForm.PipeName,
                UID = GetUID(popOrderForm),
                SubscriptionType = StringUtils.ReplaceBadChar(GetStatusSubscription(popOrderForm)),
                StatusMembership = GetStatusMembersihp(popOrderForm),
                StatusIdentification = _userContextProvider.GetCurrentUserContext().IsIdentified ? "Identifie" : "Non identifie",
                StatusLoggued = _userContextProvider.GetCurrentUserContext().IsAuthenticated ? "Loggue" : "Non Loggue",
                BasketSource = GetBasketSource(),
                BasketMix = GetBasketInfos(popOrderForm)
            };
        }

        public OmnitureCommonViewModel BuildShipping(PopOrderForm popOrderForm, int sellerId)
        {
            var omnitureViewModel = new OmnitureCommonViewModel()
            {
                Prefix = GetPrefix(popOrderForm),
                Suffix = GetSuffix(popOrderForm)
            };

            try
            {
                var products = from lineGroup in popOrderForm.LineGroups.Seller(sellerId)
                               let isFnac = lineGroup.OrderType != OrderTypeEnum.MarketPlace
                               let seller = isFnac ? "Fnac" : lineGroup.Seller.DisplayName
                               let type = lineGroup.OrderType == OrderTypeEnum.MarketPlace ? "MP" : lineGroup.OrderType == OrderTypeEnum.Numerical ? "Demat" : "Fcom"
                               from article in lineGroup.Articles
                               let prid = article.ProductID.GetValueOrDefault()
                               let state = isFnac ? "1" : (article as MarketPlaceArticle).Offer.ProductStatus == ProductStatus.Neuf ? "1" : "0"
                               select string.Format(";{0};;;;evar7={1}|evar8={2}|evar9={3}", prid, seller, type, state);

                omnitureViewModel.Products = string.Join(",", products);
            }
            catch
            {
                // something went wrong when building products list
                // but this should should not throw 500
            }

            omnitureViewModel.FnacPlus = BuildFnacPlusData(popOrderForm);
            return omnitureViewModel;
        }

        public OmnitureFnacPlusViewModel BuildFnacPlusData(PopOrderForm popOrderForm)
        {
            OmnitureFnacPlusViewModel viewModel = null;

            if (_fnacPlusCardService.FnacPlusEnabled)
            {
                viewModel = new OmnitureFnacPlusViewModel();

                var currentSegment = GetCurrentSegment(popOrderForm);
                var futureSegment = GetFutureSegment(popOrderForm);
                viewModel.CurrentCustomerSegment = StringUtils.ReplaceBadChar(GetSegmentName(currentSegment));
                viewModel.FutureCustomerSegment = StringUtils.ReplaceBadChar(GetSegmentName(futureSegment));

                var card = _fnacPlusCardService.GetFnacPlusCardFromBasket(popOrderForm.LineGroups);
                if (card != null)
                    viewModel.AddedProduct = string.Format(";{0};;;;eVar48=Non applicable", card.ProductID.Value);
            }

            return viewModel;
        }

        public OmnitureOrderThankYouViewModel BuildOrderThankYou(PopOrderForm popOrderForm)
        {
            var viewModel = new OmnitureOrderThankYouViewModel()
            {
                Prefix = GetPrefix(popOrderForm),
                PipeRoute = GetPipeRoute(popOrderForm)
            };
            var trackingData = popOrderForm.GetPrivateData<TrackingData>();

            var trackingOrder = trackingData.Value;

            try
            {
                viewModel.OrderNumbers = popOrderForm.OrderGlobalInformations.MainOrderUserReference;
                viewModel.OrderType = GetOrderType(popOrderForm);
                viewModel.DeliveryMode = trackingOrder.OrderShippingMethods;
                viewModel.PaymentMethod = string.Join(",", trackingOrder.BillingMethods.Select(x => x.Label).ToArray());
                viewModel.Products = GetOrderThankYouProducts(trackingOrder);
                viewModel.OrderShippingCost = trackingOrder.OrderShippingCost;

                var aboIlliData = popOrderForm.GetPrivateData<AboIlliData>("AboIlliGroup", true);

                if (aboIlliData != null)
                {
                    viewModel.OrderedExpressPlus = aboIlliData.HasAboIlliInBasket;
                    viewModel.OrderedExpressPlusTrial = aboIlliData.AboIlliSubscriptionArticleIsTrial;
                }
            }
            catch
            {
                // something went wrong when building the model
                // but this should should not throw 500
            }

            viewModel.FnacPlus = BuildFnacPlusData(popOrderForm);
            return viewModel;
        }
        #endregion Omniture Model Builders

        #region Private
        private static void FillDateAndTimeInfo(TrackingViewModel trackingViewModel)
        {
            var now = DateTime.Now;

            trackingViewModel.Day = now.DayOfWeek.ToString();

            if (now.DayOfWeek == DayOfWeek.Saturday || now.DayOfWeek == DayOfWeek.Sunday)
                trackingViewModel.DayType = "Weekend";
            else
                trackingViewModel.DayType = "Weekday";

            trackingViewModel.Hour = BuildHoursRounded(now.TimeOfDay);
        }

        private string GetSubscriptionStatus(TrackingViewModel trackingViewModel)
        {
            var subscriptionStatus = string.Empty;
            var userExtendedContext = _userExtendedContextProvider.GetCurrentUserExtendedContext();

            if (userExtendedContext.Customer.ContractDTO.Segments != null)
            {
                if (userExtendedContext.Customer.ContractDTO.Segments.Contains(CustomerSegment.EssaiPlusNonConverti))
                {
                    subscriptionStatus = _applicationContext.GetMessage("Omniture.Common.NonConverti");
                }
                else if (userExtendedContext.Customer.ContractDTO.Segments.Contains(CustomerSegment.ExpressPlusNonRenouvele))
                {
                    subscriptionStatus = _applicationContext.GetMessage("Omniture.Common.NonRenouvele");
                }
            }
            else
            {
                subscriptionStatus = _applicationContext.GetMessage("Omniture.Common.NonAbonne");
            }

            return subscriptionStatus;
        }

        private static void FillTrackingDataFromOTYPrivatedata(PopOrderForm popOrderForm, TrackingViewModel trackingViewModel)
        {
            var otyData = popOrderForm.GetPrivateData<ThankYouOrderDetailData>(create: false);
            if (otyData != null)
            {
                var otyProductsDetails = otyData?.ThankYouOrdersDetails.SingleOrDefault(od => od.IsFnac);
                if (otyProductsDetails != null)
                {
                    trackingViewModel.BasketMix = GetBasketMixString(otyProductsDetails.Articles.Sum(a => a.Quantity), 0);

                    var productTrackingViewModels = new List<ProductTrackingViewModel>();
                    foreach (var orderDetailArticle in otyProductsDetails.Articles)
                    {
                        productTrackingViewModels.Add(new ProductTrackingViewModel
                        {
                            Prid = orderDetailArticle.Prid.ToString(),
                            Quantity = orderDetailArticle.Quantity,
                            Seller = "Fnac",
                            State = "1",
                            Stock = orderDetailArticle.AvailabilityId.GetValueOrDefault().ToString() + "-0-0",
                            Type = "Fnac.com",
                            UnitPrice = orderDetailArticle.Price.GetValueOrDefault(),
                            WithdrawalShopName = otyProductsDetails.ShopFullname,
                            ProductShippingMethod = "Self Checkout"
                        });
                    }
                    trackingViewModel.Products = productTrackingViewModels;

                    trackingViewModel.PurchaseID = otyData.MainOrderUserReference;
                    trackingViewModel.PaymentMethod = "Carte bancaire préenregistrée";
                    trackingViewModel.OrderShippingMethod = "Self Checkout";
                    trackingViewModel.OrderType = "Fnac.com";
                }
            }
        }

        private static void FillArticleDetailsFromLinegroup(PopOrderForm popOrderForm, TrackingViewModel trackingViewModel)
        {
            trackingViewModel.BasketMix = GetBasketInfos(popOrderForm);

            try
            {
                trackingViewModel.Products = (from lineGroup in popOrderForm.LineGroups
                                              let isFnac = lineGroup.OrderType != OrderTypeEnum.MarketPlace
                                              let seller = isFnac ? "Fnac" : lineGroup.Seller.DisplayName
                                              let type = lineGroup.OrderType == OrderTypeEnum.MarketPlace ? "Marketplace" : lineGroup.OrderType == OrderTypeEnum.Numerical ? "Dematerialise" : "Fnac.com"
                                              from article in lineGroup.Articles.OfType<BaseArticle>()
                                              let prid = article.ProductID.GetValueOrDefault().ToString()
                                              let state = isFnac ? "1" : (article as MarketPlaceArticle).Offer.ProductStatus == ProductStatus.Neuf ? "1" : "0"
                                              select new ProductTrackingViewModel
                                              {
                                                  Prid = article.Referentiel == 3 ? $"MP-{prid}" : prid,
                                                  Seller = seller,
                                                  Type = type,
                                                  State = state,
                                                  Quantity = article.Quantity.GetValueOrDefault(),
                                                  UnitPrice = article.PriceUserEur.GetValueOrDefault(),
                                                  Stock = article.AvailabilityId.GetValueOrDefault().ToString()
                                              }).ToList();
            }
            catch
            {
                // something went wrong when building products list
                // but this should should not throw 500
            }
        }

        private string GetPageName(PopOrderForm popOrderForm, string prefix, TrackedPage trackedPage)
        {
            switch (trackedPage)
            {
                case TrackedPage.popOnePage:
                    return _applicationContext.GetMessage($"orderpipe.pop.omniture.onepage.pagename.{prefix}");
                case TrackedPage.popShipping:
                    return _applicationContext.GetMessage($"orderpipe.pop.omniture.shipping.pagename.{prefix}");
                case TrackedPage.popPayment:
                    return _applicationContext.GetMessage($"orderpipe.pop.omniture.payment.pagename.{prefix}");
                case TrackedPage.popBasket:
                    return _applicationContext.GetMessage($"orderpipe.pop.omniture.basket.pagename.{prefix}");
                case TrackedPage.scoOrderThankYou:
                    return $"{_applicationContext.GetMessage($"orderpipe.pop.omniture.orderthankyou.pagename.webmobile")} : Self Checkout";
                default:
                    return string.Empty;
            }
        }

        private static string BuildHoursRounded(TimeSpan timeSpan)
        {
            var hours = timeSpan.Hours % 12;

            var suffix = timeSpan.Hours >= 12 ? "PM" : "AM";

            if (hours == 0)
            {
                hours = 12;
            }

            var minutes = timeSpan.Minutes >= 30 ? "30" : "00";

            return $"{hours}:{minutes}{suffix}";
        }

        private string GetAccount()
        {
            var account = string.Empty;

            if (_applicationContext.GetSiteContext().Market == "FR")
            {

                if (_applicationContext.GetSiteContext().SiteID == FnacSites.FNACPRO)
                {
                    account = "fnacproprod";
                }
                else
                {
                    account = "fnaccomprod";
                }

            }
            else if (_applicationContext.GetLocalCountry() == "ES")
            {
                account = "fnacesprod";
            }
            else if (_applicationContext.GetLocalCountry() == "PT")
            {
                account = "fnacptprod";
            }
            else if (_applicationContext.GetLocalCountry() == "CH")
            {
                account = "fnacchfrprod";
            }
            else if (_applicationContext.GetLocalCountry() == "BE")
            {
                if (_applicationContext.GetLocale() == "fr-BE")
                {
                    account = "fnacbefrprod";
                }
                else
                {
                    account = "fnacbenlprod";
                }
            }

            return account;
        }

        private string GetUID(PopOrderForm popOrderForm)
        {
            var userExtendedContext = _userExtendedContextProvider.GetCurrentUserExtendedContext();

            if (userExtendedContext.Visitor != null && userExtendedContext.Visitor.IsAuthenticated && userExtendedContext.Visitor.IsIdentified)
            {
                return $"1{popOrderForm.UserContextInformations.UID}";
            }

            return $"0{popOrderForm.UserContextInformations.UID}";
        }

        private string GetBasketSource()
        {
            var source = HttpContext.Current.Request.QueryString["o"];
            if (source != null)
            {
                return source.Equals("layer", StringComparison.InvariantCultureIgnoreCase) ? "layer" : "panier flottant";
            }

            return string.Empty;
        }

        private CustomerSegment GetCurrentSegment(PopOrderForm popOrderForm)
        {
            var userExtendedContext = _userExtendedContextProvider.GetCurrentUserExtendedContext();
            var customer = userExtendedContext.Customer;
            var segments = customer.ContractDTO.Segments;

            var currentSegment = CustomerSegment.NonAbonneExpressPlus;

            if (segments.Contains(CustomerSegment.NonAbonneExpressPlus))
                currentSegment = CustomerSegment.NonAbonneExpressPlus;
            else if (segments.Contains(CustomerSegment.ExpressPlusNonRenouvele))
                currentSegment = CustomerSegment.ExpressPlusNonRenouvele;
            else if (segments.Contains(CustomerSegment.ExpressPlus))
                currentSegment = CustomerSegment.ExpressPlus;
            else if (segments.Contains(CustomerSegment.EssaiPlusNonConverti))
                currentSegment = CustomerSegment.EssaiPlusNonConverti;
            else if (segments.Contains(CustomerSegment.EssaiPlus))
                currentSegment = CustomerSegment.EssaiPlus;

            return currentSegment;
        }

        private CustomerSegment GetFutureSegment(PopOrderForm popOrderForm)
        {
            var fnacPlusCardFromBasket = _fnacPlusCardService.GetFnacPlusCardFromBasket(popOrderForm.LineGroups);

            var futureSegment = GetCurrentSegment(popOrderForm);

            // Il y a une carte FNAC+ au panier : on change de segment
            if (fnacPlusCardFromBasket != null)
            {
                var cardType = fnacPlusCardFromBasket.TypeId.Value;

                if (cardType == (int)CategoryCardType.ConversionFnacPlus || cardType == (int)CategoryCardType.RenouvellementFnacPlus)
                    futureSegment = CustomerSegment.ExpressPlus;
                else if (cardType == (int)CategoryCardType.SouscriptionFnacPlusTrial || cardType == (int)CategoryCardType.SouscriptionFnacPlusTrialDecouverte)
                    futureSegment = CustomerSegment.EssaiPlus;
            }

            return futureSegment;
        }

        private static string GetBasketInfos(PopOrderForm popOrderForm)
        {
            var nbFnacCom = popOrderForm.LineGroups.Count(l => l.Seller.Type != Base.LineGroups.SellerType.MarketPlace);
            var nbMP = popOrderForm.LineGroups.Count(l => l.Seller.Type == Base.LineGroups.SellerType.MarketPlace);

            return GetBasketMixString(nbFnacCom, nbMP);
        }

        private static string GetBasketMixString(int nbrFnacComArticles, int nbrMarketPlaceArticles)
        {
            return $"{nbrFnacComArticles} Fcom + {nbrMarketPlaceArticles} MP";
        }

        private string GetStatusSubscription(PopOrderForm popOrderForm)
        {
            var aboIlliData = popOrderForm.GetPrivateData<AboIlliData>("AboIlliGroup", true);

            if (aboIlliData != null && aboIlliData.HasAboIlliInBasket)
            {
                if (aboIlliData.AboIlliSubscriptionArticleIsTrial)
                    return _applicationContext.GetMessage("Omniture.Common.AbonneEssai");
                else
                    return _applicationContext.GetMessage("Omniture.Common.AbonnePayant");
            }

            return _applicationContext.GetMessage("orderpipe.pop.omniture.CustomerSegment.NonAbonneExpressPlus");
        }

        private string GetStatusMembersihp(PopOrderForm popOrderForm)
        {
            var currentSegment = GetCurrentSegment(popOrderForm);

            if (currentSegment == CustomerSegment.ExpressPlus || currentSegment == CustomerSegment.EssaiPlus)
            {
                return "Adherent";
            }

            return "Non adherent";
        }

        private string GetSegmentName(CustomerSegment segment)
        {
            var libelle = string.Empty;
            switch (segment)
            {
                case CustomerSegment.NonAbonneExpressPlus:
                case CustomerSegment.ExpressPlusNonRenouvele:
                case CustomerSegment.ExpressPlus:
                case CustomerSegment.EssaiPlusNonConverti:
                case CustomerSegment.EssaiPlus:
                    libelle = _applicationContext.GetMessage("orderpipe.pop.omniture.CustomerSegment." + segment);
                    break;
            }

            return libelle;
        }

        private string GetPrefix(PopOrderForm popOrderForm)
        {
            var prefix = "desktop";

            if (popOrderForm.OrderMobileInformations.IsMobile)
            {
                prefix = "webmobile";
            }

            if (MobileAppValues.IsAndroidTabletApp(popOrderForm.OrderMobileInformations.CallingPlatform))
            {
                prefix = "android.tablet";
            }
            else if (MobileAppValues.IsAndroidApp(popOrderForm.OrderMobileInformations.CallingPlatform))
            {
                prefix = "android";
            }
            else if (MobileAppValues.IsIpadApp(popOrderForm.OrderMobileInformations.CallingPlatform))
            {
                prefix = "ipad";
            }
            else if (MobileAppValues.IsIphoneApp(popOrderForm.OrderMobileInformations.CallingPlatform))
            {
                prefix = "iphone";
            }

            return prefix;
        }

        private string GetSuffix(PopOrderForm popOrderForm)
        {
            var suffix = "";
            var oneClickData = popOrderForm.GetPrivateData<Base.OneClick.OneClickData>(create: false);

            if (oneClickData != null
                && oneClickData.IsAvailable.HasValue
                && oneClickData.IsAvailable.Value)
            {
                suffix = ".oneclic";
            }

            return suffix;
        }

        private string GetPipeRoute(PopOrderForm popOrderForm)
        {
            switch (popOrderForm.GetRoute())
            {
                case OFRoute.OneClick:
                    return "oneclic";
                case OFRoute.OnePage:
                    return "onepage";
                case OFRoute.Full:
                    return "standard";
                default:
                    return string.Empty;
            }
        }

        private string GetOrderThankYouProducts(TrackingOrder trackingOrder)
        {
            var stringBuilder = new StringBuilder();

            foreach (var trackingProduct in trackingOrder.Products)
            {
                if (stringBuilder.Length > 0)
                    stringBuilder.Append(",");

                string sellerId;
                string prodCat;

                if (trackingProduct.IsMarketPlace)
                {
                    sellerId = trackingProduct.SellerAnonymizedId;
                    prodCat = "Marketplace";
                }
                else
                {
                    sellerId = "Fnac";

                    if (trackingProduct.IsDemat)
                    {
                        prodCat = "Dematerialise";
                    }
                    else
                    {
                        prodCat = "Fnac.com";
                    }
                }

                string prodPrid;
                if (trackingProduct.ReferentialId == 3)
                {
                    prodPrid = "MP-" + trackingProduct.ProductId;
                }
                else
                {
                    prodPrid = trackingProduct.ProductId.ToString();
                }

                var prodStatus = (trackingProduct.ProductStatus == (int)ProductStatus.Neuf) ? 1 : 0;

                var total = trackingProduct.Quantity * trackingProduct.PriceUserEur;
                stringBuilder.AppendFormat(System.Globalization.CultureInfo.InvariantCulture.NumberFormat,
                        ";{0};{1};{2:F2};;evar7={3}|evar8={4}|evar9={5}|eVar23={6}|eVar24={7}",
                        prodPrid, trackingProduct.Quantity, total, sellerId, prodCat, prodStatus, string.IsNullOrEmpty(trackingProduct.ShippingStoreName) ? "Autre livraison" : trackingProduct.ShippingStoreName, trackingProduct.ShippingArticleMode);
            }

            return stringBuilder.ToString();

        }

        private string GetOrderType(PopOrderForm popOrderForm)
        {
            var anyMarketPlace = popOrderForm.LineGroups.Any(l => l.OrderType == OrderTypeEnum.MarketPlace);
            var anyNonMarketPlace = popOrderForm.LineGroups.Any(l => l.OrderType != OrderTypeEnum.MarketPlace);

            if (anyMarketPlace && anyNonMarketPlace)
            {
                return "Mixte";
            }

            if (anyMarketPlace)
            {
                return "Marketplace";
            }

            return "Fnac.com";
        }
        #endregion Private
    }
}
