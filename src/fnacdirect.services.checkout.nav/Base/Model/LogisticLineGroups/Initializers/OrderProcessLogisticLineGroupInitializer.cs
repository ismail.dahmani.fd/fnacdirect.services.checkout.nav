using FnacDirect.OrderPipe.Base.Business.LogisticLineGroups.Internals.Initializers;
using FnacDirect.OrderPipe.Base.DAL;
using FnacDirect.OrderPipe.Base.LineGroups;
using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.OrderPipe.Base.Model.B2B;
using FnacDirect.WSOrderProcess.Models;
using System.Collections.Generic;

namespace FnacDirect.OrderPipe.Base.Business.LogisticLineGroups.Initializers
{
    public class WSOrderProcessLogisticLineGroupInitializer : BaseLogisticLineGroupInitializer
    {
        private ILogisticLineGroupGenerator _logisticLineGroupGenerator;
        private readonly ICustomerDAL _customerDal;

        public WSOrderProcessLogisticLineGroupInitializer(ILogisticLineGroupGenerator logisticLineGroupGenerator, ICustomerDAL customerDal)
        {
            _logisticLineGroupGenerator = logisticLineGroupGenerator;
            _customerDal = customerDal;
        }

        public override IEnumerable<ILogisticLineGroup> Initialize(IDictionary<ILineGroupHeader, IEnumerable<Article>> selectedLineGroups)
        {
            var partnerData = Context.PrivateDataContainer.GetPrivateData<OrderProcessData>(true);

            var shippingMethodEvaluation = Context.IGotShippingMethodEvaluation.ShippingMethodEvaluation;
            var output = new List<ILogisticLineGroup>();

            foreach (var lineGroup in selectedLineGroups.Keys)
            {
                var logisticLineGroup = _logisticLineGroupGenerator.Generate(lineGroup);

                if (partnerData.OrderOrigin == (int)OrderOrigin.Batch_B2B)
                {
                    logisticLineGroup.Informations.OrderType = partnerData.OrderType;
                    logisticLineGroup.OrderType = partnerData.OrderType == (int)OrderInfoOrderTypeEnum.StandardPro ? OrderTypeEnum.Pro : OrderTypeEnum.FnacCom;
                }
                else
                {
                    logisticLineGroup.Informations.OrderType = (int)OrderInfoOrderTypeEnum.Standard;
                    logisticLineGroup.OrderType = OrderTypeEnum.FnacCom;
                }

                logisticLineGroup.OrderOrigin = partnerData.OrderOrigin;

                logisticLineGroup.Articles = new List<Article>(selectedLineGroups[lineGroup]);

                Helper.CopyShippingMethodEvaluation(lineGroup, logisticLineGroup, shippingMethodEvaluation);

                output.Add(logisticLineGroup);
            }

            return output;
        }
    }
}
