﻿using FnacDirect.Customer.Model;
using System.Collections.Generic;

namespace FnacDirect.OrderPipe.Base.Business.Shipping.Postal
{
    public class RelayAddressEntity : AddressEntity
    {
        public IEnumerable<FnacDirect.Relay.Model.DayEntity> DaysOpen { get; set; }
    }
}
