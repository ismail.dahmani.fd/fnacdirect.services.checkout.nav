using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace FnacDirect.OrderPipe.Base.Business.Operations
{
    /// <summary>
    /// Christmas data container
    /// </summary>
    public class ChristmasData : GroupData
    {
        /// <summary>
        /// Indicates if we are in current phase
        /// </summary>
        public bool HasCurrentPhase { get; set; }
        /// <summary>
        /// The shipping wording for the current phase
        /// </summary>
        public string ShippingWording { get; set; }

        /// <summary>
        /// Current phase picto (as UiResource)
        /// </summary>
        public string PictoUiresourceKey { get; set; }

        /// <summary>
        /// Basket wording
        /// </summary>
        public string BasketUiResourceKey { get; set; }

        /// <summary>
        /// Inticate if the split is automatic and free
        /// </summary>
        public bool IsFreeAndAutoSplit { get; set; }

        /// <summary>
        /// Elligible shipping methods
        /// </summary>
        public List<int> ElligibleShippingMethods { get; set; }

        /// <summary>
        /// Current phase event date
        /// </summary>
        public DateTime? EventDateOfCurrentPhase { get; set; }

        /// <summary>
        /// Flag that indicate if the christmas gift pict should be displayed next to the article
        /// </summary>
        [XmlIgnore]
        public Dictionary<int, bool> ShouldDisplayChristmasGift { get; set; } = new Dictionary<int, bool>();

        /// <summary>
        /// Determines if christmas gift sould be displayed on article
        /// </summary>
        /// <param name="prid"></param>
        /// <returns></returns>
        public bool GetShippingBeforChristmasEligibility(int prid)
        {
            return ShouldDisplayChristmasGift.ContainsKey(prid) && ShouldDisplayChristmasGift[prid];
        }

        /// <summary>
        /// Gets if the shipping method is elligible
        /// </summary>
        /// <param name="prid"></param>
        /// <returns></returns>
        public bool IsElligibleShippingMethodForChristmas(int prid)
        {
            return ElligibleShippingMethods != null && ElligibleShippingMethods.Contains(prid);
        }
    }
}
