using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FnacDirect.OrderPipe.BaseMvc.Web.CrossSellBasket
{
    public class ReferenceViewModel
    {
        public ReferenceViewModel(int reference, int prid)
        {
            Referentiel = reference;
            Prid = prid;
        }

        public int Referentiel { get; private set; }
        public int Prid { get; private set; }
    }

    public class BasketItemReferenceViewModel
        : ReferenceViewModel
    {
        private readonly List<ReferenceViewModel> _services
            = new List<ReferenceViewModel>();

        public IReadOnlyCollection<ReferenceViewModel> Services
        {
            get
            {
                return _services.AsReadOnly();
            }
        }

        public BasketItemReferenceViewModel(int reference, int prid, params ReferenceViewModel[] services)
            : base(reference, prid)
        {
            _services = new List<ReferenceViewModel>(services);
        }

        public int MasterProductId { get; set; }
    }
}
