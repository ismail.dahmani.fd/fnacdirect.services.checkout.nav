using FnacDirect.OrderPipe.Base.Model;
using FnacDirect.OrderPipe.Base.Model.Facets;
using FnacDirect.OrderPipe.Core.Services;
using FnacDirect.Technical.Framework.Web.Mvc;
using System.Linq;
using System.Text.RegularExpressions;

namespace FnacDirect.OrderPipe.Base.Business.Payment
{
    public class OrangePaymentService : IOrangePaymentService
    {
        private readonly IUserExtendedContextProvider _userExtendedContextProvider;
        private readonly IPaymentEligibilityService _paymentEligibilityService;

        private readonly Regex _customerAllowedForOrange;
        private const int _maxAmountEligibilityOrangePayment = 30;

        public OrangePaymentService(IUserExtendedContextProvider userExtendedContextProvider,
            IPipeParametersProvider pipeParametersProvider,
            IPaymentEligibilityService paymentEligibilityService)
        {
            _userExtendedContextProvider = userExtendedContextProvider;
            _paymentEligibilityService = paymentEligibilityService;

            _customerAllowedForOrange = new Regex(pipeParametersProvider.Get("orange.customer.allowed.email.regularexpression", @"fnacdarty\.com$"));
        }

        public bool IsAllowed(IGotBillingInformations iGotBillingInformations, IGotLineGroups iGotLineGroups, IGotShippingMethodEvaluation shippingMethodEvaluation)
        {
            var rules = _paymentEligibilityService.GetRules("orderpipe.pop.payment.orange.eligibilityrules");

            if (!_paymentEligibilityService.IsAllowed(iGotLineGroups, rules))
            {
                return false;
            }

            if (!IsAllowedByEmail())
            {
                return false;
            }

            var paymentSelectionData = iGotBillingInformations.GetPrivateData<PaymentSelectionData>("payment");
            var remainingAmount = paymentSelectionData.RemainingAmount;
            if (remainingAmount > _maxAmountEligibilityOrangePayment)
            {
                return false;
            }

            var standardArticles = iGotLineGroups.LineGroups.GetArticles().OfType<StandardArticle>();
            var basketContainOnlyEbookArticle = standardArticles.All(a => a.IsEbook);
            if (!basketContainOnlyEbookArticle)
            {
                return false;
            }

            var adresse = shippingMethodEvaluation?.BillingAddress;
            var isShippingAdresseFranceOrNull = adresse == null || adresse.CountryId == (int)CountryEnum.France;
            if (!isShippingAdresseFranceOrNull)
            {
                return false;
            }

            return true;
        }

        public bool IsAllowedByEmail()
        {
            var currentUser = _userExtendedContextProvider.GetCurrentUserExtendedContext();
            var email = currentUser?.ContractDto?.Info?.Email;

            return email != null && _customerAllowedForOrange.IsMatch(email);
        }
    }
}
