using FnacDirect.OrderPipe.Base.BaseModel.Model;

namespace FnacDirect.OrderPipe.Base.Model.OrderInformations
{
    public interface IOrderMobileInformations
    {
        bool IsMobile { get; set; }

        string CallingPlatform { get; set; }

        int? Origin { get; set; }

        OriginType OriginType { get; }
    }
}
